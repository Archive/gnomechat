#!/bin/sh

rm -rf ~/.gnomechat && \
gconftool-2 --recursive-unset "/apps/gnomechat" && \
sudo rm -rf ~/.gconf/apps/gnomechat ~/.gconf/schemas/apps/gnomechat /etc/gconf/gconf.xml.defaults/apps/gnomechat/ /etc/gconf/gconf.xml.defaults/schemas/apps/gnomechat/ /gnome/head/INSTALL/etc/gconf/gconf.xml.defaults/apps/gnomechat/ /gnome/head/INSTALL/etc/gconf/gconf.xml.defaults/schemas/apps/gnomechat/ /gnome/head/INSTALL/etc/gconf/schemas/gnomechat.schemas /gnome/head/INSTALL/etc/gconf/schemas/gnomechat-networks.schemas
