/*
 *  GnomeChat: src/prefs-keys.h
 *
 *  Copyright (c) 2003 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.01234567890
 */


#ifndef __PREFS_KEYS_H__
#define __PREFS_KEYS_H__


#define GNOMECHAT_PREFIX						"/apps/gnomechat"
#define FIRST_RUN_KEY							"/apps/gnomechat/first-run-completed"

/* DCC Preferences */
#define DCC_DIR									"/apps/gnomechat/dcc"
#define DCC_RECV_FOLDER_KEY						"/apps/gnomechat/dcc/receive-folder"
#define DCC_RECV_NAMING_KEY						"/apps/gnomechat/dcc/received-files-naming"
#define DCC_SEND_FOLDER_KEY						"/apps/gnomechat/dcc/send-folder"

/* Identity */
#define IDENTITY_DIR							"/apps/gnomechat/identity"
#define IDENTITY_NICK_KEY						"/apps/gnomechat/identity/nick"
#define IDENTITY_REAL_NAME_KEY					"/apps/gnomechat/identity/real-name"
#define IDENTITY_USER_NAME_KEY					"/apps/gnomechat/identity/user-name"
#define IDENTITY_PHOTO_KEY						"/apps/gnomechat/identity/photo"

/* Visual Notification (tab-changed colors, tray applet) */
#define NOTIFY_DIR								"/apps/gnomechat/notify"
#define NOTIFY_USE_TRAY_KEY						"/apps/gnomechat/notify/use-tray"
#define NOTIFY_COLOR_TEXT_KEY 					"/apps/gnomechat/notify/text-color"
#define NOTIFY_COLOR_MSG_KEY 					"/apps/gnomechat/notify/msg-color"


/* ************* *
 *  Main Window  *
 * ************* */
 
 #define WINDOWS_DIR							"/apps/gnomechat/windows"
/* Note: Network-Session-saving will not remove these keys, just the UI for
 * these keys. These keys will become "widget state" keys (so we know what
 * to do when opening a new window/chat). */

/* Widget State Keys */
#define MAIN_WINDOW_SHOW_TREE_KEY				"/apps/gnomechat/windows/show-tree"
#define MAIN_WINDOW_TREE_SIZE_KEY				"/apps/gnomechat/windows/tree-size"
#define MAIN_WINDOW_SHOW_USERLIST_KEY			"/apps/gnomechat/windows/show-userlist"
#define MAIN_WINDOW_USERLIST_SIZE_KEY			"/apps/gnomechat/windows/userlist-size"

/* Size Keys */
#define MAIN_WINDOW_FORCE_SIZE_KEY				"/apps/gnomechat/windows/main-window/force-size"
#define MAIN_WINDOW_WIDTH_KEY					"/apps/gnomechat/windows/main-window/width"
#define MAIN_WINDOW_HEIGHT_KEY					"/apps/gnomechat/windows/main-window/height"

/* Position Keys */
#define MAIN_WINDOW_FORCE_POSITION_KEY			"/apps/gnomechat/windows/force-position"
#define MAIN_WINDOW_LEFT_EDGE_KEY				"/apps/gnomechat/windows/left-edge"
#define MAIN_WINDOW_TOP_EDGE_KEY				"/apps/gnomechat/windows/top-edge"

/* Preferences */
#define MAIN_WINDOW_TOPIC_IN_TITLEBAR_KEY		"/apps/gnomechat/windows/topic-in-titlebar"
#define MAIN_WINDOW_LAYOUT_KEY					"/apps/gnomechat/windows/layout"
#define MAIN_WINDOW_TAB_SIDE_KEY				"/apps/gnomechat/windows/notebook-tab-side"


/* **************** *
 *  Connect Dialog  *
 * **************** */
 
#define CONNECT_DIALOG_WIDTH_KEY				"/apps/gnomechat/dialogs/connect/width"
#define CONNECT_DIALOG_HEIGHT_KEY				"/apps/gnomechat/dialogs/connect/height"
#define CONNECT_DIALOG_SHOW_INFO_KEY			"/apps/gnomechat/dialogs/connect/show-info"
#define CONNECT_DIALOG_INFO_SIZE_KEY			"/apps/gnomechat/dialogs/connect/info-size"
#define CONNECT_DIALOG_SELECTED_NETWORK_KEY		"/apps/gnomechat/dialogs/connect/selected-network"
#define CONNECT_DIALOG_SHOW_MOTD_KEY			"/apps/gnomechat/dialogs/connect/show-motd"


/* ******************** *
 *  Preferences Dialog  *
 * ******************** */
 
#define PREFS_DIALOG_WIDTH_KEY					"/apps/gnomechat/dialogs/prefs/width"
#define PREFS_DIALOG_HEIGHT_KEY					"/apps/gnomechat/dialogs/prefs/height"
#define PREFS_DIALOG_SHOW_WINDOW_OPTIONS_KEY	"/apps/gnomechat/dialogs/prefs/show-window-options"


/* ************* *
 *  Find Dialog  *
 * ************* */
 
#define FIND_IS_CASE_INSENSITIVE_KEY			"/apps/gnomechat/dialogs/find/case-insensitive"


/* ********************************* *
 *  Viewer Preferences (EscoViewer)  *
 * ********************************* */
 
#define VIEWER_DIR								"/apps/gnomechat/viewer"
#define VIEWER_SHOW_TIMESTAMPS_KEY				"/apps/gnomechat/viewer/show-timestamps"
#define VIEWER_USE_IMAGE_SMILEYS_KEY			"/apps/gnomechat/viewer/use-image-smileys"
// #define VIEWER_THEME_KEY                     "/apps/gnomechat/viewer/theme"


/* ******** *
 *  Alerts  *
 * ******** */
 
#define ASK_TO_CLEAR_CHAT_KEY					"/apps/gnomechat/alerts/ask-to-clear-chat"
#define ASK_TO_REMOVE_NETWORK_KEY				"/apps/gnomechat/alerts/ask-to-remove-network"
#define ASK_TO_REMOVE_SERVER_KEY				"/apps/gnomechat/alerts/ask-to-remove-server"


/* Below this point are keys that will die at some point */

/* #FIXME: /apps/gnomechat/interface must die! -- And will once I rework EscoChatBox to use more
 * Paned widgets, and do viewer theming */
#define INTERFACE_USE_MULTILINE_ENTRY_KEY		"/apps/gnomechat/interface/autosave/use-multiline-entry"


/* ************************************************ *
 *  Broken GConf-based Fonts/Colors -- Don't Touch  *
 * ************************************************ */
/* #FIXME: The keys below should be superceeded by some form of
 * freedesktop.org-compatible, XML-based theming. */

/* Directories */
#define VIEWER_STYLES_DIR						"/apps/gnomechat/interface/viewer"

#define VIEWER_GLOBAL_DIR						"/apps/gnomechat/interface/viewer/global"

#define VIEWER_ME_ACTION_DIR					"/apps/gnomechat/interface/viewer/me/action"
#define VIEWER_ME_NICK_DIR						"/apps/gnomechat/interface/viewer/me/nick"

#define VIEWER_TO_ME_ACTION_DIR					"/apps/gnomechat/interface/viewer/to-me/action"
#define VIEWER_TO_ME_TEXT_DIR					"/apps/gnomechat/interface/viewer/to-me/text"
#define VIEWER_TO_ME_NICK_DIR					"/apps/gnomechat/interface/viewer/to-me/nick"

#define VIEWER_OTHERS_ACTION_DIR				"/apps/gnomechat/interface/viewer/others/action"
#define VIEWER_OTHERS_NICK_DIR					"/apps/gnomechat/interface/viewer/others/nick"

#define VIEWER_SERVER_NAME_DIR					"/apps/gnomechat/interface/viewer/server/name"
#define VIEWER_SERVER_TEXT_DIR					"/apps/gnomechat/interface/viewer/server/text"

#define VIEWER_CHANNEL_NAME_DIR					"/apps/gnomechat/interface/viewer/channel/name"
#define VIEWER_CHANNEL_NICK_DIR					"/apps/gnomechat/interface/viewer/channel/nick"
#define VIEWER_CHANNEL_TEXT_DIR					"/apps/gnomechat/interface/viewer/channel/text"
#define VIEWER_CHANNEL_VALUE_DIR				"/apps/gnomechat/interface/viewer/channel/value"

#define VIEWER_LINK_DIR							"/apps/gnomechat/interface/viewer/link"
#define VIEWER_ERROR_DIR						"/apps/gnomechat/interface/viewer/error"

/* Key Bits */
/* "global" Dir Only */
#define FONT_KEY_BIT	"font"
/* In every viewer style */
#define FG_KEY_BIT		"foreground"
#define BG_KEY_BIT		"background"
#define BOLD_KEY_BIT	"bold"
#define ITALIC_KEY_BIT	"italic"
#define ULINE_KEY_BIT	"underline"


/* ******************************************* *
 *  Broken, GConf Network List -- Don't Touch  *
 * ******************************************* */
/* #FIXME: most of these keys will die when the RDF arch is totally ready.
 * The basic concept of a "networks" dir will remain, for saving things like channel passwords,
 * possibly network session, etc. -- IOW, legitimate network-specific prefs */
#define NETWORK_DIR								"/apps/gnomechat/networks"
#define NETWORK_LIST_KEY						"/apps/gnomechat/networks/network-ids"
#define CATEGORY_DIR							"/apps/gnomechat/networks/categories"

#define ICON_KEY_BIT							"icon"
#define NAME_KEY_BIT							"name"
#define DESCRIPTION_KEY_BIT						"description"
#define WEBSITE_KEY_BIT							"website"
#define CATEGORY_KEY_BIT						"category"
#define CHANNELS_KEY_BIT						"autojoin-channels"
#define AUTOCONNECT_KEY_BIT						"autoconnect"
#define SERVER_IDS_KEY_BIT						"server-ids"
#define ENABLED_KEY_BIT							"enabled"
#define LOCATION_KEY_BIT						"location"
#define COUNTRY_CODE_KEY_BIT					"country-code"
#define ADDRESS_KEY_BIT							"address"
#define PORT_KEY_BIT							"port"
#define PASSWD_KEY_BIT							"passwd"
#define CHARSET_KEY_BIT							"character-set"


#endif /* __PREFS_KEYS_H__ */
