/*
 *  GnomeChat: src/gnomechat-vfs-transfer.c
 *
 *  Copyright (c) 2003 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.01234567890
 */

#ifdef HAVE_CONFIG_H
#	include <config.h>
#endif /* HAVE_CONFIG_H */

#include "gnomechat-vfs-transfer.h"

enum
{
	PROP_0,
	UPDATE_ONLY
};



/* *************************************************************************** *
 *  GnomechatVFSTransfer -- Single-File GnomeVFS GnomechatTransfer Object      *
 * *************************************************************************** */

static gpointer vfs_parent_class = NULL;


/* GnomechatTransferIface Functions */
static GnomechatTransferType
gnomechat_vfs_transfer_get_transfer_type (GnomechatTransfer * xfer)
{
	GnomechatVFSTransfer *vfs_xfer = (GnomechatVFSTransfer *) xfer;

	g_return_val_if_fail (GNOMECHAT_IS_VFS_TRANSFER (xfer), GNOMECHAT_TRANSFER_INVALID);

	return vfs_xfer->type;
}


static GnomechatTransferStatus
gnomechat_vfs_transfer_get_status (GnomechatTransfer * xfer)
{
	GnomechatVFSTransfer *vfs_xfer = (GnomechatVFSTransfer *) xfer;

	g_return_val_if_fail (GNOMECHAT_IS_VFS_TRANSFER (xfer), GNOMECHAT_TRANSFER_STATUS_INVALID);

	return vfs_xfer->status;
}


static GnomeVFSResult
gnomechat_vfs_transfer_get_error (GnomechatTransfer * xfer)
{
	GnomechatVFSTransfer *vfs_xfer = (GnomechatVFSTransfer *) xfer;

	g_return_val_if_fail (GNOMECHAT_IS_VFS_TRANSFER (xfer), GNOME_VFS_ERROR_INTERNAL);

	return vfs_xfer->error;
}


static GnomeVFSURI *
gnomechat_vfs_transfer_get_from_uri (GnomechatTransfer * xfer)
{
	GnomechatVFSTransfer *vfs_xfer = (GnomechatVFSTransfer *) xfer;

	g_return_val_if_fail (GNOMECHAT_IS_VFS_TRANSFER (xfer), NULL);

	return vfs_xfer->from_uri;
}


static void
gnomechat_vfs_transfer_set_from_uri (GnomechatTransfer * xfer,
									 GnomeVFSURI * uri)
{
	GnomechatVFSTransfer *vfs_xfer = (GnomechatVFSTransfer *) xfer;

	g_return_if_fail (GNOMECHAT_IS_VFS_TRANSFER (xfer));
	g_return_if_fail (vfs_xfer->status == GNOMECHAT_TRANSFER_STATUS_READY
					  || vfs_xfer->status == GNOMECHAT_TRANSFER_STATUS_TRANSFERRING);
	g_return_if_fail (uri != NULL);

	vfs_xfer->from_uri = gnome_vfs_uri_ref (uri);
}


static GnomeVFSURI *
gnomechat_vfs_transfer_get_to_uri (GnomechatTransfer * xfer)
{
	GnomechatVFSTransfer *vfs_xfer = (GnomechatVFSTransfer *) xfer;

	g_return_val_if_fail (GNOMECHAT_IS_VFS_TRANSFER (xfer), NULL);

	return vfs_xfer->to_uri;
}


static void
gnomechat_vfs_transfer_set_to_uri (GnomechatTransfer * xfer,
								   GnomeVFSURI * uri)
{
	GnomechatVFSTransfer *vfs_xfer = (GnomechatVFSTransfer *) xfer;

	g_return_if_fail (GNOMECHAT_IS_VFS_TRANSFER (xfer));
	g_return_if_fail (vfs_xfer->status != GNOMECHAT_TRANSFER_STATUS_CHECKING
					  && vfs_xfer->status != GNOMECHAT_TRANSFER_STATUS_TRANSFERRING);
	g_return_if_fail (uri != NULL);

	vfs_xfer->to_uri = gnome_vfs_uri_ref (uri);
}


static GnomeVFSFileSize
gnomechat_vfs_transfer_get_file_size (GnomechatTransfer * xfer)
{
	GnomechatVFSTransfer *vfs_xfer = (GnomechatVFSTransfer *) xfer;

	g_return_val_if_fail (GNOMECHAT_IS_VFS_TRANSFER (xfer), 0);

	return vfs_xfer->file_size;
}


static GnomeVFSFileSize
gnomechat_vfs_transfer_get_bytes_copied (GnomechatTransfer * xfer)
{
	GnomechatVFSTransfer *vfs_xfer = (GnomechatVFSTransfer *) xfer;

	g_return_val_if_fail (GNOMECHAT_IS_VFS_TRANSFER (xfer), 0);

	return vfs_xfer->bytes_copied;
}


/* Called when transfer progress occurs */
static gint
xfer_progress_update_cb (GnomeVFSAsyncHandle * handle,
						 GnomeVFSXferProgressInfo * info,
						 GnomechatVFSTransfer * vfs_xfer)
{
	switch (info->status)
	{
	case GNOME_VFS_XFER_PROGRESS_STATUS_OK:
		vfs_xfer->file_size = info->file_size;
		vfs_xfer->bytes_copied = info->bytes_copied;

		/* Emit the GnomechatTransfer "progress" signal */
		gnomechat_transfer_progress ((GnomechatTransfer *) vfs_xfer, info->bytes_copied);

		/* We're done... */
		if (info->file_size == info->bytes_copied)
		{
			vfs_xfer->status = GNOMECHAT_TRANSFER_STATUS_DONE;
			gnomechat_transfer_status_changed ((GnomechatTransfer *) vfs_xfer,
											   vfs_xfer->status);
		}
		break;

	case GNOME_VFS_XFER_PROGRESS_STATUS_VFSERROR:
		vfs_xfer->handle = NULL;
		vfs_xfer->error = info->vfs_status;
		vfs_xfer->status = GNOMECHAT_TRANSFER_STATUS_ERROR;

		gnomechat_transfer_status_changed ((GnomechatTransfer *) vfs_xfer,
										   vfs_xfer->status);
		break;

	case GNOME_VFS_XFER_PROGRESS_STATUS_OVERWRITE:
		g_return_val_if_reached (GNOME_VFS_XFER_OVERWRITE_ACTION_REPLACE);
		break;

	case GNOME_VFS_XFER_PROGRESS_STATUS_DUPLICATE:
		g_return_val_if_reached (GNOME_VFS_XFER_ERROR_ACTION_SKIP);
		break;
	}

	return GNOME_VFS_OK;
}


static void
xfer_get_file_info_cb (GnomeVFSAsyncHandle * handle,
					   GList * results,
					   GnomechatVFSTransfer * vfs_xfer)
{
	GList *list;
	time_t to_file_mtime = 0,
	  from_file_mtime = 0;

	vfs_xfer->handle = NULL;

	for (list = results; list != NULL; list = list->next)
	{
		GnomeVFSGetFileInfoResult *result = list->data;

		if (result->result != GNOME_VFS_OK)
		{
			vfs_xfer->handle = NULL;
			vfs_xfer->status = GNOMECHAT_TRANSFER_STATUS_ERROR;
			vfs_xfer->error = result->result;

			gnomechat_transfer_status_changed ((GnomechatTransfer *) vfs_xfer,
											   vfs_xfer->status);
			return;
		}
		else if (gnome_vfs_uri_equal (result->uri, vfs_xfer->to_uri)
				 && result->file_info->flags & GNOME_VFS_FILE_INFO_FIELDS_MTIME)
		{
			to_file_mtime = result->file_info->mtime;
		}
		else if (gnome_vfs_uri_equal (result->uri, vfs_xfer->from_uri)
				 && result->file_info->flags & GNOME_VFS_FILE_INFO_FIELDS_MTIME)
		{
			from_file_mtime = result->file_info->mtime;
		}
	}

	/* If the mtimes don't match, or if either is 0, re-get the file */
	if (!(vfs_xfer->update_only) || to_file_mtime != from_file_mtime || to_file_mtime == 0
		|| from_file_mtime == 0)
	{
		GList *from_uris = g_list_append (NULL, vfs_xfer->from_uri),
		 *to_uris = g_list_append (NULL, vfs_xfer->to_uri);

		vfs_xfer->error = gnome_vfs_async_xfer (&(vfs_xfer->handle), from_uris, to_uris,
												GNOME_VFS_XFER_DEFAULT,
												GNOME_VFS_XFER_ERROR_MODE_QUERY,
												GNOME_VFS_XFER_OVERWRITE_MODE_REPLACE,
												GNOME_VFS_PRIORITY_DEFAULT,
												(GnomeVFSAsyncXferProgressCallback)
												xfer_progress_update_cb, vfs_xfer,
												NULL, NULL);

		vfs_xfer->status = GNOMECHAT_TRANSFER_STATUS_TRANSFERRING;

		gnomechat_transfer_status_changed ((GnomechatTransfer *) vfs_xfer,
										   vfs_xfer->status);
	}
}


static void
gnomechat_vfs_transfer_start_transfer (GnomechatTransfer * xfer)
{
	GnomechatVFSTransfer *vfs_xfer = (GnomechatVFSTransfer *) xfer;
	GList *uris = NULL;

	g_return_if_fail (GNOMECHAT_IS_VFS_TRANSFER (xfer));
	g_return_if_fail (vfs_xfer->status != GNOMECHAT_TRANSFER_STATUS_CHECKING &&
					  vfs_xfer->status != GNOMECHAT_TRANSFER_STATUS_TRANSFERRING);

	uris = g_list_prepend (uris, vfs_xfer->from_uri);
	uris = g_list_prepend (uris, vfs_xfer->to_uri);

	gnome_vfs_async_get_file_info (&(vfs_xfer->handle), uris, GNOME_VFS_FILE_INFO_DEFAULT,
								   GNOME_VFS_PRIORITY_DEFAULT,
								   (GnomeVFSAsyncGetFileInfoCallback) xfer_get_file_info_cb, xfer);
}


static void
gnomechat_vfs_transfer_cancel_transfer (GnomechatTransfer * xfer)
{
	GnomechatVFSTransfer *vfs_xfer = (GnomechatVFSTransfer *) xfer;

	g_return_if_fail (GNOMECHAT_IS_VFS_TRANSFER (xfer));

	if (vfs_xfer->handle != NULL)
		gnome_vfs_async_cancel (vfs_xfer->handle);
}


/* GnomechatTransferIface Initialization */
static void
gnomechat_vfs_transfer_transfer_init (GnomechatTransferIface * iface)
{
	iface->get_transfer_type = gnomechat_vfs_transfer_get_transfer_type;
	iface->get_status = gnomechat_vfs_transfer_get_status;
	iface->get_error = gnomechat_vfs_transfer_get_error;

	iface->get_file_size = gnomechat_vfs_transfer_get_file_size;
	iface->get_bytes_copied = gnomechat_vfs_transfer_get_bytes_copied;

	iface->get_from_uri = gnomechat_vfs_transfer_get_from_uri;
	iface->set_from_uri = gnomechat_vfs_transfer_set_from_uri;

	iface->get_to_uri = gnomechat_vfs_transfer_get_to_uri;
	iface->set_to_uri = gnomechat_vfs_transfer_set_to_uri;

	iface->start_transfer = gnomechat_vfs_transfer_start_transfer;
	iface->cancel_transfer = gnomechat_vfs_transfer_cancel_transfer;

	iface->get_n_files = NULL;
	iface->get_file_index = NULL;
}


/* GObject Callbacks */
static void
gnomechat_vfs_transfer_get_property (GObject * object,
									 guint param_id,
									 GValue * value,
									 GParamSpec * pspec)
{
	GnomechatVFSTransfer *xfer = GNOMECHAT_VFS_TRANSFER (object);

	switch (param_id)
	{
	case UPDATE_ONLY:
		g_value_set_boolean (value, xfer->update_only);
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}


static void
gnomechat_vfs_transfer_set_property (GObject * object,
									 guint param_id,
									 const GValue * value,
									 GParamSpec * pspec)
{
	GnomechatVFSTransfer *xfer = GNOMECHAT_VFS_TRANSFER (object);

	switch (param_id)
	{
	case UPDATE_ONLY:
		xfer->update_only = g_value_get_boolean (value);
		g_object_notify (object, "update-only");
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}


static void
gnomechat_vfs_transfer_finalize (GObject * object)
{
	GnomechatVFSTransfer *vfs_xfer = GNOMECHAT_VFS_TRANSFER (object);

	if (vfs_xfer->handle != NULL)
		gnome_vfs_async_cancel (vfs_xfer->handle);

	gnome_vfs_uri_unref (vfs_xfer->from_uri);
	gnome_vfs_uri_unref (vfs_xfer->to_uri);

	if (G_OBJECT_CLASS (vfs_parent_class)->finalize != NULL)
		(*G_OBJECT_CLASS (vfs_parent_class)->finalize) (object);
}


/* GType Initialization */
static void
gnomechat_vfs_transfer_class_init (GnomechatVFSTransferClass * class)
{
	GObjectClass *object_class = G_OBJECT_CLASS (class);

	vfs_parent_class = g_type_class_peek_parent (class);

	object_class->set_property = gnomechat_vfs_transfer_set_property;
	object_class->get_property = gnomechat_vfs_transfer_get_property;
	object_class->finalize = gnomechat_vfs_transfer_finalize;

	g_object_class_install_property (object_class, UPDATE_ONLY,
									 g_param_spec_boolean ("update-only",
														   "Update Only",
														   "Only update if the from "
														   "file is newer", FALSE,
														   (G_PARAM_READWRITE |
															G_PARAM_CONSTRUCT)));
}


static void
gnomechat_vfs_transfer_instance_init (GnomechatVFSTransfer * vfs_xfer)
{
}


GType
gnomechat_vfs_transfer_get_type (void)
{
	static GType type = G_TYPE_INVALID;

	if (type == G_TYPE_INVALID)
	{
		static const GTypeInfo info = {
			sizeof (GnomechatVFSTransferClass),
			NULL,				/* base_init */
			NULL,				/* base_finalize */
			(GClassInitFunc) gnomechat_vfs_transfer_class_init,
			NULL,				/* class_finalize */
			NULL,				/* class_data */
			sizeof (GnomechatVFSTransfer),
			0,					/* n_preallocs */
			(GInstanceInitFunc) gnomechat_vfs_transfer_instance_init,
		};

		static const GInterfaceInfo xfer_info = {
			(GInterfaceInitFunc) gnomechat_vfs_transfer_transfer_init,
			NULL,
			NULL
		};

		type = g_type_register_static (G_TYPE_OBJECT, "GnomechatVFSTransfer", &info, 0);
		g_type_add_interface_static (type, GNOMECHAT_TYPE_TRANSFER, &xfer_info);
	}

	return type;
}


GnomechatVFSTransfer *
gnomechat_vfs_transfer_new (GnomeVFSURI * from_uri,
							GnomeVFSURI * to_uri,
							gboolean update_only)
{
	GnomechatVFSTransfer *xfer;

	g_return_val_if_fail (from_uri != NULL, NULL);
	g_return_val_if_fail (to_uri != NULL, NULL);

	xfer = g_object_new (GNOMECHAT_TYPE_VFS_TRANSFER, "update-only", update_only, NULL);
	xfer->from_uri = gnome_vfs_uri_ref (from_uri);
	xfer->to_uri = gnome_vfs_uri_ref (to_uri);

	return xfer;
}


/* *************************************************************************** *
 *  GnomechatVFSMultiTransfer -- Multi-File GnomeVFS GnomechatTransfer Object  *
 * *************************************************************************** */

static gpointer multi_parent_class = NULL;


/* GnomechatTransferIface Functions */
static GnomechatTransferType
gnomechat_vfs_multi_transfer_get_transfer_type (GnomechatTransfer * xfer)
{
	g_return_val_if_fail (GNOMECHAT_IS_VFS_MULTI_TRANSFER (xfer), GNOMECHAT_TRANSFER_INVALID);

	return GNOMECHAT_TRANSFER_MULTI;
}


static GnomechatTransferStatus
gnomechat_vfs_multi_transfer_get_status (GnomechatTransfer * xfer)
{
	GnomechatVFSMultiTransfer *multi_xfer = (GnomechatVFSMultiTransfer *) xfer;

	g_return_val_if_fail (GNOMECHAT_IS_VFS_MULTI_TRANSFER (xfer),
						  GNOMECHAT_TRANSFER_STATUS_INVALID);

	return multi_xfer->status;
}


static GnomeVFSResult
gnomechat_vfs_multi_transfer_get_error (GnomechatTransfer * xfer)
{
	GnomechatVFSMultiTransfer *multi_xfer = (GnomechatVFSMultiTransfer *) xfer;

	g_return_val_if_fail (GNOMECHAT_IS_VFS_MULTI_TRANSFER (xfer), GNOME_VFS_ERROR_INTERNAL);

	return multi_xfer->error;
}


static GnomeVFSFileSize
gnomechat_vfs_multi_transfer_get_file_size (GnomechatTransfer * xfer)
{
	GnomechatVFSMultiTransfer *multi_xfer = (GnomechatVFSMultiTransfer *) xfer;

	g_return_val_if_fail (GNOMECHAT_IS_VFS_MULTI_TRANSFER (xfer), 0);

	return multi_xfer->total_size;
}


static GnomeVFSFileSize
gnomechat_vfs_multi_transfer_get_bytes_copied (GnomechatTransfer * xfer)
{
	GnomechatVFSMultiTransfer *multi_xfer = (GnomechatVFSMultiTransfer *) xfer;

	g_return_val_if_fail (GNOMECHAT_IS_VFS_MULTI_TRANSFER (xfer), 0);

	return multi_xfer->bytes_copied;
}


static GnomeVFSURI *
gnomechat_vfs_multi_transfer_get_from_uri (GnomechatTransfer * xfer)
{
	GnomechatVFSMultiTransfer *multi_xfer = (GnomechatVFSMultiTransfer *) xfer;

	g_return_val_if_fail (GNOMECHAT_IS_VFS_MULTI_TRANSFER (xfer), NULL);

	return (multi_xfer->current_xfer != NULL ?
			gnome_vfs_uri_ref (multi_xfer->current_xfer->from_uri) : NULL);
}


static GnomeVFSURI *
gnomechat_vfs_multi_transfer_get_to_uri (GnomechatTransfer * xfer)
{
	GnomechatVFSMultiTransfer *multi_xfer = (GnomechatVFSMultiTransfer *) xfer;

	g_return_val_if_fail (GNOMECHAT_IS_VFS_MULTI_TRANSFER (xfer), NULL);

	return (multi_xfer->current_xfer != NULL ?
			gnome_vfs_uri_ref (multi_xfer->current_xfer->to_uri) : NULL);
}


static void
emit_vfs_xfer_done (GnomeVFSURI * uri,
					GnomechatVFSTransfer * vfs_xfer,
					GnomechatVFSMultiTransfer * multi_xfer)
{
	if (vfs_xfer->status != GNOMECHAT_TRANSFER_STATUS_DONE)
	{
		vfs_xfer->status = GNOMECHAT_TRANSFER_STATUS_DONE;
		gnomechat_transfer_status_changed ((GnomechatTransfer *) vfs_xfer,
										   GNOMECHAT_TRANSFER_STATUS_DONE);
	}
}


static gint
multi_xfer_progress_update_cb (GnomeVFSAsyncHandle * handle,
							   GnomeVFSXferProgressInfo * info,
							   GnomechatVFSMultiTransfer * multi_xfer)
{
	gint retval = 1;

	switch (info->status)
	{
	case GNOME_VFS_XFER_PROGRESS_STATUS_OK:
		{
			GnomeVFSURI *uri;
			GnomechatVFSTransfer *vfs_xfer;

			switch (info->phase)
			{
			case GNOME_VFS_XFER_PHASE_READYTOGO:
				multi_xfer->status = GNOMECHAT_TRANSFER_STATUS_TRANSFERRING;
				gnomechat_transfer_status_changed ((GnomechatTransfer *) multi_xfer,
												   GNOMECHAT_TRANSFER_STATUS_TRANSFERRING);
				break;

			case GNOME_VFS_XFER_PHASE_COPYING:
			case GNOME_VFS_XFER_PHASE_MOVING:
				uri = gnome_vfs_uri_new (info->source_name);
				vfs_xfer = g_hash_table_lookup (multi_xfer->transfers, uri);
				gnome_vfs_uri_unref (uri);

				if (vfs_xfer->status != GNOMECHAT_TRANSFER_STATUS_TRANSFERRING)
				{
					vfs_xfer->status = GNOMECHAT_TRANSFER_STATUS_TRANSFERRING;
					gnomechat_transfer_status_changed ((GnomechatTransfer *) vfs_xfer,
													   GNOMECHAT_TRANSFER_STATUS_TRANSFERRING);
				}

				vfs_xfer->file_size = info->file_size;
				vfs_xfer->bytes_copied = info->bytes_copied;
				gnomechat_transfer_progress ((GnomechatTransfer *) vfs_xfer, info->bytes_copied);

				multi_xfer->total_size = info->bytes_total;
				multi_xfer->bytes_copied = info->total_bytes_copied;

				multi_xfer->n_files = info->files_total;
				multi_xfer->file_index = info->file_index;
				gnomechat_transfer_progress ((GnomechatTransfer *) multi_xfer,
											 info->total_bytes_copied);
				break;

			case GNOME_VFS_XFER_PHASE_FILECOMPLETED:
				uri = gnome_vfs_uri_new (info->source_name);
				vfs_xfer = g_hash_table_lookup (multi_xfer->transfers, uri);
				gnome_vfs_uri_unref (uri);

				vfs_xfer->status = GNOMECHAT_TRANSFER_STATUS_DONE;
				gnomechat_transfer_status_changed ((GnomechatTransfer *) vfs_xfer,
												   GNOMECHAT_TRANSFER_STATUS_DONE);
				break;

			case GNOME_VFS_XFER_PHASE_COMPLETED:
				g_hash_table_foreach (multi_xfer->transfers, (GHFunc) emit_vfs_xfer_done,
									  multi_xfer);

				multi_xfer->status = GNOMECHAT_TRANSFER_STATUS_DONE;
				gnomechat_transfer_status_changed ((GnomechatTransfer *) multi_xfer,
												   GNOMECHAT_TRANSFER_STATUS_DONE);
				break;

			default:
				break;
			}
		}
		break;

	case GNOME_VFS_XFER_PROGRESS_STATUS_VFSERROR:
		//vfs_xfer->error = info->vfs_status;
		//vfs_xfer->status = GNOMECHAT_TRANSFER_STATUS_ERROR;
		//gnomechat_transfer_status_changed ((GnomechatTransfer *) vfs_xfer,
		//                                 GNOMECHAT_TRANSFER_STATUS_ERROR);

		multi_xfer->handle = NULL;
		multi_xfer->error = info->vfs_status;
		gnomechat_transfer_status_changed ((GnomechatTransfer *) multi_xfer,
										   GNOMECHAT_TRANSFER_STATUS_ERROR);

		retval = GNOME_VFS_XFER_ERROR_ACTION_SKIP;
		break;

	case GNOME_VFS_XFER_PROGRESS_STATUS_OVERWRITE:
		retval = GNOME_VFS_XFER_OVERWRITE_ACTION_REPLACE;
		break;

	case GNOME_VFS_XFER_PROGRESS_STATUS_DUPLICATE:
		retval = GNOME_VFS_XFER_ERROR_ACTION_SKIP;
		break;
	}

	return retval;
}


static void
collect_changed_transfers (GnomeVFSURI * uri,
						   GnomechatVFSTransfer * xfer,
						   GList ** list)
{
	if (xfer->status != GNOMECHAT_TRANSFER_STATUS_ERROR
		&& (xfer->from_mtime != xfer->to_mtime || xfer->to_mtime == 0 || xfer->from_mtime == 0)
		&& g_list_find (*list, xfer) == NULL)
	{
		*list = g_list_append (*list, xfer);
	}
}


static void
multi_get_file_info_cb (GnomeVFSAsyncHandle * handle,
						GList * results,
						GnomechatVFSMultiTransfer * multi_xfer)
{
	GnomechatVFSTransfer *vfs_xfer;
	GList *xfer_list = NULL,
	 *from_uris = NULL,
	 *to_uris = NULL;

	multi_xfer->handle = NULL;

	for (; results != NULL; results = results->next)
	{
		GnomeVFSGetFileInfoResult *result = results->data;

		vfs_xfer = g_hash_table_lookup (multi_xfer->transfers, result->uri);

		if (vfs_xfer != NULL)
		{
			/* If we're the source URI */
			if (gnome_vfs_uri_equal (result->uri, vfs_xfer->from_uri))
			{
				if (result->result == GNOME_VFS_OK)
				{
					if (result->file_info->flags & GNOME_VFS_FILE_INFO_FIELDS_MTIME)
					{
						vfs_xfer->from_mtime = result->file_info->mtime;
					}
					else
					{
						vfs_xfer->from_mtime = 0;
					}
				}
				else
				{
					vfs_xfer->status = GNOMECHAT_TRANSFER_STATUS_ERROR;
					vfs_xfer->error = result->result;
					gnomechat_transfer_status_changed ((GnomechatTransfer *) multi_xfer,
													   GNOMECHAT_TRANSFER_STATUS_ERROR);
				}
			}
			/* Otherwise, if we're the target URI */
			else if (gnome_vfs_uri_equal (result->uri, vfs_xfer->to_uri))
			{
				if (result->result == GNOME_VFS_OK)
				{
					if (result->file_info->flags & GNOME_VFS_FILE_INFO_FIELDS_MTIME)
					{
						vfs_xfer->to_mtime = result->file_info->mtime;
					}
					else
					{
						vfs_xfer->to_mtime = 0;
					}
				}
				else
				{
					vfs_xfer->to_mtime = 0;
				}
			}
			else
			{
				g_assert_not_reached ();
			}
		}
	}

	g_hash_table_foreach (multi_xfer->transfers, (GHFunc) collect_changed_transfers,
						  &xfer_list);

	/* No changed transfers */
	if (xfer_list == NULL)
	{
		multi_xfer->status = GNOMECHAT_TRANSFER_STATUS_DONE;
		gnomechat_transfer_status_changed ((GnomechatTransfer *) multi_xfer,
										   GNOMECHAT_TRANSFER_STATUS_DONE);
		return;
	}

	for (; xfer_list != NULL; xfer_list = g_list_remove_link (xfer_list, xfer_list))
	{
		vfs_xfer = xfer_list->data;

		if (vfs_xfer != NULL)
		{
			from_uris = g_list_prepend (from_uris, gnome_vfs_uri_ref (vfs_xfer->from_uri));
			to_uris = g_list_prepend (to_uris, gnome_vfs_uri_ref (vfs_xfer->to_uri));
		}
	}

	from_uris = g_list_reverse (from_uris);
	to_uris = g_list_reverse (to_uris);

	multi_xfer->error = gnome_vfs_async_xfer (&(multi_xfer->handle), from_uris, to_uris,
											  GNOME_VFS_XFER_DEFAULT,
											  GNOME_VFS_XFER_ERROR_MODE_QUERY,
											  GNOME_VFS_XFER_OVERWRITE_MODE_REPLACE,
											  GNOME_VFS_PRIORITY_DEFAULT,
											  (GnomeVFSAsyncXferProgressCallback)
											  multi_xfer_progress_update_cb, multi_xfer,
											  NULL, NULL);
}


static void
collect_all_uris (GnomeVFSURI * uri,
				  GnomechatVFSTransfer * xfer,
				  GList ** list)
{
	if (xfer->status != GNOMECHAT_TRANSFER_STATUS_CHECKING)
	{
		xfer->status = GNOMECHAT_TRANSFER_STATUS_CHECKING;
		xfer->error = GNOME_VFS_ERROR_IN_PROGRESS;
		gnomechat_transfer_status_changed ((GnomechatTransfer *) xfer,
										   GNOMECHAT_TRANSFER_STATUS_CHECKING);
	}

	*list = g_list_prepend (*list, gnome_vfs_uri_ref (uri));
}


static void
gnomechat_vfs_multi_transfer_start_transfer (GnomechatTransfer * xfer)
{
	GnomechatVFSMultiTransfer *multi_xfer = (GnomechatVFSMultiTransfer *) xfer;
	GList *uris = NULL;

	g_return_if_fail (GNOMECHAT_IS_VFS_MULTI_TRANSFER (xfer));

	multi_xfer->status = GNOMECHAT_TRANSFER_STATUS_CHECKING;
	multi_xfer->error = GNOME_VFS_ERROR_IN_PROGRESS;
	gnomechat_transfer_status_changed (xfer, GNOMECHAT_TRANSFER_STATUS_CHECKING);

	g_hash_table_foreach (multi_xfer->transfers, (GHFunc) collect_all_uris, &uris);

	uris = g_list_reverse (uris);

	gnome_vfs_async_get_file_info (&(multi_xfer->handle), uris, GNOME_VFS_FILE_INFO_DEFAULT,
								   GNOME_VFS_PRIORITY_DEFAULT,
								   (GnomeVFSAsyncGetFileInfoCallback) multi_get_file_info_cb,
								   multi_xfer);
}


static void
gnomechat_vfs_multi_transfer_cancel_transfer (GnomechatTransfer * xfer)
{
	GnomechatVFSMultiTransfer *multi_xfer = (GnomechatVFSMultiTransfer *) xfer;

	g_return_if_fail (GNOMECHAT_IS_VFS_MULTI_TRANSFER (xfer));

	if (multi_xfer->handle != NULL)
		gnome_vfs_async_cancel (multi_xfer->handle);
}


static gulong
gnomechat_vfs_multi_transfer_get_n_files (GnomechatTransfer * xfer)
{
	GnomechatVFSMultiTransfer *multi_xfer = (GnomechatVFSMultiTransfer *) xfer;

	g_return_val_if_fail (GNOMECHAT_IS_VFS_MULTI_TRANSFER (xfer), 0);

	return multi_xfer->n_files;
}


static gulong
gnomechat_vfs_multi_transfer_get_file_index (GnomechatTransfer * xfer)
{
	GnomechatVFSMultiTransfer *multi_xfer = (GnomechatVFSMultiTransfer *) xfer;

	g_return_val_if_fail (GNOMECHAT_IS_VFS_MULTI_TRANSFER (xfer), 0);

	return multi_xfer->file_index;
}


/* GnomechatTransferIface Initialization */
static void
gnomechat_vfs_multi_transfer_transfer_init (GnomechatTransferIface * iface)
{
	iface->get_transfer_type = gnomechat_vfs_multi_transfer_get_transfer_type;
	iface->get_status = gnomechat_vfs_multi_transfer_get_status;
	iface->get_error = gnomechat_vfs_multi_transfer_get_error;

	iface->get_file_size = gnomechat_vfs_multi_transfer_get_file_size;
	iface->get_bytes_copied = gnomechat_vfs_multi_transfer_get_bytes_copied;

	iface->get_from_uri = gnomechat_vfs_multi_transfer_get_from_uri;
	iface->set_from_uri = NULL;

	iface->get_to_uri = gnomechat_vfs_multi_transfer_get_to_uri;
	iface->set_to_uri = NULL;

	iface->start_transfer = gnomechat_vfs_multi_transfer_start_transfer;
	iface->cancel_transfer = gnomechat_vfs_multi_transfer_cancel_transfer;

	iface->get_n_files = gnomechat_vfs_multi_transfer_get_n_files;
	iface->get_file_index = gnomechat_vfs_multi_transfer_get_file_index;
}


/* GObject Callbacks */
static void
gnomechat_vfs_multi_transfer_dispose (GObject * object)
{
	GnomechatVFSMultiTransfer *multi_xfer = GNOMECHAT_VFS_MULTI_TRANSFER (object);

	g_hash_table_destroy (multi_xfer->transfers);

	if (G_OBJECT_CLASS (multi_parent_class)->dispose != NULL)
		(*G_OBJECT_CLASS (multi_parent_class)->dispose) (object);
}


static void
gnomechat_vfs_multi_transfer_get_property (GObject * object,
										   guint param_id,
										   GValue * value,
										   GParamSpec * pspec)
{
	GnomechatVFSMultiTransfer *xfer = GNOMECHAT_VFS_MULTI_TRANSFER (object);

	switch (param_id)
	{
	case UPDATE_ONLY:
		g_value_set_boolean (value, xfer->update_only);
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}


static void
gnomechat_vfs_multi_transfer_set_property (GObject * object,
										   guint param_id,
										   const GValue * value,
										   GParamSpec * pspec)
{
	GnomechatVFSMultiTransfer *xfer = GNOMECHAT_VFS_MULTI_TRANSFER (object);

	switch (param_id)
	{
	case UPDATE_ONLY:
		xfer->update_only = g_value_get_boolean (value);
		g_object_notify (object, "update-only");
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}


/* GType Initialization */
static void
gnomechat_vfs_multi_transfer_class_init (GnomechatVFSMultiTransferClass * class)
{
	GObjectClass *object_class = G_OBJECT_CLASS (class);

	multi_parent_class = g_type_class_peek_parent (class);

	object_class->dispose = gnomechat_vfs_multi_transfer_dispose;
	object_class->set_property = gnomechat_vfs_multi_transfer_set_property;
	object_class->get_property = gnomechat_vfs_multi_transfer_get_property;

	g_object_class_install_property (object_class, UPDATE_ONLY,
									 g_param_spec_boolean ("update-only",
														   "Update Only",
														   "Only update if the from "
														   "file is newer", FALSE,
														   (G_PARAM_READWRITE |
															G_PARAM_CONSTRUCT)));
}


static void
gnomechat_vfs_multi_transfer_instance_init (GnomechatVFSMultiTransfer * multi_xfer)
{
	multi_xfer->transfers = g_hash_table_new_full (gnome_vfs_uri_hash, gnome_vfs_uri_hequal,
												   (GDestroyNotify) gnome_vfs_uri_unref,
												   g_object_unref);
}


GType
gnomechat_vfs_multi_transfer_get_type (void)
{
	static GType type = 0;

	if (!type)

	{
		static const GTypeInfo info = {
			sizeof (GnomechatVFSMultiTransferClass),
			NULL,				/* base_init */
			NULL,				/* base_finalize */
			(GClassInitFunc) gnomechat_vfs_multi_transfer_class_init,
			NULL,				/* class_finalize */
			NULL,				/* class_data */
			sizeof (GnomechatVFSMultiTransfer),
			0,					/* n_preallocs */
			(GInstanceInitFunc) gnomechat_vfs_multi_transfer_instance_init,
		};

		static const GInterfaceInfo xfer_info = {
			(GInterfaceInitFunc) gnomechat_vfs_multi_transfer_transfer_init,
			NULL,
			NULL
		};

		type = g_type_register_static (G_TYPE_OBJECT, "GnomechatVFSMultiTransfer",
									   &info, 0);
		g_type_add_interface_static (type, GNOMECHAT_TYPE_TRANSFER, &xfer_info);
	}

	return type;
}


GnomechatVFSMultiTransfer *
gnomechat_vfs_multi_transfer_new (gboolean update_only)
{
	return g_object_new (GNOMECHAT_TYPE_VFS_MULTI_TRANSFER, "update-only", update_only,
						 NULL);
}


GnomechatVFSTransfer *
gnomechat_vfs_multi_transfer_append (GnomechatVFSMultiTransfer * xfer,
									 GnomeVFSURI * from_uri,
									 GnomeVFSURI * to_uri)
{
	GnomechatVFSTransfer *vfs_xfer;

	g_return_val_if_fail (GNOMECHAT_IS_VFS_MULTI_TRANSFER (xfer), NULL);
	g_return_val_if_fail (xfer->status != GNOMECHAT_TRANSFER_STATUS_CHECKING
						  && xfer->status != GNOMECHAT_TRANSFER_STATUS_TRANSFERRING, NULL);
	g_return_val_if_fail (from_uri != NULL, NULL);
	g_return_val_if_fail (to_uri != NULL, NULL);

	vfs_xfer = gnomechat_vfs_transfer_new (from_uri, to_uri, (xfer->update_only != 0));

	g_hash_table_insert (xfer->transfers, gnome_vfs_uri_ref (from_uri), g_object_ref (vfs_xfer));
	g_hash_table_insert (xfer->transfers, gnome_vfs_uri_ref (to_uri), g_object_ref (vfs_xfer));

	return vfs_xfer;
}
