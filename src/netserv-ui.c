/*
 *  GnomeChat: src/netserv-ui.c
 *
 *  Copyright (c) 2003 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "gnomechat.h"
#include "netserv-ui.h"

#include "connect.h"
#include "esco-userlist-view.h"
#include "pixbufs.h"
#include "prefs.h"
#include "stock-items.h"
#include "utils.h"

#include <gtk/gtkcellrendererpixbuf.h>
#include <gtk/gtkcellrenderertext.h>
#include <gtk/gtkcheckbutton.h>
#include <gtk/gtkentry.h>
#include <gtk/gtkhbox.h>
#include <gtk/gtkhseparator.h>
#include <gtk/gtkimage.h>
#include <gtk/gtklabel.h>
#include <gtk/gtkliststore.h>
#include <gtk/gtkmessagedialog.h>
#include <gtk/gtknotebook.h>
#include <gtk/gtkradiobutton.h>
#include <gtk/gtkscrolledwindow.h>
#include <gtk/gtksizegroup.h>
#include <gtk/gtkspinbutton.h>
#include <gtk/gtkstock.h>
#include <gtk/gtktable.h>
#include <gtk/gtktextview.h>
#include <gtk/gtktreeview.h>
#include <gtk/gtkvbbox.h>
#include <gtk/gtkviewport.h>

#include <libgnomeui/gnome-entry.h>


enum
{
	CHANLIST_CHAN_ON_ITEM,
	CHANLIST_STATUS,
	CHANLIST_NAME,
	CHANLIST_N_COLS
};

enum
{
	MASK_NAME,
	N_MASK_COLS
};


typedef enum
{
	CHANNEL_CHANGE_NONE,
	CHANNEL_CHANGE_TOPIC,
	CHANNEL_CHANGE_KEY,
	CHANNEL_CHANGE_LIMIT,
	CHANNEL_CHANGE_HAS_KEY,
	CHANNEL_CHANGE_HAS_LIMIT,
	CHANNEL_CHANGE_TOPICPROTECT,
	CHANNEL_CHANGE_MODERATED,
	CHANNEL_CHANGE_NOMSG,
	CHANNEL_CHANGE_INVITE_ONLY,
	CHANNEL_CHANGE_QUIET,
	CHANNEL_CHANGE_SECRET,
	CHANNEL_CHANGE_PRIVATE,
	CHANNEL_CHANGE_ANONYMOUS,
	CHANNEL_CHANGE_BAN,
	CHANNEL_CHANGE_BAN_EXCEPTION,
	CHANNEL_CHANGE_INVITE,
	CHANNEL_CHANGE_INVITE_EXCEPTION,
	CHANNEL_CHANGE_LAST
}
ChannelChangeType;

typedef struct
{
	ChannelChangeType type;
	GValue *new_value;
}
ChannelChangeEntry;

typedef struct
{
	GtkWidget *win;

	gint anim_id;
	GtkWidget *recv_box;
	GtkWidget *recv_img;

	GtkWidget *page_box;

	GtkWidget *nick_label;
	GtkWidget *realname_label;
	GtkWidget *username_label;
	GtkWidget *hostname_label;
	GtkWidget *ip_label;
	GtkWidget *server_label;
	GtkWidget *comment_label;
	GtkWidget *chanmode_optmenu;

	GtkTreeSelection *channels_sel;
	GtkWidget *channels_bbox;

	GIrcClient *irc;
	GIrcUser *user;
	gchar *hash_key;
	gchar *nick;
}
WhoisDialog;


typedef struct
{
	GtkWidget *win;
	GtkWidget *text_view;

	gchar *key;
}
MotdDialog;


typedef struct
{
	GtkWidget *win;

	GtkWidget *settings_box;

	gint settings_anim;
	GtkWidget *settings_recv_box;
	GtkWidget *settings_recv_img;

	GtkWidget *topicprotect_check;
	GtkWidget *topic_entry;

	GtkWidget *nomsg_check;
	GtkWidget *moderated_check;

	GtkWidget *limit_check;
	GtkWidget *limit_spinbtn;

	GtkWidget *inviteonly_check;
	GtkWidget *passwd_check;
	GtkWidget *passwd_entry;

	GtkWidget *no_ps_mode_radio;
	GtkWidget *p_mode_radio;
	GtkWidget *s_mode_radio;

	GtkWidget *users_box;

	gint users_anim;
	GtkWidget *users_recv_box;
	GtkWidget *users_recv_img;

	GtkWidget *users_view;
	GtkWidget *users_bbox;
	GtkWidget *users_op_bbox;

	GtkWidget *bans_box;

	gint bans_anim;
	GtkWidget *bans_recv_box;
	GtkWidget *bans_recv_img;

	GtkTreeSelection *bans_sel;
	GtkWidget *bans_bbox;
	GtkWidget *bans_rm_btn;

	GtkWidget *banexcepts_group;
	GtkTreeSelection *banexcepts_sel;
	GtkWidget *banexcepts_bbox;
	GtkWidget *banexcepts_rm_btn;

	GtkWidget *invites_page;
	GtkWidget *invites_box;

	gint invites_anim;
	GtkWidget *invites_recv_box;
	GtkWidget *invites_recv_img;

	GtkTreeSelection *invite_sel;
	GtkWidget *invite_bbox;
	GtkWidget *invite_rm_btn;

	GtkWidget *invexcepts_group;
	GtkTreeSelection *invexcepts_sel;
	GtkWidget *invexcepts_bbox;
	GtkWidget *invexcepts_rm_btn;

	GIrcClient *irc;
	gchar *key;
	gchar *name;
	GIrcChannelUserModeFlags my_modes;
	GIrcChannelInfoType info;
}
ChannelPropsDialog;


typedef struct
{
	GtkWidget *win;
	GtkWidget *entry;
	GtkWidget *ban_check;

	GIrcClient *irc;
	gchar *key;
	gchar *channel;
	gchar *nick;
}
KickDialog;


static GHashTable *whois_dialogs = NULL;
static GHashTable *motd_dialogs = NULL;
static GHashTable *chanprops_dialogs = NULL;
static GHashTable *kick_dialogs = NULL;


static void
whois_irc_destroyed (WhoisDialog * dialog,
					 GObject * old_location)
{
	dialog->irc = NULL;
	gtk_widget_set_sensitive (dialog->channels_bbox, FALSE);
	gtk_dialog_set_response_sensitive (GTK_DIALOG (dialog->win), GTK_RESPONSE_ACCEPT,
									   FALSE);
}


static void
whois_dialog_free (WhoisDialog * dialog)
{
	if (dialog != NULL)
	{
		if (dialog->irc != NULL)
		{
			g_object_weak_unref (G_OBJECT (dialog->irc),
								 (GWeakNotify) whois_irc_destroyed, dialog);
		}

		if (dialog->user != NULL)
			girc_user_unref (dialog->user);

		if (dialog->hash_key != NULL)
			g_free (dialog->hash_key);

		if (dialog->nick != NULL)
			g_free (dialog->nick);

		g_free (dialog);
	}
}


static void
whois_dialog_response_cb (GtkWidget * win,
						  gint response,
						  WhoisDialog * dialog)
{
	g_source_remove (dialog->anim_id);

	if (response == GTK_RESPONSE_ACCEPT
		&& dialog->nick != NULL && dialog->nick[0] != '\0')
	{
		girc_client_open_query (dialog->irc, dialog->nick);
	}

	g_hash_table_remove (whois_dialogs, dialog->hash_key);
	gtk_widget_destroy (win);
}


static void
whois_dialog_sel_changed_cb (GtkTreeSelection * sel,
							 GtkWidget * bbox)
{
	gtk_widget_set_sensitive (bbox, gtk_tree_selection_get_selected (sel, NULL, NULL));
}


static void
whois_dialog_join_btn_clicked_cb (GtkButton * button,
								  GtkTreeSelection * sel)
{
	GtkTreeModel *model;
	GtkTreeIter iter;

	if (gtk_tree_selection_get_selected (sel, &model, &iter))
	{
		GIrcUserChannelOn *chan;
		gchar *channel;

		gtk_tree_model_get (model, &iter, CHANLIST_CHAN_ON_ITEM, &chan, -1);

		channel = chan->name;

		if (channel != NULL)
		{
			GIrcClient *irc = GIRC_CLIENT (g_object_get_data (G_OBJECT (sel), "gnomechat-irc"));

			if (irc != NULL)
			{
				gchar *msg = g_strdup_printf ("JOIN %s", channel);

				girc_client_send_raw (irc, msg);
				g_free (msg);
			}
		}

		girc_user_channel_on_free (chan);
	}
}


static gboolean
update_anim (GtkWidget ** image)
{
	if (!GTK_IS_IMAGE (*image))
		return FALSE;

	pixbuf_anim_update (*image);

	return TRUE;
}


static gboolean
whois_dialog_set_done_loading (WhoisDialog * dialog)
{
	g_source_remove (dialog->anim_id);
	gtk_widget_hide (dialog->recv_box);
	gtk_widget_set_sensitive (dialog->page_box, TRUE);

	return FALSE;
}


static void
whois_dialog_set_is_loading (WhoisDialog * dialog)
{
	gtk_widget_show (dialog->recv_box);

	dialog->anim_id = g_timeout_add (100, (GSourceFunc) update_anim, &(dialog->recv_img));
	gtk_widget_set_sensitive (dialog->page_box, FALSE);
}


static gint
sort_chanlist_status_col (GtkTreeModel * model,
						  GtkTreeIter * iter1,
						  GtkTreeIter * iter2,
						  WhoisDialog * dialog)
{
	GIrcUserChannelOn *chan1,
	 *chan2;
	gint retval;

	gtk_tree_model_get (model, iter1, CHANLIST_CHAN_ON_ITEM, &chan1, -1);
	gtk_tree_model_get (model, iter2, CHANLIST_CHAN_ON_ITEM, &chan2, -1);

	retval = girc_user_channel_on_collate (chan1, chan2, girc_client_get_encoding (dialog->irc));

	girc_user_channel_on_free (chan1);
	girc_user_channel_on_free (chan2);

	return retval;
}


static guint
sort_chanlist_name_col (GtkTreeModel * model,
						GtkTreeIter * iter1,
						GtkTreeIter * iter2,
						WhoisDialog * dialog)
{
	GIrcUserChannelOn *chan1,
	 *chan2;
	const gchar *encoding;
	gchar *name1,
	 *name2;
	gint retval = 0;

	gtk_tree_model_get (model, iter1, CHANLIST_CHAN_ON_ITEM, &chan1, -1);
	gtk_tree_model_get (model, iter2, CHANLIST_CHAN_ON_ITEM, &chan2, -1);

	encoding = girc_client_get_encoding (dialog->irc);

	name1 = girc_convert_to_utf8 (chan1->name, encoding);
	girc_user_channel_on_free (chan1);

	name2 = girc_convert_to_utf8 (chan2->name, encoding);
	girc_user_channel_on_free (chan2);

	if (name1 == NULL && name2 != NULL)
	{
		retval = 1;
	}
	else if (name1 != NULL && name2 == NULL)
	{
		retval = -1;
	}
	else
	{
		retval = g_utf8_collate (name1, name2);
	}

	g_free (name1);
	g_free (name2);

	return retval;
}


static void
render_chanlist_status_col (GtkTreeViewColumn * col,
							GtkCellRenderer * cell,
							GtkTreeModel * model,
							GtkTreeIter * iter,
							WhoisDialog * dialog)
{
	GIrcUserChannelOn *chan;
	const gchar *stock_id;

	gtk_tree_model_get (model, iter, CHANLIST_CHAN_ON_ITEM, &chan, -1);

	if (chan->modes & GIRC_CHANNEL_USER_MODE_CREATOR
		|| chan->modes & GIRC_CHANNEL_USER_MODE_OPS
		|| chan->modes & GIRC_CHANNEL_USER_MODE_HALFOPS)
	{
		if (chan->modes & GIRC_CHANNEL_USER_MODE_VOICE)
			stock_id = GNOMECHAT_STOCK_STATUS_OP_VOICE;
		else
			stock_id = GNOMECHAT_STOCK_STATUS_OP;
	}
	else if (chan->modes & GIRC_CHANNEL_USER_MODE_VOICE)
	{
		stock_id = GNOMECHAT_STOCK_STATUS_VOICE;
	}
	else
	{
		stock_id = GNOMECHAT_STOCK_STATUS_NORMAL;
	}

	g_object_set (cell, "stock-id", stock_id, NULL);

	girc_user_channel_on_free (chan);
}


static void
render_chanlist_name_col (GtkTreeViewColumn * col,
						  GtkCellRenderer * cell,
						  GtkTreeModel * model,
						  GtkTreeIter * iter,
						  WhoisDialog * dialog)
{
	GIrcUserChannelOn *chan;
	const gchar *encoding;
	gchar *name;

	gtk_tree_model_get (model, iter, CHANLIST_CHAN_ON_ITEM, &chan, -1);

	encoding = girc_client_get_encoding (dialog->irc);

	name = girc_convert_to_utf8 (chan->name, encoding);
	girc_user_channel_on_free (chan);

	g_object_set (cell, "text", name, NULL);

	g_free (name);
}


static WhoisDialog *
create_whois_dialog (GIrcClient * irc,
					 GtkWindow * parent,
					 const gchar * hash_key,
					 const gchar * nick)
{
	WhoisDialog *dialog;
	GtkWidget *group_box,
	 *hbox,
	 *table,
	 *tree_view,
	 *label,
	 *scrwin,
	 *separator,
	 *button;
	GtkTreeModel *model;
	GtkTreeViewColumn *col;
	GtkCellRenderer *cell;
	GtkSizeGroup *size_group;

	dialog = g_new0 (WhoisDialog, 1);

	dialog->irc = irc;
	dialog->hash_key = g_strdup (hash_key);
	dialog->nick = g_strdup (nick);

	g_object_weak_ref (G_OBJECT (irc), (GWeakNotify) whois_irc_destroyed, dialog);

	dialog->win = gtk_dialog_new ();
	gtk_dialog_set_has_separator (GTK_DIALOG (dialog->win), FALSE);
	gtk_dialog_add_buttons (GTK_DIALOG (dialog->win),
							GNOMECHAT_STOCK_QUERY, GTK_RESPONSE_ACCEPT,
							GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE, NULL);
	gtk_dialog_set_default_response (GTK_DIALOG (dialog->win), GTK_RESPONSE_CLOSE);
	gtk_window_set_role (GTK_WINDOW (dialog->win), "Whois");
	gtk_window_set_transient_for (GTK_WINDOW (dialog->win), parent);
	util_set_window_icon_from_stock (GTK_WINDOW (dialog->win),
									 GNOMECHAT_STOCK_USER_PROPERTIES);
	g_signal_connect (dialog->win, "response", G_CALLBACK (whois_dialog_response_cb),
					  dialog);

	/* Top Container */
	table = gtk_table_new (3, 3, FALSE);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (dialog->win)->vbox), table);
	gtk_container_set_border_width (GTK_CONTAINER (table), 5);
	gtk_widget_show (table);

	/* Loading box & animation */
	dialog->recv_box = gtk_viewport_new (NULL, NULL);
	gtk_viewport_set_shadow_type (GTK_VIEWPORT (dialog->recv_box), GTK_SHADOW_ETCHED_IN);
	gtk_table_attach (GTK_TABLE (table), dialog->recv_box, 1, 2, 1, 2, 0, 0, 0, 0);
	gtk_widget_show (dialog->recv_box);

	hbox = gtk_hbox_new (FALSE, 12);
	gtk_container_set_border_width (GTK_CONTAINER (hbox), 12);
	gtk_container_add (GTK_CONTAINER (dialog->recv_box), hbox);
	gtk_widget_show (hbox);

	dialog->recv_img = pixbuf_anim_from_file (GNOMECHAT_USERPROPS_ANIM_FILE,
											  GNOMECHAT_USERPROPS_ANIM_N_FRAMES);
	gtk_box_pack_start (GTK_BOX (hbox), dialog->recv_img, FALSE, FALSE, 0);
	gtk_widget_show (dialog->recv_img);
	g_object_add_weak_pointer (G_OBJECT (dialog->recv_img),
							   (gpointer) & (dialog->recv_img));

	label = gtk_label_new (_("<b>Retrieving user information...</b>"));
	gtk_label_set_use_markup (GTK_LABEL (label), TRUE);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);


	/* Actual dialog contents */
	dialog->page_box = gtk_vbox_new (FALSE, 12);
	gtk_table_attach (GTK_TABLE (table), dialog->page_box, 0, 3, 0, 3,
					  (GTK_EXPAND | GTK_FILL), (GTK_EXPAND | GTK_FILL), 0, 0);
	gtk_widget_set_sensitive (dialog->page_box, FALSE);
	gtk_widget_set_size_request (dialog->page_box,
								 util_get_font_width (dialog->page_box) * 60, -1);
	gtk_widget_show (dialog->page_box);

	/* General Group */
	group_box = gtk_vbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (dialog->page_box), group_box, FALSE, FALSE, 0);
	gtk_widget_show (group_box);

	hbox = gtk_hbox_new (FALSE, 3);
	gtk_box_pack_start (GTK_BOX (group_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new (_("<b>General</b>"));
	gtk_label_set_use_markup (GTK_LABEL (label), TRUE);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	separator = gtk_hseparator_new ();
	gtk_container_add (GTK_CONTAINER (hbox), separator);
	gtk_widget_show (separator);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (group_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	table = gtk_table_new (7, 2, FALSE);
	gtk_table_set_col_spacings (GTK_TABLE (table), 3);
	gtk_box_pack_start (GTK_BOX (hbox), table, TRUE, TRUE, 24);
	gtk_widget_show (table);

	size_group = gtk_size_group_new (GTK_SIZE_GROUP_HORIZONTAL);

	label = gtk_label_new (_("Nickname: "));
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 0, 1, GTK_FILL, GTK_FILL, 0, 0);
	gtk_size_group_add_widget (size_group, label);
	gtk_widget_show (label);

	dialog->nick_label = gtk_label_new (NULL);
	gtk_label_set_selectable (GTK_LABEL (dialog->nick_label), TRUE);
	gtk_misc_set_alignment (GTK_MISC (dialog->nick_label), 0.0, 0.5);
	gtk_table_attach (GTK_TABLE (table), dialog->nick_label, 1, 2, 0, 1,
					  (GTK_EXPAND | GTK_FILL), (GTK_EXPAND | GTK_FILL), 0, 0);
	gtk_widget_show (dialog->nick_label);

	label = gtk_label_new (_("Real name: "));
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 1, 2, GTK_FILL, GTK_FILL, 0, 0);
	gtk_size_group_add_widget (size_group, label);
	gtk_widget_show (label);

	dialog->realname_label = gtk_label_new (NULL);
	gtk_label_set_selectable (GTK_LABEL (dialog->realname_label), TRUE);
	gtk_misc_set_alignment (GTK_MISC (dialog->realname_label), 0.0, 0.5);
	gtk_table_attach (GTK_TABLE (table), dialog->realname_label, 1, 2, 1, 2,
					  (GTK_EXPAND | GTK_FILL), (GTK_EXPAND | GTK_FILL), 0, 0);
	gtk_widget_show (dialog->realname_label);

	label = gtk_label_new (_("User name: "));
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 2, 3, GTK_FILL, GTK_FILL, 0, 0);
	gtk_size_group_add_widget (size_group, label);
	gtk_widget_show (label);

	dialog->username_label = gtk_label_new (NULL);
	gtk_label_set_selectable (GTK_LABEL (dialog->username_label), TRUE);
	gtk_misc_set_alignment (GTK_MISC (dialog->username_label), 0.0, 0.5);
	gtk_table_attach (GTK_TABLE (table), dialog->username_label, 1, 2, 2, 3,
					  (GTK_EXPAND | GTK_FILL), (GTK_EXPAND | GTK_FILL), 0, 0);
	gtk_widget_show (dialog->username_label);

	label = gtk_label_new (_("Host name: "));
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 3, 4, GTK_FILL, GTK_FILL, 0, 0);
	gtk_size_group_add_widget (size_group, label);
	gtk_widget_show (label);

	dialog->hostname_label = gtk_label_new (NULL);
	gtk_label_set_selectable (GTK_LABEL (dialog->hostname_label), TRUE);
	gtk_misc_set_alignment (GTK_MISC (dialog->hostname_label), 0.0, 0.5);
	gtk_table_attach (GTK_TABLE (table), dialog->hostname_label, 1, 2, 3, 4,
					  (GTK_EXPAND | GTK_FILL), (GTK_EXPAND | GTK_FILL), 0, 0);
	gtk_widget_show (dialog->hostname_label);

	label = gtk_label_new (_("Host address: "));
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 4, 5, GTK_FILL, GTK_FILL, 0, 0);
	gtk_size_group_add_widget (size_group, label);
	gtk_widget_show (label);

	dialog->ip_label = gtk_label_new (NULL);
	gtk_label_set_selectable (GTK_LABEL (dialog->ip_label), TRUE);
	gtk_misc_set_alignment (GTK_MISC (dialog->ip_label), 0.0, 0.5);
	gtk_table_attach (GTK_TABLE (table), dialog->ip_label, 1, 2, 4, 5,
					  (GTK_EXPAND | GTK_FILL), (GTK_EXPAND | GTK_FILL), 0, 0);
	gtk_widget_show (dialog->ip_label);

	label = gtk_label_new (_("Server: "));
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 5, 6, GTK_FILL, GTK_FILL, 0, 0);
	gtk_size_group_add_widget (size_group, label);
	gtk_widget_show (label);

	dialog->server_label = gtk_label_new (NULL);
	gtk_label_set_selectable (GTK_LABEL (dialog->server_label), TRUE);
	gtk_misc_set_alignment (GTK_MISC (dialog->server_label), 0.0, 0.5);
	gtk_table_attach (GTK_TABLE (table), dialog->server_label, 1, 2, 5, 6,
					  (GTK_EXPAND | GTK_FILL), (GTK_EXPAND | GTK_FILL), 0, 0);
	gtk_widget_show (dialog->server_label);

	label = gtk_label_new (_("Server Comment: "));
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 6, 7, GTK_FILL, GTK_FILL, 0, 0);
	gtk_size_group_add_widget (size_group, label);
	gtk_widget_show (label);

	dialog->comment_label = gtk_label_new (NULL);
	gtk_label_set_selectable (GTK_LABEL (dialog->comment_label), TRUE);
	gtk_misc_set_alignment (GTK_MISC (dialog->comment_label), 0.0, 0.5);
	gtk_table_attach (GTK_TABLE (table), dialog->comment_label, 1, 2, 6, 7,
					  (GTK_EXPAND | GTK_FILL), (GTK_EXPAND | GTK_FILL), 0, 0);
	gtk_widget_show (dialog->comment_label);

	g_object_unref (size_group);

	/* Channels Group */
	group_box = gtk_vbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (dialog->page_box), group_box, TRUE, TRUE, 0);
	gtk_widget_show (group_box);

	hbox = gtk_hbox_new (FALSE, 3);
	gtk_box_pack_start (GTK_BOX (group_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new (_("<b>Channels</b>"));
	gtk_label_set_use_markup (GTK_LABEL (label), TRUE);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	separator = gtk_hseparator_new ();
	gtk_container_add (GTK_CONTAINER (hbox), separator);
	gtk_widget_show (separator);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (group_box), hbox, TRUE, TRUE, 0);
	gtk_widget_show (hbox);

	table = gtk_table_new (1, 2, FALSE);
	gtk_table_set_col_spacings (GTK_TABLE (table), 9);
	gtk_box_pack_start (GTK_BOX (hbox), table, TRUE, TRUE, 24);
	gtk_widget_show (table);

	scrwin = gtk_scrolled_window_new (NULL, NULL);
	gtk_widget_set_size_request (scrwin, -1, util_get_font_height (scrwin) * 8);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrwin), GTK_POLICY_NEVER,
									GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrwin), GTK_SHADOW_IN);
	gtk_table_attach (GTK_TABLE (table), scrwin, 0, 1, 0, 1, (GTK_EXPAND | GTK_FILL),
					  (GTK_EXPAND | GTK_FILL), 0, 0);
	gtk_widget_show (scrwin);

	model = GTK_TREE_MODEL (gtk_list_store_new (CHANLIST_N_COLS,
												GIRC_TYPE_USER_CHANNEL_ON,
												GIRC_TYPE_CHANNEL_USER_MODE_FLAGS,
												G_TYPE_STRING));

	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (model),
										  CHANLIST_STATUS, GTK_SORT_ASCENDING);

	gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (model),
									 CHANLIST_STATUS,
									 (GtkTreeIterCompareFunc) sort_chanlist_status_col,
									 dialog, NULL);
	gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (model),
									 CHANLIST_NAME,
									 (GtkTreeIterCompareFunc) sort_chanlist_name_col,
									 dialog, NULL);

	tree_view = gtk_tree_view_new_with_model (model);
	gtk_container_add (GTK_CONTAINER (scrwin), tree_view);
	gtk_widget_show (tree_view);

	col = gtk_tree_view_column_new ();
	gtk_tree_view_column_set_sort_column_id (col, CHANLIST_STATUS);
	gtk_tree_view_column_set_resizable (col, FALSE);
	gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view), col);

	cell = gtk_cell_renderer_pixbuf_new ();
	gtk_tree_view_column_pack_start (col, cell, FALSE);
	gtk_tree_view_column_set_cell_data_func (col, cell,
											 (GtkTreeCellDataFunc)
											 render_chanlist_status_col, NULL, NULL);

	col = gtk_tree_view_column_new ();
	gtk_tree_view_column_set_title (col, _("Channel Name"));
	gtk_tree_view_column_set_sort_column_id (col, CHANLIST_NAME);
	gtk_tree_view_column_set_resizable (col, FALSE);
	gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view), col);

	cell = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_start (col, cell, FALSE);
	gtk_tree_view_column_set_cell_data_func (col, cell,
											 (GtkTreeCellDataFunc)
											 render_chanlist_name_col, dialog, NULL);

	dialog->channels_bbox = gtk_vbutton_box_new ();

	gtk_button_box_set_layout (GTK_BUTTON_BOX (dialog->channels_bbox),
							   GTK_BUTTONBOX_START);
	gtk_widget_set_sensitive (dialog->channels_bbox, FALSE);
	gtk_table_attach (GTK_TABLE (table), dialog->channels_bbox, 1, 2, 0, 1,
					  GTK_FILL, GTK_FILL, 0, 0);
	gtk_widget_show (dialog->channels_bbox);

	dialog->channels_sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree_view));
	g_object_set_data (G_OBJECT (dialog->channels_sel), "gnomechat-irc", irc);
	g_signal_connect (dialog->channels_sel, "changed",
					  G_CALLBACK (whois_dialog_sel_changed_cb), dialog->channels_bbox);

	button = gtk_button_new_from_stock (GNOMECHAT_STOCK_INFO);
	gtk_box_pack_start (GTK_BOX (dialog->channels_bbox), button, FALSE, FALSE, 0);
	gtk_widget_show (button);

	button = gtk_button_new_from_stock (GNOMECHAT_STOCK_JOIN);
	gtk_box_pack_start (GTK_BOX (dialog->channels_bbox), button, FALSE, FALSE, 0);
	g_signal_connect (button, "clicked", G_CALLBACK (whois_dialog_join_btn_clicked_cb),
					  dialog->channels_sel);
	gtk_widget_show (button);

	return dialog;
}


static void
fill_whois_dialog (WhoisDialog * dialog,
				   GIrcUser * user)
{
	GSList *list;
	gchar *title,
	 *net_name;
	GtkListStore *store =
		GTK_LIST_STORE (gtk_tree_view_get_model (dialog->channels_sel->tree_view));

	net_name = prefs_get_network_name (connect_get_connection_netid (dialog->irc));
	title = g_strdup_printf (_("%s on %s - IRC Chat"), user->nick, net_name);
	g_free (net_name);

	gtk_window_set_title (GTK_WINDOW (dialog->win), title);
	g_free (title);

	gtk_widget_set_sensitive (dialog->page_box, TRUE);

	dialog->user = girc_user_ref (user);

	gtk_label_set_text (GTK_LABEL (dialog->nick_label), user->nick);
	gtk_label_set_text (GTK_LABEL (dialog->realname_label), user->realname);
	gtk_label_set_text (GTK_LABEL (dialog->username_label), user->username);
	gtk_label_set_text (GTK_LABEL (dialog->hostname_label), user->hostname);
	gtk_label_set_text (GTK_LABEL (dialog->ip_label), user->ip_address);
	gtk_label_set_text (GTK_LABEL (dialog->server_label), user->server);

	gtk_list_store_clear (store);

	for (list = girc_g_hash_table_copy_to_slist (user->channels, NULL);
		 list != NULL; list = g_slist_remove_link (list, list))
	{
		if (GIRC_IS_USER_CHANNEL_ON (list->data))
		{
			GtkTreeIter iter;

			gtk_list_store_append (store, &iter);
			gtk_list_store_set (store, &iter, CHANLIST_CHAN_ON_ITEM, list->data, -1);
		}
	}

	g_timeout_add (1000, (GSourceFunc) whois_dialog_set_done_loading, dialog);
}


void
netserv_ui_open_whois_dialog (GIrcClient * irc,
							  GtkWindow * parent,
							  GIrcUser * user,
							  const gchar * nick)
{
	WhoisDialog *dialog;
	gchar *hash_key;

	if (whois_dialogs == NULL)
	{
		whois_dialogs = g_hash_table_new_full (g_str_hash, g_str_equal, g_free,
											   (GDestroyNotify) whois_dialog_free);
	}

	hash_key = g_strdup_printf ("%p:%s", irc, nick);
	dialog = g_hash_table_lookup (whois_dialogs, hash_key);

	if (dialog == NULL)
	{
		dialog = create_whois_dialog (irc, parent, hash_key, nick);
		g_hash_table_insert (whois_dialogs, hash_key, dialog);
	}
	else
	{
		g_free (hash_key);
	}

	gtk_window_present (GTK_WINDOW (dialog->win));
	whois_dialog_set_is_loading (dialog);

	if (user != NULL)
	{
		fill_whois_dialog (dialog, user);
	}
	else
	{
		gchar *str;

		gtk_window_present (GTK_WINDOW (dialog->win));

		str = g_strdup_printf ("WHOIS %s", nick);
		girc_client_send_raw (irc, str);
		g_free (str);
	}
}


static void
motd_dialog_response_cb (GtkWidget * dialog,
						 gint response,
						 const gchar * key)
{
	g_hash_table_remove (motd_dialogs, key);
	gtk_widget_destroy (dialog);
}


static MotdDialog *
create_motd_dialog (GtkWindow * parent,
					const gchar * server)
{
	MotdDialog *dialog = NULL;
	GtkWidget *scrwin;
	PangoFontDescription *font_desc;
	gchar *title,
	 *monospace_font;

	dialog = g_new0 (MotdDialog, 1);
	g_assert (dialog != NULL);

	dialog->key = g_strdup (server);

	dialog->win = gtk_dialog_new ();
	gtk_dialog_set_has_separator (GTK_DIALOG (dialog->win), FALSE);
	gtk_dialog_add_buttons (GTK_DIALOG (dialog->win),
							GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE, NULL);
	gtk_dialog_set_default_response (GTK_DIALOG (dialog->win), GTK_RESPONSE_CLOSE);
	gtk_window_set_role (GTK_WINDOW (dialog->win), "MOTD");
	gtk_window_set_transient_for (GTK_WINDOW (dialog->win), parent);
	util_set_window_icon_from_stock (GTK_WINDOW (dialog->win), GNOMECHAT_STOCK_SERVER);

	title = g_strdup_printf (_("Message Of The Day - %s - IRC Chat"), server);
	gtk_window_set_title (GTK_WINDOW (dialog->win), title);
	g_free (title);

	g_signal_connect (dialog->win, "response", G_CALLBACK (motd_dialog_response_cb),
					  dialog->key);

	scrwin = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrwin), GTK_SHADOW_IN);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrwin),
									GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
	gtk_container_set_border_width (GTK_CONTAINER (scrwin), 5);
	gtk_widget_set_size_request (scrwin, -1, util_get_font_height (scrwin) * 6);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (dialog->win)->vbox), scrwin);
	gtk_widget_show (scrwin);

	dialog->text_view = gtk_text_view_new ();
	gtk_text_view_set_editable (GTK_TEXT_VIEW (dialog->text_view), FALSE);
	gtk_text_view_set_cursor_visible (GTK_TEXT_VIEW (dialog->text_view), FALSE);
	gtk_container_add (GTK_CONTAINER (scrwin), dialog->text_view);
	gtk_widget_show (dialog->text_view);

	monospace_font = prefs_get_monospace_font ();
	font_desc = pango_font_description_from_string (monospace_font);
	gtk_widget_modify_font (dialog->text_view, font_desc);
	pango_font_description_free (font_desc);
	g_free (monospace_font);

	return dialog;
}


static void
fill_motd_dialog (MotdDialog * dialog,
				  const gchar * encoding,
				  const gchar * motd)
{
	GtkTextBuffer *buffer;

	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (dialog->text_view));

	if (motd != NULL)
	{
		gchar *motd_utf8 = girc_convert_to_utf8 (motd, encoding);

		if (motd_utf8 != NULL)
		{
			gtk_text_buffer_set_text (buffer, motd_utf8, -1);
			g_free (motd_utf8);
		}
	}
	else
	{
		gtk_text_buffer_set_text (buffer,
								  _("There is no message of the day for this server"),
								  -1);
	}
}


void
netserv_ui_open_motd_dialog (GIrcClient * irc,
							 GtkWindow * parent,
							 const gchar * server,
							 const gchar * motd)
{
	MotdDialog *dialog = NULL;

	if (motd_dialogs == NULL)
	{
		motd_dialogs = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_free);
	}

	dialog = g_hash_table_lookup (motd_dialogs, server);

	if (dialog == NULL)
	{
		dialog = create_motd_dialog (parent, server);
		g_hash_table_insert (motd_dialogs, dialog->key, dialog);
	}

	gtk_window_present (GTK_WINDOW (dialog->win));

	if (motd != NULL)
	{
		const gchar *encoding;

		encoding = girc_client_get_encoding (irc);
		fill_motd_dialog (dialog, encoding, motd);
	}
	else
	{
		gchar *str;

		str = g_strdup_printf ("MOTD %s", server);
		girc_client_send_raw (irc, str);
		g_free (str);
	}
}

static void
channel_props_dialog_free (gpointer data)
{
	ChannelPropsDialog *dialog = data;

	if (dialog != NULL)
	{
		if (dialog->key)
			g_free (dialog->key);

		if (dialog->name)
			g_free (dialog->name);

		g_free (dialog);
	}
}


static void
channel_props_response_cb (GtkWidget * win,
						   gint response,
						   ChannelPropsDialog * dialog)
{
	switch (response)
	{
	case GTK_RESPONSE_HELP:
		g_warning ("FIXME: Connect channel properties dialog to help system.");
		break;

	case GTK_RESPONSE_OK:
		{
			g_warning ("FIXME: Implement changing channel modes to match dialog");
		}
	default:
		if (dialog->settings_anim > 0)
			g_source_remove (dialog->settings_anim);
		if (dialog->users_anim > 0)
			g_source_remove (dialog->users_anim);
		if (dialog->bans_anim > 0)
			g_source_remove (dialog->bans_anim);
		if (dialog->invites_anim > 0)
			g_source_remove (dialog->invites_anim);

		g_hash_table_remove (chanprops_dialogs, dialog->key);
		gtk_widget_destroy (win);
		break;
	}
}


static void
channel_props_users_sel_changed_cb (EscoUserlistView * view,
									ChannelPropsDialog * dialog)
{
	GSList *users;
	guint len;

	users = esco_userlist_view_get_selected (view);
	len = g_slist_length (users);

	if (len > 1)
	{
		gtk_widget_set_sensitive (dialog->users_op_bbox,
								  (dialog->my_modes >= GIRC_CHANNEL_USER_MODE_HALFOPS &&
								   dialog->my_modes >=
								   GIRC_CHANNEL_USER (users->data)->modes));
	}
	else
	{
		gtk_widget_set_sensitive (dialog->users_op_bbox, FALSE);
	}

	girc_g_slist_deep_free (users, (GFreeFunc) girc_channel_user_unref);
}


static void
channel_props_query_btn_clicked_cb (GtkButton * btn,
									ChannelPropsDialog * dialog)
{
	esco_userlist_view_query_selected (ESCO_USERLIST_VIEW (dialog->users_view));
}


static void
channel_props_sendfile_btn_clicked_cb (GtkButton * btn,
									   ChannelPropsDialog * dialog)
{
	esco_userlist_view_send_file_to_selected (ESCO_USERLIST_VIEW (dialog->users_view));
}


static void
channel_props_user_props_btn_clicked_cb (GtkButton * btn,
										 ChannelPropsDialog * dialog)
{
	esco_userlist_view_info_on_selected (ESCO_USERLIST_VIEW (dialog->users_view));
}


static void
channel_props_kick_btn_clicked_cb (GtkButton * btn,
								   ChannelPropsDialog * dialog)
{
	GSList *users;

	for (users =
		 esco_userlist_view_get_selected (ESCO_USERLIST_VIEW (dialog->users_view));
		 users != NULL; users = g_slist_remove_link (users, users))
	{
		if (users->data != NULL)
		{
			netserv_ui_open_kick_dialog (dialog->irc, GTK_WINDOW (dialog->win),
										 dialog->name,
										 GIRC_CHANNEL_USER (users->data)->user->nick,
										 NULL);
			girc_channel_user_unref (users->data);
		}
	}
}


static void
channel_props_bans_sel_changed_cb (GtkTreeSelection * sel,
								   ChannelPropsDialog * dialog)
{
	gtk_widget_set_sensitive (dialog->bans_rm_btn,
							  (dialog->my_modes >= GIRC_CHANNEL_USER_MODE_HALFOPS &&
							   gtk_tree_selection_get_selected (sel, NULL, NULL)));
}


static void
channel_props_bans_add_clicked_cb (GtkButton * btn,
								   ChannelPropsDialog * dialog)
{
	g_warning ("FIXME: Implement adding bans");
}


static void
channel_props_bans_rm_clicked_cb (GtkButton * btn,
								  ChannelPropsDialog * dialog)
{
	GtkTreeModel *model;
	GtkTreeIter iter;

	if (gtk_tree_selection_get_selected (dialog->bans_sel, &model, &iter))
	{
		gchar *mask;

		gtk_tree_model_get (model, &iter, MASK_NAME, &mask, -1);

		if (mask != NULL)
		{
			gchar *msg = g_strdup_printf ("MODE %s -b %s", dialog->name, mask);

			g_free (mask);

			girc_client_send_raw (dialog->irc, msg);
			g_free (msg);
		}
	}
}


static void
channel_props_banexcepts_sel_changed_cb (GtkTreeSelection * sel,
										 ChannelPropsDialog * dialog)
{
	gtk_widget_set_sensitive (dialog->banexcepts_rm_btn,
							  (dialog->my_modes >= GIRC_CHANNEL_USER_MODE_HALFOPS &&
							   gtk_tree_selection_get_selected (sel, NULL, NULL)));
}


static void
channel_props_banexcepts_add_clicked_cb (GtkButton * btn,
										 ChannelPropsDialog * dialog)
{
	g_warning ("FIXME: Implement adding ban exceptions");
}


static void
channel_props_banexcepts_rm_clicked_cb (GtkButton * btn,
										ChannelPropsDialog * dialog)
{
	GtkTreeModel *model;
	GtkTreeIter iter;

	if (gtk_tree_selection_get_selected (dialog->banexcepts_sel, &model, &iter))
	{
		gchar *mask;

		gtk_tree_model_get (model, &iter, MASK_NAME, &mask, -1);

		if (mask != NULL)
		{
			gchar *msg = g_strdup_printf ("MODE %s -e %s", dialog->name, mask);

			g_free (mask);

			girc_client_send_raw (dialog->irc, msg);
			g_free (msg);
		}
	}
}


static void
channel_props_invite_sel_changed_cb (GtkTreeSelection * sel,
									 ChannelPropsDialog * dialog)
{
	gtk_widget_set_sensitive (dialog->invite_rm_btn,
							  (dialog->my_modes >= GIRC_CHANNEL_USER_MODE_HALFOPS &&
							   gtk_tree_selection_get_selected (sel, NULL, NULL)));
}


static void
channel_props_invite_add_clicked_cb (GtkButton * btn,
									 ChannelPropsDialog * dialog)
{
	g_warning ("FIXME: Implement adding invitations");
}


static void
channel_props_invite_rm_clicked_cb (GtkButton * btn,
									ChannelPropsDialog * dialog)
{
	GtkTreeModel *model;
	GtkTreeIter iter;

	if (gtk_tree_selection_get_selected (dialog->invite_sel, &model, &iter))
	{
		gchar *mask;

		gtk_tree_model_get (model, &iter, MASK_NAME, &mask, -1);

		if (mask != NULL)
		{
			gchar *msg = g_strdup_printf ("MODE %s -I %s", dialog->name, mask);

			g_free (mask);

			girc_client_send_raw (dialog->irc, msg);
			g_free (msg);
		}
	}
}


static void
channel_props_invexcepts_sel_changed_cb (GtkTreeSelection * sel,
										 ChannelPropsDialog * dialog)
{
	gtk_widget_set_sensitive (dialog->invexcepts_rm_btn,
							  (dialog->my_modes >= GIRC_CHANNEL_USER_MODE_HALFOPS &&
							   gtk_tree_selection_get_selected (sel, NULL, NULL)));
}


static void
channel_props_invexcepts_add_clicked_cb (GtkButton * btn,
										 ChannelPropsDialog * dialog)
{
	g_warning ("FIXME: Implement adding invitation exception");
}


static void
channel_props_invexcepts_rm_clicked_cb (GtkButton * btn,
										ChannelPropsDialog * dialog)
{
	GtkTreeModel *model;
	GtkTreeIter iter;

	if (gtk_tree_selection_get_selected (dialog->invexcepts_sel, &model, &iter))
	{
		gchar *mask;

		gtk_tree_model_get (model, &iter, MASK_NAME, &mask, -1);

		if (mask != NULL)
		{
			gchar *msg = g_strdup_printf ("MODE %s -E %s", dialog->name, mask);

			g_free (mask);

			girc_client_send_raw (dialog->irc, msg);
			g_free (msg);
		}
	}
}


static void
set_channel_props_to_loading (ChannelPropsDialog * dialog,
							  GIrcChannelInfoType info)
{
	switch (info)
	{
	case GIRC_CHANNEL_INFO_NONE:
		gtk_widget_show (dialog->settings_recv_box);
		dialog->settings_anim = g_timeout_add (100, (GSourceFunc) update_anim,
											   &(dialog->settings_recv_img));
		gtk_widget_set_sensitive (dialog->settings_box, FALSE);

		gtk_widget_show (dialog->users_recv_box);
		dialog->users_anim = g_timeout_add (100, (GSourceFunc) update_anim,
											&(dialog->users_recv_img));
		gtk_widget_set_sensitive (dialog->users_box, FALSE);

		gtk_widget_show (dialog->bans_recv_box);
		dialog->bans_anim = g_timeout_add (100, (GSourceFunc) update_anim,
										   &(dialog->bans_recv_img));
		gtk_widget_set_sensitive (dialog->bans_box, FALSE);

		if (girc_client_has_channel_mode (dialog->irc, 'I'))
		{
			gtk_widget_show (dialog->invites_recv_box);
			dialog->invites_anim = g_timeout_add (100, (GSourceFunc) update_anim,
												  &(dialog->invites_recv_img));
			gtk_widget_set_sensitive (dialog->invites_box, FALSE);
		}
		break;

	case GIRC_CHANNEL_INFO_MODES:
		gtk_widget_show (dialog->settings_recv_box);
		dialog->settings_anim = g_timeout_add (100, (GSourceFunc) update_anim,
											   &(dialog->settings_recv_img));
		gtk_widget_set_sensitive (dialog->settings_box, FALSE);
		break;

	case GIRC_CHANNEL_INFO_USERS:
		gtk_widget_show (dialog->users_recv_box);
		dialog->users_anim = g_timeout_add (100, (GSourceFunc) update_anim,
											&(dialog->users_recv_img));
		gtk_widget_set_sensitive (dialog->users_box, FALSE);
		break;

	case GIRC_CHANNEL_INFO_BAN:
	case GIRC_CHANNEL_INFO_BAN_EXCEPTION:
		gtk_widget_show (dialog->bans_recv_box);
		dialog->bans_anim = g_timeout_add (100, (GSourceFunc) update_anim,
										   &(dialog->bans_recv_img));
		gtk_widget_set_sensitive (dialog->bans_box, FALSE);
		break;

	case GIRC_CHANNEL_INFO_INVITE:
	case GIRC_CHANNEL_INFO_INVITE_EXCEPTION:
		if (girc_client_has_channel_mode (dialog->irc, 'I'))
		{
			gtk_widget_show (dialog->invites_recv_box);
			dialog->invites_anim = g_timeout_add (100, (GSourceFunc) update_anim,
												  &(dialog->invites_recv_img));
			gtk_widget_set_sensitive (dialog->invites_box, FALSE);
		}
		break;

	default:
		break;
	}
}


static void
set_channel_props_to_done (ChannelPropsDialog * dialog,
						   GIrcChannelInfoType info)
{
	switch (info)
	{
	case GIRC_CHANNEL_INFO_NONE:
		g_source_remove (dialog->settings_anim);
		gtk_widget_hide (dialog->settings_recv_box);
		gtk_widget_set_sensitive (dialog->settings_box, TRUE);

		g_source_remove (dialog->users_anim);
		gtk_widget_hide (dialog->users_recv_box);
		gtk_widget_set_sensitive (dialog->users_box, TRUE);

		g_source_remove (dialog->bans_anim);
		gtk_widget_hide (dialog->bans_recv_box);
		gtk_widget_set_sensitive (dialog->bans_box, TRUE);

		if (girc_client_has_channel_mode (dialog->irc, 'I'))
		{
			g_source_remove (dialog->invites_anim);
			gtk_widget_hide (dialog->invites_recv_box);
			gtk_widget_set_sensitive (dialog->invites_box, TRUE);
		}
		break;

	case GIRC_CHANNEL_INFO_MODES:
		g_source_remove (dialog->settings_anim);
		gtk_widget_hide (dialog->settings_recv_box);
		gtk_widget_set_sensitive (dialog->settings_box, TRUE);
		break;

	case GIRC_CHANNEL_INFO_USERS:
		g_source_remove (dialog->users_anim);
		gtk_widget_hide (dialog->users_recv_box);
		gtk_widget_set_sensitive (dialog->users_box, TRUE);
		break;

	case GIRC_CHANNEL_INFO_BAN:
	case GIRC_CHANNEL_INFO_BAN_EXCEPTION:
		g_source_remove (dialog->bans_anim);
		gtk_widget_hide (dialog->bans_recv_box);
		gtk_widget_set_sensitive (dialog->bans_box, TRUE);
		break;

	case GIRC_CHANNEL_INFO_INVITE:
	case GIRC_CHANNEL_INFO_INVITE_EXCEPTION:
		if (girc_client_has_channel_mode (dialog->irc, 'I'))
		{
			g_source_remove (dialog->invites_anim);
			gtk_widget_hide (dialog->invites_recv_box);
			gtk_widget_set_sensitive (dialog->invites_box, TRUE);
		}
		break;

	default:
		break;
	}
}



static ChannelPropsDialog *
create_channel_props_dialog (GIrcClient * irc,
							 GtkWindow * parent,
							 const gchar * key,
							 const gchar * name)
{
	ChannelPropsDialog *dialog = g_new0 (ChannelPropsDialog, 1);
	GtkWidget *notebook,
	 *table,
	 *label,
	 *separator,
	 *page_box,
	 *group_box,
	 *hbox,
	 *hbox2,
	 *vbox,
	 *vbox2,
	 *scrwin,
	 *tree_view,
	 *button;
	GtkTreeModel *model;
	GtkTreeViewColumn *col;
	GtkCellRenderer *cell;
	GtkSizeGroup *btn_group;
	gchar *net_name,
	 *title;
	guint max = 0;
	gboolean supported = FALSE;

	dialog->irc = irc;
	dialog->key = g_strdup (key);
	dialog->name = g_strdup (name);

	net_name = prefs_get_network_name (connect_get_connection_netid (irc));
	title = g_strdup_printf (_("%s Properties - %s - IRC Chat"), name, net_name);
	g_free (net_name);

	dialog->win = gtk_dialog_new_with_buttons (title, parent, GTK_DIALOG_NO_SEPARATOR,
											   GTK_STOCK_HELP, GTK_RESPONSE_HELP,
											   GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
											   GTK_STOCK_OK, GTK_RESPONSE_OK, NULL);
	g_free (title);

	gtk_dialog_set_default_response (GTK_DIALOG (dialog->win), GTK_RESPONSE_CANCEL);
	gtk_window_set_role (GTK_WINDOW (dialog->win), "ChannelProperties");
	util_set_window_icon_from_stock (GTK_WINDOW (dialog->win),
									 GNOMECHAT_STOCK_CHANNEL_PROPERTIES);
	g_signal_connect (dialog->win, "response", G_CALLBACK (channel_props_response_cb),
					  dialog);

	notebook = gtk_notebook_new ();
	gtk_container_set_border_width (GTK_CONTAINER (notebook), 5);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (dialog->win)->vbox), notebook);
	gtk_widget_show (notebook);

	/* Settings Page */
	label = gtk_label_new (_("Settings"));
	gtk_widget_show (label);

	table = gtk_table_new (3, 3, FALSE);
	gtk_container_set_border_width (GTK_CONTAINER (table), 12);
	gtk_notebook_append_page (GTK_NOTEBOOK (notebook), table, label);
	gtk_widget_show (table);

	/* Loading box & animation */
	dialog->settings_recv_box = gtk_viewport_new (NULL, NULL);
	gtk_viewport_set_shadow_type (GTK_VIEWPORT (dialog->settings_recv_box),
								  GTK_SHADOW_ETCHED_IN);
	gtk_table_attach (GTK_TABLE (table), dialog->settings_recv_box, 1, 2, 1, 2,
					  0, 0, 0, 0);
	gtk_widget_show (dialog->settings_recv_box);

	hbox = gtk_hbox_new (FALSE, 12);
	gtk_container_set_border_width (GTK_CONTAINER (hbox), 12);
	gtk_container_add (GTK_CONTAINER (dialog->settings_recv_box), hbox);
	gtk_widget_show (hbox);

	dialog->settings_recv_img = pixbuf_anim_from_file (GNOMECHAT_CHANPROPS_ANIM_FILE,
													   GNOMECHAT_CHANPROPS_ANIM_N_FRAMES);
	gtk_box_pack_start (GTK_BOX (hbox), dialog->settings_recv_img, FALSE, FALSE, 0);
	g_object_add_weak_pointer (G_OBJECT (dialog->settings_recv_img),
							   (gpointer) & (dialog->settings_recv_img));
	gtk_widget_show (dialog->settings_recv_img);

	label = gtk_label_new (_("<b>Retrieving channel settings...</b>"));
	gtk_label_set_use_markup (GTK_LABEL (label), TRUE);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	/* Actual Page */
	dialog->settings_box = page_box = gtk_vbox_new (FALSE, 12);
	gtk_table_attach (GTK_TABLE (table), page_box, 0, 3, 0, 3,
					  (GTK_EXPAND | GTK_FILL), (GTK_EXPAND | GTK_FILL), 0, 0);
	gtk_widget_show (page_box);

	/* Topic Group */
	group_box = gtk_vbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (page_box), group_box, FALSE, FALSE, 0);
	gtk_widget_show (group_box);

	hbox = gtk_hbox_new (FALSE, 3);
	gtk_box_pack_start (GTK_BOX (group_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new (_("<b>Topic</b>"));
	gtk_label_set_use_markup (GTK_LABEL (label), TRUE);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	separator = gtk_hseparator_new ();
	gtk_container_add (GTK_CONTAINER (hbox), separator);
	gtk_widget_show (separator);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (group_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	vbox = gtk_vbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (hbox), vbox, TRUE, TRUE, 24);
	gtk_widget_show (vbox);

	dialog->topicprotect_check =
		gtk_check_button_new_with_mnemonic (_("Only allow channel operators to change "
											  "the _topic"));
	gtk_box_pack_start (GTK_BOX (vbox), dialog->topicprotect_check, FALSE, FALSE, 0);
	gtk_widget_show (dialog->topicprotect_check);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new_with_mnemonic (_("To_pic: "));
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	g_object_get (irc, "topic-length", &max, NULL);
	dialog->topic_entry = gtk_entry_new ();
	if (max > 0 && max < 65535)
		gtk_entry_set_max_length (GTK_ENTRY (dialog->topic_entry), max);
	util_set_label_widget_pair (label, dialog->topic_entry);
	gtk_container_add (GTK_CONTAINER (hbox), dialog->topic_entry);
	gtk_widget_show (dialog->topic_entry);

	/* Messages Group */
	group_box = gtk_vbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (page_box), group_box, FALSE, FALSE, 0);
	gtk_widget_show (group_box);

	hbox = gtk_hbox_new (FALSE, 3);
	gtk_box_pack_start (GTK_BOX (group_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new (_("<b>Messages</b>"));
	gtk_label_set_use_markup (GTK_LABEL (label), TRUE);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	separator = gtk_hseparator_new ();
	gtk_container_add (GTK_CONTAINER (hbox), separator);
	gtk_widget_show (separator);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (group_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	vbox = gtk_vbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (hbox), vbox, TRUE, TRUE, 24);
	gtk_widget_show (vbox);

	dialog->nomsg_check =
		gtk_check_button_new_with_mnemonic (_("Only allow those in this channel to talk "
											  "o_n it"));
	gtk_box_pack_start (GTK_BOX (vbox), dialog->nomsg_check, FALSE, FALSE, 0);
	gtk_widget_show (dialog->nomsg_check);

	dialog->moderated_check =
		gtk_check_button_new_with_mnemonic (_("Only allow operators and users with "
											  "per_mission to talk"));
	gtk_box_pack_start (GTK_BOX (vbox), dialog->moderated_check, FALSE, FALSE, 0);
	gtk_widget_show (dialog->moderated_check);

	/* Joining Group */
	group_box = gtk_vbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (page_box), group_box, FALSE, FALSE, 0);
	gtk_widget_show (group_box);

	hbox = gtk_hbox_new (FALSE, 3);
	gtk_box_pack_start (GTK_BOX (group_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new (_("<b>Users</b>"));
	gtk_label_set_use_markup (GTK_LABEL (label), TRUE);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	separator = gtk_hseparator_new ();
	gtk_container_add (GTK_CONTAINER (hbox), separator);
	gtk_widget_show (separator);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (group_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	vbox = gtk_vbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (hbox), vbox, TRUE, TRUE, 24);
	gtk_widget_show (vbox);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	dialog->limit_check = gtk_check_button_new_with_mnemonic (_("A_llow a maximum of "));
	gtk_box_pack_start (GTK_BOX (hbox), dialog->limit_check, FALSE, FALSE, 0);
	gtk_widget_show (dialog->limit_check);

	dialog->limit_spinbtn =
		gtk_spin_button_new (GTK_ADJUSTMENT (gtk_adjustment_new (1.0, 0.0, G_MAXFLOAT,
																 1.0, 10.0, 10.0)),
							 1.0, 0);
	gtk_box_pack_start (GTK_BOX (hbox), dialog->limit_spinbtn, FALSE, FALSE, 0);
	gtk_widget_show (dialog->limit_spinbtn);

	label = gtk_label_new (_(" users to join"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	dialog->inviteonly_check =
		gtk_check_button_new_with_mnemonic (_("Only allow _invited users to join"));
	gtk_box_pack_start (GTK_BOX (vbox), dialog->inviteonly_check, FALSE, FALSE, 0);
	gtk_widget_show (dialog->inviteonly_check);

	vbox2 = gtk_vbox_new (FALSE, 3);
	gtk_box_pack_start (GTK_BOX (vbox), vbox2, FALSE, FALSE, 0);
	gtk_widget_show (vbox2);

	dialog->passwd_check =
		gtk_check_button_new_with_mnemonic (_("Require a password to _join:"));
	gtk_box_pack_start (GTK_BOX (vbox2), dialog->passwd_check, FALSE, FALSE, 0);
	gtk_widget_show (dialog->passwd_check);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (vbox2), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	dialog->passwd_entry = gtk_entry_new ();
	gtk_box_pack_start (GTK_BOX (hbox), dialog->passwd_entry, TRUE, TRUE, 24);
	gtk_widget_show (dialog->passwd_entry);

	/* Privacy Group */
	group_box = gtk_vbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (page_box), group_box, FALSE, FALSE, 0);
	gtk_widget_show (group_box);

	hbox = gtk_hbox_new (FALSE, 3);
	gtk_box_pack_start (GTK_BOX (group_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new (_("<b>Privacy</b>"));
	gtk_label_set_use_markup (GTK_LABEL (label), TRUE);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	separator = gtk_hseparator_new ();
	gtk_container_add (GTK_CONTAINER (hbox), separator);
	gtk_widget_show (separator);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (group_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	vbox = gtk_vbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (hbox), vbox, TRUE, TRUE, 24);
	gtk_widget_show (vbox);

	dialog->no_ps_mode_radio =
		gtk_radio_button_new_with_mnemonic (NULL, _("List this c_hannel and its users"));
	gtk_box_pack_start (GTK_BOX (vbox), dialog->no_ps_mode_radio, FALSE, FALSE, 0);
	gtk_widget_show (dialog->no_ps_mode_radio);

	dialog->p_mode_radio =
		gtk_radio_button_new_with_mnemonic_from_widget (GTK_RADIO_BUTTON
														(dialog->no_ps_mode_radio),
														_("_Hide users on this channel "
														  "from being listed"));
	gtk_box_pack_start (GTK_BOX (vbox), dialog->p_mode_radio, FALSE, FALSE, 0);
	gtk_widget_show (dialog->p_mode_radio);

	dialog->s_mode_radio =
		gtk_radio_button_new_with_mnemonic_from_widget (GTK_RADIO_BUTTON
														(dialog->no_ps_mode_radio),
														_("Hide this channel _and its "
														  "users from being listed"));
	gtk_box_pack_start (GTK_BOX (vbox), dialog->s_mode_radio, FALSE, FALSE, 0);
	gtk_widget_show (dialog->s_mode_radio);


	/* Users Page */
	label = gtk_label_new (_("Users"));
	gtk_widget_show (label);

	table = gtk_table_new (3, 3, FALSE);
	gtk_container_set_border_width (GTK_CONTAINER (table), 12);
	gtk_notebook_append_page (GTK_NOTEBOOK (notebook), table, label);
	gtk_widget_show (table);

	/* Loading box & animation */
	dialog->users_recv_box = gtk_viewport_new (NULL, NULL);
	gtk_viewport_set_shadow_type (GTK_VIEWPORT (dialog->users_recv_box),
								  GTK_SHADOW_ETCHED_IN);
	gtk_table_attach (GTK_TABLE (table), dialog->users_recv_box, 1, 2, 1, 2, 0, 0, 0, 0);
	gtk_widget_show (dialog->users_recv_box);

	hbox = gtk_hbox_new (FALSE, 12);
	gtk_container_set_border_width (GTK_CONTAINER (hbox), 12);
	gtk_container_add (GTK_CONTAINER (dialog->users_recv_box), hbox);
	gtk_widget_show (hbox);

	dialog->users_recv_img = pixbuf_anim_from_file (GNOMECHAT_CHANPROPS_ANIM_FILE,
													GNOMECHAT_CHANPROPS_ANIM_N_FRAMES);
	gtk_box_pack_start (GTK_BOX (hbox), dialog->users_recv_img, FALSE, FALSE, 0);
	g_object_add_weak_pointer (G_OBJECT (dialog->users_recv_img),
							   (gpointer) & (dialog->users_recv_img));
	gtk_widget_show (dialog->users_recv_img);

	label = gtk_label_new (_("<b>Retrieving user list...</b>"));
	gtk_label_set_use_markup (GTK_LABEL (label), TRUE);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	/* The actual page */
	dialog->users_box = page_box = gtk_hbox_new (FALSE, 6);
	gtk_table_attach (GTK_TABLE (table), page_box, 0, 3, 0, 3,
					  (GTK_EXPAND | GTK_FILL), (GTK_EXPAND | GTK_FILL), 0, 0);
	gtk_widget_show (page_box);

	scrwin = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrwin), GTK_POLICY_NEVER,
									GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrwin), GTK_SHADOW_IN);
	gtk_container_add (GTK_CONTAINER (page_box), scrwin);
	gtk_widget_show (scrwin);

	dialog->users_view = esco_userlist_view_new (irc);
	g_signal_connect (dialog->users_view, "selection-changed",
					  G_CALLBACK (channel_props_users_sel_changed_cb), dialog);
	gtk_container_add (GTK_CONTAINER (scrwin), dialog->users_view);
	gtk_widget_show (dialog->users_view);

	vbox = gtk_vbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (page_box), vbox, FALSE, TRUE, 0);
	gtk_widget_show (vbox);

	btn_group = gtk_size_group_new (GTK_SIZE_GROUP_HORIZONTAL);

	dialog->users_bbox = gtk_vbutton_box_new ();
	gtk_box_pack_start (GTK_BOX (vbox), dialog->users_bbox, FALSE, FALSE, 0);
	gtk_widget_show (dialog->users_bbox);

	button = gtk_button_new_from_stock (GNOMECHAT_STOCK_QUERY);
	gtk_box_pack_start (GTK_BOX (dialog->users_bbox), button, FALSE, FALSE, 0);
	g_signal_connect (button, "clicked", G_CALLBACK (channel_props_query_btn_clicked_cb),
					  dialog);
	gtk_size_group_add_widget (btn_group, button);
	gtk_widget_show (button);

	button = gtk_button_new_from_stock (GNOMECHAT_STOCK_SENDFILE);
	g_signal_connect (button, "clicked",
					  G_CALLBACK (channel_props_sendfile_btn_clicked_cb), dialog);
	gtk_box_pack_start (GTK_BOX (dialog->users_bbox), button, FALSE, FALSE, 0);
	gtk_size_group_add_widget (btn_group, button);
	gtk_widget_show (button);

	button = gtk_button_new_from_stock (GTK_STOCK_PROPERTIES);
	gtk_box_pack_start (GTK_BOX (dialog->users_bbox), button, FALSE, FALSE, 0);
	g_signal_connect (button, "clicked",
					  G_CALLBACK (channel_props_user_props_btn_clicked_cb), dialog);
	gtk_size_group_add_widget (btn_group, button);
	gtk_widget_show (button);

	dialog->users_op_bbox = gtk_vbutton_box_new ();
	gtk_box_pack_start (GTK_BOX (vbox), dialog->users_op_bbox, FALSE, FALSE, 0);
	gtk_widget_show (dialog->users_op_bbox);

	button = gtk_button_new_from_stock (GNOMECHAT_STOCK_KICK);
	g_signal_connect (button, "clicked", G_CALLBACK (channel_props_kick_btn_clicked_cb), dialog);
	gtk_box_pack_start (GTK_BOX (dialog->users_op_bbox), button, FALSE, FALSE, 0);
	gtk_size_group_add_widget (btn_group, button);
	gtk_widget_show (button);

	g_object_unref (btn_group);


	/* Bans Page */
	label = gtk_label_new (_("Bans"));
	gtk_widget_show (label);

	table = gtk_table_new (3, 3, FALSE);
	gtk_container_set_border_width (GTK_CONTAINER (table), 12);
	gtk_notebook_append_page (GTK_NOTEBOOK (notebook), table, label);
	gtk_widget_show (table);

	/* Loading box & animation */
	dialog->bans_recv_box = gtk_viewport_new (NULL, NULL);
	gtk_viewport_set_shadow_type (GTK_VIEWPORT (dialog->bans_recv_box),
								  GTK_SHADOW_ETCHED_IN);
	gtk_table_attach (GTK_TABLE (table), dialog->bans_recv_box, 1, 2, 1, 2, 0, 0, 0, 0);
	gtk_widget_show (dialog->bans_recv_box);

	hbox = gtk_hbox_new (FALSE, 12);
	gtk_container_set_border_width (GTK_CONTAINER (hbox), 12);
	gtk_container_add (GTK_CONTAINER (dialog->bans_recv_box), hbox);
	gtk_widget_show (hbox);

	dialog->bans_recv_img = pixbuf_anim_from_file (GNOMECHAT_CHANPROPS_ANIM_FILE,
												   GNOMECHAT_CHANPROPS_ANIM_N_FRAMES);
	gtk_box_pack_start (GTK_BOX (hbox), dialog->bans_recv_img, FALSE, FALSE, 0);
	g_object_add_weak_pointer (G_OBJECT (dialog->bans_recv_img),
							   (gpointer) & (dialog->bans_recv_img));
	gtk_widget_show (dialog->bans_recv_img);

	label = gtk_label_new (_("<b>Retrieving ban information...</b>"));
	gtk_label_set_use_markup (GTK_LABEL (label), TRUE);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	/* Actual Page */
	dialog->bans_box = page_box = gtk_vbox_new (FALSE, 12);
	gtk_table_attach (GTK_TABLE (table), page_box, 0, 3, 0, 3,
					  (GTK_EXPAND | GTK_FILL), (GTK_EXPAND | GTK_FILL), 0, 0);
	gtk_widget_show (page_box);

	/* Ban Group */
	group_box = gtk_vbox_new (FALSE, 6);
	gtk_container_add (GTK_CONTAINER (page_box), group_box);
	gtk_widget_show (group_box);

	hbox = gtk_hbox_new (FALSE, 3);
	gtk_box_pack_start (GTK_BOX (group_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new (_("<b>Banned Users</b>"));
	gtk_label_set_use_markup (GTK_LABEL (label), TRUE);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	separator = gtk_hseparator_new ();
	gtk_container_add (GTK_CONTAINER (hbox), separator);
	gtk_widget_show (separator);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_container_add (GTK_CONTAINER (group_box), hbox);
	gtk_widget_show (hbox);

	hbox2 = gtk_hbox_new (FALSE, 3);
	gtk_box_pack_start (GTK_BOX (hbox), hbox2, TRUE, TRUE, 24);
	gtk_widget_show (hbox2);

	scrwin = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrwin), GTK_POLICY_NEVER,
									GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrwin), GTK_SHADOW_IN);
	gtk_container_add (GTK_CONTAINER (hbox2), scrwin);
	gtk_widget_show (scrwin);

	model = GTK_TREE_MODEL (gtk_list_store_new (N_MASK_COLS, G_TYPE_STRING));

	tree_view = gtk_tree_view_new_with_model (model);
	gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (tree_view), FALSE);
	gtk_container_add (GTK_CONTAINER (scrwin), tree_view);
	gtk_widget_show (tree_view);

	dialog->bans_sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree_view));

	col = gtk_tree_view_column_new ();
	gtk_tree_view_column_set_sort_column_id (col, MASK_NAME);
	gtk_tree_view_column_set_resizable (col, FALSE);
	gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view), col);

	cell = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_start (col, cell, FALSE);
	gtk_tree_view_column_add_attribute (col, cell, "text", MASK_NAME);

	dialog->bans_bbox = gtk_vbutton_box_new ();
	gtk_button_box_set_layout (GTK_BUTTON_BOX (dialog->bans_bbox), GTK_BUTTONBOX_START);
	gtk_box_pack_start (GTK_BOX (hbox2), dialog->bans_bbox, FALSE, FALSE, 0);
	gtk_widget_show (dialog->bans_bbox);

	button = gtk_button_new_from_stock (GTK_STOCK_ADD);
	g_signal_connect (button, "clicked", G_CALLBACK (channel_props_bans_add_clicked_cb),
					  dialog);
	gtk_box_pack_start (GTK_BOX (dialog->bans_bbox), button, FALSE, FALSE, 0);
	gtk_widget_show (button);

	dialog->bans_rm_btn = button = gtk_button_new_from_stock (GTK_STOCK_REMOVE);
	g_signal_connect (button, "clicked", G_CALLBACK (channel_props_bans_rm_clicked_cb),
					  dialog);
	gtk_box_pack_start (GTK_BOX (dialog->bans_bbox), button, FALSE, FALSE, 0);
	gtk_widget_show (button);

	g_signal_connect (dialog->bans_sel, "changed",
					  G_CALLBACK (channel_props_bans_sel_changed_cb), dialog);

	/* Ban Exceptions Group */
	g_object_get (irc, "ban-exceptions", &supported, NULL);

	dialog->banexcepts_group = group_box = gtk_vbox_new (FALSE, 6);
	gtk_container_add (GTK_CONTAINER (page_box), group_box);
	if (supported)
		gtk_widget_show (group_box);

	hbox = gtk_hbox_new (FALSE, 3);
	gtk_box_pack_start (GTK_BOX (group_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new (_("<b>Ban Exceptions</b>"));
	gtk_label_set_use_markup (GTK_LABEL (label), TRUE);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	separator = gtk_hseparator_new ();
	gtk_container_add (GTK_CONTAINER (hbox), separator);
	gtk_widget_show (separator);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_container_add (GTK_CONTAINER (group_box), hbox);
	gtk_widget_show (hbox);

	hbox2 = gtk_hbox_new (FALSE, 3);
	gtk_box_pack_start (GTK_BOX (hbox), hbox2, TRUE, TRUE, 24);
	gtk_widget_show (hbox2);

	scrwin = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrwin), GTK_POLICY_NEVER,
									GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrwin), GTK_SHADOW_IN);
	gtk_container_add (GTK_CONTAINER (hbox2), scrwin);
	gtk_widget_show (scrwin);

	model = GTK_TREE_MODEL (gtk_list_store_new (N_MASK_COLS, G_TYPE_STRING));

	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (model), MASK_NAME,
										  GTK_SORT_ASCENDING);

	tree_view = gtk_tree_view_new_with_model (model);
	gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (tree_view), FALSE);
	gtk_container_add (GTK_CONTAINER (scrwin), tree_view);
	gtk_widget_show (tree_view);

	dialog->banexcepts_sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree_view));

	col = gtk_tree_view_column_new ();
	gtk_tree_view_column_set_sort_column_id (col, MASK_NAME);
	gtk_tree_view_column_set_resizable (col, FALSE);
	gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view), col);

	cell = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_start (col, cell, FALSE);
	gtk_tree_view_column_add_attribute (col, cell, "text", MASK_NAME);

	dialog->banexcepts_bbox = gtk_vbutton_box_new ();
	gtk_button_box_set_layout (GTK_BUTTON_BOX (dialog->banexcepts_bbox),
							   GTK_BUTTONBOX_START);
	gtk_box_pack_start (GTK_BOX (hbox2), dialog->banexcepts_bbox, FALSE, FALSE, 0);
	gtk_widget_show (dialog->banexcepts_bbox);

	button = gtk_button_new_from_stock (GTK_STOCK_ADD);
	g_signal_connect (button, "clicked",
					  G_CALLBACK (channel_props_banexcepts_add_clicked_cb), dialog);
	gtk_box_pack_start (GTK_BOX (dialog->banexcepts_bbox), button, FALSE, FALSE, 0);
	gtk_widget_show (button);

	dialog->banexcepts_rm_btn = button = gtk_button_new_from_stock (GTK_STOCK_REMOVE);
	g_signal_connect (button, "clicked",
					  G_CALLBACK (channel_props_banexcepts_rm_clicked_cb), dialog);
	gtk_box_pack_start (GTK_BOX (dialog->banexcepts_bbox), button, FALSE, FALSE, 0);
	gtk_widget_show (button);

	g_signal_connect (dialog->banexcepts_sel, "changed",
					  G_CALLBACK (channel_props_banexcepts_sel_changed_cb), dialog);


	/* Invites Page */
	label = gtk_label_new (_("Invitations"));
	gtk_widget_show (label);

	dialog->invites_page = table = gtk_table_new (3, 3, FALSE);
	gtk_container_set_border_width (GTK_CONTAINER (table), 12);
	gtk_notebook_append_page (GTK_NOTEBOOK (notebook), table, label);

	if (girc_client_has_channel_mode (irc, GIRC_CHANNEL_MODE_CHAR_INVITE))
	{
		gtk_widget_show (table);
	}

	/* Loading box & animation */
	dialog->invites_recv_box = gtk_viewport_new (NULL, NULL);
	gtk_viewport_set_shadow_type (GTK_VIEWPORT (dialog->invites_recv_box),
								  GTK_SHADOW_ETCHED_IN);
	gtk_table_attach (GTK_TABLE (table), dialog->invites_recv_box, 1, 2, 1, 2,
					  0, 0, 0, 0);
	gtk_widget_show (dialog->invites_recv_box);

	hbox = gtk_hbox_new (FALSE, 12);
	gtk_container_set_border_width (GTK_CONTAINER (hbox), 12);
	gtk_container_add (GTK_CONTAINER (dialog->invites_recv_box), hbox);
	gtk_widget_show (hbox);

	dialog->invites_recv_img = pixbuf_anim_from_file (GNOMECHAT_CHANPROPS_ANIM_FILE,
													  GNOMECHAT_CHANPROPS_ANIM_N_FRAMES);
	gtk_box_pack_start (GTK_BOX (hbox), dialog->invites_recv_img, FALSE, FALSE, 0);
	g_object_add_weak_pointer (G_OBJECT (dialog->invites_recv_img),
							   (gpointer) & (dialog->invites_recv_img));
	gtk_widget_show (dialog->invites_recv_img);

	label = gtk_label_new (_("<b>Retrieving invitation information...</b>"));
	gtk_label_set_use_markup (GTK_LABEL (label), TRUE);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	/* Actual Contents */
	dialog->invites_box = page_box = gtk_vbox_new (FALSE, 12);
	gtk_table_attach (GTK_TABLE (table), page_box, 0, 3, 0, 3,
					  (GTK_EXPAND | GTK_FILL), (GTK_EXPAND | GTK_FILL), 0, 0);
	gtk_widget_show (page_box);

	/* Invite Group */
	group_box = gtk_vbox_new (FALSE, 6);
	gtk_container_add (GTK_CONTAINER (page_box), group_box);
	gtk_widget_show (group_box);

	hbox = gtk_hbox_new (FALSE, 3);
	gtk_box_pack_start (GTK_BOX (group_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new (_("<b>Invited Users</b>"));
	gtk_label_set_use_markup (GTK_LABEL (label), TRUE);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	separator = gtk_hseparator_new ();
	gtk_container_add (GTK_CONTAINER (hbox), separator);
	gtk_widget_show (separator);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_container_add (GTK_CONTAINER (group_box), hbox);
	gtk_widget_show (hbox);

	hbox2 = gtk_hbox_new (FALSE, 3);
	gtk_box_pack_start (GTK_BOX (hbox), hbox2, TRUE, TRUE, 24);
	gtk_widget_show (hbox2);

	scrwin = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrwin), GTK_POLICY_NEVER,
									GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrwin), GTK_SHADOW_IN);
	gtk_container_add (GTK_CONTAINER (hbox2), scrwin);
	gtk_widget_show (scrwin);

	model = GTK_TREE_MODEL (gtk_list_store_new (N_MASK_COLS, G_TYPE_STRING));

	tree_view = gtk_tree_view_new_with_model (model);
	gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (tree_view), FALSE);
	gtk_container_add (GTK_CONTAINER (scrwin), tree_view);
	gtk_widget_show (tree_view);

	dialog->invite_sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree_view));

	col = gtk_tree_view_column_new ();
	gtk_tree_view_column_set_sort_column_id (col, MASK_NAME);
	gtk_tree_view_column_set_resizable (col, FALSE);
	gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view), col);

	cell = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_start (col, cell, FALSE);
	gtk_tree_view_column_add_attribute (col, cell, "text", MASK_NAME);

	dialog->invite_bbox = gtk_vbutton_box_new ();
	gtk_button_box_set_layout (GTK_BUTTON_BOX (dialog->invite_bbox), GTK_BUTTONBOX_START);
	gtk_box_pack_start (GTK_BOX (hbox2), dialog->invite_bbox, FALSE, FALSE, 0);
	gtk_widget_show (dialog->invite_bbox);

	button = gtk_button_new_from_stock (GTK_STOCK_ADD);
	g_signal_connect (button, "clicked", G_CALLBACK (channel_props_invite_add_clicked_cb),
					  dialog);
	gtk_box_pack_start (GTK_BOX (dialog->invite_bbox), button, FALSE, FALSE, 0);
	gtk_widget_show (button);

	dialog->invite_rm_btn = button = gtk_button_new_from_stock (GTK_STOCK_REMOVE);
	g_signal_connect (button, "clicked", G_CALLBACK (channel_props_invite_rm_clicked_cb),
					  dialog);
	gtk_box_pack_start (GTK_BOX (dialog->invite_bbox), button, FALSE, FALSE, 0);
	gtk_widget_show (button);

	g_signal_connect (dialog->invite_sel, "changed",
					  G_CALLBACK (channel_props_invite_sel_changed_cb), dialog);

	/* Invite Exceptions Group */
	g_object_get (irc, "invite-exceptions", &supported, NULL);

	dialog->invexcepts_group = group_box = gtk_vbox_new (FALSE, 6);
	gtk_container_add (GTK_CONTAINER (page_box), group_box);
	if (supported)
		gtk_widget_show (group_box);

	hbox = gtk_hbox_new (FALSE, 3);
	gtk_box_pack_start (GTK_BOX (group_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new (_("<b>Invitation Exceptions</b>"));
	gtk_label_set_use_markup (GTK_LABEL (label), TRUE);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	separator = gtk_hseparator_new ();
	gtk_container_add (GTK_CONTAINER (hbox), separator);
	gtk_widget_show (separator);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_container_add (GTK_CONTAINER (group_box), hbox);
	gtk_widget_show (hbox);

	hbox2 = gtk_hbox_new (FALSE, 3);
	gtk_box_pack_start (GTK_BOX (hbox), hbox2, TRUE, TRUE, 24);
	gtk_widget_show (hbox2);

	scrwin = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrwin), GTK_POLICY_NEVER,
									GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrwin), GTK_SHADOW_IN);
	gtk_container_add (GTK_CONTAINER (hbox2), scrwin);
	gtk_widget_show (scrwin);

	model = GTK_TREE_MODEL (gtk_list_store_new (N_MASK_COLS, G_TYPE_STRING));

	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (model), MASK_NAME,
										  GTK_SORT_ASCENDING);

	tree_view = gtk_tree_view_new_with_model (model);
	gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (tree_view), FALSE);
	gtk_container_add (GTK_CONTAINER (scrwin), tree_view);
	gtk_widget_show (tree_view);

	dialog->invexcepts_sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree_view));

	col = gtk_tree_view_column_new ();
	gtk_tree_view_column_set_sort_column_id (col, MASK_NAME);
	gtk_tree_view_column_set_resizable (col, FALSE);
	gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view), col);

	cell = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_start (col, cell, FALSE);
	gtk_tree_view_column_add_attribute (col, cell, "text", MASK_NAME);

	dialog->invexcepts_bbox = gtk_vbutton_box_new ();
	gtk_button_box_set_layout (GTK_BUTTON_BOX (dialog->invexcepts_bbox),
							   GTK_BUTTONBOX_START);
	gtk_box_pack_start (GTK_BOX (hbox2), dialog->invexcepts_bbox, FALSE, FALSE, 0);
	gtk_widget_show (dialog->invexcepts_bbox);

	button = gtk_button_new_from_stock (GTK_STOCK_ADD);
	g_signal_connect (button, "clicked",
					  G_CALLBACK (channel_props_invexcepts_add_clicked_cb), dialog);
	gtk_box_pack_start (GTK_BOX (dialog->invexcepts_bbox), button, FALSE, FALSE, 0);
	gtk_widget_show (button);

	dialog->invexcepts_rm_btn = button = gtk_button_new_from_stock (GTK_STOCK_REMOVE);
	g_signal_connect (button, "clicked",
					  G_CALLBACK (channel_props_invexcepts_rm_clicked_cb), dialog);
	gtk_box_pack_start (GTK_BOX (dialog->invexcepts_bbox), button, FALSE, FALSE, 0);
	gtk_widget_show (button);

	g_signal_connect (dialog->invexcepts_sel, "changed",
					  G_CALLBACK (channel_props_invexcepts_sel_changed_cb), dialog);

	return dialog;
}


void
netserv_ui_open_channel_props_dialog (GIrcClient * irc,
									  GtkWindow * parent,
									  GIrcChannel * channel,
									  const gchar * name)
{
	ChannelPropsDialog *dialog;
	gchar *key;

	g_return_if_fail (GIRC_IS_CLIENT (irc));
	g_return_if_fail (name != NULL && name[0] != '\0');

	if (chanprops_dialogs == NULL)
	{
		chanprops_dialogs = g_hash_table_new_full (girc_ascii_strcasehash,
												   girc_ascii_strcaseequal,
												   NULL, channel_props_dialog_free);
	}

	key = g_strdup_printf ("%s:%s", connect_get_connection_netid (irc), name);

	dialog = g_hash_table_lookup (chanprops_dialogs, key);

	if (dialog == NULL)
	{
		dialog = create_channel_props_dialog (irc, parent, key, name);
		g_hash_table_insert (chanprops_dialogs, dialog->key, dialog);
	}

	g_free (key);

	gtk_window_present (GTK_WINDOW (dialog->win));
	set_channel_props_to_loading (dialog, GIRC_CHANNEL_INFO_NONE);

	netserv_ui_update_channel_props_dialog (irc, channel, name, GIRC_CHANNEL_INFO_NONE);
}


static void
fill_channel_props_dialog (ChannelPropsDialog * dialog,
						   GIrcChannel * channel,
						   GIrcChannelInfoType info)
{
	GtkListStore *store;
	GtkTreeIter iter;
	GSList *list;

	switch (info)
	{
		/* Channel modes */
	case GIRC_CHANNEL_INFO_MODES:
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->topicprotect_check),
									  (channel->modes & GIRC_CHANNEL_MODE_TOPIC_PROTECT));

		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->nomsg_check),
									  (channel->modes & GIRC_CHANNEL_MODE_NOMSG));

		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->moderated_check),
									  (channel->modes & GIRC_CHANNEL_MODE_MODERATED));

		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->inviteonly_check),
									  (channel->modes & GIRC_CHANNEL_MODE_INVITE_ONLY));

		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->limit_check),
									  (channel->modes & GIRC_CHANNEL_MODE_LIMIT));
		gtk_widget_set_sensitive (dialog->limit_spinbtn,
								  (channel->modes & GIRC_CHANNEL_MODE_LIMIT));

		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->passwd_check),
									  (channel->modes & GIRC_CHANNEL_MODE_KEY));

		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->p_mode_radio),
									  (channel->modes & GIRC_CHANNEL_MODE_PRIVATE));

		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->s_mode_radio),
									  (channel->modes & GIRC_CHANNEL_MODE_SECRET));
		break;

	case GIRC_CHANNEL_INFO_TOPIC:
		if (channel->topic != NULL)
		{
			const gchar *encoding;
			gchar *topic;

			encoding = girc_client_get_encoding (dialog->irc);
			topic = girc_convert_to_utf8 (channel->topic, encoding);

			gtk_entry_set_text (GTK_ENTRY (dialog->topic_entry), topic);
			g_free (topic);
		}
		else
		{
			gtk_entry_set_text (GTK_ENTRY (dialog->topic_entry), "");
		}
		break;

	case GIRC_CHANNEL_INFO_KEY:
		if (channel->key != NULL)
		{
			const gchar *encoding;
			gchar *key;

			encoding = girc_client_get_encoding (dialog->irc);
			key = girc_convert_to_utf8 (channel->key, encoding);

			gtk_entry_set_text (GTK_ENTRY (dialog->passwd_entry), key);
			g_free (key);
		}
		else
		{
			gtk_entry_set_text (GTK_ENTRY (dialog->topic_entry), "");
		}
		break;

	case GIRC_CHANNEL_INFO_LIMIT:
		gtk_spin_button_set_value (GTK_SPIN_BUTTON (dialog->limit_spinbtn),
								   (gfloat) channel->limit);
		break;

		/* My or someone else's user */
	case GIRC_CHANNEL_INFO_USERS:
		/* Settings page */
		gtk_widget_set_sensitive (dialog->topicprotect_check,
								  dialog->my_modes >= GIRC_CHANNEL_USER_MODE_HALFOPS);

		gtk_widget_set_sensitive (dialog->nomsg_check,
								  dialog->my_modes >= GIRC_CHANNEL_USER_MODE_HALFOPS);

		gtk_widget_set_sensitive (dialog->moderated_check,
								  dialog->my_modes >= GIRC_CHANNEL_USER_MODE_HALFOPS);

		gtk_widget_set_sensitive (dialog->inviteonly_check,
								  dialog->my_modes >= GIRC_CHANNEL_USER_MODE_HALFOPS);

		gtk_widget_set_sensitive (dialog->limit_check,
								  dialog->my_modes >= GIRC_CHANNEL_USER_MODE_HALFOPS);

		gtk_widget_set_sensitive (dialog->passwd_check,
								  dialog->my_modes >= GIRC_CHANNEL_USER_MODE_HALFOPS);

		gtk_widget_set_sensitive (dialog->passwd_entry,
								  (channel->modes & GIRC_CHANNEL_MODE_KEY));

		gtk_widget_set_sensitive (dialog->no_ps_mode_radio,
								  dialog->my_modes >= GIRC_CHANNEL_USER_MODE_HALFOPS);

		gtk_widget_set_sensitive (dialog->p_mode_radio,
								  dialog->my_modes >= GIRC_CHANNEL_USER_MODE_HALFOPS);

		gtk_widget_set_sensitive (dialog->s_mode_radio,
								  dialog->my_modes >= GIRC_CHANNEL_USER_MODE_HALFOPS);

		gtk_editable_set_editable (GTK_EDITABLE (dialog->topic_entry),
								   (channel->modes & GIRC_CHANNEL_MODE_TOPIC_PROTECT ?
									(dialog->my_modes >= GIRC_CHANNEL_USER_MODE_HALFOPS) :
									(channel->my_user != NULL)));

		gtk_editable_set_editable (GTK_EDITABLE (dialog->passwd_entry),
								   (channel->my_user != NULL &&
									dialog->my_modes >= GIRC_CHANNEL_USER_MODE_HALFOPS));

		/* Users page */
		esco_userlist_view_clear (ESCO_USERLIST_VIEW (dialog->users_view));

		for (list = girc_channel_get_users (channel); list != NULL;
			 list = g_slist_remove_link (list, list))
		{
			if (list->data != NULL)
			{
				esco_userlist_view_add_user (ESCO_USERLIST_VIEW (dialog->users_view),
											 GIRC_CHANNEL_USER (list->data));
				girc_channel_user_unref (list->data);
			}
		}

		/* Bans, & Invites pages */
		gtk_widget_set_sensitive (dialog->bans_bbox,
								  (dialog->my_modes & GIRC_CHANNEL_USER_MODE_CREATOR ||
								   dialog->my_modes & GIRC_CHANNEL_USER_MODE_OPS ||
								   dialog->my_modes & GIRC_CHANNEL_USER_MODE_HALFOPS));

		gtk_widget_set_sensitive (dialog->banexcepts_bbox,
								  (dialog->my_modes & GIRC_CHANNEL_USER_MODE_CREATOR ||
								   dialog->my_modes & GIRC_CHANNEL_USER_MODE_OPS ||
								   dialog->my_modes & GIRC_CHANNEL_USER_MODE_HALFOPS));

		gtk_widget_set_sensitive (dialog->invite_bbox,
								  (dialog->my_modes & GIRC_CHANNEL_USER_MODE_CREATOR ||
								   dialog->my_modes & GIRC_CHANNEL_USER_MODE_OPS ||
								   dialog->my_modes & GIRC_CHANNEL_USER_MODE_HALFOPS));

		gtk_widget_set_sensitive (dialog->invexcepts_bbox,
								  (dialog->my_modes & GIRC_CHANNEL_USER_MODE_CREATOR ||
								   dialog->my_modes & GIRC_CHANNEL_USER_MODE_OPS ||
								   dialog->my_modes & GIRC_CHANNEL_USER_MODE_HALFOPS));
		break;

		/* Bans */
	case GIRC_CHANNEL_INFO_BAN:
		store = GTK_LIST_STORE (gtk_tree_view_get_model (dialog->bans_sel->tree_view));
		gtk_list_store_clear (store);

		for (list = channel->bans; list != NULL; list = list->next)
		{
			GIrcChannelMask *mask;

			if (list->data != NULL)
			{
				mask = GIRC_CHANNEL_MASK (list->data);

				gtk_list_store_append (store, &iter);
				gtk_list_store_set (store, &iter, MASK_NAME, mask->mask, -1);
			}
		}
		break;

		/* Ban Exceptions */
	case GIRC_CHANNEL_INFO_BAN_EXCEPTION:
		store =
			GTK_LIST_STORE (gtk_tree_view_get_model (dialog->banexcepts_sel->tree_view));
		gtk_list_store_clear (store);

		for (list = channel->ban_excepts; list != NULL; list = list->next)
		{
			GIrcChannelMask *mask;

			if (list->data != NULL)
			{
				mask = GIRC_CHANNEL_MASK (list->data);

				gtk_list_store_append (store, &iter);
				gtk_list_store_set (store, &iter, MASK_NAME, mask->mask, -1);
			}
		}
		break;

		/* Invites */
	case GIRC_CHANNEL_INFO_INVITE:
		store = GTK_LIST_STORE (gtk_tree_view_get_model (dialog->invite_sel->tree_view));
		gtk_list_store_clear (store);

		for (list = channel->invites; list != NULL; list = list->next)
		{
			GIrcChannelMask *mask;

			if (list->data != NULL)
			{
				mask = GIRC_CHANNEL_MASK (list->data);

				gtk_list_store_append (store, &iter);
				gtk_list_store_set (store, &iter, MASK_NAME, mask->mask, -1);
			}
		}
		break;

		/* Invite Exceptions */
	case GIRC_CHANNEL_INFO_INVITE_EXCEPTION:
		store =
			GTK_LIST_STORE (gtk_tree_view_get_model (dialog->invexcepts_sel->tree_view));
		gtk_list_store_clear (store);

		for (list = channel->invite_excepts; list != NULL; list = list->next)
		{
			GIrcChannelMask *mask;

			if (list->data != NULL)
			{
				mask = GIRC_CHANNEL_MASK (list->data);

				gtk_list_store_append (store, &iter);
				gtk_list_store_set (store, &iter, MASK_NAME, mask->mask, -1);
			}
		}
		break;

	case GIRC_CHANNEL_INFO_NONE:
	case GIRC_CHANNEL_INFO_LAST:
		g_assert_not_reached ();
		break;
	}

	set_channel_props_to_done (dialog, info);
}


void
netserv_ui_update_channel_props_dialog (GIrcClient * irc,
										GIrcChannel * channel,
										const gchar * name,
										GIrcChannelInfoType info)
{
	ChannelPropsDialog *dialog;
	gchar *key;

	g_return_if_fail (GIRC_IS_CLIENT (irc));
	g_return_if_fail (name != NULL && name[0] != '\0');
	g_return_if_fail (info >= GIRC_CHANNEL_INFO_NONE);

	if (chanprops_dialogs == NULL)
	{
		chanprops_dialogs = g_hash_table_new_full (girc_ascii_strcasehash,
												   girc_ascii_strcaseequal,
												   NULL, channel_props_dialog_free);
	}

	key = g_strdup_printf ("%s:%s", connect_get_connection_netid (irc), name);
	dialog = g_hash_table_lookup (chanprops_dialogs, key);
	g_free (key);

	if (dialog == NULL)
		return;

	gtk_window_present (GTK_WINDOW (dialog->win));

	if (channel == NULL)
	{
		g_warning ("FIXME: Implement retrieving channel info.");
		return;
	}

	if (channel->my_user != NULL)
	{
		dialog->my_modes = channel->my_user->modes;
	}
	else
	{
		dialog->my_modes = GIRC_CHANNEL_USER_MODE_NONE;
	}

	if (info == GIRC_CHANNEL_INFO_NONE)
	{
		for (info = GIRC_CHANNEL_INFO_NONE + 1; info < GIRC_CHANNEL_INFO_LAST; info++)
		{
			fill_channel_props_dialog (dialog, channel, info);
		}
	}
	else
	{
		fill_channel_props_dialog (dialog, channel, info);
	}
}


static void
kick_dialog_free (KickDialog * dialog)
{
	if (dialog != NULL)
	{
		g_free (dialog->key);
		g_free (dialog->channel);
		g_free (dialog->nick);

		g_free (dialog);
	}
}


static void
kick_dialog_response_cb (GtkWidget * win,
						 gint response,
						 KickDialog * dialog)
{
	switch (response)
	{
	case GTK_RESPONSE_HELP:
		g_message ("FIXME: Connect Kick dialog to help system.");
		break;
	case GTK_RESPONSE_OK:
		{
			gchar *reason = gtk_editable_get_chars (GTK_EDITABLE (dialog->entry), 0, -1),
			 *msg;

			if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->ban_check)))
			{
				if (reason != NULL)
				{
					msg = g_strdup_printf ("MODE %s +b %s\r\nKICK %s %s :%s",
										   dialog->channel, dialog->nick,
										   dialog->channel, dialog->nick, reason);
					g_free (reason);
				}
				else
				{
					msg = g_strdup_printf ("MODE %s +b %s\r\nKICK %s %s",
										   dialog->channel, dialog->nick,
										   dialog->channel, dialog->nick);
				}
			}
			else
			{
				if (reason != NULL)
				{
					msg = g_strdup_printf ("KICK %s %s :%s",
										   dialog->channel, dialog->nick, reason);
					g_free (reason);
				}
				else
				{
					msg = g_strdup_printf ("KICK %s %s", dialog->channel, dialog->nick);
				}
			}


			girc_client_send_raw (dialog->irc, msg);
			g_free (msg);
		}
	default:
		g_hash_table_remove (kick_dialogs, dialog->key);
		gtk_widget_destroy (win);
		break;
	}
}


static KickDialog *
create_kick_dialog (GIrcClient * irc,
					const gchar * key,
					GtkWindow * parent,
					const gchar * channel,
					const gchar * nick,
					const gchar * reason)
{
	KickDialog *dialog;
	GtkEntry *entry;
	GtkWidget *hbox,
	 *vbox,
	 *label;
	gchar *title;
	guint max_len;

	dialog = g_new0 (KickDialog, 1);
	dialog->irc = irc;
	dialog->key = g_strdup (key);
	dialog->channel = g_strdup (channel);
	dialog->nick = g_strdup (nick);

	title = g_strdup_printf (_("Kick %s from %s - IRC Chat"), nick, channel);

	dialog->win = gtk_message_dialog_new (parent, 0, GTK_MESSAGE_QUESTION, GTK_BUTTONS_NONE,
										  _("<b>Kick %s from %s?</b>\n"
											"Kicking a user from a channel will forcibly "
											"remove them from the channel."), nick, channel);
	gtk_dialog_add_buttons (GTK_DIALOG (dialog->win), GTK_STOCK_HELP, GTK_RESPONSE_HELP,
							GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
							GNOMECHAT_STOCK_KICK, GTK_RESPONSE_OK, NULL);
	gtk_dialog_set_default_response (GTK_DIALOG (dialog->win), GTK_RESPONSE_OK);
	gtk_window_set_role (GTK_WINDOW (dialog->win), "KickDialog");
	gtk_window_set_title (GTK_WINDOW (dialog->win), title);
	g_free (title);
	util_set_window_icon_from_stock (GTK_WINDOW (dialog->win), GNOMECHAT_STOCK_KICK);
	g_signal_connect (dialog->win, "response", G_CALLBACK (kick_dialog_response_cb),
					  dialog);

	gtk_label_set_use_markup (GTK_LABEL (GTK_MESSAGE_DIALOG (dialog->win)->label), TRUE);
	gtk_image_set_from_stock (GTK_IMAGE (GTK_MESSAGE_DIALOG (dialog->win)->image),
							  GNOMECHAT_STOCK_KICK, GTK_ICON_SIZE_DIALOG);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog->win)->vbox), hbox, FALSE, FALSE, 5);
	gtk_widget_show (hbox);

	vbox = gtk_vbox_new (FALSE, 3);
	gtk_box_pack_start (GTK_BOX (hbox), vbox, TRUE, TRUE, 12);
	gtk_widget_show (vbox);

	/* Ban Checkbutton */
	dialog->ban_check = gtk_check_button_new_with_mnemonic (_("_Ban this user and prevent them "
															  "from coming back."));
	gtk_box_pack_start (GTK_BOX (vbox), dialog->ban_check, FALSE, FALSE, 0);
	gtk_widget_show (dialog->ban_check);

	/* Reason Box */
	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new (_("Reason to give: "));
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	dialog->entry = gnome_entry_new ("kick-reason");
	gtk_box_pack_start (GTK_BOX (hbox), dialog->entry, TRUE, TRUE, 0);
	gtk_widget_show (dialog->entry);

	entry = GTK_ENTRY (gnome_entry_gtk_entry (GNOME_ENTRY (dialog->entry)));
	gtk_entry_set_activates_default (entry, TRUE);
	g_object_get (irc, "kick-message-length", &max_len, NULL);
	gtk_entry_set_max_length (entry, (guint16) max_len);
	gtk_entry_set_text (entry, reason);

	/* Ban Checkbutton */
	dialog->ban_check = gtk_check_button_new_with_mnemonic (_("_Ban this user and prevent them "
															  "from coming back."));
	gtk_box_pack_start (GTK_BOX (vbox), dialog->ban_check, FALSE, FALSE, 0);
	gtk_widget_show (dialog->ban_check);

	/* Reason Box */
	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new (_("Reason to give: "));
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	dialog->entry = gnome_entry_new ("kick-reason");
	gtk_box_pack_start (GTK_BOX (hbox), dialog->entry, TRUE, TRUE, 0);
	gtk_widget_show (dialog->entry);

	entry = GTK_ENTRY (gnome_entry_gtk_entry (GNOME_ENTRY (dialog->entry)));
	gtk_entry_set_activates_default (entry, TRUE);
	g_object_get (irc, "kick-message-length", &max_len, NULL);
	gtk_entry_set_max_length (entry, (guint16) max_len);
	gtk_entry_set_text (entry, reason);

	return dialog;
}


void
netserv_ui_open_kick_dialog (GIrcClient * irc,
							 GtkWindow * parent,
							 const gchar * channel,
							 const gchar * nick,
							 const gchar * reason)
{
	KickDialog *dialog;
	gchar *key;

	g_return_if_fail (channel != NULL && channel[0] != '\0');
	g_return_if_fail (nick != NULL && nick[0] != '\0');

	if (kick_dialogs == NULL)
	{
		kick_dialogs = g_hash_table_new_full (girc_ascii_strcasehash,
											  girc_ascii_strcaseequal,
											  NULL, (GDestroyNotify) kick_dialog_free);
	}

	key = g_strdup_printf ("%s:%s:%s", connect_get_connection_netid (irc), channel, nick);

	dialog = g_hash_table_lookup (kick_dialogs, key);

	if (dialog == NULL)
	{
		dialog = create_kick_dialog (irc, key, parent, channel, nick, reason);
		g_hash_table_insert (kick_dialogs, dialog->key, dialog);
	}

	g_free (key);

	gtk_window_present (GTK_WINDOW (dialog->win));
}
