/*
 *  GnomeChat: src/esco-userlist-view.h
 *
 *  Copyright (c) 2002 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __ESCO_USERLIST_VIEW_H__
#define __ESCO_USERLIST_VIEW_H__


#include <gtk/gtktreeview.h>
#include <libgircclient/girc-client.h>


#define ESCO_TYPE_USERLIST_VIEW				(esco_userlist_view_get_type ())
#define ESCO_USERLIST_VIEW(obj)				(G_TYPE_CHECK_INSTANCE_CAST ((obj), ESCO_TYPE_USERLIST_VIEW, EscoUserlistView))
#define ESCO_USERLIST_VIEW_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST ((klass), ESCO_TYPE_USERLIST_VIEW, EscoUserlistViewClass))
#define ESCO_IS_USERLIST_VIEW(obj)			(G_TYPE_CHECK_INSTANCE_TYPE ((obj), ESCO_TYPE_USERLIST_VIEW))
#define ESCO_IS_USERLIST_VIEW_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), ESCO_TYPE_USERLIST_VIEW))
#define ESCO_USERLIST_VIEW_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS ((obj), ESCO_TYPE_USERLIST_VIEW, EscoUserlistViewClass))


typedef struct _EscoUserlistView EscoUserlistView;
typedef struct _EscoUserlistViewClass EscoUserlistViewClass;

typedef void (*EscoUserlistViewSignalFunc) (EscoUserlistView *view);
typedef void (*EscoUserlistViewActionFunc) (EscoUserlistView * view,
											GIrcChannelUser * user);


struct _EscoUserlistView
{
	GtkTreeView parent;

	GIrcClient *irc;
	GtkWidget *menu;
	gchar *encoding;
	gboolean any_selected;
};

struct _EscoUserlistViewClass
{
	GtkTreeViewClass parent_class;

	/* Signals */
	EscoUserlistViewSignalFunc selection_changed;
	EscoUserlistViewActionFunc query;
	EscoUserlistViewActionFunc info;
	EscoUserlistViewActionFunc send_file;
};


GType esco_userlist_view_get_type (void);

GtkWidget *esco_userlist_view_new (GIrcClient * irc);

void esco_userlist_view_set_irc (EscoUserlistView * view,
								 GIrcClient * irc);
void esco_userlist_view_set_encoding (EscoUserlistView * view,
									  const gchar * encoding);

void esco_userlist_view_query_selected (EscoUserlistView * view);
void esco_userlist_view_info_on_selected (EscoUserlistView * view);
void esco_userlist_view_send_file_to_selected (EscoUserlistView * view);

GSList *esco_userlist_view_get_selected (EscoUserlistView * view);

void esco_userlist_view_add_user (EscoUserlistView * view,
								  GIrcChannelUser * user);
void esco_userlist_view_remove_user (EscoUserlistView * view,
									 GIrcChannelUser * user);

void esco_userlist_view_update_user (EscoUserlistView * view,
									 GIrcChannelUser * user);

void esco_userlist_view_clear (EscoUserlistView * view);


#endif /* __ESCO_USERLIST_VIEW_H__ */
