/*
 *  GnomeChat: src/main.c
 *
 *  Copyright (c) 2002 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "gnomechat.h"

#include "connect-ui.h"
#include "pixbufs.h"
#include "prefs.h"
#include "stock-items.h"
#include "tray-icon.h"
#include "welcome.h"
#include "windows.h"

#include "gnomechat-rdf-model.h"

#include <gtk/gtkmain.h>

#include <libgnome/gnome-program.h>
#include <libgnomeui/gnome-ui-init.h>
#include <libgnomeui/gnome-client.h>

#include <sys/stat.h>
#include <sys/types.h>


const gchar *const appname = PACKAGE;
const gchar *const wmclass = "GnomeChat";
const gchar *const genericname = N_("IRC Chat");


typedef struct _AppInfo
{
	GnomeProgram *program;
	GnomeClient *client;

	GList *chats_list;
	GNode *ui_tree;
}
AppInfo;


static AppInfo *appinfo = NULL;


static inline void
first_run_init (void)
{
	welcome_open_window ();
}


static inline void
standard_init (void)
{
	connect_ui_open_dialog (GTK_WINDOW (gtk_widget_get_toplevel (windows_open_new_window ())));
}


static void
client_connect_cb (GnomeClient * client,
				   gboolean restarted,
				   gpointer data)
{
}


static gboolean
client_save_yourself_cb (GnomeClient * client,
						 gint cmd,
						 GnomeSaveStyle save_style,
						 gboolean shutdown,
						 GnomeInteractStyle interact_style,
						 gboolean fast_shutdown,
						 gpointer data)
{

	return TRUE;
}

int
main (int argc,
	  char **argv)
{
	appinfo = g_new0 (AppInfo, 1);

	if (!g_thread_supported ())
		g_thread_init (NULL);

	appinfo->program =
		gnome_program_init (PACKAGE, VERSION, LIBGNOMEUI_MODULE, argc, argv,
							GNOME_PROGRAM_STANDARD_PROPERTIES,
							GNOME_CLIENT_PARAM_SM_CONNECT, TRUE,
							GNOME_PARAM_ENABLE_SOUND, TRUE,
							GNOME_PARAM_HUMAN_READABLE_NAME, GENERIC_NAME, NULL);
	appinfo->client = gnome_master_client ();
	gnome_client_set_restart_style (appinfo->client, GNOME_RESTART_IF_RUNNING);
	g_signal_connect (appinfo->client, "connect", G_CALLBACK (client_connect_cb), NULL);
	g_signal_connect (appinfo->client, "save-yourself", G_CALLBACK (client_save_yourself_cb), NULL);

	stock_init ();
	prefs_init ();

	if (!prefs_get_first_run_completed ())
		first_run_init ();
	else
		standard_init ();

	if (prefs_get_show_tray_icon ())
		tray_icon_init ();

	gtk_main ();

	return (0);
}


gchar *
gnomechat_get_pixmap_path (const gchar * filename)
{
	return gnome_program_locate_file (appinfo->program, GNOME_FILE_DOMAIN_APP_PIXMAP, filename,
									  TRUE, NULL);
}


G_CONST_RETURN gchar *
gnomechat_get_private_dir (void)
{
	static const gchar *dir = NULL;

	if (dir == NULL)
	{
		dir = g_build_filename (g_get_home_dir (), ".gnome2", "GnomeChat", NULL);

		if (!g_file_test (dir, (G_FILE_TEST_EXISTS | G_FILE_TEST_IS_DIR)))
		{
			mkdir (dir, 0700);
		}
	}

	return dir;
}


G_CONST_RETURN gchar *
gnomechat_get_cache_dir (void)
{
	static const gchar *dir = NULL;

	if (dir == NULL)
	{
		dir = g_build_filename (gnomechat_get_private_dir (), "Cache", NULL);

		if (!g_file_test (dir, (G_FILE_TEST_EXISTS | G_FILE_TEST_IS_DIR)))
		{
			mkdir (dir, 0700);
		}
	}

	return dir;
}


void
gnomechat_quit ()
{
	prefs_shutdown ();
	gtk_main_quit ();
}
