/*
 *  GnomeChat: src/esco-encoding-picker.h
 *
 *  Copyright (c) 2002 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef __ESCO_ENCODING_PICKER_H__
#define __ESCO_ENCODING_PICKER_H__


#include "esco-enums.h"

#include <gtk/gtkoptionmenu.h>


#define ESCO_TYPE_ENCODING_PICKER				(esco_encoding_picker_get_type ())
#define ESCO_ENCODING_PICKER(obj)				(G_TYPE_CHECK_INSTANCE_CAST ((obj), ESCO_TYPE_ENCODING_PICKER, EscoEncodingPicker))
#define ESCO_ENCODING_PICKER_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST ((klass), ESCO_TYPE_ENCODING_PICKER, EscoEncodingPickerClass))
#define ESCO_IS_ENCODING_PICKER(obj)			(G_TYPE_CHECK_INSTANCE_TYPE ((obj), ESCO_TYPE_ENCODING_PICKER))
#define ESCO_IS_ENCODING_PICKER_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), ESCO_TYPE_ENCODING_PICKER))
#define ESCO_ENCODING_PICKER_GET_CLASS(obj)		(G_TYPE_INSTANCE_GET_CLASS ((obj), ESCO_TYPE_ENCODING_PICKER, EscoEncodingPickerClass))


typedef struct _EscoEncodingPicker EscoEncodingPicker;
typedef struct _EscoEncodingPickerPrivate EscoEncodingPickerPrivate;
typedef struct _EscoEncodingPickerClass EscoEncodingPickerClass;


typedef void (*EscoEncodingPickerSignalFunc) (EscoEncodingPicker * epicker);


struct _EscoEncodingPicker
{
	GtkOptionMenu parent;

	EscoEncodingPickerPrivate *_priv;
};

struct _EscoEncodingPickerClass
{
	GtkOptionMenuClass parent_class;

	EscoEncodingPickerSignalFunc dialog_opened;
	EscoEncodingPickerSignalFunc help_clicked;
	EscoEncodingPickerSignalFunc encoding_changed;
};


GType esco_encoding_picker_get_type (void);

GtkWidget *esco_encoding_picker_new (const gchar * title);

G_CONST_RETURN gchar *esco_encoding_picker_get_encoding (EscoEncodingPicker * epicker);
void esco_encoding_picker_set_encoding (EscoEncodingPicker * epicker,
										const gchar * encoding);

G_CONST_RETURN gchar *esco_encoding_picker_get_title (EscoEncodingPicker * epicker);
void esco_encoding_picker_set_title (EscoEncodingPicker * epicker,
									 const gchar * title);

gboolean esco_encoding_picker_get_modal (EscoEncodingPicker * epicker);
void esco_encoding_picker_set_modal (EscoEncodingPicker * epicker,
									 gboolean modal);

GtkWidget *esco_encoding_picker_get_dialog (EscoEncodingPicker * epicker);

#endif /* __ESCO_ENCODING_PICKER_H__ */
