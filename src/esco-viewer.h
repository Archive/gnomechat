/*
 *  GnomeChat: src/esco-viewer.h
 *
 *  Copyright (c) 2002 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef __ESCO_VIEWER_H__
#define __ESCO_VIEWER_H__


#include <esco-enums.h>
#include <viewer-styles.h>

#include <glib.h>
#include <glib-object.h>
#include <gtk/gtktextview.h>
#include <libgircclient/girc-client.h>


#define ESCO_TYPE_VIEWER			(esco_viewer_get_type ())
#define ESCO_VIEWER(obj)			(G_TYPE_CHECK_INSTANCE_CAST ((obj), ESCO_TYPE_VIEWER, EscoViewer))
#define ESCO_VIEWER_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass), ESCO_TYPE_VIEWER, EscoViewerClass))
#define ESCO_IS_VIEWER(obj)			(G_TYPE_CHECK_INSTANCE_TYPE ((obj), ESCO_TYPE_VIEWER))
#define ESCO_IS_VIEWER_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), ESCO_TYPE_VIEWER))
#define ESCO_VIEWER_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS ((obj), ESCO_TYPE_VIEWER, EscoViewerClass))


typedef enum
{
	ESCO_VIEWER_MESSAGE_MY_MSG,
	ESCO_VIEWER_MESSAGE_MY_ACTION,
	ESCO_VIEWER_MESSAGE_MY_NOTICE,
	ESCO_VIEWER_MESSAGE_NICK_CHANGED,

	ESCO_VIEWER_MESSAGE_MSG,
	ESCO_VIEWER_MESSAGE_ACTION,
	ESCO_VIEWER_MESSAGE_NOTICE,

	ESCO_VIEWER_MESSAGE_USER_CHANGED,
	ESCO_VIEWER_MESSAGE_CHANNEL_CHANGED,
	ESCO_VIEWER_MESSAGE_CHANNEL_MODES_CHANGED,
	ESCO_VIEWER_MESSAGE_CHANNEL_USER_CHANGED,
	ESCO_VIEWER_MESSAGE_CHANNEL_USER_MODES_CHANGED,

	ESCO_VIEWER_MESSAGE_SERVER,
	ESCO_VIEWER_MESSAGE_ERROR,

	ESCO_VIEWER_MESSAGE_STATUS,
	ESCO_VIEWER_MESSAGE_CARDINAL,

	ESCO_VIEWER_MESSAGE_UNKNOWN
}
EscoViewerMessageType;


typedef struct _EscoViewer EscoViewer;
typedef struct _EscoViewerClass EscoViewerClass;


typedef void (*EscoViewerPesterFunction) (EscoViewer * view,
										  EscoPesterType pester);
typedef void (*EscoViewerLinkClickedFunction) (EscoViewer * view,
											   const gchar * uri);


struct _EscoViewer
{
	GtkTextView parent;

	/* Properties */
	EscoChatModeType mode;
	gchar *nick;
	gchar *encoding;

	/* Stuff we use */
	gchar *plain_nick;
	GtkTextMark *ins_mark;

	GSList *buffer_items;

	EscoPesterType pester_level;
	PangoTabArray *tabs;
	gint nick_pixel_size;
	gint time_pixel_size;
};

struct _EscoViewerClass
{
	GtkTextViewClass parent_class;

	EscoViewerPesterFunction pester;
	EscoViewerLinkClickedFunction link_clicked;
};

GType esco_viewer_get_type (void);

GtkWidget *esco_viewer_new (EscoChatModeType mode,
							const gchar * nick);

/* Properties */
void esco_viewer_set_mode (EscoViewer * view,
						   EscoChatModeType mode);
void esco_viewer_set_nick (EscoViewer * view,
						   const gchar * nick);
void esco_viewer_set_encoding (EscoViewer * view,
							   const gchar * encoding);

/* Contents */
void esco_viewer_append_line (EscoViewer * view,
							  EscoViewerMessageType type,
							  guint data,
							  const gchar * arg1,
							  const gchar * arg2,
							  const gchar * arg3,
							  gboolean arg4);
void esco_viewer_clear (EscoViewer * view);

/* Misc */
void esco_viewer_clear_pester (EscoViewer * view);
void esco_viewer_find (EscoViewer * viewer,
					   const gchar * search_str,
					   gboolean case_insensitive);


/* These functions affect all EscoViewer objects */

/* Features */
void esco_viewers_set_show_timestamps (gboolean show);
void esco_viewers_set_use_image_smileys (gboolean image_smileys);
void esco_viewers_set_nicks_indented (gboolean indent);

/* Styles */
void esco_viewers_set_style_fg (ViewerStyleType type,
								const gchar * fg_color);
void esco_viewers_set_style_bg (ViewerStyleType type,
								const gchar * bg_color);
void esco_viewers_set_style_bold (ViewerStyleType type,
								  gboolean bold);
void esco_viewers_set_style_italic (ViewerStyleType type,
									gboolean italic);
void esco_viewers_set_style_uline (ViewerStyleType type,
								   gboolean uline);

void esco_viewers_set_font (const gchar * font);
void esco_viewers_set_fg (const gchar * fg_color);
void esco_viewers_set_bg (const gchar * fg_color);
void esco_viewers_set_uri_prefixes (GSList * prefixes);

#endif /* __ESCO_VIEWER_H__ */
