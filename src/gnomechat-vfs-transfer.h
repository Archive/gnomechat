/*
 *  GnomeChat: src/gnomechat-vfs-transfer.h
 *
 *  Copyright (c) 2003 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.01234567890
 */

/*
 * GnomeVFS-based GObject file-transfer wrapper, with optional ability to check the
 * to-file's mtime, and only do the xfer if the from-file is later.
 * Implements the GnomechatTransfer interface.
 */


#ifndef __GNOMECHAT_VFS_TRANSFER_H__
#define __GNOMECHAT_VFS_TRANSFER_H__

#include "gnomechat-transfer.h"

#include <libgnomevfs/gnome-vfs-async-ops.h>


#define GNOMECHAT_TYPE_VFS_TRANSFER						(gnomechat_vfs_transfer_get_type ())
#define GNOMECHAT_VFS_TRANSFER(obj)						(G_TYPE_CHECK_INSTANCE_CAST ((obj), GNOMECHAT_TYPE_VFS_TRANSFER, GnomechatVFSTransfer))
#define GNOMECHAT_VFS_TRANSFER_CLASS(klass)				(G_TYPE_CHECK_CLASS_CAST ((klass), GNOMECHAT_TYPE_VFS_TRANSFER, GnomechatVFSTransferClass))
#define GNOMECHAT_IS_VFS_TRANSFER(obj)					(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GNOMECHAT_TYPE_VFS_TRANSFER))
#define GNOMECHAT_IS_VFS_TRANSFER_CLASS(klass)			(G_TYPE_CHECK_CLASS_TYPE ((klass), GNOMECHAT_TYPE_VFS_TRANSFER))
#define GNOMECHAT_TRANSFER_GET_CLASS(obj)				(G_TYPE_INSTANCE_GET_CLASS ((obj), GNOMECHAT_TYPE_VFS_TRANSFER, GnomechatVFSTransferClass))

#define GNOMECHAT_TYPE_VFS_MULTI_TRANSFER				(gnomechat_vfs_multi_transfer_get_type ())
#define GNOMECHAT_VFS_MULTI_TRANSFER(obj)				(G_TYPE_CHECK_INSTANCE_CAST ((obj), GNOMECHAT_TYPE_VFS_MULTI_TRANSFER, GnomechatVFSMultiTransfer))
#define GNOMECHAT_VFS_MULTI_TRANSFER_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST ((klass), GNOMECHAT_TYPE_VFS_MULTI_TRANSFER, GnomechatVFSMultiTransferClass))
#define GNOMECHAT_IS_VFS_MULTI_TRANSFER(obj)			(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GNOMECHAT_TYPE_VFS_MULTI_TRANSFER))
#define GNOMECHAT_IS_VFS_MULTI_TRANSFER_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), GNOMECHAT_TYPE_VFS_MULTI_TRANSFER))
#define GNOMECHAT_MULTI_TRANSFER_GET_CLASS(obj)			(G_TYPE_INSTANCE_GET_CLASS ((obj), GNOMECHAT_TYPE_VFS_MULTI_TRANSFER, GnomechatVFSMultiTransferClass))


typedef struct _GnomechatVFSTransfer GnomechatVFSTransfer;
typedef struct _GnomechatVFSTransferClass GnomechatVFSTransferClass;

typedef struct _GnomechatVFSMultiTransfer GnomechatVFSMultiTransfer;
typedef struct _GnomechatVFSMultiTransferClass GnomechatVFSMultiTransferClass;


struct _GnomechatVFSTransfer
{
	GObject object;

	GnomeVFSAsyncHandle *handle;

	GnomeVFSURI *from_uri;
	GnomeVFSURI *to_uri;
	time_t from_mtime;
	time_t to_mtime;

	GnomeVFSFileSize file_size;
	GnomeVFSFileSize bytes_copied;

	GnomeVFSResult error:6;
	GnomechatTransferStatus status:4;
	GnomechatTransferType type:3;
	gboolean update_only:1;
};

struct _GnomechatVFSTransferClass
{
	GObjectClass parent_class;
};


struct _GnomechatVFSMultiTransfer
{
	GObject object;

	GnomeVFSAsyncHandle *handle;
	GHashTable *transfers;

	/* This Transfer */
	GnomechatVFSTransfer *current_xfer;

	/* All Transfers */
	GnomeVFSFileSize total_size;
	GnomeVFSFileSize bytes_copied;
	gulong n_files;
	gulong file_index;

	GnomeVFSResult error:6;
	GnomechatTransferStatus status:4;
	GnomechatTransferType type:3;
	gboolean update_only:1;
};

struct _GnomechatVFSMultiTransferClass
{
	GObjectClass parent_class;
};


GType gnomechat_vfs_transfer_get_type (void);
GType gnomechat_vfs_multi_transfer_get_type (void);


GnomechatVFSTransfer *gnomechat_vfs_transfer_new (GnomeVFSURI * from_uri,
												  GnomeVFSURI * to_uri,
												  gboolean update_only);

GnomechatVFSMultiTransfer *gnomechat_vfs_multi_transfer_new (gboolean update_only);

GnomechatVFSTransfer *gnomechat_vfs_multi_transfer_append (GnomechatVFSMultiTransfer * xfer,
														   GnomeVFSURI * from_uri,
														   GnomeVFSURI * to_uri);


#endif /* __GNOMECHAT_VFS_TRANSFER_H__ */
