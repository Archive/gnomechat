/*
 *  GnomeChat: src/connect.c
 *
 *  Copyright (c) 2002, 2003 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.01234567890
 */


#include "gnomechat.h"
#include "connect.h"

#include "esco-chat-box.h"
#include "esco-userlist-view.h"
#include "esco-viewer.h"
#include "netserv-ui.h"
#include "prefs.h"
#include "stock-items.h"
#include "utils.h"
#include "windows.h"

#include <gtk/gtkdialog.h>
#include <gtk/gtkeditable.h>
#include <gtk/gtklabel.h>

#include <libgircclient/girc-client.h>

#include <string.h>

#define NETWORK_ID_DATAKEY (get_network_id_quark ())


typedef struct
{
	gchar *network_id;
	gchar *current_server_id;

	GSList *server_ids;

	GIrcClient *connection;
	GtkWidget *status_label;

	GHashTable *chat_boxes;

	gulong quit_cb_id;
	gboolean motd_attempt:1;
}
OpenConnection;

typedef struct
{
	GtkWidget *win;

	GtkWidget *nick_label;
	GtkWidget *realname_label;
	GtkWidget *username_label;
	GtkWidget *hostname_label;
	GtkWidget *ip_label;
	GtkWidget *server_label;
	GtkWidget *comment_label;

	GtkWidget *channels_view;
	GtkTreeSelection *channels_sel;
	GtkWidget *bbox;
}
WhoisDialog;


static GHashTable *open_connections = NULL;


/* ******************* *
 *  UTILITY FUNCTIONS  *
 * ******************* */

static GQuark
get_network_id_quark (void)
{
	static GQuark quark = 0;

	if (quark == 0)
	{
		quark = g_quark_from_static_string ("gnomechat-network-id");
	}

	return quark;
}


static gboolean
clean_table (gpointer key,
			 gpointer value,
			 gpointer data)
{
	if (value == data)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}


static void
hash_item_destroyed (gpointer hash_table,
					 GObject * old_location)
{
	g_hash_table_foreach_remove ((GHashTable *) hash_table, clean_table, old_location);
}


static void
remove_weak_refs (gpointer key,
				  gpointer value,
				  gpointer data)
{
	g_object_weak_unref (G_OBJECT (value), hash_item_destroyed,
						 ((OpenConnection *) (data))->chat_boxes);
}


static void
unref_target (gpointer target)
{
	if (target != NULL)
	{
		girc_target_unref (target);
	}
}


static OpenConnection *
open_connection_new (const gchar * network_id,
					 GtkWidget * status_label)
{
	OpenConnection *retval = NULL;

	retval = g_new0 (OpenConnection, 1);
	g_assert (retval != NULL);

	retval->network_id = g_strdup (network_id);

	retval->connection = NULL;

	retval->server_ids = prefs_get_network_server_ids (network_id);
	retval->current_server_id = (gchar *) retval->server_ids->data;

	retval->status_label = status_label;
	g_object_add_weak_pointer (G_OBJECT (status_label),
							   (gpointer *) & (retval->status_label));

	retval->chat_boxes = g_hash_table_new_full (g_direct_hash, g_direct_equal,
												unref_target, NULL);

	return retval;
}


static void
open_connection_free (OpenConnection * conn)
{
	if (conn != NULL)
	{
		for (; conn->server_ids != NULL;
			 conn->server_ids = g_slist_remove (conn->server_ids, conn->server_ids->data))
		{
			g_free (conn->server_ids->data);
		}

		if (conn->chat_boxes != NULL)
		{
			g_hash_table_foreach (conn->chat_boxes, remove_weak_refs, conn);
			g_hash_table_destroy (conn->chat_boxes);
		}

		g_free (conn);
	}
}


static void
add_connection_id_to_list (gpointer key,
						   gpointer value,
						   GSList ** list_ptr)
{
	*list_ptr = g_slist_append (*list_ptr, key);
}


static void
change_my_nicks (gpointer key,
				 EscoChatBox * box,
				 const gchar * new_nick)
{
	esco_chat_box_set_nick (box, new_nick);
}


static gchar *
get_changer_name (const GValue * changer)
{
	gchar *retval = NULL;

	if (changer != NULL)
	{
		if (G_VALUE_HOLDS_STRING (changer))
		{
			retval = g_value_dup_string (changer);
		}
		else if (G_VALUE_HOLDS_BOXED (changer))
		{
			gpointer boxed = g_value_get_boxed (changer);

			if (GIRC_IS_CHANNEL_USER (changer))
			{
				retval = g_strdup (GIRC_CHANNEL_USER (boxed)->user->nick);
			}
			else if (GIRC_IS_USER (changer))
			{
				retval = g_strdup (GIRC_USER (boxed)->nick);
			}
		}
	}

	return retval;
}


/* This function is up here so we can do this thing w/o prototypes */
static void
quit_cb (GIrcClient * irc,
		 gboolean requested,
		 OpenConnection * conn)
{
	GSList *list;
	gchar *name,
	 *msg;

	name = prefs_get_network_name (conn->network_id);
	msg = g_strdup_printf (_("Disconnected from %s."), name);

	for (list = girc_g_hash_table_copy_to_slist (conn->chat_boxes, NULL);
		 list != NULL; list = g_slist_remove_link (list, list))
	{
		EscoViewer *viewer = (EscoViewer *) (((EscoChatBox *) (list->data))->viewer);

		if (ESCO_IS_CHAT_BOX (list->data))
		{
			esco_viewer_append_line (viewer, ESCO_VIEWER_MESSAGE_CARDINAL,
									 0, msg, NULL, NULL, FALSE);
		}
	}

	g_free (msg);

	if (!requested)
	{
		msg = g_strdup_printf (_("Reconnecting to %s..."), name);

		for (list = girc_g_hash_table_copy_to_slist (conn->chat_boxes, NULL);
			 list != NULL; list = g_slist_remove_link (list, list))
		{
			EscoViewer *viewer = (EscoViewer *) (((EscoChatBox *) (list->data))->viewer);

			if (ESCO_IS_CHAT_BOX (list->data))
			{
				esco_viewer_append_line (viewer, ESCO_VIEWER_MESSAGE_CARDINAL,
										 0, msg, NULL, NULL, FALSE);
			}
		}

		g_free (msg);

		girc_client_open (irc);
	}
	else
	{
		/* We need to unref here because it crashes otherwise, and I can't think of
		   a way to fix it without seriously fucking some shit up in libgtcpsocket. */
		g_object_unref (irc);
	}

	g_free (name);
}


static void
try_next_addr (OpenConnection * conn)
{
	gchar *address,
	 *net_name,
	 *msg,
	 *encoding,
	 *pass;
	guint port;

	g_free (conn->server_ids->data);
	conn->server_ids = g_slist_remove_link (conn->server_ids, conn->server_ids);
	conn->current_server_id = (gchar *) (conn->server_ids->data);

	address = prefs_get_server_address (conn->network_id, conn->current_server_id);
	net_name = prefs_get_network_name (conn->network_id);

	msg = g_strdup_printf (_("%sConnecting to %s%s\nFinding %s..."),
						   "<b>", net_name, "</b>", address);
	gtk_label_set_markup (GTK_LABEL (conn->status_label), msg);
	g_free (msg);

	port = prefs_get_server_port (conn->network_id, conn->current_server_id);
	pass = prefs_get_server_passwd (conn->network_id, conn->current_server_id);
	encoding = prefs_get_server_charset (conn->network_id, conn->current_server_id);

	if (encoding == NULL)
		encoding = prefs_get_network_charset (conn->network_id);

	g_signal_handler_disconnect (conn->connection, conn->quit_cb_id);
	girc_client_quit (conn->connection, NULL);
	conn->quit_cb_id = g_signal_connect (conn->connection, "quit", G_CALLBACK (quit_cb),
										 conn);

	g_object_set (conn->connection, "address", address, "port", port,
				  "encoding", encoding, "pass", pass, NULL);

	g_free (address);
	g_free (encoding);
	g_free (pass);

	girc_client_open (conn->connection);
}


/* ******************** *
 *  CALLBACK FUNCTIONS  *
 * ******************** */

static void
lookup_done_cb (GTcpConnection * connection,
				GTcpLookupStatus status,
				OpenConnection * conn)
{
	if (status == GTCP_LOOKUP_OK)
	{
		gchar *msg,
		 *net_name,
		 *server_name;

		net_name = prefs_get_network_name (conn->network_id);
		server_name = prefs_get_server_address (conn->network_id,
												conn->current_server_id);

		msg = g_strdup_printf (_("%sConnecting to %s%s\nOpening connection to %s..."),
							   "<b>", net_name, "</b>", server_name);

		g_free (net_name);
		g_free (server_name);

		gtk_label_set_markup (GTK_LABEL (conn->status_label), msg);
		g_free (msg);
	}
	else
	{
		try_next_addr (conn);
	}
}


static void
connect_done_cb (GTcpConnection * connection,
				 GTcpConnectionStatus status,
				 OpenConnection * conn)
{
	if (status == GTCP_CONNECTION_CONNECTED)
	{
		EscoChatBox *box = g_hash_table_lookup (conn->chat_boxes, NULL);
		gchar *login_msg,
		 *msg,
		 *net_name,
		 *server_name;

		g_assert (box != NULL);

		server_name = prefs_get_server_address (conn->network_id,
												conn->current_server_id);

		login_msg = g_strdup_printf (_("Logging in to %s..."), server_name);
		g_free (server_name);

		net_name = prefs_get_network_name (conn->network_id);
		msg = g_strdup_printf (_("%sConnecting to %s%s\n%s"),
							   "<b>", net_name, "</b>", login_msg);

		gtk_label_set_markup (GTK_LABEL (conn->status_label), msg);
		g_free (msg);

		msg = g_strdup_printf (_("Connected to %s..."), net_name);
		g_free (net_name);

		esco_viewer_append_line (ESCO_VIEWER (box->viewer), ESCO_VIEWER_MESSAGE_CARDINAL,
								 0, msg, NULL, NULL, FALSE);
		g_free (msg);

		esco_viewer_append_line (ESCO_VIEWER (box->viewer), ESCO_VIEWER_MESSAGE_STATUS, 0,
								 login_msg, NULL, NULL, FALSE);

		g_free (login_msg);
	}
	else
	{
		/* Try the next port number (current_port + 1) until port == 7010 */
		guint port = (gtcp_connection_get_port (connection) + 1);

		if (port > 7010)
			return;

		g_signal_handler_disconnect (conn->connection, conn->quit_cb_id);
		girc_client_quit (conn->connection, NULL);
		conn->quit_cb_id = g_signal_connect (conn->connection, "quit",
											 G_CALLBACK (quit_cb), conn);

		gtcp_connection_set_port (connection, port);

		girc_client_open (conn->connection);
	}
}


static void
connect_complete_cb (GIrcClient * irc,
					 GIrcClientStatus status,
					 const gchar * servername,
					 const gchar * nick,
					 OpenConnection * conn)
{
	if (status == GIRC_CLIENT_CONNECTED)
	{
		GSList *channels;

		esco_chat_box_set_nick (ESCO_CHAT_BOX (g_hash_table_lookup (conn->chat_boxes,
																	NULL)), nick);

		g_signal_emit_by_name (gtk_widget_get_toplevel (conn->status_label), "response",
							   GTK_RESPONSE_DELETE_EVENT);

		for (channels = prefs_get_network_channels (conn->network_id); channels != NULL;
			 channels = g_slist_remove_link (channels, channels))
		{
			gchar *cmd;

			if (channels->data != NULL)
			{
				cmd = g_strdup_printf ("JOIN %s", (const gchar *) channels->data);
				girc_client_send_raw (irc, cmd);
				g_free (cmd);

				g_free (channels->data);
			}
		}

	}
	else
	{
		try_next_addr (conn);
	}
}


static void
nick_changed_cb (GIrcClient * irc,
				 const gchar * new_nick,
				 OpenConnection * conn)
{
	g_hash_table_foreach (conn->chat_boxes, (GHFunc) change_my_nicks,
						  (gpointer) new_nick);
}


static void
msg_recv_cb (GIrcClient * irc,
			 GIrcReplyType reply,
			 const gchar * msg,
			 OpenConnection * conn)
{
	EscoChatBox *box = g_hash_table_lookup (conn->chat_boxes, NULL);
	gchar *servername = NULL;

	g_return_if_fail (box != NULL);

	g_object_get (irc, "server-name", &servername, NULL);

	esco_viewer_append_line (ESCO_VIEWER (box->viewer), ESCO_VIEWER_MESSAGE_SERVER,
							 reply, servername, msg, NULL, FALSE);

	g_free (servername);
}


static void
target_opened_cb (GIrcClient * irc,
				  GIrcTarget * target,
				  OpenConnection * conn)
{
	EscoChatBox *box = ESCO_CHAT_BOX (windows_get_empty_chat ());
	gchar *msg = NULL,
	 *net_name;

	g_return_if_fail (box != NULL);

	g_hash_table_insert (conn->chat_boxes, girc_target_ref (target), box);
	g_object_weak_ref (G_OBJECT (box), hash_item_destroyed, conn->chat_boxes);

	esco_userlist_view_clear (ESCO_USERLIST_VIEW (box->userlist_view));
	esco_chat_box_set_irc (box, irc);

	net_name = prefs_get_network_name (conn->network_id);
	esco_chat_box_set_subtitle (box, net_name);
	g_free (net_name);

	box->target = girc_target_ref (target);

	if (GIRC_IS_CHANNEL (target))
	{
		esco_chat_box_set_mode (box, ESCO_CHAT_MODE_CHANNEL);
		esco_chat_box_set_title (box, target->channel.name);
		esco_chat_box_set_nick (box, girc_client_get_nick (irc));

		msg = g_strdup_printf (_("Joining %s..."), target->channel.name);
	}
	else if (GIRC_IS_QUERY (target))
	{
		esco_chat_box_set_mode (box, ESCO_CHAT_MODE_USER);
		esco_chat_box_set_title (box, target->query.user->nick);
		esco_chat_box_set_nick (box, girc_client_get_nick (irc));

		msg = g_strdup_printf (_("Opening private chat with %s..."),
							   target->query.user->nick);
	}
	else
	{
		g_assert_not_reached ();
	}

	esco_viewer_append_line (ESCO_VIEWER (box->viewer), ESCO_VIEWER_MESSAGE_CARDINAL,
							 0, msg, NULL, NULL, FALSE);
	g_free (msg);
}


static void
target_closed_cb (GIrcClient * irc,
				  GIrcTarget * target,
				  const gchar * reason,
				  OpenConnection * conn)
{
	EscoChatBox *box = g_hash_table_lookup (conn->chat_boxes, target);
	gchar *msg = NULL;

	if (box == NULL)
		return;

	if (GIRC_IS_CHANNEL (target))
	{
		if (reason != NULL && reason[0] != '\0')
		{
			msg = g_strdup_printf (_("Left %s (Reason: %s)"), target->channel.name,
								   reason);
		}
		else
		{
			msg = g_strdup_printf (_("Left %s"), target->channel.name);
		}
	}
	else if (GIRC_IS_QUERY (target))
	{
		msg = g_strdup_printf (_("Closing private chat with %s."),
							   target->query.user->nick);
	}
	else
	{
		g_assert_not_reached ();
	}

	esco_viewer_append_line (ESCO_VIEWER (box->viewer), ESCO_VIEWER_MESSAGE_CARDINAL,
							 0, msg, NULL, NULL, FALSE);
	g_free (msg);

	esco_chat_box_set_irc (box, NULL);
	esco_chat_box_set_mode (box, ESCO_CHAT_MODE_NONE);
	esco_chat_box_set_title (box, NULL);
	esco_chat_box_set_nick (box, NULL);
	esco_chat_box_set_subtitle (box, NULL);
	esco_userlist_view_clear (ESCO_USERLIST_VIEW (box->userlist_view));

	girc_target_unref (box->target);
	box->target = NULL;

	g_hash_table_remove (conn->chat_boxes, target);
}


static void
target_recv_cb (GIrcClient * irc,
				GIrcTarget * target,
				GValue * sender,
				GIrcUserMessageType msg_type,
				const gchar * msg,
				OpenConnection * conn)
{
	EscoChatBox *box;
	gchar *nick;

	box = g_hash_table_lookup (conn->chat_boxes, target);

	g_return_if_fail (box != NULL);

	if (GIRC_IS_CHANNEL (target))
	{
		nick = get_changer_name (sender);
	}
	else if (GIRC_IS_QUERY (target))
	{
		nick = g_strdup (target->query.user->nick);
	}
	else
	{
		nick = g_strdup ("***");
	}

	switch (msg_type)
	{
	case GIRC_USER_MESSAGE_PRIVMSG:
		esco_viewer_append_line (ESCO_VIEWER (box->viewer), ESCO_VIEWER_MESSAGE_MSG,
								 0, nick, msg, NULL, FALSE);
		break;

	case GIRC_USER_MESSAGE_ACTION:
		esco_viewer_append_line (ESCO_VIEWER (box->viewer), ESCO_VIEWER_MESSAGE_ACTION,
								 0, nick, msg, NULL, FALSE);
		break;

	case GIRC_USER_MESSAGE_NOTICE:
		esco_viewer_append_line (ESCO_VIEWER (box->viewer), ESCO_VIEWER_MESSAGE_NOTICE,
								 0, nick, msg, NULL, FALSE);
		break;

	default:
		break;
	}

	g_free (nick);
}


static void
query_changed_cb (GIrcClient * irc,
				  GIrcQuery * query,
				  GIrcQueryChangedType changed,
				  const gchar * msg,
				  OpenConnection * conn)
{
	EscoChatBox *box = g_hash_table_lookup (conn->chat_boxes, query);

	g_return_if_fail (box != NULL);

	esco_viewer_append_line (ESCO_VIEWER (box->viewer),
							 ESCO_VIEWER_MESSAGE_USER_CHANGED,
							 changed, query->user->nick, msg, NULL, FALSE);
}


static void
channel_changed_cb (GIrcClient * irc,
					GIrcChannel * channel,
					GValue * changer,
					GIrcChannelInfoType changed,
					GValue * value,
					gboolean is_on,
					OpenConnection * conn)
{
	EscoChatBox *box = g_hash_table_lookup (conn->chat_boxes, channel);
	gchar *value_str = NULL;

	g_return_if_fail (box != NULL);

	switch (changed)
	{
	case GIRC_CHANNEL_INFO_MODES:
		gtk_editable_set_editable (GTK_EDITABLE (box->topic_entry),
								   ((channel->modes &
									 GIRC_CHANNEL_MODE_TOPIC_PROTECT)
									&& channel->my_user->modes <
									GIRC_CHANNEL_USER_MODE_HALFOPS));
		break;

	case GIRC_CHANNEL_INFO_USERS:
		{
			GSList *users;

			esco_userlist_view_clear (ESCO_USERLIST_VIEW (box->userlist_view));

			for (users = girc_channel_get_users (channel); users != NULL;
				 users = g_slist_remove_link (users, users))
			{
				if (users->data != NULL)
				{
					esco_userlist_view_add_user (ESCO_USERLIST_VIEW (box->userlist_view),
												 users->data);
					girc_channel_user_unref (users->data);
				}
			}

		}
		break;

	case GIRC_CHANNEL_INFO_KEY:
		value_str = g_strdup (channel->key);
		break;

	case GIRC_CHANNEL_INFO_LIMIT:
		value_str = g_strdup_printf ("%u", channel->limit);
		break;

	case GIRC_CHANNEL_INFO_TOPIC:
		esco_chat_box_set_topic (box, channel->topic);
	case GIRC_CHANNEL_INFO_BAN:
	case GIRC_CHANNEL_INFO_BAN_EXCEPTION:
	case GIRC_CHANNEL_INFO_INVITE:
	case GIRC_CHANNEL_INFO_INVITE_EXCEPTION:
		if (value != NULL)
		{
			value_str = g_value_dup_string (value);
		}
		break;

	default:
		g_assert_not_reached ();
		break;
	}

	if (changer != NULL)
	{
		gchar *changer_name = get_changer_name (changer);

		if (changer_name != NULL)
		{
			if (changed == GIRC_CHANNEL_INFO_MODES)
			{
				GIrcChannelModeFlags modes = g_value_get_flags (value);
				gboolean is_on = FALSE;

				if (modes & GIRC_CHANNEL_MODE_ANONYMOUS)
				{
					is_on = channel->modes & GIRC_CHANNEL_MODE_ANONYMOUS;

					esco_viewer_append_line (ESCO_VIEWER (box->viewer),
											 ESCO_VIEWER_MESSAGE_CHANNEL_MODES_CHANGED,
											 GIRC_CHANNEL_MODE_ANONYMOUS, changer_name,
											 NULL, NULL, is_on);
				}

				if (modes & GIRC_CHANNEL_MODE_SECRET)
				{
					is_on = channel->modes & GIRC_CHANNEL_MODE_SECRET;

					esco_viewer_append_line (ESCO_VIEWER (box->viewer),
											 ESCO_VIEWER_MESSAGE_CHANNEL_MODES_CHANGED,
											 GIRC_CHANNEL_MODE_SECRET, changer_name,
											 NULL, NULL, is_on);
				}

				if (modes & GIRC_CHANNEL_MODE_PRIVATE)
				{
					is_on = channel->modes & GIRC_CHANNEL_MODE_PRIVATE;

					esco_viewer_append_line (ESCO_VIEWER (box->viewer),
											 ESCO_VIEWER_MESSAGE_CHANNEL_MODES_CHANGED,
											 GIRC_CHANNEL_MODE_PRIVATE, changer_name,
											 NULL, NULL, is_on);
				}

				if (modes & GIRC_CHANNEL_MODE_INVITE_ONLY)
				{
					is_on = channel->modes & GIRC_CHANNEL_MODE_INVITE_ONLY;

					esco_viewer_append_line (ESCO_VIEWER (box->viewer),
											 ESCO_VIEWER_MESSAGE_CHANNEL_MODES_CHANGED,
											 GIRC_CHANNEL_MODE_INVITE_ONLY, changer_name,
											 NULL, NULL, is_on);
				}

				if (modes & GIRC_CHANNEL_MODE_MODERATED)
				{
					is_on = channel->modes & GIRC_CHANNEL_MODE_MODERATED;

					esco_viewer_append_line (ESCO_VIEWER (box->viewer),
											 ESCO_VIEWER_MESSAGE_CHANNEL_MODES_CHANGED,
											 GIRC_CHANNEL_MODE_MODERATED, changer_name,
											 NULL, NULL, is_on);
				}

				if (modes & GIRC_CHANNEL_MODE_NOMSG)
				{
					is_on = channel->modes & GIRC_CHANNEL_MODE_NOMSG;

					esco_viewer_append_line (ESCO_VIEWER (box->viewer),
											 ESCO_VIEWER_MESSAGE_CHANNEL_MODES_CHANGED,
											 GIRC_CHANNEL_MODE_NOMSG, changer_name,
											 NULL, NULL, is_on);
				}

				if (modes & GIRC_CHANNEL_MODE_QUIET)
				{
					is_on = channel->modes & GIRC_CHANNEL_MODE_QUIET;

					esco_viewer_append_line (ESCO_VIEWER (box->viewer),
											 ESCO_VIEWER_MESSAGE_CHANNEL_MODES_CHANGED,
											 GIRC_CHANNEL_MODE_QUIET, changer_name,
											 NULL, NULL, is_on);
				}

				if (modes & GIRC_CHANNEL_MODE_REOP)
				{
					is_on = channel->modes & GIRC_CHANNEL_MODE_REOP;

					esco_viewer_append_line (ESCO_VIEWER (box->viewer),
											 ESCO_VIEWER_MESSAGE_CHANNEL_MODES_CHANGED,
											 GIRC_CHANNEL_MODE_REOP, changer_name,
											 NULL, NULL, is_on);
				}

				if (modes & GIRC_CHANNEL_MODE_TOPIC_PROTECT)
				{
					is_on = channel->modes & GIRC_CHANNEL_MODE_TOPIC_PROTECT;

					esco_viewer_append_line (ESCO_VIEWER (box->viewer),
											 ESCO_VIEWER_MESSAGE_CHANNEL_MODES_CHANGED,
											 GIRC_CHANNEL_MODE_TOPIC_PROTECT,
											 changer_name, NULL, NULL, is_on);
				}
			}
			else if (changed != GIRC_CHANNEL_INFO_USERS)
			{
				esco_viewer_append_line (ESCO_VIEWER (box->viewer),
										 ESCO_VIEWER_MESSAGE_CHANNEL_CHANGED, changed,
										 changer_name, value_str, NULL, FALSE);
			}

			g_free (changer_name);
		}
	}

	netserv_ui_update_channel_props_dialog (irc, channel, channel->name, changed);
}


static void
channel_user_changed_cb (GIrcClient * irc,
						 GIrcChannel * channel,
						 GIrcChannelUser * user,
						 GIrcChannelUserChangedType changed,
						 GValue * changer,
						 GValue * value,
						 OpenConnection * conn)
{
	EscoChatBox *box = g_hash_table_lookup (conn->chat_boxes, channel);

	g_return_if_fail (box != NULL);

	switch (changed)
	{
		/* New or Leaving Users, must add or remove. */
	case GIRC_CHANNEL_USER_CHANGED_JOIN:
		esco_viewer_append_line (ESCO_VIEWER (box->viewer),
								 ESCO_VIEWER_MESSAGE_CHANNEL_USER_CHANGED, changed,
								 user->user->nick, channel->name, NULL, FALSE);
		esco_userlist_view_add_user (ESCO_USERLIST_VIEW (box->userlist_view), user);
		break;

	case GIRC_CHANNEL_USER_CHANGED_KICKED:
		{
			gchar *changer_name = get_changer_name (changer);

			esco_viewer_append_line (ESCO_VIEWER (box->viewer),
									 ESCO_VIEWER_MESSAGE_CHANNEL_USER_CHANGED, changed,
									 user->user->nick, changer_name,
									 g_value_get_string (value), FALSE);

			g_free (changer_name);

			esco_userlist_view_remove_user (ESCO_USERLIST_VIEW (box->userlist_view),
											user);
		}
		break;
	case GIRC_CHANNEL_USER_CHANGED_QUIT:
	case GIRC_CHANNEL_USER_CHANGED_PART:
		esco_viewer_append_line (ESCO_VIEWER (box->viewer),
								 ESCO_VIEWER_MESSAGE_CHANNEL_USER_CHANGED, changed,
								 user->user->nick, channel->name,
								 g_value_get_string (value), FALSE);

		esco_userlist_view_remove_user (ESCO_USERLIST_VIEW (box->userlist_view), user);
		break;

	case GIRC_CHANNEL_USER_CHANGED_NICK:
		esco_userlist_view_update_user (ESCO_USERLIST_VIEW (box->userlist_view), user);

		esco_viewer_append_line (ESCO_VIEWER (box->viewer),
								 ESCO_VIEWER_MESSAGE_CHANNEL_USER_CHANGED, changed,
								 g_value_get_string (value), user->user->nick, NULL,
								 FALSE);
		break;

	case GIRC_CHANNEL_USER_CHANGED_MODES:
		{
			GIrcChannelUserModeFlags modes = g_value_get_flags (value);
			gboolean is_on = FALSE;
			gchar *changer_name = get_changer_name (changer);

			if (user == channel->my_user)
			{
				esco_chat_box_set_status (box, user->modes);
			}

			esco_userlist_view_update_user (ESCO_USERLIST_VIEW (box->userlist_view),
											user);

			if (modes & GIRC_CHANNEL_USER_MODE_CREATOR)
			{
				is_on = user->modes & GIRC_CHANNEL_USER_MODE_CREATOR;

				esco_viewer_append_line (ESCO_VIEWER (box->viewer),
										 ESCO_VIEWER_MESSAGE_CHANNEL_USER_MODES_CHANGED,
										 GIRC_CHANNEL_USER_MODE_CREATOR,
										 changer_name, user->user->nick, NULL, is_on);
			}

			if (modes & GIRC_CHANNEL_USER_MODE_OPS)
			{
				is_on = user->modes & GIRC_CHANNEL_USER_MODE_OPS;

				esco_viewer_append_line (ESCO_VIEWER (box->viewer),
										 ESCO_VIEWER_MESSAGE_CHANNEL_USER_MODES_CHANGED,
										 GIRC_CHANNEL_USER_MODE_OPS,
										 changer_name, user->user->nick, NULL, is_on);
			}

			if (modes & GIRC_CHANNEL_USER_MODE_HALFOPS)
			{
				is_on = user->modes & GIRC_CHANNEL_USER_MODE_HALFOPS;

				esco_viewer_append_line (ESCO_VIEWER (box->viewer),
										 ESCO_VIEWER_MESSAGE_CHANNEL_USER_MODES_CHANGED,
										 GIRC_CHANNEL_USER_MODE_HALFOPS,
										 changer_name, user->user->nick, NULL, is_on);
			}

			if (modes & GIRC_CHANNEL_USER_MODE_VOICE)
			{
				is_on = user->modes & GIRC_CHANNEL_USER_MODE_VOICE;

				esco_viewer_append_line (ESCO_VIEWER (box->viewer),
										 ESCO_VIEWER_MESSAGE_CHANNEL_USER_MODES_CHANGED,
										 GIRC_CHANNEL_USER_MODE_VOICE,
										 changer_name, user->user->nick, NULL, is_on);
			}

			g_free (changer_name);
		}
		break;

	default:
		g_assert_not_reached ();
		break;
	}
}


static void
notice_recv_cb (GIrcClient * irc,
				const gchar * sender,
				const gchar * msg,
				const gchar * data,
				OpenConnection * conn)
{
	EscoChatBox *box;
	gchar *name = NULL;

	box = g_hash_table_lookup (conn->chat_boxes, NULL);

	g_return_if_fail (box != NULL);

	if (sender == NULL)
		name = prefs_get_network_name (conn->network_id);

	esco_viewer_append_line (ESCO_VIEWER (box->viewer), ESCO_VIEWER_MESSAGE_SERVER,
							 0, (sender != NULL ? sender : name), msg, NULL, FALSE);

	g_free (name);
}


static void
whois_recv_cb (GIrcClient * irc,
			   GIrcUser * user,
			   OpenConnection * conn)
{
	netserv_ui_open_whois_dialog (irc, NULL, user, user->nick);
}


static void
error_recv_cb (GIrcClient * irc,
			   GIrcServerErrorType error,
			   const gchar * msg,
			   OpenConnection * conn)
{
	GSList *boxes;

	for (boxes = girc_g_hash_table_copy_to_slist (conn->chat_boxes, NULL);
		 boxes != NULL; boxes = g_slist_remove (boxes, boxes->data))
	{
		EscoViewer *viewer = ESCO_VIEWER (ESCO_CHAT_BOX (boxes->data)->viewer);

		esco_viewer_append_line (viewer, ESCO_VIEWER_MESSAGE_ERROR,
								 error, msg, NULL, NULL, FALSE);
	}
}


static void
motd_recv_cb (GIrcClient * irc,
			  GIrcMotd * motd,
			  OpenConnection * conn)
{
	if (conn->motd_attempt == FALSE && !prefs_get_show_motd_on_connect ())
	{
		conn->motd_attempt = TRUE;
		return;
	}

	netserv_ui_open_motd_dialog (irc, NULL, motd->server, motd->motd);
}


static void
ctcp_recv_cb (GIrcClient * irc,
			  GIrcUser * user,
			  GIrcCtcpMessageType type,
			  const gchar * data,
			  OpenConnection * conn)
{
	EscoChatBox *box = g_hash_table_lookup (conn->chat_boxes, NULL);
	gchar *msg,
	 *reply;

	g_return_if_fail (box != NULL);

	switch (type)
	{
	case GIRC_CTCP_MESSAGE_VERSION:
		msg = g_strdup_printf (_("%s has requested to know what version of %s you are using."),
							   user->nick, GENERIC_NAME);
		reply = g_strdup_printf ("NOTICE %s :\001VERSION %s:%s\001", user->nick,
								 PACKAGE, VERSION);
		break;

	case GIRC_CTCP_MESSAGE_USERINFO:
		msg = g_strdup_printf (_("%s has requested to know more about you."), user->nick);
		reply = NULL;
		break;

	case GIRC_CTCP_MESSAGE_CLIENTINFO:
		msg = g_strdup_printf (_("%s has requested to know more about %s's capabilities."),
							   user->nick, GENERIC_NAME);
		reply = NULL;
		break;

	case GIRC_CTCP_MESSAGE_PING:
		msg = g_strdup_printf (_("%s has requested a speed check."), user->nick);
		reply = g_strdup_printf ("NOTICE %s :\001PING %lu\001", user->nick, (gulong) time (NULL));
		break;

	case GIRC_CTCP_MESSAGE_TIME:
		msg = g_strdup_printf (_("%s has requested your local time."), user->nick);
		reply = g_strdup_printf ("NOTICE %s :\001TIME %lu", user->nick, (gulong) time (NULL));
		break;

	default:
		msg = reply = NULL;
		break;
	}

	if (msg != NULL)
	{
		esco_viewer_append_line (ESCO_VIEWER (box->viewer), ESCO_VIEWER_MESSAGE_STATUS, 0,
								 msg, NULL, NULL, FALSE);
		g_free (msg);
	}

	if (reply != NULL)
	{
		girc_client_send_raw (conn->connection, reply);
	}
}


/* ************ *
 *  PUBLIC API  *
 * ************ */

void
connect_open_connection (const gchar * net_id,
						 GtkWidget * status_label)
{
	OpenConnection *conn = NULL;
	EscoChatBox *box;
	gchar *nick,
	 *realname,
	 *username,
	 *address,
	 *pass,
	 *charset,
	 *msg,
	 *net_name;
	guint port;

	g_return_if_fail (util_str_is_not_empty (net_id));
	g_return_if_fail (status_label != NULL);
	g_return_if_fail (GTK_IS_LABEL (status_label));

	if (open_connections == NULL)
	{
		open_connections = g_hash_table_new_full (g_str_hash, g_str_equal, NULL,
												  (GDestroyNotify) open_connection_free);
	}

	conn = g_hash_table_lookup (open_connections, net_id);

	if (conn != NULL)
	{
		if (conn->status_label != NULL)
		{
			GtkWindow *win = (GtkWindow *) gtk_widget_get_toplevel (conn->status_label);

			if (GTK_IS_WINDOW (win))
				gtk_window_present (win);
		}

		return;
	}

	conn = open_connection_new (net_id, status_label);
	g_hash_table_insert (open_connections, conn->network_id, conn);

	address = prefs_get_server_address (net_id, conn->current_server_id);

	net_name = prefs_get_network_name (net_id);
	msg = g_strdup_printf (_("%sConnecting to %s%s\nFinding %s..."),
						   "<b>", net_name, "</b>", address);
	gtk_label_set_markup (GTK_LABEL (conn->status_label), msg);
	g_free (msg);

	nick = prefs_get_nick ();
	realname = prefs_get_realname ();
	username = prefs_get_username ();
	pass = prefs_get_server_passwd (net_id, conn->current_server_id);
	charset = prefs_get_server_charset (net_id, conn->current_server_id);
	port = prefs_get_server_port (net_id, conn->current_server_id);

	if (charset == NULL)
		charset = prefs_get_network_charset (net_id);

	conn->connection = GIRC_CLIENT (girc_client_new (nick, realname, username, address,
													 port, pass, charset));
	g_object_set_qdata_full (G_OBJECT (conn->connection), NETWORK_ID_DATAKEY,
							 g_strdup (net_id), g_free);

	g_free (nick);
	g_free (realname);
	g_free (username);
	g_free (pass);
	g_free (charset);

	box = ESCO_CHAT_BOX (windows_get_empty_chat ());
	g_hash_table_insert (conn->chat_boxes, NULL, box);

	esco_chat_box_set_irc (box, GIRC_CLIENT (conn->connection));
	esco_chat_box_set_mode (box, ESCO_CHAT_MODE_SERVER);
	esco_chat_box_set_title (box, net_name);

	g_free (net_name);

	/* Connection Signals */
	g_signal_connect (conn->connection, "lookup-done", G_CALLBACK (lookup_done_cb), conn);
	g_signal_connect (conn->connection, "connect-done",
					  G_CALLBACK (connect_done_cb), conn);
	g_signal_connect (conn->connection, "connect-complete",
					  G_CALLBACK (connect_complete_cb), conn);
	conn->quit_cb_id =
		g_signal_connect (conn->connection, "quit", G_CALLBACK (quit_cb), conn);

	g_signal_connect (conn->connection, "ctcp-recv", G_CALLBACK (ctcp_recv_cb), conn);

	/* Target (Query & Channel) signals */
	g_signal_connect (conn->connection, "target-opened", G_CALLBACK (target_opened_cb),
					  conn);
	g_signal_connect (conn->connection, "target-closed", G_CALLBACK (target_closed_cb),
					  conn);
	g_signal_connect (conn->connection, "target-recv", G_CALLBACK (target_recv_cb), conn);

	/* Channel Signals */
	g_signal_connect (conn->connection, "channel-changed",
					  G_CALLBACK (channel_changed_cb), conn);
	g_signal_connect (conn->connection, "channel-user-changed",
					  G_CALLBACK (channel_user_changed_cb), conn);

	/* Query Signals */
	g_signal_connect (conn->connection, "query-changed", G_CALLBACK (query_changed_cb),
					  conn);

	/* Server Message Signals */
	g_signal_connect (conn->connection, "msg-recv", G_CALLBACK (msg_recv_cb), conn);
	g_signal_connect (conn->connection, "error-recv", G_CALLBACK (error_recv_cb), conn);
	g_signal_connect (conn->connection, "notice-recv", G_CALLBACK (notice_recv_cb), conn);
	g_signal_connect (conn->connection, "whois-recv", G_CALLBACK (whois_recv_cb), conn);
	g_signal_connect (conn->connection, "motd-recv", G_CALLBACK (motd_recv_cb), conn);
	g_signal_connect (conn->connection, "nick-changed", G_CALLBACK (nick_changed_cb),
					  conn);

	girc_client_open (GIRC_CLIENT (conn->connection));
}


GSList *
connect_get_open_connections (void)
{
	GSList *retval = NULL;

	if (open_connections != NULL)
	{
		g_hash_table_foreach (open_connections, (GHFunc) add_connection_id_to_list,
							  &retval);
	}

	return retval;
}


void
connect_close_connection (const gchar * id)
{
	OpenConnection *conn = NULL;

	g_return_if_fail (util_str_is_not_empty (id));

	if (open_connections == NULL)
	{
		open_connections = g_hash_table_new_full (g_str_hash, g_str_equal, NULL,
												  (GDestroyNotify) open_connection_free);
		return;
	}

	conn = g_hash_table_lookup (open_connections, id);

	if (conn != NULL && conn->connection != NULL)
		girc_client_quit (GIRC_CLIENT (conn->connection), NULL);

	g_hash_table_remove (open_connections, id);
}


GIrcClient *
connect_get_connection (const gchar * id)
{
	OpenConnection *conn;

	g_return_val_if_fail (util_str_is_not_empty (id), NULL);

	if (open_connections == NULL)
	{
		open_connections = g_hash_table_new_full (g_str_hash, g_str_equal, NULL,
												  (GDestroyNotify) open_connection_free);
		return NULL;
	}

	conn = g_hash_table_lookup (open_connections, id);

	return (conn != NULL ? conn->connection : NULL);
}


G_CONST_RETURN gchar *
connect_get_connection_netid (GIrcClient * irc)
{
	return g_object_get_qdata ((GObject *) irc, NETWORK_ID_DATAKEY);
}


EscoChatBox *
connect_get_chat_box_for_target (const gchar * id,
								 const GIrcTarget * target)
{
	OpenConnection *conn = NULL;

	g_return_val_if_fail (id != NULL && id[0] != '\0', NULL);
	g_return_val_if_fail (GIRC_IS_TARGET (target), NULL);

	conn = g_hash_table_lookup (open_connections, id);
	g_assert (conn != NULL);

	return g_hash_table_lookup (conn->chat_boxes, target);
}

/*
void
connect_send_message (EscoChatBox * box,
					  const gchar * msg)
{
	OpenConnection *conn = NULL;

	g_return_if_fail (box->id != NULL && box->id[0] != '\0');
	g_return_if_fail (ESCO_IS_CHAT_BOX (box));

	conn = g_hash_table_lookup (open_connections, id);
	g_assert (conn != NULL);

	
} */
