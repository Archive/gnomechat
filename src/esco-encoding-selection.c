/*
 *  GnomeChat: src/esco-encoding-selection.c
 *
 *  Copyright (c) 2002 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "gnomechat.h"

#include "esco-encoding-selection.h"
#include "esco-encoding.h"

#include "esco-type-builtins.h"

#include <gtk/gtkcellrenderertext.h>
#include <gtk/gtkhpaned.h>
#include <gtk/gtkliststore.h>
#include <gtk/gtktreeview.h>
#include <gtk/gtktreeselection.h>
#include <gtk/gtkscrolledwindow.h>
#include <gtk/gtkstock.h>

#include <utils.h>


/* ************************************************************************** *
 *  EscoEncodingSelection                                                      *
 * ************************************************************************** */

/* Types */
enum
{
	SELECTION_PROP_0,
	SELECTION_PROP_ENCODING
};

enum
{
	SELECTION_CHANGED,
	SELECTION_SIGNAL_LAST
};


enum
{
	GROUP_LIST_TYPE_ITEM,
	GROUP_LIST_NAME_ITEM,
	GROUP_LIST_MODEL_ITEM,
	GROUP_LIST_NUM_COLS
};

enum
{
	ENCODING_LIST_NAME_ITEM,
	ENCODING_LIST_RECOMMENDED_ITEM,
	ENCODING_LIST_ALIASES_ITEM,
	ENCODING_LIST_NUM_COLS
};


struct _EscoEncodingSelectionPrivate
{
	GtkWidget *group_view;
	GtkTreeSelection *group_sel;

	GtkWidget *encoding_view;
	GtkTreeModel *encoding_model;
	GtkTreeSelection *encoding_sel;

	EscoEncodingGroupType group;
	gchar *encoding;

	/* Used for construct-time encoding selection */
	gchar *new_encoding;
};


/* Global Variables */
static gpointer selection_parent_class = NULL;
static gint selection_signals[SELECTION_SIGNAL_LAST] = { 0 };


/* Utility Functions */
static GtkTreeModel *
load_encoding_model (const EscoEncoding * encodings)
{
	GtkListStore *model;
	GtkTreeIter iter;
	gint i;

	model = gtk_list_store_new (ENCODING_LIST_NUM_COLS,
								G_TYPE_STRING, G_TYPE_BOOLEAN, G_TYPE_POINTER);
	g_assert (model != NULL);

	for (i = 0; encodings[i].list_name != NULL; i++)
	{
		gtk_list_store_append (model, &iter);
		gtk_list_store_set (model, &iter,
							ENCODING_LIST_NAME_ITEM, encodings[i].list_name,
							ENCODING_LIST_RECOMMENDED_ITEM, encodings[i].recommended,
							ENCODING_LIST_ALIASES_ITEM, encodings[i].aliases, -1);
	}

	return GTK_TREE_MODEL (model);
}


static void
render_encoding_name (GtkTreeViewColumn * col,
					  GtkCellRenderer * cell,
					  GtkTreeModel * model,
					  GtkTreeIter * iter,
					  gpointer data)
{
	gchar *name,
	 *markup;
	gboolean recommended = FALSE;

	gtk_tree_model_get (model, iter,
						ENCODING_LIST_NAME_ITEM, &name,
						ENCODING_LIST_RECOMMENDED_ITEM, &recommended, -1);



	if (recommended)
		markup = g_strdup_printf ("%s <span foreground=\"red\">*</span>", name);
	else
		markup = g_strdup (name);

	g_object_set (cell, "markup", markup, NULL);
}


static void
find_and_select_encoding (EscoEncodingSelection * sel,
						  const gchar * encoding)
{
	GtkTreeModel *groups,
	 *encodings;
	GtkTreeIter group_iter,
	  encoding_iter;
	gchar **aliases;
	gint i;

	groups = gtk_tree_view_get_model (sel->_priv->group_sel->tree_view);

	if (gtk_tree_model_get_iter_first (groups, &group_iter))
	{
		do
		{
			gtk_tree_model_get (groups, &group_iter,
								GROUP_LIST_MODEL_ITEM, &encodings, -1);

			if (gtk_tree_model_get_iter_first (encodings, &encoding_iter))
			{
				do
				{
					gtk_tree_model_get (encodings, &encoding_iter,
										ENCODING_LIST_ALIASES_ITEM, &aliases, -1);

					for (i = 0; aliases[i] != NULL; i++)
					{
						if (g_ascii_strcasecmp (encoding, aliases[i]) == 0)
						{
							gtk_tree_selection_select_iter (sel->_priv->group_sel,
															&group_iter);
							util_scroll_tree_view_to_selection (sel->_priv->group_sel);
							gtk_tree_selection_select_iter (sel->_priv->encoding_sel,
															&encoding_iter);
							util_scroll_tree_view_to_selection (sel->_priv->encoding_sel);
							return;
						}
					}
				}
				while (gtk_tree_model_iter_next (encodings, &encoding_iter));
			}
		}
		while (gtk_tree_model_iter_next (groups, &group_iter));
	}
}


/* Sub-Widget Callbacks */
static void
group_sel_changed_cb (GtkTreeSelection * group_sel,
					  EscoEncodingSelection * sel)
{
	GtkTreeModel *model;
	GtkTreeIter iter;
	EscoEncodingGroupType group;

	if (gtk_tree_selection_get_selected (group_sel, &model, &iter))
	{
		gtk_tree_model_get (model, &iter,
							GROUP_LIST_TYPE_ITEM, &group,
							GROUP_LIST_MODEL_ITEM,
							(gpointer *) & (sel->_priv->encoding_model), -1);

		sel->_priv->group = group;
		gtk_tree_view_set_model (sel->_priv->encoding_sel->tree_view,
								 sel->_priv->encoding_model);

		if (gtk_tree_model_get_iter_first (sel->_priv->encoding_model, &iter))
		{
			gtk_tree_selection_select_iter (sel->_priv->encoding_sel, &iter);
		}
	}
}


static void
encoding_sel_changed_cb (GtkTreeSelection * encoding_sel,
						 EscoEncodingSelection * sel)
{
	GtkTreeModel *model;
	GtkTreeIter iter;
	gchar **aliases = NULL;

	if (gtk_tree_selection_get_selected (encoding_sel, &model, &iter))
	{
		gtk_tree_model_get (model, &iter, ENCODING_LIST_ALIASES_ITEM, &aliases, -1);

		sel->_priv->encoding = (gchar *) aliases[0];

		g_object_notify (G_OBJECT (sel), "encoding");
		g_signal_emit (sel, selection_signals[SELECTION_CHANGED], 0);
	}
}


/* Object Callbacks */
static void
esco_encoding_selection_get_property (GObject * object,
									  guint param_id,
									  GValue * value,
									  GParamSpec * pspec)
{
	EscoEncodingSelection *sel = ESCO_ENCODING_SELECTION (object);

	switch (param_id)
	{
	case SELECTION_PROP_ENCODING:
		g_value_set_string (value, sel->_priv->encoding);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}


static void
esco_encoding_selection_set_property (GObject * object,
									  guint param_id,
									  const GValue * value,
									  GParamSpec * pspec)
{
	EscoEncodingSelection *sel = ESCO_ENCODING_SELECTION (object);

	switch (param_id)
	{
	case SELECTION_PROP_ENCODING:
		esco_encoding_selection_set_encoding (sel, g_value_get_string (value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}


static GObject *
esco_encoding_selection_constructor (GType type,
									 guint n_construct_params,
									 GObjectConstructParam * construct_params)
{

	GObject *object;
	EscoEncodingSelection *sel;
	GtkWidget *paned,
	 *scrwin,
	 *tree_view;
	GtkListStore *model;
	GtkTreeViewColumn *col;
	GtkCellRenderer *cell;
	GSList *groups;
	GtkTreeIter iter;

	object = (*G_OBJECT_CLASS (selection_parent_class)->constructor) (type,
																	  n_construct_params,
																	  construct_params);

	sel = ESCO_ENCODING_SELECTION (object);

	paned = gtk_hpaned_new ();
	gtk_container_add (GTK_CONTAINER (object), paned);
	gtk_widget_show (paned);

	scrwin = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrwin),
									GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrwin), GTK_SHADOW_IN);
	gtk_paned_add1 (GTK_PANED (paned), scrwin);
	gtk_widget_show (scrwin);

	model = gtk_list_store_new (GROUP_LIST_NUM_COLS,
								ESCO_TYPE_ENCODING_GROUP_TYPE,
								G_TYPE_STRING, G_TYPE_OBJECT);
	for (groups = esco_encoding_get_all_groups ();
		 groups != NULL; groups = g_slist_remove (groups, groups->data))
	{
		EscoEncodingGroup *group = ESCO_ENCODING_GROUP (groups->data);

		gtk_list_store_append (model, &iter);
		gtk_list_store_set (model, &iter,
							GROUP_LIST_TYPE_ITEM, group->group,
							GROUP_LIST_NAME_ITEM, group->name,
							GROUP_LIST_MODEL_ITEM, load_encoding_model (group->encodings),
							-1);
	}

	tree_view = sel->_priv->group_view =
		gtk_tree_view_new_with_model (GTK_TREE_MODEL (model));
	gtk_container_add (GTK_CONTAINER (scrwin), tree_view);
	gtk_widget_show (tree_view);

	col = gtk_tree_view_column_new_with_attributes (_("Location"),
													gtk_cell_renderer_text_new (),
													"text", GROUP_LIST_NAME_ITEM, NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view), col);

	sel->_priv->group_sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree_view));
	g_signal_connect (sel->_priv->group_sel, "changed", G_CALLBACK (group_sel_changed_cb),
					  sel);

	scrwin = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrwin),
									GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrwin), GTK_SHADOW_IN);
	gtk_widget_set_size_request (scrwin, util_get_font_width (scrwin) * 32,
								 util_get_font_height (scrwin) * 11);
	gtk_paned_add2 (GTK_PANED (paned), scrwin);
	gtk_widget_show (scrwin);

	tree_view = sel->_priv->encoding_view = gtk_tree_view_new ();
	gtk_container_add (GTK_CONTAINER (scrwin), tree_view);
	gtk_widget_show (tree_view);

	col = gtk_tree_view_column_new ();
	gtk_tree_view_column_set_title (col, _("Character Encoding"));
	gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view), col);

	cell = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_start (col, cell, FALSE);
	gtk_tree_view_column_set_cell_data_func (col, cell, render_encoding_name, NULL, NULL);

	sel->_priv->encoding_sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree_view));
	g_signal_connect (sel->_priv->encoding_sel, "changed",
					  G_CALLBACK (encoding_sel_changed_cb), sel);

	if (sel->_priv->new_encoding)
	{
		find_and_select_encoding (sel, sel->_priv->new_encoding);
		g_free (sel->_priv->new_encoding);
		sel->_priv->new_encoding = NULL;
	}
	else
	{
		gtk_tree_model_get_iter_first (GTK_TREE_MODEL (model), &iter);
		gtk_tree_selection_select_iter (sel->_priv->group_sel, &iter);
	}

	return object;
}

static void
esco_encoding_selection_finalize (GObject * object)
{
	EscoEncodingSelection *sel = ESCO_ENCODING_SELECTION (object);

	if (sel->_priv->new_encoding)
		g_free (sel->_priv->new_encoding);

	g_free (sel->_priv);

	if (G_OBJECT_CLASS (selection_parent_class)->finalize)
		(*G_OBJECT_CLASS (selection_parent_class)->finalize) (object);
}


/* GType Functions */
static void
esco_encoding_selection_class_init (EscoEncodingSelectionClass * class)
{
	GObjectClass *object_class = G_OBJECT_CLASS (class);

	selection_parent_class = g_type_class_peek_parent (class);

	object_class->get_property = esco_encoding_selection_get_property;
	object_class->set_property = esco_encoding_selection_set_property;
	object_class->constructor = esco_encoding_selection_constructor;
	object_class->finalize = esco_encoding_selection_finalize;

	g_object_class_install_property (object_class, SELECTION_PROP_ENCODING,
									 g_param_spec_string ("encoding",
														  _("Character Encoding"),
														  _("The currently selected "
															"encoding."), NULL,
														  (G_PARAM_CONSTRUCT |
														   G_PARAM_READWRITE)));

	selection_signals[SELECTION_CHANGED] =
		g_signal_new ("changed", G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_LAST,
					  G_STRUCT_OFFSET (EscoEncodingSelectionClass, changed), NULL,
					  NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
}


static void
esco_encoding_selection_instance_init (EscoEncodingSelection * sel)
{
	sel->_priv = g_new0 (EscoEncodingSelectionPrivate, 1);
	g_assert (sel->_priv != NULL);
}


/* PUBLIC API */
GType
esco_encoding_selection_get_type (void)
{
	static GType type = 0;

	if (!type)
	{
		static const GTypeInfo info = {
			sizeof (EscoEncodingSelectionClass),
			NULL,				/* base_init */
			NULL,				/* base_finalize */
			(GClassInitFunc) esco_encoding_selection_class_init,
			NULL,				/* class_finalize */
			NULL,				/* class_data */
			sizeof (EscoEncodingSelection),
			0,					/* n_preallocs */
			(GInstanceInitFunc) esco_encoding_selection_instance_init,
		};

		type = g_type_register_static (GTK_TYPE_VBOX, "EscoEncodingSelection", &info, 0);
	}

	return type;
}


GtkWidget *
esco_encoding_selection_new (void) 
{
	const gchar *encoding;

	g_get_charset (&encoding);

	return GTK_WIDGET (g_object_new (ESCO_TYPE_ENCODING_SELECTION,
									 "encoding", encoding, NULL));
}


void
esco_encoding_selection_set_encoding (EscoEncodingSelection * sel,
									  const gchar * encoding) 
{
	g_return_if_fail (sel != NULL);
	g_return_if_fail (ESCO_IS_ENCODING_SELECTION (sel));
	g_return_if_fail (esco_encoding_is_valid (encoding));

	if (sel->_priv->group_view)
		find_and_select_encoding (sel, encoding);
	else
		sel->_priv->new_encoding = g_strdup (encoding);
}


G_CONST_RETURN gchar *
esco_encoding_selection_get_encoding (EscoEncodingSelection * sel)
{
	g_return_val_if_fail (sel != NULL, NULL);
	g_return_val_if_fail (ESCO_IS_ENCODING_SELECTION (sel), NULL);

	return sel->_priv->encoding;
}


/* ************************************************************************** *
 *  EscoEncodingSelectionDialog                                                *
 * ************************************************************************** */

enum
{
	DIALOG_PROP_0,
	DIALOG_PROP_ENCODING
};

enum
{
	DIALOG_CHANGED,
	DIALOG_SIGNAL_LAST
};


struct _EscoEncodingSelectionDialogPrivate
{
	EscoEncodingSelection *sel;
};


/* Global Vars */
static gpointer dialog_parent_class = NULL;
static gint dialog_signals[DIALOG_SIGNAL_LAST] = { 0 };


/* Object Callbacks */
static void
esco_encoding_selection_dialog_get_property (GObject * object,
											 guint param_id,
											 GValue * value,
											 GParamSpec * pspec)
{
	EscoEncodingSelectionDialog *esd = ESCO_ENCODING_SELECTION_DIALOG (object);

	switch (param_id)
	{
	case DIALOG_PROP_ENCODING:
		g_value_set_string (value, esd->_priv->sel->_priv->encoding);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}


static void
esco_encoding_selection_dialog_set_property (GObject * object,
											 guint param_id,
											 const GValue * value,
											 GParamSpec * pspec)
{
	EscoEncodingSelectionDialog *esd = ESCO_ENCODING_SELECTION_DIALOG (object);

	switch (param_id)
	{
	case DIALOG_PROP_ENCODING:
		esco_encoding_selection_dialog_set_encoding (esd, g_value_get_string (value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}


static GObject *
esco_encoding_selection_dialog_constructor (GType type,
											guint n_construct_params,
											GObjectConstructParam * construct_params)
{

	GObject *object;
	EscoEncodingSelectionDialog *esd;
	GtkWidget *sel;
	GtkDialog *dialog;

	object = (*G_OBJECT_CLASS (dialog_parent_class)->constructor) (type,
																   n_construct_params,
																   construct_params);
	esd = ESCO_ENCODING_SELECTION_DIALOG (object);
	dialog = GTK_DIALOG (object);

	gtk_dialog_add_buttons (dialog, GTK_STOCK_HELP, GTK_RESPONSE_HELP,
							GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
							GTK_STOCK_OK, GTK_RESPONSE_OK, NULL);
	gtk_dialog_set_has_separator (dialog, FALSE);
	gtk_dialog_set_default_response (dialog, GTK_RESPONSE_OK);

	sel = esco_encoding_selection_new ();
	esd->_priv->sel = ESCO_ENCODING_SELECTION (sel);
	gtk_container_set_border_width (GTK_CONTAINER (sel), 5);
	g_signal_connect_swapped (sel, "changed",
							  G_CALLBACK (esco_encoding_selection_dialog_changed), esd);
	gtk_container_add (GTK_CONTAINER (dialog->vbox), sel);
	gtk_widget_show (sel);

	return object;
}


static void
esco_encoding_selection_dialog_finalize (GObject * object)
{
	g_free (ESCO_ENCODING_SELECTION_DIALOG (object)->_priv);

	if (G_OBJECT_CLASS (dialog_parent_class)->finalize)
		(*G_OBJECT_CLASS (dialog_parent_class)->finalize) (object);
}


/* GType Functions */
static void
esco_encoding_selection_dialog_class_init (EscoEncodingSelectionDialogClass * class)
{
	GObjectClass *object_class = G_OBJECT_CLASS (class);

	dialog_parent_class = g_type_class_peek_parent (class);

	object_class->get_property = esco_encoding_selection_dialog_get_property;
	object_class->set_property = esco_encoding_selection_dialog_set_property;
	object_class->constructor = esco_encoding_selection_dialog_constructor;
	object_class->finalize = esco_encoding_selection_dialog_finalize;

	g_object_class_install_property (object_class, DIALOG_PROP_ENCODING,
									 g_param_spec_string ("encoding",
														  _("Character Encoding"),
														  _("The currently selected "
															"encoding."), NULL,
														  G_PARAM_READWRITE));
	dialog_signals[DIALOG_CHANGED] =
		g_signal_new ("changed", G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_LAST,
					  G_STRUCT_OFFSET (EscoEncodingSelectionDialogClass, changed),
					  NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
}


static void
esco_encoding_selection_dialog_instance_init (EscoEncodingSelectionDialog * esd)
{
	esd->_priv = g_new0 (EscoEncodingSelectionDialogPrivate, 1);
	g_assert (esd->_priv != NULL);
}


/* PUBLIC API */
GType
esco_encoding_selection_dialog_get_type (void)
{
	static GType type = 0;

	if (!type)
	{
		static const GTypeInfo info = {
			sizeof (EscoEncodingSelectionDialogClass),
			NULL,				/* base_init */
			NULL,				/* base_finalize */
			(GClassInitFunc) esco_encoding_selection_dialog_class_init,
			NULL,				/* class_finalize */
			NULL,				/* class_data */
			sizeof (EscoEncodingSelectionDialog),
			0,					/* n_preallocs */
			(GInstanceInitFunc) esco_encoding_selection_dialog_instance_init,
		};

		type = g_type_register_static (GTK_TYPE_DIALOG,
									   "EscoEncodingSelectionDialog", &info, 0);
	}

	return type;
}


GtkWidget *
esco_encoding_selection_dialog_new (const gchar * title)
{
	return GTK_WIDGET (g_object_new (ESCO_TYPE_ENCODING_SELECTION_DIALOG,
									 "title", title, NULL));
}


void
esco_encoding_selection_dialog_set_encoding (EscoEncodingSelectionDialog * esd,
											 const gchar * encoding)
{
	g_return_if_fail (esd != NULL);
	g_return_if_fail (ESCO_IS_ENCODING_SELECTION_DIALOG (esd));

	esco_encoding_selection_set_encoding (esd->_priv->sel, encoding);
	g_object_notify (G_OBJECT (esd), "encoding");
}


G_CONST_RETURN gchar *
esco_encoding_selection_dialog_get_encoding (EscoEncodingSelectionDialog * esd)
{
	g_return_val_if_fail (esd != NULL, NULL);
	g_return_val_if_fail (ESCO_IS_ENCODING_SELECTION_DIALOG (esd), NULL);

	return esco_encoding_selection_get_encoding (esd->_priv->sel);
}


void
esco_encoding_selection_dialog_changed (EscoEncodingSelectionDialog * esd)
{
	g_signal_emit (esd, dialog_signals[DIALOG_CHANGED], 0, NULL);
}
