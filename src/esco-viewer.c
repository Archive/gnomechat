/*
 *  GnomeChat: src/esco-viewer.h
 *
 *  Copyright (c) 2002 James M. Cape.
 *  All Rights Reserved.
 *  URI and smiley rendering functions Copyright (c) 2003 Adam Olsen.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "gnomechat.h"

#include "esco-viewer.h"
#include "esco-type-builtins.h"

#include "esco-encoding.h"
#include "esco-encoding-menu.h"
#include "prefs.h"
#include "utils.h"
#include "stock-items.h"

#include <pango/pango-font.h>
#include <gdk/gdkcolor.h>

#include <gtk/gtkimage.h>
#include <gtk/gtkimagemenuitem.h>
#include <gtk/gtkseparatormenuitem.h>
#include <gtk/gtkstock.h>

#include <libgnome/gnome-url.h>

#include <libgircclient/girc-types.h>

#include <string.h>
#include <time.h>


#define BUFFER_ITEM(boxed)		((BufferItem *)(boxed))
#define MAX_SCROLLBACK_LINES	512


typedef struct
{
	EscoViewerMessageType type;

	time_t time;

	guint data;

	gchar *arg1;
	gchar *arg2;
	gchar *arg3;

	gboolean arg4;
}
BufferItem;


enum
{
	PROP_0,
	PROP_MODE,
	PROP_NICK,
	PROP_ENCODING
};

enum
{
	SIGNAL_PESTER,
	SIGNAL_LINK_CLICKED,
	SIGNAL_LAST
};


/* Global Class Variables */
static gint esco_viewer_signals[SIGNAL_LAST] = { 0 };
static gpointer parent_class = NULL;


/* Basic Classwide Variables */
static GtkTextTagTable *table = NULL;
static GSList *all_viewers = NULL;
static GSList *uri_prefixes = NULL;
static gboolean show_timestamps = FALSE;
static gboolean show_pixbuf_smileys = TRUE;

static const gchar *const color_strings[] = {
	"#FFFFFF",
	"#000000",
	"#00007B",
	"#009400",
	"#FF0000",
	"#7B0000",
	"#9C009C",
	"#FF7B00",
	"#FFFF00",
	"#00FF00",
	"#009494",
	"#00FFFF",
	"#0000FF",
	"#FF00FF",
	"#7B7B7B",
	"#D6D6D6",
	NULL
};

static const gchar *const smiley_strings[10][11] = {
	{GNOMECHAT_STOCK_SMILEY_REGULAR, ":)", ":-)", "(:", "(-:", NULL},	// REGULAR
	{GNOMECHAT_STOCK_SMILEY_BIG, ":>", ":->", "<:", "<-:", NULL},	// BIG
	{GNOMECHAT_STOCK_SMILEY_WINK, ";)", ";-)", "(-;", "(;", NULL},	// WINK
	{GNOMECHAT_STOCK_SMILEY_CRY, "p(", "p-(", ":'(", ")d", ")-d", ")':", NULL},	// CRY
	{GNOMECHAT_STOCK_SMILEY_FROWN, ":(", ":-(", ")-:", "):", NULL},	// FROWN
	{GNOMECHAT_STOCK_SMILEY_SMIRK, ":/", ":-/", ":-\\", ":\\", "/:", "/-:", "\\:", "\\-:", NULL},	// SMIRK
	{GNOMECHAT_STOCK_SMILEY_TONGUE, ":p", ":-p", "d:", "d-:", NULL},	// TONGUE
	{GNOMECHAT_STOCK_SMILEY_LAUGH, ":d", ":-d", NULL},	// LAUGH
	{GNOMECHAT_STOCK_SMILEY_SHOCK, ":o", ":-o", "o:", "o-:", NULL},	// SHOCK
	{GNOMECHAT_STOCK_SMILEY_NONE, ":|", ":-|", "|:", "|-:", NULL},	// NONE
};


static GtkTextTag *style_tags[VIEWER_STYLE_LAST] = { NULL };
static GtkTextTag *timestamp_tag = NULL;
static GtkTextTag *cardinal_tag = NULL;


/* Utility Functions */
static void
free_buffer_item (BufferItem * line)
{
	if (line != NULL)
	{
		g_free (line->arg1);
		g_free (line->arg2);
		g_free (line->arg3);
		g_free (line);
	}
}


static void
update_tabs (EscoViewer * view)
{
	pango_tab_array_set_tab (view->tabs, 0, PANGO_TAB_LEFT, 1);
	pango_tab_array_set_tab (view->tabs, 1, PANGO_TAB_LEFT, view->time_pixel_size);
	gtk_text_view_set_tabs (GTK_TEXT_VIEW (view), view->tabs);

	gtk_text_view_set_indent (GTK_TEXT_VIEW (view), 0 - view->time_pixel_size);
}


static void
append_timestamp (EscoViewer * view,
				  time_t seconds)
{
	GtkTextBuffer *buffer;
	GtkTextIter iter,
	  start_iter;
	GdkRectangle rect;
	gint x,
	  y;

	struct tm tm_time;
	gchar time_buf[20];

	gchar *time_str;

	localtime_r (&seconds, &tm_time);
	strftime (time_buf, G_N_ELEMENTS (time_buf), "%r", &tm_time);

	time_str = g_strconcat ("[", time_buf, "] ", NULL);

	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (view));
	gtk_text_buffer_get_end_iter (buffer, &iter);

	gtk_text_buffer_insert (buffer, &iter, time_str, -1);
	g_free (time_str);

	/* Set tabs */
	gtk_text_view_get_iter_location (GTK_TEXT_VIEW (view), &iter, &rect);
	gtk_text_view_buffer_to_window_coords (GTK_TEXT_VIEW (view),
										   GTK_TEXT_WINDOW_TEXT,
										   rect.x + rect.width,
										   rect.y + rect.height, &x, &y);
	if (x + 1 > view->time_pixel_size)
	{
		view->time_pixel_size = x + 1;
		update_tabs (view);
	}

	gtk_text_buffer_insert (buffer, &iter, "\t", 1);

	gtk_text_buffer_get_iter_at_mark (buffer, &start_iter, view->ins_mark);
	gtk_text_buffer_apply_tag (buffer, timestamp_tag, &start_iter, &iter);

	gtk_text_buffer_get_end_iter (buffer, &iter);
	gtk_text_buffer_move_mark (buffer, view->ins_mark, &iter);
}


static void
update_global_colors (EscoViewer * view,
					  gboolean valid_fg,
					  GdkColor * fg,
					  gboolean valid_bg,
					  GdkColor * bg)
{
	GdkColor status_color;
	GtkWidget *widget = (GtkWidget *) view;
	GtkRcStyle *rc_style;

	rc_style = gtk_widget_get_modifier_style (widget);

	if (valid_fg)
	{
		rc_style->color_flags[GTK_STATE_NORMAL] |= GTK_RC_TEXT;

		rc_style->text[GTK_STATE_NORMAL].red = fg->red;
		rc_style->text[GTK_STATE_NORMAL].green = fg->green;
		rc_style->text[GTK_STATE_NORMAL].blue = fg->blue;

		if (valid_bg)
		{
			rc_style->color_flags[GTK_STATE_NORMAL] |= GTK_RC_BASE;

			rc_style->base[GTK_STATE_NORMAL].red = bg->red;
			rc_style->base[GTK_STATE_NORMAL].blue = bg->blue;
			rc_style->base[GTK_STATE_NORMAL].green = bg->blue;

			status_color.red = (fg->red + bg->red) / 2;
			status_color.green = (fg->green + bg->green) / 2;
			status_color.blue = (fg->blue + bg->blue) / 2;
		}
		else
		{
			rc_style->color_flags[GTK_STATE_NORMAL] =
				rc_style->color_flags[GTK_STATE_NORMAL] & ~GTK_RC_BASE;

			status_color.red = (fg->red + rc_style->base[GTK_STATE_NORMAL].red) / 2;
			status_color.green = (fg->green + rc_style->base[GTK_STATE_NORMAL].green) / 2;
			status_color.blue = (fg->blue + rc_style->base[GTK_STATE_NORMAL].blue) / 2;
		}
	}
	else
	{
		rc_style->color_flags[GTK_STATE_NORMAL] =
			rc_style->color_flags[GTK_STATE_NORMAL] & ~GTK_RC_TEXT;

		if (valid_bg)
		{
			rc_style->color_flags[GTK_STATE_NORMAL] |= GTK_RC_BASE;

			rc_style->base[GTK_STATE_NORMAL].red = bg->red;
			rc_style->base[GTK_STATE_NORMAL].blue = bg->blue;
			rc_style->base[GTK_STATE_NORMAL].green = bg->blue;

			status_color.red = (rc_style->text[GTK_STATE_NORMAL].red + bg->red) / 2;
			status_color.green = (rc_style->text[GTK_STATE_NORMAL].green + bg->green) / 2;
			status_color.blue = (rc_style->text[GTK_STATE_NORMAL].blue + bg->blue) / 2;
		}
		else
		{
			rc_style->color_flags[GTK_STATE_NORMAL] =
				rc_style->color_flags[GTK_STATE_NORMAL] & ~GTK_RC_BASE;

			status_color.red = (rc_style->text[GTK_STATE_NORMAL].red
								+ rc_style->base[GTK_STATE_NORMAL].red) / 2;
			status_color.green = (rc_style->text[GTK_STATE_NORMAL].green
								  + rc_style->base[GTK_STATE_NORMAL].green) / 2;
			status_color.blue = (rc_style->text[GTK_STATE_NORMAL].blue
								 + rc_style->base[GTK_STATE_NORMAL].blue) / 2;
		}
	}

	g_object_set (timestamp_tag, "foreground-gdk", &status_color, NULL);

	gtk_widget_modify_style (widget, rc_style);
}


static void
set_initial_style (EscoViewer * view)
{
	GtkRcStyle *rc_style;
	PangoFontDescription *font_desc;
	gchar *font,
	 *fg_color,
	 *bg_color;
	gboolean valid_fg,
	  valid_bg;
	GdkColor fg,
	  bg;

	gtk_widget_reset_rc_styles (GTK_WIDGET (view));
	rc_style = gtk_widget_get_modifier_style (GTK_WIDGET (view));

	font = prefs_get_viewer_font ();
	if (util_str_is_not_empty (font))
	{
		font_desc = pango_font_description_from_string (font);

		if (rc_style->font_desc != NULL)
			pango_font_description_free (rc_style->font_desc);

		rc_style->font_desc = font_desc;
	}
	else if (rc_style->font_desc)
	{
		pango_font_description_free (rc_style->font_desc);
		rc_style->font_desc = NULL;
	}

	g_free (font);


	gtk_widget_modify_style (GTK_WIDGET (view), rc_style);

	fg_color = prefs_get_viewer_fg ();
	valid_fg = (util_str_is_not_empty (fg_color) && gdk_color_parse (fg_color, &fg));
	g_free (fg_color);

	bg_color = prefs_get_viewer_bg ();
	valid_bg = (util_str_is_not_empty (bg_color) && gdk_color_parse (bg_color, &bg));
	g_free (bg_color);

	update_global_colors (view, valid_fg, &fg, valid_bg, &bg);
}


static void
set_pester_level (EscoViewer * view,
				  EscoPesterType pester_level)
{
	if (pester_level > view->pester_level)
	{
		view->pester_level = pester_level;
		g_signal_emit (view, esco_viewer_signals[SIGNAL_PESTER], 0, pester_level);
	}
}


static G_CONST_RETURN gchar *
get_smiley_stock_id (const gchar * smiley)
{
	static GHashTable *stock_ids = NULL;
	gint i,
	  i2 = 1;

	g_return_val_if_fail (smiley != NULL, NULL);

	if (stock_ids == NULL)
	{
		stock_ids = g_hash_table_new (g_str_hash, g_str_equal);

		for (i = 0; i < G_N_ELEMENTS (smiley_strings); i++)
		{
			for (i2 = 0; smiley_strings[i][i2] != NULL; i2++)
			{
				g_hash_table_insert (stock_ids, (gpointer) smiley_strings[i][i2],
									 (gpointer) smiley_strings[i][0]);
			}
		}
	}

	return g_hash_table_lookup (stock_ids, smiley);
}


static GdkPixbuf *
get_smiley_pixbuf (GtkWidget * view,
				   const gchar * stock_id)
{
	static GHashTable *pixbufs = NULL;
	GdkPixbuf *pixbuf = NULL;
	gint i;

	g_return_val_if_fail (stock_id != NULL, NULL);

	if (pixbufs == NULL)
	{
		pixbufs = g_hash_table_new_full (g_str_hash, g_str_equal, NULL, g_object_unref);


		for (i = 0; i < G_N_ELEMENTS (smiley_strings); i++)
		{
			pixbuf = gtk_widget_render_icon (view, smiley_strings[i][0],
											 GTK_ICON_SIZE_MENU, NULL);

			g_hash_table_insert (pixbufs, (gpointer) smiley_strings[i][0], pixbuf);
		}
	}

	pixbuf = (GdkPixbuf *) g_hash_table_lookup (pixbufs, stock_id);

	return pixbuf;
}


static gboolean
render_uri_to_buffer (GtkTextView * view,
					  GtkTextIter * iter,
					  const gchar * start,
					  const gchar * end)
{
	const gchar *pos = start;
	gunichar c;
	GtkTextBuffer *buffer = gtk_text_view_get_buffer ((GtkTextView *) view);

	/* Parse the first character of the scheme, which must be a-z */
	c = g_utf8_get_char (pos);

	if (c < 'a' || c > 'z')
		return FALSE;

	pos = g_utf8_next_char (pos);

	/* Parse the rest of the scheme, which can be a-z, 0-9, +, -, or . */
	do
	{
		c = g_utf8_get_char (pos);

		if (c == '\0' || pos >= end)
			return FALSE;

		if (c != ':')
			pos = g_utf8_next_char (pos);
	}
	while ((c >= 'a' && c <= 'z') || (c >= '0' && c <= '9') || c == '+' || c == '-'
		   || c == '.');

	/* Parse the seperator */
	c = g_utf8_get_char (pos);

	if (c != ':')
		return FALSE;

	pos = g_utf8_next_char (pos);

	/* Parse the rest of the URI.  I decided to be lazy here, and just assume everything is valid */
	c = g_utf8_get_char (pos);

	if (c == '\0' || pos >= end)
		return FALSE;

	/* Render it already */
	gtk_text_buffer_insert_with_tags (buffer, iter, start, (gint) (end - start),
									  style_tags[VIEWER_STYLE_LINK], NULL);
	return TRUE;
}

static gboolean
render_smiley_to_buffer (GtkTextView * view,
						 GtkTextIter * iter,
						 const gchar * start,
						 const gchar * end)
{
	const gchar *smiley;
	gchar *smiley_str;
	GdkPixbuf *pixbuf;
	GtkTextBuffer *buffer = gtk_text_view_get_buffer ((GtkTextView *) view);

	smiley_str = g_utf8_strdown (start, end - start);
	smiley = get_smiley_stock_id (smiley_str);
	g_free (smiley_str);

	if (smiley)
	{
		pixbuf = get_smiley_pixbuf (GTK_WIDGET (view), smiley);
		gtk_text_buffer_insert_pixbuf (buffer, iter, pixbuf);

		return TRUE;
	}

	return FALSE;
}


static void
render_msg_to_buffer (GtkTextView * view,
					  GtkTextIter * iter,
					  const gchar * raw)
{
	GtkTextBuffer *buffer = gtk_text_view_get_buffer (view);
	const gchar *start = raw,
	 *end = start;

	if (raw == NULL)
		return;

	while (g_utf8_get_char (start) != '\0')
	{
		/* Render whitespace */
		while (g_unichar_isspace (g_utf8_get_char (end)))
			end = g_utf8_next_char (end);

		gtk_text_buffer_insert (buffer, iter, start, (gint) (end - start));
		start = end;

		/* Render non-whitespace */
		while (!g_unichar_isspace (g_utf8_get_char (end))
			   && g_utf8_get_char (end) != '\0')
			end = g_utf8_next_char (end);

		if (!render_uri_to_buffer (view, iter, start, end) &&
			!(show_pixbuf_smileys && render_smiley_to_buffer (view, iter, start, end)))
		{
			/* Ordinary text */
			gtk_text_buffer_insert (buffer, iter, start, (gint) (end - start));
		}
		start = end;
	}
}


/*
static void
get_color_tags (guint8 color1,
				guint8 color2,
				GtkTextTag ** fg_tag,
				GtkTextTag ** bg_tag)
{
	GtkTextTag *tag;
	gchar *name;

	if (color1 > 99 || color2 > 99)
		return;

	// Foreground
	// Get the real colors
	if (color1 == 99)
		color1 = 16;
	else if (color1 > 15)
		color1 = color1 % 16;

	name = g_strdup_printf ("color-fg-%d", color1);
	tag = gtk_text_tag_table_lookup (table, name);

	if (tag == NULL)
	{
		tag = gtk_text_tag_new (name);
		g_object_set (tag, "foreground", color_strings[color1], NULL);
		gtk_text_tag_table_add (table, tag);
	}

	if (fg_tag != NULL)
		*fg_tag = tag;

	// Background
	if (color2 == 99)
		color2 = 16;
	else if (color2 > 15)
		color2 = color2 % 16;

	name = g_strdup_printf ("color-bg-%d", color2);
	tag = gtk_text_tag_table_lookup (table, name);

	if (tag == NULL)
	{
		tag = gtk_text_tag_new (name);
		g_object_set (tag, "background", color_strings[color2], NULL);
		gtk_text_tag_table_add (table, tag);
	}

	if (bg_tag != NULL)
		*bg_tag = tag;
}
*/


static gboolean
link_event_cb (GtkTextTag * texttag,
			   GObject * obj,
			   GdkEvent * event,
			   GtkTextIter * iter,
			   gpointer data)
{

	if (event->type == GDK_BUTTON_PRESS && ((GdkEventButton *) (event))->button == 1)
	{
		GtkTextBuffer *buffer;
		GtkTextIter *start_iter,
		 *end_iter;
		gchar *uri = NULL;

		start_iter = gtk_text_iter_copy (iter);
		end_iter = gtk_text_iter_copy (iter);

		while (!gtk_text_iter_begins_tag (start_iter, style_tags[VIEWER_STYLE_LINK]))
		{
			gtk_text_iter_backward_char (start_iter);
		}

		while (!gtk_text_iter_ends_tag (end_iter, style_tags[VIEWER_STYLE_LINK]))
		{
			gtk_text_iter_forward_char (end_iter);
		}

		buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (obj));
		uri = gtk_text_buffer_get_text (buffer, start_iter, end_iter, FALSE);
		gtk_text_iter_free (start_iter);
		gtk_text_iter_free (end_iter);

		if (uri != NULL)
		{
			gnome_url_show (uri, NULL);
			g_free (uri);
		}
	}

	return FALSE;
}


static GtkTextTagTable *
create_global_tags_table (void)
{
	GtkTextTagTable *table;
	GtkTextTag *tag;
	ViewerStyle *style;
	gint i;

	table = gtk_text_tag_table_new ();

	for (i = 0; i < VIEWER_STYLE_LAST; i++)
	{
		const gchar *const style_names[] = {
			"link",
			"my-action",
			"my-nick",
			"to-me",
			"to-me-action",
			"to-me-nick",
			"action",
			"nick",
			"server",
			"server-name",
			"channel",
			"channel-nick",
			"channel-name",
			"channel-value",
			"error"
		};

		tag = gtk_text_tag_new (style_names[i]);
		style_tags[i] = tag;
		style = prefs_get_viewer_style (i);

		if (i == VIEWER_STYLE_LINK)
		{
			g_signal_connect (tag, "event", G_CALLBACK (link_event_cb), NULL);
		}

		g_object_set (tag, "background-full-height", TRUE, NULL);

		if (style->fg_color)
			g_object_set (tag, "foreground", style->fg_color, NULL);

		if (style->bg_color)
			g_object_set (tag, "background", style->bg_color, NULL);

		if (style->bold)
			g_object_set (tag, "weight", PANGO_WEIGHT_BOLD, NULL);

		if (style->italic)
			g_object_set (tag, "style", PANGO_STYLE_ITALIC, NULL);

		if (style->uline)
			g_object_set (tag, "underline", PANGO_UNDERLINE_SINGLE, NULL);

		gtk_text_tag_table_add (table, tag);
	}

	/* App-related status messages */
	tag = gtk_text_tag_new (appname);
	g_object_set (G_OBJECT (tag), "indent", 0, NULL);
	gtk_text_tag_table_add (table, tag);

	timestamp_tag = gtk_text_tag_new ("timestamp");
	gtk_text_tag_table_add (table, timestamp_tag);

	cardinal_tag = gtk_text_tag_new ("cardinal");
	g_object_set (G_OBJECT (cardinal_tag),
				  "background-full-height", TRUE, "weight", PANGO_WEIGHT_BOLD, NULL);
	gtk_text_tag_table_add (table, cardinal_tag);

	return table;
}


/* ****************************** *
 *  Message Formatting Functions  *
 * ****************************** */

/* Outgoing User Messages */
static void
render_my_msg (EscoViewer * view,
			   BufferItem * item)
{
	GtkTextBuffer *buffer;
	GtkTextIter iter;

	/* We don't do encoding conversion here, because we'll only have UTF-8 strings
	   because they're all coming from GTK+ */

	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (view));
	gtk_text_buffer_get_end_iter (buffer, &iter);

	gtk_text_buffer_insert_with_tags (buffer, &iter, view->nick, -1,
									  style_tags[VIEWER_STYLE_MY_NICK], NULL);

	render_msg_to_buffer (GTK_TEXT_VIEW (view), &iter, item->arg1);
}


static void
render_my_action (EscoViewer * view,
				  BufferItem * item)
{
	GtkTextBuffer *buffer;
	GtkTextIter iter,
	  start_iter;

	/* We don't do encoding conversion here, because we'll only have UTF-8 strings
	   because they're all coming from GTK+ */

	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (view));
	gtk_text_buffer_get_end_iter (buffer, &iter);

	gtk_text_buffer_insert (buffer, &iter, "* ", 2);
	gtk_text_buffer_insert_with_tags (buffer, &iter, view->plain_nick, -1,
									  style_tags[VIEWER_STYLE_MY_NICK], NULL);

	gtk_text_buffer_insert (buffer, &iter, " ", 1);

	render_msg_to_buffer (GTK_TEXT_VIEW (view), &iter, item->arg1);

	gtk_text_buffer_get_iter_at_mark (buffer, &start_iter, view->ins_mark);
	gtk_text_buffer_apply_tag (buffer, style_tags[VIEWER_STYLE_MY_ACTIONS], &start_iter,
							   &iter);
}


/* Incoming User Messages */
static gboolean
render_msg (EscoViewer * view,
			BufferItem * item)
{
	GtkTextBuffer *buffer;
	GtkTextIter iter;
	gchar *nick,
	 *msg;
	GtkTextTag *tag;
	gboolean to_me;

	nick = girc_convert_to_utf8 (item->arg1, view->encoding);

	g_return_val_if_fail (nick != NULL, FALSE);

	msg = girc_convert_to_utf8 (item->arg2, view->encoding);

	to_me = (girc_g_utf8_strcasestr (msg, view->plain_nick) != NULL);

	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (view));
	gtk_text_buffer_get_end_iter (buffer, &iter);

	if (to_me)
	{
		tag = style_tags[VIEWER_STYLE_TO_ME_NICK];
	}
	else
	{
		tag = style_tags[VIEWER_STYLE_NICK];
	}

	gtk_text_buffer_insert_with_tags (buffer, &iter, "<", 1, tag, NULL);
	gtk_text_buffer_insert_with_tags (buffer, &iter, nick, -1, tag, NULL);
	gtk_text_buffer_insert_with_tags (buffer, &iter, ">", 1, tag, NULL);
	gtk_text_buffer_insert (buffer, &iter, " ", 1);

	render_msg_to_buffer (GTK_TEXT_VIEW (view), &iter, msg);

	if (to_me)
	{
		GtkTextIter start_iter;

		gtk_text_buffer_get_iter_at_mark (buffer, &start_iter, view->ins_mark);
		gtk_text_buffer_apply_tag (buffer, style_tags[VIEWER_STYLE_TO_ME],
								   &start_iter, &iter);
	}

	g_free (nick);
	g_free (msg);

	return to_me;
}


static gboolean
render_action (EscoViewer * view,
			   BufferItem * item)
{
	GtkTextBuffer *buffer;
	GtkTextTag *tag;
	gchar *nick,
	 *action;
	GtkTextIter iter,
	  start_iter;
	gboolean to_me;

	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (view));
	gtk_text_buffer_get_end_iter (buffer, &iter);

	nick = girc_convert_to_utf8 (item->arg1, view->encoding);
	g_return_val_if_fail (nick != NULL, FALSE);

	action = girc_convert_to_utf8 (item->arg2, view->encoding);

	to_me = (girc_g_utf8_strcasestr (action, view->plain_nick) != NULL);

	if (to_me)
	{
		tag = style_tags[VIEWER_STYLE_TO_ME_NICK];
	}
	else
	{
		tag = style_tags[VIEWER_STYLE_NICK];
	}

	gtk_text_buffer_insert (buffer, &iter, "* ", 2);
	gtk_text_buffer_insert_with_tags (buffer, &iter, nick, -1, tag, NULL);
	gtk_text_buffer_insert (buffer, &iter, " ", 1);

	render_msg_to_buffer (GTK_TEXT_VIEW (view), &iter, action);

	gtk_text_buffer_get_iter_at_mark (buffer, &start_iter, view->ins_mark);

	if (to_me)
	{
		tag = style_tags[VIEWER_STYLE_TO_ME_ACTIONS];
	}
	else
	{
		tag = style_tags[VIEWER_STYLE_ACTIONS];
	}

	gtk_text_buffer_apply_tag (buffer, tag, &start_iter, &iter);

	g_free (nick);
	g_free (action);

	return to_me;
}


static gboolean
render_notice (EscoViewer * view,
			   BufferItem * item)
{
	GtkTextBuffer *buffer;
	GtkTextIter iter;
	gchar *nick,
	 *msg;
	GtkTextTag *tag;
	gboolean to_me;

	nick = girc_convert_to_utf8 (item->arg1, view->encoding);
	g_return_val_if_fail (nick != NULL, FALSE);

	msg = girc_convert_to_utf8 (item->arg2, view->encoding);

	to_me = (girc_g_utf8_strcasestr (msg, view->plain_nick) != NULL);

	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (view));
	gtk_text_buffer_get_end_iter (buffer, &iter);

	if (to_me)
	{
		tag = style_tags[VIEWER_STYLE_TO_ME_NICK];
	}
	else
	{
		tag = style_tags[VIEWER_STYLE_NICK];
	}

	gtk_text_buffer_insert_with_tags (buffer, &iter, "(", 1, tag, NULL);
	gtk_text_buffer_insert_with_tags (buffer, &iter, nick, -1, tag, NULL);
	gtk_text_buffer_insert_with_tags (buffer, &iter, ")", 1, tag, NULL);
	gtk_text_buffer_insert (buffer, &iter, " ", 1);
	gtk_text_buffer_insert_with_tags (buffer, &iter, "Notice:", -1, tag, NULL);
	gtk_text_buffer_insert (buffer, &iter, " ", 1);

	render_msg_to_buffer (GTK_TEXT_VIEW (view), &iter, msg);

	g_free (nick);
	g_free (msg);

	return to_me;
}


static void
render_user_changed (EscoViewer * view,
					 BufferItem * item)
{
	GtkTextBuffer *buffer;
	GtkTextIter start_iter,
	  iter;
	gchar *nick,
	 *value;

	nick = girc_convert_to_utf8 (item->arg1, view->encoding);
	g_return_if_fail (nick != NULL);

	value = girc_convert_to_utf8 (item->arg2, view->encoding);

	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (view));
	gtk_text_buffer_get_end_iter (buffer, &iter);
	gtk_text_buffer_get_end_iter (buffer, &start_iter);

	gtk_text_buffer_insert_with_tags (buffer, &iter, nick, -1,
									  style_tags[VIEWER_STYLE_CHANNEL_NICK], NULL);

	switch (item->data)
	{
	case GIRC_QUERY_CHANGED_NICK:
		g_return_if_fail (value != NULL);
		gtk_text_buffer_insert (buffer, &iter, " has changed their nickname to \"", -1);
		gtk_text_buffer_insert_with_tags (buffer, &iter, value, -1,
										  style_tags[VIEWER_STYLE_CHANNEL_VALUE], NULL);
		gtk_text_buffer_insert (buffer, &iter, "\"", -1);
		break;

	case GIRC_QUERY_CHANGED_QUIT:
		gtk_text_buffer_insert (buffer, &iter, " has quit IRC", -1);

		value = girc_convert_to_utf8 (item->arg3, view->encoding);
		if (value != NULL)
		{
			gtk_text_buffer_insert (buffer, &iter, " (Reason: ", -1);
			gtk_text_buffer_insert_with_tags (buffer, &iter, value, -1,
											  style_tags[VIEWER_STYLE_CHANNEL_VALUE],
											  NULL);
			gtk_text_buffer_insert (buffer, &iter, ")", -1);
		}
		break;

	default:
		g_assert_not_reached ();
		break;
	}

	g_free (nick);
	g_free (value);

	gtk_text_buffer_insert (buffer, &iter, ".", -1);

	gtk_text_buffer_get_iter_at_mark (buffer, &start_iter, view->ins_mark);
	gtk_text_buffer_apply_tag (buffer, style_tags[VIEWER_STYLE_CHANNEL], &start_iter,
							   &iter);
}


static void
render_channel_changed (EscoViewer * view,
						BufferItem * item)
{
	GtkTextBuffer *buffer;
	GtkTextIter start_iter,
	  iter;
	gchar *nick = NULL,
	 *value = NULL;

	nick = girc_convert_to_utf8 (item->arg1, view->encoding);
	g_return_if_fail (nick != NULL);

	value = girc_convert_to_utf8 (item->arg2, view->encoding);

	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (view));
	gtk_text_buffer_get_end_iter (buffer, &iter);

	gtk_text_buffer_insert_with_tags (buffer, &iter, nick, -1,
									  style_tags[VIEWER_STYLE_CHANNEL_NICK], NULL);

	switch (item->data)
	{
		// Unused Here
	case GIRC_CHANNEL_INFO_MODES:
	case GIRC_CHANNEL_INFO_USERS:
		break;

	case GIRC_CHANNEL_INFO_TOPIC:
		if (value != NULL)
		{
			gtk_text_buffer_insert (buffer, &iter, " has changed the topic to \"", -1);
			gtk_text_buffer_insert_with_tags (buffer, &iter, value, -1,
											  style_tags[VIEWER_STYLE_CHANNEL_VALUE],
											  NULL);
			gtk_text_buffer_insert (buffer, &iter, "\".", 2);
		}
		else
		{
			gtk_text_buffer_insert (buffer, &iter, " has deleted the channel topic.", -1);
		}
		break;

	case GIRC_CHANNEL_INFO_KEY:
		if (value != NULL)
		{
			gtk_text_buffer_insert (buffer, &iter, " has changed the password to \"", -1);
			gtk_text_buffer_insert_with_tags (buffer, &iter, value, -1,
											  style_tags[VIEWER_STYLE_CHANNEL_VALUE],
											  NULL);
			gtk_text_buffer_insert (buffer, &iter, "\".", 2);
		}
		else
		{
			gtk_text_buffer_insert (buffer, &iter, " has removed the password.", -1);
		}
		break;

	case GIRC_CHANNEL_INFO_LIMIT:
		if (value != NULL)
		{
			gtk_text_buffer_insert (buffer, &iter,
									" has limited the number of users who can join to ",
									-1);
			gtk_text_buffer_insert_with_tags (buffer, &iter, value, -1,
											  style_tags[VIEWER_STYLE_CHANNEL_VALUE],
											  NULL);
			gtk_text_buffer_insert (buffer, &iter, ".", 1);
		}
		else
		{
			gtk_text_buffer_insert (buffer, &iter,
									" has removed the limit on the number of users who "
									"can join.", -1);
		}
		break;

	case GIRC_CHANNEL_INFO_BAN:
		if (item->arg4)
		{
			gtk_text_buffer_insert (buffer, &iter, " has banned users who match ", -1);
		}
		else
		{
			gtk_text_buffer_insert (buffer, &iter,
									" has removed the ban on users who match ", -1);
		}

		gtk_text_buffer_insert_with_tags (buffer, &iter, value, -1,
										  style_tags[VIEWER_STYLE_CHANNEL_VALUE], NULL);
		gtk_text_buffer_insert (buffer, &iter, ".", 1);
		break;

	case GIRC_CHANNEL_INFO_BAN_EXCEPTION:
		if (item->arg4)
		{
			gtk_text_buffer_insert (buffer, &iter, " has excepted users who match ", -1);
		}
		else
		{
			gtk_text_buffer_insert (buffer, &iter,
									" has removed the exception for users who match ",
									-1);
		}

		gtk_text_buffer_insert_with_tags (buffer, &iter, value, -1,
										  style_tags[VIEWER_STYLE_CHANNEL_VALUE], NULL);
		gtk_text_buffer_insert (buffer, &iter, " from being banned.", -1);
		break;

	case GIRC_CHANNEL_INFO_INVITE:
		if (item->arg4)
		{
			gtk_text_buffer_insert (buffer, &iter, " has invited users who match ", -1);
		}
		else
		{
			gtk_text_buffer_insert (buffer, &iter,
									" has removed the invitation to users who match ",
									-1);
		}

		gtk_text_buffer_insert_with_tags (buffer, &iter, value, -1,
										  style_tags[VIEWER_STYLE_CHANNEL_VALUE], NULL);
		gtk_text_buffer_insert (buffer, &iter, ".", 1);
		break;

	case GIRC_CHANNEL_INFO_INVITE_EXCEPTION:
		if (item->arg4)
		{
			gtk_text_buffer_insert (buffer, &iter, " has excepted users who match ", -1);
		}
		else
		{
			gtk_text_buffer_insert (buffer, &iter,
									" has removed the exception for users who match ",
									-1);
		}

		gtk_text_buffer_insert_with_tags (buffer, &iter, value, -1,
										  style_tags[VIEWER_STYLE_CHANNEL_VALUE], NULL);
		gtk_text_buffer_insert (buffer, &iter, " from being invited.", -1);
		break;

	default:
		g_assert_not_reached ();
		break;
	}

	g_free (nick);
	g_free (value);

	gtk_text_buffer_get_iter_at_mark (buffer, &start_iter, view->ins_mark);
	gtk_text_buffer_apply_tag (buffer, style_tags[VIEWER_STYLE_CHANNEL], &start_iter,
							   &iter);
}


static void
render_channel_modes_changed (EscoViewer * view,
							  BufferItem * item)
{
	GtkTextBuffer *buffer;
	GtkTextIter start_iter,
	  iter;
	gchar *nick;

	nick = girc_convert_to_utf8 (item->arg1, view->encoding);
	g_return_if_fail (nick != NULL);

	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (view));
	gtk_text_buffer_get_end_iter (buffer, &iter);

	gtk_text_buffer_insert_with_tags (buffer, &iter, nick, -1,
									  style_tags[VIEWER_STYLE_CHANNEL_NICK], NULL);

	switch (item->data)
	{
	case GIRC_CHANNEL_MODE_ANONYMOUS:
		if (item->arg4)
		{
			gtk_text_buffer_insert (buffer, &iter,
									" has made discussion on this channel anonymous.",
									-1);
		}
		else
		{
			gtk_text_buffer_insert (buffer, &iter,
									" has made discussion on this channel attributable.",
									-1);
		}
		break;
	case GIRC_CHANNEL_MODE_INVITE_ONLY:
		if (item->arg4)
		{
			gtk_text_buffer_insert (buffer, &iter,
									" has made this channel invite-only.", -1);
		}
		else
		{
			gtk_text_buffer_insert (buffer, &iter,
									" has made this channel open to the public.", -1);
		}
		break;

	case GIRC_CHANNEL_MODE_MODERATED:
		if (item->arg4)
		{
			gtk_text_buffer_insert (buffer, &iter,
									" has restricted talking on this channel to operators"
									" and users who have been given voice priviledges.",
									-1);
		}
		else
		{
			gtk_text_buffer_insert (buffer, &iter,
									" has allowed anyone to talk on this channel.", -1);
		}
		break;

	case GIRC_CHANNEL_MODE_NOMSG:
		if (item->arg4)
		{
			gtk_text_buffer_insert (buffer, &iter,
									" has restricted outside users from speaking on this"
									" channel.", -1);
		}
		else
		{
			gtk_text_buffer_insert (buffer, &iter,
									" has allowed outside users to talk on this channel.",
									-1);
		}
		break;

	case GIRC_CHANNEL_MODE_PRIVATE:
		if (item->arg4)
		{
			gtk_text_buffer_insert (buffer, &iter,
									" has set the privacy level to only show that this"
									" channel exists.", -1);
		}
		break;

	case GIRC_CHANNEL_MODE_SECRET:
		if (item->arg4)
		{
			gtk_text_buffer_insert (buffer, &iter,
									" has set the privacy level to completely hide"
									" this channel.", -1);
		}
		else if (item->data & ~GIRC_CHANNEL_MODE_PRIVATE)
		{
			gtk_text_buffer_insert (buffer, &iter,
									" has disabled privacy protections for this channel.",
									-1);
		}
		break;

	case GIRC_CHANNEL_MODE_QUIET:
		if (item->arg4)
		{
			gtk_text_buffer_insert (buffer, &iter,
									" has set the channel not to display users joining"
									" and leaving.", -1);
		}
		else
		{
			gtk_text_buffer_insert (buffer, &iter,
									" has set the channel to display users joining and"
									" leaving.", -1);
		}
		break;

	case GIRC_CHANNEL_MODE_REOP:
		if (item->arg4)
		{
			gtk_text_buffer_insert (buffer, &iter,
									" has set the channel to automatically give out"
									" channel operator permissions if they are lost.",
									-1);
		}
		else
		{
			gtk_text_buffer_insert (buffer, &iter,
									" has set the channel not to automatically give"
									" out channel operator permissions.", -1);
		}
		break;

	case GIRC_CHANNEL_MODE_TOPIC_PROTECT:
		if (item->arg4)
		{
			gtk_text_buffer_insert (buffer, &iter,
									" has restricted the ability to change the topic"
									" to channel operators.", -1);
		}
		else
		{
			gtk_text_buffer_insert (buffer, &iter,
									" has allowed anyone to set the channel topic.", -1);
		}
		break;
	}

	g_free (nick);

	gtk_text_buffer_get_iter_at_mark (buffer, &start_iter, view->ins_mark);
	gtk_text_buffer_apply_tag (buffer, style_tags[VIEWER_STYLE_CHANNEL], &start_iter,
							   &iter);
}


static void
render_channel_user_changed (EscoViewer * view,
							 BufferItem * item)
{
	GtkTextBuffer *buffer;
	GtkTextIter start_iter,
	  iter;
	gchar *nick,
	 *name,
	 *value = NULL;

	nick = girc_convert_to_utf8 (item->arg1, view->encoding);
	g_return_if_fail (nick != NULL);

	name = girc_convert_to_utf8 (item->arg2, view->encoding);
	g_return_if_fail (name != NULL);

	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (view));
	gtk_text_buffer_get_end_iter (buffer, &iter);

	gtk_text_buffer_insert_with_tags (buffer, &iter, nick, -1,
									  style_tags[VIEWER_STYLE_CHANNEL_NICK], NULL);

	switch (item->data)
	{
		/* Unused here */
	case GIRC_CHANNEL_USER_CHANGED_MODES:
		break;

	case GIRC_CHANNEL_USER_CHANGED_JOIN:
		gtk_text_buffer_insert (buffer, &iter, " has joined ", -1);
		gtk_text_buffer_insert_with_tags (buffer, &iter, name, -1,
										  style_tags[VIEWER_STYLE_CHANNEL_NAME], NULL);
		gtk_text_buffer_insert (buffer, &iter, ".", -1);
		break;

	case GIRC_CHANNEL_USER_CHANGED_PART:
		gtk_text_buffer_insert (buffer, &iter, " has left ", -1);
		gtk_text_buffer_insert_with_tags (buffer, &iter, name, -1,
										  style_tags[VIEWER_STYLE_CHANNEL_NAME], NULL);

		value = girc_convert_to_utf8 (item->arg3, view->encoding);
		if (value != NULL)
		{
			gtk_text_buffer_insert (buffer, &iter, " (Reason: ", -1);
			gtk_text_buffer_insert_with_tags (buffer, &iter, value, -1,
											  style_tags[VIEWER_STYLE_CHANNEL_VALUE],
											  NULL);
			gtk_text_buffer_insert (buffer, &iter, ").", -1);
		}
		else
		{
			gtk_text_buffer_insert (buffer, &iter, ".", -1);
		}
		break;

	case GIRC_CHANNEL_USER_CHANGED_KICKED:
		gtk_text_buffer_insert (buffer, &iter, " has been kicked out by ", -1);
		gtk_text_buffer_insert_with_tags (buffer, &iter, name, -1,
										  style_tags[VIEWER_STYLE_CHANNEL_NAME], NULL);

		value = girc_convert_to_utf8 (item->arg3, view->encoding);
		if (value != NULL)
		{
			gtk_text_buffer_insert (buffer, &iter, " (Reason: ", -1);
			gtk_text_buffer_insert_with_tags (buffer, &iter, value, -1,
											  style_tags[VIEWER_STYLE_CHANNEL_VALUE],
											  NULL);
			gtk_text_buffer_insert (buffer, &iter, ").", -1);
		}
		else
		{
			gtk_text_buffer_insert (buffer, &iter, ".", -1);
		}
		break;

	case GIRC_CHANNEL_USER_CHANGED_QUIT:
		gtk_text_buffer_insert (buffer, &iter, " has quit IRC", -1);

		value = girc_convert_to_utf8 (item->arg3, view->encoding);
		if (value != NULL)
		{
			gtk_text_buffer_insert (buffer, &iter, " (Reason: ", -1);
			gtk_text_buffer_insert_with_tags (buffer, &iter, value, -1,
											  style_tags[VIEWER_STYLE_CHANNEL_VALUE],
											  NULL);
			gtk_text_buffer_insert (buffer, &iter, ").", -1);
		}
		else
		{
			gtk_text_buffer_insert (buffer, &iter, ".", -1);
		}
		break;

	case GIRC_CHANNEL_USER_CHANGED_NICK:
		gtk_text_buffer_insert (buffer, &iter, " is now known as \"", -1);
		gtk_text_buffer_insert_with_tags (buffer, &iter, name, -1,
										  style_tags[VIEWER_STYLE_CHANNEL_VALUE], NULL);
		gtk_text_buffer_insert (buffer, &iter, "\".", -1);
		break;

	default:
		g_assert_not_reached ();
		break;
	}

	g_free (nick);
	g_free (name);
	g_free (value);

	gtk_text_buffer_get_iter_at_mark (buffer, &start_iter, view->ins_mark);
	gtk_text_buffer_apply_tag (buffer, style_tags[VIEWER_STYLE_CHANNEL], &start_iter,
							   &iter);
}


static void
render_channel_user_modes_changed (EscoViewer * view,
								   BufferItem * item)
{
	GtkTextBuffer *buffer;
	GtkTextIter start_iter,
	  iter;
	gchar *nick,
	 *name;

	nick = girc_convert_to_utf8 (item->arg1, view->encoding);
	g_return_if_fail (nick != NULL);

	name = girc_convert_to_utf8 (item->arg2, view->encoding);
	g_return_if_fail (name != NULL);

	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (view));
	gtk_text_buffer_get_end_iter (buffer, &iter);

	gtk_text_buffer_insert_with_tags (buffer, &iter, nick, -1,
									  style_tags[VIEWER_STYLE_CHANNEL_NICK], NULL);

	if (item->arg4)
	{
		gtk_text_buffer_insert (buffer, &iter, " has given", -1);
	}
	else
	{
		gtk_text_buffer_insert (buffer, &iter, " has taken", -1);
	}

	switch (item->data)
	{
	case GIRC_CHANNEL_USER_MODE_CREATOR:
		gtk_text_buffer_insert (buffer, &iter, " channel creator permissions", -1);
		break;
	case GIRC_CHANNEL_USER_MODE_OPS:
		gtk_text_buffer_insert (buffer, &iter, " channel operator permissions", -1);
		break;
	case GIRC_CHANNEL_USER_MODE_HALFOPS:
		gtk_text_buffer_insert (buffer, &iter, " channel half-operator permissions", -1);
		break;
	case GIRC_CHANNEL_USER_MODE_VOICE:
		gtk_text_buffer_insert (buffer, &iter, " unfettered speaking ability", -1);
		break;
	default:
		g_assert_not_reached ();
		break;
	}

	if (item->arg4)
	{
		gtk_text_buffer_insert (buffer, &iter, " to ", -1);
	}
	else
	{
		gtk_text_buffer_insert (buffer, &iter, " from ", -1);
	}

	gtk_text_buffer_insert_with_tags (buffer, &iter, name, -1,
									  style_tags[VIEWER_STYLE_CHANNEL_NICK], NULL);

	gtk_text_buffer_insert (buffer, &iter, ".", -1);

	gtk_text_buffer_get_iter_at_mark (buffer, &start_iter, view->ins_mark);
	gtk_text_buffer_apply_tag (buffer, style_tags[VIEWER_STYLE_CHANNEL], &start_iter,
							   &iter);

	g_free (nick);
	g_free (name);
}


static void
render_server (EscoViewer * view,
			   BufferItem * item)
{
	GtkTextBuffer *buffer;
	GtkTextIter iter,
	  start_iter;
	gchar *msg,
	 *servername;

	servername = girc_convert_to_utf8 (item->arg1, view->encoding);
	g_return_if_fail (servername != NULL);

	msg = girc_convert_to_utf8 (item->arg2, view->encoding);

	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (view));
	gtk_text_buffer_get_end_iter (buffer, &start_iter);
	gtk_text_buffer_get_end_iter (buffer, &iter);

	gtk_text_buffer_insert_with_tags (buffer, &iter, servername, -1,
									  style_tags[VIEWER_STYLE_SERVER_NAME], NULL);

	gtk_text_buffer_insert (buffer, &iter, ": ", 2);
	render_msg_to_buffer (GTK_TEXT_VIEW (view), &iter, msg);

	gtk_text_buffer_get_iter_at_mark (buffer, &start_iter, view->ins_mark);
	gtk_text_buffer_apply_tag (buffer, style_tags[VIEWER_STYLE_SERVER],
							   &start_iter, &iter);

	g_free (servername);
	g_free (msg);
}


static void
render_error (EscoViewer * view,
			  BufferItem * item)
{
	GtkTextBuffer *buffer;
	GtkTextIter iter,
	  start_iter;
	gchar *msg;

	g_return_if_fail (view != NULL);
	g_return_if_fail (ESCO_IS_VIEWER (view));
	g_return_if_fail (item->arg1 != NULL);

	msg = girc_convert_to_utf8 (item->arg1, view->encoding);
	g_return_if_fail (msg != NULL);

	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (view));
	gtk_text_buffer_get_end_iter (buffer, &iter);

	gtk_text_buffer_insert (buffer, &iter, _("Error:"), -1);
	gtk_text_buffer_insert (buffer, &iter, " ", 1);

	gtk_text_buffer_get_end_iter (buffer, &iter);
	gtk_text_buffer_move_mark (buffer, view->ins_mark, &iter);

	render_msg_to_buffer (GTK_TEXT_VIEW (view), &iter, msg);

	gtk_text_buffer_get_iter_at_mark (buffer, &start_iter, view->ins_mark);
	gtk_text_buffer_apply_tag (buffer, style_tags[VIEWER_STYLE_ERROR],
							   &start_iter, &iter);

	g_free (msg);
}


static void
render_status (EscoViewer * view,
			   BufferItem * item)
{
	GtkTextBuffer *buffer;
	GtkTextIter iter,
	  start_iter;

	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (view));
	gtk_text_buffer_get_end_iter (buffer, &iter);

	/* We don't need to do charset/encoding conversion here, because our status
	   messages will always be in UTF-8 format -- translators, this means you :-) */
	gtk_text_buffer_insert (buffer, &iter, GENERIC_NAME, -1);
	gtk_text_buffer_insert (buffer, &iter, ": ", 2);

	gtk_text_buffer_get_end_iter (buffer, &iter);
	gtk_text_buffer_move_mark (buffer, view->ins_mark, &iter);

	render_msg_to_buffer (GTK_TEXT_VIEW (view), &iter, item->arg1);

	gtk_text_buffer_get_iter_at_mark (buffer, &start_iter, view->ins_mark);
	gtk_text_buffer_apply_tag (buffer, timestamp_tag, &start_iter, &iter);
}


static void
render_cardinal (EscoViewer * view,
				 BufferItem * item)
{
	GtkTextBuffer *buffer;
	GtkTextIter iter,
	  start_iter;

	/* We don't need to do charset/encoding conversion here, because our status
	   messages will always be in UTF-8 format -- translators, this means you :-) */

	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (view));
	gtk_text_buffer_get_end_iter (buffer, &iter);

	gtk_text_buffer_move_mark (buffer, view->ins_mark, &iter);

	render_msg_to_buffer (GTK_TEXT_VIEW (view), &iter, item->arg1);

	gtk_text_buffer_get_iter_at_mark (buffer, &start_iter, view->ins_mark);
	gtk_text_buffer_apply_tag (buffer, cardinal_tag, &start_iter, &iter);
}


static inline void
render_buffer_item (EscoViewer * view,
					BufferItem * item,
					gboolean set_pester)
{
	GtkTextBuffer *buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (view));
	GtkTextIter iter;
	gboolean to_me;

	switch (item->type)
	{
	case ESCO_VIEWER_MESSAGE_MY_MSG:
		render_my_msg (view, item);
		break;
	case ESCO_VIEWER_MESSAGE_MY_ACTION:
		render_my_action (view, item);
		break;
	case ESCO_VIEWER_MESSAGE_MY_NOTICE:
		// render_my_notice (view, item);
		break;
	case ESCO_VIEWER_MESSAGE_NICK_CHANGED:
		break;

	case ESCO_VIEWER_MESSAGE_MSG:
		to_me = render_msg (view, item);
		if (set_pester)
		{
			if (to_me)
				set_pester_level (view, ESCO_PESTER_MSG);
			else
				set_pester_level (view, ESCO_PESTER_TEXT);
		}
		break;
	case ESCO_VIEWER_MESSAGE_ACTION:
		to_me = render_action (view, item);
		if (set_pester)
		{
			if (to_me)
				set_pester_level (view, ESCO_PESTER_MSG);
			else
				set_pester_level (view, ESCO_PESTER_TEXT);
		}
		break;
	case ESCO_VIEWER_MESSAGE_NOTICE:
		to_me = render_notice (view, item);
		if (set_pester)
		{
			if (to_me)
				set_pester_level (view, ESCO_PESTER_MSG);
			else
				set_pester_level (view, ESCO_PESTER_TEXT);
		}
		break;

	case ESCO_VIEWER_MESSAGE_USER_CHANGED:
		render_user_changed (view, item);

		if (set_pester)
			set_pester_level (view, ESCO_PESTER_IRC);
		break;

	case ESCO_VIEWER_MESSAGE_CHANNEL_CHANGED:
		render_channel_changed (view, item);

		if (set_pester)
			set_pester_level (view, ESCO_PESTER_IRC);
		break;
	case ESCO_VIEWER_MESSAGE_CHANNEL_MODES_CHANGED:
		render_channel_modes_changed (view, item);

		if (set_pester)
			set_pester_level (view, ESCO_PESTER_IRC);
		break;
	case ESCO_VIEWER_MESSAGE_CHANNEL_USER_CHANGED:
		render_channel_user_changed (view, item);

		if (set_pester)
			set_pester_level (view, ESCO_PESTER_IRC);
		break;
	case ESCO_VIEWER_MESSAGE_CHANNEL_USER_MODES_CHANGED:
		render_channel_user_modes_changed (view, item);

		if (set_pester)
			set_pester_level (view, ESCO_PESTER_IRC);
		break;

	case ESCO_VIEWER_MESSAGE_SERVER:
		render_server (view, item);

		if (set_pester)
			set_pester_level (view, ESCO_PESTER_IRC);
		break;
	case ESCO_VIEWER_MESSAGE_ERROR:
		render_error (view, item);

		if (set_pester)
			set_pester_level (view, ESCO_PESTER_IRC);
		break;

	case ESCO_VIEWER_MESSAGE_STATUS:
		render_status (view, item);

		if (set_pester)
			set_pester_level (view, ESCO_PESTER_IRC);
		break;
	case ESCO_VIEWER_MESSAGE_CARDINAL:
		render_cardinal (view, item);

		if (set_pester)
			set_pester_level (view, ESCO_PESTER_IRC);
		break;

	default:
		g_assert_not_reached ();
		break;
	}

	gtk_text_buffer_get_end_iter (buffer, &iter);
	gtk_text_buffer_insert (buffer, &iter, "\n", 1);
	gtk_text_buffer_move_mark (buffer, view->ins_mark, &iter);
}


static void
replay_buffer_list (EscoViewer * view)
{
	GIConv converter;
	GtkTextBuffer *buffer;
	GSList *items;

	/* Just dump out if we've got nothing to replay. */
	if (view->buffer_items == NULL)
		return;

	/* Clear the buffer first */
	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (view));
	gtk_text_buffer_set_text (buffer, "", 0);

	converter = g_iconv_open ("UTF-8", view->encoding);

	if (converter == (GIConv) - 1)
	{
		const EscoEncoding *encoding =
			esco_encoding_get_encoding_by_alias (view->encoding);
		gchar *message =
			g_strdup_printf (_("Your system is not capable of displaying text which uses "
							   "\"%s\" character coding.\n\nPlease select a different "
							   "character coding."), encoding->ui_name);
		gtk_text_buffer_set_text (buffer, message, -1);
		g_free (message);
		return;
	}

	/* Now re-render all the buffered items */
	for (items = view->buffer_items; items != NULL; items = items->next)
	{
		BufferItem *item = BUFFER_ITEM (items->data);

		/* DEBUGGING */
		g_assert (item != NULL && item->type != ESCO_VIEWER_MESSAGE_UNKNOWN);

		/* Render the timestamp, if any */
		if (show_timestamps && item->type != ESCO_VIEWER_MESSAGE_CARDINAL)
			append_timestamp (view, item->time);

		/* The actual message */
		render_buffer_item (view, item, FALSE);
	}

	g_iconv_close (converter);

	gtk_text_view_scroll_to_mark (GTK_TEXT_VIEW (view), view->ins_mark,
								  0.0, TRUE, 0.5, 0.5);
}


static void
view_destroyed (gpointer data,
				GObject * where_view_was)
{
	all_viewers = g_slist_remove (all_viewers, where_view_was);
}


/* GtkTextView Callbacks */
static void
esco_viewer_populate_popup (GtkTextView * text_view,
							GtkMenu * menu)
{
	GtkTextBuffer *buffer = gtk_text_view_get_buffer (text_view);
	GtkWidget *menuitem,
	 *image;
	GtkTextIter iter;

	gtk_text_buffer_get_iter_at_mark (buffer, &iter, gtk_text_buffer_get_insert (buffer));

	if (gtk_text_iter_has_tag (&iter, style_tags[VIEWER_STYLE_LINK]))
	{
		menuitem = gtk_image_menu_item_new_with_mnemonic (_("_Open Link"));
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
		gtk_widget_show (menuitem);

		image = gtk_image_new_from_stock (GTK_STOCK_JUMP_TO, GTK_ICON_SIZE_MENU);
		gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (menuitem), image);
		gtk_widget_show (image);

		menuitem = gtk_image_menu_item_new_with_mnemonic (_("_Copy Link Location"));
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
		gtk_widget_show (menuitem);

		image = gtk_image_new_from_stock (GTK_STOCK_COPY, GTK_ICON_SIZE_MENU);
		gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (menuitem), image);
		gtk_widget_show (image);

		menuitem = gtk_separator_menu_item_new ();
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
		gtk_widget_show (menuitem);
	}

	if (GTK_TEXT_VIEW_CLASS (parent_class)->populate_popup != NULL)
		(*GTK_TEXT_VIEW_CLASS (parent_class)->populate_popup) (text_view, menu);
}


/* GtkWidget Callbacks */
static void
esco_viewer_realize (GtkWidget * widget) 
{
	if (GTK_WIDGET_CLASS (parent_class)->realize)
		(*GTK_WIDGET_CLASS (parent_class)->realize) (widget);

	gdk_window_set_events (widget->window,
						   gdk_window_get_events (widget->window)
						   | GDK_POINTER_MOTION_MASK);
}


static void
esco_viewer_state_changed (GtkWidget * widget,
						   GtkStateType state)
{
	if (state == GTK_STATE_NORMAL || state == GTK_STATE_INSENSITIVE)
		set_initial_style (ESCO_VIEWER (widget));

	if (GTK_WIDGET_CLASS (parent_class)->state_changed)
		(*GTK_WIDGET_CLASS (parent_class)->state_changed) (widget, state);
}


static gboolean
esco_viewer_motion_notify_event (GtkWidget * widget,
								 GdkEventMotion * event)
{
	static GdkCursor *hand_cursor = NULL;
	static GdkCursor *ibar_cursor = NULL;
	GtkTextView *view = GTK_TEXT_VIEW (widget);
	GtkTextWindowType type;
	GtkTextIter iter;
	GdkWindow *win;
	gint x,
	  y,
	  buf_x,
	  buf_y;
	gboolean retval = FALSE;

	retval = GTK_WIDGET_CLASS (parent_class)->motion_notify_event (widget, event);
	type = gtk_text_view_get_window_type (view, event->window);
	win = gtk_text_view_get_window (view, type);

	gdk_window_get_pointer (win, &x, &y, NULL);

	if (type != GTK_TEXT_WINDOW_TEXT)
		return retval;

	gtk_text_view_window_to_buffer_coords (view, type, x, y, &buf_x, &buf_y);
	gtk_text_view_get_iter_at_location (view, &iter, buf_x, buf_y);

	if (!hand_cursor)
		hand_cursor = gdk_cursor_new (GDK_HAND2);

	if (!ibar_cursor)
		ibar_cursor = gdk_cursor_new (GDK_XTERM);

	if (gtk_text_iter_has_tag (&iter, style_tags[VIEWER_STYLE_LINK]))
	{
		gdk_window_set_cursor (win, hand_cursor);
	}
	else
	{
		gdk_window_set_cursor (win, ibar_cursor);
	}

	return retval;
}


/* GObject Callbacks */
static void
esco_viewer_get_property (GObject * object,
						  guint param_id,
						  GValue * value,
						  GParamSpec * pspec)
{
	EscoViewer *view = ESCO_VIEWER (object);

	switch (param_id)
	{
	case PROP_MODE:
		g_value_set_enum (value, view->mode);
		break;
	case PROP_NICK:
		g_value_set_string (value, view->nick);
		break;
	case PROP_ENCODING:
		g_value_set_string (value, view->encoding);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}


static void
esco_viewer_set_property (GObject * object,
						  guint param_id,
						  const GValue * value,
						  GParamSpec * pspec)
{
	EscoViewer *view = ESCO_VIEWER (object);

	switch (param_id)
	{
	case PROP_MODE:
		esco_viewer_set_mode (view, g_value_get_enum (value));
		break;
	case PROP_NICK:
		esco_viewer_set_nick (view, g_value_get_string (value));
		break;
	case PROP_ENCODING:
		esco_viewer_set_encoding (view, g_value_get_string (value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}


static void
esco_viewer_finalize (GObject * object)
{
	EscoViewer *view = ESCO_VIEWER (object);

	g_free (view->encoding);
	g_free (view->plain_nick);
	g_free (view->nick);

	pango_tab_array_free (view->tabs);

	for (; view->buffer_items != NULL;
		 view->buffer_items = g_slist_remove (view->buffer_items,
											  view->buffer_items->data))
	{
		free_buffer_item (BUFFER_ITEM (view->buffer_items->data));
	}
}


/* GType Functions */
static void
esco_viewer_class_init (EscoViewerClass * class)
{
	GObjectClass *object_class = G_OBJECT_CLASS (class);
	GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (class);
	GtkTextViewClass *text_view_class = GTK_TEXT_VIEW_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	object_class->set_property = esco_viewer_set_property;
	object_class->get_property = esco_viewer_get_property;
	object_class->finalize = esco_viewer_finalize;

	widget_class->realize = esco_viewer_realize;
	widget_class->motion_notify_event = esco_viewer_motion_notify_event;
	widget_class->state_changed = esco_viewer_state_changed;

	text_view_class->populate_popup = esco_viewer_populate_popup;

	class->pester = NULL;
	class->link_clicked = NULL;

	table = create_global_tags_table ();

	show_timestamps = prefs_get_viewer_show_timestamps ();
	show_pixbuf_smileys = prefs_get_use_image_smileys ();
	uri_prefixes = prefs_get_uri_prefixes ();

	g_object_class_install_property (object_class,
									 PROP_MODE,
									 g_param_spec_enum ("mode",
														_("Viewer Mode"),
														_("Whether this viewer "
														  "is a query, "
														  "channel, or server"),
														ESCO_TYPE_CHAT_MODE_TYPE,
														ESCO_CHAT_MODE_SERVER,
														(G_PARAM_READWRITE
														 | G_PARAM_CONSTRUCT)));
	g_object_class_install_property (object_class, PROP_NICK,
									 g_param_spec_string ("nick",
														  _("Viewer Nickname"),
														  _("The nickname to "
															"highlight text for"),
														  NULL,
														  (G_PARAM_READWRITE
														   | G_PARAM_CONSTRUCT)));
	g_object_class_install_property (object_class, PROP_ENCODING,
									 g_param_spec_string ("encoding",
														  _("Viewer Encoding"),
														  _("The encoding to "
															"use for translating "
															"incoming strings to UTF-8."),
														  "UTF-8",
														  (G_PARAM_READWRITE
														   | G_PARAM_CONSTRUCT)));
	esco_viewer_signals[SIGNAL_PESTER] =
		g_signal_new ("pester", G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_LAST,
					  G_STRUCT_OFFSET (EscoViewerClass, pester), NULL, NULL,
					  g_cclosure_marshal_VOID__ENUM, G_TYPE_NONE, 1,
					  ESCO_TYPE_PESTER_TYPE);
	esco_viewer_signals[SIGNAL_LINK_CLICKED] =
		g_signal_new ("link-clicked", G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_LAST,
					  G_STRUCT_OFFSET (EscoViewerClass, link_clicked), NULL,
					  NULL, g_cclosure_marshal_VOID__STRING, G_TYPE_NONE, 1,
					  G_TYPE_STRING);
}


static void
esco_viewer_instance_init (EscoViewer * view)
{
	GtkTextView *text;
	GtkTextBuffer *buffer;
	GtkTextIter iter;

	view->nick = NULL;
	view->plain_nick = NULL;
	view->tabs = pango_tab_array_new (2, TRUE);
	view->nick_pixel_size = 0;
	view->time_pixel_size = 0;

	buffer = gtk_text_buffer_new (table);
	text = GTK_TEXT_VIEW (view);

	gtk_text_view_set_buffer (text, buffer);

	set_initial_style (view);

	gtk_text_view_set_justification (text, GTK_JUSTIFY_LEFT);
	gtk_text_view_set_wrap_mode (text, GTK_WRAP_WORD);
	gtk_text_view_set_editable (text, FALSE);
	gtk_text_view_set_cursor_visible (text, FALSE);
	gtk_text_view_set_left_margin (text, 2);
	gtk_text_view_set_right_margin (text, 2);
	gtk_text_view_set_pixels_inside_wrap (text, 0);
	gtk_text_view_set_pixels_above_lines (text, 0);
	gtk_text_view_set_tabs (GTK_TEXT_VIEW (view), view->tabs);
	gtk_text_buffer_get_end_iter (buffer, &iter);

	view->ins_mark = gtk_text_buffer_create_mark (buffer, "gnomechat-mark", &iter, TRUE);

	all_viewers = g_slist_append (all_viewers, view);

	g_object_weak_ref (G_OBJECT (view), view_destroyed, NULL);
}


/* PUBLIC API */
GType
esco_viewer_get_type (void)
{
	static GType type = 0;

	if (!type)
	{
		static const GTypeInfo info = {
			sizeof (EscoViewerClass),
			NULL,				/* base_init */
			NULL,				/* base_finalize */
			(GClassInitFunc) esco_viewer_class_init,
			NULL,				/* class_finalize */
			NULL,				/* class_data */
			sizeof (EscoViewer),
			0,					/* n_preallocs */
			(GInstanceInitFunc) esco_viewer_instance_init,
		};

		type = g_type_register_static (GTK_TYPE_TEXT_VIEW, "EscoViewer", &info, 0);
	}

	return type;
}


GtkWidget *
esco_viewer_new (EscoChatModeType mode,
				 const gchar * nick)
{
	return g_object_new (ESCO_TYPE_VIEWER, "mode", mode, "nick", nick, NULL);
}


void
esco_viewer_set_mode (EscoViewer * view,
					  EscoChatModeType mode)
{
	g_return_if_fail (view != NULL);
	g_return_if_fail (ESCO_IS_VIEWER (view));

	view->mode = mode;

	g_object_notify (G_OBJECT (view), "mode");
}


void
esco_viewer_set_nick (EscoViewer * view,
					  const gchar * nick)
{
	g_return_if_fail (view != NULL);
	g_return_if_fail (ESCO_IS_VIEWER (view));

	if (view->nick)
		g_free (view->nick);
	if (view->plain_nick)
		g_free (view->plain_nick);

	view->nick = g_strconcat ("<", nick, "> ", NULL);
	view->plain_nick = g_strdup (nick);

	g_object_notify (G_OBJECT (view), "nick");
}


void
esco_viewer_set_encoding (EscoViewer * view,
						  const gchar * encoding)
{
	g_return_if_fail (view != NULL);
	g_return_if_fail (ESCO_IS_VIEWER (view));
	g_return_if_fail (esco_encoding_is_valid (encoding));

	if (view->encoding)
		g_free (view->encoding);

	view->encoding = g_strdup (encoding);

	replay_buffer_list (view);

	g_object_notify (G_OBJECT (view), "encoding");
}


void
esco_viewer_append_line (EscoViewer * view,
						 EscoViewerMessageType type,
						 guint data,
						 const gchar * arg1,
						 const gchar * arg2,
						 const gchar * arg3,
						 gboolean arg4)
{
	BufferItem *item;

	g_return_if_fail (view != NULL);
	g_return_if_fail (ESCO_IS_VIEWER (view));
	g_return_if_fail (type >= ESCO_VIEWER_MESSAGE_MY_MSG
					  && type < ESCO_VIEWER_MESSAGE_UNKNOWN);
	g_return_if_fail (arg1 != NULL);

	item = g_new0 (BufferItem, 1);

	item->type = type;
	item->time = time (NULL);
	item->data = data;
	item->arg1 = g_strdup (arg1);
	item->arg2 = g_strdup (arg2);
	item->arg3 = g_strdup (arg3);
	item->arg4 = arg4;

	view->buffer_items = g_slist_append (view->buffer_items, item);

	if (show_timestamps && type != ESCO_VIEWER_MESSAGE_CARDINAL)
		append_timestamp (view, item->time);

	render_buffer_item (view, item, TRUE);

	if (g_slist_length (view->buffer_items) > MAX_SCROLLBACK_LINES)
	{
		GtkTextBuffer *buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (view));
		GtkTextIter start,
		  end;

		free_buffer_item (BUFFER_ITEM (view->buffer_items->data));
		view->buffer_items = g_slist_remove_link (view->buffer_items, view->buffer_items);

		gtk_text_buffer_get_iter_at_line (buffer, &start, 0);
		gtk_text_buffer_get_iter_at_line (buffer, &end, 1);

		gtk_text_buffer_delete (buffer, &start, &end);
	}

	gtk_text_view_scroll_to_mark (GTK_TEXT_VIEW (view), view->ins_mark,
								  0.0, TRUE, 0.5, 0.5);
}


void
esco_viewer_clear (EscoViewer * view)
{
	GSList *list;
	GtkTextBuffer *buffer;
	GtkTextIter start,
	  end;

	g_return_if_fail (ESCO_IS_VIEWER (view));

	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (view));
	gtk_text_buffer_get_bounds (buffer, &start, &end);
	gtk_text_buffer_delete (buffer, &start, &end);

	for (list = view->buffer_items; list != NULL; list = g_slist_remove_link (list, list))
	{
		free_buffer_item (list->data);
	}
}


void
esco_viewer_clear_pester (EscoViewer * view)
{
	view->pester_level = ESCO_PESTER_NONE;
}


void
esco_viewer_find (EscoViewer * viewer,
				  const gchar * search_str,
				  gboolean case_insensitive)
{
	GtkTextView *view;
	GtkTextBuffer *buffer;
	GtkTextMark *mark;
	GtkTextIter *search_start_iter = NULL,
	  iter1,
	  iter2;

	g_return_if_fail (viewer != NULL);
	g_return_if_fail (ESCO_IS_VIEWER (viewer));
	g_return_if_fail (search_str != NULL && search_str[0] != '\0');

	view = GTK_TEXT_VIEW (viewer);
	buffer = gtk_text_view_get_buffer (view);

	/* First, get our selection, if one exists. */
	mark = gtk_text_buffer_get_selection_bound (buffer);

	/* Then, get the selection bounds */
	if (mark != NULL && gtk_text_buffer_get_selection_bounds (buffer, &iter1, &iter2))
	{
		/* Then get the text contained in the selection */
		gchar *text = gtk_text_buffer_get_text (buffer, &iter1, &iter2, FALSE);

		if (text != NULL)
		{
			/* If the text in the selection matches the search_str, start 
			   searching backwards from before the selection */
			if (text[0] != '\0'
				&& ((case_insensitive && girc_g_utf8_strcaseequal (search_str, text))
					|| (!case_insensitive && strcmp (search_str, text) == 0)))
			{
				search_start_iter = gtk_text_iter_copy (&iter1);
			}

			g_free (text);
		}
	}

	/* Otherwise, start searching backwards from the end of the buffer */
	if (search_start_iter == NULL)
	{
		gtk_text_buffer_get_end_iter (buffer, &iter1);
		search_start_iter = gtk_text_iter_copy (&iter1);
	}

	/* If we find a match, set the selection to the new match, and scroll to
	   the new match */
	if (gtk_text_iter_backward_search (search_start_iter, search_str,
									   (GTK_TEXT_SEARCH_VISIBLE_ONLY),
									   &iter1, &iter2, NULL))
	{
		gtk_text_buffer_move_mark (buffer, mark, &iter1);

		mark = gtk_text_buffer_get_insert (buffer);
		gtk_text_buffer_move_mark (buffer, mark, &iter2);

		gtk_text_view_scroll_mark_onscreen (view, mark);
	}

	gtk_text_iter_free (search_start_iter);
}


void
esco_viewers_set_show_timestamps (gboolean show)
{
	GSList *viewers;

	show_timestamps = show;

	for (viewers = all_viewers; viewers != NULL; viewers = viewers->next)
	{
		replay_buffer_list (ESCO_VIEWER (viewers->data));
	}
}


void
esco_viewers_set_use_image_smileys (gboolean show)
{
	GSList *viewers;

	show_pixbuf_smileys = show;
	for (viewers = all_viewers; viewers != NULL; viewers = viewers->next)
	{
		replay_buffer_list (ESCO_VIEWER (viewers->data));
	}
}


void
esco_viewers_set_font (const gchar * font)
{
	GSList *viewers;
	PangoFontDescription *font_desc = NULL;
	GtkRcStyle *rc_style;

	if (all_viewers == NULL || all_viewers->data == NULL
		|| !GTK_IS_WIDGET (all_viewers->data))
		return;

	if (util_str_is_not_empty (font))
	{
		font_desc = pango_font_description_from_string (font);

		for (viewers = all_viewers; viewers != NULL; viewers = viewers->next)
		{
			gtk_widget_modify_font (GTK_WIDGET (viewers->data), font_desc);
			update_tabs (ESCO_VIEWER (viewers->data));
		}

		pango_font_description_free (font_desc);
	}
	else
	{
		for (viewers = all_viewers; viewers != NULL; viewers = viewers->next)
		{
			rc_style = gtk_widget_get_modifier_style (GTK_WIDGET (viewers->data));

			if (rc_style->font_desc)
			{
				pango_font_description_free (rc_style->font_desc);
				rc_style->font_desc = NULL;
			}

			gtk_widget_modify_style (GTK_WIDGET (viewers->data), rc_style);
			update_tabs (ESCO_VIEWER (viewers->data));
		}
	}
}


void
esco_viewers_set_fg (const gchar * fg_color)
{
	GSList *viewers;
	GdkColor fg,
	  bg;
	gchar *bg_color;
	gboolean valid_fg,
	  valid_bg;

	if (all_viewers == NULL || all_viewers->data == NULL
		|| !GTK_IS_WIDGET (all_viewers->data))
		return;

	valid_fg = (util_str_is_not_empty (fg_color) && gdk_color_parse (fg_color, &fg));

	bg_color = prefs_get_viewer_bg ();
	valid_bg = (util_str_is_not_empty (bg_color) && gdk_color_parse (bg_color, &bg));

	for (viewers = all_viewers; viewers != NULL; viewers = viewers->next)
	{
		update_global_colors (ESCO_VIEWER (viewers->data), valid_fg, &fg, valid_bg, &bg);
	}

	g_free (bg_color);
}


void
esco_viewers_set_bg (const gchar * bg_color)
{
	GSList *viewers;
	GdkColor bg,
	  fg;
	gboolean valid_fg,
	  valid_bg;
	gchar *fg_color;

	if (all_viewers == NULL || all_viewers->data == NULL
		|| !GTK_IS_WIDGET (all_viewers->data))
		return;

	fg_color = prefs_get_viewer_fg ();
	valid_fg = (util_str_is_not_empty (fg_color) && gdk_color_parse (fg_color, &fg));

	valid_bg = (util_str_is_not_empty (bg_color) && gdk_color_parse (bg_color, &bg));

	for (viewers = all_viewers; viewers != NULL; viewers = viewers->next)
	{
		update_global_colors (ESCO_VIEWER (viewers->data), valid_fg, &fg, valid_bg, &bg);
	}
	g_free (fg_color);
}


void
esco_viewers_set_style_fg (ViewerStyleType style_type,
						   const gchar * fg_color)
{
	gchar *global_fg;

	g_return_if_fail (style_type >= VIEWER_STYLE_FIRST && style_type < VIEWER_STYLE_LAST);

	global_fg = prefs_get_viewer_fg ();

	if (util_str_is_not_empty (fg_color) && g_ascii_strcasecmp (fg_color, global_fg) != 0)
		g_object_set (style_tags[style_type], "foreground", fg_color, NULL);
	else
		g_object_set (style_tags[style_type], "foreground-set", FALSE, NULL);

	g_free (global_fg);
}


void
esco_viewers_set_style_bg (ViewerStyleType style_type,
						   const gchar * bg_color)
{
	gchar *global_bg;

	g_return_if_fail (style_type >= VIEWER_STYLE_FIRST && style_type < VIEWER_STYLE_LAST);

	global_bg = prefs_get_viewer_bg ();

	if (util_str_is_not_empty (bg_color) && g_ascii_strcasecmp (bg_color, global_bg) != 0)
		g_object_set (style_tags[style_type], "background", bg_color, NULL);
	else
		g_object_set (style_tags[style_type], "background-set", FALSE, NULL);

	g_free (global_bg);
}


void
esco_viewers_set_style_bold (ViewerStyleType style_type,
							 gboolean bold)
{
	g_return_if_fail (style_type >= VIEWER_STYLE_FIRST && style_type < VIEWER_STYLE_LAST);

	if (bold)
		g_object_set (style_tags[style_type], "weight", PANGO_WEIGHT_BOLD, NULL);
	else
		g_object_set (style_tags[style_type], "weight-set", FALSE, NULL);
}


void
esco_viewers_set_style_italic (ViewerStyleType style_type,
							   gboolean italic)
{
	g_return_if_fail (style_type >= VIEWER_STYLE_FIRST && style_type < VIEWER_STYLE_LAST);

	if (italic)
		g_object_set (style_tags[style_type], "style", PANGO_STYLE_ITALIC, NULL);
	else
		g_object_set (style_tags[style_type], "style-set", FALSE, NULL);
}


void
esco_viewers_set_style_uline (ViewerStyleType style_type,
							  gboolean uline)
{
	g_return_if_fail (style_type >= VIEWER_STYLE_FIRST && style_type < VIEWER_STYLE_LAST);

	if (uline)
		g_object_set (style_tags[style_type], "underline", PANGO_UNDERLINE_SINGLE, NULL);
	else
		g_object_set (style_tags[style_type], "underline-set", FALSE, NULL);
}


void
esco_viewers_set_uri_prefixes (GSList * prefixes)
{
	if (uri_prefixes != NULL)
		girc_g_slist_deep_free (uri_prefixes, g_free);

	uri_prefixes = prefixes;
}
