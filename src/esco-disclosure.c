/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Author: Gregory Merchan <merchan@phys.lsu.edu>
 *
 *  Copyright 2003 Gregory Merchan
 *
 *  Original Authors: Iain Holmes <iain@ximian.com>
 *                    James M. Cape (warnings fixes)
 *
 *  Original Copyright 2002 Iain Holmes
 *  Original Copyright 2002 James M. Cape
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include "gnomechat.h"
#include "esco-disclosure.h"

#define EXPANDER_SIZE 10


struct _EscoDisclosurePrivate
{
	guint32 expand_id;

	GtkExpanderStyle style:3;
	gboolean opening:1;
};


static gpointer parent_class = NULL;


/* UTILITY FUNCTIONS */
static void
get_x_y (EscoDisclosure * disclosure,
		 guint * x,
		 guint * y,
		 GtkStateType * state_type)
{
	GtkCheckButton *check_button;
	GtkWidget *widget = GTK_WIDGET (disclosure);
	GtkBin *bin = GTK_BIN (disclosure);
	guint indicator_size = 0,
	  focus_width,
	  focus_pad,
	  width;
	gboolean interior_focus;

	if (GTK_WIDGET_VISIBLE (disclosure) && GTK_WIDGET_MAPPED (disclosure))
	{
		check_button = GTK_CHECK_BUTTON (disclosure);

		gtk_widget_style_get (widget,
							  "interior_focus", &interior_focus,
							  "focus-line-width", &focus_width,
							  "focus-padding", &focus_pad, NULL);

		*state_type = GTK_WIDGET_STATE (widget);
		if ((*state_type != GTK_STATE_NORMAL) && (*state_type != GTK_STATE_PRELIGHT))
		{
			*state_type = GTK_STATE_NORMAL;
		}

		if (bin->child)
		{
			width =
				bin->child->allocation.x - widget->allocation.x -
				(2 * GTK_CONTAINER (widget)->border_width);
		}
		else
		{
			width = widget->allocation.width;
		}

		*x = widget->allocation.x + (width) / 2;
		*y = widget->allocation.y + widget->allocation.height / 2;

		if (interior_focus == FALSE)
		{
			*x += focus_width + focus_pad;
		}

		*state_type =
			(GTK_WIDGET_STATE (widget) ==
			 (GTK_STATE_ACTIVE ? GTK_STATE_NORMAL : GTK_WIDGET_STATE (widget)));

		if (gtk_widget_get_direction (widget) == GTK_TEXT_DIR_RTL)
		{
			*x = widget->allocation.x + widget->allocation.width - (indicator_size + *x -
																	widget->allocation.x);
		}
	}
	else
	{
		*x = 0;
		*y = 0;
		*state_type = GTK_STATE_NORMAL;
	}
}


static gboolean
expand_collapse_timeout (GtkWidget * widget)
{
	EscoDisclosure *disclosure = ESCO_DISCLOSURE (widget);
	GtkStateType state_type;
	gint x,
	  y;

	gdk_window_invalidate_rect (widget->window, &widget->allocation, TRUE);
	get_x_y (disclosure, &x, &y, &state_type);

	gtk_paint_expander (widget->style,
						widget->window,
						state_type,
						&widget->allocation,
						widget, "disclosure", x, y, disclosure->_priv->style);

	if (disclosure->_priv->opening)
	{
		disclosure->_priv->style++;
	}
	else
	{
		disclosure->_priv->style--;
	}

	if (disclosure->_priv->style == GTK_EXPANDER_COLLAPSED ||
		disclosure->_priv->style == GTK_EXPANDER_EXPANDED)
	{
		return FALSE;
	}

	return TRUE;
}


static void
do_animation (EscoDisclosure * disclosure,
			  gboolean opening)
{
	if (disclosure->_priv->expand_id > 0)
	{
		g_source_remove (disclosure->_priv->expand_id);
	}

	disclosure->_priv->opening = opening;
	disclosure->_priv->expand_id =
		g_timeout_add (50, (GSourceFunc) expand_collapse_timeout, disclosure);
}


static void
esco_disclosure_draw_indicator (GtkCheckButton * check_button,
								GdkRectangle * area)
{
	GtkWidget *widget;
	EscoDisclosure *disclosure;
	GtkStateType state_type;
	gint x,
	  y;

	widget = GTK_WIDGET (check_button);
	disclosure = ESCO_DISCLOSURE (check_button);

	get_x_y (disclosure, &x, &y, &state_type);
	gtk_paint_expander (widget->style,
						widget->window,
						state_type,
						area, widget, "disclosure", x, y, disclosure->_priv->style);
}


static void
esco_disclosure_toggled (GtkToggleButton * toggle_button)
{
	EscoDisclosure *disclosure;

	disclosure = ESCO_DISCLOSURE (toggle_button);

	if (GTK_WIDGET_DRAWABLE (disclosure))
	{
		do_animation (disclosure, gtk_toggle_button_get_active (toggle_button));
	}
	else if (disclosure->_priv->style == GTK_EXPANDER_COLLAPSED)
	{
		disclosure->_priv->style = GTK_EXPANDER_EXPANDED;
	}
	else
	{
		disclosure->_priv->style = GTK_EXPANDER_COLLAPSED;
	}

	if (GTK_TOGGLE_BUTTON_CLASS (parent_class)->toggled != NULL)
		(*GTK_TOGGLE_BUTTON_CLASS (parent_class)->toggled) (toggle_button);
}


static void
esco_disclosure_finalize (GObject * gobject)
{
	EscoDisclosure *disclosure;

	disclosure = ESCO_DISCLOSURE (gobject);
	g_free (disclosure->_priv);

	if (G_OBJECT_CLASS (parent_class)->finalize != NULL)
		(*G_OBJECT_CLASS (parent_class)->finalize) (gobject);
}


static void
esco_disclosure_class_init (EscoDisclosureClass * class)
{
	parent_class = g_type_class_peek_parent (class);

	G_OBJECT_CLASS (class)->finalize = esco_disclosure_finalize;
	GTK_TOGGLE_BUTTON_CLASS (class)->toggled = esco_disclosure_toggled;
	GTK_CHECK_BUTTON_CLASS (class)->draw_indicator = esco_disclosure_draw_indicator;

	gtk_widget_class_install_style_property (GTK_WIDGET_CLASS (class),
											 g_param_spec_uint ("expander-size",
																_("Expander Size"),
																_("Size of the expander "
																  "arrow"),
																0, G_MAXUINT,
																EXPANDER_SIZE,
																G_PARAM_READABLE));
}


static void
esco_disclosure_init (EscoDisclosure * disclosure)
{
	disclosure->_priv = g_new0 (EscoDisclosurePrivate, 1);
}


GType
esco_disclosure_get_type (void)
{
	static GType type = 0;

	if (!type)
	{
		GTypeInfo info = {
			sizeof (EscoDisclosureClass),
			NULL,
			NULL,
			(GClassInitFunc) esco_disclosure_class_init,
			NULL,
			NULL,
			sizeof (EscoDisclosure),
			0,
			(GInstanceInitFunc) esco_disclosure_init
		};

		type = g_type_register_static (GTK_TYPE_CHECK_BUTTON, "EscoDisclosure", &info, 0);
	}

	return type;
}


/**
 * esco_disclosure_new:
 * 
 * Creates a new GtkDisclosure widget.
 * 
 * Returns: a new #EscoDisclosure widget.
 *
 * Since: 2.4
 **/
GtkWidget *
esco_disclosure_new (void)
{
	return GTK_WIDGET (g_object_new (ESCO_TYPE_DISCLOSURE, NULL));
}


GtkWidget *
esco_disclosure_new_with_label (const gchar * label)
{
	return GTK_WIDGET (g_object_new (ESCO_TYPE_DISCLOSURE, "label", label, NULL));
}


GtkWidget *
esco_disclosure_new_with_mnemonic (const gchar * label)
{
	return GTK_WIDGET (g_object_new (ESCO_TYPE_DISCLOSURE, "label", label,
									 "use_underline", TRUE, NULL));
}
