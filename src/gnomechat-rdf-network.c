/*
 *  GnomeChat: src/gnomechat-rdf-network.c
 *
 *  Copyright (c) 2003 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.01234567890
 */

#include "gnomechat.h"

#include <locale.h>

#include "gnomechat-rdf-network.h"

#include "gnomechat-rdf-types.h"
#include "gnomechat-rdf-server.h"
#include "gnomechat-vfs-transfer.h"

#include "utils.h"

#include <libgircclient/girc-utils.h>
#include <libgnomeui/gnome-thumbnail.h>


#define DATA_TRANSFER_IN_PROGRESS(network) (network->_priv->data_xfer != NULL || network->_priv->icon_xfer != NULL)


enum
{
	PROP_0,
	URI,
	DATA_URI,
	DATA_PATH,
	ICON_URI,
	ICON,
	DESCRIPTION,
	WEBSITE,
	ENCODING,
	SERVERS,
	CHANNELS
};

enum
{
	CHANGED,
	LAST_SIGNAL
};


struct _GnomechatRdfNetworkPrivate
{
	/* Properties */
	gchar *uri;

	gchar *data_uri;
	gchar *data_path;

	gchar *icon_uri;
	gchar *icon_path;
	GdkPixbuf *icon;

	GnomechatI18nString *description;
	GnomechatI18nString *website;
	gchar *encoding;

	GSList *servers;
	GSList *channels;

	/* Internals */
	GnomechatVFSTransfer *data_xfer;
	GnomechatVFSTransfer *icon_xfer;

	gboolean data_is_local:1;
	gboolean icon_is_local:1;
};


static gpointer parent_class = NULL;


/* ********************************** *
 *  GnomechatTransferIface Functions  *
 * ********************************** */

static GnomechatTransferType
gnomechat_rdf_network_get_transfer_type (GnomechatRdfNetwork * network)
{
	g_return_val_if_fail (GNOMECHAT_IS_RDF_NETWORK (network), GNOMECHAT_TRANSFER_INVALID);

	return (GNOMECHAT_TRANSFER_DOWNLOAD | GNOMECHAT_TRANSFER_MULTI);
}


static GnomechatTransferStatus
gnomechat_rdf_network_get_status (GnomechatRdfNetwork * network)
{
	g_return_val_if_fail (GNOMECHAT_IS_RDF_NETWORK (network), GNOMECHAT_TRANSFER_STATUS_INVALID);

	if (network->_priv->data_xfer != NULL)
		return gnomechat_transfer_get_status (GNOMECHAT_TRANSFER (network->_priv->data_xfer));
	else if (network->_priv->icon_xfer != NULL)
		return gnomechat_transfer_get_status (GNOMECHAT_TRANSFER (network->_priv->icon_xfer));
	else
		return GNOMECHAT_TRANSFER_STATUS_READY;
}


static GnomeVFSResult
gnomechat_rdf_network_get_error (GnomechatRdfNetwork * network)
{
	g_return_val_if_fail (GNOMECHAT_IS_RDF_NETWORK (network), GNOME_VFS_ERROR_BAD_PARAMETERS);

	if (network->_priv->data_xfer != NULL)
		return gnomechat_transfer_get_error (GNOMECHAT_TRANSFER (network->_priv->data_xfer));
	else if (network->_priv->icon_xfer != NULL)
		return gnomechat_transfer_get_error (GNOMECHAT_TRANSFER (network->_priv->icon_xfer));
	else
		return GNOME_VFS_OK;
}


static GnomeVFSURI *
gnomechat_rdf_network_get_from_uri (GnomechatRdfNetwork * network)
{
	g_return_val_if_fail (GNOMECHAT_IS_RDF_NETWORK (network), NULL);

	if (network->_priv->data_xfer != NULL)
		return gnomechat_transfer_get_from_uri (GNOMECHAT_TRANSFER (network->_priv->data_xfer));
	else if (network->_priv->icon_xfer != NULL)
		return gnomechat_transfer_get_from_uri (GNOMECHAT_TRANSFER (network->_priv->icon_xfer));
	else
		return NULL;
}


static GnomeVFSURI *
gnomechat_rdf_network_get_to_uri (GnomechatRdfNetwork * network)
{
	g_return_val_if_fail (GNOMECHAT_IS_RDF_NETWORK (network), NULL);

	if (network->_priv->data_xfer != NULL)
		return gnomechat_transfer_get_to_uri (GNOMECHAT_TRANSFER (network->_priv->data_xfer));
	else if (network->_priv->icon_xfer != NULL)
		return gnomechat_transfer_get_to_uri (GNOMECHAT_TRANSFER (network->_priv->icon_xfer));
	else
		return NULL;
}


static GnomeVFSFileSize
gnomechat_rdf_network_get_file_size (GnomechatRdfNetwork * network)
{
	g_return_val_if_fail (GNOMECHAT_IS_RDF_NETWORK (network), 0);

	if (network->_priv->data_xfer != NULL)
		return gnomechat_transfer_get_file_size (GNOMECHAT_TRANSFER (network->_priv->data_xfer));
	else if (network->_priv->icon_xfer != NULL)
		return gnomechat_transfer_get_file_size (GNOMECHAT_TRANSFER (network->_priv->icon_xfer));
	else
		return 0;
}


static GnomeVFSFileSize
gnomechat_rdf_network_get_bytes_copied (GnomechatRdfNetwork * network)
{
	g_return_val_if_fail (GNOMECHAT_IS_RDF_NETWORK (network), 0);

	if (network->_priv->data_xfer != NULL)
		return gnomechat_transfer_get_bytes_copied (GNOMECHAT_TRANSFER (network->_priv->data_xfer));
	else if (network->_priv->icon_xfer != NULL)
		return gnomechat_transfer_get_file_size (GNOMECHAT_TRANSFER (network->_priv->icon_xfer));
	else
		return 0;
}


static void
gnomechat_rdf_network_start_transfer (GnomechatRdfNetwork * network)
{
	g_return_if_fail (GNOMECHAT_IS_RDF_NETWORK (network));

	if (network->_priv->data_xfer != NULL)
		gnomechat_transfer_start_transfer (GNOMECHAT_TRANSFER (network->_priv->data_xfer));
	else if (network->_priv->icon_xfer != NULL)
		gnomechat_transfer_start_transfer (GNOMECHAT_TRANSFER (network->_priv->icon_xfer));
}


static void
gnomechat_rdf_network_cancel_transfer (GnomechatRdfNetwork * network)
{
	g_return_if_fail (GNOMECHAT_IS_RDF_NETWORK (network));

	if (network->_priv->data_xfer != NULL)
		gnomechat_transfer_cancel_transfer (GNOMECHAT_TRANSFER (network->_priv->data_xfer));
	else if (network->_priv->icon_xfer != NULL)
		gnomechat_transfer_cancel_transfer (GNOMECHAT_TRANSFER (network->_priv->icon_xfer));
}


static gulong
gnomechat_rdf_network_get_n_files (GnomechatRdfNetwork * network)
{
	g_return_val_if_fail (GNOMECHAT_IS_RDF_NETWORK (network), 0);

	return (!(network->_priv->data_is_local) + !(network->_priv->icon_is_local));
}


static gulong
gnomechat_rdf_network_get_file_index (GnomechatRdfNetwork * network)
{
	g_return_val_if_fail (GNOMECHAT_IS_RDF_NETWORK (network), 0);

	if (network->_priv->data_xfer != NULL)
		return 1;
	else if (network->_priv->icon_xfer != NULL)
		return 2;
	else
		return 0;
}


/* ********************** *
 *  GInterface Functions  *
 * ********************** */

static void
gnomechat_rdf_network_xfer_iface_init (GnomechatTransferIface * iface)
{
	iface->get_transfer_type = (GnomechatTransferGetTypeFunc) gnomechat_rdf_network_get_transfer_type;
	iface->get_status = (GnomechatTransferGetStatusFunc) gnomechat_rdf_network_get_status;
	iface->get_error = (GnomechatTransferGetResultFunc) gnomechat_rdf_network_get_error;

	iface->get_from_uri = (GnomechatTransferGetURIFunc) gnomechat_rdf_network_get_from_uri;
	iface->set_from_uri = NULL;

	iface->get_to_uri = (GnomechatTransferGetURIFunc) gnomechat_rdf_network_get_to_uri;
	iface->set_to_uri = NULL;

	iface->get_file_size = (GnomechatTransferGetSizeFunc) gnomechat_rdf_network_get_file_size;
	iface->get_bytes_copied = (GnomechatTransferGetSizeFunc) gnomechat_rdf_network_get_bytes_copied;

	iface->start_transfer = (GnomechatTransferFunc) gnomechat_rdf_network_start_transfer;
	iface->cancel_transfer = (GnomechatTransferFunc) gnomechat_rdf_network_cancel_transfer;

	iface->get_n_files = (GnomechatTransferGetULongFunc) gnomechat_rdf_network_get_n_files;
	iface->get_file_index = (GnomechatTransferGetULongFunc) gnomechat_rdf_network_get_file_index;
}


/* ******************* *
 *  GObject Functions  *
 * ******************* */

static void
gnomechat_rdf_network_set_property (GObject * object, guint param_id, const GValue * value,
									GParamSpec * pspec)
{
	GnomechatRdfNetwork *network = GNOMECHAT_RDF_NETWORK (object);

	switch (param_id)
	{
	case URI:
		g_free (network->_priv->uri);
		network->_priv->uri = g_value_dup_string (value);
		g_object_notify (object, "uri");
		break;

	case DESCRIPTION:
		gnomechat_i18n_string_free (network->_priv->description);
		network->_priv->description = g_value_dup_boxed (value);
		g_object_notify (object, "description");
		break;
	case WEBSITE:
		gnomechat_i18n_string_free (network->_priv->website);
		network->_priv->website = g_value_dup_boxed (value);
		g_object_notify (object, "website");
		break;

	case ENCODING:
		g_free (network->_priv->encoding);
		network->_priv->encoding = g_value_dup_string (value);
		g_object_notify (object, "encoding");
		break;

	case SERVERS:
		{
			GValueArray *array = g_value_get_boxed (value);
			guint i;

			girc_g_slist_deep_free (network->_priv->servers, g_object_unref);

			if (array == NULL)
			{
				network->_priv->servers = NULL;
				return;
			}

			for (i = 0; i < array->n_values; i++)
			{
				network->_priv->servers =
					g_slist_append (network->_priv->servers,
									g_value_dup_object (g_value_array_get_nth (array, i)));
			}

			g_object_notify (object, "servers");
		}
		break;
	case CHANNELS:
		{
			GValueArray *array = g_value_get_boxed (value);
			guint i;

			girc_g_slist_deep_free (network->_priv->channels, g_free);

			if (array == NULL)
			{
				network->_priv->channels = NULL;
				return;
			}

			for (i = 0; i < array->n_values; i++)
			{
				network->_priv->channels =
					g_slist_append (network->_priv->channels,
									g_value_dup_string (g_value_array_get_nth (array, i)));
			}

			g_object_notify (object, "channels");
		}
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}


static void
gnomechat_rdf_network_get_property (GObject * object, guint param_id, GValue * value,
									GParamSpec * pspec)
{
	GnomechatRdfNetwork *network = GNOMECHAT_RDF_NETWORK (object);

	switch (param_id)
	{
	case URI:
		g_value_set_string (value, network->_priv->uri);
		break;
	case DATA_URI:
		g_value_set_string (value, network->_priv->data_uri);
		break;
	case DATA_PATH:
		g_value_set_string (value, network->_priv->data_path);
		break;

	case ICON_URI:
		g_value_set_string (value, network->_priv->icon_uri);
		break;

	case ICON:
		g_value_set_object (value, network->_priv->icon);
		break;

	case DESCRIPTION:
		g_value_set_boxed (value, network->_priv->description);
		break;
	case WEBSITE:
		g_value_set_boxed (value, network->_priv->website);
		break;

	case ENCODING:
		g_value_set_string (value, network->_priv->encoding);
		break;

	case SERVERS:
		girc_g_value_set_value_array_from_slist (value, network->_priv->servers,
												 GNOMECHAT_TYPE_RDF_SERVER);
		break;
	case CHANNELS:
		girc_g_value_set_value_array_from_slist (value, network->_priv->channels, G_TYPE_STRING);
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}

static void
gnomechat_rdf_network_dispose (GObject * object)
{
	GnomechatRdfNetwork *network = GNOMECHAT_RDF_NETWORK (object);

	if (network->_priv->data_xfer != NULL)
		g_object_unref (network->_priv->data_xfer);

	if (network->_priv->icon_xfer != NULL)
		g_object_unref (network->_priv->icon_xfer);

	if (network->_priv->icon != NULL)
		g_object_unref (network->_priv->icon);

	girc_g_slist_deep_free (network->_priv->servers, g_object_unref);

	if (G_OBJECT_CLASS (parent_class)->dispose != NULL)
		(*G_OBJECT_CLASS (parent_class)->dispose) (object);
}


static void
gnomechat_rdf_network_finalize (GObject * object)
{
	GnomechatRdfNetwork *network = GNOMECHAT_RDF_NETWORK (object);

	g_free (network->_priv->uri);
	g_free (network->_priv->data_uri);
	g_free (network->_priv->data_path);
	g_free (network->_priv->icon_uri);
	g_free (network->_priv->encoding);

	gnomechat_i18n_string_free (network->_priv->description);
	gnomechat_i18n_string_free (network->_priv->website);

	girc_g_slist_deep_free (network->_priv->channels, g_free);

	g_free (network->_priv);

	if (G_OBJECT_CLASS (parent_class)->finalize != NULL)
		(*G_OBJECT_CLASS (parent_class)->finalize) (object);
}


/* ***************** *
 *  GType Functions  *
 * ***************** */

static void
gnomechat_rdf_network_class_init (GnomechatRdfNetworkClass * class)
{
	GObjectClass *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	object_class->set_property = gnomechat_rdf_network_set_property;
	object_class->get_property = gnomechat_rdf_network_get_property;
	object_class->dispose = gnomechat_rdf_network_dispose;
	object_class->finalize = gnomechat_rdf_network_finalize;

	g_object_class_install_property (object_class, URI,
									 g_param_spec_string ("uri", "IRC Network URI",
														  "The URI for this network.", NULL,
														  (G_PARAM_READWRITE | G_PARAM_CONSTRUCT)));

	g_object_class_install_property (object_class, DATA_URI,
									 g_param_spec_string ("data-uri", "Data URI",
														  "The URI for this network's RDF data.",
														  NULL, G_PARAM_READABLE));
	g_object_class_install_property (object_class, DATA_PATH,
									 g_param_spec_string ("data-path", "Local Data Path",
														  "The local path for this network's RDF "
														  "data (downloaded from data-uri).",
														  NULL, G_PARAM_READABLE));

	g_object_class_install_property (object_class, ICON_URI,
									 g_param_spec_string ("icon-uri", "Icon URI",
														  "The URI for this network's icon image.",
														  NULL, G_PARAM_READABLE));
	g_object_class_install_property (object_class, ICON,
									 g_param_spec_object ("icon", "Icon Pixbuf",
														  "The loaded icon image.", GDK_TYPE_PIXBUF,
														  G_PARAM_READABLE));

	g_object_class_install_property (object_class, DESCRIPTION,
									 g_param_spec_boxed ("description", "Description",
														 "A description of the network.",
														 GNOMECHAT_TYPE_I18N_STRING,
														 G_PARAM_READWRITE));
	g_object_class_install_property (object_class, WEBSITE,
									 g_param_spec_boxed ("website", "Website URI",
														 "The URI for this network's website.",
														 GNOMECHAT_TYPE_I18N_STRING,
														 G_PARAM_READWRITE));

	g_object_class_install_property (object_class, ENCODING,
									 g_param_spec_string ("encoding", "Character Encoding",
														  "The character encoding used by servers "
														  "on this network.", NULL,
														  G_PARAM_READWRITE));

	g_object_class_install_property (object_class, SERVERS,
									 g_param_spec_value_array
									 ("servers", "Servers",
									  "A value array of GnomechatRdfServer objects.",
									  g_param_spec_object ("server", "A Server",
														   "An individual server object.",
														   GNOMECHAT_TYPE_RDF_SERVER,
														   G_PARAM_READWRITE), G_PARAM_READWRITE));
	g_object_class_install_property (object_class, CHANNELS,
									 g_param_spec_value_array
									 ("channels", "Channels",
									  "A value array of auto-joined channel strings.",
									  g_param_spec_string ("channel", "Channel",
														   "An individual auto-joined channel.",
														   NULL, G_PARAM_READWRITE),
									  G_PARAM_READWRITE));
}


static void
gnomechat_rdf_network_instance_init (GnomechatRdfNetwork * network)
{
	network->_priv = g_new0 (GnomechatRdfNetworkPrivate, 1);

	network->_priv->data_xfer = NULL;
	network->_priv->icon_xfer = NULL;
	network->_priv->data_is_local = TRUE;
	network->_priv->icon_is_local = TRUE;

	network->_priv->icon_uri = NULL;
	network->_priv->icon = NULL;
	network->_priv->description = NULL;
	network->_priv->website = NULL;
	network->_priv->encoding = NULL;
	network->_priv->servers = NULL;
	network->_priv->channels = NULL;
}


/* ************************************************************************** *
 *  PUBLIC API                                                                *
 * ************************************************************************** */


/* *********************** *
 *  Constructor Functions  *
 * *********************** */

GType
gnomechat_rdf_network_get_type (void)
{
	static GType type = G_TYPE_INVALID;

	if (type == G_TYPE_INVALID)
	{
		static const GTypeInfo info = {
			sizeof (GnomechatRdfNetworkClass),
			NULL,				/* base_init */
			NULL,				/* base_finalize */
			(GClassInitFunc) gnomechat_rdf_network_class_init,
			NULL,				/* class_finalize */
			NULL,				/* class_data */
			sizeof (GnomechatRdfNetwork),
			0,					/* n_preallocs */
			(GInstanceInitFunc) gnomechat_rdf_network_instance_init,
		};
		static const GInterfaceInfo xfer_iface_info = {
			(GInterfaceInitFunc) gnomechat_rdf_network_xfer_iface_init,
			NULL,
			NULL
		};

		type = g_type_register_static (G_TYPE_OBJECT, "GnomechatRdfNetwork", &info, 0);

		g_type_add_interface_static (type, GNOMECHAT_TYPE_TRANSFER, &xfer_iface_info);
	}

	return type;
}


GObject *
gnomechat_rdf_network_new (const gchar * uri)
{
	return g_object_new (GNOMECHAT_TYPE_RDF_NETWORK, "uri", uri, NULL);
}


/* ************************ *
 *  URI Property Functions  *
 * ************************ */

G_CONST_RETURN gchar *
gnomechat_rdf_network_get_uri (GnomechatRdfNetwork * network)
{
	g_return_val_if_fail (GNOMECHAT_IS_RDF_NETWORK (network), NULL);

	return network->_priv->uri;
}


void
gnomechat_rdf_network_set_uri (GnomechatRdfNetwork * network, const gchar * uri)
{
	g_return_if_fail (GNOMECHAT_IS_RDF_NETWORK (network));
	g_return_if_fail (uri != NULL && uri[0] != '\0');

	g_free (network->_priv->uri);
	network->_priv->uri = g_strdup (uri);

	g_object_notify (G_OBJECT (network), "uri");
}


/* ***************************** *
 *  Data URI Property Functions  *
 * ***************************** */

G_CONST_RETURN gchar *
gnomechat_rdf_network_get_data_uri (GnomechatRdfNetwork * network)
{
	g_return_val_if_fail (GNOMECHAT_IS_RDF_NETWORK (network), NULL);

	return network->_priv->data_uri;
}


/* Data Transfer Callbacks */
static void
data_xfer_status_changed_cb (GnomechatTransfer * data_xfer, GnomechatTransferStatus status,
							 GnomechatRdfNetwork * network)
{
	switch (status)
	{
	case GNOMECHAT_TRANSFER_STATUS_CHECKING:
	case GNOMECHAT_TRANSFER_STATUS_TRANSFERRING:
	case GNOMECHAT_TRANSFER_STATUS_ERROR:
		gnomechat_transfer_status_changed (GNOMECHAT_TRANSFER (network), status);
		break;

	case GNOMECHAT_TRANSFER_STATUS_DONE:
		{
			GnomeVFSURI *to_uri = gnomechat_transfer_get_to_uri (data_xfer);

			g_object_unref (network->_priv->data_xfer);

			/* If there's a waiting icon transfer, start it now */
			if (network->_priv->icon_xfer != NULL)
				gnomechat_transfer_start_transfer (GNOMECHAT_TRANSFER (network->_priv->icon_xfer));
			else
				gnomechat_transfer_status_changed (GNOMECHAT_TRANSFER (network), status);

			g_free (network->_priv->data_path);
			network->_priv->data_path =
				gnome_vfs_uri_to_string (to_uri, GNOME_VFS_URI_HIDE_TOPLEVEL_METHOD);
			gnome_vfs_uri_unref (to_uri);

			g_object_notify (G_OBJECT (network), "data-path");
			return;
		}
		break;

	default:
		g_return_if_reached ();
		break;
	}
}


static void
data_xfer_progress_cb (GnomechatTransfer * data_xfer, GnomeVFSFileSize bytes_completed,
					   GnomechatRdfNetwork * network)
{
	gnomechat_transfer_progress (GNOMECHAT_TRANSFER (network), bytes_completed);
}


static void
data_xfer_cancelled_cb (GnomechatTransfer * data_xfer, GnomechatRdfNetwork * network)
{
	g_object_unref (data_xfer);

	gnomechat_transfer_cancelled (GNOMECHAT_TRANSFER (network));
}


void
gnomechat_rdf_network_set_data_uri (GnomechatRdfNetwork * network, const gchar * data_uri)
{
	GnomeVFSURI *from_uri;

	g_return_if_fail (GNOMECHAT_IS_RDF_NETWORK (network));
	g_return_if_fail (data_uri != NULL && data_uri[0] != '\0');

	from_uri = gnome_vfs_uri_new (data_uri);

	g_return_if_fail (from_uri != NULL);

	/* Clean out the old data URI stuff */
	if (network->_priv->data_xfer != NULL)
	{
		gnomechat_transfer_cancel_transfer (GNOMECHAT_TRANSFER (network->_priv->data_xfer));
		g_object_unref (network->_priv->data_xfer);
	}
	g_free (network->_priv->data_uri);

	/* Fill in the new URI */
	network->_priv->data_uri = g_strdup (data_uri);

	g_object_notify (G_OBJECT (network), "data-uri");

	/* If we're dealing with a remote file, setup & start the transfer to a cache file */
	if (!gnome_vfs_uri_is_local (from_uri))
	{
		GnomeVFSURI *to_uri;
		gchar *md5,
		 *to_uri_text;

		network->_priv->data_is_local = FALSE;

		md5 = gnome_thumbnail_md5 (data_uri);
		to_uri_text = g_strconcat ("file://", gnomechat_get_cache_dir (), "/", md5, NULL);
		g_free (md5);

		to_uri = gnome_vfs_uri_new (to_uri_text);
		g_free (to_uri_text);

		g_return_if_fail (to_uri != NULL);

		network->_priv->data_xfer = gnomechat_vfs_transfer_new (from_uri, to_uri, TRUE);
		g_object_add_weak_pointer (G_OBJECT (network->_priv->data_xfer),
								   (gpointer *) & (network->_priv->data_xfer));
		g_signal_connect (network->_priv->data_xfer, "status-changed",
						  G_CALLBACK (data_xfer_status_changed_cb), network);
		g_signal_connect (network->_priv->data_xfer, "progress",
						  G_CALLBACK (data_xfer_progress_cb), network);
		g_signal_connect (network->_priv->data_xfer, "cancelled",
						  G_CALLBACK (data_xfer_cancelled_cb), network);

		gnome_vfs_uri_unref (to_uri);

		gnomechat_transfer_start_transfer (GNOMECHAT_TRANSFER (network->_priv->data_xfer));
	}
	/* Otherwise, we're dealing with a local file, so skip to the end part */
	else
	{
		g_free (network->_priv->data_path);
		network->_priv->data_path = g_strdup (data_uri);

		g_object_notify (G_OBJECT (network), "data-path");
	}

	gnome_vfs_uri_unref (from_uri);
}


G_CONST_RETURN gchar *
gnomechat_rdf_network_get_data_path (GnomechatRdfNetwork * network)
{
	g_return_val_if_fail (GNOMECHAT_IS_RDF_NETWORK (network), NULL);

	return network->_priv->data_path;
}


/* ***************************** *
 *  Icon URI Property Functions  *
 * ***************************** */

G_CONST_RETURN gchar *
gnomechat_rdf_network_get_icon_uri (GnomechatRdfNetwork * network)
{
	g_return_val_if_fail (GNOMECHAT_IS_RDF_NETWORK (network), NULL);

	return network->_priv->icon_uri;
}


/* Icon Transfer Callbacks */
static void
icon_xfer_status_changed_cb (GnomechatTransfer * icon_xfer, GnomechatTransferStatus status,
							 GnomechatRdfNetwork * network)
{
	/* Load the icon if we've completed the download */
	if (status == GNOMECHAT_TRANSFER_STATUS_DONE)
	{
		GnomeVFSURI *to_uri = gnomechat_transfer_get_to_uri (icon_xfer);
		GError *err = NULL;
		gchar *icon_path = gnome_vfs_uri_to_string (to_uri, GNOME_VFS_URI_HIDE_TOPLEVEL_METHOD);

		g_object_unref (icon_xfer);
		gnome_vfs_uri_unref (to_uri);

		g_return_if_fail (icon_path != NULL);

		network->_priv->icon = gdk_pixbuf_new_from_file (icon_path, &err);
		if (err != NULL)
		{
			g_warning ("Unable to load icon file \"%s\": %s", icon_path, err->message);
			g_error_free (err);
		}

		g_free (icon_path);

		g_object_notify (G_OBJECT (network), "icon");
	}

	gnomechat_transfer_status_changed (GNOMECHAT_TRANSFER (network), status);
}


static void
icon_xfer_progress_cb (GnomechatTransfer * icon_xfer, GnomeVFSFileSize bytes_completed,
					   GnomechatTransfer * network)
{
	gnomechat_transfer_progress (network, bytes_completed);
}


static void
icon_xfer_cancelled_cb (GnomechatTransfer * icon_xfer, GnomechatTransfer * network)
{
	g_object_unref (icon_xfer);

	gnomechat_transfer_cancelled (network);
}


void
gnomechat_rdf_network_set_icon_uri (GnomechatRdfNetwork * network, const gchar * icon_uri)
{
	GnomeVFSURI *from_uri;

	g_return_if_fail (GNOMECHAT_IS_RDF_NETWORK (network));
	g_return_if_fail (icon_uri != NULL && icon_uri[0] != '\0');

	/* Check the new URI */
	from_uri = gnome_vfs_uri_new (icon_uri);

	g_return_if_fail (from_uri != NULL);

	/* Clean out the old icon URI stuff */
	if (network->_priv->icon_xfer != NULL)
	{
		gnomechat_transfer_cancel_transfer (GNOMECHAT_TRANSFER (network->_priv->icon_xfer));
		g_object_unref (network->_priv->icon_xfer);
	}
	g_free (network->_priv->icon_uri);
	network->_priv->icon_uri = g_strdup (icon_uri);

	g_object_notify (G_OBJECT (network), "icon-uri");

	/* If we're dealing with a remote file, setup & start the transfer to a cache file */
	if (!gnome_vfs_uri_is_local (from_uri))
	{
		GnomeVFSURI *to_uri;
		gchar *md5,
		 *to_uri_text;

		network->_priv->icon_is_local = FALSE;

		md5 = gnome_thumbnail_md5 (icon_uri);
		to_uri_text = g_strconcat ("file://", gnomechat_get_cache_dir (), "/", md5, NULL);
		g_free (md5);

		to_uri = gnome_vfs_uri_new (to_uri_text);
		g_free (to_uri_text);

		g_return_if_fail (to_uri != NULL);

		network->_priv->icon_xfer = gnomechat_vfs_transfer_new (from_uri, to_uri, TRUE);
		g_object_add_weak_pointer (G_OBJECT (network->_priv->icon_xfer),
								   (gpointer *) & (network->_priv->icon_xfer));
		g_signal_connect (network->_priv->icon_xfer, "status-changed",
						  G_CALLBACK (icon_xfer_status_changed_cb), network);
		g_signal_connect (network->_priv->icon_xfer, "progress",
						  G_CALLBACK (icon_xfer_progress_cb), network);
		g_signal_connect (network->_priv->icon_xfer, "cancelled",
						  G_CALLBACK (icon_xfer_cancelled_cb), network);

		gnome_vfs_uri_unref (to_uri);

		/* Only start transferring if the data xfer is not being used/already finished */
		if (network->_priv->data_xfer == NULL)
			gnomechat_transfer_start_transfer (GNOMECHAT_TRANSFER (network->_priv->icon_xfer));
	}
	/* Otherwise, we're dealing with a local file, so just load it */
	else
	{
		GError *err = NULL;
		gchar *icon_path = gnome_vfs_uri_to_string (from_uri, GNOME_VFS_URI_HIDE_TOPLEVEL_METHOD);

		g_return_if_fail (icon_path != NULL);

		network->_priv->icon = gdk_pixbuf_new_from_file (icon_path, &err);
		if (err != NULL)
		{
			g_warning ("Unable to load icon file \"%s\": %s", icon_path, err->message);
			g_error_free (err);
		}

		g_free (icon_path);

		g_object_notify (G_OBJECT (network), "icon");
	}

	gnome_vfs_uri_unref (from_uri);
}


/* ************************* *
 *  Icon Property Functions  *
 * ************************* */

GdkPixbuf *
gnomechat_rdf_network_get_icon (GnomechatRdfNetwork * network)
{
	g_return_val_if_fail (GNOMECHAT_IS_RDF_NETWORK (network), NULL);

	return network->_priv->icon;
}


/* ******************************** *
 *  Description Property Functions  *
 * ******************************** */

G_CONST_RETURN gchar *
gnomechat_rdf_network_get_description (GnomechatRdfNetwork * network)
{
	g_return_val_if_fail (GNOMECHAT_IS_RDF_NETWORK (network), NULL);

	return (network->_priv->description != NULL ? network->_priv->description->str : NULL);
}


void
gnomechat_rdf_network_try_description (GnomechatRdfNetwork * network, const gchar * description,
									   const gchar * lang)
{
	g_return_if_fail (GNOMECHAT_IS_RDF_NETWORK (network));

	/* Un-setter */
	if (description == NULL)
	{
		gnomechat_i18n_string_free (network->_priv->description);
		network->_priv->description = NULL;
		return;
	}

	if (network->_priv->description != NULL)
	{
		const gchar *locale = setlocale (LC_MESSAGES, NULL);
		LocaleMatch old_match = util_locale_match (network->_priv->description->lang, locale),
			new_match = util_locale_match (lang, locale);

		/* Existing description is better for this locale. */
		if (new_match < old_match)
			return;

		gnomechat_i18n_string_free (network->_priv->description);
	}

	network->_priv->description = gnomechat_i18n_string_new (description, lang);
	g_object_notify (G_OBJECT (network), "description");
}


/* **************************** *
 *  Website Property Functions  *
 * **************************** */

G_CONST_RETURN gchar *
gnomechat_rdf_network_get_website (GnomechatRdfNetwork * network)
{
	g_return_val_if_fail (GNOMECHAT_IS_RDF_NETWORK (network), NULL);

	return (network->_priv->website != NULL ? network->_priv->website->str : NULL);
}


void
gnomechat_rdf_network_try_website (GnomechatRdfNetwork * network, const gchar * website,
								   const gchar * lang)
{
	g_return_if_fail (GNOMECHAT_IS_RDF_NETWORK (network));

	/* Un-setter */
	if (website == NULL)
	{
		gnomechat_i18n_string_free (network->_priv->website);
		network->_priv->website = NULL;
		return;
	}

	if (network->_priv->website != NULL)
	{
		const gchar *locale = setlocale (LC_MESSAGES, NULL);
		LocaleMatch old_match = util_locale_match (network->_priv->website->lang, locale),
			new_match = util_locale_match (lang, locale);

		/* Existing website is better for this locale. */
		if (new_match < old_match)
			return;

		gnomechat_i18n_string_free (network->_priv->website);
	}

	network->_priv->website = gnomechat_i18n_string_new (website, lang);
	g_object_notify (G_OBJECT (network), "website");
}


/* ***************************** *
 *  Encoding Property Functions  *
 * ***************************** */

G_CONST_RETURN gchar *
gnomechat_rdf_network_get_encoding (GnomechatRdfNetwork * network)
{
	g_return_val_if_fail (GNOMECHAT_IS_RDF_NETWORK (network), NULL);

	return network->_priv->encoding;
}


void
gnomechat_rdf_network_set_encoding (GnomechatRdfNetwork * network, const gchar * encoding)
{
	g_return_if_fail (GNOMECHAT_IS_RDF_NETWORK (network));
	g_return_if_fail (encoding != NULL && encoding[0] != '\0');

	g_free (network->_priv->encoding);
	network->_priv->encoding = g_strdup (encoding);

	g_object_notify (G_OBJECT (network), "encoding");
}


GSList *
gnomechat_rdf_network_get_servers (GnomechatRdfNetwork * network)
{
	g_return_val_if_fail (GNOMECHAT_IS_RDF_NETWORK (network), NULL);

	return network->_priv->servers;
}


void
gnomechat_rdf_network_append_server (GnomechatRdfNetwork * network, GnomechatRdfServer * server)
{
	g_return_if_fail (GNOMECHAT_IS_RDF_NETWORK (network));
	g_return_if_fail (GNOMECHAT_IS_RDF_SERVER (server));

	network->_priv->servers = g_slist_append (network->_priv->servers, g_object_ref (server));

	g_object_notify (G_OBJECT (network), "servers");
}


void
gnomechat_rdf_network_remove_server (GnomechatRdfNetwork * network, GnomechatRdfServer * server)
{
	g_return_if_fail (GNOMECHAT_IS_RDF_NETWORK (network));
	g_return_if_fail (GNOMECHAT_IS_RDF_SERVER (server));

	network->_priv->servers = g_slist_remove (network->_priv->servers, server);
	g_object_unref (server);

	g_object_notify (G_OBJECT (network), "servers");
}


/* ***************************** *
 *  Channels Property Functions  *
 * ***************************** */

GSList *
gnomechat_rdf_network_get_channels (GnomechatRdfNetwork * network)
{
	g_return_val_if_fail (GNOMECHAT_IS_RDF_NETWORK (network), NULL);

	return network->_priv->servers;
}


void
gnomechat_rdf_network_append_channel (GnomechatRdfNetwork * network, const gchar * channel)
{
	g_return_if_fail (GNOMECHAT_IS_RDF_NETWORK (network));
	g_return_if_fail (channel != NULL && channel[0] == '#' && channel[1] != '\0');

	network->_priv->channels = g_slist_append (network->_priv->channels, g_strdup (channel));

	g_object_notify (G_OBJECT (network), "channels");
}


void
gnomechat_rdf_network_remove_channel (GnomechatRdfNetwork * network, const gchar * channel)
{
	GSList *list;

	g_return_if_fail (GNOMECHAT_IS_RDF_NETWORK (network));
	g_return_if_fail (channel != NULL && channel[0] == '#' && channel[1] != '\0');

	for (list = network->_priv->channels; list != NULL; list = list->next)
	{
		if (girc_g_utf8_strcasecmp (channel, list->data) == 0)
		{
			g_free (list->data);
			network->_priv->channels = g_slist_remove_link (network->_priv->channels, list);
		}
	}
}
