/*
 *  GnomeChat: src/esco-hpaned.c
 *  stolen from nautilus/src/libnautilus-private/nautilus-horizontal-splitter.c
 *
 *  Copyright (c) 2002 James M. Cape.
 *  Copyright (C) 1999, 2000 Eazel, Inc.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <gnome.h>
#include <esco-hpaned.h>


#define CLOSE_THRESHOLD 5
#define CLICK_SLOP 1
#define CLICK_TIMEOUT	400


enum
{
	PROP_0,
	PROP_OPEN,
	PROP_CLOSE_SIDE,
	PROP_SIZE_SAVE_FUNC,
	PROP_SIZE_LOAD_FUNC
};

enum
{
	SIGNAL_TOGGLED,
	SIGNAL_LAST
};


static void esco_hpaned_class_init (EscoHPanedClass * class);
static void esco_hpaned_instance_init (EscoHPaned * paned);

static void esco_hpaned_get_property (GObject * object,
									  guint param_id,
									  GValue * value,
									  GParamSpec * pspec);
static void esco_hpaned_set_property (GObject * object,
									  guint param_id,
									  const GValue * value,
									  GParamSpec * pspec);

static void esco_hpaned_realize (GtkWidget * widget);
static gboolean esco_hpaned_button_press_event (GtkWidget * widget,
												GdkEventButton * event);
static gboolean esco_hpaned_button_release_event (GtkWidget * widget,
												  GdkEventButton * event);


static gint esco_hpaned_signals[SIGNAL_LAST] = { 0 };
static gpointer parent_class = NULL;


GType
esco_hpaned_get_type (void)
{
	static GType hpaned_type = 0;

	if (!hpaned_type)
	{
		static const GTypeInfo hpaned_info = {
			sizeof (EscoHPanedClass),
			NULL,				/* base_init */
			NULL,				/* base_finalize */
			(GClassInitFunc) esco_hpaned_class_init,
			NULL,				/* class_finalize */
			NULL,				/* class_data */
			sizeof (EscoHPaned),
			0,					/* n_preallocs */
			(GInstanceInitFunc) esco_hpaned_instance_init,
		};

		hpaned_type = g_type_register_static (GTK_TYPE_HPANED,
											  "EscoHPaned", &hpaned_info, 0);
	}

	return hpaned_type;
}


static void
esco_hpaned_class_init (EscoHPanedClass * class)
{
	GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (class);
	GObjectClass *object_class = G_OBJECT_CLASS (class);

	object_class->get_property = esco_hpaned_get_property;
	object_class->set_property = esco_hpaned_set_property;

	widget_class->realize = esco_hpaned_realize;
	widget_class->button_press_event = esco_hpaned_button_press_event;
	widget_class->button_release_event = esco_hpaned_button_release_event;

	parent_class = g_type_class_peek_parent (class);

	g_object_class_install_property (object_class,
									 PROP_OPEN,
									 g_param_spec_boolean ("open",
														   _("HPaned "
															 "Open/Closed"),
														   _("Whether the "
															 "HPaned is open "
															 "or closed"),
														   TRUE,
														   G_PARAM_READABLE |
														   G_PARAM_WRITABLE));
	g_object_class_install_property (object_class,
									 PROP_CLOSE_SIDE,
									 g_param_spec_enum ("close-side",
														_("HPaned Close Side"),
														_("Which side the "
														  "Hpaned hides when it "
														  "closes."),
														GTK_TYPE_POSITION_TYPE,
														GTK_POS_LEFT,
														G_PARAM_READABLE |
														G_PARAM_WRITABLE));
	g_object_class_install_property (object_class,
									 PROP_SIZE_SAVE_FUNC,
									 g_param_spec_pointer ("size-save-func",
														   _("HPaned Size "
															 "Saving "
															 "Function"),
														   _("Which function "
															 "to call when "
															 "this paned's "
															 "size is set."),
														   G_PARAM_READABLE |
														   G_PARAM_WRITABLE));
	g_object_class_install_property (object_class,
									 PROP_SIZE_LOAD_FUNC,
									 g_param_spec_pointer ("size-load-func",
														   _("HPaned Size "
															 "Loading "
															 "Function"),
														   _("Which function "
															 "to call when "
															 "this paned's "
															 "is realized, "
															 "to get the "
															 "initial size."),
														   G_PARAM_READABLE |
														   G_PARAM_WRITABLE));

	esco_hpaned_signals[SIGNAL_TOGGLED] =
		g_signal_new ("toggled",
					  G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_LAST,
					  G_STRUCT_OFFSET (EscoHPanedClass, toggled),
					  NULL, NULL,
					  g_cclosure_marshal_VOID__BOOLEAN,
					  G_TYPE_NONE, 1, G_TYPE_BOOLEAN);
}


static void
esco_hpaned_instance_init (EscoHPaned * paned)
{
	paned->open = TRUE;
	paned->close_side = GTK_POS_LEFT;
	paned->size_save_func = NULL;
	paned->size_load_func = NULL;
}


static void
esco_hpaned_get_property (GObject * object,
						  guint param_id,
						  GValue * value,
						  GParamSpec * pspec)
{
	EscoHPaned *paned = ESCO_HPANED (object);

	switch (param_id)
	{
		case PROP_OPEN:
			g_value_set_boolean (value, paned->open);
			break;
		case PROP_CLOSE_SIDE:
			g_value_set_enum (value, paned->close_side);
			break;
		case PROP_SIZE_SAVE_FUNC:
			g_value_set_pointer (value, paned->size_save_func);
			break;
		case PROP_SIZE_LOAD_FUNC:
			g_value_set_pointer (value, paned->size_load_func);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
			break;
	}
}


static void
esco_hpaned_set_property (GObject * object,
						  guint param_id,
						  const GValue * value,
						  GParamSpec * pspec)
{
	EscoHPaned *paned = ESCO_HPANED (object);

	switch (param_id)
	{
		case PROP_OPEN:
			esco_hpaned_set_open (paned, g_value_get_boolean (value));
			break;
		case PROP_CLOSE_SIDE:
			esco_hpaned_set_close_side (paned, g_value_get_enum (value));
			break;
		case PROP_SIZE_SAVE_FUNC:
			esco_hpaned_set_size_save_func (paned, g_value_get_pointer (value));
			break;
		case PROP_SIZE_LOAD_FUNC:
			esco_hpaned_set_size_load_func (paned, g_value_get_pointer (value));
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
			break;
	}
}


static void
esco_hpaned_realize (GtkWidget * widget)
{
	EscoHPaned *paned = ESCO_HPANED (widget);
	gint saved_pos,
	  real_pos;

	if (GTK_WIDGET_CLASS (parent_class)->realize)
		GTK_WIDGET_CLASS (parent_class)->realize (widget);

	if (paned->size_load_func)
	{
		saved_pos = paned->size_load_func ();

		if (paned->close_side == GTK_POS_LEFT)
			real_pos = saved_pos;
		else
			real_pos = widget->allocation.width - saved_pos;

		gtk_paned_set_position (GTK_PANED (widget), real_pos);
	}
}


static gboolean
esco_hpaned_button_press_event (GtkWidget * widget,
								GdkEventButton * event)
{
	EscoHPaned *paned = ESCO_HPANED (widget);
	gboolean result;

	result =
		GTK_WIDGET_CLASS (parent_class)->button_press_event (widget, event);

	if (result)
	{
		paned->press_coord = event->x;
		paned->press_time = event->time;

		if (paned->open)
			paned->saved_size = gtk_paned_get_position (GTK_PANED (widget));
	}

	return result;
}


static gboolean
esco_hpaned_button_release_event (GtkWidget * widget,
								  GdkEventButton * event)
{
	EscoHPaned *paned = ESCO_HPANED (widget);
	gboolean result;
	gint delta,
	  delta_time;
	gint pos;

	result =
		GTK_WIDGET_CLASS (parent_class)->button_release_event (widget, event);

	if (result)
	{
		delta = abs (event->x - paned->press_coord);
		delta_time = event->time - paned->press_time;
		pos = gtk_paned_get_position (GTK_PANED (widget));

		/* If we're a click, or a drag to w/i 5px of the edge,
		   toggle the open/closed state. */
		if ((delta < CLICK_SLOP && delta_time < CLICK_TIMEOUT)
			|| (pos < CLOSE_THRESHOLD))
		{
			esco_hpaned_set_open (paned, !(paned->open));
		}
		/* Otherwise, save the current size */
		else if (paned->size_save_func != NULL)
		{
			if (paned->close_side == GTK_POS_LEFT)
			{
				paned->size_save_func (pos);
			}
			else if (paned->close_side == GTK_POS_RIGHT)
			{
				paned->size_save_func (GTK_WIDGET (paned)->allocation.width -
									   pos);
			}
		}
	}

	return result;
}

/* Public Functions */
GtkWidget *
esco_hpaned_new (GtkPositionType close_side)
{
	return GTK_WIDGET (g_object_new (ESCO_TYPE_HPANED,
									 "close-side", close_side, NULL));
}


gboolean
esco_hpaned_get_open (EscoHPaned * paned)
{
	g_return_val_if_fail (paned != NULL, FALSE);
	g_return_val_if_fail (ESCO_IS_HPANED (paned), FALSE);

	return paned->open;
}


void
esco_hpaned_set_open (EscoHPaned * paned,
					  gboolean open)
{
	g_return_if_fail (paned != NULL);
	g_return_if_fail (ESCO_IS_HPANED (paned));

	if (open)
	{
		gtk_paned_set_position (GTK_PANED (paned), paned->saved_size);
	}
	else
	{
		if (paned->close_side == GTK_POS_LEFT)
		{
			gtk_paned_set_position (GTK_PANED (paned), 0);
		}
		else if (paned->close_side == GTK_POS_RIGHT)
		{
			gtk_paned_set_position (GTK_PANED (paned),
									(GTK_WIDGET (paned)->allocation.width - 5));
		}
	}

	paned->open = open;

	g_signal_emit (paned, esco_hpaned_signals[SIGNAL_TOGGLED], 0, open, NULL);
}


GtkPositionType
esco_hpaned_get_close_side (EscoHPaned * paned)
{
	g_return_val_if_fail (paned != NULL, -1);
	g_return_val_if_fail (ESCO_IS_HPANED (paned), -1);

	return paned->close_side;
}


void
esco_hpaned_set_size_save_func (EscoHPaned * paned,
								EscoHPanedSizeSaveFunction prefs_func)
{
	g_return_if_fail (paned != NULL);
	g_return_if_fail (ESCO_IS_HPANED (paned));

	paned->size_save_func = prefs_func;
}


void
esco_hpaned_set_size_load_func (EscoHPaned * paned,
								EscoHPanedSizeLoadFunction prefs_func)
{
	g_return_if_fail (paned != NULL);
	g_return_if_fail (ESCO_IS_HPANED (paned));

	paned->size_load_func = prefs_func;
}


void
esco_hpaned_set_close_side (EscoHPaned * paned,
							GtkPositionType close_side)
{
	g_return_if_fail (paned != NULL);
	g_return_if_fail (ESCO_IS_HPANED (paned));
	g_return_if_fail (close_side == GTK_POS_LEFT
					  || close_side == GTK_POS_RIGHT);

	paned->close_side = close_side;
}
