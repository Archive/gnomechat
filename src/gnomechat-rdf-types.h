/*
 *  GnomeChat: src/gnomechat-rdf-types.h
 *
 *  Copyright (c) 2003 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.01234567890
 */

#ifndef __GNOMECHAT_RDF_TYPES_H__
#define __GNOMECHAT_RDF_TYPES_H__

#include <glib-object.h>


#define GNOMECHAT_TYPE_I18N_STRING		(gnomechat_i18n_string_get_type ())

typedef struct _GnomechatI18nString GnomechatI18nString;

struct _GnomechatI18nString
{
	gchar *str;
	gchar *lang;
};


GType gnomechat_i18n_string_get_type (void);
GnomechatI18nString *gnomechat_i18n_string_new (const gchar * lang,
												const gchar * str);
GnomechatI18nString *gnomechat_i18n_string_dup (GnomechatI18nString * str);
void gnomechat_i18n_string_free (GnomechatI18nString * str);


#endif /* __GNOMECHAT_RDF_TYPES_H__ */
