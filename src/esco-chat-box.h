/*
 *  GnomeChat: src/esco-chat-box.h
 *
 *  Copyright (c) 2002 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef __ESCO_CHAT_BOX_H__
#define __ESCO_CHAT_BOX_H__


#include <esco-enums.h>

#include <gtk/gtkwidget.h>
#include <gtk/gtkvbox.h>
#include <libgircclient/girc-client.h>


#define ESCO_TYPE_CHAT_BOX				(esco_chat_box_get_type ())
#define ESCO_CHAT_BOX(obj)				(G_TYPE_CHECK_INSTANCE_CAST ((obj), ESCO_TYPE_CHAT_BOX, EscoChatBox))
#define ESCO_CHAT_BOX_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST ((klass), ESCO_TYPE_CHAT_BOX, EscoChatBoxClass))
#define ESCO_IS_CHAT_BOX(obj)			(G_TYPE_CHECK_INSTANCE_TYPE ((obj), ESCO_TYPE_CHAT_BOX))
#define ESCO_IS_CHAT_BOX_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), ESCO_TYPE_CHAT_BOX))
#define ESCO_CHAT_BOX_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS ((obj), ESCO_TYPE_CHAT_BOX, EscoChatBoxClass))


typedef struct _EscoChatBox EscoChatBox;
typedef struct _EscoChatBoxClass EscoChatBoxClass;

typedef void (*EscoChatBoxPesterFunc) (EscoChatBox * box,
									   EscoPesterType type);
typedef void (*EscoChatBoxSignalFunc) (EscoChatBox * box);
typedef void (*EscoChatBoxSelectionChangedFunc) (EscoChatBox * box,
												 gboolean any_selected);


struct _EscoChatBox
{
	GtkVBox parent;

	/* < Public, Read-Only > */
	/* Properties */
	gchar *encoding;
	gchar *nick;
	gchar *topic;

	/* The paned */
	GtkWidget *paned;

	/* The text widget */
	GtkWidget *viewer;

	/* Userlist Sidebar */
	GtkWidget *userlist_box;

	/* Userlist treeview & friends */
	GtkWidget *userlist_img;
	GtkWidget *userlist_view;

	/* Userlist buttons */
	GtkWidget *userlist_bbox;

	/* Bottom Entry */
	GtkWidget *nick_align;
	GtkWidget *nick_label;
	GtkWidget *expander_button;
	GtkWidget *main_text;
	GtkWidget *main_text_align;
	GtkWidget *main_text_scrwin;
	GtkWidget *send_button;

	/* Topic Dock Item */
	GtkWidget *topic_hbox;
	GtkWidget *topic_image;
	GtkWidget *topic_label;
	GtkWidget *topic_entry;
	GtkWidget *topic_props_button;
	GtkWidget *topic_send_button;

	/* Notebook tab */
	GtkWidget *tab_hbox;
	GtkWidget *tab_image;
	GtkWidget *tab_label;
	GtkWidget *tab_close_button;

	/* IRC Client */
	GIrcClient *irc;
	GIrcTarget *target;

	/* Various strings */
	gchar *title;
	gchar *subtitle;
	gchar *status_icon;

	EscoChatModeType mode:3;
	EscoPesterType pester:3;
	/* Hack job, prevents a lot of pain when closing a target on destroy. */
	gboolean dying:1;
};

struct _EscoChatBoxClass
{
	GtkVBoxClass parent_class;

	/* Signals */
	EscoChatBoxPesterFunc pester;
	EscoChatBoxSignalFunc focused;
	EscoChatBoxSignalFunc title_changed;
	EscoChatBoxSignalFunc mode_changed;
	EscoChatBoxSelectionChangedFunc user_selection_changed;
};


GType esco_chat_box_get_type (void);

GtkWidget *esco_chat_box_new ();

G_CONST_RETURN gchar *esco_chat_box_get_mode_icon (EscoChatBox * box);

void esco_chat_box_set_encoding (EscoChatBox * box,
								 const gchar * encoding);
void esco_chat_box_set_nick (EscoChatBox * box,
							 const gchar * nick);
void esco_chat_box_set_mode (EscoChatBox * box,
							 EscoChatModeType mode);
void esco_chat_box_set_title (EscoChatBox * box,
							  const gchar * title);
void esco_chat_box_set_irc (EscoChatBox * box,
							GIrcClient * irc);
void esco_chat_box_set_status (EscoChatBox * box,
							   guint status);

void esco_chat_box_set_subtitle (EscoChatBox * box,
								 const gchar * str);
void esco_chat_box_set_topic (EscoChatBox * box,
							  const gchar * topic);
							  
void esco_chat_box_clear_viewer (EscoChatBox *box);

#endif /* __ESCO_CHAT_H__ */
