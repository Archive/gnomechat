/*
 *  GnomeChat: src/esco-encoding.c
 *
 *  Copyright (c) 2002 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "esco-encoding.h"


#ifdef HAVE_CONFIG_H
#	include <config.h>
#endif /* HAVE_CONFIG_H */

#include <string.h>

#include <bonobo/bonobo-i18n.h>



/*
 * RAW DATA
 *
 * See:
 *     http://www.iana.org/assignments/character-sets
 *     file:///usr/lib/gconv/gconv-modules
 * for the source to all this
 *
 */


/* Unicode Data */
static const gchar *const utf8_aliases[] = {
	"UTF-8",
	"ISO-10646/UTF8",
	"UTF8",
	"ISO-10646/UTF-8",
	NULL
};

static const gchar *const utf7_aliases[] = {
	"UTF-7",
	"UTF7",
	NULL
};

static const gchar *const utf16_aliases[] = {
	"UTF-16",
	"UTF16",
	NULL
};

static const gchar *const utf16be_aliases[] = {
	"UTF-16BE",
	"UTF16BE",
	NULL
};

static const gchar *const utf16le_aliases[] = {
	"UTF-16LE",
	"UTF16LE",
	NULL
};

static const gchar *const utf32_aliases[] = {
	"UTF-32",
	"UTF32",
	NULL
};

static const gchar *const utf32be_aliases[] = {
	"UTF-32BE",
	"UTF32BE",
	NULL
};

static const gchar *const utf32le_aliases[] = {
	"UTF-32LE",
	"UTF32LE",
	NULL
};

static const gchar *const ucs2_aliases[] = {
	"UCS-2",
	"ISO-10646/UCS2",
	"UCS2",
	NULL
};

static const gchar *const ucs2be_aliases[] = {
	"UCS-2BE",
	NULL
};

static const gchar *const ucs2le_aliases[] = {
	"UCS-2LE",
	NULL
};

static const gchar *const ucs4_aliases[] = {
	"UCS-4",
	"ISO-10646:1993/UCS4",
	"ISO-10646/UCS4",
	"UCS4",
	"CSUCS4",
	NULL
};

static const gchar *const ucs4be_aliases[] = {
	"UCS-4BE",
	NULL
};

static const gchar *const ucs4le_aliases[] = {
	"UCS-4LE",
	NULL
};


static EscoEncoding unicode_encodings[] = {
	{ESCO_ENCODING_GROUP_UNICODE,
	 N_("UTF-8"), N_("Universal: UTF-8"),
	 (const gchar **) utf8_aliases, TRUE},

	{ESCO_ENCODING_GROUP_UNICODE,
	 N_("UTF-7"), N_("Universal: UTF-7"),
	 (const gchar **) utf7_aliases, FALSE},

	{ESCO_ENCODING_GROUP_UNICODE,
	 N_("UTF-16"), N_("Universal: UTF-16"),
	 (const gchar **) utf16_aliases, FALSE},

	{ESCO_ENCODING_GROUP_UNICODE,
	 N_("UTF-16 (Mainframe)"), N_("Universal: UTF-16 (Mainframe)"),
	 (const gchar **) utf16be_aliases, FALSE},

	{ESCO_ENCODING_GROUP_UNICODE,
	 N_("UTF-16 (IBM PC)"), N_("Universal: UTF-16 (IBM PC)"),
	 (const gchar **) utf16le_aliases, FALSE},

	{ESCO_ENCODING_GROUP_UNICODE,
	 N_("UTF-32"), N_("Universal: UTF-32"),
	 (const gchar **) utf32_aliases, FALSE},

	{ESCO_ENCODING_GROUP_UNICODE,
	 N_("UTF-32 (Mainframe)"), N_("Universal: UTF-32 (Mainframe)"),
	 (const gchar **) utf32be_aliases, FALSE},

	{ESCO_ENCODING_GROUP_UNICODE,
	 N_("UTF-32 (IBM PC)"), N_("Universal: UTF-32 (IBM PC)"),
	 (const gchar **) utf32le_aliases, FALSE},

	{ESCO_ENCODING_GROUP_UNICODE,
	 N_("UCS-2"), N_("Universal: UCS-2"),
	 (const gchar **) ucs2_aliases, FALSE},

	{ESCO_ENCODING_GROUP_UNICODE,
	 N_("UCS-2 (Mainframe)"), N_("Universal: UCS-2 (Mainframe)"),
	 (const gchar **) ucs2be_aliases, FALSE},

	{ESCO_ENCODING_GROUP_UNICODE,
	 N_("UCS-2 (IBM PC)"), N_("Universal: UCS-2 (IBM PC)"),
	 (const gchar **) ucs2le_aliases, FALSE},

	{ESCO_ENCODING_GROUP_UNICODE,
	 N_("UCS-4"), N_("Universal: UCS-4"),
	 (const gchar **) ucs4_aliases, FALSE},

	{ESCO_ENCODING_GROUP_UNICODE,
	 N_("UCS-4 (Mainframe)"), N_("Universal: UCS-4 (Mainframe)"),
	 (const gchar **) ucs4be_aliases, FALSE},

	{ESCO_ENCODING_GROUP_UNICODE,
	 N_("UCS-4 (IBM PC)"), N_("Universal: UCS-4 (IBM PC)"),
	 (const gchar **) ucs4le_aliases, FALSE},

	{ESCO_ENCODING_GROUP_INVALID, NULL, NULL, NULL, FALSE}
};


/* East Asian Data */
/* Chinese Simplified */
static const gchar *const gb2312_aliases[] = {
	"EUC-CN",
	"GB2312",
	"CSGB2312",
	"CN-GB",
	"EUCCN",
	NULL
};

static const gchar *const gbk_aliases[] = {
	"GBK",
	"GB13000",
	"CP936",
	NULL
};

static const gchar *const gb18030_aliases[] = {
	"GB18030",
	NULL
};

static const gchar *const iso2022cn_aliases[] = {
	"ISO-2022-CN",
	"ISO2022CN",
	"CSISO2022CN",
	NULL
};

/* Chinese Traditional */
static const gchar *const big5_aliases[] = {
	"BIG5",
	"BIG-5",
	"BIGFIVE",
	"BIG-FIVE",
	"CN-BIG5",
	"CP950",
	NULL
};

static const gchar *const big5hkscs_aliases[] = {
	"BIG5HKSCS",
	"BIG5-HKSCS",
	NULL
};

static const gchar *const euctw_aliases[] = {
	"EUC-TW",
	"EUCTW",
	"OSF0005000a",
	NULL
};

/* Japanese */
static const gchar *const eucjp_aliases[] = {
	"EUC-JP",
	"EUCJP",
	"CSEUCPKDFMTJAPANESE",
	"OSF00030010",
	"UJIS",
	NULL
};

static const gchar *const iso2022jp_aliases[] = {
	"ISO-2022-JP",
	"ISO2022JP",
	"CSISO2022JP",
	NULL
};

static const gchar *const shiftjis_aliases[] = {
	"SJIS",
	"SHIFT-JIS",
	"SHIFT_JIS",
	"CP932",
	"MS_KANJI",
	"CSSHIFTJIS",
	NULL
};

/* Korean */
static const gchar *const iso2022kr_aliases[] = {
	"ISO-2022-KR",
	"ISO2022KR",
	"CSISO2022KR",
	NULL
};

static const gchar *const euckr_aliases[] = {
	"EUC-KR",
	"CSEUCKR",
	"OSF0004000a",
	NULL
};

static const gchar *const uhc_aliases[] = {
	"UHC",
	"MSCP949",
	"CP949",
	"OSF100203B5",
	NULL
};

static const gchar *const johab_aliases[] = {
	"JOHAB",
	"MSCP1361",
	"CP1361",
	NULL
};

/* Thai */
static const gchar *const tis620_aliases[] = {
	"TIS-620",
	"TIS620",
	"TIS620-0",
	"TIS620.2529-1",
	"TIS620.2533-0",
	"ISO-IR-166",
	NULL
};

/* Vietnamese */
static const gchar *const tcvn_aliases[] = {
	"TCVN5712-1",
	"TCVN5712-1:1993",
	"TCVN-5712",
	"TCVN",
	NULL
};

static const gchar *const viscii_aliases[] = {
	"VISCII",
	"CSVISCII",
	NULL
};

static const gchar *const cp1258_aliases[] = {
	"CP1258",
	"WINDOWS-1258",
	NULL
};


/* Encodings */
static EscoEncoding east_asian_encodings[] = {
	{ESCO_ENCODING_GROUP_EAST_ASIAN,
	 N_("Chinese Simplified (GB)"), N_("Chinese Simplified: GB"),
	 (const gchar **) gb2312_aliases, TRUE},
	{ESCO_ENCODING_GROUP_EAST_ASIAN,
	 N_("Chinese Simplified (GBK)"), N_("Chinese Simplified: GBK"),
	 (const gchar **) gbk_aliases, FALSE},
	{ESCO_ENCODING_GROUP_EAST_ASIAN,
	 N_("Chinese Simplified (GB18030)"), N_("Chinese Simplified: GB18030"),
	 (const gchar **) gb18030_aliases, FALSE},
	{ESCO_ENCODING_GROUP_EAST_ASIAN,
	 N_("Chinese Simplified (ISO 2022-CN)"), N_("Chinese Simplified: ISO 2022-CN"),
	 (const gchar **) iso2022cn_aliases, FALSE},

	{ESCO_ENCODING_GROUP_EAST_ASIAN,
	 N_("Chinese Traditional (Big 5)"), N_("Chinese Traditional: Big 5"),
	 (const gchar **) big5_aliases, TRUE},
	{ESCO_ENCODING_GROUP_EAST_ASIAN,
	 N_("Chinese Traditional (Big 5 HK)"), N_("Chinese Traditional: Big 5 (HK)"),
	 (const gchar **) big5hkscs_aliases, FALSE},
	{ESCO_ENCODING_GROUP_EAST_ASIAN,
	 N_("Chinese Traditional (EUC-TW)"), N_("Chinese Traditional: EUC-TW"),
	 (const gchar **) euctw_aliases, FALSE},

	{ESCO_ENCODING_GROUP_EAST_ASIAN,
	 N_("Japanese (ISO 2022-JP)"), N_("Japanese: ISO 2022-JP"),
	 (const gchar **) iso2022jp_aliases, TRUE},
	{ESCO_ENCODING_GROUP_EAST_ASIAN,
	 N_("Japanese (EUC-JP)"), N_("Japanese: EUC-JP"),
	 (const gchar **) eucjp_aliases, FALSE},
	{ESCO_ENCODING_GROUP_EAST_ASIAN,
	 N_("Japanese (Shift JIS)"), N_("Japanese: Shift JIS"),
	 (const gchar **) shiftjis_aliases, FALSE},

	{ESCO_ENCODING_GROUP_EAST_ASIAN,
	 N_("Korean (ISO 2022-KR)"), N_("Korean: ISO 2022-KR"),
	 (const gchar **) iso2022kr_aliases, TRUE},
	{ESCO_ENCODING_GROUP_EAST_ASIAN,
	 N_("Korean (EUC-KR)"), N_("Korean: EUC-KR"),
	 (const gchar **) euckr_aliases, FALSE},
	{ESCO_ENCODING_GROUP_EAST_ASIAN,
	 N_("Korean (UHC)"), N_("Korean: UHC"),
	 (const gchar **) uhc_aliases, FALSE},
	{ESCO_ENCODING_GROUP_EAST_ASIAN,
	 N_("Korean (JOHAB)"), N_("Korean: JOHAB"),
	 (const gchar **) johab_aliases, FALSE},

	{ESCO_ENCODING_GROUP_EAST_ASIAN,
	 N_("Thai (TIS-620)"), N_("Thai: TIS-620"),
	 (const gchar **) tis620_aliases, TRUE},

	{ESCO_ENCODING_GROUP_EAST_ASIAN,
	 N_("Vietnamese (TCVN)"), N_("Vietnamese: TCVN"),
	 (const gchar **) tcvn_aliases, TRUE},
	{ESCO_ENCODING_GROUP_EAST_ASIAN,
	 N_("Vietnamese (VISCII)"), N_("Vietnamese: VISCII"),
	 (const gchar **) viscii_aliases, FALSE},
	{ESCO_ENCODING_GROUP_EAST_ASIAN,
	 N_("Vietnamese (Windows)"), N_("Vietnamese: Windows"),
	 (const gchar **) cp1258_aliases, FALSE},

	{ESCO_ENCODING_GROUP_INVALID, NULL, NULL, NULL, FALSE}
};


/* Western Asian Data */
/* Armenian */
static const gchar *const armscii_aliases[] = {
	"ARMSCII-8",
	"ARMSCII",
	NULL
};

/* Georgian */
static const gchar *const geostd8_aliases[] = {
	"GEORGIAN-ACADEMY",
	"GEOSTD8",
	NULL
};

/* Turkish */
static const gchar *const iso8859_9_aliases[] = {
	"ISO-8859-9",
	"ISO8859-9",
	"ISO88599",
	"ISO_8859-9",
	"ISO_8859-9:1989",
	"8859_3",
	"LATIN5",
	"L5",
	"CSLATIN5",
	"OSF00010009",
	"IBM920",
	"CP920",
	"TS-5881",
	"ECMA-128",
	NULL
};

static const gchar *const ibm857_aliases[] = {
	"IBM857",
	"CP857",
	"857",
	"CSIBM857",
	"OSF10020359",
	NULL
};

static const gchar *const cp1254_aliases[] = {
	"CP1254",
	"WINDOWS-1254",
	"MS-TURK",
	NULL
};


/* Encodings */
static EscoEncoding west_asian_encodings[] = {
	{ESCO_ENCODING_GROUP_WEST_ASIAN,
	 N_("Armenian (ARMSCII)"), N_("Armenian: ARMSCII"),
	 (const gchar **) armscii_aliases, TRUE},

	{ESCO_ENCODING_GROUP_WEST_ASIAN,
	 N_("Georgian (GEOSTD8)"), N_("Georgian: GEOSTD8"),
	 (const gchar **) geostd8_aliases, TRUE},

	{ESCO_ENCODING_GROUP_WEST_ASIAN,
	 N_("Turkish (ISO 8859-9)"), N_("Turkish: ISO 8859-9"),
	 (const gchar **) iso8859_9_aliases, TRUE},
	{ESCO_ENCODING_GROUP_WEST_ASIAN,
	 N_("Turkish (IBM-857)"), N_("Turkish: IBM-857"),
	 (const gchar **) ibm857_aliases, FALSE},
	{ESCO_ENCODING_GROUP_WEST_ASIAN,
	 N_("Turkish (Microsoft)"), N_("Turkish: Microsoft"),
	 (const gchar **) cp1254_aliases, FALSE},

	{ESCO_ENCODING_GROUP_INVALID, NULL, NULL, NULL, FALSE}
};


/* Eastern European Data */
/* Baltic */
static const gchar *const iso8859_13_aliases[] = {
	"ISO-8839-13",
	"ISO8859-13",
	"ISO885913",
	"ISO-IR-179",
	"LATIN7",
	"L7",
	"BALTIC",
	NULL
};

static const gchar *const iso8859_4_aliases[] = {
	"ISO-8839-4",
	"ISO8859-4",
	"ISO88594",
	"8859_4",
	"ISO_8859-4",
	"ISO_8859-4:1988" "ISO-IR-110",
	"LATIN4",
	"L4",
	"CSISOLATIN4" "OSF00010004",
	NULL
};

static const gchar *const cp1257_aliases[] = {
	"CP1257",
	"WINDOWS-1257",
	"WINBALTRIM",
	NULL
};

/* Cyrillic */
static const gchar *const iso8859_5_aliases[] = {
	"ISO-8839-5",
	"ISO8859-5",
	"ISO88595",
	"8859_5",
	"ISO_8859-5",
	"ISO_8859-5:1988" "ISO-IR-144",
	"CYRILLIC",
	"CSISOLATINCYRILLIC",
	"OSF00010005" "IBM915",
	"CP915",
	NULL
};

static const gchar *const ibm855_aliases[] = {
	"IBM855",
	"CP855",
	"855",
	"CSCP855",
	"OSF1002035",
	NULL
};

static const gchar *const koi8_aliases[] = {
	"KOI-8",
	"KOI8",
	NULL
};

static const gchar *const cp1251_aliases[] = {
	"CP1251",
	"WINDOWS-1251",
	"MS-CRYL",
	NULL
};

/* Eastern */
static const gchar *const iso8859_2_aliases[] = {
	"ISO-8839-2",
	"ISO8859-2",
	"ISO88592",
	"8859_2",
	"ISO_8859-2",
	"ISO_8859-2:1987" "ISO-IR-101",
	"LATIN2",
	"L2",
	"CSISOLATIN4" "OSF00010002",
	"IBM912",
	"CP912",
	NULL
};

static const gchar *const ibm852_aliases[] = {
	"IBM852",
	"CP852",
	"852",
	"CSCP852",
	"OSF10020354",
	NULL
};

static const gchar *const cp10007_aliases[] = {
	"CP10007",
	"MS-MAC-CYRILLIC",
	"MSMACCYRILLIC",
	NULL
};

static const gchar *const isoir111_aliases[] = {
	"ECMA-CYRILLIC",
	"ECMACYRILLIC",
	"ISO-IR-111",
	"CSISO111ECMACRYLLIC",
	NULL
};

static const gchar *const cp1250_aliases[] = {
	"CP1250",
	"WINDOWS-1250",
	"MS-EE",
	NULL
};

/* Romainian */
static const gchar *const iso8859_16_aliases[] = {
	"ISO-8839-16",
	"ISO8859-16",
	"ISO885916",
	"ISO-IR-126",
	"LATIN10",
	"L10",
	NULL
};

/* Russian */
static const gchar *const koi8r_aliases[] = {
	"KOI8-R",
	"KOI8R",
	"CSKOI8R",
	NULL
};

static const gchar *const ibm866_aliases[] = {
	"IBM866",
	"CP866",
	"866",
	"CSCP866",
	NULL
};

/* Ukranian */
static const gchar *const koi8u_aliases[] = {
	"KOI-8-U",
	"KOI8U",
	NULL
};

static const gchar *const macuk_aliases[] = {
	"MAC-UK",
	"MACUK",
	"MACUKRANIAN",
	"MAC-CYRILLIC",
	"MACCYRILLIC",
	NULL
};


/* Encodings */
static EscoEncoding east_european_encodings[] = {
	{ESCO_ENCODING_GROUP_EAST_EUROPEAN,
	 N_("Baltic (ISO 8859-13)"), N_("Baltic: ISO 8859-13"),
	 iso8859_13_aliases, TRUE},
	{ESCO_ENCODING_GROUP_EAST_EUROPEAN,
	 N_("Baltic (ISO 8859-4)"), N_("Baltic: ISO 8859-4"),
	 iso8859_4_aliases, FALSE},
	{ESCO_ENCODING_GROUP_EAST_EUROPEAN,
	 N_("Baltic (Microsoft)"), N_("Baltic: Microsoft"),
	 cp1257_aliases, FALSE},

	{ESCO_ENCODING_GROUP_EAST_EUROPEAN,
	 N_("Cyrillic (ISO 8859-5)"), N_("Cyrillic: ISO 8853-5"),
	 iso8859_5_aliases, TRUE},
	{ESCO_ENCODING_GROUP_EAST_EUROPEAN,
	 N_("Cyrillic (IBM-855)"), N_("Cyrillic: IBM-855"),
	 ibm855_aliases, FALSE},
	{ESCO_ENCODING_GROUP_EAST_EUROPEAN,
	 N_("Cyrillic (KOI-8)"), N_("Cyrillic: KOI-8"),
	 koi8_aliases, FALSE},
	{ESCO_ENCODING_GROUP_EAST_EUROPEAN,
	 N_("Cyrillic (ISO IR-111)"), N_("Cyrillic: ISO IR-111"),
	 isoir111_aliases, FALSE},
	{ESCO_ENCODING_GROUP_EAST_EUROPEAN,
	 N_("Cyrillic (Microsoft)"), N_("Cyrillic: Microsoft"),
	 cp1251_aliases, FALSE},
	{ESCO_ENCODING_GROUP_EAST_EUROPEAN,
	 N_("Cyrillic (Apple)"), N_("Cyrillic: Apple"),
	 cp10007_aliases, FALSE},

	{ESCO_ENCODING_GROUP_EAST_EUROPEAN,
	 N_("Eastern (ISO 8859-2)"), N_("Eastern: ISO 8853-2"),
	 iso8859_2_aliases, TRUE},
	{ESCO_ENCODING_GROUP_EAST_EUROPEAN,
	 N_("Eastern (IBM-852)"), N_("Eastern: IBM-852"),
	 ibm852_aliases, FALSE},
	{ESCO_ENCODING_GROUP_EAST_EUROPEAN,
	 N_("Eastern (Microsoft)"), N_("Eastern: Microsoft"),
	 cp1250_aliases, FALSE},

	{ESCO_ENCODING_GROUP_EAST_EUROPEAN,
	 N_("Romainian (ISO 8859-16)"), N_("Romainian: ISO 8859-16"),
	 iso8859_16_aliases, TRUE},

	{ESCO_ENCODING_GROUP_EAST_EUROPEAN,
	 N_("Russian (KOI-8-R)"), N_("Russian: KOI-8-R"),
	 koi8r_aliases, TRUE},
	{ESCO_ENCODING_GROUP_EAST_EUROPEAN,
	 N_("Russian (IBM-866)"), N_("Russian: IBM-866"),
	 ibm866_aliases, FALSE},

	{ESCO_ENCODING_GROUP_EAST_EUROPEAN,
	 N_("Ukranian (KOI-8-U)"), N_("Ukranian: KOI-8-U"),
	 koi8u_aliases, TRUE},
	{ESCO_ENCODING_GROUP_EAST_EUROPEAN,
	 N_("Ukranian (Apple)"), N_("Ukranian: Apple"),
	 macuk_aliases, FALSE},

	{ESCO_ENCODING_GROUP_INVALID, NULL, NULL, NULL, FALSE}
};


/* Western European Data */
static const gchar *const iso8859_15_aliases[] = {
	"ISO8859-15",
	"ISO-8859-15",
	"ISO885915",
	"ISO_8859-15",
	"ISO_8859-15:1998",
	"ISO-IR-203",
	NULL
};

static const gchar *const iso8859_1_aliases[] = {
	"ISO-8859-1",
	"ISO-IR-100",
	"ISO_8859-1:1987",
	"ISO_8859-1",
	"ISO8859-1",
	"ISO88591",
	"8859_1",
	"LATIN1",
	"L1",
	"IBM819",
	"CP819",
	"CSISOLATIN1",
	"OSF00010001",
	NULL
};

static const gchar *const ibm850_aliases[] = {
	"CP850",
	"850",
	"IBM850",
	NULL
};

static const gchar *const cp1252_aliases[] = {
	"CP1252",
	"WINDOWS-1252",
	"MS-ANSI",
	NULL
};

static const gchar *const macroman_aliases[] = {
	"MACINTOSH",
	"MAC",
	"CSMACINTOSH",
	NULL
};

static const gchar *const iso8859_14_aliases[] = {
	"ISO-8859-14",
	"ISO8859-14",
	"ISO885914",
	"ISO_8859-14",
	"ISO_8859-14:1998",
	"ISO-IR-199",
	"LATIN8",
	"L8",
	"ISO-CELTIC",
	NULL
};

static const gchar *const iso8859_7_aliases[] = {
	"ISO-8859-7",
	"ISO8859-7",
	"ISO88597",
	"ISO_8859-7",
	"ISO_8859-7:1987",
	"8859_7",
	"ELOT_928",
	"EMCA-118",
	"GREEK",
	"GREEK8",
	"CSISOLATINGREEK",
	"OSF00010007",
	"IBM813",
	"CP813",
	NULL
};

static const gchar *const cp1253_aliases[] = {
	"CP1253",
	"WINDOWS-1253",
	"MS-GREEK",
	NULL
};

static const gchar *const iso8859_10_aliases[] = {
	"ISO-8859-10",
	"ISO8859-10",
	"ISO885910",
	"ISO_8859-10",
	"ISO_8859-10:1992",
	"ISO-IR-157",
	"LATIN6",
	"L6",
	"CSISOLATIN6",
	"OSF00010003A",
	NULL
};

static const gchar *const iso8859_3_aliases[] = {
	"ISO-8859-3",
	"ISO8859-3",
	"ISO88593",
	"ISO_8859-3",
	"ISO_8859-3:1988",
	"8859_3",
	"LATIN3",
	"L3",
	"CSISOLATIN3",
	"OSF00010003",
	NULL
};

static const gchar *const ascii_aliases[] = {
	"ANSI_X3.4-1968",
	"ANSI_X3.4",
	"ASCII",
	"US-ASCII",
	"US",
	"ISO-646-US",
	"ISO-IR-6",
	"ISO_646.IRV:1991",
	"IBM367",
	"CP367",
	"CSASCII",
	NULL
};


static EscoEncoding west_european_encodings[] = {
	{ESCO_ENCODING_GROUP_WEST_EUROPEAN,
	 N_("Celtic (ISO 8859-14)"), N_("Celtic: ISO 8859-14"),
	 iso8859_14_aliases, TRUE},

	{ESCO_ENCODING_GROUP_WEST_EUROPEAN,
	 N_("Greek (ISO 8859-7)"), N_("Greek: ISO 8859-7"),
	 iso8859_7_aliases, TRUE},
	{ESCO_ENCODING_GROUP_WEST_EUROPEAN,
	 N_("Greek (Microsoft)"), N_("Greek: Microsoft"),
	 cp1253_aliases, FALSE},

	{ESCO_ENCODING_GROUP_WEST_EUROPEAN,
	 N_("Nordic (ISO 8859-10)"), N_("Nordic: ISO 8859-10"),
	 iso8859_10_aliases, TRUE},

	{ESCO_ENCODING_GROUP_WEST_EUROPEAN,
	 N_("Southern (ISO 8859-3)"), N_("Southern: ISO 8859-3"),
	 iso8859_3_aliases, TRUE},

	{ESCO_ENCODING_GROUP_WEST_EUROPEAN,
	 N_("Western (ISO 8859-15)"), N_("Western: ISO 8859-15"),
	 iso8859_15_aliases, TRUE},
	{ESCO_ENCODING_GROUP_WEST_EUROPEAN,
	 N_("Western (ISO 8859-1)"), N_("Western: ISO 8859-1"),
	 iso8859_1_aliases, FALSE},
	{ESCO_ENCODING_GROUP_WEST_EUROPEAN,
	 N_("Western (IBM-850)"), N_("Western: IBM 850"),
	 ibm850_aliases, FALSE},
	{ESCO_ENCODING_GROUP_WEST_EUROPEAN,
	 N_("Western (Microsoft)"), N_("Western: Microsoft"),
	 cp1252_aliases, FALSE},
	{ESCO_ENCODING_GROUP_WEST_EUROPEAN,
	 N_("Western (Apple)"), N_("Western: Apple"),
	 macroman_aliases, FALSE},

	{ESCO_ENCODING_GROUP_WEST_EUROPEAN,
	 N_("English (ASCII)"), N_("English: ASCII"),
	 ascii_aliases, FALSE},

	{ESCO_ENCODING_GROUP_INVALID, NULL, NULL, NULL, FALSE}
};


/* Middle Eastern Data */
static const gchar *const iso8859_6_aliases[] = {
	"ISO-8859-6",
	"ISO8859-6",
	"ISO88596",
	"ISO_8859-6",
	"ISO_8859-6:1987",
	"8859_6",
	"ISO-IR-127",
	"ARABIC",
	"EMCA-114",
	"ASMO-708",
	"CSISOLATINARABIC",
	"OSF00010006",
	"IBM1089",
	"CP1089",
	NULL
};

static const gchar *const ibm864_aliases[] = {
	"IBM864",
	"CP864",
	"864",
	"CSIBM864",
	"OSF10020360",
	NULL
};

static const gchar *const cp1256_aliases[] = {
	"CP1256",
	"WINDOWS-1256",
	"MS-ARAB",
	NULL
};

static const gchar *const iso8859_8_aliases[] = {
	"ISO-8859-8",
	"ISO8859-8",
	"ISO88598",
	"8859_8",
	"ISO_8859-8",
	"ISO_8859-8:1988",
	"ISO-IR-138",
	"HEBREW",
	"CSISOLATINHEBREW",
	"IBM916",
	"CP916",
	"OSF00010008",
	NULL
};

static const gchar *const ibm862_aliases[] = {
	"IBM862",
	"CP862",
	"862",
	"CSPC862LATINHEBREW",
	"OSF1002035E",
	NULL
};

static const gchar *const cp1255_aliases[] = {
	"CP1255",
	"WINDOWS-1255",
	"MS-HEBR",
	NULL
};


static EscoEncoding middle_eastern_encodings[] = {
	{ESCO_ENCODING_GROUP_MIDDLE_EASTERN,
	 N_("Arabic (ISO 8859-6)"), N_("Arabic: ISO 8859-6"),
	 iso8859_6_aliases, TRUE},
	{ESCO_ENCODING_GROUP_MIDDLE_EASTERN,
	 N_("Arabic (IBM 864)"), N_("Arabic: IBM 864"),
	 ibm864_aliases, FALSE},
	{ESCO_ENCODING_GROUP_MIDDLE_EASTERN,
	 N_("Arabic (Microsoft)"), N_("Arabic: Microsoft"),
	 cp1256_aliases, FALSE},

	{ESCO_ENCODING_GROUP_MIDDLE_EASTERN,
	 N_("Hebrew (ISO 8859-8)"), N_("Hebrew: ISO 8859-8"),
	 iso8859_8_aliases, TRUE},
	{ESCO_ENCODING_GROUP_MIDDLE_EASTERN,
	 N_("Hebrew (IBM-862)"), N_("Hebrew: IBM-862"),
	 ibm862_aliases, FALSE},
	{ESCO_ENCODING_GROUP_MIDDLE_EASTERN,
	 N_("Hebrew (Microsoft)"), N_("Hebrew: Microsoft"),
	 cp1255_aliases, FALSE},

	{ESCO_ENCODING_GROUP_INVALID, NULL, NULL, NULL, FALSE}
};


static const EscoEncodingGroup language_groups[] = {
	{ESCO_ENCODING_GROUP_UNICODE, N_("Universal"), unicode_encodings},
	{ESCO_ENCODING_GROUP_EAST_ASIAN, N_("Eastern Asia"), east_asian_encodings},
	{ESCO_ENCODING_GROUP_WEST_ASIAN, N_("Western Asia"), west_asian_encodings},
	{ESCO_ENCODING_GROUP_EAST_EUROPEAN, N_("Eastern Europe"), east_european_encodings},
	{ESCO_ENCODING_GROUP_WEST_EUROPEAN, N_("Western Europe"), west_european_encodings},
	{ESCO_ENCODING_GROUP_MIDDLE_EASTERN, N_("Middle East"), middle_eastern_encodings}
};


/* Global Vars */
static GHashTable *encoding_table = NULL;


/* Private API */
static gint
compare_encodings (const EscoEncoding * encoding1,
				   const EscoEncoding * encoding2)
{
	return strcmp (encoding1->list_name, encoding2->list_name);
}


static void
encoding_table_init (void)
{
	if (encoding_table == NULL)
	{
		EscoEncoding *enc_info;
		gchar **aliases;
		gint i,
		  i2,
		  i3;

		encoding_table = g_hash_table_new (esco_encoding_alias_hash, esco_encoding_alias_equal);

		for (i = 0; i < G_N_ELEMENTS (language_groups); i++)
		{
			for (i2 = 0;
				 language_groups[i].encodings[i2].group != ESCO_ENCODING_GROUP_INVALID;
				 i2++)
			{
				aliases = (gchar **) language_groups[i].encodings[i2].aliases;
				enc_info = ESCO_ENCODING (&(language_groups[i].encodings[i2]));

				for (i3 = 0; aliases[i3] != NULL; i3++)
				{
					g_hash_table_insert (encoding_table, aliases[i3], enc_info);
				}
			}
		}
	}
}


/* Public API */
GSList *
esco_encoding_get_all_groups (void)
{
	GSList *retval = NULL;
	gint i;

	for (i = 0; i < G_N_ELEMENTS (language_groups); i++)
	{
		retval = g_slist_prepend (retval, (gpointer) & (language_groups[i]));
	}

	return g_slist_reverse (retval);
}


G_CONST_RETURN EscoEncodingGroup *
esco_encoding_get_group (EscoEncodingGroupType group)
{
	EscoEncodingGroup *retval = NULL;
	gint i;

	g_return_val_if_fail (group > 0 && group < ESCO_ENCODING_GROUP_LAST, NULL);

	for (i = 0; i < G_N_ELEMENTS (language_groups) && retval == NULL; i++)
	{
		if (group == language_groups[i].group)
		{
			retval = ESCO_ENCODING_GROUP (&(language_groups[i]));
		}
	}

	return retval;
}


G_CONST_RETURN EscoEncodingGroup *
esco_encoding_get_group_by_alias (const gchar * encoding)
{
	EscoEncodingGroup *retval = NULL;
	EscoEncoding *enc_data = NULL;

	g_return_val_if_fail (encoding != NULL, NULL);

	enc_data = g_hash_table_lookup (encoding_table, encoding);

	if (enc_data != NULL)
		retval = ESCO_ENCODING_GROUP (esco_encoding_get_group (enc_data->group));

	return retval;
}


G_CONST_RETURN EscoEncoding *
esco_encoding_get_encoding_by_alias (const gchar * encoding)
{
	g_return_val_if_fail (encoding != NULL, NULL);

	return ESCO_ENCODING (g_hash_table_lookup (encoding_table, encoding));
}


GSList *
esco_encoding_get_recommended_encodings (void)
{
	GSList *retval = NULL;
	gint i,
	  i2;

	for (i = 0; i < G_N_ELEMENTS (language_groups); i++)
	{
		for (i2 = 0;
			 language_groups[i].encodings[i2].group != ESCO_ENCODING_GROUP_INVALID; i2++)
		{
			if (language_groups[i].encodings[i2].recommended)
			{
				retval =
					g_slist_prepend (retval,
									 (gpointer) & (language_groups[i].encodings[i2]));
			}
		}
	}

	return g_slist_sort (retval, (GCompareFunc) compare_encodings);
}


gboolean
esco_encoding_is_valid (const gchar * encoding)
{
	if (encoding == NULL || encoding[0] == '\0')
		return FALSE;

	encoding_table_init ();

	return (g_hash_table_lookup (encoding_table, encoding) != NULL);
}


guint
esco_encoding_alias_hash (gconstpointer encoding)
{
	gchar *key = g_ascii_strup ((gchar *) encoding, -1);
	guint retval = g_str_hash (key);

	if (key)
		g_free (key);

	return retval;
}


gboolean
esco_encoding_alias_equal (gconstpointer encoding1,
						   gconstpointer encoding2)
{
	return (g_ascii_strcasecmp (encoding1, encoding2) == 0);
}
