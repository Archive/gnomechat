/*
 *  GnomeChat: src/prefs.h
 *
 *  Copyright (c) 2002 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef __PREFS_H__
#define __PREFS_H__


#include "esco-enums.h"
#include "viewer-styles.h"

#include <gtk/gtkenums.h>


typedef enum
{
	/* The Enchilada */
	GNOMECHAT_WINDOW_MAIN,

	/* Dialogs */
	GNOMECHAT_WINDOW_CONNECT,
	GNOMECHAT_WINDOW_PREFS,
	// GNOMECHAT_WINDOW_CONNECT_PROGRESS,
	// GNOMECHAT_WINDOW_DOWNLOAD_PROGRESS,

	GNOMECHAT_WINDOW_LAST,
	GNOMECHAT_WINDOW_FIRST = GNOMECHAT_WINDOW_MAIN
}
GnomechatWindowID;


typedef enum
{
	GNOMECHAT_ALERT_REMOVE_NETWORK,
	GNOMECHAT_ALERT_REMOVE_SERVER,
	GNOMECHAT_ALERT_CLEAR_CHAT,

	GNOMECHAT_ALERT_LAST,
	GNOMECHAT_ALERT_FIRST = GNOMECHAT_ALERT_REMOVE_NETWORK
}
GnomechatAlertID;


/* FIXME: This will be moved when DCC gets implemented */
typedef enum
{
	GDCC_RECV_FILE_FILENAME,
	GDCC_RECV_FILE_SENDER_FOLDER,
	GDCC_RECV_FILE_SENDER_FILENAME,

	GDCC_RECV_FILE_LAST
}
GDccRecvFileNamingStyle;


/* ************** *
 *  Housekeeping  *
 * ************** */

void prefs_init (void);
void prefs_shutdown (void);


/* ************************ *
 *  Preferences Changesets  *
 * ************************ */

/* Prefs Changeset Initialization/Cancelling */
void prefs_dialog_opened (void);
void prefs_dialog_cancelled (void);
void prefs_dialog_closed (void);

/* Prefs Fonts Colors Changeset Initialization/Cancelling */
void prefs_fonts_colors_dialog_opened (void);
void prefs_fonts_colors_dialog_cancelled (void);
void prefs_fonts_colors_dialog_closed (void);

/* Prefs Network List Changeset Initialization/Cancelling */
void prefs_netedit_dialog_opened (void);
void prefs_netedit_dialog_cancelled (void);
void prefs_netedit_dialog_closed (void);


/* ******************* *
 *  Utility Functions  *
 * ******************* */

gchar *prefs_get_default_font (void);
gchar *prefs_get_monospace_font (void);
GSList *prefs_get_uri_prefixes (void);
gchar *prefs_get_unique_id (void);

gboolean prefs_get_first_run_completed (void);
void prefs_set_first_run_completed (gboolean completed);


/* **************************** *
 *  Generic Window Preferences  *
 * **************************** */

gint prefs_get_window_width (GnomechatWindowID id);
void prefs_set_window_width (GnomechatWindowID id,
							 gint width);
gint prefs_get_window_height (GnomechatWindowID id);
void prefs_set_window_height (GnomechatWindowID id,
							  gint height);


/* ************************* *
 *  Main Window Preferences  *
 * ************************* */

/* Force Size */
gboolean prefs_get_can_set_main_window_force_size (void);
gboolean prefs_get_main_window_force_size (void);
void prefs_set_main_window_force_size (gboolean force_size);

/* Force Window Position */
gboolean prefs_get_can_set_main_window_force_position (void);
gboolean prefs_get_main_window_force_position (void);
void prefs_set_main_window_force_position (gboolean force_pos);

/* Left Edge */
gboolean prefs_get_can_set_main_window_left_edge (void);
gint prefs_get_main_window_left_edge (void);
void prefs_set_main_window_left_edge (gint left_edge);

/* Top Edge */
gboolean prefs_get_can_set_main_window_top_edge (void);
gint prefs_get_main_window_top_edge (void);
void prefs_set_main_window_top_edge (gint top_edge);

/* Window Layout */
gboolean prefs_get_can_set_main_window_layout (void);
EscoWindowLayoutType prefs_get_main_window_layout (void);
void prefs_set_main_window_layout (EscoWindowLayoutType layout);

/* Notebook Tabs */
gboolean prefs_get_can_set_main_window_tab_side (void);
GtkPositionType prefs_get_main_window_tab_side (void);
void prefs_set_main_window_tab_side (GtkPositionType pos);

/* Show Chats Tree */
gboolean prefs_get_main_window_show_chats_tree (void);
void prefs_set_main_window_show_chats_tree (gboolean show_tree);

/* Chats Tree Size */
gint prefs_get_main_window_chats_tree_size (void);
void prefs_set_main_window_chats_tree_size (gint size);

/* Show User List */
gboolean prefs_get_main_window_show_user_list (void);
void prefs_set_main_window_show_user_list (gboolean show_list);

/* User List Size */
gint prefs_get_main_window_user_list_size (void);
void prefs_set_main_window_user_list_size (gint size);

/* Channel Topic In Titlebar -- read-only, set with gconf-editor or gconftool-2. */
gboolean prefs_get_main_window_topic_in_titlebar (void);

/* GtkTextView for Main Entry -- Deprecated && pointless */
gboolean prefs_get_use_multiline_entry (void);
void prefs_set_use_multiline_entry (gboolean use_multiline);


/* **************************** *
 *  Connect Dialog Preferences  *
 * **************************** */

/* Show the MOTD dialog when a connection has been made */
gboolean prefs_get_can_set_show_motd_on_connect (void);
gboolean prefs_get_show_motd_on_connect (void);
void prefs_set_show_motd_on_connect (gboolean show);

/* Show the network info pane */
gboolean prefs_get_connect_show_info (void);
void prefs_set_connect_show_info (gboolean show_info);

/* Info pane size */
gint prefs_get_connect_info_size (void);
void prefs_set_connect_info_size (gint size);

/* Last-selected network */
gchar *prefs_get_connect_selected_network (void);
void prefs_set_connect_selected_network (const gchar *network);


/* ************************** *
 *  Prefs Dialog Preferences  *
 * ************************** */

/* Windows Page - More Options disclosure -- Deprecated */
gboolean prefs_get_prefs_show_window_options (void);
void prefs_set_prefs_show_window_options (gboolean show);


/* ************************* *
 *  Find Dialog Preferences  *
 * ************************* */

/* Case-Insensitive Searches */
gboolean prefs_get_case_insensitive_find (void);
void prefs_set_case_insensitive_find (gboolean insensitive);

/* Last-Used Search */
gchar *prefs_get_last_find (void);
void prefs_set_last_find (const gchar * str);


/* ********************* *
 *  Confirmation Alerts  *
 * ********************* */

gboolean prefs_get_can_set_show_alert (GnomechatAlertID id);
gboolean prefs_get_show_alert (GnomechatAlertID id);
void prefs_set_show_alert (GnomechatAlertID id,
						   gboolean show);


/* ************************** *
 *  Notification Preferences  *
 * ************************** */

/* Notification Colors (tab/menuitem/listitem highlighting) */
gboolean prefs_get_can_set_notification_color (EscoPesterType pester);
gchar *prefs_get_notification_color (EscoPesterType pester);
void prefs_set_notification_color (EscoPesterType pester,
								   const gchar * color);

/* Tray Icon */
gboolean prefs_get_can_set_show_tray_icon (void);
gboolean prefs_get_show_tray_icon (void);
void prefs_set_show_tray_icon (gboolean show);


/* ******************** *
 *  Viewer Preferences  *
 * ******************** */

/* Timestamps */
gboolean prefs_get_can_set_show_timestamps (void);
gboolean prefs_get_viewer_show_timestamps (void);
void prefs_set_viewer_show_timestamps (gboolean show_timestamps);

/* Smileys */
gboolean prefs_get_can_set_use_image_smileys (void);
gboolean prefs_get_use_image_smileys (void);
void prefs_set_use_image_smileys (gboolean use_image_smileys);


/* ********************** *
 *  Identity Preferences  *
 * ********************** */

gboolean prefs_get_can_set_nick (void);
gchar *prefs_get_nick (void);

gboolean prefs_get_can_set_username (void);
gchar *prefs_get_realname (void);

gboolean prefs_get_can_set_realname (void);
gchar *prefs_get_username (void);

gboolean prefs_get_can_set_photo_file (void);
gchar *prefs_get_photo_file (void);

void prefs_set_identity (const gchar * nick,
						 const gchar * realname,
						 const gchar * username,
						 const gchar * photo_file);


/* ***************** *
 *  DCC Preferences  *
 * ***************** */

/* DCC Send Location */
gchar *prefs_get_dcc_send_folder (void);
void prefs_set_dcc_send_folder (const gchar * path);

/* DCC File Naming Style */
gboolean prefs_get_can_set_dcc_receieved_files_naming (void);
GDccRecvFileNamingStyle prefs_get_dcc_received_files_naming (void);
void prefs_set_dcc_received_files_naming (GDccRecvFileNamingStyle style);

/* DCC Recv Location */
gboolean prefs_get_can_set_dcc_receieve_folder (void);
gchar *prefs_get_dcc_receive_folder (void);
void prefs_set_dcc_receive_folder (const gchar * path);


/* ***************************** *
 *  Viewer Styles -- Deprecated  *
 * ***************************** */

/* Global Viewer Style */
gboolean prefs_get_can_set_viewer_font (void);
gchar *prefs_get_viewer_font (void);
void prefs_set_viewer_font (const gchar * font);

gboolean prefs_get_can_set_viewer_fg (void);
gchar *prefs_get_viewer_fg (void);
void prefs_set_viewer_fg (const gchar * fg_color);

gboolean prefs_get_can_set_viewer_bg (void);
gchar *prefs_get_viewer_bg (void);
void prefs_set_viewer_bg (const gchar * bg_color);

/* Other Viewer Styles */
gboolean prefs_get_can_set_viewer_style_fg (ViewerStyleType type);
gchar *prefs_get_viewer_style_fg (ViewerStyleType style);
void prefs_set_viewer_style_fg (ViewerStyleType style,
								const gchar * fg_color);

gboolean prefs_get_can_set_viewer_style_bg (ViewerStyleType type);
gchar *prefs_get_viewer_style_bg (ViewerStyleType style);
void prefs_set_viewer_style_bg (ViewerStyleType style,
								const gchar * bg_color);

gboolean prefs_get_can_set_viewer_style_bold (ViewerStyleType type);
gboolean prefs_get_viewer_style_bold (ViewerStyleType style);
void prefs_set_viewer_style_bold (ViewerStyleType style,
								  gboolean bold);

gboolean prefs_get_can_set_viewer_style_uline (ViewerStyleType type);
gboolean prefs_get_viewer_style_uline (ViewerStyleType style);
void prefs_set_viewer_style_uline (ViewerStyleType style,
								   gboolean underline);

gboolean prefs_get_can_set_viewer_style_italic (ViewerStyleType type);
gboolean prefs_get_viewer_style_italic (ViewerStyleType style);
void prefs_set_viewer_style_italic (ViewerStyleType style,
									gboolean italic);

ViewerStyle *prefs_get_viewer_style (ViewerStyleType type);


/* **************************** *
 *  Network List -- Deprecated  *
 * **************************** */
 
/* Network List Functions */
gboolean prefs_get_can_set_network_ids (void);
GSList *prefs_get_network_ids (void);
void prefs_set_network_ids (GSList * list);

/* Network Item Functions */
void prefs_delete_network (const gchar * id);

gboolean prefs_get_can_set_network_name (const gchar * id);
gchar *prefs_get_network_name (const gchar * id);
void prefs_set_network_name (const gchar * id,
							 const gchar * name);

gboolean prefs_get_can_set_network_description (const gchar * id);
gchar *prefs_get_network_description (const gchar * id);
void prefs_set_network_description (const gchar * id,
									const gchar * description);

gboolean prefs_get_can_set_network_website (const gchar * id);
gchar *prefs_get_network_website (const gchar * id);
void prefs_set_network_website (const gchar * id,
								const gchar * website_url);

gboolean prefs_get_can_set_network_category (const gchar * id);
gchar *prefs_get_network_category (const gchar * id);
void prefs_set_network_category (const gchar * id,
								 const gchar * category);

gboolean prefs_get_can_set_network_server_ids (const gchar * id);
GSList *prefs_get_network_server_ids (const gchar * id);
void prefs_set_network_server_ids (const gchar * id,
								   GSList * server_ids);

gboolean prefs_get_can_set_network_channels (const gchar * id);
GSList *prefs_get_network_channels (const gchar * id);
void prefs_set_network_channels (const gchar * id,
								 GSList * channels);

gboolean prefs_get_can_set_network_charset (const gchar * id);
gchar *prefs_get_network_charset (const gchar * id);
void prefs_set_network_charset (const gchar * id,
								const gchar * charset);

gboolean prefs_get_can_set_network_autoconnect (const gchar * id);
gboolean prefs_get_network_autoconnect (const gchar * id);
void prefs_set_network_autoconnect (const gchar * id,
									gboolean autoconnect);

gboolean prefs_get_can_set_network_icon (const gchar * id);
gchar *prefs_get_network_icon (const gchar * id);
void prefs_set_network_icon (const gchar * id,
							 const gchar * icon);


/* Network Category Item Functions */
gboolean prefs_get_can_set_category_name (const gchar * category);
gchar *prefs_get_category_name (const gchar * category);
void prefs_set_category_name (const gchar * category,
							  const gchar * name);

gboolean prefs_get_can_set_category_description (const gchar * category);
gchar *prefs_get_category_description (const gchar * category);
void prefs_set_category_description (const gchar * category,
									 const gchar * description);

gboolean prefs_get_can_set_category_icon (const gchar * category);
gchar *prefs_get_category_icon (const gchar * category);
void prefs_set_category_icon (const gchar * category,
							  const gchar * icon);


/* Server Item Functions */
void prefs_delete_server (const gchar * net_id,
						  const gchar * server_id);

gboolean prefs_get_can_set_server_enabled (const gchar * net_id,
										   const gchar * server_id);
gboolean prefs_get_server_enabled (const gchar * net_id,
								   const gchar * server_id);
void prefs_set_server_enabled (const gchar * net_id,
							   const gchar * server_id,
							   gboolean enabled);

gboolean prefs_get_can_set_server_address (const gchar * net_id,
										   const gchar * server_id);
gchar *prefs_get_server_address (const gchar * net_id,
								 const gchar * server_id);
void prefs_set_server_address (const gchar * net_id,
							   const gchar * server_id,
							   const gchar * server_address);

gboolean prefs_get_can_set_server_port (const gchar * net_id,
										const gchar * server_id);
gint prefs_get_server_port (const gchar * net_id,
							const gchar * server_id);
void prefs_set_server_port (const gchar * net_id,
							const gchar * server_id,
							gint port);

gboolean prefs_get_can_set_server_passwd (const gchar * net_id,
										  const gchar * server_id);
gchar *prefs_get_server_passwd (const gchar * net_id,
								const gchar * server_id);
void prefs_set_server_passwd (const gchar * net_id,
							  const gchar * server_id,
							  const gchar * passwd);

gboolean prefs_get_can_set_server_charset (const gchar * net_id,
										   const gchar * server_id);
gchar *prefs_get_server_charset (const gchar * net_id,
								 const gchar * server_id);
void prefs_set_server_charset (const gchar * net_id,
							   const gchar * server_id,
							   const gchar * charset);

gboolean prefs_get_can_set_server_icon (const gchar * net_id,
										const gchar * server_id);
gchar *prefs_get_server_icon (const gchar * net_id,
							  const gchar * server_id);
void prefs_set_server_icon (const gchar * net_id,
							const gchar * server_id,
							const gchar * icon);

gboolean prefs_get_can_set_server_location (const gchar * net_id,
											const gchar * server_id);
gchar *prefs_get_server_location (const gchar * net_id,
								  const gchar * server_id);
void prefs_set_server_location (const gchar * net_id,
								const gchar * server_id,
								const gchar * location);

gboolean prefs_get_can_set_server_country_code (const gchar * net_id,
												const gchar * server_id);
gchar *prefs_get_server_country_code (const gchar * net_id,
									  const gchar * server_id);
void prefs_set_server_country_code (const gchar * net_id,
									const gchar * server_id,
									const gchar * country_code);

#endif /* __PREFS_H__ */
