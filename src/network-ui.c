/*
 *  GnomeChat: src/network-ui.c
 *
 *  Copyright (c) 2003 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "gnomechat.h"
#include "network-ui.h"

#include "eggtreemodelfilter.h"
#include "esco-encoding-picker.h"
#include "gnomechat-rdf-model.h"
#include "pixbufs.h"
#include "stock-items.h"
#include "utils.h"

#include <gtk/gtkalignment.h>
#include <gtk/gtkcellrendererpixbuf.h>
#include <gtk/gtkcellrenderertext.h>
#include <gtk/gtkcheckbutton.h>
#include <gtk/gtkdialog.h>
#include <gtk/gtkentry.h>
#include <gtk/gtkeventbox.h>
#include <gtk/gtkimage.h>
#include <gtk/gtkhbox.h>
#include <gtk/gtkhpaned.h>
#include <gtk/gtklabel.h>
#include <gtk/gtkliststore.h>
#include <gtk/gtkmessagedialog.h>
#include <gtk/gtknotebook.h>
#include <gtk/gtktextview.h>
#include <gtk/gtktreeview.h>
#include <gtk/gtkscrolledwindow.h>
#include <gtk/gtksizegroup.h>
#include <gtk/gtkstock.h>
#include <gtk/gtkvbox.h>
#include <gtk/gtkvbbox.h>

#include <libgnomeui/gnome-href.h>

/* Valid DnD Type IDs */
enum
{
	TEXT__URI_LIST,
	TEXT__X_MOZ_URL,
	IMAGE__PNG,
	TEXT__PLAIN
};


static GHashTable *edit_dialogs = NULL;


/* ************************************************************************** *
 *  Server Editing Dialog                                                     *
 * ************************************************************************** */

typedef struct
{
	gchar *key;

	GnomechatRdfServer *server;

	GtkWidget *uri_entry;

	GtkTreeSelection *ports_sel;
	GtkWidget *ports_remove_button;

	GtkWidget *encoding_picker;
}
ServerEditDialog;


/* static void
open_server_edit_dialog (GtkWindow * parent,
						 GnomechatRdfModel * rdf,
						 GnomechatRdfServer * server)
{
} */


/* ************************************************************************** *
 *  Network Editing Dialog                                                    *
 * ************************************************************************** */

typedef struct
{
	gchar *key;

	GObject *network;

	GtkWidget *win;

	GtkWidget *image;
	gchar *image_uri;
	GtkWidget *name_label;

	GtkWidget *uri_entry;
	GtkWidget *description_textview;
	GtkWidget *website_entry;
	GtkWidget *encoding_picker;

	GtkTreeSelection *servers_sel;
	GtkWidget *servers_edit_button;
	GtkWidget *servers_remove_button;

	GtkTreeSelection *channels_sel;
	GtkWidget *channels_remove_button;
}
NetworkEditDialog;


/* ******************* *
 *  Utility Functions  *
 * ******************* */

static void
netedit_dialog_free (NetworkEditDialog * dialog)
{
	g_free (dialog->key);

	if (dialog->network != NULL)
		g_object_unref (dialog->network);

	if (GTK_IS_WIDGET (dialog->win))
		gtk_widget_destroy (dialog->win);

	g_free (dialog);
}


/* ********************** *
 *  Sub-Widget Callbacks  *
 * ********************** */

static void
netedit_response_cb (GtkWidget * win, gint response, NetworkEditDialog * dialog)
{
	if (response == GTK_RESPONSE_OK)
	{
		/* Save the network here. */
	}

	g_hash_table_remove (edit_dialogs, dialog->key);
}


/* Information Page */
static void
netedit_evbox_drag_data_received_cb (GtkWidget * widget,
									 GdkDragContext * dc,
									 gint x,
									 gint y,
									 GtkSelectionData * selection_data,
									 guint data_type, guint time, NetworkEditDialog * dialog)
{
	switch (data_type)
	{
	case TEXT__URI_LIST:
		g_message ("text/uri-list");
		break;

	case TEXT__X_MOZ_URL:
		g_message ("text/x-moz-url");
		break;

	case IMAGE__PNG:
		g_message ("image/png");
		break;

	default:
		g_assert_not_reached ();
		break;
	}
}


static void
netedit_name_changed_cb (GtkEntry * entry, NetworkEditDialog * dialog)
{
	gchar *name,
	 *title;

	name = util_get_name_from_uri (gtk_entry_get_text (entry));
	gtk_label_set_text (GTK_LABEL (dialog->name_label), name);

	title = g_strdup_printf (_("Edit %s - IRC Chat"), name);
	g_free (name);

	gtk_window_set_title (GTK_WINDOW (dialog->win), title);
	g_free (title);
}


/* Servers Page */
static void
netedit_servers_sel_changed_cb (GtkTreeSelection * sel, NetworkEditDialog * dialog)
{
	gboolean sensitive = gtk_tree_selection_get_selected (sel, NULL, NULL);

	gtk_widget_set_sensitive (dialog->servers_edit_button, sensitive);
	gtk_widget_set_sensitive (dialog->servers_remove_button, sensitive);
}


static void
netedit_servers_add_button_clicked_cb (GtkButton * button, NetworkEditDialog * dialog)
{

}


static void
netedit_servers_edit_button_clicked_cb (GtkButton * button, NetworkEditDialog * dialog)
{

}


static void
netedit_servers_remove_button_clicked_cb (GtkButton * button, NetworkEditDialog * dialog)
{

}


/* Channels Page */
static void
netedit_channels_sel_changed_cb (GtkTreeSelection * sel, NetworkEditDialog * dialog)
{
	gtk_widget_set_sensitive (dialog->channels_remove_button,
							  gtk_tree_selection_get_selected (sel, NULL, NULL));
}


static void
netedit_channels_cell_edited_cb (GtkCellRendererText * cell,
								 const gchar * path,
								 const gchar * new_text, NetworkEditDialog * dialog)
{
	GtkTreeModel *model;
	GtkTreeIter iter;

	if (gtk_tree_selection_get_selected (dialog->channels_sel, &model, &iter))
	{
		gtk_list_store_set (GTK_LIST_STORE (model), &iter, 0, new_text, -1);
	}
}


static void
netedit_channels_add_button_clicked_cb (GtkButton * button, NetworkEditDialog * dialog)
{
	GtkTreeModel *model = gtk_tree_view_get_model (dialog->channels_sel->tree_view);
	GtkTreePath *path;
	GtkTreeIter iter;

	gtk_list_store_append (GTK_LIST_STORE (model), &iter);
	gtk_list_store_set (GTK_LIST_STORE (model), &iter, 0, _("#Channel"), -1);

	path = gtk_tree_model_get_path (model, &iter);

	if (path != NULL)
	{
		GtkTreeViewColumn *col = gtk_tree_view_get_column (dialog->channels_sel->tree_view, 0);

		gtk_tree_view_set_cursor (dialog->channels_sel->tree_view, path, col, TRUE);
		gtk_tree_path_free (path);
	}
}


static void
netedit_channels_remove_button_clicked_cb (GtkButton * button, NetworkEditDialog * dialog)
{
	GtkTreeModel *model;
	GtkTreeIter iter;

	if (gtk_tree_selection_get_selected (dialog->channels_sel, &model, &iter))
	{
		gtk_list_store_remove (GTK_LIST_STORE (model), &iter);
	}
}


/* ***************** *
 *  Dialog Creation  *
 * ***************** */

static inline NetworkEditDialog *
netedit_create_dialog (GtkWindow * parent, GtkTreeModel * rdf_model, GnomechatRdfNetwork * network)
{
	GtkWidget *main_box,
	 *notebook,
	 *contents_box,
	 *hbox,
	 *bbox,
	 *evbox,
	 *scrwin,
	 *label,
	 *button,
	 *tree_view;
	PangoFontDescription *font_desc;
	GtkTreeViewColumn *col;
	GtkCellRenderer *cell;
	GtkListStore *channels_store;
	GtkSizeGroup *label_group,
	 *widget_group;
	const gchar *uri;

	NetworkEditDialog *dialog = g_new0 (NetworkEditDialog, 1);
	gchar *name = NULL,
		*title = NULL;

	static const GtkTargetEntry image_targets[] = {
		{"text/uri-list", 0, TEXT__URI_LIST},
		{"text/x-moz-url", 0, TEXT__X_MOZ_URL},
		{"image/png", 0, IMAGE__PNG}
	};


	if (network != NULL)
	{
		uri = gnomechat_rdf_network_get_uri (network);
		dialog->key = g_strdup (uri);

		name = util_get_name_from_uri (uri);
		title = g_strdup_printf (_("Edit %s - %s"), name, GENERIC_NAME);

		dialog->network = g_object_ref (network);
	}
	else
	{
		uri = NULL;
		dialog->key = g_strdup_printf ("new-%u", g_random_int ());

		name = g_strdup (_("New Network"));
		title = g_strdup_printf ("%s - %s", name, GENERIC_NAME);
	}

	label_group = gtk_size_group_new (GTK_SIZE_GROUP_HORIZONTAL);
	widget_group = gtk_size_group_new (GTK_SIZE_GROUP_HORIZONTAL);

	dialog->win = gtk_dialog_new_with_buttons (title, parent, (GTK_DIALOG_DESTROY_WITH_PARENT
															   | GTK_DIALOG_NO_SEPARATOR),
											   GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
											   GTK_STOCK_OK, GTK_RESPONSE_OK, NULL);
	g_free (title);
	gtk_container_set_border_width (GTK_CONTAINER (dialog->win), 5);
	gtk_dialog_set_default_response (GTK_DIALOG (dialog->win), GTK_RESPONSE_OK);
	util_set_window_icon_from_stock (GTK_WINDOW (dialog->win), GNOMECHAT_STOCK_SERVER_PROPERTIES);
	g_signal_connect (dialog->win, "response", G_CALLBACK (netedit_response_cb), dialog);

	main_box = gtk_vbox_new (FALSE, 18);
	gtk_container_set_border_width (GTK_CONTAINER (main_box), 5);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (dialog->win)->vbox), main_box);
	gtk_widget_show (main_box);

	hbox = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (main_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	evbox = gtk_event_box_new ();
	gtk_box_pack_start (GTK_BOX (hbox), evbox, FALSE, FALSE, 0);
	gtk_drag_dest_set (evbox, (GTK_DEST_DEFAULT_MOTION | GTK_DEST_DEFAULT_HIGHLIGHT
							   | GTK_DEST_DEFAULT_DROP),
					   image_targets, G_N_ELEMENTS (image_targets),
					   (GDK_ACTION_COPY | GDK_ACTION_MOVE | GDK_ACTION_LINK));
	g_signal_connect (evbox, "drag-data-received", G_CALLBACK (netedit_evbox_drag_data_received_cb),
					  dialog);
	gtk_widget_show (evbox);

	dialog->image = gtk_image_new_from_stock (GNOMECHAT_STOCK_NETWORK, GTK_ICON_SIZE_DIALOG);
	gtk_container_add (GTK_CONTAINER (evbox), dialog->image);
	gtk_widget_show (dialog->image);

	dialog->name_label = gtk_label_new (name);
	g_free (name);
	gtk_misc_set_alignment (GTK_MISC (dialog->name_label), 0.0, 0.5);
	gtk_container_add (GTK_CONTAINER (hbox), dialog->name_label);
	gtk_widget_show (dialog->name_label);

	font_desc = pango_font_description_copy (dialog->name_label->style->font_desc);
	pango_font_description_set_weight (font_desc, PANGO_WEIGHT_BOLD);
	pango_font_description_set_size (font_desc,
									 pango_font_description_get_size (font_desc) *
									 PANGO_SCALE_XX_LARGE);
	gtk_widget_modify_font (dialog->name_label, font_desc);
	pango_font_description_free (font_desc);

	notebook = gtk_notebook_new ();
	gtk_container_add (GTK_CONTAINER (main_box), notebook);
	gtk_widget_show (notebook);

	/* Information Page */
	label = gtk_label_new (_("Information"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_widget_show (label);

	contents_box = gtk_vbox_new (FALSE, 6);
	gtk_container_set_border_width (GTK_CONTAINER (contents_box), 12);
	gtk_notebook_append_page_menu (GTK_NOTEBOOK (notebook), contents_box, label, label);
	gtk_widget_show (contents_box);

	/* URI */
	hbox = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (contents_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new_with_mnemonic (_("_Location:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_misc_set_padding (GTK_MISC (label), 0, 2);
	gtk_size_group_add_widget (label_group, label);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	dialog->uri_entry = gtk_entry_new ();
	gtk_entry_set_text (GTK_ENTRY (dialog->uri_entry), uri);
	g_signal_connect (dialog->uri_entry, "changed", G_CALLBACK (netedit_name_changed_cb), dialog);
	util_set_label_widget_pair (label, dialog->uri_entry);
	gtk_size_group_add_widget (widget_group, dialog->uri_entry);
	gtk_container_add (GTK_CONTAINER (hbox), dialog->uri_entry);
	gtk_widget_show (dialog->uri_entry);

	/* Description */
	hbox = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (contents_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new_with_mnemonic (_("_Description:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.0);
	gtk_misc_set_padding (GTK_MISC (label), 0, 4);
	gtk_size_group_add_widget (label_group, label);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	scrwin = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrwin), GTK_SHADOW_IN);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrwin), GTK_POLICY_NEVER,
									GTK_POLICY_AUTOMATIC);
	gtk_size_group_add_widget (widget_group, scrwin);
	gtk_container_add (GTK_CONTAINER (hbox), scrwin);
	gtk_widget_show (scrwin);

	dialog->description_textview = gtk_text_view_new ();
	gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW (dialog->description_textview), GTK_WRAP_WORD);
	gtk_container_add (GTK_CONTAINER (scrwin), dialog->description_textview);
	gtk_widget_show (dialog->description_textview);

	/* Website */
	hbox = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (contents_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new_with_mnemonic (_("_Website:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_size_group_add_widget (label_group, label);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	dialog->website_entry = gtk_entry_new ();
	gtk_container_add (GTK_CONTAINER (hbox), dialog->website_entry);
	gtk_size_group_add_widget (widget_group, dialog->website_entry);
	gtk_widget_show (dialog->website_entry);

	/* Encoding */
	hbox = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (contents_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new_with_mnemonic (_("_Character Coding:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_size_group_add_widget (label_group, label);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	dialog->encoding_picker = esco_encoding_picker_new (_("Select A Character Coding - IRC Chat"));
	gtk_container_add (GTK_CONTAINER (hbox), dialog->encoding_picker);
	gtk_size_group_add_widget (widget_group, dialog->encoding_picker);
	gtk_widget_show (dialog->encoding_picker);

	/* Servers Page */
	label = gtk_label_new (_("Servers"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_widget_show (label);

	contents_box = gtk_hbox_new (FALSE, 12);
	gtk_container_set_border_width (GTK_CONTAINER (contents_box), 12);
	gtk_notebook_append_page_menu (GTK_NOTEBOOK (notebook), contents_box, label, label);
	gtk_widget_show (contents_box);

	scrwin = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrwin), GTK_SHADOW_IN);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrwin),
									GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
	gtk_container_add (GTK_CONTAINER (contents_box), scrwin);
	gtk_widget_show (scrwin);

	tree_view = gtk_tree_view_new ();
	gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (tree_view), FALSE);
	gtk_container_add (GTK_CONTAINER (scrwin), tree_view);
	gtk_widget_show (tree_view);

	dialog->servers_sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree_view));
	g_signal_connect (dialog->servers_sel, "changed", G_CALLBACK (netedit_servers_sel_changed_cb),
					  dialog);

	col = gtk_tree_view_column_new ();
	gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view), col);

	cell = gtk_cell_renderer_pixbuf_new ();
	gtk_tree_view_column_pack_start (col, cell, FALSE);
	gtk_tree_view_column_set_cell_data_func (col, cell, gnomechat_rdf_model_render_icon, NULL,
											 NULL);

	cell = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_start (col, cell, TRUE);
	gtk_tree_view_column_set_cell_data_func (col, cell, gnomechat_rdf_model_render_name, NULL,
											 NULL);

	bbox = gtk_vbutton_box_new ();
	gtk_button_box_set_layout (GTK_BUTTON_BOX (bbox), GTK_BUTTONBOX_START);
	gtk_box_pack_start (GTK_BOX (contents_box), bbox, FALSE, FALSE, 0);
	gtk_widget_show (bbox);

	button = gtk_button_new_from_stock (GTK_STOCK_ADD);
	g_signal_connect (button, "clicked", G_CALLBACK (netedit_servers_add_button_clicked_cb),
					  dialog);
	gtk_box_pack_start (GTK_BOX (bbox), button, FALSE, FALSE, 0);
	gtk_widget_show (button);

	dialog->servers_edit_button = gtk_button_new_from_stock (GNOMECHAT_STOCK_EDIT);
	g_signal_connect (dialog->servers_edit_button, "clicked",
					  G_CALLBACK (netedit_servers_edit_button_clicked_cb), dialog);
	gtk_box_pack_start (GTK_BOX (bbox), dialog->servers_edit_button, FALSE, FALSE, 0);
	gtk_widget_show (dialog->servers_edit_button);

	dialog->servers_remove_button = gtk_button_new_from_stock (GTK_STOCK_REMOVE);
	g_signal_connect (dialog->servers_remove_button, "clicked",
					  G_CALLBACK (netedit_servers_remove_button_clicked_cb), dialog);
	gtk_box_pack_start (GTK_BOX (bbox), dialog->servers_remove_button, FALSE, FALSE, 0);
	gtk_widget_show (dialog->servers_remove_button);

	gtk_widget_set_sensitive (dialog->servers_edit_button, FALSE);
	gtk_widget_set_sensitive (dialog->servers_remove_button, FALSE);

	/* Channels Page */
	label = gtk_label_new (_("Channels"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_widget_show (label);

	contents_box = gtk_hbox_new (FALSE, 12);
	gtk_container_set_border_width (GTK_CONTAINER (contents_box), 12);
	gtk_notebook_append_page_menu (GTK_NOTEBOOK (notebook), contents_box, label, label);
	gtk_widget_show (contents_box);

	scrwin = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrwin), GTK_SHADOW_IN);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrwin),
									GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
	gtk_container_add (GTK_CONTAINER (contents_box), scrwin);
	gtk_widget_show (scrwin);

	channels_store = gtk_list_store_new (1, G_TYPE_STRING);

	tree_view = gtk_tree_view_new_with_model (GTK_TREE_MODEL (channels_store));
	gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (tree_view), FALSE);
	gtk_container_add (GTK_CONTAINER (scrwin), tree_view);
	gtk_widget_show (tree_view);

	dialog->channels_sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree_view));
	g_signal_connect (dialog->channels_sel, "changed", G_CALLBACK (netedit_channels_sel_changed_cb),
					  dialog);

	col = gtk_tree_view_column_new ();
	gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view), col);

	cell = g_object_new (GTK_TYPE_CELL_RENDERER_PIXBUF,
						 "stock-id", GNOMECHAT_STOCK_CHANNEL, "stock-size", GTK_ICON_SIZE_MENU,
						 NULL);
	gtk_tree_view_column_pack_start (col, cell, FALSE);

	cell = g_object_new (GTK_TYPE_CELL_RENDERER_TEXT, "editable", TRUE, NULL);
	g_signal_connect (cell, "edited", G_CALLBACK (netedit_channels_cell_edited_cb), dialog);
	gtk_tree_view_column_pack_start (col, cell, TRUE);
	gtk_tree_view_column_add_attribute (col, cell, "text", 0);

	bbox = gtk_vbutton_box_new ();
	gtk_button_box_set_layout (GTK_BUTTON_BOX (bbox), GTK_BUTTONBOX_START);
	gtk_box_pack_start (GTK_BOX (contents_box), bbox, FALSE, FALSE, 0);
	gtk_widget_show (bbox);

	button = gtk_button_new_from_stock (GTK_STOCK_ADD);
	g_signal_connect (button, "clicked", G_CALLBACK (netedit_channels_add_button_clicked_cb),
					  dialog);
	gtk_box_pack_start (GTK_BOX (bbox), button, FALSE, FALSE, 0);
	gtk_widget_show (button);

	dialog->channels_remove_button = gtk_button_new_from_stock (GTK_STOCK_REMOVE);
	g_signal_connect (dialog->channels_remove_button, "clicked",
					  G_CALLBACK (netedit_channels_remove_button_clicked_cb), dialog);
	gtk_box_pack_start (GTK_BOX (bbox), dialog->channels_remove_button, FALSE, FALSE, 0);
	gtk_widget_show (dialog->channels_remove_button);

	gtk_widget_set_sensitive (dialog->channels_remove_button, FALSE);

	g_object_unref (label_group);
	g_object_unref (widget_group);

	/* Fill in the widgets */
	if (network != NULL)
	{
		GSList *channels = NULL;
		GdkPixbuf *pixbuf;
		GtkTreeIter iter;
		const gchar *str;

		if (gnomechat_rdf_model_get_iter_from_data (GNOMECHAT_RDF_MODEL (rdf_model), &iter,
													G_OBJECT (network)))
		{
			GtkTreePath *path = gtk_tree_model_get_path (rdf_model, &iter);

			if (path != NULL)
			{
				GtkTreeModel *model = egg_tree_model_filter_new (rdf_model, path);

				gtk_tree_path_free (path);

				gtk_tree_view_set_model (dialog->servers_sel->tree_view, model);

				if (gtk_tree_model_get_iter_first (model, &iter))
					gtk_tree_selection_select_iter (dialog->servers_sel, &iter);

				g_object_unref (model);
			}
		}

		for (channels = gnomechat_rdf_network_get_channels (network); channels != NULL;
			 channels = channels->next)
		{
			gtk_list_store_append (channels_store, &iter);
			gtk_list_store_set (channels_store, &iter, 0, channels->data, -1);
		}

		if (gtk_tree_model_get_iter_first (GTK_TREE_MODEL (channels_store), &iter))
			gtk_tree_selection_select_iter (dialog->channels_sel, &iter);

		g_object_unref (channels_store);

		pixbuf = gnomechat_rdf_network_get_icon (network);
		if (pixbuf != NULL)
		{
			gtk_image_set_from_pixbuf (GTK_IMAGE (dialog->image), pixbuf);
		}
		else
		{
			gtk_image_set_from_stock (GTK_IMAGE (dialog->image), GNOMECHAT_STOCK_NETWORK,
									  GTK_ICON_SIZE_DIALOG);
		}

		str = gnomechat_rdf_network_get_description (network);
		if (str != NULL)
		{
			GtkTextBuffer *buffer =
				gtk_text_view_get_buffer (GTK_TEXT_VIEW (dialog->description_textview));
			GtkTextIter iter;

			gtk_text_buffer_get_start_iter (buffer, &iter);
			gtk_text_buffer_insert (buffer, &iter, str, -1);
		}

		str = gnomechat_rdf_network_get_website (network);
		if (str != NULL)
		{
			gtk_entry_set_text (GTK_ENTRY (dialog->website_entry), str);
		}

		str = gnomechat_rdf_network_get_encoding (network);
		if (str != NULL)
		{
			esco_encoding_picker_set_encoding (ESCO_ENCODING_PICKER (dialog->encoding_picker), str);
		}
		else
		{
			esco_encoding_picker_set_encoding (ESCO_ENCODING_PICKER (dialog->encoding_picker),
											   "WINDOWS-1252");
		}
	}

	return dialog;
}


void
network_ui_open_network_edit_dialog (GtkWindow * parent, GtkTreeModel * rdf_model,
									 GnomechatRdfNetwork * network)
{
	NetworkEditDialog *dialog = NULL;

	g_return_if_fail (GTK_IS_WINDOW (parent));
	g_return_if_fail (GTK_IS_TREE_MODEL (rdf_model));
	g_return_if_fail (network == NULL || GNOMECHAT_IS_RDF_NETWORK (network));

	if (edit_dialogs == NULL)
	{
		edit_dialogs = g_hash_table_new_full (g_str_hash, g_str_equal,
											  g_free, (GDestroyNotify) netedit_dialog_free);
	}
	else if (network != NULL)
	{
		dialog = g_hash_table_lookup (edit_dialogs, gnomechat_rdf_network_get_uri (network));
	}

	if (dialog != NULL)
	{
		gtk_window_present (GTK_WINDOW (dialog->win));
		return;
	}
	else
	{
		dialog = netedit_create_dialog (parent, rdf_model, network);
		g_hash_table_insert (edit_dialogs, g_strdup (dialog->key), dialog);
	}

	gtk_window_present (GTK_WINDOW (dialog->win));
}


/* ************************************************************************** *
 *  Connect Dialog                                                            *
 * ************************************************************************** */

typedef struct
{
	GtkWidget *win;

	GtkTreeModel *model;
	GtkTreeSelection *sel;

	GnomechatRdfNetwork *network;
	gulong network_notify_icon_id;
	gulong network_notify_uri_id;
	gulong network_notify_description_id;
	gulong network_notify_website_id;

	GtkWidget *info_box;

	GtkWidget *icon_image;
	GtkWidget *name_label;
	GtkWidget *description_label;

	GtkWidget *website_box;
	GtkWidget *website_href;
}
ConnectDialog;


static void
connect_dialog_response_cb (GtkWidget * win, gint response, ConnectDialog ** ptr)
{
	gtk_widget_destroy (win);
	if ((*ptr)->network != NULL)
		g_object_unref ((*ptr)->network);
	g_object_unref ((*ptr)->model);
	g_free (*ptr);
	*ptr = NULL;
}


static void
connect_paned_notify_position_cb (GtkPaned * paned, GParamSpec * pspec, gpointer data)
{
	prefs_set_connect_info_size (gtk_paned_get_position (paned));
}


static void
connect_rm_btn_clicked_cb (GtkButton * btn, ConnectDialog * dialog)
{
	GtkTreeModel *model;
	GtkTreeIter iter;

	if (gtk_tree_selection_get_selected (dialog->sel, &model, &iter))
	{
		GtkTreeIter real_iter;

		if (prefs_get_show_alert (GNOMECHAT_ALERT_REMOVE_NETWORK))
		{
			GtkWidget *alert,
			 *hbox,
			 *check;
			gchar *name,
			 *str;
			gint response;

			str = g_strdup_printf ("<span weight=\"bold\" size=\"larger\">%s</span>\n\n%s",
								   _("Remove %s?"), _("Removing a network will prevent future "
													  "connections to that network."));
			name = util_get_name_from_uri (gnomechat_rdf_network_get_uri (dialog->network));
			alert = gtk_message_dialog_new (GTK_WINDOW (dialog->win),
											GTK_DIALOG_DESTROY_WITH_PARENT,
											GTK_MESSAGE_WARNING, GTK_BUTTONS_NONE, str, name);
			gtk_container_set_border_width (GTK_CONTAINER (alert), 5);
			g_free (str);
			g_free (name);

			gtk_dialog_add_buttons (GTK_DIALOG (alert),
									GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
									GTK_STOCK_REMOVE, GTK_RESPONSE_OK, NULL);
			gtk_window_set_role (GTK_WINDOW (alert), "RemoveNetwork");

			str = g_strdup_printf (_("Remove Network - %s"), GENERIC_NAME);
			gtk_window_set_title (GTK_WINDOW (alert), str);
			g_free (str);

			util_set_window_icon_from_stock (GTK_WINDOW (alert), GTK_STOCK_DIALOG_WARNING);
			gtk_label_set_use_markup (GTK_LABEL (GTK_MESSAGE_DIALOG (alert)->label), TRUE);
			gtk_dialog_set_default_response (GTK_DIALOG (alert), GTK_RESPONSE_OK);

			hbox = gtk_hbox_new (FALSE, 0);
			gtk_box_pack_start (GTK_BOX (GTK_DIALOG (alert)->vbox), hbox, FALSE, FALSE, 6);
			gtk_widget_show (hbox);

			check = gtk_check_button_new_with_mnemonic (_("_Ask before removing networks "
														  "in the future"));
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check), TRUE);
			g_signal_connect (check, "toggled", G_CALLBACK (util_show_again_toggled_cb),
							  GUINT_TO_POINTER (GNOMECHAT_ALERT_REMOVE_NETWORK));
			gtk_box_pack_start (GTK_BOX (hbox), check, FALSE, FALSE, 12);
			gtk_widget_show (check);

			gtk_widget_set_sensitive (dialog->win, FALSE);
			response = gtk_dialog_run (GTK_DIALOG (alert));
			gtk_widget_set_sensitive (dialog->win, TRUE);
			gtk_widget_destroy (alert);

			if (response != GTK_RESPONSE_OK)
				return;
		}

		egg_tree_model_filter_convert_iter_to_child_iter (EGG_TREE_MODEL_FILTER (model), &real_iter,
														  &iter);

		gnomechat_rdf_model_remove (GNOMECHAT_RDF_MODEL (dialog->model), &real_iter);
	}
}


static void
connect_props_btn_clicked_cb (GtkButton * btn, ConnectDialog * dialog)
{
	if (gtk_tree_selection_get_selected (dialog->sel, NULL, NULL))
	{
		network_ui_open_network_edit_dialog (GTK_WINDOW (dialog->win), dialog->model,
											 dialog->network);
	}
}


static void
network_notify_icon_cb (GnomechatRdfNetwork * network, GParamSpec * pspec, ConnectDialog * dialog)
{
	const gchar *str = gnomechat_rdf_network_get_uri (dialog->network);

	if (str != NULL)
	{
		gchar *name = util_get_name_from_uri (str);

		gtk_label_set_text (GTK_LABEL (dialog->name_label), name);

		g_free (name);
	}
}


static void
network_notify_uri_cb (GnomechatRdfNetwork * network, GParamSpec * pspec, ConnectDialog * dialog)
{
	GdkPixbuf *pixbuf = gnomechat_rdf_network_get_icon (dialog->network);

	if (pixbuf != NULL)
	{
		GdkPixbuf *scaled_pixbuf = pixbuf_scale_to_stock_size (pixbuf, GTK_ICON_SIZE_DIALOG);

		gtk_image_set_from_pixbuf (GTK_IMAGE (dialog->icon_image), scaled_pixbuf);
		g_object_unref (scaled_pixbuf);
	}
	else
	{
		gtk_image_set_from_stock (GTK_IMAGE (dialog->icon_image), GNOMECHAT_STOCK_NETWORK,
								  GTK_ICON_SIZE_DIALOG);
	}
}


static void
network_notify_description_cb (GnomechatRdfNetwork * network, GParamSpec * pspec,
							   ConnectDialog * dialog)
{
	const gchar *str = gnomechat_rdf_network_get_description (dialog->network);

	if (str != NULL)
	{
		gtk_label_set_text (GTK_LABEL (dialog->description_label), str);
	}
	else
	{
		gtk_label_set_text (GTK_LABEL (dialog->description_label),
							"Information for this network is currently being downloaded. "
							"Please be patient.");
	}
}


static void
network_notify_website_cb (GnomechatRdfNetwork * network, GParamSpec * pspec,
						   ConnectDialog * dialog)
{
	const gchar *str = gnomechat_rdf_network_get_website (dialog->network);

	if (str != NULL)
	{
		gnome_href_set_text (GNOME_HREF (dialog->website_href), str);
		gnome_href_set_url (GNOME_HREF (dialog->website_href), str);
		gtk_widget_show (dialog->website_box);
	}
	else
	{
		gtk_widget_hide (dialog->website_box);
	}
}


static void
connect_dialog_sel_changed_cb (GtkTreeSelection * sel, ConnectDialog * dialog)
{
	GtkTreeModel *model;
	GtkTreeIter iter;

	if (gtk_tree_selection_get_selected (sel, &model, &iter))
	{
		GtkTreeIter real_iter;

		egg_tree_model_filter_convert_iter_to_child_iter (EGG_TREE_MODEL_FILTER (model), &real_iter,
														  &iter);

		if (dialog->network != NULL)
		{
			g_signal_handler_disconnect (dialog->network, dialog->network_notify_icon_id);
			g_signal_handler_disconnect (dialog->network, dialog->network_notify_uri_id);
			g_signal_handler_disconnect (dialog->network, dialog->network_notify_description_id);
			g_signal_handler_disconnect (dialog->network, dialog->network_notify_website_id);
			g_object_unref (dialog->network);
		}

		dialog->network = GNOMECHAT_RDF_NETWORK (gnomechat_rdf_model_get_data
												 (GNOMECHAT_RDF_MODEL (dialog->model), &real_iter));

		if (dialog->network != NULL)
		{
			network_notify_icon_cb (dialog->network, NULL, dialog);
			network_notify_uri_cb (dialog->network, NULL, dialog);
			network_notify_description_cb (dialog->network, NULL, dialog);
			network_notify_website_cb (dialog->network, NULL, dialog);

			dialog->network_notify_icon_id =
				g_signal_connect (dialog->network, "notify::icon",
								  G_CALLBACK (network_notify_icon_cb), dialog);
			dialog->network_notify_uri_id =
				g_signal_connect (dialog->network, "notify::uri",
								  G_CALLBACK (network_notify_uri_cb), dialog);
			dialog->network_notify_description_id =
				g_signal_connect (dialog->network, "notify::description",
								  G_CALLBACK (network_notify_description_cb), dialog);
			dialog->network_notify_website_id =
				g_signal_connect (dialog->network, "notify::website",
								  G_CALLBACK (network_notify_website_cb), dialog);
		}
		else
		{
			gtk_widget_hide (dialog->info_box);
		}

		gtk_widget_show (dialog->info_box);
	}
	else
	{
		gtk_widget_hide (dialog->info_box);
	}
}


static gboolean
filter_for_networks (GtkTreeModel * model, GtkTreeIter * iter, gpointer user_data)
{
	GObject *data;
	gboolean retval;

	data = gnomechat_rdf_model_get_data (GNOMECHAT_RDF_MODEL (model), iter);
	retval = GNOMECHAT_IS_RDF_NETWORK (data);

	g_object_unref (data);

	return retval;
}


void
network_ui_open_connect_dialog (GtkWindow * parent)
{
	static ConnectDialog *dialog = NULL;
	GtkWidget *paned,
	 *scrwin,
	 *align,
	 *hbox,
	 *label,
	 *image,
	 *button,
	 *tree_view;
	PangoFontDescription *font_desc;
	GtkTreeViewColumn *col;
	GtkCellRenderer *cell;
	GtkTreeModel *filter;

	if (dialog != NULL)
	{
		gtk_window_present (GTK_WINDOW (dialog->win));
		return;
	}

	dialog = g_new0 (ConnectDialog, 1);

	dialog->win = gtk_dialog_new_with_buttons (_("Connect - IRC Chat"), parent,
											   GTK_DIALOG_NO_SEPARATOR,
											   GTK_STOCK_CLOSE, GTK_RESPONSE_CANCEL,
											   GNOMECHAT_STOCK_CONNECT, GTK_RESPONSE_OK, NULL);
	gtk_window_set_default_size (GTK_WINDOW (dialog->win),
								 prefs_get_window_width (GNOMECHAT_WINDOW_CONNECT),
								 prefs_get_window_height (GNOMECHAT_WINDOW_CONNECT));
	util_set_window_icon_from_stock (GTK_WINDOW (dialog->win), GNOMECHAT_STOCK_CONNECT);
	util_set_save_window_size (dialog->win, GNOMECHAT_WINDOW_CONNECT);
	g_signal_connect (dialog->win, "response", G_CALLBACK (connect_dialog_response_cb), &dialog);

	gtk_container_set_border_width (GTK_CONTAINER (dialog->win), 5);

	paned = gtk_hpaned_new ();
	gtk_paned_set_position (GTK_PANED (paned), prefs_get_connect_info_size ());
	gtk_container_set_border_width (GTK_CONTAINER (paned), 5);
	g_signal_connect (paned, "notify::position", G_CALLBACK (connect_paned_notify_position_cb),
					  NULL);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (dialog->win)->vbox), paned);
	gtk_widget_show (paned);

	scrwin = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrwin), GTK_SHADOW_IN);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrwin), GTK_POLICY_NEVER,
									GTK_POLICY_AUTOMATIC);
	gtk_paned_pack1 (GTK_PANED (paned), scrwin, FALSE, FALSE);
	gtk_widget_show (scrwin);

	dialog->model = gnomechat_rdf_model_get_bookmarks ();
	filter = egg_tree_model_filter_new (dialog->model, NULL);
	egg_tree_model_filter_set_visible_func (EGG_TREE_MODEL_FILTER (filter),
											filter_for_networks, NULL, NULL);

	tree_view = gtk_tree_view_new_with_model (filter);
	g_object_unref (filter);
	gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (tree_view), FALSE);
	gtk_container_add (GTK_CONTAINER (scrwin), tree_view);
	gtk_widget_show (tree_view);

	col = gtk_tree_view_column_new ();
	gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view), col);

	cell = gtk_cell_renderer_pixbuf_new ();
	gtk_tree_view_column_pack_start (col, cell, FALSE);
	gtk_tree_view_column_set_cell_data_func (col, cell, gnomechat_rdf_model_render_icon, NULL,
											 NULL);

	cell = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_start (col, cell, TRUE);
	gtk_tree_view_column_set_cell_data_func (col, cell, gnomechat_rdf_model_render_name, NULL,
											 NULL);

	dialog->sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree_view));
	g_signal_connect (dialog->sel, "changed", G_CALLBACK (connect_dialog_sel_changed_cb), dialog);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_paned_pack2 (GTK_PANED (paned), hbox, TRUE, FALSE);
	gtk_widget_show (hbox);

	dialog->info_box = gtk_vbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (hbox), dialog->info_box, TRUE, TRUE, 12);
	gtk_widget_show (dialog->info_box);

	hbox = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (dialog->info_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	dialog->icon_image = gtk_image_new_from_stock (GNOMECHAT_STOCK_NETWORK, GTK_ICON_SIZE_DIALOG);
	gtk_box_pack_start (GTK_BOX (hbox), dialog->icon_image, FALSE, FALSE, 0);
	gtk_widget_show (dialog->icon_image);

	dialog->name_label = gtk_label_new ("Network Name");
	font_desc = pango_font_description_copy (dialog->name_label->style->font_desc);
	pango_font_description_set_weight (font_desc, PANGO_WEIGHT_BOLD);
	pango_font_description_set_size (font_desc,
									 pango_font_description_get_size (font_desc) *
									 PANGO_SCALE_XX_LARGE);
	gtk_widget_modify_font (dialog->name_label, font_desc);
	pango_font_description_free (font_desc);
	gtk_misc_set_alignment (GTK_MISC (dialog->name_label), 0.0, 0.5);
	gtk_box_pack_start (GTK_BOX (hbox), dialog->name_label, TRUE, TRUE, 0);
	gtk_widget_show (dialog->name_label);

	align = gtk_alignment_new (0.5, 0.5, 0.0, 0.0);
	gtk_box_pack_start (GTK_BOX (hbox), align, FALSE, FALSE, 0);
	gtk_widget_show (align);

	hbox = gtk_hbox_new (TRUE, 0);
	gtk_container_add (GTK_CONTAINER (align), hbox);
	gtk_widget_show (hbox);

	button = gtk_button_new ();
	gtk_button_set_relief (GTK_BUTTON (button), GTK_RELIEF_NONE);
	g_signal_connect (button, "clicked", G_CALLBACK (connect_rm_btn_clicked_cb), dialog);
	gtk_container_add (GTK_CONTAINER (hbox), button);
	gtk_widget_show (button);

	image = gtk_image_new_from_stock (GTK_STOCK_REMOVE, GTK_ICON_SIZE_BUTTON);
	gtk_container_add (GTK_CONTAINER (button), image);
	gtk_widget_show (image);

	button = gtk_button_new ();
	gtk_button_set_relief (GTK_BUTTON (button), GTK_RELIEF_NONE);
	g_signal_connect (button, "clicked", G_CALLBACK (connect_props_btn_clicked_cb), dialog);
	gtk_container_add (GTK_CONTAINER (hbox), button);
	gtk_widget_show (button);

	image = gtk_image_new_from_stock (GTK_STOCK_PROPERTIES, GTK_ICON_SIZE_BUTTON);
	gtk_container_add (GTK_CONTAINER (button), image);
	gtk_widget_show (image);

	dialog->description_label = gtk_label_new ("Description goes here...");
	gtk_misc_set_alignment (GTK_MISC (dialog->description_label), 0.0, 0.0);
	gtk_label_set_line_wrap (GTK_LABEL (dialog->description_label), TRUE);
	gtk_box_pack_start (GTK_BOX (dialog->info_box), dialog->description_label, FALSE, FALSE, 0);
	gtk_widget_show (dialog->description_label);

	dialog->website_box = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (dialog->info_box), dialog->website_box, FALSE, FALSE, 0);
	gtk_widget_show (dialog->website_box);

	label = gtk_label_new (_("Website:"));
	gtk_box_pack_start (GTK_BOX (dialog->website_box), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	dialog->website_href = gnome_href_new ("", "");
	gtk_box_pack_start (GTK_BOX (dialog->website_box), dialog->website_href, FALSE, FALSE, 0);
	gtk_widget_show (dialog->website_href);

	gtk_window_present (GTK_WINDOW (dialog->win));
}
