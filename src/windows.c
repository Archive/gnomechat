/*
 *  GnomeChat: src/windows.c
 *
 *  Copyright (c) 2002 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "gnomechat.h"
#include "windows.h"

#include "esco-window.h"
#include "esco-chat-box.h"

#include <gtk/gtknotebook.h>


void win_destroyed (gpointer data,
					GObject * where_obj_was);


static GList *main_windows = NULL;


GtkWidget *
windows_open_new_window (void)
{
	GtkWidget *win;
	GtkWidget *box;

	win = esco_window_new ();
	main_windows = g_list_append (main_windows, win);
	g_object_weak_ref (G_OBJECT (win), win_destroyed, NULL);

	box = esco_window_add_new_chat (ESCO_WINDOW (win));

	gtk_widget_show (win);

	return box;
}


void
windows_set_tab_position (GtkPositionType pos)
{
	GList *windows = main_windows;

	while (windows)
	{
		gtk_notebook_set_tab_pos (GTK_NOTEBOOK
								  (ESCO_WINDOW (windows->data)->notebook), pos);
		windows = windows->next;
	}
}


void
windows_find_and_focus_chat (gpointer box)
{
	GList *windows = main_windows,
	 *kids = NULL;

	while (windows)
	{
		kids = ESCO_WINDOW (windows->data)->children;
		while (kids)
		{
			if (box == kids->data)
			{
				gtk_window_present (GTK_WINDOW (windows->data));
				esco_window_switch_to_chat (ESCO_WINDOW (windows->data),
											GTK_WIDGET (box));
				return;
			}

			kids = kids->next;
		}

		windows = windows->next;
	}
}


GtkWidget *
windows_get_empty_chat (void)
{
	GList *windows;

	for (windows = main_windows; windows != NULL; windows = windows->next)
	{
		GList *kids;

		for (kids = ESCO_WINDOW (windows->data)->children;
			 kids != NULL; kids = kids->next)
		{
			if (ESCO_CHAT_BOX (kids->data)->mode == ESCO_CHAT_MODE_NONE)
				return GTK_WIDGET (kids->data);
		}
	}

	return esco_window_add_new_chat (ESCO_WINDOW (main_windows->data));
}


void
win_destroyed (gpointer data,
			   GObject * where_obj_was)
{
	main_windows = g_list_remove (main_windows, where_obj_was);

	if (!main_windows)
	{
		gnomechat_quit ();
	}
}
