/*
 *  GnomeChat: src/esco-chat-box.c
 *
 *  Copyright (c) 2002, 2003 James M. Cape.
 *  All Rights Reserved.
 *  Multiline parsing Copyright (c) 2003 Adam Olsen
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "gnomechat.h"
#include "esco-chat-box.h"

#include "esco-type-builtins.h"
#include "esco-enums.h"

#include "gtkwrapbox.h"
#include "gtkhwrapbox.h"
#include "esco-chats-menu.h"
#include "esco-disclosure.h"
#include "esco-encoding.h"
#include "esco-userlist-view.h"
#include "esco-viewer.h"
#include "netserv-ui.h"
#include "menus.h"
#include "outgoing.h"
#include "prefs.h"
#include "stock-items.h"
#include "utils.h"

#include <string.h>

#include <gdk/gdkkeysyms.h>

#include <gtk/gtkalignment.h>
#include <gtk/gtkcheckbutton.h>
#include <gtk/gtkentry.h>
#include <gtk/gtkhbox.h>
#include <gtk/gtkhpaned.h>
#include <gtk/gtkimage.h>
#include <gtk/gtklabel.h>
#include <gtk/gtkmenuitem.h>
#include <gtk/gtkmessagedialog.h>
#include <gtk/gtkoptionmenu.h>
#include <gtk/gtkscrolledwindow.h>
#include <gtk/gtkstock.h>
#include <gtk/gtktextview.h>

#include <libgircclient/girc-client.h>


#define DEFAULT_TITLE			N_("(None)")
#define DEFAULT_SUBTITLE		NULL
#define DEFAULT_ICON			NULL
#define DEFAULT_NICK			""
#define DEFAULT_MODE			ESCO_CHAT_MODE_NONE


/* Class enums */
enum
{
	PROP_0,
	PROP_NICK,
	PROP_MODE,
	PROP_TITLE,
	PROP_ENCODING,
	PROP_TOPIC
};

enum
{
	PESTER,
	FOCUSED,
	TITLE_CHANGED,
	MODE_CHANGED,
	USER_SELECTION_CHANGED,

	SIGNAL_LAST
};


/* Global Variables */
static gint signals[SIGNAL_LAST] = { 0 };
static gpointer parent_class = NULL;


/* Utility Functions */
static void
set_status_icon (EscoChatBox * box,
				 const gchar * icon)
{
	g_return_if_fail (box != NULL);
	g_return_if_fail (ESCO_IS_CHAT_BOX (box));

	if (box->status_icon)
		g_free (box->status_icon);

	box->status_icon = g_strdup (icon);

	if (box->topic_image != NULL)
	{
		if (icon == NULL)
		{
			gtk_widget_hide (box->topic_image);
		}
		else
		{
			gtk_widget_show (box->topic_image);
			gtk_image_set_from_stock (GTK_IMAGE (box->topic_image), icon, GTK_ICON_SIZE_MENU);
		}
	}
}


static void
irc_destroyed (EscoChatBox * box,
			   GObject * old_irc)
{
	box->irc = NULL;
	box->target = NULL;

	esco_chat_box_set_mode (box, ESCO_CHAT_MODE_NONE);
	esco_chat_box_set_nick (box, NULL);
	esco_chat_box_set_title (box, NULL);
}


static void
show_hide_from_mode (EscoChatBox * box,
					 EscoChatModeType mode)
{
	const gchar *icon;

	switch (mode)
	{
	case ESCO_CHAT_MODE_NONE:
		icon = GTK_STOCK_NEW;

		if (box->topic_hbox != NULL)
			gtk_widget_hide (box->topic_hbox);

		if (box->viewer != NULL)
			gtk_widget_set_sensitive (box->viewer, FALSE);

		if (box->userlist_box != NULL)
			gtk_widget_hide (box->userlist_box);

		set_status_icon (box, NULL);
		break;

	case ESCO_CHAT_MODE_SERVER:
		icon = GNOMECHAT_STOCK_SERVER;

		if (box->topic_hbox != NULL)
			gtk_widget_show (box->topic_hbox);

		if (box->topic_image != NULL)
			gtk_widget_hide (box->topic_image);

		if (box->topic_props_button != NULL)
		{
			gtk_image_set_from_stock (GTK_IMAGE
									  (GTK_BIN (box->topic_props_button)->
									   child),
									  GNOMECHAT_STOCK_SERVER_PROPERTIES, GTK_ICON_SIZE_MENU);
			gtk_widget_show (box->topic_props_button);
		}

		if (box->topic_send_button != NULL)
			gtk_widget_hide (box->topic_send_button);

		if (box->viewer != NULL)
			gtk_widget_set_sensitive (box->viewer, TRUE);

		if (box->userlist_box != NULL)
			gtk_widget_hide (box->userlist_box);

		if (box->topic_entry != NULL)
			gtk_widget_set_sensitive (box->topic_entry, FALSE);

		set_status_icon (box, GNOMECHAT_STOCK_SERVER);
		break;

	case ESCO_CHAT_MODE_CHANNEL:
		icon = GNOMECHAT_STOCK_CHANNEL;

		if (box->topic_image != NULL)
			gtk_widget_show (box->topic_image);

		if (box->topic_props_button != NULL)
		{
			gtk_image_set_from_stock (GTK_IMAGE
									  (GTK_BIN (box->topic_props_button)->
									   child),
									  GNOMECHAT_STOCK_CHANNEL_PROPERTIES, GTK_ICON_SIZE_MENU);
			gtk_widget_show (box->topic_props_button);
		}

		if (box->topic_send_button != NULL)
			gtk_widget_hide (box->topic_send_button);

		if (box->topic_hbox != NULL)
			gtk_widget_show (box->topic_hbox);

		if (box->viewer != NULL)
			gtk_widget_set_sensitive (box->viewer, TRUE);

		if (box->userlist_box != NULL)
			gtk_widget_show (box->userlist_box);

		if (box->topic_entry != NULL)
			gtk_widget_set_sensitive (box->topic_entry, TRUE);

		set_status_icon (box, GNOMECHAT_STOCK_STATUS_NORMAL);
		break;

	case ESCO_CHAT_MODE_USER:
		icon = GNOMECHAT_STOCK_USER;

		if (box->topic_hbox != NULL)
			gtk_widget_show (box->topic_hbox);

		if (box->topic_image != NULL)
			gtk_widget_show (box->topic_image);

		if (box->topic_props_button != NULL)
		{
			gtk_image_set_from_stock (GTK_IMAGE
									  (GTK_BIN (box->topic_props_button)->
									   child), GNOMECHAT_STOCK_USER_PROPERTIES, GTK_ICON_SIZE_MENU);
			gtk_widget_show (box->topic_props_button);
		}

		if (box->topic_send_button != NULL)
			gtk_widget_show (box->topic_send_button);

		if (box->viewer != NULL)
			gtk_widget_set_sensitive (box->viewer, TRUE);

		if (box->userlist_box != NULL)
			gtk_widget_hide (box->userlist_box);

		if (box->topic_entry != NULL)
			gtk_widget_set_sensitive (box->topic_entry, FALSE);

		set_status_icon (box, GNOMECHAT_STOCK_USER);
		break;

	default:
		icon = NULL;

		if (box->topic_hbox != NULL)
			gtk_widget_hide (box->topic_hbox);

		if (box->viewer != NULL)
			gtk_widget_set_sensitive (box->viewer, FALSE);

		if (box->userlist_box != NULL)
			gtk_widget_hide (box->userlist_box);

		if (box->topic_entry != NULL)
			gtk_widget_set_sensitive (box->topic_entry, FALSE);

		set_status_icon (box, NULL);
		break;
	}

	if (box->tab_image != NULL)
		gtk_image_set_from_stock (GTK_IMAGE (box->tab_image), icon, GTK_ICON_SIZE_MENU);
}


static void
clear_notification (EscoChatBox * box)
{
	GtkStateType i;

	box->pester = ESCO_PESTER_NONE;

	for (i = GTK_STATE_NORMAL; i <= GTK_STATE_INSENSITIVE; i++)
	{
		gtk_widget_modify_fg (box->tab_label, i, NULL);
	}

	esco_viewer_clear_pester (ESCO_VIEWER (box->viewer));
}


/* Sub-Widget Signal Callbacks */
static void
paned_button_release_event_cb (GtkPaned * paned,
							   GdkEventButton *event,
							   EscoChatBox * box)
{
	prefs_set_main_window_user_list_size (gtk_paned_get_position (paned));
}


static void
topic_props_button_clicked_cb (GtkButton * button,
							   EscoChatBox * box)
{
	switch (box->mode)
	{
	case ESCO_CHAT_MODE_USER:
		netserv_ui_open_whois_dialog (box->irc,
									  GTK_WINDOW (gtk_widget_get_toplevel
												  (GTK_WIDGET (box))), NULL,
									  GIRC_QUERY (box->target)->user->nick);
		break;

	case ESCO_CHAT_MODE_CHANNEL:
		netserv_ui_open_channel_props_dialog (box->irc,
											  GTK_WINDOW
											  (gtk_widget_get_toplevel
											   (GTK_WIDGET (box))),
											  GIRC_CHANNEL (box->target),
											  GIRC_CHANNEL (box->target)->name);
		break;

	case ESCO_CHAT_MODE_SERVER:
		break;

	default:
		break;
	}
}


static void
viewer_pester_cb (EscoViewer * view,
				  EscoPesterType pester,
				  EscoChatBox * box)
{
	gchar *prefs_color = NULL;

	if (GTK_WIDGET_MAPPED (box))
	{
		clear_notification (box);
		return;
	}

	if (pester <= ESCO_PESTER_IRC)
		return;

	prefs_color = prefs_get_notification_color (pester);

	if (prefs_color != NULL)
	{
		GdkColor color;
		GtkStateType i;

		gdk_color_parse (prefs_color, &color);

		for (i = GTK_STATE_NORMAL; i <= GTK_STATE_INSENSITIVE; i++)
		{
			gtk_widget_modify_fg (box->tab_label, i, &color);
		}

		g_free (prefs_color);
	}

	g_signal_emit (box, signals[PESTER], 0, pester);

	box->pester = pester;
}


static void
userlist_sel_changed_cb (EscoUserlistView * view,
						 EscoChatBox * box)
{
	if (view->any_selected)
	{
		gtk_widget_show (box->userlist_bbox);
	}
	else
	{
		gtk_widget_hide (box->userlist_bbox);
	}

	g_signal_emit (box, signals[USER_SELECTION_CHANGED], 0, view->any_selected);
}


static void
userlist_query_button_clicked_cb (GtkButton * button,
								  EscoChatBox * box)
{
	esco_userlist_view_query_selected (ESCO_USERLIST_VIEW (box->userlist_view));
}


static void
userlist_send_button_clicked_cb (GtkButton * button,
								 EscoChatBox * box)
{
	esco_userlist_view_send_file_to_selected (ESCO_USERLIST_VIEW (box->userlist_view));
}


static void
userlist_props_button_clicked_cb (GtkButton * button,
								  EscoChatBox * box)
{
	esco_userlist_view_info_on_selected (ESCO_USERLIST_VIEW (box->userlist_view));
}


static void
expander_button_toggled_cb (GtkToggleButton * btn,
							EscoChatBox * box)
{
	GtkWidget *toplevel;
	gboolean active = gtk_toggle_button_get_active (btn);

	if (prefs_get_use_multiline_entry () != active)
	{
		prefs_set_use_multiline_entry (active);
	}

	if (active)
	{
		gtk_alignment_set (GTK_ALIGNMENT (box->nick_align), 1.0, 0.05, 1.0, 0.0);

		gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW
										(box->main_text_scrwin),
										GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
		gtk_text_view_set_pixels_above_lines (GTK_TEXT_VIEW (box->main_text), 2);
	}
	else
	{
		gtk_alignment_set (GTK_ALIGNMENT (box->nick_align), 1.0, 0.5, 1.0, 0.0);

		gtk_text_view_set_pixels_above_lines (GTK_TEXT_VIEW (box->main_text), 3);
		gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW
										(box->main_text_scrwin),
										GTK_POLICY_NEVER, GTK_POLICY_NEVER);
	}

	toplevel = gtk_widget_get_toplevel (box->main_text);
	if (GTK_WIDGET_TOPLEVEL (toplevel))
	{
		gtk_window_set_focus (GTK_WINDOW (toplevel), box->main_text);
		gtk_text_view_place_cursor_onscreen (GTK_TEXT_VIEW (box->main_text));
	}
}


static void
main_text_changed_cb (GtkTextBuffer * buffer,
					  EscoChatBox * box)
{
	gchar *text;
	GtkTextIter start,
	  end;

	gtk_text_buffer_get_bounds (buffer, &start, &end);
	text = gtk_text_buffer_get_text (buffer, &start, &end, FALSE);

	if (text != NULL && text[0] != '\0')
	{
		gtk_widget_set_sensitive (box->send_button, TRUE);

		if (strrchr (text, '\n'))
		{
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (box->expander_button), TRUE);
			gtk_widget_set_sensitive (box->expander_button, FALSE);
		}
		else
		{
			gtk_widget_set_sensitive (box->expander_button, TRUE);
		}
	}
	else
	{
		gtk_widget_set_sensitive (box->send_button, FALSE);
		gtk_widget_set_sensitive (box->expander_button, TRUE);
	}

	g_free (text);
}


typedef struct
{
	GtkWidget *menu;
	EscoChatBox *box;
	gchar *partial;
	gsize partial_len;
	gint count;
	gint best_depth;
	gint best_index;
}
TabCompleteMatch;

static gboolean
tabcomplete_nick_match (GtkTreeModel * model,
						GtkTreePath * path,
						GtkTreeIter * iter,
						TabCompleteMatch * match)
{
	GIrcChannelUser *user;
	gchar *nick,
	 *orig_nick,
	 *substr;

	// Get the nick
//  gtk_tree_model_get (model, iter,
//                      USER_LIST_USER_ITEM, &user, -1);
	// XXX: USER_LIST_USER_ITEM is only in esco-userlist-view.c
	gtk_tree_model_get (model, iter, 0, &user, -1);
	g_assert (user != NULL);
	orig_nick = girc_convert_to_utf8 (user->user->nick,
									  ((EscoUserlistView *) match->box->userlist_view)->encoding);
	nick = g_utf8_casefold (orig_nick, -1);

	// Add it if it matches
	if ((substr = strstr (nick, match->partial)))
	{
		// Insert the item
		GtkWidget *menu_item = gtk_menu_item_new_with_label (orig_nick);

		g_object_ref (menu_item);
		gtk_menu_shell_append (GTK_MENU_SHELL (match->menu), menu_item);
		gtk_widget_show (menu_item);

		// Select it if it's the closest hit we've had so far
		if (substr - nick < match->best_depth)
		{
			match->best_depth = substr - nick;
			match->best_index = match->count;
		}

		match->count++;
	}

	g_free (orig_nick);
	g_free (nick);

	return FALSE;
}

typedef struct
{
	GtkWidget *option_menu;
	GtkTextView *view;
	EscoChatBox *box;
	GtkWidget *menu;
	GtkTextChildAnchor *anchor;
}
TabComplete;

static void
tabcomplete_menu_deactivate_cb (GtkMenuShell * menushell,
								TabComplete * tc)
{
	gint selection = gtk_option_menu_get_history (GTK_OPTION_MENU (tc->option_menu));
	GtkTextBuffer *buffer = gtk_text_view_get_buffer (tc->view);
	GtkTextIter start,
	  end;
	GList *menuitems;
	GtkMenuItem *menu_item;
	GtkWidget *menu_item_label;
	gchar *nick;

	// Retrieve the chosen nick
	menuitems = gtk_container_get_children (GTK_CONTAINER (tc->menu));
	menu_item = g_list_nth_data (menuitems, selection);
	menu_item_label = gtk_bin_get_child (GTK_BIN (menu_item));
	nick = g_strdup (gtk_label_get_text (GTK_LABEL (menu_item_label)));

	// Delete the embedded option menu
	gtk_widget_hide (tc->option_menu);
	gtk_text_buffer_get_iter_at_child_anchor (buffer, &start, tc->anchor);
	end = start;
	gtk_text_iter_forward_chars (&end, 1);
	gtk_text_buffer_delete (buffer, &start, &end);

	// Insert the chosen nick
	if (gtk_text_iter_starts_line (&start))
	{
		gtk_text_buffer_insert (buffer, &start, nick, -1);
		gtk_text_buffer_insert_at_cursor (buffer, ": ", -1);
	}
	else
	{
		gtk_text_buffer_insert (buffer, &start, nick, -1);
	}

	// Clean up
	g_object_unref (tc->anchor);
	g_free (tc);
	g_free (nick);
}

/* Simulating a mouse click is an evil hack, but it seems to be the
   only thing that works.  And it has to be done in an idle timer too,
   otherwise the option_menu won't have initialized properly */
static gboolean
tabcomplete_menu_activate_timer (GtkWidget * option_menu)
{
	GdkEvent *event = gdk_event_new (GDK_BUTTON_PRESS);

	event->type = GDK_BUTTON_PRESS;
	((GdkEventButton *) event)->button = 1;

	gtk_widget_event (option_menu, event);

	gdk_event_free (event);
	return FALSE;
}


static gboolean
main_text_keypress_cb (GtkTextView * view,
					   GdkEventKey * event,
					   EscoChatBox * box)
{
	/* Accept any of the variants of tab keys, but not if control
	   or alt are held down */
	if (!(event->state & (GDK_CONTROL_MASK | GDK_MOD1_MASK)) &&
		(event->keyval == GDK_Tab ||
		 event->keyval == GDK_KP_Tab ||
		 event->keyval == GDK_ISO_Left_Tab || event->keyval == GDK_3270_BackTab))
	{
		GtkTextBuffer *buffer = gtk_text_view_get_buffer (view);
		GtkTextMark *cursor = gtk_text_buffer_get_insert (buffer);
		GtkTextIter start,
		  end;
		GtkTextChildAnchor *anchor;
		GtkWidget *menu;
		GtkWidget *option_menu;
		TabComplete *tc;
		TabCompleteMatch match;
		gchar *partial;

		gtk_text_buffer_get_iter_at_mark (buffer, &end, cursor);
		start = end;

		// Find the space-delimited nickname
		if (gtk_text_iter_starts_line (&start) ||
			g_unichar_isgraph (gtk_text_iter_get_char (&start)))
		{
			return TRUE;
		}
		gtk_text_iter_backward_char (&start);
		while (TRUE)
		{
			if (!g_unichar_isgraph (gtk_text_iter_get_char (&start)))
			{
				gtk_text_iter_forward_char (&start);
				break;
			}
			if (!gtk_text_iter_backward_char (&start))
			{
				break;
			}
		}
		if (gtk_text_iter_equal (&start, &end))
		{
			return TRUE;
		}
		partial = gtk_text_buffer_get_text (buffer, &start, &end, FALSE);

		// Populate the menu with nicks
		menu = gtk_menu_new ();

		match.menu = menu;
		match.box = box;
		match.partial = g_utf8_casefold (partial, -1);
		g_free (partial);
		match.partial_len = strlen (match.partial);
		match.count = 0;
		match.best_depth = G_MAXINT;
		match.best_index = 0;
		gtk_tree_model_foreach (gtk_tree_view_get_model
								(GTK_TREE_VIEW (box->userlist_view)),
								(GtkTreeModelForeachFunc) tabcomplete_nick_match, &match);
		g_free (match.partial);

		if (gtk_container_get_children (GTK_CONTAINER (menu)) == NULL)
		{
			return TRUE;
		}

		// Delete the partial text and insert an anchor
		gtk_text_buffer_delete (buffer, &start, &end);
		gtk_text_buffer_get_iter_at_mark (buffer, &end, cursor);
		anchor = gtk_text_buffer_create_child_anchor (buffer, &end);
		g_object_ref (anchor);
		gtk_text_buffer_get_iter_at_mark (buffer, &end, cursor);
		start = end;
		gtk_text_iter_backward_char (&start);
		gtk_text_buffer_apply_tag_by_name (buffer, "menu", &start, &end);

		// Create the option menu
		option_menu = gtk_option_menu_new ();
		gtk_option_menu_set_menu (GTK_OPTION_MENU (option_menu), menu);
		gtk_option_menu_set_history (GTK_OPTION_MENU (option_menu), match.best_index);

		tc = g_new0 (TabComplete, 1);
		tc->option_menu = option_menu;
		tc->view = view;
		tc->box = box;
		tc->menu = menu;
		tc->anchor = anchor;
		g_signal_connect (menu, "deactivate", G_CALLBACK (tabcomplete_menu_deactivate_cb), tc);

		// Insert the option menu
		gtk_text_view_add_child_at_anchor (view, option_menu, anchor);
		gtk_widget_show (option_menu);
		g_idle_add ((GSourceFunc) tabcomplete_menu_activate_timer, option_menu);

		return TRUE;
	}
	else if (!gtk_toggle_button_get_active
			 (GTK_TOGGLE_BUTTON (box->expander_button))
			 && !(event->state & GDK_CONTROL_MASK) && (event->keyval == GDK_Return
													   || event->keyval == GDK_KP_Enter))
	{
		gtk_button_clicked (GTK_BUTTON (box->send_button));
		return TRUE;
	}

	return FALSE;
}


static void
send_button_clicked_cb (GtkButton * btn,
						EscoChatBox * box)
{
	gchar *text = NULL;
	GtkTextBuffer *buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (box->main_text));
	GtkTextIter start,
	  end;

	gtk_text_buffer_get_bounds (buffer, &start, &end);
	text = gtk_text_buffer_get_text (buffer, &start, &end, FALSE);

	if (text != NULL && text[0] != '\0' && box->irc != NULL)
	{
		gchar **lines = g_strsplit (text, "\n", -1);
		guint i;

		for (i = 0; lines[i] != NULL; i++)
		{
			if (lines[i][0] != '\0')
				outgoing_send (box, lines[i]);
		}

		g_strfreev (lines);

		gtk_text_buffer_set_text (buffer, "", 0);
	}

	g_free (text);
}


/* EscoChatBox Callbacks */
static void
esco_chat_box_title_changed (EscoChatBox * box)
{
	const gchar *str;

	if (box->title != NULL)
		str = (const gchar *) box->title;
	else
		str = _(DEFAULT_TITLE);

	if (box->topic_label != NULL)
		gtk_label_set_text (GTK_LABEL (box->topic_label), str);
	if (box->tab_label != NULL)
		gtk_label_set_text (GTK_LABEL (box->tab_label), str);
}


static void
esco_chat_box_focused (EscoChatBox * box)
{
	GtkWidget *toplevel;

	clear_notification (box);

	toplevel = gtk_widget_get_toplevel (box->main_text);
	if (GTK_WIDGET_TOPLEVEL (toplevel))
	{
		gtk_window_set_focus (GTK_WINDOW (toplevel), box->main_text);
	}

	gtk_text_view_place_cursor_onscreen (GTK_TEXT_VIEW (box->main_text));
}


static void
esco_chat_box_mode_changed (EscoChatBox * box)
{
	show_hide_from_mode (box, box->mode);
}


/* GtkObject Callbacks */
static void
esco_chat_box_destroy (GtkObject * object)
{
	EscoChatBox *box = ESCO_CHAT_BOX (object);

	box->dying = TRUE;

	if (box->irc != NULL)
	{
		g_object_weak_unref (G_OBJECT (box->irc), (GWeakNotify) irc_destroyed, box);

		if (box->target != NULL)
		{
			girc_client_close_target (box->irc, box->target);
			box->target = NULL;
		}
	}

	if (GTK_OBJECT_CLASS (parent_class)->destroy != NULL)
		(*GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}


/* GObject Callbacks */
static void
esco_chat_box_get_property (GObject * object,
							guint param_id,
							GValue * value,
							GParamSpec * pspec)
{
	EscoChatBox *box = ESCO_CHAT_BOX (object);

	switch (param_id)
	{
	case PROP_MODE:
		g_value_set_enum (value, box->mode);
		break;
	case PROP_NICK:
		g_value_set_string (value, box->nick);
		break;
	case PROP_TITLE:
		g_value_set_string (value, box->title);
		break;
	case PROP_ENCODING:
		g_value_set_string (value, box->encoding);
		break;
	case PROP_TOPIC:
		g_value_set_string (value, box->topic);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}


static void
esco_chat_box_set_property (GObject * object,
							guint param_id,
							const GValue * value,
							GParamSpec * pspec)
{
	EscoChatBox *box = ESCO_CHAT_BOX (object);

	switch (param_id)
	{
	case PROP_MODE:
		esco_chat_box_set_mode (box, g_value_get_enum (value));
		break;
	case PROP_NICK:
		esco_chat_box_set_nick (box, g_value_get_string (value));
		break;
	case PROP_TITLE:
		esco_chat_box_set_title (box, g_value_get_string (value));
		break;
	case PROP_ENCODING:
		esco_chat_box_set_encoding (box, g_value_get_string (value));
		break;
	case PROP_TOPIC:
		esco_chat_box_set_topic (box, g_value_get_string (value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}


static GObject *
esco_chat_box_constructor (GType type,
						   guint n_construct_params,
						   GObjectConstructParam * construct_params)
{
	GObject *object;
	EscoChatBox *box;

	/* Sub-Widgets */
	GtkWidget *scrwin,
	 *hbox,
	 *image,
	 *align,
	 *button;
	const gchar *mode_icon;

	object = (*G_OBJECT_CLASS (parent_class)->constructor)
		(type, n_construct_params, construct_params);

	box = ESCO_CHAT_BOX (object);

	/* Initial spacing setup */
	gtk_box_set_spacing (GTK_BOX (box), 3);
	gtk_box_set_homogeneous (GTK_BOX (box), FALSE);
	gtk_container_set_border_width (GTK_CONTAINER (box), 2);

	/* Actual Construction */
	esco_chats_menus_add_item (box);

	/* Topic bar */
	box->topic_hbox = gtk_hbox_new (FALSE, 3);
	gtk_box_pack_start (GTK_BOX (box), box->topic_hbox, FALSE, FALSE, 0);
	gtk_widget_show (box->topic_hbox);

	box->topic_image = gtk_image_new_from_stock (box->status_icon, GTK_ICON_SIZE_MENU);
	gtk_box_pack_start (GTK_BOX (box->topic_hbox), box->topic_image, FALSE, FALSE, 0);

	box->topic_label = gtk_label_new (box->title);
	gtk_box_pack_start (GTK_BOX (box->topic_hbox), box->topic_label, FALSE, FALSE, 0);
	gtk_widget_show (box->topic_label);

	box->topic_entry = gtk_entry_new ();
	gtk_widget_set_sensitive (box->topic_entry, FALSE);
	gtk_container_add (GTK_CONTAINER (box->topic_hbox), box->topic_entry);
	gtk_widget_show (box->topic_entry);

	box->topic_send_button = gtk_button_new ();
	gtk_box_pack_start (GTK_BOX (box->topic_hbox), box->topic_send_button, FALSE, FALSE, 0);

	image = gtk_image_new_from_stock (GTK_STOCK_SAVE, GTK_ICON_SIZE_MENU);
	gtk_container_add (GTK_CONTAINER (box->topic_send_button), image);
	gtk_widget_show (image);

	box->topic_props_button = gtk_button_new ();
	gtk_box_pack_start (GTK_BOX (box->topic_hbox), box->topic_props_button, FALSE, FALSE, 0);
	g_signal_connect (box->topic_props_button, "clicked",
					  G_CALLBACK (topic_props_button_clicked_cb), box);

	image = gtk_image_new_from_stock (GNOMECHAT_STOCK_CHANNEL_PROPERTIES, GTK_ICON_SIZE_MENU);
	gtk_container_add (GTK_CONTAINER (box->topic_props_button), image);
	gtk_widget_show (image);

	/* Viewer */
	box->paned = gtk_hpaned_new ();
	gtk_paned_set_position (GTK_PANED (box->paned), prefs_get_main_window_user_list_size ());
	g_signal_connect (box->paned, "button-release-event", G_CALLBACK (paned_button_release_event_cb), box);
	gtk_container_add (GTK_CONTAINER (box), box->paned);
	gtk_widget_show (box->paned);

	scrwin = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrwin), GTK_SHADOW_IN);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrwin),
									GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_paned_pack1 (GTK_PANED (box->paned), scrwin, TRUE, TRUE);
	gtk_widget_show (scrwin);

	box->viewer = esco_viewer_new (box->mode, box->nick);
	esco_viewer_set_encoding (ESCO_VIEWER (box->viewer), box->encoding);
	g_signal_connect (box->viewer, "pester", G_CALLBACK (viewer_pester_cb), box);
	gtk_container_add (GTK_CONTAINER (scrwin), box->viewer);
	gtk_widget_show (box->viewer);

	/* UserList */
	box->userlist_box = gtk_vbox_new (FALSE, 0);
	gtk_paned_pack2 (GTK_PANED (box->paned), box->userlist_box, FALSE, FALSE);

	scrwin = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrwin), GTK_SHADOW_IN);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrwin),
									GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_container_add (GTK_CONTAINER (box->userlist_box), scrwin);
	gtk_widget_show (scrwin);

	box->userlist_view = esco_userlist_view_new (box->irc);
	gtk_container_add (GTK_CONTAINER (scrwin), box->userlist_view);
	g_signal_connect (box->userlist_view, "selection-changed",
					  G_CALLBACK (userlist_sel_changed_cb), box);
	gtk_widget_show (box->userlist_view);

	/* User Buttons */
	box->userlist_bbox = gtk_hwrap_box_new (FALSE);
	gtk_wrap_box_set_line_justify (GTK_WRAP_BOX (box->userlist_bbox), GTK_JUSTIFY_TOP);
	gtk_wrap_box_set_justify (GTK_WRAP_BOX (box->userlist_bbox), GTK_JUSTIFY_LEFT);
	gtk_wrap_box_set_aspect_ratio (GTK_WRAP_BOX (box->userlist_bbox), 3.0);
	gtk_box_pack_start (GTK_BOX (box->userlist_box), box->userlist_bbox, FALSE, FALSE, 0);

	/* Query */
	button = gtk_button_new ();
	g_signal_connect (button, "clicked", G_CALLBACK (userlist_query_button_clicked_cb), box);
	gtk_wrap_box_pack (GTK_WRAP_BOX (box->userlist_bbox), button, TRUE, TRUE, FALSE, TRUE);
	util_set_tooltip (button, _("Chat Privately"),
					  _("Activating this button opens a private chat to the "
						"selected user."), TRUE);
	gtk_widget_show (button);

	image = gtk_image_new_from_stock (GNOMECHAT_STOCK_QUERY, GTK_ICON_SIZE_MENU);
	gtk_container_add (GTK_CONTAINER (button), image);
	gtk_widget_show (image);

	/* Send File */
	button = gtk_button_new ();
	g_signal_connect (button, "clicked", G_CALLBACK (userlist_send_button_clicked_cb), box);
	gtk_wrap_box_pack (GTK_WRAP_BOX (box->userlist_bbox), button, TRUE, TRUE, FALSE, TRUE);
	util_set_tooltip (button, _("Send File"),
					  _("Activating this button will send a file to the " "selected user."), TRUE);
	gtk_widget_show (button);

	image = gtk_image_new_from_stock (GTK_STOCK_SAVE, GTK_ICON_SIZE_MENU);
	gtk_container_add (GTK_CONTAINER (button), image);
	gtk_widget_show (image);

	/* Info */
	button = gtk_button_new ();
	g_signal_connect (button, "clicked", G_CALLBACK (userlist_props_button_clicked_cb), box);
	gtk_wrap_box_pack (GTK_WRAP_BOX (box->userlist_bbox), button, TRUE, TRUE, FALSE, TRUE);
	util_set_tooltip (button, _("User Properties"),
					  _("Activating this button will open the selected user's "
						"properties from the server."), TRUE);
	gtk_widget_show (button);

	image = gtk_image_new_from_stock (GNOMECHAT_STOCK_USER_PROPERTIES, GTK_ICON_SIZE_MENU);
	gtk_container_add (GTK_CONTAINER (button), image);
	gtk_widget_show (image);

	/* Tab Label -- the EscoWindow will handle placing this */
	box->tab_hbox = gtk_hbox_new (FALSE, 3);
	gtk_widget_show (box->tab_hbox);

	mode_icon = esco_chat_box_get_mode_icon (box);

	box->tab_image = gtk_image_new_from_stock (mode_icon, GTK_ICON_SIZE_MENU);
	gtk_box_pack_start (GTK_BOX (box->tab_hbox), box->tab_image, FALSE, FALSE, 0);
	gtk_widget_show (box->tab_image);

	box->tab_label = gtk_label_new (box->title);
	gtk_box_pack_start (GTK_BOX (box->tab_hbox), box->tab_label, FALSE, FALSE, 0);
	gtk_widget_show (box->tab_label);

	/* Entry Row. */
	hbox = gtk_hbox_new (FALSE, 3);
	gtk_box_pack_start (GTK_BOX (box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	box->nick_align = gtk_alignment_new (1.0, 0.5, 1.0, 0.5);
	gtk_box_pack_start (GTK_BOX (hbox), box->nick_align, FALSE, FALSE, 0);
	gtk_widget_show (box->nick_align);

	box->expander_button = esco_disclosure_new ();
	gtk_container_add (GTK_CONTAINER (box->nick_align), box->expander_button);
	util_set_tooltip (box->expander_button,
					  _("Your nickname - Click to toggle between a single-line "
						"and multi-line entry box"),
					  _("Activating this item will expand the entry box to "
						"use multiple lines."), FALSE);
	g_signal_connect (box->expander_button, "toggled",
					  G_CALLBACK (expander_button_toggled_cb), box);
	gtk_widget_show (box->expander_button);

	box->nick_label = gtk_label_new (box->nick);
	gtk_container_add (GTK_CONTAINER (box->expander_button), box->nick_label);
	gtk_widget_show (box->nick_label);

	box->main_text_align = gtk_alignment_new (0.5, 0.5, 1.0, 0.0);
	gtk_container_add (GTK_CONTAINER (hbox), box->main_text_align);
	gtk_widget_show (box->main_text_align);

	box->main_text_scrwin = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (box->main_text_scrwin),
									GTK_POLICY_NEVER, GTK_POLICY_NEVER);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW
										 (box->main_text_scrwin), GTK_SHADOW_IN);
	gtk_container_add (GTK_CONTAINER (box->main_text_align), box->main_text_scrwin);

	box->main_text = gtk_text_view_new ();
	gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW (box->main_text), GTK_WRAP_WORD);
	gtk_text_view_set_pixels_above_lines (GTK_TEXT_VIEW (box->main_text), 2);
	gtk_text_view_set_pixels_below_lines (GTK_TEXT_VIEW (box->main_text), 2);
	gtk_text_view_set_left_margin (GTK_TEXT_VIEW (box->main_text), 2);
	gtk_text_view_set_right_margin (GTK_TEXT_VIEW (box->main_text), 2);
	gtk_text_buffer_create_tag (gtk_text_view_get_buffer
								(GTK_TEXT_VIEW (box->main_text)), "menu", "rise", -10000, NULL);
	gtk_container_add (GTK_CONTAINER (box->main_text_scrwin), box->main_text);
	g_signal_connect (gtk_text_view_get_buffer (GTK_TEXT_VIEW (box->main_text)),
					  "changed", G_CALLBACK (main_text_changed_cb), box);
	g_signal_connect (box->main_text, "key-press-event", G_CALLBACK (main_text_keypress_cb), box);
	gtk_widget_show (box->main_text);
	gtk_widget_show (box->main_text_scrwin);

	align = gtk_alignment_new (0.0, 1.0, 0.0, 0.0);
	gtk_box_pack_start (GTK_BOX (hbox), align, FALSE, FALSE, 0);
	gtk_widget_show (align);

	box->send_button = gtk_button_new_with_label (_("_Send"));
	g_signal_connect (box->send_button, "clicked", G_CALLBACK (send_button_clicked_cb), box);
	util_set_tooltip (box->send_button, _("Send Message"),
					  _("Activating this button sends the message in the "
						"entry to the Channel, Server, or User."), FALSE);
	gtk_button_set_use_underline (GTK_BUTTON (box->send_button), TRUE);
	gtk_container_add (GTK_CONTAINER (align), box->send_button);
	gtk_widget_set_sensitive (box->send_button, FALSE);
	gtk_widget_show (box->send_button);

	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (box->expander_button),
								  prefs_get_use_multiline_entry ());

	show_hide_from_mode (box, box->mode);

	return object;
}


static void
esco_chat_box_finalize (GObject * object)
{
	EscoChatBox *box = ESCO_CHAT_BOX (object);

	g_free (box->title);
	g_free (box->subtitle);
	g_free (box->status_icon);
	g_free (box->encoding);
	g_free (box->topic);

	if (G_OBJECT_CLASS (parent_class)->finalize)
		G_OBJECT_CLASS (parent_class)->finalize (object);
}


/* GType Functions */
static void
esco_chat_box_class_init (EscoChatBoxClass * class)
{
	GObjectClass *object_class = G_OBJECT_CLASS (class);
	GtkObjectClass *gtk_object_class = GTK_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	object_class->get_property = esco_chat_box_get_property;
	object_class->set_property = esco_chat_box_set_property;
	object_class->constructor = esco_chat_box_constructor;
	object_class->finalize = esco_chat_box_finalize;

	gtk_object_class->destroy = esco_chat_box_destroy;

	class->focused = esco_chat_box_focused;
	class->title_changed = esco_chat_box_title_changed;
	class->mode_changed = esco_chat_box_mode_changed;

	g_object_class_install_property (object_class,
									 PROP_MODE,
									 g_param_spec_enum ("mode",
														_("ChatBox Mode"),
														_("The view mode of "
														  "the chatbox"),
														ESCO_TYPE_CHAT_MODE_TYPE,
														DEFAULT_MODE,
														G_PARAM_READWRITE | G_PARAM_CONSTRUCT));
	g_object_class_install_property (object_class,
									 PROP_NICK,
									 g_param_spec_string ("nick",
														  _("ChatBox Nickname"),
														  _("The nickname of "
															"the chatbox user."),
														  NULL,
														  G_PARAM_READWRITE | G_PARAM_CONSTRUCT));
	g_object_class_install_property (object_class,
									 PROP_TITLE,
									 g_param_spec_string ("title",
														  _("ChatBox Title"),
														  _
														  ("The title to use."),
														  NULL,
														  G_PARAM_READWRITE | G_PARAM_CONSTRUCT));
	g_object_class_install_property (object_class, PROP_ENCODING,
									 g_param_spec_string ("encoding",
														  _("ChatBox Encoding"),
														  _
														  ("The character encoding "
														   "to use."), "UTF-8",
														  G_PARAM_READWRITE | G_PARAM_CONSTRUCT));
	g_object_class_install_property (object_class, PROP_TOPIC,
									 g_param_spec_string ("topic",
														  _("ChatBox Topic"),
														  _("The topic entry's "
															"contents."), NULL,
														  G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

	signals[PESTER] =
		g_signal_new ("pester",
					  G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_LAST,
					  G_STRUCT_OFFSET (EscoChatBoxClass, pester),
					  NULL, NULL,
					  g_cclosure_marshal_VOID__ENUM, G_TYPE_NONE, 1, ESCO_TYPE_PESTER_TYPE);

	signals[FOCUSED] =
		g_signal_new ("focused",
					  G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_FIRST,
					  G_STRUCT_OFFSET (EscoChatBoxClass, focused),
					  NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
	signals[TITLE_CHANGED] =
		g_signal_new ("title-changed",
					  G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_LAST,
					  G_STRUCT_OFFSET (EscoChatBoxClass, title_changed),
					  NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
	signals[MODE_CHANGED] =
		g_signal_new ("mode-changed",
					  G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_LAST,
					  G_STRUCT_OFFSET (EscoChatBoxClass, mode_changed),
					  NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
	signals[USER_SELECTION_CHANGED] =
		g_signal_new ("user-selection-changed",
					  G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_LAST,
					  G_STRUCT_OFFSET (EscoChatBoxClass, user_selection_changed),
					  NULL, NULL, g_cclosure_marshal_VOID__BOOLEAN, G_TYPE_NONE, 1, G_TYPE_BOOLEAN);
}


static void
esco_chat_box_instance_init (EscoChatBox * box)
{
	box->viewer = NULL;
	box->status_icon = NULL;
}



/* PUBLIC API */
GType
esco_chat_box_get_type (void)
{
	static GType type = 0;

	if (!type)
	{
		static const GTypeInfo info = {
			sizeof (EscoChatBoxClass),
			NULL,				/* base_init */
			NULL,				/* base_finalize */
			(GClassInitFunc) esco_chat_box_class_init,
			NULL,				/* class_finalize */
			NULL,				/* class_data */
			sizeof (EscoChatBox),
			0,					/* n_preallocs */
			(GInstanceInitFunc) esco_chat_box_instance_init,
		};

		type = g_type_register_static (GTK_TYPE_VBOX, "EscoChatBox", &info, 0);
	}

	return type;
}


GtkWidget *
esco_chat_box_new (void)
{
	return GTK_WIDGET (g_object_new (ESCO_TYPE_CHAT_BOX, NULL));
}


void
esco_chat_box_set_nick (EscoChatBox * box,
						const gchar * nick)
{
	g_return_if_fail (box != NULL);
	g_return_if_fail (ESCO_IS_CHAT_BOX (box));

	if (box->dying)
		return;

	g_free (box->nick);

	if (nick != NULL)
		box->nick = g_strdup (nick);
	else
		box->nick = g_strdup (DEFAULT_NICK);

	if (box->nick_label != NULL)
		gtk_label_set_text (GTK_LABEL (box->nick_label), nick);

	if (box->viewer != NULL)
		esco_viewer_set_nick (ESCO_VIEWER (box->viewer), nick);

	g_object_notify (G_OBJECT (box), "nick");
}


void
esco_chat_box_set_mode (EscoChatBox * box,
						EscoChatModeType mode)
{
	g_return_if_fail (box != NULL);
	g_return_if_fail (ESCO_IS_CHAT_BOX (box));

	if (box->dying)
		return;

	box->mode = mode;

	g_signal_emit (G_OBJECT (box), signals[MODE_CHANGED], 0, NULL);

	g_object_notify (G_OBJECT (box), "mode");
}


void
esco_chat_box_set_encoding (EscoChatBox * box,
							const gchar * encoding)
{
	gchar *topic_str = NULL;

	g_return_if_fail (box != NULL);
	g_return_if_fail (ESCO_IS_CHAT_BOX (box));
	g_return_if_fail (esco_encoding_is_valid (encoding));

	g_free (box->encoding);

	box->encoding = g_strdup (encoding);

	if (box->topic != NULL)
	{
		topic_str = girc_convert_to_utf8 (box->topic, box->encoding);

		if (topic_str != NULL)
		{
			gtk_entry_set_text (GTK_ENTRY (box->topic_entry), topic_str);
			g_free (topic_str);
		}
		else
		{
			gtk_entry_set_text (GTK_ENTRY (box->topic_entry), "");
		}
	}

	/* Sub-widgets */
	if (box->viewer != NULL)
		esco_viewer_set_encoding (ESCO_VIEWER (box->viewer), encoding);

	if (box->userlist_view != NULL)
		esco_userlist_view_set_encoding (ESCO_USERLIST_VIEW (box->userlist_view), encoding);

	g_object_notify (G_OBJECT (box), "encoding");
}


void
esco_chat_box_set_title (EscoChatBox * box,
						 const gchar * str)
{
	g_return_if_fail (box != NULL);
	g_return_if_fail (ESCO_IS_CHAT_BOX (box));

	if (box->dying)
		return;

	g_free (box->title);

	if (str != NULL)
		box->title = g_strdup (str);
	else
		box->title = g_strdup (_(DEFAULT_TITLE));

	g_signal_emit (box, signals[TITLE_CHANGED], 0, NULL);
}


G_CONST_RETURN gchar *
esco_chat_box_get_mode_icon (EscoChatBox * box)
{
	const gchar *mode_icon;

	switch (box->mode)
	{
	case ESCO_CHAT_MODE_SERVER:
		mode_icon = GNOMECHAT_STOCK_SERVER;
		break;
	case ESCO_CHAT_MODE_CHANNEL:
		mode_icon = GNOMECHAT_STOCK_CHANNEL;
		break;
	case ESCO_CHAT_MODE_USER:
		mode_icon = GNOMECHAT_STOCK_USER;
		break;
	default:
		mode_icon = GTK_STOCK_NEW;
		break;
	}

	return mode_icon;
}


void
esco_chat_box_set_irc (EscoChatBox * box,
					   GIrcClient * irc)
{
	g_return_if_fail (box != NULL);
	g_return_if_fail (ESCO_IS_CHAT_BOX (box));
	g_return_if_fail (irc == NULL || (irc != NULL && GIRC_IS_CLIENT (irc)));

	if (box->dying)
		return;

	if (box->irc != NULL)
		g_object_weak_unref (G_OBJECT (irc), (GWeakNotify) irc_destroyed, box);

	box->target = NULL;

	box->irc = irc;

	if (irc != NULL)
		g_object_weak_ref (G_OBJECT (irc), (GWeakNotify) irc_destroyed, box);

	if (box->userlist_view != NULL)
		esco_userlist_view_set_irc (ESCO_USERLIST_VIEW (box->userlist_view), irc);
}


void
esco_chat_box_set_status (EscoChatBox * box,
						  GIrcChannelUserModeFlags modes)
{
	const gchar *icon;

	if (box->dying)
		return;

	if (modes & GIRC_CHANNEL_USER_MODE_CREATOR
		|| modes & GIRC_CHANNEL_USER_MODE_OPS || modes & GIRC_CHANNEL_USER_MODE_HALFOPS)
	{
		if (modes & GIRC_CHANNEL_USER_MODE_VOICE)
			icon = GNOMECHAT_STOCK_STATUS_OP_VOICE;
		else
			icon = GNOMECHAT_STOCK_STATUS_OP;
	}
	else if (modes & GIRC_CHANNEL_USER_MODE_VOICE)
	{
		icon = GNOMECHAT_STOCK_STATUS_VOICE;
	}
	else
	{
		icon = GNOMECHAT_STOCK_STATUS_NORMAL;
	}

	set_status_icon (box, icon);
}


void
esco_chat_box_set_subtitle (EscoChatBox * box,
							const gchar * str)
{
	g_return_if_fail (box != NULL);
	g_return_if_fail (ESCO_IS_CHAT_BOX (box));

	if (box->dying)
		return;

	g_free (box->subtitle);

	box->subtitle = g_strdup (str);

	g_signal_emit (box, signals[TITLE_CHANGED], 0, NULL);
}


void
esco_chat_box_set_topic (EscoChatBox * box,
						 const gchar * topic)
{
	g_return_if_fail (box != NULL);
	g_return_if_fail (ESCO_IS_CHAT_BOX (box));

	if (box->dying)
		return;

	g_free (box->topic);

	box->topic = g_strdup (topic);

	if (topic != NULL)
	{
		gchar *topic_str;

		topic_str = girc_convert_to_utf8 (topic, box->encoding);

		if (topic_str != NULL)
		{
			gtk_entry_set_text (GTK_ENTRY (box->topic_entry), topic_str);
			util_set_tooltip (box->topic_entry, topic_str, NULL, FALSE);
			g_free (topic_str);
		}
		else
		{
			util_set_tooltip (box->topic_entry, NULL, NULL, FALSE);
			gtk_entry_set_text (GTK_ENTRY (box->topic_entry), "");
		}
	}

	g_object_notify (G_OBJECT (box), "topic");
}


void
esco_chat_box_clear_viewer (EscoChatBox * box)
{
	GtkTextBuffer *buffer;

	g_return_if_fail (box != NULL);
	g_return_if_fail (ESCO_IS_CHAT_BOX (box));

	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (box->viewer));

	if (gtk_text_buffer_get_char_count (buffer) == 0)
		return;

	if (prefs_get_show_alert (GNOMECHAT_ALERT_CLEAR_CHAT))
	{
		GtkWidget *dialog,
		 *hbox,
		 *vbox,
		 *check;
		gchar *str;
		gint response;

		str = g_strdup_printf ("<b>%s</b>\n%s", _("Clear Chat?"),
							   _("Clearing this chat will permenantly loose any messages "
								 "recieved."));
		dialog = gtk_message_dialog_new (GTK_WINDOW (gtk_widget_get_toplevel (GTK_WIDGET (box))),
										 GTK_DIALOG_DESTROY_WITH_PARENT,
										 GTK_MESSAGE_WARNING, GTK_BUTTONS_NONE, str);
		g_free (str);
		gtk_dialog_add_buttons (GTK_DIALOG (dialog), GTK_STOCK_CANCEL,
								GTK_RESPONSE_CANCEL, GTK_STOCK_CLEAR, GTK_RESPONSE_OK, NULL);
		gtk_dialog_set_has_separator (GTK_DIALOG (dialog), FALSE);
		gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_CANCEL);
		gtk_window_set_role (GTK_WINDOW (dialog), "Clear");
		str = g_strconcat (_("Clear Chat?"), " - ", GENERIC_NAME, NULL);
		gtk_window_set_title (GTK_WINDOW (dialog), str);
		g_free (str);
		util_set_window_icon_from_stock (GTK_WINDOW (dialog), GTK_STOCK_DIALOG_WARNING);
		gtk_label_set_use_markup (GTK_LABEL (GTK_MESSAGE_DIALOG (dialog)->label), TRUE);

		hbox = gtk_hbox_new (FALSE, 0);
		gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), hbox, FALSE, FALSE, 6);
		gtk_widget_show (hbox);

		vbox = gtk_vbox_new (FALSE, 0);
		gtk_box_pack_start (GTK_BOX (hbox), vbox, FALSE, FALSE, 24);
		gtk_widget_show (vbox);

		check = gtk_check_button_new_with_label (_("Ask when clearing chats in the " "future."));
		g_signal_connect (check, "toggled", G_CALLBACK (util_show_again_toggled_cb),
						  GUINT_TO_POINTER (GNOMECHAT_ALERT_CLEAR_CHAT));
		gtk_container_add (GTK_CONTAINER (vbox), check);
		gtk_widget_show (check);

		response = gtk_dialog_run (GTK_DIALOG (dialog));
		gtk_widget_destroy (dialog);

		if (response != GTK_RESPONSE_OK)
			return;
	}

	esco_viewer_clear (ESCO_VIEWER (box->viewer));
}
