/*
 *  GnomeChat: src/chats-manager.c
 *
 *  Copyright (c) 2002 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "esco-chats-menu.h"

#include "esco-type-builtins.h"

#include "prefs.h"
#include "pixbufs.h"
#include "windows.h"

#include <gtk/gtkseparatormenuitem.h>
#include <gtk/gtkimagemenuitem.h>
#include <gtk/gtkimage.h>
#include <gtk/gtklabel.h>

#include <string.h>


#define MENUITEM_QUARK (menuitem_data_quark())

enum
{
	PESTER,
	LAST_SIGNAL
};


static GSList *all_menus = NULL;
static GSList *all_items = NULL;


static gint signals[LAST_SIGNAL] = { 0 };


static GQuark
menuitem_data_quark (void)
{
	static GQuark quark = 0;

	if (quark == 0)
	{
		quark = g_quark_from_static_string ("gnomechat-chat-box");
	}

	return quark;
}


static gint
menuitem_collate (gconstpointer data1,
				  gconstpointer data2)
{
	EscoChatBox *box1 = NULL,
	 *box2 = NULL;

	box1 = ESCO_CHAT_BOX (g_object_get_qdata (G_OBJECT (data1), MENUITEM_QUARK));
	box2 = ESCO_CHAT_BOX (g_object_get_qdata (G_OBJECT (data2), MENUITEM_QUARK));

	if (!box1 || !box2)
		return 0;

	return g_utf8_collate (box1->title, box2->title);
}


static gint
menuitem_chat_collate (gconstpointer data1,
					   gconstpointer data2)
{
	EscoChatBox *box1 = NULL,
	 *box2 = NULL;

	box1 = ESCO_CHAT_BOX (g_object_get_qdata (G_OBJECT (data1), MENUITEM_QUARK));
	box2 = ESCO_CHAT_BOX (g_object_get_qdata (G_OBJECT (data2), MENUITEM_QUARK));

	if (!box1 || !box2)
		return 0;

	if (box1->mode == box2->mode)
	{
		return g_utf8_collate (box1->title, box2->title);
	}
	else if (box1->mode < box1->mode)
	{
		return -1;
	}
	else if (box1->mode > box1->mode)
	{
		return 1;
	}

	return 0;
}


static GtkWidget *
find_box_in_menu (GtkMenuShell * menu,
				  gpointer box)
{
	GList *items;
	gpointer box_data;
	GtkWidget *retval = NULL;

	for (items = menu->children; items != NULL; items = items->next)
	{
		box_data = g_object_get_qdata (G_OBJECT (items->data), MENUITEM_QUARK);

		if (box_data == box)
		{
			retval = items->data;
			break;
		}
	}

	return retval;
}


static void
menuitem_activate_cb (GtkMenuItem * item,
					  EscoChatBox *box)
{
	windows_find_and_focus_chat (box);
}


static void
sort_menu (EscoChatsMenu * menu)
{
	GList *kids_free = NULL,
	 *kids = NULL,
	 *servers = NULL,
	 *chats = NULL,
	 *empty = NULL;
	gboolean any_servers = FALSE,
	  any_chats = FALSE;
	GtkWidget *box_wid = NULL;

	if (!all_items || !GTK_MENU_SHELL (menu)->children)
		return;

	for (kids = kids_free = g_list_copy (GTK_MENU_SHELL (menu)->children);
		 kids != NULL; kids = kids->next)
	{
		box_wid = g_object_get_qdata (G_OBJECT (kids->data), MENUITEM_QUARK);

		if (box_wid && ESCO_IS_CHAT_BOX (box_wid))
		{
			switch (ESCO_CHAT_BOX (box_wid)->mode)
			{
			case ESCO_CHAT_MODE_SERVER:
				servers = g_list_insert_sorted (servers, kids->data, menuitem_collate);
				break;

			case ESCO_CHAT_MODE_CHANNEL:
			case ESCO_CHAT_MODE_USER:
				chats = g_list_insert_sorted (chats, kids->data, menuitem_chat_collate);
				break;

			case ESCO_CHAT_MODE_NONE:
				empty = g_list_append (empty, kids->data);
				break;
			}

			g_object_ref (kids->data);
			gtk_container_remove (GTK_CONTAINER (menu), GTK_WIDGET (kids->data));
		}
		else
		{
			gtk_widget_destroy (GTK_WIDGET (kids->data));
		}
	}
	g_list_free (kids_free);


	for (any_servers = (servers != NULL);
		 servers != NULL; servers = g_list_remove (servers, servers->data))
	{
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), GTK_WIDGET (servers->data));
		g_object_unref (servers->data);
	}
	if (any_servers)
	{
		if (menu->separator1 == NULL)
		{
			menu->separator1 = gtk_separator_menu_item_new ();
			gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu->separator1);
			g_object_add_weak_pointer (G_OBJECT (menu->separator1),
									   (gpointer *) & (menu->separator1));
		}
		if (chats != NULL || empty != NULL)
			gtk_widget_show (menu->separator1);
	}


	for (any_chats = (chats != NULL);
		 chats != NULL; chats = g_list_remove (chats, chats->data))
	{
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), GTK_WIDGET (chats->data));
		g_object_unref (chats->data);
	}
	if (any_chats)
	{
		if (menu->separator2 == NULL)
		{
			menu->separator2 = gtk_separator_menu_item_new ();
			gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu->separator2);
			g_object_add_weak_pointer (G_OBJECT (menu->separator2),
									   (gpointer *) & (menu->separator2));
		}
		if (empty != NULL)
			gtk_widget_show (menu->separator2);
	}

	for (; empty != NULL; empty = g_list_remove (empty, empty->data))
	{
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), GTK_WIDGET (empty->data));
		g_object_unref (empty->data);
	}
}


static void
append_item_to_menu (EscoChatsMenu * menu,
					 EscoChatBox * box)
{
	GtkWidget *menuitem,
	 *image;

	menuitem = gtk_image_menu_item_new_with_label (box->title != NULL ? box->title : "");
	gtk_label_set_use_markup (GTK_LABEL (GTK_BIN (menuitem)->child), TRUE);
	image = gtk_image_new_from_stock (esco_chat_box_get_mode_icon (box),
									  GTK_ICON_SIZE_MENU);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (menuitem), image);

	g_signal_connect (menuitem, "activate", G_CALLBACK (menuitem_activate_cb), box);
	g_object_set_qdata (G_OBJECT (menuitem), MENUITEM_QUARK, box);

	gtk_widget_show_all (menuitem);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
}


static void
append_item_to_all_menus (EscoChatBox * box)
{
	GSList *menus;

	for (menus = all_menus; menus != NULL; menus = menus->next)
	{
		append_item_to_menu (ESCO_CHATS_MENU (menus->data), box);
		sort_menu (ESCO_CHATS_MENU (menus->data));
	}
}


static void
fill_menu (EscoChatsMenu * menu)
{
	GSList *items;

	for (items = all_items; items != NULL; items = items->next)
	{
		append_item_to_menu (menu, ESCO_CHAT_BOX (items->data));
	}
}


static void
remove_item_from_menus (gpointer where_box_was)
{
	GSList *menus;
	GtkWidget *item;

	for (menus = all_menus; menus != NULL; menus = menus->next)
	{
		item = find_box_in_menu (GTK_MENU_SHELL (menus->data), where_box_was);

		if (item)
			gtk_widget_destroy (item);

		sort_menu (ESCO_CHATS_MENU (menus->data));
	}
}


static void
menu_destroyed (gpointer data,
				GObject * where_menu_was)
{
	all_menus = g_slist_remove (all_menus, where_menu_was);
}


static void
box_destroyed (gpointer data,
			   GObject * where_box_was)
{
	all_items = g_slist_remove (all_items, where_box_was);
	remove_item_from_menus (where_box_was);
}


static void
box_mode_changed_cb (EscoChatBox * box,
					 gpointer data)
{
	GtkWidget *menuitem,
	 *image;
	GSList *menus;

	for (menus = all_menus; menus != NULL; menus = menus->next)
	{
		menuitem = find_box_in_menu (GTK_MENU_SHELL (menus->data), box);

		image = gtk_image_menu_item_get_image (GTK_IMAGE_MENU_ITEM (menuitem));
		gtk_image_set_from_stock (GTK_IMAGE (image),
								  esco_chat_box_get_mode_icon (box), GTK_ICON_SIZE_MENU);
		sort_menu (ESCO_CHATS_MENU (menus->data));
	}
}


static void
box_title_changed_cb (EscoChatBox * box,
					  gpointer data)
{
	GSList *menus;

	for (menus = all_menus; menus != NULL; menus = menus->next)
	{
		GtkWidget *menuitem = find_box_in_menu (GTK_MENU_SHELL (menus->data), box);

		gtk_label_set_text (GTK_LABEL (GTK_BIN (menuitem)->child), box->title);
		sort_menu (ESCO_CHATS_MENU (menus->data));

	}
}


static void
box_focused_cb (EscoChatBox * box,
				gpointer data)
{
	GSList *list;
	EscoPesterType max_level = ESCO_PESTER_NONE;

	/* First, find the higest pester level among all our items */
	for (list = all_items; list != NULL; list = list->next)
	{
		EscoChatBox *current_box = ESCO_CHAT_BOX (list->data);

		if (max_level < current_box->pester)
		{
			max_level = current_box->pester;
		}
	}

	/* Then, iterate through all the menus, setting the menuitem label and
	   emitting the "pester" signal with the current higest pester level. */
	for (list = all_menus; list != NULL; list = list->next)
	{
		GtkWidget *menuitem = find_box_in_menu (GTK_MENU_SHELL (list->data), box);

		gtk_label_set_markup (GTK_LABEL (GTK_BIN (menuitem)->child), box->title);

		g_signal_emit (list->data, signals[PESTER], 0, max_level);
	}
}


static void
box_pester_cb (EscoChatBox * box,
			   EscoPesterType level,
			   gpointer data)
{
	EscoChatBox *max_box = NULL;
	GtkWidget *menuitem;
	GSList *list;
	EscoPesterType max_level = ESCO_PESTER_NONE;

	/* First, find the maximum level in all our items */
	for (list = all_items; list != NULL; list = list->next)
	{
		if (max_level < ESCO_CHAT_BOX (list->data)->pester)
		{
			max_box = ESCO_CHAT_BOX (list->data);
			max_level = max_box->pester;
		}
	}

	/* Then, if the level we're being passed is greater than the previously
	   found max, or if the box who has the max level is the same one that's
	   giving us, this signal, and the given level is lower, emit the "pester"
	   signal for all our menus. */
	if (max_level < level || (max_level > level && max_box == box))
	{
		for (list = all_menus; list != NULL; list = list->next)
		{
			g_signal_emit (list->data, signals[PESTER], 0, level);
		}
	}

	/* Change the foreground color of the menuitem label if needed */
	if (!GTK_WIDGET_MAPPED (box) && level >= ESCO_PESTER_TEXT)
	{
		gchar *color,
		 *markup;

		color = prefs_get_notification_color (level);
		markup = g_strconcat ("<span foreground=\"", color, "\">", box->title,
							  "</span>", NULL);
		g_free (color);

		for (list = all_menus; list != NULL; list = list->next)
		{
			menuitem = find_box_in_menu (GTK_MENU_SHELL (list->data), box);
			gtk_label_set_markup (GTK_LABEL (GTK_BIN (menuitem)->child), markup);
		}

		g_free (markup);
	}
}


static void
esco_chats_menu_class_init (EscoChatsMenuClass * class)
{
	signals[PESTER] =
		g_signal_new ("pester",
					  G_TYPE_FROM_CLASS (G_OBJECT_CLASS (class)),
					  G_SIGNAL_RUN_LAST,
					  G_STRUCT_OFFSET (EscoChatsMenuClass, pester),
					  NULL, NULL,
					  g_cclosure_marshal_VOID__ENUM,
					  G_TYPE_NONE, 1, ESCO_TYPE_PESTER_TYPE);
}


static void
esco_chats_menu_instance_init (EscoChatsMenu * menu)
{
	menu->separator1 = NULL;
	menu->separator2 = NULL;

	fill_menu (menu);
	sort_menu (menu);

	all_menus = g_slist_append (all_menus, menu);
	g_object_weak_ref (G_OBJECT (menu), menu_destroyed, NULL);
}


GType
esco_chats_menu_get_type (void)
{
	static GType type = 0;

	if (!type)
	{
		static const GTypeInfo info = {
			sizeof (EscoChatsMenuClass),
			NULL,				/* base_init */
			NULL,				/* base_finalize */
			(GClassInitFunc) esco_chats_menu_class_init,
			NULL,				/* class_finalize */
			NULL,				/* class_data */
			sizeof (EscoChatsMenu),
			0,					/* n_preallocs */
			(GInstanceInitFunc) esco_chats_menu_instance_init,
		};

		type = g_type_register_static (GTK_TYPE_MENU, "EscoChatsMenu", &info, 0);
	}

	return type;
}


GtkWidget *
esco_chats_menu_new (void)
{
	return GTK_WIDGET (g_object_new (ESCO_TYPE_CHATS_MENU, NULL));
}


void
esco_chats_menus_add_item (EscoChatBox * box)
{
	g_return_if_fail (box != NULL);
	g_return_if_fail (ESCO_IS_CHAT_BOX (box));

	all_items = g_slist_append (all_items, box);
	g_object_weak_ref (G_OBJECT (box), box_destroyed, NULL);

	append_item_to_all_menus (box);

	g_signal_connect (box, "mode-changed", G_CALLBACK (box_mode_changed_cb), NULL);
	g_signal_connect (box, "title-changed", G_CALLBACK (box_title_changed_cb), NULL);
	g_signal_connect (box, "focused", G_CALLBACK (box_focused_cb), NULL);
	g_signal_connect (box, "pester", G_CALLBACK (box_pester_cb), NULL);
}
