/*
 *  GnomeChat: src/gnomechat-rdf-types.c
 *
 *  Copyright (c) 2003 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.01234567890
 */

#include "gnomechat-rdf-types.h"


/* ************************************************************************** *
 *  GnomechatI18nString Functions                                             *
 * ************************************************************************** */

GType
gnomechat_i18n_string_get_type (void)
{
	static GType type = G_TYPE_INVALID;

	if (type == G_TYPE_INVALID)
	{
		type = g_boxed_type_register_static ("GnomechatI18nString",
											 (GBoxedCopyFunc) gnomechat_i18n_string_dup,
											 (GBoxedFreeFunc) gnomechat_i18n_string_free);
	}

	return type;
}


GnomechatI18nString *
gnomechat_i18n_string_dup (GnomechatI18nString * str)
{
	g_return_val_if_fail (str != NULL, NULL);

	return gnomechat_i18n_string_new (str->lang, str->str);
}


void
gnomechat_i18n_string_free (GnomechatI18nString * str)
{
	if (str == NULL)
		return;

	g_free (str->str);
	g_free (str->lang);

	g_free (str);
}


GnomechatI18nString *
gnomechat_i18n_string_new (const gchar * str,
						   const gchar * lang)
{
	GnomechatI18nString *retval;

	g_return_val_if_fail (str != NULL, NULL);

	retval = g_new0 (GnomechatI18nString, 1);

	retval->lang = g_strdup (lang);
	retval->str = g_strdup (str);

	return retval;
}
