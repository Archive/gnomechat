/*
 *  GnomeChat: src/pixbufs.c
 *
 *  Copyright (c) 2002 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "gnomechat.h"

#include "pixbufs.h"
#include "utils.h"

#include <gtk/gtkimage.h>
#include <gtk/gtkstock.h>


#define PIXBUF_ANIM_OBJ_KEY		(get_anim_obj_quark ())
#define CURRENT_FRAME_OBJ_KEY	(get_current_frame_quark ())
#define N_FRAMES_KEY			(get_number_of_frames_quark ())


/* Utility Functions */
static GQuark
get_anim_obj_quark (void)
{
	static GQuark quark = 0;

	if (quark == 0)
	{
		quark = g_quark_from_static_string ("gnomechat-entire-pixbuf");
	}

	return quark;
}


static GQuark
get_current_frame_quark (void)
{
	static GQuark quark = 0;

	if (quark == 0)
	{
		quark = g_quark_from_static_string ("gnomechat-current-frame");
	}

	return quark;
}


static GQuark
get_number_of_frames_quark (void)
{
	static GQuark quark = 0;

	if (quark == 0)
	{
		quark = g_quark_from_static_string ("gnomechat-n-frames");
	}

	return quark;
}


/* Public API */
GdkPixbuf *
pixbuf_load_file (const gchar * filename)
{
	GdkPixbuf *pixbuf = NULL;
	gchar *path = NULL;

	if (util_str_is_not_empty (filename))
	{
		if (g_path_is_absolute (filename) && g_file_test (filename, G_FILE_TEST_EXISTS))
		{
			path = g_strdup (filename);
		}
		else
		{
			path = gnomechat_get_pixmap_path (filename);
		}
	}

	if (path != NULL)
	{
		pixbuf = gdk_pixbuf_new_from_file (path, NULL);
		g_free (path);
	}

	return pixbuf;
}


GtkWidget *
pixbuf_anim_from_file (const gchar * filename,
					   guint n_frames)
{
	GtkWidget *image;
	GdkPixbuf *pixbuf,
	 *frame;
	guint width,
	  height,
	  frame_width;

	g_return_val_if_fail (filename != NULL && filename[0] != '\0', NULL);
	g_return_val_if_fail (n_frames > 0, NULL);

	pixbuf = pixbuf_load_file (filename);

	if (pixbuf == NULL)
		return gtk_image_new_from_stock (GTK_STOCK_MISSING_IMAGE, GTK_ICON_SIZE_DIALOG);
	else
		image = gtk_image_new ();

	width = gdk_pixbuf_get_width (pixbuf);
	height = gdk_pixbuf_get_height (pixbuf);

	frame_width = width / n_frames;

	frame = gdk_pixbuf_new_subpixbuf (pixbuf, 0, 0, frame_width, height);

	g_object_set_qdata (G_OBJECT (image), PIXBUF_ANIM_OBJ_KEY, pixbuf);
	g_object_set_qdata (G_OBJECT (image), CURRENT_FRAME_OBJ_KEY, GUINT_TO_POINTER (0));
	g_object_set_qdata (G_OBJECT (image), N_FRAMES_KEY, GUINT_TO_POINTER (n_frames));

	gtk_image_set_from_pixbuf (GTK_IMAGE (image), frame);
	g_object_unref (frame);

	return image;
}


void
pixbuf_anim_update (GtkWidget * image)
{
	GdkPixbuf *frame,
	 *pixbuf = GDK_PIXBUF (g_object_get_qdata (G_OBJECT (image), PIXBUF_ANIM_OBJ_KEY));
	guint current_frame,
	  width,
	  height,
	  n_frames = GPOINTER_TO_UINT (g_object_get_qdata (G_OBJECT (image), N_FRAMES_KEY)),
	  frame_width;

	g_return_if_fail (image != NULL);
	g_return_if_fail (GTK_IS_IMAGE (image));
	g_return_if_fail (pixbuf != NULL);

	width = gdk_pixbuf_get_width (pixbuf);
	height = gdk_pixbuf_get_height (pixbuf);

	frame_width = width / (n_frames > 0 ? n_frames : 1);

	current_frame = GPOINTER_TO_UINT (g_object_get_qdata (G_OBJECT (image), CURRENT_FRAME_OBJ_KEY));
	current_frame = (current_frame + 1 > n_frames - 1 ? 0 : current_frame + 1);

	frame = gdk_pixbuf_new_subpixbuf (pixbuf, current_frame * frame_width, 0, frame_width, height);
	gtk_image_set_from_pixbuf (GTK_IMAGE (image), frame);
	g_object_unref (frame);

	g_object_set_qdata (G_OBJECT (image), CURRENT_FRAME_OBJ_KEY, GUINT_TO_POINTER (current_frame));
}


GdkPixbuf *
pixbuf_scale_to_stock_size (GdkPixbuf * src,
							GtkIconSize size)
{
	GdkPixbuf *dest = NULL;
	gint dest_width = 0,
	  dest_height = 0;

	g_return_val_if_fail (GDK_IS_PIXBUF (src), NULL);

	if (gtk_icon_size_lookup (size, &dest_width, &dest_height))
	{
		if (dest_width != gdk_pixbuf_get_width (src) || dest_height != gdk_pixbuf_get_height (src))
		{
			dest = gdk_pixbuf_scale_simple (src, dest_width, dest_height, GDK_INTERP_BILINEAR);
		}
		else
		{
			dest = g_object_ref (src);
		}
	}
	else
	{
		dest = g_object_ref (src);
	}

	return dest;
}
