/*
 *  GnomeChat: src/prefs-ui-utils.h
 *
 *  Copyright (c) 2002 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef __PREFS_UI_UTILS_H__
#define __PREFS_UI_UTILS_H__


#include <gtk/gtkbox.h>
#include <gtk/gtkliststore.h>
#include <gtk/gtktreestore.h>


#define PREFS_NETWORK_ID_KEY (prefs_ui_util_get_network_id_quark ())
#define OPTMENU_LABEL(optmenu) \
	(((GtkBoxChild *)(g_list_last (GTK_BOX (GTK_BIN (optmenu)->child)->children)->data))->widget)


enum
{
	SERVER_LIST_ENABLED_ITEM,
	SERVER_LIST_COUNTRY_CODE_ITEM,
	SERVER_LIST_LOCATION_ITEM,
	SERVER_LIST_ADDRESS_ITEM,
	SERVER_LIST_KEY_ITEM,
	SERVER_LIST_NUM_COLS
};

enum
{
	NETWORK_LIST_STOCK_PIXBUF_ITEM,
	NETWORK_LIST_NAME_ITEM,
	NETWORK_LIST_DESCRIPTION_ITEM,
	NETWORK_LIST_WEBSITE_ITEM,
	NETWORK_LIST_KEY_ITEM,
	NETWORK_LIST_NUM_COLS
};

enum
{
	CHANNEL_LIST_NAME_ITEM,
	CHANNEL_LIST_NUM_COLS
};


/* These are just some convenience functions to auto-create some complex
   UI items based on preferences */
GtkWidget *prefs_ui_util_get_network_menu (void);
GtkTreeModel *prefs_ui_util_get_network_model (void);

GtkTreeModel *prefs_ui_util_get_server_model (const gchar * network_id);

GtkTreeModel *prefs_ui_util_get_channel_model (const gchar * network_id);

GQuark prefs_ui_util_get_network_id_quark (void);


#endif /* __PREFS_UI_UTILS_H__ */
