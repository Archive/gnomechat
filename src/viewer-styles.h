/*
 *  GnomeChat: src/viewer-styles.h
 *
 *  Copyright (c) 2002 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef __VIEWER_STYLES_H__
#define __VIEWER_STYLES_H__


#include <glib.h>


typedef enum
{
	VIEWER_STYLE_FIRST,
	VIEWER_STYLE_LINK = VIEWER_STYLE_FIRST,
	VIEWER_STYLE_MY_ACTIONS,
	VIEWER_STYLE_MY_NICK,
	VIEWER_STYLE_TO_ME,
	VIEWER_STYLE_TO_ME_ACTIONS,
	VIEWER_STYLE_TO_ME_NICK,
	VIEWER_STYLE_ACTIONS,
	VIEWER_STYLE_NICK,
	VIEWER_STYLE_SERVER,
	VIEWER_STYLE_SERVER_NAME,
	VIEWER_STYLE_CHANNEL,
	VIEWER_STYLE_CHANNEL_NICK,
	VIEWER_STYLE_CHANNEL_NAME,
	VIEWER_STYLE_CHANNEL_VALUE,
	VIEWER_STYLE_ERROR,
	VIEWER_STYLE_LAST
}
ViewerStyleType;


typedef struct _ViewerStyle
{
	gchar *fg_color;
	gchar *bg_color;
	gboolean bold;
	gboolean italic;
	gboolean uline;
}
ViewerStyle;


#endif	/* __VIEWER_STYLES_H__ */
