/*
 *  GnomeChat: src/chats-manager.h
 *
 *  Copyright (c) 2002 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef __ESCO_CHATS_MENU_H__
#define __ESCO_CHATS_MENU_H__


#include <esco-enums.h>
#include <esco-chat-box.h>

#include <glib.h>
#include <glib-object.h>

#include <gtk/gtkmenu.h>
#include <gtk/gtkwidget.h>


#define ESCO_TYPE_CHATS_MENU			(esco_chats_menu_get_type ())
#define ESCO_CHATS_MENU(obj)			(G_TYPE_CHECK_INSTANCE_CAST ((obj), ESCO_TYPE_CHATS_MENU, EscoChatsMenu))
#define ESCO_CHATS_MENU_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass), ESCO_TYPE_CHATS_MENU, EscoChatsMenuClass))
#define ESCO_IS_CHATS_MENU(obj)			(G_TYPE_CHECK_INSTANCE_TYPE ((obj), ESCO_TYPE_CHATS_MENU))
#define ESCO_IS_CHATS_MENU_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), ESCO_TYPE_CHATS_MENU))
#define ESCO_CHATS_MENU_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS ((obj), ESCO_TYPE_CHATS_MENU, EscoChatsMenuClass))


typedef struct _EscoChatsMenu EscoChatsMenu;
typedef struct _EscoChatsMenuClass EscoChatsMenuClass;


typedef void (*EscoChatsMenuPesterFunc) (EscoChatsMenu *menu,
										 EscoPesterType pester);


struct _EscoChatsMenu
{
	GtkMenu parent;

	GtkWidget *separator1;
	GtkWidget *separator2;
};

struct _EscoChatsMenuClass
{
	GtkMenuClass parent_class;

	EscoChatsMenuPesterFunc pester;
};


GType esco_chats_menu_get_type (void);

GtkWidget *esco_chats_menu_new (void);

void esco_chats_menus_add_item (EscoChatBox * box);


#endif /* __ESCO_CHATS_MENU_H__ */
