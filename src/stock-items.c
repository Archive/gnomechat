/*
 *  GnomeChat: src/stock-items.c
 *
 *  Copyright (c) 2002 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "gnomechat.h"
#include "stock-items.h"

#include "utils.h"

#include <gtk/gtkiconfactory.h>
#include <gtk/gtkstock.h>


typedef struct
{
	const gchar *stock_id;
	const GtkIconSize fallback_size;
	const gchar *const filenames[6];	/* 16, 18, 24, 20, 32, 48 */
}
StockFileSet;


static const StockFileSet stock_file_sets[] = {
	{GNOMECHAT_STOCK_ICON, GTK_ICON_SIZE_DIALOG,
	 {PACKAGE "/gnomechat_16.png",
	  NULL,
	  NULL,
	  NULL,
	  NULL,
	  "gnomechat.png"}
	 },
	{GNOMECHAT_STOCK_MY_USER, GTK_ICON_SIZE_DIALOG,
	 {NULL,
	  NULL,
	  NULL,
	  NULL,
	  NULL,
	  PACKAGE "/my-user_48.png"}
	 },
	{GNOMECHAT_STOCK_VCARD, GTK_ICON_SIZE_DIALOG,
	 {PACKAGE "/vcard_16.png",
	  NULL,
	  PACKAGE "/vcard_24.xpm",
	  NULL,
	  PACKAGE "/vcard_32.png",
	  PACKAGE "/vcard_48.png"}
	 },

	{GNOMECHAT_STOCK_GLOBE, GTK_ICON_SIZE_DIALOG,
	 {PACKAGE "/globe_16.png",
	  NULL,
	  PACKAGE "/globe_24.png",
	  NULL,
	  PACKAGE "/globe_32.png",
	  PACKAGE "/globe_48.png"}
	 },

	{GNOMECHAT_STOCK_CONNECT, GTK_ICON_SIZE_LARGE_TOOLBAR,
	 {PACKAGE "/connect_16.png",
	  NULL,
	  PACKAGE "/connect_24.png",
	  NULL,
	  NULL,
	  NULL}
	 },
	{GNOMECHAT_STOCK_DISCONNECT, GTK_ICON_SIZE_LARGE_TOOLBAR,
	 {PACKAGE "/disconnect_16.png",
	  NULL,
	  PACKAGE "/disconnect_24.png",
	  NULL,
	  NULL,
	  NULL}
	 },

	{GNOMECHAT_STOCK_ENCODING, GTK_ICON_SIZE_LARGE_TOOLBAR,
	 {PACKAGE "/encoding_16.png",
	  NULL,
	  PACKAGE "/encoding_24.png",
	  NULL,
	  NULL,
	  NULL}
	 },

	{GNOMECHAT_STOCK_NETWORK, GTK_ICON_SIZE_DIALOG,
	 {PACKAGE "/network_16.png",
	  NULL,
	  NULL,
	  NULL,
	  NULL,
	  PACKAGE "/network_48.png"}
	 },
	{GNOMECHAT_STOCK_SERVER, GTK_ICON_SIZE_DIALOG,
	 {PACKAGE "/server_16.png",
	  NULL,
	  PACKAGE "/server_24.png",
	  NULL,
	  NULL,
	  PACKAGE "/server_48.png"}
	 },
	{GNOMECHAT_STOCK_CHANNEL, GTK_ICON_SIZE_LARGE_TOOLBAR,
	 {PACKAGE "/channel_16.png",
	  NULL,
	  PACKAGE "/channel_24.png",
	  NULL,
	  NULL,
	  NULL}
	 },
	{GNOMECHAT_STOCK_QUERY, GTK_ICON_SIZE_DIALOG,
	 {PACKAGE "/query_16.png",
	  NULL,
	  PACKAGE "/query_24.png",
	  PACKAGE "/query_20.png",
	  PACKAGE "/query_32.png",
	  PACKAGE "/query_48.png"}
	 },
	{GNOMECHAT_STOCK_USER, GTK_ICON_SIZE_LARGE_TOOLBAR,
	 {PACKAGE "/user_16.png",
	  NULL,
	  PACKAGE "/user_24.png",
	  NULL,
	  NULL,
	  NULL}
	 },

	{GNOMECHAT_STOCK_CATEGORY_REGIONAL, GTK_ICON_SIZE_DIALOG,
	 {NULL,
	  NULL,
	  NULL,
	  NULL,
	  NULL,
	  PACKAGE "/regional.png"}
	 },
	{GNOMECHAT_STOCK_CATEGORY_INTEREST, GTK_ICON_SIZE_DIALOG,
	 {NULL,
	  NULL,
	  NULL,
	  NULL,
	  NULL,
	  PACKAGE "/interest.png"}
	 },

	{GNOMECHAT_STOCK_KICK, GTK_ICON_SIZE_LARGE_TOOLBAR,
	 {PACKAGE "/kick_16.png",
	  NULL,
	  PACKAGE "/kick_24.png",
	  PACKAGE "/kick_20.png",
	  NULL,
	  PACKAGE "/kick_48.png"}
	 },

	{GNOMECHAT_STOCK_SERVER_PROPERTIES, GTK_ICON_SIZE_MENU,
	 {PACKAGE "/server_props_16.png",
	  NULL,
	  PACKAGE "/server_props_24.png",
	  NULL,
	  NULL,
	  NULL}
	 },
	{GNOMECHAT_STOCK_CHANNEL_PROPERTIES, GTK_ICON_SIZE_LARGE_TOOLBAR,
	 {PACKAGE "/channel_props_16.png",
	  NULL,
	  PACKAGE "/channel_props_24.png",
	  NULL,
	  NULL,
	  NULL}
	 },
	{GNOMECHAT_STOCK_USER_PROPERTIES, GTK_ICON_SIZE_LARGE_TOOLBAR,
	 {PACKAGE "/user_props_16.png",
	  NULL,
	  PACKAGE "/user_props_24.png",
	  NULL,
	  NULL,
	  NULL}
	 },

	{GNOMECHAT_STOCK_STATUS_OP, GTK_ICON_SIZE_DND,
	 {PACKAGE "/op_16.png",
	  NULL,
	  NULL,
	  NULL,
	  PACKAGE "/op_32.png",
	  NULL}
	 },
	{GNOMECHAT_STOCK_STATUS_VOICE, GTK_ICON_SIZE_DND,
	 {PACKAGE "/voice_16.png",
	  NULL,
	  NULL,
	  NULL,
	  PACKAGE "/voice_32.png",
	  NULL}
	 },
	{GNOMECHAT_STOCK_STATUS_OP_VOICE, GTK_ICON_SIZE_DND,
	 {PACKAGE "/op_voice_16.png",
	  NULL,
	  NULL,
	  NULL,
	  PACKAGE "/op_voice_32.png",
	  NULL}
	 },
	{
	 GNOMECHAT_STOCK_STATUS_IRCOP, GTK_ICON_SIZE_DND,
	 {PACKAGE "/ircop_16.png",
	  NULL,
	  NULL,
	  NULL,
	  PACKAGE "/ircop_32.png",
	  NULL}
	 },
	{
	 GNOMECHAT_STOCK_STATUS_NORMAL, GTK_ICON_SIZE_DND,
	 {PACKAGE "/normal_16.png",
	  NULL,
	  NULL,
	  NULL,
	  PACKAGE "/normal_32.png",
	  NULL}
	 },

	{
	 GNOMECHAT_STOCK_SMILEY_REGULAR, GTK_ICON_SIZE_MENU,
	 {PACKAGE "/smiley_regular_16.png",
	  NULL,
	  NULL,
	  NULL,
	  NULL,
	  NULL}
	 },
	{GNOMECHAT_STOCK_SMILEY_BIG, GTK_ICON_SIZE_MENU,
	 {PACKAGE "/smiley_big_16.png",
	  NULL,
	  NULL,
	  NULL,
	  NULL,
	  NULL}
	 },
	{GNOMECHAT_STOCK_SMILEY_WINK, GTK_ICON_SIZE_MENU,
	 {PACKAGE "/smiley_wink_16.png",
	  NULL,
	  NULL,
	  NULL,
	  NULL,
	  NULL}
	 },
	{GNOMECHAT_STOCK_SMILEY_CRY, GTK_ICON_SIZE_MENU,
	 {PACKAGE "/smiley_cry_16.png",
	  NULL,
	  NULL,
	  NULL,
	  NULL,
	  NULL}
	 },
	{GNOMECHAT_STOCK_SMILEY_FROWN, GTK_ICON_SIZE_MENU,
	 {PACKAGE "/smiley_frown_16.png",
	  NULL,
	  NULL,
	  NULL,
	  NULL,
	  NULL}
	 },
	{GNOMECHAT_STOCK_SMILEY_SMIRK, GTK_ICON_SIZE_MENU,
	 {PACKAGE "/smiley_smirk_16.png",
	  NULL,
	  NULL,
	  NULL,
	  NULL,
	  NULL}
	 },
	{GNOMECHAT_STOCK_SMILEY_TONGUE, GTK_ICON_SIZE_MENU,
	 {PACKAGE "/smiley_tongue_16.png",
	  NULL,
	  NULL,
	  NULL,
	  NULL,
	  NULL}
	 },
	{GNOMECHAT_STOCK_SMILEY_LAUGH, GTK_ICON_SIZE_MENU,
	 {PACKAGE "/smiley_laugh_16.png",
	  NULL,
	  NULL,
	  NULL,
	  NULL,
	  NULL}
	 },
	{GNOMECHAT_STOCK_SMILEY_SHOCK, GTK_ICON_SIZE_MENU,
	 {PACKAGE "/smiley_shock_16.png",
	  NULL,
	  NULL,
	  NULL,
	  NULL,
	  NULL}
	 },
	{GNOMECHAT_STOCK_SMILEY_NONE, GTK_ICON_SIZE_MENU,
	 {PACKAGE "/smiley_none_16.png",
	  NULL,
	  NULL,
	  NULL,
	  NULL,
	  NULL}
	 }
};


static GtkStockItem stock_items[] = {
	{GNOMECHAT_STOCK_CONNECT,
	 N_("Connec_t"),
	 0, 0, GETTEXT_PACKAGE},
	{GNOMECHAT_STOCK_DISCONNECT,
	 N_("_Disconnect"),
	 0, 0, GETTEXT_PACKAGE},

	{GNOMECHAT_STOCK_EDIT,
	 N_("_Edit"),
	 0, 0, GETTEXT_PACKAGE},
	{GNOMECHAT_STOCK_INFO,
	 N_("_Information"),
	 0, 0, GETTEXT_PACKAGE},

	{GNOMECHAT_STOCK_SENDFILE,
	 N_("Send _File"),
	 0, 0, GETTEXT_PACKAGE},
	{GNOMECHAT_STOCK_JOIN,
	 N_("_Join"),
	 0, 0, GETTEXT_PACKAGE},
	{GNOMECHAT_STOCK_QUERY,
	 N_("Cha_t"),
	 0, 0, GETTEXT_PACKAGE},

	{GNOMECHAT_STOCK_KICK,
	 N_("_Kick"),
	 0, 0, GETTEXT_PACKAGE},

	{GNOMECHAT_STOCK_SERVER_PROPERTIES,
	 N_("_Server Properties"),
	 0, 0, GETTEXT_PACKAGE},
	{GNOMECHAT_STOCK_CHANNEL_PROPERTIES,
	 N_("Channel _Properties"),
	 0, 0, GETTEXT_PACKAGE},
	{GNOMECHAT_STOCK_USER_PROPERTIES,
	 N_("_User Properties"),
	 0, 0, GETTEXT_PACKAGE},

	{GNOMECHAT_STOCK_ENCODING,
	 N_("Character C_oding"),
	 0, 0, GETTEXT_PACKAGE}
};


static GHashTable *file_ids = NULL;
static GtkIconFactory *factory = NULL;


static void
add_source (GtkIconSet * set,
			GtkIconSize size,
			const gchar * filename,
			gboolean wildcard_size)
{
	gchar *path;
	GtkIconSource *source;

	path = gnomechat_get_pixmap_path (filename);

	if (path == NULL)
	{
		g_warning ("Unable to find file '%s' in pixmap path.", filename);
		return;
	}

	source = gtk_icon_source_new ();
	gtk_icon_source_set_filename (source, path);
	gtk_icon_source_set_size_wildcarded (source, wildcard_size);
	gtk_icon_source_set_size (source, size);
	gtk_icon_set_add_source (set, source);

	gtk_icon_source_free (source);
	g_free (path);
}


void
stock_init (void)
{
	GtkIconSet *set;
	GtkIconSize size;
	gint i_set,
	  i_file;

	/* Stock Images */
	if (factory == NULL)
		factory = gtk_icon_factory_new ();

	if (file_ids == NULL)
		file_ids = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_free);

	for (i_set = 0; i_set < G_N_ELEMENTS (stock_file_sets); i_set++)
	{
		set = gtk_icon_set_new ();

		/* Load the standard sizes */
		for (i_file = 0; i_file < G_N_ELEMENTS (stock_file_sets[i_set].filenames); i_file++)
		{
			if (stock_file_sets[i_set].fallback_size != i_file + 1
				&& stock_file_sets[i_set].filenames[i_file] != NULL)
			{
				add_source (set, i_file + 1, stock_file_sets[i_set].filenames[i_file], FALSE);
			}
		}

		/* Load the fallback size */
		size = stock_file_sets[i_set].fallback_size;
		add_source (set, size, stock_file_sets[i_set].filenames[size - 1], TRUE);

		/* Add the new stock image to the factory */
		gtk_icon_factory_add (factory, stock_file_sets[i_set].stock_id, set);
		gtk_icon_set_unref (set);
	}

	/* Create a copy of GTK_STOCK_SAVE to use in GNOMECHAT_STOCK_SENDFILE */
	set = gtk_icon_set_copy (gtk_icon_factory_lookup_default (GTK_STOCK_SAVE));
	gtk_icon_factory_add (factory, GNOMECHAT_STOCK_SENDFILE, set);
	gtk_icon_set_unref (set);

	/* Create a copy of GTK_STOCK_PROPERTIES to use in GNOMECHAT_STOCK_EDIT */
	set = gtk_icon_set_copy (gtk_icon_factory_lookup_default (GTK_STOCK_PROPERTIES));
	add_source (set, GTK_ICON_SIZE_BUTTON, PACKAGE "/properties_20.png", FALSE);
	gtk_icon_factory_add (factory, GNOMECHAT_STOCK_EDIT, set);
	gtk_icon_set_unref (set);

	/* Create a copy of GTK_STOCK_DIALOG_INFO to use in GNOMECHAT_STOCK_INFO */
	set = gtk_icon_set_copy (gtk_icon_factory_lookup_default (GTK_STOCK_DIALOG_INFO));
	gtk_icon_factory_add (factory, GNOMECHAT_STOCK_INFO, set);
	gtk_icon_set_unref (set);

	/* Create a copy of GTK_STOCK_JUMP_TO to use in GNOMECHAT_STOCK_JOIN */
	set = gtk_icon_set_copy (gtk_icon_factory_lookup_default (GTK_STOCK_JUMP_TO));
	gtk_icon_factory_add (factory, GNOMECHAT_STOCK_JOIN, set);
	gtk_icon_set_unref (set);

	/* Add our properties_20.png to gtk-stock-properties */
	set = gtk_icon_factory_lookup_default (GTK_STOCK_PROPERTIES);
	add_source (set, GTK_ICON_SIZE_BUTTON, PACKAGE "/properties_20.png", FALSE);

	/* Add our find_48.png to gtk-stock-find */
//  set = gtk_icon_factory_lookup_default (GTK_STOCK_FIND);
//  add_source (set, GTK_ICON_SIZE_DIALOG, PACKAGE "/find_48.png", FALSE);

	/* Add the factory to the default list */
	gtk_icon_factory_add_default (factory);

	/* Add the stock items */
	gtk_stock_add_static (stock_items, G_N_ELEMENTS (stock_items));
}


G_CONST_RETURN gchar *
stock_get_id_from_file (const gchar * filename)
{
	GtkIconSet *set;
	GdkPixbuf *pixbuf;
	GError *err = NULL;
	gchar *path = NULL;

	g_return_val_if_fail (util_str_is_not_empty (filename), NULL);

	if (gtk_icon_factory_lookup_default (filename))
	{
		return filename;
	}

	if (g_path_is_absolute (filename) && g_file_test (filename, G_FILE_TEST_EXISTS))
	{
		path = (gchar *) filename;
	}
	else
	{
		path = gnomechat_get_pixmap_path (filename);
	}

	if (path != NULL)
	{
		pixbuf = gdk_pixbuf_new_from_file (path, &err);

		if (pixbuf != NULL)
		{
			set = gtk_icon_set_new_from_pixbuf (pixbuf);
			gtk_icon_factory_add (factory, filename, set);
		}
		else
		{
			g_warning ("Unable to load image '%s': %s", path, err->message);
			g_error_free (err);
		}
	}
	else
	{
		g_warning ("Unable to find file '%s' in pixmap path.", filename);
	}

	if (path != filename)
		g_free (path);

	return filename;
}


GList *
stock_get_icon_list_from_stock (GtkWidget * widget,
								const gchar * stock_id)
{
	GtkIconSet *set;
	GList *list = NULL;

	set = gtk_style_lookup_icon_set (widget->style, stock_id);

	if (set != NULL)
	{
		GdkPixbuf *pixbuf = NULL;
		GtkIconSize *sizes = NULL;
		gint i,
		  n_sizes;

		gtk_icon_set_get_sizes (set, &sizes, &n_sizes);

		for (i = 0; i < n_sizes; i++)
		{
			pixbuf = gtk_widget_render_icon (widget, stock_id, sizes[i], NULL);

			if (pixbuf != NULL)
				list = g_list_prepend (list, pixbuf);
		}

		g_free (sizes);
	}

	return g_list_reverse (list);
}
