/*
 *  GnomeChat: src/gnomechat-rdf-network.h
 *
 *  Copyright (c) 2003 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. 01234567890
 */

#ifndef __GNOMECHAT_RDF_NETWORK_H__
#define __GNOMECHAT_RDF_NETWORK_H__

#include "gnomechat-rdf-server.h"

#include <gdk-pixbuf/gdk-pixbuf.h>


#define GNOMECHAT_TYPE_RDF_NETWORK				(gnomechat_rdf_network_get_type ())
#define GNOMECHAT_RDF_NETWORK(obj)				(G_TYPE_CHECK_INSTANCE_CAST ((obj), GNOMECHAT_TYPE_RDF_NETWORK, GnomechatRdfNetwork))
#define GNOMECHAT_IS_RDF_NETWORK(obj)			(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GNOMECHAT_TYPE_RDF_NETWORK))
#define GNOMECHAT_RDF_NETWORK_CLASS(obj)		(G_TYPE_CHECK_CLASS_CAST ((obj), GNOMECHAT_TYPE_RDF_NETWORK, GnomechatRdfNetworkClass))
#define GNOMECHAT_IS_RDF_NETWORK_CLASS(obj)		(G_TYPE_CHECK_CLASS_TYPE ((obj), GNOMECHAT_TYPE_RDF_NETWORK))
#define GNOMECHAT_RDF_NETWORK_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS ((obj), GNOMECHAT_TYPE_RDF_NETWORK, GnomechatRdfNetworkClass))


typedef struct _GnomechatRdfNetwork GnomechatRdfNetwork;
typedef struct _GnomechatRdfNetworkPrivate GnomechatRdfNetworkPrivate;
typedef struct _GnomechatRdfNetworkClass GnomechatRdfNetworkClass;

struct _GnomechatRdfNetwork
{
	/*< private > */
	GObject parent;

	GnomechatRdfNetworkPrivate *_priv;
};

struct _GnomechatRdfNetworkClass
{
	GObjectClass parent_class;
};


GType gnomechat_rdf_network_get_type (void);

GObject *gnomechat_rdf_network_new (const gchar * uri);

G_CONST_RETURN gchar *gnomechat_rdf_network_get_uri (GnomechatRdfNetwork * network);
void gnomechat_rdf_network_set_uri (GnomechatRdfNetwork * network, const gchar * uri);

G_CONST_RETURN gchar *gnomechat_rdf_network_get_data_uri (GnomechatRdfNetwork * network);
void gnomechat_rdf_network_set_data_uri (GnomechatRdfNetwork * network, const gchar * data_uri);
G_CONST_RETURN gchar *gnomechat_rdf_network_get_data_path (GnomechatRdfNetwork * network);

G_CONST_RETURN gchar *gnomechat_rdf_network_get_icon_uri (GnomechatRdfNetwork * network);
void gnomechat_rdf_network_set_icon_uri (GnomechatRdfNetwork * network, const gchar * icon_uri);

GdkPixbuf *gnomechat_rdf_network_get_icon (GnomechatRdfNetwork * network);

G_CONST_RETURN gchar *gnomechat_rdf_network_get_description (GnomechatRdfNetwork * network);
void gnomechat_rdf_network_try_description (GnomechatRdfNetwork * network,
											const gchar * description, const gchar * lang);

G_CONST_RETURN gchar *gnomechat_rdf_network_get_website (GnomechatRdfNetwork * network);
void gnomechat_rdf_network_try_website (GnomechatRdfNetwork * network,
										const gchar * website, const gchar * lang);

G_CONST_RETURN gchar *gnomechat_rdf_network_get_encoding (GnomechatRdfNetwork * network);
void gnomechat_rdf_network_set_encoding (GnomechatRdfNetwork * network, const gchar * encoding);

GSList *gnomechat_rdf_network_get_servers (GnomechatRdfNetwork * network);
void gnomechat_rdf_network_append_server (GnomechatRdfNetwork * network,
										  GnomechatRdfServer * server);
void gnomechat_rdf_network_remove_server (GnomechatRdfNetwork * network,
										  GnomechatRdfServer * server);

GSList *gnomechat_rdf_network_get_channels (GnomechatRdfNetwork * network);
void gnomechat_rdf_network_append_channel (GnomechatRdfNetwork * network, const gchar * channel);
void gnomechat_rdf_network_remove_channel (GnomechatRdfNetwork * network, const gchar * channel);

#endif /* __GNOMECHAT_RDF_NETWORK_H__ */
