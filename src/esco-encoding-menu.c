/*
 *  GnomeChat: src/esco-encoding-menu.c
 *
 *  Copyright (c) 2002 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "gnomechat.h"
#include "esco-encoding-menu.h"

#include "esco-encoding.h"
#include "esco-encoding-selection.h"

#include <gtk/gtkradiomenuitem.h>
#include <gtk/gtkseparatormenuitem.h>


#define ENCODING_OBJ_DATA "encoding-data"


enum
{
	PROP_0,
	PROP_ENCODING,
	PROP_TITLE,
	PROP_MODAL,
	PROP_DIALOG
};

enum
{
	DIALOG_OPENED,
	HELP_CLICKED,
	ENCODING_CHANGED,
	LAST_SIGNAL
};


struct _EscoEncodingMenuPrivate
{
	/* Object Properties */
	/* The ptr can be modified, but not the actual data */
	EscoEncoding *current_encoding;
	/* Regular Properties */
	gchar *title;
	gboolean modal:1;
	/* Selection Dialog */
	GtkWidget *esd;

	/* Menu Items */
	GSList *items;
	GtkWidget *other;
	GtkWidget *separator;
	GtkWidget *old;

	/* Encodings covered by current menu items */
	GHashTable *menu_encodings;

	/* Temporary var used to set current_encoding */
	gchar *encoding;
};


/* Function Prototype, needed (ugh) */
static void esd_response_cb (GtkWidget * esd,
							 gint response,
							 EscoEncodingMenu * menu);


/* Global Vars */
static gpointer parent_class = NULL;
static gint signals[LAST_SIGNAL] = { 0 };


/* Utility Functions */
static void
menuitem_activate_cb (GtkWidget * menuitem,
					  EscoEncodingMenu * menu)
{
	if (menuitem == menu->_priv->other)
	{
		if (menu->_priv->esd)
		{
			gtk_window_present (GTK_WINDOW (menu->_priv->esd));
		}
		else
		{
			menu->_priv->esd = esco_encoding_selection_dialog_new (menu->_priv->title);
			gtk_window_set_modal (GTK_WINDOW (menu->_priv->esd), menu->_priv->modal);

			g_object_add_weak_pointer (G_OBJECT (menu->_priv->esd),
									   (gpointer) & (menu->_priv->esd));
			g_signal_connect (menu->_priv->esd, "response",
							  G_CALLBACK (esd_response_cb), menu);
			g_signal_emit (G_OBJECT (menu), signals[DIALOG_OPENED], 0);

			gtk_window_present (GTK_WINDOW (menu->_priv->esd));

			g_object_notify (G_OBJECT (menu), "dialog");
		}
	}
	else
	{
		/* Save this item, for when we do an "Other..." & the dialog is cancelled */
		menu->_priv->old = menuitem;
		menu->_priv->current_encoding = g_object_get_qdata (G_OBJECT (menuitem),
															g_quark_from_static_string
															(ENCODING_OBJ_DATA));
	}
}


static GtkWidget *
create_menuitem_from_encoding (EscoEncodingMenu * menu,
							   const EscoEncoding * const encoding)
{
	GtkWidget *menuitem;
	gint i;

	menuitem = gtk_radio_menu_item_new_with_label (menu->_priv->items, encoding->ui_name);
	menu->_priv->items = gtk_radio_menu_item_get_group (GTK_RADIO_MENU_ITEM (menuitem));
	g_object_set_qdata (G_OBJECT (menuitem),
						g_quark_from_static_string (ENCODING_OBJ_DATA),
						(gpointer) encoding);
	g_signal_connect (menuitem, "activate", G_CALLBACK (menuitem_activate_cb), menu);
	gtk_widget_show (menuitem);

	for (i = 0; encoding->aliases[i] != NULL; i++)
	{
		g_hash_table_insert (menu->_priv->menu_encodings, (gpointer) encoding->aliases[i],
							 menuitem);
	}

	return menuitem;
}


static void
find_and_activate_encoding (EscoEncodingMenu * menu,
							const gchar * encoding)
{
	GtkWidget *current;
	gint pos;

	current = g_hash_table_lookup (menu->_priv->menu_encodings, encoding);

	if (current == NULL)
	{
		pos = g_list_index (GTK_MENU_SHELL (menu)->children, menu->_priv->other);

		gtk_widget_show (menu->_priv->separator);

		menu->_priv->current_encoding =
			ESCO_ENCODING (esco_encoding_get_encoding_by_alias (encoding));
		current = create_menuitem_from_encoding (menu, menu->_priv->current_encoding);
		gtk_menu_shell_insert (GTK_MENU_SHELL (menu), current, pos);
		gtk_widget_show (current);
	}
	else
	{
		menu->_priv->current_encoding =
			g_object_get_qdata (G_OBJECT (current),
								g_quark_from_static_string (ENCODING_OBJ_DATA));
		pos = g_list_index (GTK_MENU_SHELL (menu)->children, current);
	}

	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (current), TRUE);
	gtk_menu_set_active (GTK_MENU (menu), pos);
	g_signal_emit_by_name (menu, "selection-done");
}


static void
esd_response_cb (GtkWidget * esd,
				 gint response,
				 EscoEncodingMenu * menu)
{
	switch (response)
	{
	case GTK_RESPONSE_HELP:
		g_signal_emit (menu, signals[HELP_CLICKED], 0);
		return;
		break;

	case GTK_RESPONSE_OK:
		find_and_activate_encoding (menu,
									esco_encoding_selection_dialog_get_encoding
									(ESCO_ENCODING_SELECTION_DIALOG (menu->_priv->esd)));
		break;
	default:
		gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (menu->_priv->old), TRUE);
		g_signal_emit_by_name (menu, "selection-done");
		break;
	}

	gtk_widget_destroy (esd);
	g_object_notify (G_OBJECT (menu), "dialog");
}


/* GtkMenuShell Callbacks */
static void
esco_encoding_menu_selection_done (GtkMenuShell *menu)
{
	if (GTK_MENU_SHELL_CLASS (parent_class)->selection_done)
		(*GTK_MENU_SHELL_CLASS (parent_class)->selection_done) (menu);

	g_signal_emit (menu, signals[ENCODING_CHANGED], 0);
}


/* Object Callbacks */
static void
esco_encoding_menu_get_property (GObject * object,
								 guint param_id,
								 GValue * value,
								 GParamSpec * pspec)
{
	EscoEncodingMenu *menu = ESCO_ENCODING_MENU (object);

	switch (param_id)
	{
	case PROP_ENCODING:
		g_value_set_string (value, menu->_priv->current_encoding->aliases[0]);
		break;
	case PROP_TITLE:
		g_value_set_string (value, menu->_priv->title);
		break;
	case PROP_MODAL:
		g_value_set_boolean (value, menu->_priv->modal);
		break;
	case PROP_DIALOG:
		g_value_set_object (value, menu->_priv->esd);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}


static void
esco_encoding_menu_set_property (GObject * object,
								 guint param_id,
								 const GValue * value,
								 GParamSpec * pspec)
{
	EscoEncodingMenu *menu = ESCO_ENCODING_MENU (object);

	switch (param_id)
	{
	case PROP_ENCODING:
		esco_encoding_menu_set_encoding (menu, g_value_get_string (value));
		break;
	case PROP_TITLE:
		esco_encoding_menu_set_title (menu, g_value_get_string (value));
		break;
	case PROP_MODAL:
		esco_encoding_menu_set_modal (menu, g_value_get_boolean (value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}


static GObject *
esco_encoding_menu_constructor (GType type,
								guint n_construct_params,
								GObjectConstructParam * construct_params)
{

	GObject *object;
	EscoEncodingMenu *menu;
	GtkMenuShell *shell;
	GtkWidget *menuitem;
	GSList *enc_list;

	object = (*G_OBJECT_CLASS (parent_class)->constructor) (type, n_construct_params,
															construct_params);

	menu = ESCO_ENCODING_MENU (object);
	shell = GTK_MENU_SHELL (object);

	for (enc_list = esco_encoding_get_recommended_encodings ();
		 enc_list != NULL; enc_list = g_slist_remove (enc_list, enc_list->data))
	{
		menuitem = create_menuitem_from_encoding (menu, ESCO_ENCODING (enc_list->data));
		gtk_menu_shell_append (shell, menuitem);
	}

	menu->_priv->separator = gtk_separator_menu_item_new ();
	gtk_menu_shell_append (shell, menu->_priv->separator);

	if (GTK_MENU_SHELL (menu)->children != NULL)
		gtk_widget_show (menu->_priv->separator);

	menu->_priv->other = gtk_menu_item_new_with_label (_("Other..."));
	g_signal_connect (menu->_priv->other, "activate", G_CALLBACK (menuitem_activate_cb),
					  menu);
	gtk_menu_shell_append (shell, menu->_priv->other);
	gtk_widget_show (menu->_priv->other);

	return object;
}


static void
esco_encoding_menu_dispose (GObject * object)
{
	EscoEncodingMenu *menu = ESCO_ENCODING_MENU (object);

	if (menu->_priv->esd != NULL)
		gtk_widget_destroy (menu->_priv->esd);

	if (G_OBJECT_CLASS (parent_class)->dispose)
		(*G_OBJECT_CLASS (parent_class)->dispose) (object);
}


static void
esco_encoding_menu_finalize (GObject * object)
{
	EscoEncodingMenu *menu = ESCO_ENCODING_MENU (object);

	g_free (menu->_priv->title);
	g_free (menu->_priv->encoding);
	g_hash_table_destroy (menu->_priv->menu_encodings);

	g_free (menu->_priv);

	if (G_OBJECT_CLASS (parent_class)->finalize)
		(*G_OBJECT_CLASS (parent_class)->finalize) (object);
}


/* GType Callbacks */
static void
esco_encoding_menu_class_init (EscoEncodingMenuClass * class)
{
	GObjectClass *object_class = G_OBJECT_CLASS (class);
	GtkMenuShellClass *menushell_class = GTK_MENU_SHELL_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	object_class->set_property = esco_encoding_menu_set_property;
	object_class->get_property = esco_encoding_menu_get_property;
	object_class->constructor = esco_encoding_menu_constructor;
	object_class->dispose = esco_encoding_menu_dispose;
	object_class->finalize = esco_encoding_menu_finalize;

	menushell_class->selection_done = esco_encoding_menu_selection_done;

	g_object_class_install_property (object_class, PROP_ENCODING,
									 g_param_spec_string ("encoding",
														  _("Character Encoding"),
														  _("The currently selected "
															"encoding."),
														  "UTF-8",
														  (G_PARAM_CONSTRUCT |
														   G_PARAM_READWRITE)));
	g_object_class_install_property (object_class, PROP_TITLE,
									 g_param_spec_string ("title",
														  _("Browse Dialog Title"),
														  _("The title to use for the "
															"browse dialog."),
														  _("Select A Character "
															"Encoding"),
														  (G_PARAM_CONSTRUCT |
														   G_PARAM_READWRITE)));
	g_object_class_install_property (object_class, PROP_MODAL,
									 g_param_spec_boolean ("modal",
														   _("Browse Dialog Modal"),
														   _("Whether or not the browse "
															 "dialog will be modal."),
														   FALSE,
														   (G_PARAM_CONSTRUCT |
															G_PARAM_READWRITE)));
	g_object_class_install_property (object_class, PROP_DIALOG,
									 g_param_spec_object ("dialog", _("Browse Dialog"),
														  _("The browse dialog itself."),
														  GTK_TYPE_WIDGET,
														  G_PARAM_READABLE));

	signals[DIALOG_OPENED] =
		g_signal_new ("dialog-opened", G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_LAST,
					  G_STRUCT_OFFSET (EscoEncodingMenuClass, dialog_opened), NULL, NULL,
					  g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
	signals[HELP_CLICKED] =
		g_signal_new ("help-clicked", G_TYPE_FROM_CLASS (object_class), G_SIGNAL_RUN_LAST,
					  G_STRUCT_OFFSET (EscoEncodingMenuClass, help_clicked), NULL, NULL,
					  g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
	signals[ENCODING_CHANGED] =
		g_signal_new ("encoding-changed", G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_LAST, G_STRUCT_OFFSET (EscoEncodingMenuClass, changed),
					  NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
}


static void
esco_encoding_menu_instance_init (EscoEncodingMenu * menu)
{
	menu->_priv = g_new0 (EscoEncodingMenuPrivate, 1);

	menu->_priv->menu_encodings = g_hash_table_new (esco_encoding_alias_hash,
													esco_encoding_alias_equal);

	menu->_priv->items = NULL;
}


/* PUBLIC API */
GType
esco_encoding_menu_get_type (void)
{
	static GType type = 0;

	if (!type)

	{
		static const GTypeInfo info = {
			sizeof (EscoEncodingMenuClass),
			NULL,				/* base_init */
			NULL,				/* base_finalize */
			(GClassInitFunc) esco_encoding_menu_class_init,
			NULL,				/* class_finalize */
			NULL,				/* class_data */
			sizeof (EscoEncodingMenu),
			0,					/* n_preallocs */
			(GInstanceInitFunc) esco_encoding_menu_instance_init,
		};

		type = g_type_register_static (GTK_TYPE_MENU, "EscoEncodingMenu", &info, 0);
	}

	return type;
}


GtkWidget *
esco_encoding_menu_new (const gchar * title)
{
	return GTK_WIDGET (g_object_new (ESCO_TYPE_ENCODING_MENU, "title", title, NULL));
}


G_CONST_RETURN gchar *
esco_encoding_menu_get_encoding (EscoEncodingMenu * menu)
{
	g_return_val_if_fail (menu != NULL, NULL);
	g_return_val_if_fail (ESCO_IS_ENCODING_MENU (menu), NULL);

	return menu->_priv->current_encoding->aliases[0];
}


void
esco_encoding_menu_set_encoding (EscoEncodingMenu * menu,
								 const gchar * encoding)
{
	gchar *real_encoding;

	g_return_if_fail (menu != NULL);
	g_return_if_fail (ESCO_IS_ENCODING_MENU (menu));

	if (encoding == NULL)
	{
		real_encoding = g_strdup ("UTF-8");
	}
	else
	{
		g_return_if_fail (esco_encoding_is_valid (encoding));
		real_encoding = g_strdup (encoding);
	}

	if (menu->_priv->encoding != NULL)
		g_free (menu->_priv->encoding);

	menu->_priv->encoding = real_encoding;

	/* We're already constructed */
	if (menu->_priv->other != NULL)
		find_and_activate_encoding (menu, real_encoding);

	/* If the dialog is open */
	if (menu->_priv->esd != NULL)
	{
		esco_encoding_selection_dialog_set_encoding (ESCO_ENCODING_SELECTION_DIALOG
													 (menu->_priv->esd), real_encoding);
	}

	g_object_notify (G_OBJECT (menu), "encoding");
}


G_CONST_RETURN gchar *
esco_encoding_menu_get_title (EscoEncodingMenu * menu)
{
	g_return_val_if_fail (menu != NULL, NULL);
	g_return_val_if_fail (ESCO_IS_ENCODING_MENU (menu), NULL);

	return menu->_priv->title;
}


void
esco_encoding_menu_set_title (EscoEncodingMenu * menu,
							  const gchar * title)
{
	g_return_if_fail (menu != NULL);
	g_return_if_fail (ESCO_IS_ENCODING_MENU (menu));

	g_free (menu->_priv->title);

	menu->_priv->title = g_strdup (title);
	if (menu->_priv->esd != NULL)
	{
		gtk_window_set_title (GTK_WINDOW (menu->_priv->esd), menu->_priv->title);
	}

	g_object_notify (G_OBJECT (menu), "title");
}


gboolean
esco_encoding_menu_get_modal (EscoEncodingMenu * menu)
{
	g_return_val_if_fail (menu != NULL, FALSE);
	g_return_val_if_fail (ESCO_IS_ENCODING_MENU (menu), FALSE);

	return menu->_priv->modal;
}


void
esco_encoding_menu_set_modal (EscoEncodingMenu * menu,
							  gboolean modal)
{
	g_return_if_fail (menu != NULL);
	g_return_if_fail (ESCO_IS_ENCODING_MENU (menu));

	menu->_priv->modal = modal;

	if (menu->_priv->esd != NULL)
	{
		gtk_window_set_modal (GTK_WINDOW (menu->_priv->esd), modal);
	}

	g_object_notify (G_OBJECT (menu), "modal");
}


GtkWidget *
esco_encoding_menu_get_dialog (EscoEncodingMenu * menu)
{
	g_return_val_if_fail (menu != NULL, NULL);
	g_return_val_if_fail (ESCO_IS_ENCODING_MENU (menu), NULL);

	return menu->_priv->esd;
}
