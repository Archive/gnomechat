/*
 *  GnomeChat: src/menus.c
 *
 *  Copyright (c) 2002 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "gnomechat.h"
#include "menus.h"

#include "about.h"
#include "esco-chat-box.h"
#include "esco-chats-menu.h"
#include "esco-encoding-menu.h"
#include "esco-userlist-view.h"
#include "esco-viewer.h"
#include "esco-window.h"
#include "connect-ui.h"
#include "network-ui.h"
#include "pixbufs.h"
#include "prefs.h"
#include "prefs-ui.h"
#include "stock-items.h"
#include "utils.h"
#include "windows.h"

#include <gdk/gdkkeysyms.h>

#include <gtk/gtkalignment.h>
#include <gtk/gtkcheckbutton.h>
#include <gtk/gtkdialog.h>
#include <gtk/gtkentry.h>
#include <gtk/gtkimage.h>
#include <gtk/gtklabel.h>
#include <gtk/gtkmenuitem.h>
#include <gtk/gtkstock.h>

#include <libgnomeui/gnome-entry.h>

/* FIXME: HACK! */
#undef D_
#include <libgnomeui/gnome-app-helper.h>


typedef struct
{
	GtkWidget *win;
	GtkWidget *entry;

	GtkWidget *target;

	gboolean case_insensitive:1;
}
FindDialog;

enum
{
	RESPONSE_PREV,
	RESPONSE_NEXT
};


/* *************** *
 *  UserList Menu  *
 * *************** */

static void
userlist_query_cb (GtkWidget * menuitem,
				   EscoUserlistView * view)
{
	esco_userlist_view_query_selected (view);
}


static void
userlist_props_cb (GtkWidget * menuitem,
				   EscoUserlistView * view)
{
	esco_userlist_view_info_on_selected (view);
}


static GnomeUIInfo userlist_popup_menu[] = {
	GNOMEUIINFO_ITEM_STOCK (N_("_Chat Privately"),
							N_("Chat privately with this user"), userlist_query_cb,
							GNOMECHAT_STOCK_QUERY),
	{
	 GNOME_APP_UI_ITEM,
	 N_("_Send File..."), N_("Send a file to this user"),
	 NULL, NULL, NULL,
	 GNOME_APP_PIXMAP_STOCK, GTK_STOCK_SAVE,
	 0, 0, NULL},
	GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_MENU_PROPERTIES_ITEM (userlist_props_cb, NULL),
	GNOMEUIINFO_END
};


/* *********** *
 *  Main Menu  *
 * *********** */

/* File Menu */
static void
file_new_chat_cb (GtkWidget * menuitem,
				  EscoWindow * win)
{
	esco_window_add_new_chat (win);
}


static void
file_new_window_cb (GtkWidget * menuitem,
					gpointer data)
{
	windows_open_new_window ();
}


static void
file_connect_cb (GtkWidget * menuitem,
				 GtkWindow * win)
{
	connect_ui_open_dialog (win);
}


static void
file_new_connect_cb (GtkWidget * menuitem,
					 GtkWindow * win)
{
	network_ui_open_connect_dialog (win);
}


static void
file_disconnect_cb (GtkWidget * menuitem,
					GtkWindow * win)
{
	connect_ui_open_disconnect_dialog (win);
}


static void
file_close_cb (GtkWidget * menuitem,
			   EscoWindow * win)
{
	esco_window_remove_current_chat (win);
}


static GnomeUIInfo mainwin_file_menu[] = {
	GNOMEUIINFO_MENU_NEW_ITEM (N_("_New Chat"), N_("Open a new chat"),
							   file_new_chat_cb, NULL),
	{GNOME_APP_UI_ITEM, N_("New _Window"), N_("Open a new window"), file_new_window_cb,
	 NULL, NULL, GNOME_APP_PIXMAP_NONE, NULL, 'n', (GDK_CONTROL_MASK | GDK_SHIFT_MASK),
	 NULL},
	GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_ITEM_STOCK (N_("Connec_t..."), N_("Connect to a server"), file_connect_cb,
							GNOMECHAT_STOCK_CONNECT),
	GNOMEUIINFO_ITEM_STOCK (N_("New Connec_t..."), N_("Connect to a server"),
							file_new_connect_cb, GNOMECHAT_STOCK_CONNECT),
	GNOMEUIINFO_ITEM_STOCK (N_("_Disconnect..."),
							N_("Disconnect from a server"), file_disconnect_cb,
							GNOMECHAT_STOCK_DISCONNECT),
	GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_MENU_SAVE_ITEM (NULL, NULL),
	GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_MENU_CLOSE_ITEM (file_close_cb, NULL),
	GNOMEUIINFO_MENU_QUIT_ITEM (gnomechat_quit, NULL),
	GNOMEUIINFO_END
};


/* Edit Menu */
static void
edit_cut_cb (GtkWidget * menuitem,
			 EscoWindow * win)
{
	g_signal_emit_by_name (ESCO_CHAT_BOX (win->current)->main_text, "cut-clipboard");
}


static void
edit_copy_cb (GtkWidget * menuitem,
			  EscoWindow * win)
{
	GtkWidget *focus = gtk_window_get_focus (GTK_WINDOW (win));

	if (GTK_IS_EDITABLE (focus))
	{
		gtk_editable_copy_clipboard ((GtkEditable *) focus);
	}
	else if (GTK_IS_TEXT_VIEW (focus))
	{
		g_signal_emit_by_name (focus, "copy-clipboard");
	}
	else
	{
		g_signal_emit_by_name (ESCO_CHAT_BOX (win->current)->main_text, "copy-clipboard");
	}
}


static void
edit_paste_cb (GtkWidget * menuitem,
			   EscoWindow * win)
{
	GtkWidget *focus = gtk_window_get_focus (GTK_WINDOW (win));

	if (GTK_IS_EDITABLE (focus))
	{
		gtk_editable_paste_clipboard ((GtkEditable *) focus);
	}
	else if (GTK_IS_TEXT_VIEW (focus)
			 && gtk_text_view_get_editable ((GtkTextView *) focus))
	{
		g_signal_emit_by_name (focus, "paste-clipboard");
	}
	else
	{
		g_signal_emit_by_name (ESCO_CHAT_BOX (win->current)->main_text,
							   "paste-clipboard");
	}
}


static void
edit_clear_cb (GtkWidget * menuitem,
			   EscoWindow * win)
{
	if (win && win->current)
	{
		esco_chat_box_clear_viewer (ESCO_CHAT_BOX (win->current));
	}
}


static void
find_dialog_response_cb (GtkWidget * win,
						 gint response,
						 FindDialog * dialog)
{
	switch (response)
	{
	case GTK_RESPONSE_OK:
		{
			gchar *search_str = gtk_editable_get_chars (GTK_EDITABLE (dialog->entry),
														0, -1);

			esco_viewer_find (ESCO_VIEWER (dialog->target), search_str,
							  prefs_get_case_insensitive_find ());

			g_free (search_str);
		}
		break;

	default:
		gtk_widget_destroy (win);
		g_free (dialog);
		break;
	}
}


static void
find_check_toggled_cb (GtkToggleButton * btn,
					   FindDialog * dialog)
{
	prefs_set_case_insensitive_find (gtk_toggle_button_get_active (btn));
}


static FindDialog *
create_find_dialog (GtkWindow * win,
					GtkWidget * target)
{
	FindDialog *dialog = g_new0 (FindDialog, 1);
	GtkWidget *hbox,
	 *vbox,
	 *align,
	 *image,
	 *label,
	 *check;

	dialog->target = target;
	dialog->win = gtk_dialog_new_with_buttons (_("Find Text - IRC Chat"), win,
											   (GTK_DIALOG_DESTROY_WITH_PARENT |
												GTK_DIALOG_NO_SEPARATOR),
											   GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE,
											   GTK_STOCK_FIND, GTK_RESPONSE_OK, NULL);
	g_signal_connect (dialog->win, "response", G_CALLBACK (find_dialog_response_cb),
					  dialog);
	util_set_window_icon_from_stock (GTK_WINDOW (dialog->win), GTK_STOCK_FIND);
	gtk_window_set_role (GTK_WINDOW (dialog->win), "Find");
	gtk_dialog_set_default_response (GTK_DIALOG (dialog->win), GTK_RESPONSE_OK);

	/* Dialog Contents */
	hbox = gtk_hbox_new (FALSE, 12);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (dialog->win)->vbox), hbox);
	gtk_container_set_border_width (GTK_CONTAINER (hbox), 12);
	gtk_widget_show (hbox);

	image = gtk_image_new_from_stock (GTK_STOCK_FIND, GTK_ICON_SIZE_DIALOG);
	gtk_box_pack_start (GTK_BOX (hbox), image, FALSE, FALSE, 0);
	gtk_widget_show (image);

	/* The Useful Side */
	align = gtk_alignment_new (0.0, 0.5, 1.0, 0.0);
	gtk_container_add (GTK_CONTAINER (hbox), align);
	gtk_widget_show (align);

	vbox = gtk_vbox_new (FALSE, 3);
	gtk_container_add (GTK_CONTAINER (align), vbox);
	gtk_widget_show (vbox);

	/* Entry Row */
	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new (_("Text to find: "));
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	dialog->entry = gnome_entry_new ("find-history");
	gtk_entry_set_activates_default (GTK_ENTRY
									 (gnome_entry_gtk_entry
									  (GNOME_ENTRY (dialog->entry))), TRUE);
	gtk_container_add (GTK_CONTAINER (hbox), dialog->entry);
	gtk_widget_show (dialog->entry);

	/* Checkbuttons -- Disabled for now, until I write case-insensitive search code */
	/* Padding */
	hbox = gtk_hbox_new (FALSE, 0);
	gtk_container_add (GTK_CONTAINER (vbox), hbox);
	// gtk_widget_show (hbox);

	/* Contents */
	vbox = gtk_vbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (hbox), vbox, FALSE, FALSE, 24);
	gtk_widget_show (vbox);

	check = gtk_check_button_new_with_label (_("Ignore upper and lower case"));
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check),
								  prefs_get_case_insensitive_find ());
	g_signal_connect (check, "toggled", G_CALLBACK (find_check_toggled_cb), dialog);
	gtk_box_pack_start (GTK_BOX (vbox), check, FALSE, FALSE, 0);
	gtk_widget_show (check);

	return dialog;
}


static void
edit_find_cb (GtkWidget * menuitem,
			  EscoWindow * win)
{
	static FindDialog *dialog = NULL;

	if (dialog == NULL)
	{
		dialog = create_find_dialog (GTK_WINDOW (win),
									 ESCO_CHAT_BOX (win->current)->viewer);
		g_object_add_weak_pointer (G_OBJECT (dialog->win), (gpointer) & dialog);
	}

	gtk_window_present (GTK_WINDOW (dialog->win));
}


static void
edit_find_again_cb (GtkWidget * menuitem,
					EscoWindow * win)
{
	GtkWidget *focus = gtk_window_get_focus (GTK_WINDOW (win));

	if (ESCO_IS_USERLIST_VIEW (focus))
	{
	}
	else
	{
	}
}


static void
edit_preferences_cb (GtkWidget * menuitem,
					 GtkWindow * win)
{
	prefs_ui_open_prefs_dialog (win);
}


static GnomeUIInfo mainwin_edit_menu[] = {
	GNOMEUIINFO_MENU_CUT_ITEM (edit_cut_cb, NULL),
	GNOMEUIINFO_MENU_COPY_ITEM (edit_copy_cb, NULL),
	GNOMEUIINFO_MENU_PASTE_ITEM (edit_paste_cb, NULL),
	GNOMEUIINFO_MENU_CLEAR_ITEM (edit_clear_cb, NULL),
	GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_MENU_FIND_ITEM (edit_find_cb, NULL),
	GNOMEUIINFO_MENU_FIND_AGAIN_ITEM (edit_find_again_cb, NULL),
	GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_MENU_PREFERENCES_ITEM (edit_preferences_cb, NULL),
	GNOMEUIINFO_END
};


/* View Menu */

/* Layout Types */
static void
view_as_tabs_cb (GtkWidget * menuitem,
				 EscoWindow * win)
{
	esco_window_set_layout (win, ESCO_WINDOW_LAYOUT_TABS);
}


static void
view_as_list_cb (GtkWidget * menuitem,
				 EscoWindow * win)
{
	esco_window_set_layout (win, ESCO_WINDOW_LAYOUT_TREE);
}


static void
view_as_windows_cb (GtkWidget * menuitem,
					EscoWindow * win)
{
	esco_window_set_layout (win, ESCO_WINDOW_LAYOUT_WINDOW);
}


static GnomeUIInfo mainwin_layout_type_list[] = {
	GNOMEUIINFO_RADIOITEM (N_("As _Tabs"),
						   N_("Show chats in the current window as tabs"),
						   view_as_tabs_cb, NULL),
	GNOMEUIINFO_RADIOITEM (N_("As a _List"),
						   N_("Show chats in the current window in a list"),
						   view_as_list_cb, NULL),
	GNOMEUIINFO_RADIOITEM (N_("As _Windows"),
						   N_("Show chats in the current window as individual "
							  "windows"),
						   view_as_windows_cb, NULL),
	GNOMEUIINFO_END
};


/* Rest of View Menu */
static void
view_user_props_cb (GtkWidget * menuitem,
					EscoWindow * win)
{
	esco_userlist_view_info_on_selected (ESCO_USERLIST_VIEW
										 (ESCO_CHAT_BOX (win->current)->userlist_view));
}


static void
view_chanserv_props_cb (GtkWidget * menuitem,
						EscoWindow * win)
{
	g_signal_emit_by_name (ESCO_CHAT_BOX (win->current)->topic_props_button, "clicked");
}


static GnomeUIInfo mainwin_view_menu[] = {
	GNOMEUIINFO_RADIOLIST (mainwin_layout_type_list),
	GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_ITEM_STOCK (N_("Older _Messages..."),
							N_("View older messages recieved on this item"),
							NULL, GTK_STOCK_INDEX),
	GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_ITEM_STOCK (N_("_Server Properties"),
							N_("View information about this server"),
							view_chanserv_props_cb,
							GNOMECHAT_STOCK_SERVER_PROPERTIES),
	GNOMEUIINFO_ITEM_STOCK (N_("_User Properties"),
							N_("View information about this user"), view_user_props_cb,
							GNOMECHAT_STOCK_USER_PROPERTIES),
	GNOMEUIINFO_ITEM_STOCK (N_("Channel _Properties"),
							N_("View information about this channel"),
							view_chanserv_props_cb,
							GNOMECHAT_STOCK_CHANNEL_PROPERTIES),
	GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_ITEM_STOCK (N_("Character C_oding"),
							N_("Change the character encoding this channel uses"), NULL,
							GNOMECHAT_STOCK_ENCODING),
	GNOMEUIINFO_END
};


/* Help Menu */
static void
help_about_cb (GtkWidget * menuitem,
			   GtkWindow * win)
{
	about_show_about_window (win);
}


static GnomeUIInfo mainwin_help_menu[] = {
	{
	 GNOME_APP_UI_ITEM,
	 N_("_Documentation..."), N_("Read the GnomeChat documentation"),
	 NULL, NULL, NULL,
	 GNOME_APP_PIXMAP_STOCK, GTK_STOCK_HELP,
	 GDK_F1, 0, NULL},
	GNOMEUIINFO_MENU_ABOUT_ITEM (help_about_cb, NULL),
	GNOMEUIINFO_END
};


/* All Menus Together */
static GnomeUIInfo mainwin_menubar[] = {
	{
	 GNOME_APP_UI_SUBTREE,
	 N_("_IRC Chat"), N_("Open chats with servers, channels, or users"),
	 mainwin_file_menu, NULL, NULL,
	 GNOME_APP_PIXMAP_NONE, NULL,
	 0, 0, NULL},
	GNOMEUIINFO_MENU_EDIT_TREE (mainwin_edit_menu),
	GNOMEUIINFO_MENU_VIEW_TREE (mainwin_view_menu),
	{
	 GNOME_APP_UI_ITEM,
	 N_("_Chats"), N_("Open chats with servers, channels, or users"),
	 NULL, NULL, NULL,
	 GNOME_APP_PIXMAP_NONE, NULL,
	 0, 0, NULL},
	GNOMEUIINFO_MENU_HELP_TREE (mainwin_help_menu),
	GNOMEUIINFO_END
};


/* ************ *
 *  PUBLIC API  *
 * ************ */

void
menu_set_main_window_menubar (GtkWidget * widget)
{
	EscoWindow *win = ESCO_WINDOW (widget);
	GtkWidget *chats_menu;

	gnome_app_create_menus_with_data (GNOME_APP (widget), mainwin_menubar, widget);

	/* Create & attach the chats menu */
	chats_menu = esco_chats_menu_new ();
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (mainwin_menubar[3].widget), chats_menu);

	/* Set dynamic items */
	/* Create & attach the encoding menu */
	win->menus->encoding_menu =
		esco_encoding_menu_new (_("Select An Encoding - IRC Chat"));
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (mainwin_view_menu[8].widget),
							   win->menus->encoding_menu);

	win->menus->disconnect = mainwin_file_menu[4].widget;

	win->menus->cut = mainwin_edit_menu[0].widget;
	win->menus->copy = mainwin_edit_menu[1].widget;
	win->menus->paste = mainwin_edit_menu[2].widget;

	win->menus->layouts[0] = mainwin_layout_type_list[0].widget;
	win->menus->layouts[1] = mainwin_layout_type_list[1].widget;
	win->menus->layouts[2] = mainwin_layout_type_list[2].widget;

	win->menus->server_props = mainwin_view_menu[4].widget;
	win->menus->user_props = mainwin_view_menu[5].widget;
	win->menus->channel_props = mainwin_view_menu[6].widget;
}


void
menu_fill_userlist_popup (GtkWidget * widget,
						  gpointer data)
{
	gnome_app_fill_menu_with_data (GTK_MENU_SHELL (widget),
								   userlist_popup_menu, NULL, FALSE, 0, data);
}
