/*
 *  GnomeChat: src/gnomechat-transfer.h
 *
 *  Copyright (c) 2003 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.01234567890
 */

/*
 * GInterface common to all file transfers.
 */


#ifndef __GNOMECHAT_TRANSFER_H__
#define __GNOMECHAT_TRANSFER_H__


#include <glib-object.h>
#include <libgnomevfs/gnome-vfs-file-size.h>
#include <libgnomevfs/gnome-vfs-result.h>
#include <libgnomevfs/gnome-vfs-uri.h>


#define GNOMECHAT_TYPE_TRANSFER				(gnomechat_transfer_get_type ())
#define GNOMECHAT_TRANSFER(obj)				(G_TYPE_CHECK_INSTANCE_CAST ((obj), GNOMECHAT_TYPE_TRANSFER, GnomechatTransfer))
#define GNOMECHAT_IS_TRANSFER(obj)			(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GNOMECHAT_TYPE_TRANSFER))
#define GNOMECHAT_TRANSFER_GET_IFACE(obj)	(G_TYPE_INSTANCE_GET_INTERFACE ((obj), GNOMECHAT_TYPE_TRANSFER, GnomechatTransferIface))


typedef enum					/* < prefix=GNOMECHAT_TRANSFER_STATUS > */
{
	GNOMECHAT_TRANSFER_STATUS_READY,
	GNOMECHAT_TRANSFER_STATUS_CHECKING,
	GNOMECHAT_TRANSFER_STATUS_TRANSFERRING,
	GNOMECHAT_TRANSFER_STATUS_DONE,
	GNOMECHAT_TRANSFER_STATUS_ERROR,
	GNOMECHAT_TRANSFER_STATUS_INVALID,
	GNOMECHAT_TRANSFER_STATUS_LAST
}
GnomechatTransferStatus;

typedef enum					/* < flags,prefix=GNOMECHAT_TRANSFER > */
{
	GNOMECHAT_TRANSFER_INVALID = 0,
	GNOMECHAT_TRANSFER_DOWNLOAD = 1 << 0,
	GNOMECHAT_TRANSFER_UPLOAD = 1 << 1,
	GNOMECHAT_TRANSFER_BOTH = 3,
	GNOMECHAT_TRANSFER_MULTI = 1 << 2
}
GnomechatTransferType;


typedef struct _GnomechatTransfer GnomechatTransfer;
typedef struct _GnomechatTransferIface GnomechatTransferIface;

/* VTable Prototypes */
typedef void (*GnomechatTransferFunc) (GnomechatTransfer *transfer);
typedef GnomechatTransferType (*GnomechatTransferGetTypeFunc) (GnomechatTransfer * transfer);
typedef GnomechatTransferStatus (*GnomechatTransferGetStatusFunc) (GnomechatTransfer * transfer);
typedef GnomeVFSResult (*GnomechatTransferGetResultFunc) (GnomechatTransfer * transfer);

typedef GnomeVFSURI *(*GnomechatTransferGetURIFunc) (GnomechatTransfer * transfer);
typedef void (*GnomechatTransferSetURIFunc) (GnomechatTransfer * transfer,
											 GnomeVFSURI * uri);

typedef GnomeVFSFileSize (*GnomechatTransferGetSizeFunc) (GnomechatTransfer * transfer);
typedef gulong (*GnomechatTransferGetULongFunc) (GnomechatTransfer * transfer);


struct _GnomechatTransferIface
{
	GTypeInterface g_iface;

	/* Signals */
	void (*status_changed) (GnomechatTransfer * transfer,
							GnomechatTransferStatus status);
	void (*progress) (GnomechatTransfer * transfer,
					  GnomeVFSFileSize bytes_copied);
	void (*cancelled) (GnomechatTransfer * transfer);

	/* GInterface VTable */
	GnomechatTransferGetTypeFunc get_transfer_type;
	GnomechatTransferGetStatusFunc get_status;
	GnomechatTransferGetResultFunc get_error;

	GnomechatTransferGetURIFunc get_from_uri;
	GnomechatTransferSetURIFunc set_from_uri;

	GnomechatTransferGetURIFunc get_to_uri;
	GnomechatTransferSetURIFunc set_to_uri;

	GnomechatTransferGetSizeFunc get_file_size;
	GnomechatTransferGetSizeFunc get_bytes_copied;

	GnomechatTransferFunc start_transfer;
	GnomechatTransferFunc cancel_transfer;

	/* For multi-transfer */
	GnomechatTransferGetULongFunc get_n_files;
	GnomechatTransferGetULongFunc get_file_index;
};


GType gnomechat_transfer_get_type (void);


/* Signal Emitters */
void gnomechat_transfer_status_changed (GnomechatTransfer * transfer,
										GnomechatTransferStatus status);
void gnomechat_transfer_progress (GnomechatTransfer * transfer,
								  GnomeVFSFileSize bytes_completed);
void gnomechat_transfer_cancelled (GnomechatTransfer * transfer);


/* GInterface VTable Wrappers */
GnomechatTransferType gnomechat_transfer_get_transfer_type (GnomechatTransfer * transfer);
GnomeVFSResult gnomechat_transfer_get_error (GnomechatTransfer * transfer);
GnomechatTransferStatus gnomechat_transfer_get_status (GnomechatTransfer * transfer);

GnomeVFSURI *gnomechat_transfer_get_from_uri (GnomechatTransfer * transfer);
void gnomechat_transfer_set_from_uri (GnomechatTransfer * transfer,
									  GnomeVFSURI * uri);

GnomeVFSURI *gnomechat_transfer_get_to_uri (GnomechatTransfer * transfer);
void gnomechat_transfer_set_to_uri (GnomechatTransfer * transfer,
									GnomeVFSURI * uri);

GnomeVFSFileSize gnomechat_transfer_get_file_size (GnomechatTransfer * transfer);
GnomeVFSFileSize gnomechat_transfer_get_bytes_copied (GnomechatTransfer * transfer);

void gnomechat_transfer_start_transfer (GnomechatTransfer * transfer);
void gnomechat_transfer_cancel_transfer (GnomechatTransfer * transfer);

gulong gnomechat_transfer_get_n_files (GnomechatTransfer * transfer);
gulong gnomechat_transfer_get_file_index (GnomechatTransfer * transfer);


#endif /* __GNOMECHAT_TRANSFER_H__ */
