/*
 *  GnomeChat: src/windows.h
 *
 *  Copyright (c) 2002 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __WINDOWS_H__
#define __WINDOWS_H__

#include <glib.h>
#include <gtk/gtkenums.h>
#include <gtk/gtkwidget.h>


GtkWidget *windows_open_new_window (void);
void windows_set_tab_position (GtkPositionType pos);
void windows_find_and_focus_chat (gpointer box);
GtkWidget *windows_get_empty_chat (void);


#endif /* __WINDOWS_H__ */
