/*
 *  GnomeChat: src/gnomechat-rdf-model.h
 *
 *  Copyright (c) 2003 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. 01234567890
 */

#ifndef __GNOMECHAT_RDF_MODEL_H__
#define __GNOMECHAT_RDF_MODEL_H__


#include "gnomechat-rdf-network.h"

#include <gtk/gtktreeviewcolumn.h>
#include <gtk/gtktreestore.h>


#define GNOMECHAT_TYPE_RDF_MODEL			(gnomechat_rdf_model_get_type ())
#define GNOMECHAT_RDF_MODEL(obj)			(G_TYPE_CHECK_INSTANCE_CAST ((obj), GNOMECHAT_TYPE_RDF_MODEL, GnomechatRdfModel))
#define GNOMECHAT_IS_RDF_MODEL(obj)			(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GNOMECHAT_TYPE_RDF_MODEL))
#define GNOMECHAT_RDF_MODEL_CLASS(obj)		(G_TYPE_CHECK_CLASS_CAST ((obj), GNOMECHAT_TYPE_RDF_MODEL, GnomechatRdfModelClass))
#define GNOMECHAT_IS_RDF_MODEL_CLASS(obj)	(G_TYPE_CHECK_CLASS_TYPE ((obj), GNOMECHAT_TYPE_RDF_MODEL))
#define GNOMECHAT_RDF_MODEL_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS ((obj), GNOMECHAT_TYPE_RDF_MODEL, GnomechatRdfModelClass))


typedef struct _GnomechatRdfModel GnomechatRdfModel;
typedef struct _GnomechatRdfModelPrivate GnomechatRdfModelPrivate;
typedef struct _GnomechatRdfModelClass GnomechatRdfModelClass;

struct _GnomechatRdfModel
{
	GtkTreeStore parent;

	GnomechatRdfModelPrivate *_priv;
};

struct _GnomechatRdfModelClass
{
	GtkTreeStoreClass parent_class;
};


GType gnomechat_rdf_model_get_type (void);

GtkTreeModel *gnomechat_rdf_model_get_bookmarks (void);

void gnomechat_rdf_model_append_file (GnomechatRdfModel * rdf, const gchar * filename);

GObject *gnomechat_rdf_model_get_data (GnomechatRdfModel * rdf, GtkTreeIter * iter);

GObject *gnomechat_rdf_model_get_data_from_uri (GnomechatRdfModel * rdf, const gchar * uri);

gboolean gnomechat_rdf_model_get_iter_from_uri (GnomechatRdfModel * rdf, GtkTreeIter * iter,
												const gchar * uri);
gboolean gnomechat_rdf_model_get_iter_from_data (GnomechatRdfModel * rdf, GtkTreeIter * iter,
												 GObject * data);

void gnomechat_rdf_model_remove (GnomechatRdfModel * rdf, GtkTreeIter * iter);

void gnomechat_rdf_model_remove_network (GnomechatRdfModel * rdf, GnomechatRdfNetwork * network);
void gnomechat_rdf_model_append_network (GnomechatRdfModel * rdf, GnomechatRdfNetwork * network);

void gnomechat_rdf_model_append_server (GnomechatRdfModel * rdf, GnomechatRdfServer * server,
										GnomechatRdfNetwork * network);
void gnomechat_rdf_model_remove_server (GnomechatRdfModel * rdf, GnomechatRdfServer * server);

void gnomechat_rdf_model_render_name (GtkTreeViewColumn * col, GtkCellRenderer * cell,
									  GtkTreeModel * tree_model, GtkTreeIter * iter,
									  gpointer user_data);
void gnomechat_rdf_model_render_icon (GtkTreeViewColumn * col, GtkCellRenderer * cell,
									  GtkTreeModel * tree_model, GtkTreeIter * iter,
									  gpointer user_data);

#endif /* __GNOMECHAT_RDF_MODEL_H__ */
