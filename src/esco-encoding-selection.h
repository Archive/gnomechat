/*
 *  GnomeChat: src/esco-encoding-selection.h
 *
 *  Copyright (c) 2002 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef __ESCO_ENCODING_SELECTION_H__
#define __ESCO_ENCODING_SELECTION_H__


#include "esco-encoding.h"


#include <gtk/gtkvbox.h>
#include <gtk/gtkdialog.h>


/* ************************************************************************** *
 *  EscoEncodingSelection                                                      *
 * ************************************************************************** */


#define ESCO_TYPE_ENCODING_SELECTION			(esco_encoding_selection_get_type ())
#define ESCO_ENCODING_SELECTION(obj)			(G_TYPE_CHECK_INSTANCE_CAST ((obj), ESCO_TYPE_ENCODING_SELECTION, EscoEncodingSelection))
#define ESCO_ENCODING_SELECTION_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass), ESCO_TYPE_ENCODING_SELECTION, EscoEncodingSelectionClass))
#define ESCO_IS_ENCODING_SELECTION(obj)			(G_TYPE_CHECK_INSTANCE_TYPE ((obj), ESCO_TYPE_ENCODING_SELECTION))
#define ESCO_IS_ENCODING_SELECTION_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), ESCO_TYPE_ENCODING_SELECTION))
#define ESCO_ENCODING_SELECTION_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS ((obj), ESCO_TYPE_ENCODING_SELECTION, EscoEncodingSelectionClass))


typedef struct _EscoEncodingSelection EscoEncodingSelection;
typedef struct _EscoEncodingSelectionPrivate EscoEncodingSelectionPrivate;
typedef struct _EscoEncodingSelectionClass EscoEncodingSelectionClass;


typedef void (*EscoEncodingSelectionSignalFunc) (EscoEncodingSelection * sel);


struct _EscoEncodingSelection
{
	GtkVBox parent;

	EscoEncodingSelectionPrivate *_priv;
};

struct _EscoEncodingSelectionClass
{
	GtkVBoxClass parent_class;

	EscoEncodingSelectionSignalFunc changed;
};


GType esco_encoding_selection_get_type (void);

GtkWidget *esco_encoding_selection_new (void);

G_CONST_RETURN gchar *esco_encoding_selection_get_encoding (EscoEncodingSelection * sel);
void esco_encoding_selection_set_encoding (EscoEncodingSelection * sel,
										   const gchar * encoding);

void esco_encoding_selection_changed (EscoEncodingSelection * sel);


/* ************************************************************************** *
 *  EscoEncodingSelectionDialog                                                *
 * ************************************************************************** */


#define ESCO_TYPE_ENCODING_SELECTION_DIALOG				(esco_encoding_selection_dialog_get_type ())
#define ESCO_ENCODING_SELECTION_DIALOG(obj)				(G_TYPE_CHECK_INSTANCE_CAST ((obj), ESCO_TYPE_ENCODING_SELECTION_DIALOG, EscoEncodingSelectionDialog))
#define ESCO_ENCODING_SELECTION_DIALOG_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST ((klass), ESCO_TYPE_ENCODING_SELECTION_DIALOG, EscoEncodingSelectionDialogClass))
#define ESCO_IS_ENCODING_SELECTION_DIALOG(obj)			(G_TYPE_CHECK_INSTANCE_TYPE ((obj), ESCO_TYPE_ENCODING_SELECTION_DIALOG))
#define ESCO_IS_ENCODING_SELECTION_DIALOG_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), ESCO_TYPE_ENCODING_SELECTION_DIALOG))
#define ESCO_ENCODING_SELECTION_DIALOG_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS ((obj), ESCO_TYPE_ENCODING_SELECTION_DIALOG, EscoEncodingSelectionDialogClass))


typedef struct _EscoEncodingSelectionDialog EscoEncodingSelectionDialog;
typedef struct _EscoEncodingSelectionDialogPrivate EscoEncodingSelectionDialogPrivate;
typedef struct _EscoEncodingSelectionDialogClass EscoEncodingSelectionDialogClass;


typedef void (*EscoEncodingSelectionDialogSignalFunc) (EscoEncodingSelectionDialog * sel);


struct _EscoEncodingSelectionDialog
{
	GtkDialog parent;

	EscoEncodingSelectionDialogPrivate *_priv;
};

struct _EscoEncodingSelectionDialogClass
{
	GtkDialogClass parent_class;

	EscoEncodingSelectionDialogSignalFunc changed;
};


GType esco_encoding_selection_dialog_get_type (void);

GtkWidget *esco_encoding_selection_dialog_new (const gchar * title);

G_CONST_RETURN gchar
	* esco_encoding_selection_dialog_get_encoding (EscoEncodingSelectionDialog * esd);
void esco_encoding_selection_dialog_set_encoding (EscoEncodingSelectionDialog * esd,
												  const gchar * encoding);

void esco_encoding_selection_dialog_changed (EscoEncodingSelectionDialog * esd);

#endif /* __ESCO_ENCODING_SELECTION_H__ */
