/*
 *  GnomeChat: src/esco-userlist-view.c
 *
 *  Copyright (c) 2002 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "esco-userlist-view.h"

#include <menus.h>
#include <stock-items.h>
#include <esco-encoding.h>
#include <utils.h>

#include <gtk/gtktreeselection.h>
#include <gtk/gtkcellrenderertext.h>
#include <gtk/gtkcellrendererpixbuf.h>
#include <gtk/gtkliststore.h>
#include <gtk/gtktreesortable.h>
#include <gtk/gtkmenu.h>

#include <libgtcpsocket/gtcp-i18n.h>


enum
{
	PROP_0,
	PROP_ENCODING,
	PROP_IRC
};

enum
{
	SELECTION_CHANGED,
	QUERY,
	INFO,
	SEND_FILE,
	SIGNAL_LAST
};

enum
{
	USER_LIST_USER_ITEM,
	USER_LIST_STATUS,
	USER_LIST_NICK,
	USER_LIST_N_COLUMNS
};


static gint signals[SIGNAL_LAST] = { 0 };
static gpointer parent_class = NULL;


static gboolean
search_equal_func (GtkTreeModel * model,
				   gint column,
				   const gchar * key,
				   GtkTreeIter * iter,
				   EscoUserlistView * view)
{
	GIrcChannelUser *user = NULL;
	gboolean retval = FALSE;

	gtk_tree_model_get (model, iter, USER_LIST_USER_ITEM, &user, -1);

	if (user != NULL)
	{
		gchar *nick = girc_convert_to_utf8 (user->user->nick, view->encoding);

		if (nick == NULL)
			return FALSE;

		retval = girc_g_utf8_strcaseequal (nick, key);

		g_free (nick);
	}

	return retval;
}


/* TreeModel->TreeView Functions */
static void
render_userlist_status_col (GtkTreeViewColumn * col,
							GtkCellRenderer * cell,
							GtkTreeModel * model,
							GtkTreeIter * iter,
							gpointer data)
{
	GIrcChannelUser *user;
	const gchar *stock_id;

	gtk_tree_model_get (model, iter, USER_LIST_USER_ITEM, &user, -1);

	g_assert (user != NULL);

	if (user->modes & GIRC_CHANNEL_USER_MODE_CREATOR
		|| user->modes & GIRC_CHANNEL_USER_MODE_OPS || user->modes & GIRC_CHANNEL_USER_MODE_HALFOPS)
	{
		if (user->modes & GIRC_CHANNEL_USER_MODE_VOICE)
			stock_id = GNOMECHAT_STOCK_STATUS_OP_VOICE;
		else
			stock_id = GNOMECHAT_STOCK_STATUS_OP;
	}
	else if (user->modes & GIRC_CHANNEL_USER_MODE_VOICE)
	{
		stock_id = GNOMECHAT_STOCK_STATUS_VOICE;
	}
	else
	{
		stock_id = GNOMECHAT_STOCK_STATUS_NORMAL;
	}

	g_object_set (cell, "stock-id", stock_id, NULL);
}


static void
render_userlist_nick_col (GtkTreeViewColumn * col,
						  GtkCellRenderer * cell,
						  GtkTreeModel * model,
						  GtkTreeIter * iter,
						  EscoUserlistView * view)
{
	GIrcChannelUser *user;
	gchar *nick;

	gtk_tree_model_get (model, iter, USER_LIST_USER_ITEM, &user, -1);
	nick = girc_convert_to_utf8 (user->user->nick, view->encoding);

	g_object_set (cell, "text", nick, NULL);

	if (nick != NULL)
		g_free (nick);

	girc_channel_user_unref (user);
}


static gint
sort_userlist_status_col (GtkTreeModel * model,
						  GtkTreeIter * iter1,
						  GtkTreeIter * iter2,
						  EscoUserlistView * view)
{
	GIrcChannelUser *user1 = NULL,
	 *user2 = NULL;
	gint retval;

	gtk_tree_model_get (model, iter1, USER_LIST_USER_ITEM, &user1, -1);
	gtk_tree_model_get (model, iter2, USER_LIST_USER_ITEM, &user2, -1);

	retval = girc_channel_user_collate (user1, user2, view->encoding);

	if (user1 != NULL)
		girc_channel_user_unref (user1);
	if (user2 != NULL)
		girc_channel_user_unref (user2);

	return retval;
}


static gint
sort_userlist_nick_col (GtkTreeModel * model,
						GtkTreeIter * iter1,
						GtkTreeIter * iter2,
						EscoUserlistView * view)
{
	GIrcChannelUser *user1,
	 *user2;
	gint retval;
	gchar *nick1,
	 *nick2;

	gtk_tree_model_get (model, iter1, USER_LIST_USER_ITEM, &user1, -1);
	gtk_tree_model_get (model, iter2, USER_LIST_USER_ITEM, &user2, -1);

	g_assert (user1 != NULL && user2 != NULL);

	nick1 = girc_convert_to_utf8 (user1->user->nick, view->encoding);
	nick2 = girc_convert_to_utf8 (user2->user->nick, view->encoding);

	if (nick1 == NULL && nick2 == NULL)
		retval = 0;
	else if (nick1 == NULL && nick2 != NULL)
		retval = 1;
	else if (nick1 != NULL && nick2 == NULL)
		retval = -1;
	else
		retval = g_utf8_collate (nick2, nick2);

	g_free (nick1);
	g_free (nick2);

	girc_channel_user_unref (user1);
	girc_channel_user_unref (user2);

	return retval;
}


/* Sub-Object Callbacks */
static void
sel_changed_cb (GtkTreeSelection * sel,
				EscoUserlistView * view)
{
	view->any_selected = gtk_tree_selection_count_selected_rows (sel);

	g_signal_emit (view, signals[SELECTION_CHANGED], 0);
}


/* EscoUserlistView Callbacks */
static void
esco_userlist_view_query (EscoUserlistView * view,
						  GIrcChannelUser * user)
{
	g_return_if_fail (GIRC_IS_CLIENT (view->irc));

	girc_client_open_query (view->irc, user->user->nick);
}


static void
esco_userlist_view_info (EscoUserlistView * view,
						 GIrcChannelUser * user)
{
	gchar *msg;

	g_return_if_fail (GIRC_IS_CLIENT (view->irc));

	msg = g_strdup_printf ("WHOIS %s", user->user->nick);
	girc_client_send_raw (view->irc, msg);
	g_free (msg);
}


static void
esco_userlist_view_send_file (EscoUserlistView * view,
							  GIrcChannelUser * user)
{
	g_return_if_fail (GIRC_IS_CLIENT (view->irc));

	g_warning ("FIXME: Implement sending file based on userlist's \"send-file\" signal.");
}


/* GtkWidget Callbacks (& Friends) */
static void
userlist_menu_pos_func (GtkWidget * menu,
						gint * x,
						gint * y,
						gboolean * push_in,
						GdkRectangle * rect)
{
	*x = rect->x;
	*y = rect->y;

	*push_in = FALSE;
}


static gboolean
esco_userlist_view_button_press_event (GtkWidget * widget,
									   GdkEventButton * event)
{
	EscoUserlistView *view = ESCO_USERLIST_VIEW (widget);

	if (event->button == 2)
	{
		esco_userlist_view_query_selected (view);
	}
	else if (event->button == 3)
	{
		GtkTreeView *tree_view = GTK_TREE_VIEW (widget);
		GtkTreePath *path;
		GtkTreeViewColumn *col;

		GdkRectangle rect;

		gtk_tree_view_get_path_at_pos (tree_view, event->x, event->y, &path, &col, NULL, NULL);

		if (!path)
			return FALSE;

		rect.x = event->x_root;
		rect.y = event->y_root;

		if (!GTK_IS_WIDGET (view->menu))
		{
			view->menu = gtk_menu_new ();
			menu_fill_userlist_popup (view->menu, view);
		}

		gtk_menu_popup (GTK_MENU (view->menu), NULL, NULL,
						(GtkMenuPositionFunc) userlist_menu_pos_func,
						&rect, event->button, event->time);
	}

	if (GTK_WIDGET_CLASS (parent_class)->button_press_event != NULL)
		return (*GTK_WIDGET_CLASS (parent_class)->button_press_event) (widget, event);
	else
		return FALSE;
}


/* GtkTreeView Callbacks */
static void
esco_userlist_view_row_activated (GtkTreeView * treeview,
								  GtkTreePath * path,
								  GtkTreeViewColumn * col)
{
	esco_userlist_view_query_selected (ESCO_USERLIST_VIEW (treeview));

	if (GTK_TREE_VIEW_CLASS (parent_class)->row_activated)
		(*GTK_TREE_VIEW_CLASS (parent_class)->row_activated) (treeview, path, col);
}


/* GtkObject Callbacks */
static void
esco_userlist_view_destroy (GtkObject * object)
{
	EscoUserlistView *view = ESCO_USERLIST_VIEW (object);

	if (view->menu)
		gtk_widget_destroy (view->menu);

	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(*GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}


/* GObject Callbacks */
static void
esco_userlist_view_get_property (GObject * object,
								 guint param_id,
								 GValue * value,
								 GParamSpec * pspec)
{
	EscoUserlistView *view = ESCO_USERLIST_VIEW (object);

	switch (param_id)
	{
	case PROP_ENCODING:
		g_value_set_string (value, view->encoding);
		break;
	case PROP_IRC:
		g_value_set_object (value, view->irc);
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}


static void
esco_userlist_view_set_property (GObject * object,
								 guint param_id,
								 const GValue * value,
								 GParamSpec * pspec)
{
	EscoUserlistView *view = ESCO_USERLIST_VIEW (object);

	switch (param_id)
	{
	case PROP_ENCODING:
		esco_userlist_view_set_encoding (view, g_value_get_string (value));
		break;
	case PROP_IRC:
		esco_userlist_view_set_irc (view, g_value_get_object (value));
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}


static GObject *
esco_userlist_view_constructor (GType type,
								guint n_construct_params,
								GObjectConstructParam * construct_params)
{
	GObject *object;
	EscoUserlistView *view;
	GtkTreeView *tree_view;

	/* Sub-Widgets */
	GtkListStore *model;
	GtkCellRenderer *cell;
	GtkTreeViewColumn *col;
	GtkTreeSelection *sel;

	object = (*G_OBJECT_CLASS (parent_class)->constructor)
		(type, n_construct_params, construct_params);

	view = ESCO_USERLIST_VIEW (object);
	tree_view = GTK_TREE_VIEW (object);

	model = gtk_list_store_new (USER_LIST_N_COLUMNS,
								GIRC_TYPE_CHANNEL_USER, G_TYPE_UINT, G_TYPE_STRING);

	gtk_tree_view_set_model (tree_view, GTK_TREE_MODEL (model));

	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (model),
										  USER_LIST_STATUS, GTK_SORT_ASCENDING);

	gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (model), USER_LIST_STATUS,
									 (GtkTreeIterCompareFunc) sort_userlist_status_col, view, NULL);
	gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (model), USER_LIST_NICK,
									 (GtkTreeIterCompareFunc) sort_userlist_nick_col, view, NULL);

	gtk_tree_view_set_enable_search (tree_view, TRUE);
	gtk_tree_view_set_search_column (tree_view, USER_LIST_NICK);
	gtk_tree_view_set_search_equal_func (tree_view,
										 (GtkTreeViewSearchEqualFunc) search_equal_func,
										 g_object_ref (view), g_object_unref);

	sel = gtk_tree_view_get_selection (tree_view);
	gtk_tree_selection_set_mode (sel, GTK_SELECTION_BROWSE);
	g_signal_connect (sel, "changed", G_CALLBACK (sel_changed_cb), view);

	col = gtk_tree_view_column_new ();
	gtk_tree_view_column_set_clickable (col, TRUE);
	gtk_tree_view_column_set_sort_column_id (col, USER_LIST_STATUS);
	gtk_tree_view_column_set_resizable (col, FALSE);
	gtk_tree_view_append_column (tree_view, col);

	cell = gtk_cell_renderer_pixbuf_new ();
	g_object_set (cell, "xalign", 0.5, NULL);
	gtk_tree_view_column_pack_start (col, cell, FALSE);
	gtk_tree_view_column_set_cell_data_func (col, cell,
											 (GtkTreeCellDataFunc)
											 render_userlist_status_col, NULL, NULL);

	col = gtk_tree_view_column_new ();
	gtk_tree_view_column_set_title (col, _("Users"));
	gtk_tree_view_column_set_clickable (col, TRUE);
	gtk_tree_view_column_set_sort_column_id (col, USER_LIST_NICK);
	gtk_tree_view_column_set_resizable (col, TRUE);
	gtk_tree_view_append_column (tree_view, col);

	cell = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_start (col, cell, FALSE);
	gtk_tree_view_column_set_cell_data_func (col, cell,
											 (GtkTreeCellDataFunc)
											 render_userlist_nick_col, view, NULL);

	return object;
}


static void
irc_destroyed (EscoUserlistView * view,
			   GObject * old_irc)
{
	view->irc = NULL;
}


static void
esco_userlist_view_finalize (GObject * object)
{
	EscoUserlistView *view = ESCO_USERLIST_VIEW (object);

	if (view->irc != NULL)
	{
		g_object_weak_unref (G_OBJECT (view->irc), (GWeakNotify) irc_destroyed, view);
		view->irc = NULL;
	}

	if (view->encoding != NULL)
		g_free (view->encoding);
}



/* GType Functions */
static void
esco_userlist_view_class_init (EscoUserlistViewClass * class)
{
	GObjectClass *object_class = G_OBJECT_CLASS (class);
	GtkObjectClass *gtk_object_class = GTK_OBJECT_CLASS (class);
	GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (class);
	GtkTreeViewClass *treeview_class = GTK_TREE_VIEW_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	object_class->get_property = esco_userlist_view_get_property;
	object_class->set_property = esco_userlist_view_set_property;
	object_class->constructor = esco_userlist_view_constructor;
	object_class->finalize = esco_userlist_view_finalize;

	gtk_object_class->destroy = esco_userlist_view_destroy;

	widget_class->button_press_event = esco_userlist_view_button_press_event;

	treeview_class->row_activated = esco_userlist_view_row_activated;

	class->query = esco_userlist_view_query;
	class->info = esco_userlist_view_info;
	class->send_file = esco_userlist_view_send_file;

	g_object_class_install_property (object_class, PROP_ENCODING,
									 g_param_spec_string
									 ("encoding", "Charset Encoding",
									  "The expected encoding of nicknames for users.",
									  "UTF-8", G_PARAM_READWRITE | G_PARAM_CONSTRUCT));
	g_object_class_install_property (object_class, PROP_IRC,
									 g_param_spec_object
									 ("irc", "IRC Client",
									  "The client the users in this userlist are on.",
									  GIRC_TYPE_CLIENT, G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

	signals[SELECTION_CHANGED] =
		g_signal_new ("selection-changed", G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_LAST,
					  G_STRUCT_OFFSET (EscoUserlistViewClass, selection_changed),
					  NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);

	signals[QUERY] =
		g_signal_new ("query", G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_LAST,
					  G_STRUCT_OFFSET (EscoUserlistViewClass, query),
					  NULL, NULL, g_cclosure_marshal_VOID__BOXED,
					  G_TYPE_NONE, 1, GIRC_TYPE_CHANNEL_USER);

	signals[INFO] =
		g_signal_new ("info", G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_LAST,
					  G_STRUCT_OFFSET (EscoUserlistViewClass, info),
					  NULL, NULL, g_cclosure_marshal_VOID__BOXED,
					  G_TYPE_NONE, 1, GIRC_TYPE_CHANNEL_USER);

	signals[SEND_FILE] =
		g_signal_new ("send-file", G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_LAST,
					  G_STRUCT_OFFSET (EscoUserlistViewClass, send_file),
					  NULL, NULL, g_cclosure_marshal_VOID__BOXED,
					  G_TYPE_NONE, 1, GIRC_TYPE_CHANNEL_USER);
}


static void
esco_userlist_view_instance_init (EscoUserlistView * view)
{
	view->irc = NULL;
	view->menu = NULL;
	view->encoding = NULL;
}


/* PUBLIC API */
GType
esco_userlist_view_get_type (void)
{
	static GType type = 0;

	if (!type)
	{
		static const GTypeInfo info = {
			sizeof (EscoUserlistViewClass),
			NULL,				/* base_init */
			NULL,				/* base_finalize */
			(GClassInitFunc) esco_userlist_view_class_init,
			NULL,				/* class_finalize */
			NULL,				/* class_data */
			sizeof (EscoUserlistView),
			0,					/* n_preallocs */
			(GInstanceInitFunc) esco_userlist_view_instance_init,
		};

		type = g_type_register_static (GTK_TYPE_TREE_VIEW, "EscoUserlistView", &info, 0);
	}

	return type;
}


GtkWidget *
esco_userlist_view_new (GIrcClient * irc)
{
	return GTK_WIDGET (g_object_new (ESCO_TYPE_USERLIST_VIEW, "irc", irc, NULL));
}


void
esco_userlist_view_set_irc (EscoUserlistView * view,
							GIrcClient * irc)
{
	g_return_if_fail (view != NULL);
	g_return_if_fail (ESCO_IS_USERLIST_VIEW (view));
	g_return_if_fail (irc == NULL || (irc != NULL && GIRC_IS_CLIENT (irc)));

	if (view->irc != NULL)
		g_object_weak_unref (G_OBJECT (irc), (GWeakNotify) irc_destroyed, view);

	view->irc = irc;

	if (irc != NULL)
		g_object_weak_ref (G_OBJECT (irc), (GWeakNotify) irc_destroyed, view);

	g_object_notify (G_OBJECT (view), "irc");
}


void
esco_userlist_view_set_encoding (EscoUserlistView * view,
								 const gchar * encoding)
{
	GtkTreeModel *model;

	g_return_if_fail (view != NULL);
	g_return_if_fail (ESCO_IS_USERLIST_VIEW (view));
	g_return_if_fail (esco_encoding_is_valid (encoding));

	if (view->encoding != NULL)
		g_free (view->encoding);

	view->encoding = g_strdup (encoding);

	/* Refresh the view for the new encoding */
	model = gtk_tree_view_get_model (GTK_TREE_VIEW (view));
	gtk_tree_view_set_model (GTK_TREE_VIEW (view), model);

	g_object_notify (G_OBJECT (view), "encoding");
}


static void
handle_signal_emission (EscoUserlistView * view,
						gint signal_id)
{
	GSList *users;

	for (users = esco_userlist_view_get_selected (view);
		 users != NULL; users = g_slist_remove_link (users, users))
	{
		if (users->data != NULL)
		{
			g_signal_emit (view, signal_id, 0, users->data);

			girc_channel_user_unref (GIRC_CHANNEL_USER (users->data));
		}
	}
}


void
esco_userlist_view_query_selected (EscoUserlistView * view)
{
	g_return_if_fail (view != NULL);
	g_return_if_fail (ESCO_IS_USERLIST_VIEW (view));

	handle_signal_emission (view, signals[QUERY]);
}


void
esco_userlist_view_info_on_selected (EscoUserlistView * view)
{
	g_return_if_fail (view != NULL);
	g_return_if_fail (ESCO_IS_USERLIST_VIEW (view));

	handle_signal_emission (view, signals[INFO]);
}


void
esco_userlist_view_send_file_to_selected (EscoUserlistView * view)
{
	g_return_if_fail (view != NULL);
	g_return_if_fail (ESCO_IS_USERLIST_VIEW (view));

	handle_signal_emission (view, signals[SEND_FILE]);
}


GSList *
esco_userlist_view_get_selected (EscoUserlistView * view)
{
	GtkTreeSelection *sel;
	GtkTreeModel *model;
	GList *rows;
	GSList *retval = NULL;

	g_return_val_if_fail (view != NULL, NULL);
	g_return_val_if_fail (ESCO_IS_USERLIST_VIEW (view), NULL);

	sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (view));

	for (rows = gtk_tree_selection_get_selected_rows (sel, &model);
		 rows != NULL; rows = g_list_remove_link (rows, rows))
	{
		GIrcChannelUser *user;
		GtkTreeIter iter;

		gtk_tree_model_get_iter (model, &iter, rows->data);
		gtk_tree_model_get (model, &iter, USER_LIST_USER_ITEM, &user, -1);

		if (user != NULL)
		{
			retval = g_slist_prepend (retval, user);
		}

		gtk_tree_path_free (rows->data);
	}

	return retval;
}


void
esco_userlist_view_add_user (EscoUserlistView * view,
							 GIrcChannelUser * user)
{
	GtkListStore *model;
	GtkTreeIter iter;

	g_return_if_fail (view != NULL);
	g_return_if_fail (ESCO_IS_USERLIST_VIEW (view));
	g_return_if_fail (user != NULL);
	g_return_if_fail (GIRC_IS_CHANNEL_USER (user));

	model = GTK_LIST_STORE (gtk_tree_view_get_model (GTK_TREE_VIEW (view)));

	gtk_list_store_append (model, &iter);
	gtk_list_store_set (model, &iter, USER_LIST_USER_ITEM, user, -1);
}


static gboolean
search_to_remove_user (GtkTreeModel * model,
					   GtkTreePath * path,
					   GtkTreeIter * iter,
					   const GIrcChannelUser * user)
{
	GIrcChannelUser *item_user = NULL;
	gboolean retval = FALSE;

	gtk_tree_model_get (model, iter, USER_LIST_USER_ITEM, &item_user, -1);

	if (item_user != NULL && item_user == user)
	{
		gtk_list_store_remove (GTK_LIST_STORE (model), iter);
		retval = TRUE;
	}

	girc_channel_user_unref (item_user);

	return retval;
}


void
esco_userlist_view_remove_user (EscoUserlistView * view,
								GIrcChannelUser * user)
{
	g_return_if_fail (view != NULL);
	g_return_if_fail (ESCO_IS_USERLIST_VIEW (view));
	g_return_if_fail (user != NULL);
	g_return_if_fail (GIRC_IS_CHANNEL_USER (user));

	gtk_tree_model_foreach (gtk_tree_view_get_model
							(GTK_TREE_VIEW (view)),
							(GtkTreeModelForeachFunc) search_to_remove_user, user);
}


static gboolean
search_to_update_user (GtkTreeModel * model,
					   GtkTreePath * path,
					   GtkTreeIter * iter,
					   const GIrcChannelUser * user)
{
	GIrcChannelUser *item_user = NULL;
	gboolean retval = FALSE;

	gtk_tree_model_get (model, iter, USER_LIST_USER_ITEM, &item_user, -1);

	if (item_user != NULL && item_user == user)
	{
		gtk_list_store_set (GTK_LIST_STORE (model), iter, USER_LIST_USER_ITEM, user, -1);
		retval = TRUE;
	}

	girc_channel_user_unref (item_user);

	return retval;
}


void
esco_userlist_view_update_user (EscoUserlistView * view,
								GIrcChannelUser * user)
{
	GtkTreeModel *model;

	g_return_if_fail (view != NULL);
	g_return_if_fail (ESCO_IS_USERLIST_VIEW (view));
	g_return_if_fail (user != NULL);
	g_return_if_fail (GIRC_IS_CHANNEL_USER (user));

	model = gtk_tree_view_get_model (GTK_TREE_VIEW (view));

	gtk_tree_model_foreach (model, (GtkTreeModelForeachFunc) search_to_update_user, user);
}


void
esco_userlist_view_clear (EscoUserlistView * view)
{
	g_return_if_fail (view != NULL);
	g_return_if_fail (ESCO_IS_USERLIST_VIEW (view));

	gtk_list_store_clear (GTK_LIST_STORE (gtk_tree_view_get_model (GTK_TREE_VIEW (view))));
}
