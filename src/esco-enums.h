/*
 *  GnomeChat: src/esco-enums.h
 *
 *  Copyright (c) 2002 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef __ESCO_ENUMS_H__
#define __ESCO_ENUMS_H__


typedef enum
{
	ESCO_CHATS_MENU_ITEM_SERVER,
	ESCO_CHATS_MENU_ITEM_CHAT,
	ESCO_CHATS_MENU_ITEM_EMPTY,
}
EscoChatsMenuItemType;


typedef enum
{
	ESCO_PESTER_NONE,
	ESCO_PESTER_IRC,
	ESCO_PESTER_TEXT,
	ESCO_PESTER_MSG,

	ESCO_PESTER_LAST
}
EscoPesterType;


typedef enum
{
	ESCO_CHAT_MODE_NONE,
	ESCO_CHAT_MODE_SERVER,
	ESCO_CHAT_MODE_CHANNEL,
	ESCO_CHAT_MODE_USER
}
EscoChatModeType;


typedef enum
{
	ESCO_WINDOW_LAYOUT_UNSET,
	ESCO_WINDOW_LAYOUT_TABS,
	ESCO_WINDOW_LAYOUT_TREE,
	ESCO_WINDOW_LAYOUT_WINDOW,

	ESCO_WINDOW_LAYOUT_LAST
}
EscoWindowLayoutType;


enum
{
	ESCO_CHATS_LIST_CHAT_BOX,
	ESCO_CHATS_LIST_NUM_COLUMNS
};


#endif /* __ESCO_ENUMS_H__ */
