/*
 *  GnomeChat: src/esco-encoding-picker.c
 *
 *  Copyright (c) 2002 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "gnomechat.h"
#include "esco-encoding-picker.h"

#include "esco-encoding.h"
#include "esco-encoding-menu.h"
#include "esco-type-builtins.h"


enum
{
	PROP_0,
	PROP_ENCODING,
	PROP_TITLE,
	PROP_MODAL,
	PROP_DIALOG
};

enum
{
	DIALOG_OPENED,
	HELP_CLICKED,
	ENCODING_CHANGED,
	LAST_SIGNAL
};


struct _EscoEncodingPickerPrivate
{
	gchar *encoding;
	gchar *title;
	gboolean modal:1;
};


static gpointer parent_class = NULL;
static gint signals[LAST_SIGNAL] = { 0 };


/* Sub-Widget Callbacks */
static void
menu_help_clicked_cb (gpointer * menu,
					  GObject * epicker)
{
	g_signal_emit (epicker, signals[HELP_CLICKED], 0);
}


static void
menu_dialog_opened_cb (gpointer * menu,
					   GObject * epicker)
{
	g_signal_emit (epicker, signals[DIALOG_OPENED], 0);
}


static void
menu_encoding_changed_cb (EscoEncodingMenu * menu,
						  GObject * epicker)
{

	g_signal_emit_by_name (epicker, "changed");
	g_signal_emit (epicker, signals[ENCODING_CHANGED], 0);
}


/* Object Callbacks */
static void
esco_encoding_picker_get_property (GObject * object,
								   guint param_id,
								   GValue * value,
								   GParamSpec * pspec)
{
	EscoEncodingPicker *epicker = ESCO_ENCODING_PICKER (object);

	switch (param_id)
	{
	case PROP_ENCODING:
		g_value_set_string (value, epicker->_priv->encoding);
		break;
	case PROP_TITLE:
		g_value_set_string (value, epicker->_priv->title);
		break;
	case PROP_MODAL:
		g_value_set_boolean (value, epicker->_priv->modal);
		break;
	case PROP_DIALOG:
		g_value_set_object (value,
							esco_encoding_menu_get_dialog (ESCO_ENCODING_MENU
														   (GTK_OPTION_MENU (object)->
															menu)));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}


static void
esco_encoding_picker_set_property (GObject * object,
								   guint param_id,
								   const GValue * value,
								   GParamSpec * pspec)
{
	EscoEncodingPicker *epicker = ESCO_ENCODING_PICKER (object);

	switch (param_id)
	{
	case PROP_ENCODING:
		esco_encoding_picker_set_encoding (epicker, g_value_get_string (value));
		break;
	case PROP_TITLE:
		esco_encoding_picker_set_title (epicker, g_value_get_string (value));
		break;
	case PROP_MODAL:
		esco_encoding_picker_set_modal (epicker, g_value_get_boolean (value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}


static GObject *
esco_encoding_picker_constructor (GType type,
								  guint n_construct_params,
								  GObjectConstructParam * construct_params)
{

	GObject *object;
	EscoEncodingPicker *epicker;
	GtkWidget *menu;

	object = (*G_OBJECT_CLASS (parent_class)->constructor) (type, n_construct_params,
															construct_params);

	epicker = ESCO_ENCODING_PICKER (object);

	menu = GTK_WIDGET (g_object_new (ESCO_TYPE_ENCODING_MENU,
									 "encoding", epicker->_priv->encoding,
									 "title", epicker->_priv->title,
									 "modal", epicker->_priv->modal, NULL));

	g_signal_connect (menu, "dialog-opened", G_CALLBACK (menu_dialog_opened_cb), epicker);
	g_signal_connect (menu, "help-clicked", G_CALLBACK (menu_help_clicked_cb), epicker);
	g_signal_connect (menu, "encoding-changed", G_CALLBACK (menu_encoding_changed_cb),
					  epicker);
	gtk_option_menu_set_menu (GTK_OPTION_MENU (object), menu);
	gtk_widget_show (menu);

	if (epicker->_priv->encoding)
	{
		g_free (epicker->_priv->encoding);
		epicker->_priv->encoding = NULL;
	}

	if (epicker->_priv->title)
	{
		g_free (epicker->_priv->title);
		epicker->_priv->title = NULL;
	}

	return object;
}


static void
esco_encoding_picker_finalize (GObject * object)
{
	EscoEncodingPicker *epicker = ESCO_ENCODING_PICKER (object);

	if (epicker->_priv->encoding)
		g_free (epicker->_priv->encoding);

	g_free (epicker->_priv);

	if (G_OBJECT_CLASS (parent_class)->finalize)
		(*G_OBJECT_CLASS (parent_class)->finalize) (object);
}


/* GType Functions */
static void
esco_encoding_picker_class_init (EscoEncodingPickerClass * class)
{
	GObjectClass *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	object_class->get_property = esco_encoding_picker_get_property;
	object_class->set_property = esco_encoding_picker_set_property;
	object_class->constructor = esco_encoding_picker_constructor;
	object_class->finalize = esco_encoding_picker_finalize;

	g_object_class_install_property (object_class, PROP_ENCODING,
									 g_param_spec_string ("encoding",
														  _("Character Encoding"),
														  _("The currently selected "
															"encoding."),
														  "UTF-8",
														  (G_PARAM_CONSTRUCT |
														   G_PARAM_READWRITE)));

	g_object_class_install_property (object_class, PROP_TITLE,
									 g_param_spec_string ("title",
														  _("Browse Dialog Title"),
														  _("The title to use for the "
															"browse dialog."),
														  _("Select A Character "
															"Encoding"),
														  (G_PARAM_CONSTRUCT |
														   G_PARAM_READWRITE)));

	g_object_class_install_property (object_class, PROP_MODAL,
									 g_param_spec_boolean ("modal",
														   _("Browse Dialog Modal"),
														   _("Whether or not the browse "
															 "dialog will be modal."),
														   FALSE,
														   (G_PARAM_CONSTRUCT |
															G_PARAM_READWRITE)));

	g_object_class_install_property (object_class, PROP_DIALOG,
									 g_param_spec_object ("dialog",
														  _("Browse Dialog"),
														  _("The browse dialog itself."),
														  GTK_TYPE_WIDGET,
														  G_PARAM_READABLE));

	signals[DIALOG_OPENED] =
		g_signal_new ("dialog-opened",
					  G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_LAST,
					  G_STRUCT_OFFSET
					  (EscoEncodingPickerClass, dialog_opened), NULL, NULL,
					  g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);

	signals[HELP_CLICKED] =
		g_signal_new ("help-clicked",
					  G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_LAST,
					  G_STRUCT_OFFSET
					  (EscoEncodingPickerClass, help_clicked), NULL, NULL,
					  g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);

	signals[ENCODING_CHANGED] =
		g_signal_new ("encoding-changed",
					  G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_LAST,
					  G_STRUCT_OFFSET
					  (EscoEncodingPickerClass, encoding_changed), NULL,
					  NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
}


static void
esco_encoding_picker_instance_init (EscoEncodingPicker * epicker)
{
	epicker->_priv = g_new0 (EscoEncodingPickerPrivate, 1);
	g_assert (epicker->_priv != NULL);
}


/* PUBLIC API */
GType
esco_encoding_picker_get_type (void)
{
	static GType type = 0;

	if (!type)

	{
		static const GTypeInfo info = {
			sizeof (EscoEncodingPickerClass),
			NULL,				/* base_init */
			NULL,				/* base_finalize */
			(GClassInitFunc) esco_encoding_picker_class_init,
			NULL,				/* class_finalize */
			NULL,				/* class_data */
			sizeof (EscoEncodingPicker),
			0,					/* n_preallocs */
			(GInstanceInitFunc) esco_encoding_picker_instance_init,
		};

		type = g_type_register_static (GTK_TYPE_OPTION_MENU,
									   "EscoEncodingPicker", &info, 0);
	}

	return type;
}


GtkWidget *
esco_encoding_picker_new (const gchar * title)
{
	return GTK_WIDGET (g_object_new (ESCO_TYPE_ENCODING_PICKER, "title", title, NULL));
}


G_CONST_RETURN gchar *
esco_encoding_picker_get_encoding (EscoEncodingPicker * epicker)
{
	g_return_val_if_fail (epicker != NULL, NULL);
	g_return_val_if_fail (ESCO_IS_ENCODING_PICKER (epicker), NULL);

	return epicker->_priv->encoding;
}


void
esco_encoding_picker_set_encoding (EscoEncodingPicker * epicker,
								   const gchar * encoding)
{
	GtkOptionMenu *optmenu;

	g_return_if_fail (epicker != NULL);
	g_return_if_fail (ESCO_IS_ENCODING_PICKER (epicker));

	epicker->_priv->encoding = g_strdup (encoding);

	optmenu = GTK_OPTION_MENU (epicker);

	if (optmenu->menu)
	{
		esco_encoding_menu_set_encoding (ESCO_ENCODING_MENU (optmenu->menu), encoding);
	}

	g_object_notify (G_OBJECT (epicker), "encoding");
}


G_CONST_RETURN gchar *
esco_encoding_picker_get_title (EscoEncodingPicker * epicker)
{
	g_return_val_if_fail (epicker != NULL, NULL);
	g_return_val_if_fail (ESCO_IS_ENCODING_PICKER (epicker), NULL);

	return epicker->_priv->title;
}


void
esco_encoding_picker_set_title (EscoEncodingPicker * epicker,
								const gchar * title)
{
	GtkOptionMenu *optmenu;

	g_return_if_fail (epicker != NULL);
	g_return_if_fail (ESCO_IS_ENCODING_PICKER (epicker));

	epicker->_priv->title = g_strdup (title);

	optmenu = GTK_OPTION_MENU (epicker);

	if (optmenu->menu)
	{
		esco_encoding_menu_set_title (ESCO_ENCODING_MENU (optmenu->menu), title);
	}

	g_object_notify (G_OBJECT (epicker), "title");
}


gboolean
esco_encoding_picker_get_model (EscoEncodingPicker * epicker) 
{
	g_return_val_if_fail (epicker != NULL, FALSE);
	g_return_val_if_fail (ESCO_IS_ENCODING_PICKER (epicker), FALSE);

	return epicker->_priv->modal;
}


void
esco_encoding_picker_set_modal (EscoEncodingPicker * epicker,
								gboolean modal)
{
	GtkOptionMenu *optmenu;

	g_return_if_fail (epicker != NULL);
	g_return_if_fail (ESCO_IS_ENCODING_PICKER (epicker));

	epicker->_priv->modal = modal;

	optmenu = GTK_OPTION_MENU (epicker);

	if (optmenu->menu)
		esco_encoding_menu_set_modal (ESCO_ENCODING_MENU (optmenu->menu), modal);

	g_object_notify (G_OBJECT (epicker), "modal");
}


GtkWidget *
esco_encoding_picker_get_dialog (EscoEncodingPicker * epicker)
{
	g_return_val_if_fail (epicker != NULL, NULL);
	g_return_val_if_fail (ESCO_IS_ENCODING_PICKER (epicker), NULL);

	return esco_encoding_menu_get_dialog (ESCO_ENCODING_MENU
										  (GTK_OPTION_MENU (epicker)->menu));
}
