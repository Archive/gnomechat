/*
 *  GnomeChat: src/outgoing.c
 *
 *  Copyright (c) 2003 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <outgoing.h>
#include <gnomechat.h>

#include <connect.h>
#include <esco-viewer.h>
#include <netserv-ui.h>
#include <utils.h>

#include <libgircclient/girc-client.h>


/* ************ *
 *  Data Types  *
 * ************ */

typedef void (*OutgoingParserFunc) (EscoChatBox * box,
									GIrcTarget * target,
									const gchar * data);

typedef struct
{
	const gchar *const name;

	OutgoingParserFunc parse_outgoing;

	const gchar *const *const aliases;
}
Command;


/* ****************** *
 *  Global Variables  *
 * ****************** */

static GHashTable *commands = NULL;


/* ******************* *
 *  Utility Functions  *
 * ******************* */

static inline gchar *
get_target_str (GIrcTarget * target,
				const gchar * encoding)
{
	if (target == NULL)
		return NULL;
	if (GIRC_IS_CHANNEL (target))
		return girc_convert_to_utf8 (target->channel.name, encoding);
	else
		return girc_convert_to_utf8 (target->query.user->nick, encoding);
}


/* **************************** *
 *  Outgoing Parsing Functions  *
 * **************************** */


/* ******************* *
 *  Basic IRC Parsing  *
 * ******************* */

static void
parse_outgoing_join (EscoChatBox * box,
					 GIrcTarget * target,
					 const gchar * msg)
{
	gchar *out = NULL;
	guint max_bans;
	gint i = 0;

	if (msg == NULL || msg[0] == '\0')
	{
		esco_viewer_append_line (ESCO_VIEWER (box->viewer),
								 ESCO_VIEWER_MESSAGE_ERROR, 0,
								 _("You must specify a channel to /join."),
								 NULL, NULL, FALSE);
		return;
	}

	g_object_get (box->irc, "max-bans", &max_bans, NULL);

	if (max_bans != 0)
	{
		for (i = 0, out = (gchar *) msg; *out != '\0'; ++out)
		{
			gunichar ch = g_utf8_get_char (out);

			if (ch == ',')
				i++;

			if (ch == ' ')
				break;
		}

		if (i > max_bans)
		{
			esco_viewer_append_line (ESCO_VIEWER (box->viewer),
									 ESCO_VIEWER_MESSAGE_ERROR, 0,
									 ngettext
									 ("You can only join %u channel at a time.",
									  "You can only join %u channels at a time.",
									  max_bans), NULL, NULL, FALSE);
			return;
		}
	}

	out = g_strdup_printf ("JOIN %s", msg);
	girc_client_send_raw (box->irc, out);
	g_free (out);
}


static void
parse_outgoing_part (EscoChatBox * box,
					 GIrcTarget * target,
					 const gchar * msg)
{
	if (GIRC_IS_TARGET (target))
	{
		if (msg == NULL || msg[0] == '\0' || !GIRC_IS_CHANNEL (target))
		{
			girc_client_close_target (box->irc, target);
		}
		else
		{
			gchar *target_str = girc_convert_to_utf8 (msg, target->channel.name);

			if (target_str != NULL)
			{
				gchar *out = g_strdup_printf ("PART %s :%s", target_str, msg);

				g_free (target_str);

				girc_client_send_raw (box->irc, out);
				g_free (out);
			}
		}
	}
	else
	{
		esco_viewer_append_line (ESCO_VIEWER (box->viewer),
								 ESCO_VIEWER_MESSAGE_ERROR, 0,
								 _
								 ("You can only /part channels and private chats."),
								 NULL, NULL, FALSE);
	}
}


static void
parse_outgoing_kick (EscoChatBox * box,
					 GIrcTarget * target,
					 const gchar * msg)
{
	GIrcChannelUser *target_user;
	gchar **args = NULL;
	gchar *name,
	 *vmsg,
	 *userhost;

	/* Check if we're on a channel */
	if (!GIRC_IS_CHANNEL (target))
	{
		esco_viewer_append_line (ESCO_VIEWER (box->viewer),
								 ESCO_VIEWER_MESSAGE_ERROR, 0,
								 _("You can only kick users from channels."),
								 NULL, NULL, FALSE);
		return;
	}

	/* Check if the message (and thus the user-to-kick's nickname) is NULL */
	if (msg == NULL || msg[0] == '\0')
	{
		name = girc_convert_to_utf8 (target->channel.name, box->encoding);

		vmsg = g_strdup_printf (_("You must specify a user to kick from %s."), name);
		g_free (name);

		esco_viewer_append_line (ESCO_VIEWER (box->viewer),
								 ESCO_VIEWER_MESSAGE_ERROR, 0, vmsg, NULL, NULL, FALSE);
		g_free (vmsg);
		return;
	}


	args = g_strsplit (msg, " ", 2);

	/* Check if the first nickname is NULL or empty */
	if (args[0] == NULL || args[0][0] == '\0')
	{
		name = girc_convert_to_utf8 (target->channel.name, box->encoding);

		vmsg = g_strdup_printf (_("You must specify a user to kick from %s."), name);
		g_free (name);

		esco_viewer_append_line (ESCO_VIEWER (box->viewer),
								 ESCO_VIEWER_MESSAGE_ERROR, 0, vmsg, NULL, NULL, FALSE);
		g_free (vmsg);
		g_strfreev (args);
		return;
	}

	userhost = g_strdup_printf ("%s!*@*", args[0]);

	target_user = g_hash_table_lookup (target->channel.users, userhost);
	g_free (userhost);

	/* Check if the target user is on the channel */
	if (target_user == NULL)
	{
		name = girc_convert_to_utf8 (target->channel.name, box->encoding);

		vmsg = g_strdup_printf (_("There is no user named \"%s\" on %s."), args[0], name);
		g_free (name);

		esco_viewer_append_line (ESCO_VIEWER (box->viewer),
								 ESCO_VIEWER_MESSAGE_ERROR, 0, vmsg, NULL, NULL, FALSE);
		g_free (vmsg);
		g_strfreev (args);
		return;
	}

	/* Check if we've got sufficient permissions to kick them */
	if (target->channel.my_user->modes < GIRC_CHANNEL_USER_MODE_HALFOPS ||
		target->channel.my_user->modes < target_user->modes)
	{
		name = girc_convert_to_utf8 (target->channel.name, box->encoding);
		vmsg = g_strdup_printf (_("You do not have the necessary "
								  "permissions to kick %s out of %s."), args[0], name);
		g_free (name);

		esco_viewer_append_line (ESCO_VIEWER (box->viewer),
								 ESCO_VIEWER_MESSAGE_ERROR, 0, vmsg, NULL, NULL, FALSE);
		g_free (vmsg);
		g_strfreev (args);
		return;
	}

	netserv_ui_open_kick_dialog (box->irc, GTK_WINDOW (gtk_widget_get_toplevel
													   (GTK_WIDGET (box))),
								 target->channel.name, args[0], args[1]);

	g_strfreev (args);
}


static void
parse_outgoing_topic (EscoChatBox * box,
					  GIrcTarget * target,
					  const gchar * msg)
{
	gchar *name,
	 *out = NULL;

	if (GIRC_IS_CHANNEL (target))
	{
		name = girc_convert_to_utf8 (target->channel.name, box->encoding);

		if (msg == NULL || msg[0] == '\0')
		{
			out = g_strdup_printf ("TOPIC %s", name);
		}
		else
		{
			out = g_strdup_printf ("TOPIC %s :%s", name, msg);
		}

		g_free (name);
	}
	else
	{
		gchar **args;

		/* Check if we're totally blind -- just a /topic */
		if (msg == NULL || msg[0] == '\0')
		{
			esco_viewer_append_line (ESCO_VIEWER (box->viewer),
									 ESCO_VIEWER_MESSAGE_ERROR, 0,
									 _
									 ("You can only request or set a topic for a "
									  "channel."), NULL, NULL, FALSE);
			return;
		}

		args = g_strsplit (msg, " ", 2);

		if (args[0] == NULL || args[0][0] == '\0')
		{
			esco_viewer_append_line (ESCO_VIEWER (box->viewer),
									 ESCO_VIEWER_MESSAGE_ERROR, 0,
									 _
									 ("You can only request or set a topic for a "
									  "channel."), NULL, NULL, FALSE);
			g_strfreev (args);
			return;
		}

		if (args[1] == NULL || args[1][0] == '\0')
		{
			out = g_strdup_printf ("TOPIC %s", args[0]);
		}
		else
		{
			out = g_strdup_printf ("TOPIC %s :%s", args[0], args[0]);
		}

		g_strfreev (args);
	}

	girc_client_send_raw (box->irc, out);
	g_free (out);
}


static void
parse_outgoing_nick (EscoChatBox * box,
					 GIrcTarget * target,
					 const gchar * msg)
{
	gchar *out,
	 *ptr;

	if (msg == NULL || msg[0] == '\0')
	{
		esco_viewer_append_line (ESCO_VIEWER (box->viewer),
								 ESCO_VIEWER_MESSAGE_ERROR, 0,
								 _("You must specify a new nickname."), NULL,
								 NULL, FALSE);
		return;
	}

	for (ptr = (gchar *) msg; *ptr != '\0'; ptr = g_utf8_next_char (ptr))
	{
		if (g_utf8_get_char (ptr) == ' ')
		{
			esco_viewer_append_line (ESCO_VIEWER (box->viewer),
									 ESCO_VIEWER_MESSAGE_ERROR, 0,
									 _("You can only change to one nickname."),
									 NULL, NULL, FALSE);
			return;
		}
	}

	out = g_strdup_printf ("NICK %s", msg);
	girc_client_send_raw (box->irc, out);
	g_free (out);
}


static void
parse_outgoing_whois (EscoChatBox * box,
					  GIrcTarget * target,
					  const gchar * msg)
{
	gchar *out;

	if (GIRC_IS_QUERY (target))
	{
		if (msg == NULL || msg[0] == '\0')
		{
			netserv_ui_open_whois_dialog (box->irc,
										  GTK_WINDOW (gtk_widget_get_toplevel
													  (GTK_WIDGET (box))), NULL,
										  target->query.user->nick);
			return;
		}
		else
		{
			out = g_strdup_printf ("WHOIS %s", msg);
		}
	}
	else
	{
		if (msg == NULL || msg[0] == '\0')
		{
			esco_viewer_append_line (ESCO_VIEWER (box->viewer),
									 ESCO_VIEWER_MESSAGE_ERROR, 0,
									 _("You must specify a user to retrieve "
									   "information for."), NULL, NULL, FALSE);
			return;
		}
	}

	out = g_strdup_printf ("WHOIS %s", msg);
	girc_client_send_raw (box->irc, out);
	g_free (out);
}


static void
parse_outgoing_privmsg (EscoChatBox * box,
						GIrcTarget * target,
						const gchar * msg)
{
	gchar **args;

	if (!util_str_is_not_empty (msg))
	{
		esco_viewer_append_line (ESCO_VIEWER (box->viewer),
								 ESCO_VIEWER_MESSAGE_ERROR, 0,
								 _("You can only send a message to a user or a "
								   "channel."), NULL, NULL, FALSE);
		return;
	}

	args = g_strsplit (msg, " ", 2);

	if (!util_str_is_not_empty (args[0]))
	{
		esco_viewer_append_line (ESCO_VIEWER (box->viewer),
								 ESCO_VIEWER_MESSAGE_ERROR, 0,
								 _("You can only send a message to a user or a "
								   "channel."), NULL, NULL, FALSE);
	}
	// Open a query chat if that's what we're using.
	else
	{
		EscoChatBox *real_box;

		if (!util_str_is_not_empty (args[1]))
		{
			gchar *out;

			if (girc_client_is_valid_nick (box->irc, args[0]))
			{
				const GIrcTarget *target = girc_client_open_query (box->irc, args[0]);

				real_box =
					connect_get_chat_box_for_target (connect_get_connection_netid
													 (box->irc), target);
			}
			else
			{
				real_box = box;
			}

			out = g_strdup_printf ("PRIVMSG %s :%s", args[0], args[1]);
			girc_client_send_raw (box->irc, out);
			g_free (out);

			esco_viewer_append_line (ESCO_VIEWER (real_box->viewer),
									 ESCO_VIEWER_MESSAGE_MY_MSG, 0, args[1],
									 NULL, NULL, FALSE);
		}
	}


	g_strfreev (args);
}


/* ************************** *
 *  Common IRC Alias Parsing  *
 * ************************** */


static void
parse_outgoing_me (EscoChatBox * box,
				   GIrcTarget * target,
				   const gchar * msg)
{
	gchar *target_str,
	 *out;

	if (!GIRC_IS_TARGET (target))
	{
		esco_viewer_append_line (ESCO_VIEWER (box->viewer),
								 ESCO_VIEWER_MESSAGE_ERROR, 0,
								 _("You can only send an action to a user or a "
								   "channel."), NULL, NULL, FALSE);
		return;
	}

	if (msg == NULL || msg[0] == '\0')
	{
		esco_viewer_append_line (ESCO_VIEWER (box->viewer),
								 ESCO_VIEWER_MESSAGE_ERROR, 0,
								 _
								 ("You must include an message with a /me command."),
								 NULL, NULL, FALSE);
		return;
	}

	target_str = get_target_str (target, box->encoding);

	out = g_strdup_printf ("PRIVMSG %s :\001ACTION %s\001", target_str, msg);
	g_free (target_str);

	girc_client_send_raw (box->irc, out);
	g_free (out);

	esco_viewer_append_line (ESCO_VIEWER (box->viewer),
							 ESCO_VIEWER_MESSAGE_MY_ACTION, 0, msg, NULL, NULL, FALSE);

}


/* static void
parse_outgoing_op (EscoChatBox * box,
				   GIrcTarget * target,
				   const gchar * msg)
{
	if (!GIRC_IS_TARGET (target))
	{
		return;
	}

}


static void
parse_outgoing_mode (EscoChatBox * box,
					 GIrcTarget * target,
					 const gchar * msg)
{
	if (!GIRC_IS_TARGET (target))
	{
		return;
	}

}


static void
parse_outgoing_ban (EscoChatBox * box,
					GIrcTarget * target,
					const gchar * msg)
{
	if (!GIRC_IS_TARGET (target))
	{
		return;
	}

}


static void
parse_outgoing_kickban (EscoChatBox * box,
						GIrcTarget * target,
						const gchar * msg)
{
	if (!GIRC_IS_TARGET (target))
	{
		return;
	}
} */


/* ***************** *
 *  UI-Only Parsing  *
 * ***************** */

static void
parse_outgoing_props (EscoChatBox * box,
					  GIrcTarget * target,
					  const gchar * msg)
{
	if (util_str_is_not_empty (msg))
	{
		gchar **args = NULL;
		guint i;

		args = g_strsplit (msg, " ", -1);

		for (i = 0; args[i] != NULL && args[i][0] != '\0'; i++)
		{
			gchar *vmsg = g_strdup_printf (_("Retrieving information on %s..."), args[i]);

			esco_viewer_append_line (ESCO_VIEWER (box->viewer),
									 ESCO_VIEWER_MESSAGE_STATUS, 0, vmsg, NULL,
									 NULL, FALSE);
			g_free (vmsg);

			if (girc_client_is_valid_nick (box->irc, args[i]))
			{
				netserv_ui_open_whois_dialog (box->irc,
											  GTK_WINDOW
											  (gtk_widget_get_toplevel
											   (GTK_WIDGET (box))), NULL, args[i]);
			}
			else
			{
				netserv_ui_open_channel_props_dialog (box->irc,
													  GTK_WINDOW
													  (gtk_widget_get_toplevel
													   (GTK_WIDGET (box))),
													  NULL, args[i]);
			}
		}

		g_strfreev (args);
	}
	else
	{
		if (target != NULL)
		{
			if (GIRC_IS_CHANNEL (target))
			{
				netserv_ui_open_channel_props_dialog (box->irc,
													  GTK_WINDOW
													  (gtk_widget_get_toplevel
													   (GTK_WIDGET (box))),
													  &(target->channel),
													  target->channel.name);
			}
			else
			{
				netserv_ui_open_whois_dialog (box->irc,
											  GTK_WINDOW
											  (gtk_widget_get_toplevel
											   (GTK_WIDGET (box))),
											  target->query.user,
											  target->query.user->nick);
			}
		}
		else
		{
			esco_viewer_append_line (ESCO_VIEWER (box->viewer),
									 ESCO_VIEWER_MESSAGE_ERROR, 0,
									 _("You need to specify a user or channel to view "
									   "their properties."), NULL, NULL, FALSE);
		}
	}
}


static void
parse_outgoing_clear (EscoChatBox * box,
					  GIrcTarget * target,
					  const gchar * msg)
{
	esco_chat_box_clear_viewer (box);
}


/* ************************ *
 *  Housekeeping Functions  *
 * ************************ */

static void
init_parser (void)
{
	guint i;

	if (commands == NULL)
	{
		/* Command Aliases -- Outgoing */
		static const gchar *const join_aliases[] = {
			"ENTER", NULL
		};
		static const gchar *const part_aliases[] = {
			"LEAVE", NULL
		};
		static const gchar *const privmsg_aliases[] = {
			"MSG", NULL
		};
		static const gchar *const quit_aliases[] = {
			"DISCONNECT", "DISCO", NULL
		};
		static const gchar *const props_aliases[] = {
			"PROPERTIES", "INFO", "P", NULL
		};
		static const gchar *const clear_aliases[] = {
			"CLS", NULL
		};

		/* Commands */
		static const Command command_array[] = {
			/* Channel commands */
			{"JOIN", parse_outgoing_join, join_aliases},
			{"PART", parse_outgoing_part, part_aliases},
			{"KICK", parse_outgoing_kick, NULL},
			{"TOPIC", parse_outgoing_topic, NULL},
//          {"MODE", parse_outgoing_mode, NULL},
			{"PRIVMSG", parse_outgoing_privmsg, privmsg_aliases},
			{"NOTICE", NULL, NULL},

			/* IRCop commands */
			{"OPER", NULL, NULL},
			{"SQUIT", NULL, NULL},
			{"CONNECT", NULL, NULL},

			/* Server Commands */
			{"NICK", parse_outgoing_nick, NULL},
			{"VERSION", NULL, NULL},
			{"STATS", NULL, NULL},
			{"LINKS", NULL, NULL},
			{"QUIT", NULL, quit_aliases},

			/* Misc Commands */
			{"LIST", NULL, NULL},
			{"NAMES", NULL, NULL},
			{"LUSERS", NULL, NULL},
			{"TIME", NULL, NULL},
			{"TRACE", NULL, NULL},
			{"WHO", NULL, NULL},
			{"WHOX", NULL, NULL},
			{"WHOIS", parse_outgoing_whois, NULL},

			/* Non-existant (pseudo) IRC commands */
			{"ME", parse_outgoing_me, NULL},
//          {"OP", parse_outgoing_op, NULL},
//          {"BAN", parse_outgoing_ban, NULL},
//          {"KB", parse_outgoing_kickban, NULL},

			/* UI commands */
			{"PROPS", parse_outgoing_props, props_aliases},
			{"CLEAR", parse_outgoing_clear, clear_aliases}
		};

		commands = g_hash_table_new (girc_ascii_strcasehash, girc_ascii_strcaseequal);

		for (i = 0; i < G_N_ELEMENTS (command_array); i++)
		{
			guint alias_i;

			g_hash_table_insert (commands, (gpointer) command_array[i].name,
								 (gpointer) & (command_array[i]));

			for (alias_i = 0;
				 command_array[i].aliases != NULL
				 && command_array[i].aliases[alias_i] != NULL; alias_i++)
			{
				g_hash_table_insert (commands,
									 (gpointer) command_array[i].
									 aliases[alias_i], (gpointer) & (command_array[i]));
			}
		}
	}
}


/* ********************** *
 *  Public API Functions  *
 * ********************** */

void
outgoing_send (EscoChatBox * box,
			   const gchar * data)
{
	g_return_if_fail (ESCO_IS_CHAT_BOX (box));
	g_return_if_fail (GIRC_IS_CLIENT (box->irc));
	g_return_if_fail (box->target == NULL || GIRC_IS_TARGET (box->target));
	g_return_if_fail (data != NULL && data[0] != '\0');

	init_parser ();

	if (g_utf8_get_char (data) == '/')
	{
		gchar **args = NULL;

		args = g_strsplit (g_utf8_next_char (data), " ", 2);

		if (args != NULL && args[0] != NULL)
		{
			Command *command = g_hash_table_lookup (commands, args[0]);

			if (command != NULL)
			{
				if (command->parse_outgoing != NULL)
				{
					if (args[1] != NULL && args[1][0] == '\0')
					{
						g_free (args[1]);
						args[1] = NULL;
					}

					(*command->parse_outgoing) (box, box->target, args[1]);
				}
				else
				{
					girc_client_send_raw (box->irc, g_utf8_next_char (data));
				}
			}
			else
			{
				girc_client_send_raw (box->irc, g_utf8_next_char (data));
			}
		}
		else
		{
			esco_viewer_append_line (ESCO_VIEWER (box->viewer),
									 ESCO_VIEWER_MESSAGE_ERROR, 0,
									 _("There was no message to send."), NULL,
									 NULL, FALSE);
		}

		g_strfreev (args);
	}
	else if (box->target != NULL)
	{
		gchar *msg,
		 *target_str;

		target_str = get_target_str (box->target, box->encoding);

		msg = g_strdup_printf ("PRIVMSG %s :%s", target_str, data);
		g_free (target_str);

		girc_client_send_raw (box->irc, msg);
		g_free (msg);

		esco_viewer_append_line (ESCO_VIEWER (box->viewer),
								 ESCO_VIEWER_MESSAGE_MY_MSG, 0, data, NULL, NULL, FALSE);
	}
}
