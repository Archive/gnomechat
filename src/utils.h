/*
 *  GnomeChat: src/utils.h
 *
 *  Copyright (c) 2002 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __UTILS_H__
#define __UTILS_H__

#include <gtk/gtkwindow.h>
#include <gtk/gtktreeselection.h>
#include <gtk/gtktogglebutton.h>

#include "prefs.h"


#define DEBUG_TRUEFALSE_STR(var) (var ? "TRUE" : "TRUE")


typedef enum
{
	LOCALE_MATCH_NONE,
	LOCALE_MATCH_LANGUAGE,
	LOCALE_MATCH_LANGUAGE_TERRITORY,
	LOCALE_MATCH_LANGUAGE_TERRITORY_ENCODING,
	LOCALE_MATCH_LANGUAGE_TERRITORY_ENCODING_MODIFIER
}
LocaleMatch;


gboolean util_str_is_not_empty (const gchar * str);

gint util_get_font_height (GtkWidget * widget);
gint util_get_font_width (GtkWidget * widget);

void util_set_tooltip (GtkWidget * widget,
					   const gchar * tip,
					   const gchar * long_tip,
					   gboolean set_atk_name_from_tip);
void util_set_label_widget_pair (GtkWidget * label,
								 GtkWidget * widget);

gchar *util_gconf_key_directory (const gchar * path);
G_CONST_RETURN gchar *util_gconf_key_key (const gchar * path);

void util_scroll_tree_view_to_selection (GtkTreeSelection * sel);

void util_set_window_icon_from_stock (GtkWindow * window,
									  const gchar * stock_id);

void util_g_list_deep_free (GList * list,
							GFreeFunc free_func);
void util_show_again_toggled_cb (GtkToggleButton * button,
								 GnomechatAlertID alert);

LocaleMatch util_locale_match (const gchar * locale1,
							   const gchar * locale2);

void util_object_weak_notify (gpointer ** nullify,
							  GObject * where_object_was);

void util_set_save_window_size (GtkWidget * window,
								GnomechatWindowID id);

void util_boldify_label (GtkWidget * label);

gchar *util_get_name_from_uri (const gchar * uri);

#endif /* __UTILS_H__ */
