/*
 *  GnomeChat: src/netserv-ui.h
 *
 *  Copyright (c) 2002 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __NETSERV_UI_H__
#define __NETSERV_UI_H__


#include <libgircclient/girc-client.h>
#include <gtk/gtkwindow.h>


void netserv_ui_open_whois_dialog (GIrcClient * irc,
								   GtkWindow * parent,
								   GIrcUser * user,
								   const gchar * nick);

void netserv_ui_open_motd_dialog (GIrcClient * irc,
								  GtkWindow * parent,
								  const gchar * servername,
								  const gchar * motd);

void netserv_ui_open_channel_props_dialog (GIrcClient * irc,
										   GtkWindow * parent,
										   GIrcChannel * channel,
										   const gchar * name);
void netserv_ui_update_channel_props_dialog (GIrcClient * irc,
											 GIrcChannel * channel,
											 const gchar * name,
											 GIrcChannelInfoType info);

void netserv_ui_open_kick_dialog (GIrcClient * irc,
								  GtkWindow * parent,
								  const gchar * channel,
								  const gchar * nick,
								  const gchar * reason);


#endif /* __NETSERV_UI_H__ */
