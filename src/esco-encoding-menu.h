/*
 *  GnomeChat: src/esco-encoding-picker.h
 *
 *  Copyright (c) 2002 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __ESCO_ENCODING_MENU_H__
#define __ESCO_ENCODING_MENU_H__


#include <gtk/gtkmenu.h>


#define ESCO_TYPE_ENCODING_MENU				(esco_encoding_menu_get_type ())
#define ESCO_ENCODING_MENU(obj)				(G_TYPE_CHECK_INSTANCE_CAST ((obj), ESCO_TYPE_ENCODING_MENU, EscoEncodingMenu))
#define ESCO_ENCODING_MENU_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST ((klass), ESCO_TYPE_ENCODING_MENU, EscoEncodingMenuClass))
#define ESCO_IS_ENCODING_MENU(obj)			(G_TYPE_CHECK_INSTANCE_TYPE ((obj), ESCO_TYPE_ENCODING_MENU))
#define ESCO_IS_ENCODING_MENU_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), ESCO_TYPE_ENCODING_MENU))
#define ESCO_ENCODING_MENU_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS ((obj), ESCO_TYPE_ENCODING_MENU, EscoEncodingMenuClass))


typedef struct _EscoEncodingMenu EscoEncodingMenu;
typedef struct _EscoEncodingMenuPrivate EscoEncodingMenuPrivate;
typedef struct _EscoEncodingMenuClass EscoEncodingMenuClass;

typedef void (*EscoEncodingMenuSignalFunc) (EscoEncodingMenu * menu);


struct _EscoEncodingMenu
{
	GtkMenu parent;

	EscoEncodingMenuPrivate *_priv;
};

struct _EscoEncodingMenuClass
{
	GtkMenuClass parent_class;

	EscoEncodingMenuSignalFunc dialog_opened;
	EscoEncodingMenuSignalFunc help_clicked;
	EscoEncodingMenuSignalFunc changed;
};


GType esco_encoding_menu_get_type (void);

GtkWidget *esco_encoding_menu_new (const gchar *title);

G_CONST_RETURN gchar *esco_encoding_menu_get_encoding (EscoEncodingMenu * menu);
void esco_encoding_menu_set_encoding (EscoEncodingMenu * menu,
									  const gchar * encoding);

G_CONST_RETURN gchar *esco_encoding_menu_get_title (EscoEncodingMenu * menu);
void esco_encoding_menu_set_title (EscoEncodingMenu * menu,
								   const gchar * title);

gboolean esco_encoding_menu_get_modal (EscoEncodingMenu * menu);
void esco_encoding_menu_set_modal (EscoEncodingMenu * menu,
								   gboolean modal);

/* Only != NULL when the dialog is visible */
GtkWidget *esco_encoding_menu_get_dialog (EscoEncodingMenu *menu);

#endif /* __ESCO_ENCODING_MENU_H__ */
