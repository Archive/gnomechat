/*
 *  GnomeChat: src/stock-items.h
 *
 *  Copyright (c) 2002 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __STOCK_ITEMS_H__
#define __STOCK_ITEMS_H__

#include <gtk/gtkwidget.h>


#define GNOMECHAT_STOCK_ICON				PACKAGE "-icon"
#define GNOMECHAT_STOCK_MY_USER				PACKAGE "-my-user"
#define GNOMECHAT_STOCK_VCARD				PACKAGE "-vcard"

#define GNOMECHAT_STOCK_GLOBE				PACKAGE "-globe"

#define GNOMECHAT_STOCK_CONNECT				PACKAGE "-connect"
#define GNOMECHAT_STOCK_ENCODING			PACKAGE "-encoding"

#define GNOMECHAT_STOCK_DISCONNECT			PACKAGE "-disconnect"
#define GNOMECHAT_STOCK_EDIT				PACKAGE "-edit"
#define GNOMECHAT_STOCK_INFO				PACKAGE "-info"
#define GNOMECHAT_STOCK_JOIN				PACKAGE "-join"

#define GNOMECHAT_STOCK_NETWORK				PACKAGE "-network"
#define GNOMECHAT_STOCK_SERVER				PACKAGE "-server"
#define GNOMECHAT_STOCK_CHANNEL				PACKAGE "-channel"
#define GNOMECHAT_STOCK_QUERY				PACKAGE "-query"
#define GNOMECHAT_STOCK_USER				PACKAGE "-user"

#define GNOMECHAT_STOCK_CATEGORY_LARGE		GNOMECHAT_STOCK_GLOBE
#define GNOMECHAT_STOCK_CATEGORY_SMALL		GNOMECHAT_STOCK_GLOBE
#define GNOMECHAT_STOCK_CATEGORY_REGIONAL	PACKAGE "-category-regional"
#define GNOMECHAT_STOCK_CATEGORY_CUSTOM		GNOMECHAT_STOCK_MY_USER
#define GNOMECHAT_STOCK_CATEGORY_INTEREST	PACKAGE "-category-interest"

#define GNOMECHAT_STOCK_SENDFILE			PACKAGE "-send-file"
#define GNOMECHAT_STOCK_KICK				PACKAGE "-kick"

#define GNOMECHAT_STOCK_SERVER_PROPERTIES	PACKAGE "-server-properties"
#define GNOMECHAT_STOCK_CHANNEL_PROPERTIES	PACKAGE "-channel-properties"
#define GNOMECHAT_STOCK_USER_PROPERTIES		PACKAGE "-user-properties"

#define GNOMECHAT_STOCK_STATUS_NORMAL		PACKAGE "-status-normal"
#define GNOMECHAT_STOCK_STATUS_OP			PACKAGE "-status-op"
#define GNOMECHAT_STOCK_STATUS_VOICE		PACKAGE "-status-voice"
#define GNOMECHAT_STOCK_STATUS_OP_VOICE		PACKAGE "-status-op-voice"
#define GNOMECHAT_STOCK_STATUS_IRCOP		PACKAGE "-status-ircop"

#define GNOMECHAT_STOCK_SMILEY_REGULAR		PACKAGE "-smiley-:)"
#define GNOMECHAT_STOCK_SMILEY_BIG			PACKAGE "-smiley-:>"
#define GNOMECHAT_STOCK_SMILEY_WINK			PACKAGE "-smiley-;)"
#define GNOMECHAT_STOCK_SMILEY_CRY			PACKAGE "-smiley-P)"
#define GNOMECHAT_STOCK_SMILEY_FROWN		PACKAGE "-smiley-:("
#define GNOMECHAT_STOCK_SMILEY_SMIRK		PACKAGE "-smiley-:/"
#define GNOMECHAT_STOCK_SMILEY_TONGUE		PACKAGE "-smiley-:P"
#define GNOMECHAT_STOCK_SMILEY_LAUGH		PACKAGE "-smiley-:D"
#define GNOMECHAT_STOCK_SMILEY_SHOCK		PACKAGE "-smiley-:o"
#define GNOMECHAT_STOCK_SMILEY_NONE			PACKAGE "-smiley-:|"


void stock_init (void);

G_CONST_RETURN gchar *stock_get_id_from_file (const gchar * filename);

GList *stock_get_icon_list_from_stock (GtkWidget *widget, const gchar * stock_id);

#endif /* __STOCK_ITEMS_H__ */
