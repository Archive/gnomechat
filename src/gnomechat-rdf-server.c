/*
 *  GnomeChat: src/gnomechat-rdf-server.c
 *
 *  Copyright (c) 2003 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.01234567890
 */

#include "gnomechat.h"

#include <locale.h>

#include "gnomechat-rdf-server.h"

#include "gnomechat-rdf-types.h"
#include "gnomechat-vfs-transfer.h"
#include "utils.h"

#include <libgircclient/girc-utils.h>
#include <libgnomeui/gnome-thumbnail.h>


enum
{
	PROP_0,
	URI,
	DATA_URI,
	DATA_PATH,
	NAME,
	ENCODING,
	PORTS
};


struct _GnomechatRdfServerPrivate
{
	gchar *uri;
	gchar *data_uri;
	gchar *data_path;

	GnomechatI18nString *name;
	GSList *ports;
	gchar *encoding;

	/* Internal */
	GnomechatVFSTransfer *data_xfer;
	gboolean data_is_local:1;
};


static gpointer parent_class = NULL;


/* ********************************** *
 *  GnomechatTransferIface Functions  *
 * ********************************** */

static GnomechatTransferType
gnomechat_rdf_server_get_transfer_type (GnomechatRdfServer * server)
{
	g_return_val_if_fail (GNOMECHAT_IS_RDF_SERVER (server), GNOMECHAT_TRANSFER_INVALID);

	return (GNOMECHAT_TRANSFER_DOWNLOAD | GNOMECHAT_TRANSFER_MULTI);
}


static GnomechatTransferStatus
gnomechat_rdf_server_get_status (GnomechatRdfServer * server)
{
	g_return_val_if_fail (GNOMECHAT_IS_RDF_SERVER (server), GNOMECHAT_TRANSFER_STATUS_INVALID);

	if (server->_priv->data_xfer != NULL)
		return gnomechat_transfer_get_status (GNOMECHAT_TRANSFER (server->_priv->data_xfer));
	else
		return GNOMECHAT_TRANSFER_STATUS_READY;
}


static GnomeVFSResult
gnomechat_rdf_server_get_error (GnomechatRdfServer * server)
{
	g_return_val_if_fail (GNOMECHAT_IS_RDF_SERVER (server), GNOME_VFS_ERROR_BAD_PARAMETERS);

	if (server->_priv->data_xfer != NULL)
		return gnomechat_transfer_get_error (GNOMECHAT_TRANSFER (server->_priv->data_xfer));
	else
		return GNOME_VFS_OK;
}


static GnomeVFSURI *
gnomechat_rdf_server_get_from_uri (GnomechatRdfServer * server)
{
	g_return_val_if_fail (GNOMECHAT_IS_RDF_SERVER (server), NULL);

	if (server->_priv->data_xfer != NULL)
		return gnomechat_transfer_get_from_uri (GNOMECHAT_TRANSFER (server->_priv->data_xfer));
	else
		return NULL;
}


static GnomeVFSURI *
gnomechat_rdf_server_get_to_uri (GnomechatRdfServer * server)
{
	g_return_val_if_fail (GNOMECHAT_IS_RDF_SERVER (server), NULL);

	if (server->_priv->data_xfer != NULL)
		return gnomechat_transfer_get_to_uri (GNOMECHAT_TRANSFER (server->_priv->data_xfer));
	else
		return NULL;
}


static GnomeVFSFileSize
gnomechat_rdf_server_get_file_size (GnomechatRdfServer * server)
{
	g_return_val_if_fail (GNOMECHAT_IS_RDF_SERVER (server), 0);

	if (server->_priv->data_xfer != NULL)
		return gnomechat_transfer_get_file_size (GNOMECHAT_TRANSFER (server->_priv->data_xfer));
	else
		return 0;
}


static GnomeVFSFileSize
gnomechat_rdf_server_get_bytes_copied (GnomechatRdfServer * server)
{
	g_return_val_if_fail (GNOMECHAT_IS_RDF_SERVER (server), 0);

	if (server->_priv->data_xfer != NULL)
		return gnomechat_transfer_get_bytes_copied (GNOMECHAT_TRANSFER (server->_priv->data_xfer));
	else
		return 0;
}


static void
gnomechat_rdf_server_start_transfer (GnomechatRdfServer * server)
{
	g_return_if_fail (GNOMECHAT_IS_RDF_SERVER (server));

	if (server->_priv->data_xfer != NULL)
		gnomechat_transfer_start_transfer (GNOMECHAT_TRANSFER (server->_priv->data_xfer));
}


static void
gnomechat_rdf_server_cancel_transfer (GnomechatRdfServer * server)
{
	g_return_if_fail (GNOMECHAT_IS_RDF_SERVER (server));

	if (server->_priv->data_xfer != NULL)
		gnomechat_transfer_cancel_transfer (GNOMECHAT_TRANSFER (server->_priv->data_xfer));
}


static gulong
gnomechat_rdf_server_get_n_files (GnomechatRdfServer * server)
{
	g_return_val_if_fail (GNOMECHAT_IS_RDF_SERVER (server), 0);

	return !(server->_priv->data_is_local);
}


static gulong
gnomechat_rdf_server_get_file_index (GnomechatRdfServer * server)
{
	g_return_val_if_fail (GNOMECHAT_IS_RDF_SERVER (server), 0);

	if (server->_priv->data_xfer != NULL)
		return 1;
	else
		return 0;
}


/* ********************** *
 *  GInterface Functions  *
 * ********************** */

static void
gnomechat_rdf_server_xfer_iface_init (GnomechatTransferIface * iface)
{
	iface->get_transfer_type =
		(GnomechatTransferGetTypeFunc) gnomechat_rdf_server_get_transfer_type;
	iface->get_status = (GnomechatTransferGetStatusFunc) gnomechat_rdf_server_get_status;
	iface->get_error = (GnomechatTransferGetResultFunc) gnomechat_rdf_server_get_error;

	iface->get_from_uri = (GnomechatTransferGetURIFunc) gnomechat_rdf_server_get_from_uri;
	iface->set_from_uri = NULL;

	iface->get_to_uri = (GnomechatTransferGetURIFunc) gnomechat_rdf_server_get_to_uri;
	iface->set_to_uri = NULL;

	iface->get_file_size = (GnomechatTransferGetSizeFunc) gnomechat_rdf_server_get_file_size;
	iface->get_bytes_copied = (GnomechatTransferGetSizeFunc) gnomechat_rdf_server_get_bytes_copied;

	iface->start_transfer = (GnomechatTransferFunc) gnomechat_rdf_server_start_transfer;
	iface->cancel_transfer = (GnomechatTransferFunc) gnomechat_rdf_server_cancel_transfer;

	iface->get_n_files = (GnomechatTransferGetULongFunc) gnomechat_rdf_server_get_n_files;
	iface->get_file_index = (GnomechatTransferGetULongFunc) gnomechat_rdf_server_get_file_index;
}


/* ******************* *
 *  GObject Functions  *
 * ******************* */

static void
gnomechat_rdf_server_set_property (GObject * object, guint param_id, const GValue * value,
								   GParamSpec * pspec)
{
	GnomechatRdfServer *server = GNOMECHAT_RDF_SERVER (object);

	switch (param_id)
	{
	case URI:
		g_free (server->_priv->uri);
		server->_priv->uri = g_value_dup_string (value);
		g_object_notify (object, "uri");
		break;

	case NAME:
		gnomechat_i18n_string_free (server->_priv->name);
		server->_priv->name = g_value_dup_boxed (value);
		g_object_notify (object, "name");
		break;

	case ENCODING:
		g_free (server->_priv->encoding);
		server->_priv->encoding = g_value_dup_string (value);
		g_object_notify (object, "encoding");
		break;

	case PORTS:
		{
			GValueArray *array = g_value_get_boxed (value);
			guint i;

			g_slist_free (server->_priv->ports);

			if (array == NULL)
			{
				server->_priv->ports = NULL;
				return;
			}

			for (i = 0; i < array->n_values; i++)
			{
				server->_priv->ports =
					g_slist_append (server->_priv->ports,
									GUINT_TO_POINTER (g_value_get_uint
													  (g_value_array_get_nth (array, i))));
			}

			g_object_notify (object, "ports");
		}
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}


static void
gnomechat_rdf_server_get_property (GObject * object, guint param_id, GValue * value,
								   GParamSpec * pspec)
{
	GnomechatRdfServer *server = GNOMECHAT_RDF_SERVER (object);

	switch (param_id)
	{
	case URI:
		g_value_set_string (value, server->_priv->uri);
		break;
	case DATA_URI:
		g_value_set_string (value, server->_priv->data_uri);
		break;
	case DATA_PATH:
		g_value_set_string (value, server->_priv->data_path);
		break;

	case NAME:
		g_value_set_boxed (value, server->_priv->name);
		break;

	case ENCODING:
		g_value_set_string (value, server->_priv->encoding);
		break;

	case PORTS:
		girc_g_value_set_value_array_from_slist (value, server->_priv->ports, G_TYPE_UINT);
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}

static void
gnomechat_rdf_server_dispose (GObject * object)
{
	GnomechatRdfServer *server = GNOMECHAT_RDF_SERVER (object);

	if (server->_priv->data_xfer != NULL)
		g_object_unref (server->_priv->data_xfer);

	if (G_OBJECT_CLASS (parent_class)->dispose != NULL)
		(*G_OBJECT_CLASS (parent_class)->dispose) (object);
}


static void
gnomechat_rdf_server_finalize (GObject * object)
{
	GnomechatRdfServer *server = GNOMECHAT_RDF_SERVER (object);

	g_free (server->_priv->uri);
	g_free (server->_priv->data_uri);
	g_free (server->_priv->data_path);
	g_free (server->_priv->encoding);

	gnomechat_i18n_string_free (server->_priv->name);

	g_slist_free (server->_priv->ports);

	g_free (server->_priv);

	if (G_OBJECT_CLASS (parent_class)->finalize != NULL)
		(*G_OBJECT_CLASS (parent_class)->finalize) (object);
}


/* ***************** *
 *  GType Functions  *
 * ***************** */

static void
gnomechat_rdf_server_class_init (GnomechatRdfServerClass * class)
{
	GObjectClass *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	object_class->set_property = gnomechat_rdf_server_set_property;
	object_class->get_property = gnomechat_rdf_server_get_property;
	object_class->dispose = gnomechat_rdf_server_dispose;
	object_class->finalize = gnomechat_rdf_server_finalize;

	g_object_class_install_property (object_class, URI,
									 g_param_spec_string ("uri", "IRC Server URI",
														  "The URI for this server.", NULL,
														  (G_PARAM_READWRITE | G_PARAM_CONSTRUCT)));

	g_object_class_install_property (object_class, DATA_URI,
									 g_param_spec_string ("data-uri", "Data URI",
														  "The URI for this server's RDF data.",
														  NULL, G_PARAM_READABLE));
	g_object_class_install_property (object_class, DATA_PATH,
									 g_param_spec_string ("data-path", "Local Data Path",
														  "The local path for this server's RDF "
														  "data (downloaded from data-uri).",
														  NULL, G_PARAM_READABLE));

	g_object_class_install_property (object_class, NAME,
									 g_param_spec_boxed ("name", "Server Name",
														 "A friendly name for this server.",
														 GNOMECHAT_TYPE_I18N_STRING,
														 G_PARAM_READWRITE));

	g_object_class_install_property (object_class, ENCODING,
									 g_param_spec_string ("encoding", "Character Encoding",
														  "The character encoding used by this "
														  "server.", NULL, G_PARAM_READWRITE));

	g_object_class_install_property (object_class, PORTS,
									 g_param_spec_value_array
									 ("ports", "Available Ports",
									  "A value array of port numbers which can be connected to.",
									  g_param_spec_uint ("port", "A Port",
														 "An individual port number.",
														 0, 65535, 0,
														 G_PARAM_READWRITE), G_PARAM_READWRITE));
}


static void
gnomechat_rdf_server_instance_init (GnomechatRdfServer * server)
{
	server->_priv = g_new0 (GnomechatRdfServerPrivate, 1);

	server->_priv->data_xfer = NULL;

	server->_priv->data_uri = NULL;
	server->_priv->data_path = NULL;
	server->_priv->name = NULL;
	server->_priv->encoding = NULL;
	server->_priv->ports = NULL;
}


/* ************************************************************************** *
 *  PUBLIC API                                                                *
 * ************************************************************************** */


/* *********************** *
 *  Constructor Functions  *
 * *********************** */

GType
gnomechat_rdf_server_get_type (void)
{
	static GType type = G_TYPE_INVALID;

	if (type == G_TYPE_INVALID)
	{
		static const GTypeInfo info = {
			sizeof (GnomechatRdfServerClass),
			NULL,				/* base_init */
			NULL,				/* base_finalize */
			(GClassInitFunc) gnomechat_rdf_server_class_init,
			NULL,				/* class_finalize */
			NULL,				/* class_data */
			sizeof (GnomechatRdfServer),
			0,					/* n_preallocs */
			(GInstanceInitFunc) gnomechat_rdf_server_instance_init,
		};
		static const GInterfaceInfo xfer_iface_info = {
			(GInterfaceInitFunc) gnomechat_rdf_server_xfer_iface_init,
			NULL,
			NULL
		};

		type = g_type_register_static (G_TYPE_OBJECT, "GnomechatRdfServer", &info, 0);

		g_type_add_interface_static (type, GNOMECHAT_TYPE_TRANSFER, &xfer_iface_info);
	}

	return type;
}


GObject *
gnomechat_rdf_server_new (const gchar * uri)
{
	return g_object_new (GNOMECHAT_TYPE_RDF_SERVER, "uri", uri, NULL);
}


/* ************************ *
 *  URI Property Functions  *
 * ************************ */

G_CONST_RETURN gchar *
gnomechat_rdf_server_get_uri (GnomechatRdfServer * server)
{
	g_return_val_if_fail (GNOMECHAT_IS_RDF_SERVER (server), NULL);

	return server->_priv->uri;
}


void
gnomechat_rdf_server_set_uri (GnomechatRdfServer * server, const gchar * uri)
{
	g_return_if_fail (GNOMECHAT_IS_RDF_SERVER (server));
	g_return_if_fail (uri != NULL && uri[0] != '\0');

	g_free (server->_priv->uri);
	server->_priv->uri = g_strdup (uri);

	g_object_notify (G_OBJECT (server), "uri");
}


/* ***************************** *
 *  Data URI Property Functions  *
 * ***************************** */

G_CONST_RETURN gchar *
gnomechat_rdf_server_get_data_uri (GnomechatRdfServer * server)
{
	g_return_val_if_fail (GNOMECHAT_IS_RDF_SERVER (server), NULL);

	return server->_priv->data_uri;
}


/* Data Transfer Callbacks */
static void
data_xfer_status_changed_cb (GnomechatTransfer * data_xfer, GnomechatTransferStatus status,
							 GnomechatRdfServer * server)
{
	switch (status)
	{
	case GNOMECHAT_TRANSFER_STATUS_CHECKING:
	case GNOMECHAT_TRANSFER_STATUS_TRANSFERRING:
	case GNOMECHAT_TRANSFER_STATUS_ERROR:
		break;

	case GNOMECHAT_TRANSFER_STATUS_DONE:
		{
			GnomeVFSURI *to_uri = gnomechat_transfer_get_to_uri (data_xfer);

			g_object_unref (server->_priv->data_xfer);

			g_free (server->_priv->data_path);
			server->_priv->data_path =
				gnome_vfs_uri_to_string (to_uri, GNOME_VFS_URI_HIDE_TOPLEVEL_METHOD);
			gnome_vfs_uri_unref (to_uri);

			g_object_notify (G_OBJECT (server), "data-path");
		}
		break;

	default:
		g_return_if_reached ();
		break;
	}

	gnomechat_transfer_status_changed (GNOMECHAT_TRANSFER (server), status);
}


static void
data_xfer_progress_cb (GnomechatTransfer * data_xfer, GnomeVFSFileSize bytes_completed,
					   GnomechatRdfServer * server)
{
	gnomechat_transfer_progress (GNOMECHAT_TRANSFER (server), bytes_completed);
}


static void
data_xfer_cancelled_cb (GnomechatTransfer * data_xfer, GnomechatRdfServer * server)
{
	g_object_unref (data_xfer);

	gnomechat_transfer_cancelled (GNOMECHAT_TRANSFER (server));
}


void
gnomechat_rdf_server_set_data_uri (GnomechatRdfServer * server, const gchar * data_uri)
{
	GnomeVFSURI *from_uri;

	g_return_if_fail (GNOMECHAT_IS_RDF_SERVER (server));
	g_return_if_fail (data_uri != NULL && data_uri[0] != '\0');

	from_uri = gnome_vfs_uri_new (data_uri);

	g_return_if_fail (from_uri != NULL);

	/* Clean out the old data URI stuff */
	if (server->_priv->data_xfer != NULL)
	{
		gnomechat_transfer_cancel_transfer (GNOMECHAT_TRANSFER (server->_priv->data_xfer));
	}
	g_free (server->_priv->data_uri);

	/* Fill in the new URI */
	server->_priv->data_uri = g_strdup (data_uri);

	g_object_notify (G_OBJECT (server), "data-uri");

	/* If we're dealing with a remote file, setup & start the transfer to a cache file */
	if (!gnome_vfs_uri_is_local (from_uri))
	{
		GnomeVFSURI *to_uri;
		gchar *md5,
		 *to_uri_text;

		server->_priv->data_is_local = FALSE;

		md5 = gnome_thumbnail_md5 (data_uri);
		to_uri_text = g_strconcat ("file://", gnomechat_get_cache_dir (), "/", md5, NULL);
		g_free (md5);

		to_uri = gnome_vfs_uri_new (to_uri_text);
		g_free (to_uri_text);

		g_return_if_fail (to_uri != NULL);

		server->_priv->data_xfer = gnomechat_vfs_transfer_new (from_uri, to_uri, TRUE);
		g_object_add_weak_pointer (G_OBJECT (server->_priv->data_xfer),
								   (gpointer *) & (server->_priv->data_xfer));
		g_signal_connect (server->_priv->data_xfer, "status-changed",
						  G_CALLBACK (data_xfer_status_changed_cb), server);
		g_signal_connect (server->_priv->data_xfer, "progress",
						  G_CALLBACK (data_xfer_progress_cb), server);
		g_signal_connect (server->_priv->data_xfer, "cancelled",
						  G_CALLBACK (data_xfer_cancelled_cb), server);

		gnome_vfs_uri_unref (to_uri);

		gnomechat_transfer_start_transfer (GNOMECHAT_TRANSFER (server->_priv->data_xfer));
	}
	/* Otherwise, we're dealing with a local file, so skip to the end part */
	else
	{
		g_free (server->_priv->data_path);
		server->_priv->data_path = g_strdup (data_uri);

		g_object_notify (G_OBJECT (server), "data-path");
	}

	gnome_vfs_uri_unref (from_uri);
}


G_CONST_RETURN gchar *
gnomechat_rdf_server_get_data_path (GnomechatRdfServer * server)
{
	g_return_val_if_fail (GNOMECHAT_IS_RDF_SERVER (server), NULL);

	return server->_priv->data_path;
}


/* ************************* *
 *  Name Property Functions  *
 * ************************* */

G_CONST_RETURN gchar *
gnomechat_rdf_server_get_name (GnomechatRdfServer * server)
{
	g_return_val_if_fail (GNOMECHAT_IS_RDF_SERVER (server), NULL);

	return (server->_priv->name != NULL ? server->_priv->name->str : NULL);
}


void
gnomechat_rdf_server_try_name (GnomechatRdfServer * server, const gchar * name, const gchar * lang)
{
	g_return_if_fail (GNOMECHAT_IS_RDF_SERVER (server));

	/* Un-setter */
	if (name == NULL)
	{
		gnomechat_i18n_string_free (server->_priv->name);
		server->_priv->name = NULL;
		return;
	}

	if (server->_priv->name != NULL)
	{
		const gchar *locale = setlocale (LC_MESSAGES, NULL);
		LocaleMatch old_match = util_locale_match (server->_priv->name->lang, locale),
			new_match = util_locale_match (lang, locale);

		/* Existing name is better for this locale. */
		if (new_match < old_match)
			return;

		gnomechat_i18n_string_free (server->_priv->name);
	}

	server->_priv->name = gnomechat_i18n_string_new (name, lang);
	g_object_notify (G_OBJECT (server), "name");
}


/* ***************************** *
 *  Encoding Property Functions  *
 * ***************************** */

G_CONST_RETURN gchar *
gnomechat_rdf_server_get_encoding (GnomechatRdfServer * server)
{
	g_return_val_if_fail (GNOMECHAT_IS_RDF_SERVER (server), NULL);

	return server->_priv->encoding;
}


void
gnomechat_rdf_server_set_encoding (GnomechatRdfServer * server, const gchar * encoding)
{
	g_return_if_fail (GNOMECHAT_IS_RDF_SERVER (server));
	g_return_if_fail (encoding != NULL && encoding[0] != '\0');

	g_free (server->_priv->encoding);
	server->_priv->encoding = g_strdup (encoding);

	g_object_notify (G_OBJECT (server), "encoding");
}


/* ************************** *
 *  Ports Property Functions  *
 * ************************** */

GSList *
gnomechat_rdf_server_get_ports (GnomechatRdfServer * server)
{
	g_return_val_if_fail (GNOMECHAT_IS_RDF_SERVER (server), NULL);

	return server->_priv->ports;
}


void
gnomechat_rdf_server_append_port (GnomechatRdfServer * server, guint port)
{
	g_return_if_fail (GNOMECHAT_IS_RDF_SERVER (server));
	g_return_if_fail (port >= 0 && port <= 65535);

	server->_priv->ports = g_slist_append (server->_priv->ports, GUINT_TO_POINTER (port));

	g_object_notify (G_OBJECT (server), "ports");
}


void
gnomechat_rdf_server_remove_port (GnomechatRdfServer * server, guint port)
{
	g_return_if_fail (GNOMECHAT_IS_RDF_SERVER (server));
	g_return_if_fail (port >= 0 && port <= 65535);

	server->_priv->ports = g_slist_remove (server->_priv->ports, GUINT_TO_POINTER (server));

	g_object_notify (G_OBJECT (server), "ports");
}
