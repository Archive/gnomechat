/*
 *  GnomeChat: src/about.c
 *
 *  Copyright (c) 2002 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "gnomechat.h"

#include "stock-items.h"
#include "utils.h"
#include "pixbufs.h"

#include <gtk/gtkhbox.h>
#include <libgnomeui/gnome-about.h>
#include <libgnomeui/gnome-href.h>


/* I'll bet you're expecting to find some easter eggs here, aren't you...
   Tsk tsk tsk. :-) */


void
about_show_about_window (GtkWindow * parent)
{
	static GtkWindow *win = NULL;
	GdkPixbuf *pixbuf;
	GtkWidget *hbox,
	 *href;

	const gchar *authors[] = {
		"James M. Cape",
		NULL
	};
	const gchar *documentors[] = {
		NULL
	};
	const gchar *translator_credits = NULL;

	if (win != NULL)
	{
		gtk_window_present (win);
		return;
	}

	pixbuf = pixbuf_load_file (PACKAGE "/about.jpg");
	win = GTK_WINDOW (gnome_about_new (wmclass, VERSION,
									   _("Copyright 2002, 2003 James M. Cape."),
									   _("I ain't gonna work for Maggie's brother no "
										 "more!"),
									   authors, documentors, translator_credits, pixbuf));
	g_object_unref (pixbuf);

	gtk_window_set_transient_for (win, parent);
	gtk_window_set_destroy_with_parent (win, TRUE);
	util_set_window_icon_from_stock (win, GNOMECHAT_STOCK_ICON);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (win)->vbox), hbox, FALSE, FALSE, 5);
	gtk_widget_show (hbox);

	href = gnome_href_new ("http://anarchyfaq.org/", _("An Anarchist FAQ"));
	gtk_container_add (GTK_CONTAINER (hbox), href);
	gtk_widget_show (href);

	href = gnome_href_new ("http://ignore-your.tv/software/gnomechat/",
						   _("GnomeChat Website"));
	gtk_container_add (GTK_CONTAINER (hbox), href);
	gtk_widget_show (href);

	g_object_add_weak_pointer (G_OBJECT (win), (gpointer *) &win);

	gtk_window_present (win);
}
