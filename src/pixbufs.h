/*
 *  GnomeChat: src/pixbufs.h
 *
 *  Copyright (c) 2002 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __PIXBUFS_H__
#define __PIXBUFS_H__


#include <glib.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gtk/gtkwindow.h>
#include <gtk/gtkiconfactory.h>


#define GNOMECHAT_WATERMARK_FILE			PACKAGE "/watermark.png"

#define GNOMECHAT_DISK_ANIM_FILE			PACKAGE "/disk_anim.png"

#define GNOMECHAT_GLOBE_ANIM_FILE			PACKAGE "/globe_anim.png"
#define GNOMECHAT_GLOBE_ANIM_N_FRAMES		20

#define GNOMECHAT_DOWNLOAD_ANIM_FILE		PACKAGE "/download_anim.png"
#define GNOMECHAT_DOWNLOAD_ANIM_N_FRAMES	20

#define GNOMECHAT_CHANPROPS_ANIM_FILE		PACKAGE "/channel_props_anim.png"
#define GNOMECHAT_CHANPROPS_ANIM_N_FRAMES	20

#define GNOMECHAT_USERPROPS_ANIM_FILE		PACKAGE "/user_props_anim.png"
#define GNOMECHAT_USERPROPS_ANIM_N_FRAMES	20

#define GNOMECHAT_UPLOAD_ANIM_FILE			PACKAGE "/upload_anim.png"


GdkPixbuf *pixbuf_load_file (const gchar * filename);

GtkWidget *pixbuf_anim_from_file (const gchar * filename,
								  guint n_frames);
void pixbuf_anim_update (GtkWidget * image);

GdkPixbuf *pixbuf_scale_to_stock_size (GdkPixbuf * src,
									   GtkIconSize size);


#endif /* __PIXBUFS_H__ */
