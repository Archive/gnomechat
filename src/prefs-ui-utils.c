/*
 *  GnomeChat: src/prefs-ui-utils.c
 *
 *  Copyright (c) 2002 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "gnomechat.h"
#include "prefs-ui-utils.h"

#include "prefs.h"
#include "stock-items.h"
#include "utils.h"

#include <gtk/gtkhbox.h>
#include <gtk/gtkimage.h>
#include <gtk/gtklabel.h>
#include <gtk/gtkmain.h>
#include <gtk/gtkmenuitem.h>
#include <gtk/gtksizegroup.h>

#include <string.h>


GtkWidget *
prefs_ui_util_get_network_menu (void)
{
	GtkWidget *menu,
	 *item,
	 *hbox,
	 *label,
	 *image;
	GSList *ids;
	GtkSizeGroup *group;
	gchar *icon;

	menu = gtk_menu_new ();

	group = gtk_size_group_new (GTK_SIZE_GROUP_BOTH);

	for (ids = prefs_get_network_ids ();
		 ids != NULL; ids = g_slist_remove (ids, ids->data))
	{
		item = gtk_menu_item_new ();
		g_object_set_qdata_full (G_OBJECT (item), PREFS_NETWORK_ID_KEY, ids->data,
								 g_free);

		hbox = gtk_hbox_new (FALSE, 4);
		gtk_container_add (GTK_CONTAINER (item), hbox);
		gtk_widget_show (hbox);

		icon = prefs_get_network_icon (ids->data);

		if (icon != NULL)
		{
			image = gtk_image_new_from_stock (stock_get_id_from_file (icon),
											  GTK_ICON_SIZE_MENU);
			g_free (icon);
		}
		else
		{
			image = gtk_image_new_from_stock (GNOMECHAT_STOCK_NETWORK,
											  GTK_ICON_SIZE_MENU);
		}

		gtk_box_pack_start (GTK_BOX (hbox), image, FALSE, FALSE, 0);
		gtk_size_group_add_widget (group, image);
		gtk_widget_show (image);

		label = gtk_label_new (prefs_get_network_name (ids->data));
		gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
		gtk_box_pack_start (GTK_BOX (hbox), label, TRUE, TRUE, 0);
		gtk_widget_show (label);

		gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
		gtk_widget_show (item);
	}

	g_object_unref (group);
	gtk_widget_show (menu);

	return menu;
}


GtkTreeModel *
prefs_ui_util_get_network_model (void)
{
	static GtkTreeStore *store = NULL;
	GSList *ids;
	gchar *name,
	 *icon,
	 *stock_id,
	 *description,
	 *website,
	 *category_id,
	 *old_category_id = NULL;

	GtkTreeIter parent,
	  iter;

	if (store != NULL)
	{
		gtk_tree_store_clear (store);
	}
	else
	{
		store = gtk_tree_store_new (NETWORK_LIST_NUM_COLS,
									G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING,
									G_TYPE_STRING, G_TYPE_STRING);
		g_object_add_weak_pointer (G_OBJECT (store), (gpointer) & store);
	}

	for (ids = prefs_get_network_ids ();
		 ids != NULL; ids = g_slist_remove (ids, ids->data))
	{
		category_id = prefs_get_network_category (ids->data);

		if (old_category_id == NULL || strcmp (category_id, old_category_id) != 0)
		{
			name = prefs_get_category_name (category_id);
			description = prefs_get_category_description (category_id);
			icon = prefs_get_category_icon (category_id);

			if (icon != NULL)
				stock_id = (gchar *) stock_get_id_from_file (icon);
			else
				stock_id = GNOMECHAT_STOCK_GLOBE;

			gtk_tree_store_append (store, &parent, NULL);
			gtk_tree_store_set (store, &parent,
								NETWORK_LIST_STOCK_PIXBUF_ITEM, stock_id,
								NETWORK_LIST_NAME_ITEM, name,
								NETWORK_LIST_DESCRIPTION_ITEM, description,
								NETWORK_LIST_WEBSITE_ITEM, NULL,
								NETWORK_LIST_KEY_ITEM, NULL, -1);
			g_free (name);
			g_free (description);
			g_free (icon);
		}

		name = prefs_get_network_name (ids->data);
		icon = prefs_get_network_icon (ids->data);
		description = prefs_get_network_description (ids->data);
		website = prefs_get_network_website (ids->data);

		if (icon != NULL)
		{
			stock_id = (gchar *) stock_get_id_from_file (icon);
		}
		else
		{
			stock_id = GNOMECHAT_STOCK_NETWORK;
		}

		gtk_tree_store_append (store, &iter, &parent);
		gtk_tree_store_set (store, &iter,
							NETWORK_LIST_STOCK_PIXBUF_ITEM, stock_id,
							NETWORK_LIST_NAME_ITEM, name,
							NETWORK_LIST_DESCRIPTION_ITEM, description,
							NETWORK_LIST_WEBSITE_ITEM,
							(website != NULL && website[0] != '\0' ? website : NULL),
							NETWORK_LIST_KEY_ITEM, ids->data, -1);

		g_free (icon);
		g_free (name);
		g_free (description);
		g_free (website);
		g_free (old_category_id);
		g_free (ids->data);

		old_category_id = category_id;

		while (gtk_events_pending ())
			gtk_main_iteration ();
	}

	return GTK_TREE_MODEL (store);
}


GtkTreeModel *
prefs_ui_util_get_server_model (const gchar * network_id)
{
	static GtkListStore *store = NULL;
	GtkTreeIter iter;
	GSList *ids;
	gchar *address,
	 *country_code,
	 *location;

	if (store != NULL)
	{
		gtk_list_store_clear (store);
	}
	else
	{
		store = gtk_list_store_new (SERVER_LIST_NUM_COLS,
									G_TYPE_BOOLEAN, G_TYPE_STRING, G_TYPE_STRING,
									G_TYPE_STRING, G_TYPE_STRING);
		g_object_add_weak_pointer (G_OBJECT (store), (gpointer) & store);
	}

	for (ids = prefs_get_network_server_ids (network_id);
		 ids != NULL; ids = g_slist_remove (ids, ids->data))
	{
		while (gtk_events_pending ())
			gtk_main_iteration ();

		address = prefs_get_server_address (network_id, ids->data);
		location = prefs_get_server_location (network_id, ids->data);
		country_code = prefs_get_server_country_code (network_id, ids->data);

		gtk_list_store_append (store, &iter);
		gtk_list_store_set (store, &iter,
							SERVER_LIST_ENABLED_ITEM,
							prefs_get_server_enabled (network_id, ids->data),
							SERVER_LIST_COUNTRY_CODE_ITEM, country_code,
							SERVER_LIST_LOCATION_ITEM, location,
							SERVER_LIST_ADDRESS_ITEM, address,
							SERVER_LIST_KEY_ITEM, ids->data, -1);

		g_free (country_code);
		g_free (location);
		g_free (address);
		g_free (ids->data);
	}

	return GTK_TREE_MODEL (store);
}


GtkTreeModel *
prefs_ui_util_get_channel_model (const gchar * network_id)
{
	static GtkListStore *store = NULL;
	GtkTreeIter iter;
	GSList *channels;

	if (store != NULL)
	{
		gtk_list_store_clear (store);
	}
	else
	{
		store = gtk_list_store_new (CHANNEL_LIST_NUM_COLS, G_TYPE_STRING);
		g_object_add_weak_pointer (G_OBJECT (store), (gpointer) & store);
	}

	for (channels = prefs_get_network_channels (network_id);
		 channels != NULL; channels = g_slist_remove (channels, channels->data))
	{
		gtk_list_store_append (store, &iter);
		gtk_list_store_set (store, &iter, CHANNEL_LIST_NAME_ITEM, channels->data, -1);

		g_free (channels->data);
	}

	return GTK_TREE_MODEL (store);
}


GQuark
prefs_ui_util_get_network_id_quark (void)
{
	static GQuark quark = 0;

	if (quark == 0)
	{
		quark = g_quark_from_static_string ("gnomechat-network-id");
	}

	return quark;
}
