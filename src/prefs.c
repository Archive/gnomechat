/*
 *  GnomeChat: src/prefs.c
 *
 *  Copyright (c) 2002 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "gnomechat.h"
#include "prefs.h"
#include "prefs-keys.h"

#include "utils.h"
#include "windows.h"
#include "esco-window.h"
#include "esco-viewer.h"
#include "tray-icon.h"

#include <gconf/gconf-client.h>

#include <string.h>


#define GCONF_ENTRY(entry) ((GConfEntry *)(entry))


/* These should mirror gnomechat.schemas */
#define DEFAULT_NICK1				"GCUser"
#define DEFAULT_WIN_LAYOUT			ESCO_WINDOW_LAYOUT_TABS
#define DEFAULT_WIN_WIDTH			520
#define DEFAULT_WIN_HEIGHT			480
#define DEFAULT_TAB_SIDE			GTK_POS_BOTTOM
#define DEFAULT_WIN_X				-1
#define DEFAULT_WIN_Y				-1
#define DEFAULT_NAMING_STYLE		GDCC_RECV_FILE_FILENAME
#define DEFAULT_NETWORK_CATEGORY	NETWORK_CATEGORY_USER

#define URL_HANDLER_DIR "/desktop/gnome/url-handlers"


typedef struct _Preferences Preferences;
struct _Preferences
{
	GConfClient *client;

	/* Dialog changesets (makes "Cancel" work) */
	GConfChangeSet *prefs_cs;
	GConfChangeSet *fonts_colors_cs;
	GConfChangeSet *netedit_cs;
	GConfChangeSet *servedit_cs;

	ViewerStyle *viewer_styles[VIEWER_STYLE_LAST];
};


/* The Preferences struct */
static Preferences *prefs = NULL;


/* Enum/String Pairs */
static const GConfEnumStringPair layout_type_table[] = {
	{ESCO_WINDOW_LAYOUT_TABS, "tabs"},
	{ESCO_WINDOW_LAYOUT_TREE, "tree"},
	{ESCO_WINDOW_LAYOUT_WINDOW, "window"},
	{0, NULL}
};

static const GConfEnumStringPair tab_side_table[] = {
	{GTK_POS_LEFT, "left"},
	{GTK_POS_RIGHT, "right"},
	{GTK_POS_TOP, "top"},
	{GTK_POS_BOTTOM, "bottom"},
	{0, NULL}
};

static const GConfEnumStringPair recv_naming_table[] = {
	{GDCC_RECV_FILE_FILENAME, "filename"},
	{GDCC_RECV_FILE_SENDER_FOLDER, "sender-folder"},
	{GDCC_RECV_FILE_SENDER_FILENAME, "sender.filename"},
	{0, NULL}
};



/* ******************* *
 *  Utility Functions  *
 * ******************* */


static inline const gchar *
get_viewer_style_dir (ViewerStyleType type)
{
	switch (type)
	{
	case VIEWER_STYLE_MY_ACTIONS:
		return VIEWER_ME_ACTION_DIR;
	case VIEWER_STYLE_MY_NICK:
		return VIEWER_ME_NICK_DIR;

	case VIEWER_STYLE_NICK:
		return VIEWER_OTHERS_NICK_DIR;
	case VIEWER_STYLE_ACTIONS:
		return VIEWER_OTHERS_ACTION_DIR;

	case VIEWER_STYLE_TO_ME:
		return VIEWER_TO_ME_TEXT_DIR;
	case VIEWER_STYLE_TO_ME_NICK:
		return VIEWER_TO_ME_NICK_DIR;
	case VIEWER_STYLE_TO_ME_ACTIONS:
		return VIEWER_TO_ME_ACTION_DIR;

	case VIEWER_STYLE_SERVER:
		return VIEWER_SERVER_TEXT_DIR;
	case VIEWER_STYLE_SERVER_NAME:
		return VIEWER_SERVER_NAME_DIR;

	case VIEWER_STYLE_CHANNEL:
		return VIEWER_CHANNEL_TEXT_DIR;
	case VIEWER_STYLE_CHANNEL_NAME:
		return VIEWER_CHANNEL_NAME_DIR;
	case VIEWER_STYLE_CHANNEL_NICK:
		return VIEWER_CHANNEL_NICK_DIR;
	case VIEWER_STYLE_CHANNEL_VALUE:
		return VIEWER_CHANNEL_VALUE_DIR;

	case VIEWER_STYLE_LINK:
		return VIEWER_LINK_DIR;
	case VIEWER_STYLE_ERROR:
		return VIEWER_ERROR_DIR;

	default:
		break;
	}

	g_return_val_if_reached (NULL);
}


static GSList *
get_keys_in_dir (GSList * keys_list, const gchar * dir)
{
	GSList *list;

	for (list = gconf_client_all_entries (prefs->client, dir, NULL);
		 list != NULL; list = g_slist_remove_link (list, list))
	{
		if (list->data != NULL)
		{
			keys_list = g_slist_prepend (keys_list, g_strdup (GCONF_ENTRY (list->data)->key));
			if (GCONF_ENTRY (list->data)->value != NULL)
				gconf_value_free (GCONF_ENTRY (list->data)->value);
		}
	}

	return keys_list;
}


static GSList *
get_viewer_styles_keys_list (void)
{
	GSList *keys_list = NULL;
	ViewerStyleType i;

	for (i = VIEWER_STYLE_FIRST; i < VIEWER_STYLE_LAST; i++)
	{
		keys_list = get_keys_in_dir (keys_list, get_viewer_style_dir (i));
	}

	return keys_list;
}


static inline gchar **
string_slist_to_array (GSList * keys_list)
{
	gchar **retval = NULL;
	guint i = 0;

	retval = g_malloc ((sizeof (gchar *) * (g_slist_length (keys_list) + 1)));

	for (; keys_list != NULL; keys_list = g_slist_remove (keys_list, keys_list->data))
	{
		retval[i] = keys_list->data;
		i++;
	}

	retval[i] = NULL;

	return retval;
}


static inline gchar **
get_fonts_colors_dialog_keys (void)
{
	return string_slist_to_array (get_viewer_styles_keys_list ());
}


static void
unset_keys_in_dir (const gchar * dir)
{
	GSList *keys = NULL;

	for (keys = get_keys_in_dir (keys, dir); keys != NULL; keys = g_slist_remove (keys, keys->data))
	{
		gconf_client_unset (prefs->client, (const gchar *) keys->data, NULL);
		g_free (keys->data);
	}

	gconf_client_suggest_sync (prefs->client, NULL);
}

static inline gchar **
get_prefs_dialog_keys (void)
{
	GSList *keys_list;

	/* Viewer Styles */
	keys_list = get_viewer_styles_keys_list ();
	/* DCC */
	keys_list = get_keys_in_dir (keys_list, DCC_DIR);
	/* Identity */
	keys_list = get_keys_in_dir (keys_list, IDENTITY_DIR);
	/* Global */
	keys_list = get_keys_in_dir (keys_list, VIEWER_GLOBAL_DIR);
	/* Notification Colors */
	keys_list = get_keys_in_dir (keys_list, NOTIFY_DIR);
	/* Windowing prefs */
	keys_list = get_keys_in_dir (keys_list, WINDOWS_DIR);

	return string_slist_to_array (keys_list);
}


static inline gchar **
get_netedit_dialog_keys (void)
{
	GSList *net_ids,
	 *list,
	 *keys_list = NULL;
	gchar *dir;

	keys_list = g_slist_prepend (keys_list, g_strdup (NETWORK_LIST_KEY));

	for (net_ids = prefs_get_network_ids ();
		 net_ids != NULL; net_ids = g_slist_remove (net_ids, net_ids->data))
	{
		dir = g_build_filename (NETWORK_DIR, net_ids->data, NULL);
		keys_list = get_keys_in_dir (keys_list, dir);
		g_free (dir);

		for (list = prefs_get_network_server_ids (net_ids->data);
			 list != NULL; list = g_slist_remove (list, list->data))
		{
			dir = g_build_filename (NETWORK_DIR, net_ids->data, list->data, NULL);
			keys_list = get_keys_in_dir (keys_list, dir);
			g_free (dir);

			g_free (list->data);
		}

		g_free (net_ids->data);
	}

	return string_slist_to_array (keys_list);
}


static gchar *
get_style_key (ViewerStyleType type, const gchar * key_bit)
{
	return gconf_concat_dir_and_key (get_viewer_style_dir (type), key_bit);
}


static gchar *
get_decent_fg_color (ViewerStyleType type, const gchar * bg_color)
{
	return "#000000";
}


static ViewerStyle *
get_viewer_style (ViewerStyleType type)
{
	ViewerStyle *style;
	GConfValue *bg_value;
	const gchar *dir;
	gchar *key;

	style = g_new0 (ViewerStyle, 1);

	dir = get_viewer_style_dir (type);

	key = gconf_concat_dir_and_key (dir, BG_KEY_BIT);
	style->bg_color = gconf_client_get_string (prefs->client, key, NULL);
	bg_value = gconf_client_get_without_default (prefs->client, key, NULL);
	g_free (key);

	/* Ok, what we do here is see if the user has set the background color.
	   If they have, and haven't set this FG, then get a decent color for them
	   based on the ViewerStyleType. */
	key = gconf_concat_dir_and_key (dir, FG_KEY_BIT);
	if (bg_value != NULL)
	{
		GConfValue *fg_value = gconf_client_get_without_default (prefs->client, key, NULL);

		if (fg_value != NULL)
		{
			style->fg_color = gconf_client_get_string (prefs->client, key, NULL);
			gconf_value_free (fg_value);
		}
		else
		{
			style->fg_color = get_decent_fg_color (type, gconf_value_get_string (bg_value));
		}

		gconf_value_free (bg_value);
	}
	else
	{
		style->fg_color = gconf_client_get_string (prefs->client, key, NULL);
	}
	g_free (key);

	key = gconf_concat_dir_and_key (dir, BOLD_KEY_BIT);
	style->bold = gconf_client_get_bool (prefs->client, key, NULL);
	g_free (key);

	key = gconf_concat_dir_and_key (dir, ITALIC_KEY_BIT);
	style->italic = gconf_client_get_bool (prefs->client, key, NULL);
	g_free (key);

	key = gconf_concat_dir_and_key (dir, ULINE_KEY_BIT);
	style->uline = gconf_client_get_bool (prefs->client, key, NULL);
	g_free (key);

	return style;
}


static gint
get_clamped_int (const gchar * key, gboolean can_be_negative, gint min, gint max, gint default_val)
{
	gint value;

	value = gconf_client_get_int (prefs->client, key, NULL);

	if (!can_be_negative)
	{
		if (value > max || value < min)
		{
			value = default_val;
			gconf_client_set_int (prefs->client, key, value, NULL);
		}
	}
	else
	{
		if (value > max || (value < min && value > 0))
		{
			value = default_val;
			gconf_client_set_int (prefs->client, key, value, NULL);
		}
	}

	gconf_client_suggest_sync (prefs->client, NULL);

	return value;
}


static gchar *
prefs_get_viewer_global (const gchar * bit)
{
	gchar *value = NULL,
		*key;

	key = gconf_concat_dir_and_key (VIEWER_GLOBAL_DIR, bit);
	value = gconf_client_get_string (prefs->client, key, NULL);
	g_free (key);

	return value;
}


static gboolean
compare_str_with_value (const gchar * str, const gchar * key)
{
	gchar *value = gconf_client_get_string (prefs->client, key, NULL);
	gboolean retval = (str == NULL && value == NULL);

	if (value != NULL)
	{
		if (str != NULL)
		{
			retval = (strcmp (str, value) == 0);
		}

		g_free (value);
	}

	return retval;
}


/* ************************ *
 *  Notification Callbacks  *
 * ************************ */

static void
windows_dir_changed (GConfClient * client, guint cnxn_id, GConfEntry * entry, gpointer user_data)
{
	if (strcmp (entry->key, MAIN_WINDOW_TAB_SIDE_KEY) == 0
		&& entry->value != NULL && entry->value->type == GCONF_VALUE_STRING)
	{
		gint pos;

		if (!gconf_string_to_enum ((GConfEnumStringPair *) tab_side_table,
								   gconf_value_get_string (entry->value), &pos))
		{
			pos = DEFAULT_TAB_SIDE;
			prefs_set_main_window_tab_side (pos);
		}

		windows_set_tab_position (pos);
	}
}


static void
viewer_dir_changed (GConfClient * client, guint xnxn_id, GConfEntry * entry, gpointer user_data)
{
	if (strcmp (entry->key, VIEWER_SHOW_TIMESTAMPS_KEY) == 0
		&& entry->value != NULL && entry->value->type == GCONF_VALUE_BOOL)
	{
		esco_viewers_set_show_timestamps (gconf_value_get_bool (entry->value));
	}
	else if (strcmp (entry->key, VIEWER_USE_IMAGE_SMILEYS_KEY) == 0
			 && entry->value != NULL && entry->value->type == GCONF_VALUE_BOOL)
	{
		esco_viewers_set_use_image_smileys (gconf_value_get_bool (entry->value));
	}
}


static void
notify_dir_changed (GConfClient * client, guint xnxn_id, GConfEntry * entry, gpointer user_data)
{
	if (strcmp (entry->key, NOTIFY_USE_TRAY_KEY) == 0 &&
		entry->value != NULL && entry->value->type == GCONF_VALUE_BOOL)
	{
		if (gconf_value_get_bool (entry->value))
		{
			tray_icon_init ();
		}
		else
		{
			tray_icon_shutdown ();
		}
	}
}


static void
url_handlers_changed (GConfClient * client, guint xnxn_id, GConfEntry * entry, gpointer user_data)
{
	esco_viewers_set_uri_prefixes (prefs_get_uri_prefixes ());
}


/* Busted Viewer-Style stuff from here on down */
static void
viewer_global_dir_changed (GConfClient * client,
						   guint cnxn_id, GConfEntry * entry, gpointer user_data)
{
	const gchar *key = util_gconf_key_key (entry->key);

	g_return_if_fail (key != NULL);

	/* Font */
	if (strcmp (key, FONT_KEY_BIT) == 0)
	{
		if (entry->value && entry->value->type == GCONF_VALUE_STRING)
			esco_viewers_set_font (gconf_value_get_string (entry->value));
		else
			esco_viewers_set_font (NULL);
	}
	/* Global Foreground Color */
	else if (strcmp (key, FG_KEY_BIT) == 0)
	{
		if (entry->value && entry->value->type == GCONF_VALUE_STRING)
			esco_viewers_set_fg (gconf_value_get_string (entry->value));
		else
			esco_viewers_set_fg (NULL);
	}
	/* Global Background Color */
	else if (strcmp (key, BG_KEY_BIT) == 0)
	{
		if (entry->value && entry->value->type == GCONF_VALUE_STRING)
			esco_viewers_set_bg (gconf_value_get_string (entry->value));
		else
			esco_viewers_set_bg (NULL);
	}
}


static void
viewer_style_changed (GConfClient * client, guint xnxn_id, GConfEntry * entry, gpointer user_data)
{
	const gchar *key = util_gconf_key_key (entry->key);
	ViewerStyleType style_type = GPOINTER_TO_UINT (user_data);

	g_return_if_fail (style_type >= VIEWER_STYLE_FIRST && style_type < VIEWER_STYLE_LAST);

	if (key == NULL)
		return;

	if (strcmp (key, FG_KEY_BIT) == 0)
	{
		if (entry->value != NULL && entry->value->type == GCONF_VALUE_STRING)
		{
			g_free (prefs->viewer_styles[style_type]->fg_color);

			prefs->viewer_styles[style_type]->fg_color =
				g_strdup (gconf_value_get_string (entry->value));

			esco_viewers_set_style_fg (style_type, gconf_value_get_string (entry->value));
		}
		else
		{
			esco_viewers_set_style_fg (style_type, NULL);
		}
	}
	else if (strcmp (key, BG_KEY_BIT) == 0)
	{
		if (entry->value != NULL && entry->value->type == GCONF_VALUE_STRING)
		{
			g_free (prefs->viewer_styles[style_type]->bg_color);

			prefs->viewer_styles[style_type]->bg_color =
				g_strdup (gconf_value_get_string (entry->value));

			esco_viewers_set_style_bg (style_type, gconf_value_get_string (entry->value));
		}
		else
		{
			esco_viewers_set_style_bg (style_type, NULL);
		}
	}
	else if (strcmp (key, BOLD_KEY_BIT) == 0)
	{
		if (entry->value != NULL && entry->value->type == GCONF_VALUE_BOOL)
		{
			prefs->viewer_styles[style_type]->bold = gconf_value_get_bool (entry->value);

			esco_viewers_set_style_bold (style_type, gconf_value_get_bool (entry->value));
		}
		else
		{
			esco_viewers_set_style_bold (style_type, FALSE);
		}
	}
	else if (strcmp (key, ITALIC_KEY_BIT) == 0)
	{
		if (entry->value != NULL && entry->value->type == GCONF_VALUE_BOOL)
		{
			prefs->viewer_styles[style_type]->italic = gconf_value_get_bool (entry->value);

			esco_viewers_set_style_italic (style_type, gconf_value_get_bool (entry->value));
		}
		else
		{
			esco_viewers_set_style_italic (style_type, FALSE);
		}
	}
	else if (strcmp (key, ULINE_KEY_BIT) == 0)
	{
		if (entry->value != NULL && entry->value->type == GCONF_VALUE_BOOL)
		{
			prefs->viewer_styles[style_type]->uline = gconf_value_get_bool (entry->value);

			esco_viewers_set_style_uline (style_type, gconf_value_get_bool (entry->value));
		}
		else
		{
			esco_viewers_set_style_uline (style_type, FALSE);
		}
	}
}


/* ************************************************************************** *
 *  PUBLIC API                                                                *
 * ************************************************************************** */


/* ************** *
 *  Housekeeping  *
 * ************** */

void
prefs_init (void)
{
	gint i;

	prefs = g_new0 (Preferences, 1);

	prefs->client = gconf_client_get_default ();
	gconf_client_add_dir (prefs->client, GNOMECHAT_PREFIX, GCONF_CLIENT_PRELOAD_NONE, NULL);
	gconf_client_add_dir (prefs->client, URL_HANDLER_DIR, GCONF_CLIENT_PRELOAD_NONE, NULL);

	prefs->prefs_cs = prefs->fonts_colors_cs = prefs->netedit_cs = prefs->servedit_cs = NULL;

	/* Load the viewer styles -- we don't officially preload this because we do a lot of
	   key = g_strdup (blah) nonsense in get_viewer_style(), and chances are
	   pretty good we only need to do this once. */
	for (i = 0; i < VIEWER_STYLE_LAST; i++)
		prefs->viewer_styles[i] = get_viewer_style (i);

	/* GConf-based preloading */
	/* Viewer Preferences */
	gconf_client_preload (prefs->client, VIEWER_DIR, GCONF_CLIENT_PRELOAD_ONELEVEL, NULL);
	/* Main Window Preferences */
	gconf_client_preload (prefs->client, WINDOWS_DIR, GCONF_CLIENT_PRELOAD_ONELEVEL, NULL);
	/* Notification Preferences */
	gconf_client_preload (prefs->client, NOTIFY_DIR, GCONF_CLIENT_PRELOAD_ONELEVEL, NULL);
	/* Identity Preferences */
	gconf_client_preload (prefs->client, IDENTITY_DIR, GCONF_CLIENT_PRELOAD_ONELEVEL, NULL);

	/* Network List
	 * #FIXME: Will die after RDF listing is ready for actually use. */
	gconf_client_preload (prefs->client, NETWORK_DIR, GCONF_CLIENT_PRELOAD_RECURSIVE, NULL);

	/* Change Notification */
	/* Viewer Preferences */
	gconf_client_notify_add (prefs->client, VIEWER_DIR, viewer_dir_changed, NULL, NULL, NULL);
	/* Main Window Preferences */
	gconf_client_notify_add (prefs->client, WINDOWS_DIR, windows_dir_changed, NULL, NULL, NULL);
	/* URI Handlers */
	gconf_client_notify_add (prefs->client, URL_HANDLER_DIR, url_handlers_changed, NULL, NULL,
							 NULL);
	/* Notification Preferences */
	gconf_client_notify_add (prefs->client, NOTIFY_DIR, notify_dir_changed, NULL, NULL, NULL);

	/* Busted Viewer Styles Stuff
	 * #FIXME: Will die when viewer theming gets implemented. */
	gconf_client_notify_add (prefs->client, VIEWER_GLOBAL_DIR, viewer_global_dir_changed,
							 NULL, NULL, NULL);

	gconf_client_notify_add (prefs->client, VIEWER_LINK_DIR, viewer_style_changed,
							 GUINT_TO_POINTER (VIEWER_STYLE_LINK), NULL, NULL);

	gconf_client_notify_add (prefs->client, VIEWER_ME_NICK_DIR,
							 viewer_style_changed,
							 GUINT_TO_POINTER (VIEWER_STYLE_MY_NICK), NULL, NULL);
	gconf_client_notify_add (prefs->client, VIEWER_ME_ACTION_DIR,
							 viewer_style_changed,
							 GUINT_TO_POINTER (VIEWER_STYLE_MY_ACTIONS), NULL, NULL);

	gconf_client_notify_add (prefs->client, VIEWER_TO_ME_TEXT_DIR,
							 viewer_style_changed,
							 GUINT_TO_POINTER (VIEWER_STYLE_TO_ME), NULL, NULL);
	gconf_client_notify_add (prefs->client, VIEWER_TO_ME_NICK_DIR,
							 viewer_style_changed,
							 GUINT_TO_POINTER (VIEWER_STYLE_TO_ME_NICK), NULL, NULL);
	gconf_client_notify_add (prefs->client, VIEWER_TO_ME_ACTION_DIR,
							 viewer_style_changed,
							 GUINT_TO_POINTER (VIEWER_STYLE_TO_ME_ACTIONS), NULL, NULL);

	gconf_client_notify_add (prefs->client, VIEWER_OTHERS_NICK_DIR,
							 viewer_style_changed,
							 GUINT_TO_POINTER (VIEWER_STYLE_NICK), NULL, NULL);
	gconf_client_notify_add (prefs->client, VIEWER_OTHERS_ACTION_DIR,
							 viewer_style_changed,
							 GUINT_TO_POINTER (VIEWER_STYLE_ACTIONS), NULL, NULL);

	gconf_client_notify_add (prefs->client, VIEWER_SERVER_TEXT_DIR,
							 viewer_style_changed,
							 GUINT_TO_POINTER (VIEWER_STYLE_SERVER), NULL, NULL);
	gconf_client_notify_add (prefs->client, VIEWER_SERVER_NAME_DIR,
							 viewer_style_changed,
							 GUINT_TO_POINTER (VIEWER_STYLE_SERVER_NAME), NULL, NULL);

	gconf_client_notify_add (prefs->client, VIEWER_CHANNEL_TEXT_DIR,
							 viewer_style_changed,
							 GUINT_TO_POINTER (VIEWER_STYLE_CHANNEL), NULL, NULL);
	gconf_client_notify_add (prefs->client, VIEWER_CHANNEL_NAME_DIR,
							 viewer_style_changed,
							 GUINT_TO_POINTER (VIEWER_STYLE_CHANNEL_NAME), NULL, NULL);
	gconf_client_notify_add (prefs->client, VIEWER_CHANNEL_NICK_DIR,
							 viewer_style_changed,
							 GUINT_TO_POINTER (VIEWER_STYLE_CHANNEL_NICK), NULL, NULL);
	gconf_client_notify_add (prefs->client, VIEWER_CHANNEL_VALUE_DIR,
							 viewer_style_changed,
							 GUINT_TO_POINTER (VIEWER_STYLE_CHANNEL_VALUE), NULL, NULL);

	gconf_client_notify_add (prefs->client, VIEWER_ERROR_DIR,
							 viewer_style_changed,
							 GUINT_TO_POINTER (VIEWER_STYLE_ERROR), NULL, NULL);
}


void
prefs_shutdown (void)
{
	guint i;

	gconf_client_remove_dir (prefs->client, GNOMECHAT_PREFIX, NULL);
	gconf_client_remove_dir (prefs->client, URL_HANDLER_DIR, NULL);
	g_object_unref (prefs->client);

	for (i = 0; i < VIEWER_STYLE_LAST; i++)
	{
		if (prefs->viewer_styles[i] != NULL)
		{
			g_free (prefs->viewer_styles[i]->fg_color);
			g_free (prefs->viewer_styles[i]->bg_color);
			g_free (prefs->viewer_styles[i]);
		}
	}

	g_free (prefs);
}


/* ************************ *
 *  Preferences Changesets  *
 * ************************ */

/* Prefs Dialog Changeset init/reversion */
void
prefs_dialog_opened (void)
{
	gchar **keys;

	if (prefs->prefs_cs != NULL)
		return;

	keys = get_prefs_dialog_keys ();
	prefs->prefs_cs = gconf_client_change_set_from_currentv (prefs->client, (const gchar **) keys,
															 NULL);
	g_strfreev (keys);
}


void
prefs_dialog_cancelled (void)
{
	if (prefs->prefs_cs == NULL)
		return;

	gconf_client_commit_change_set (prefs->client, prefs->prefs_cs, FALSE, NULL);
	gconf_client_suggest_sync (prefs->client, NULL);
	prefs_dialog_closed ();
}


void
prefs_dialog_closed (void)
{
	if (prefs->prefs_cs == NULL)
		return;

	gconf_change_set_unref (prefs->prefs_cs);
	prefs->prefs_cs = NULL;
}


/* More Fonts & Colors Dialog Changeset init/reversion */
void
prefs_fonts_colors_dialog_opened (void)
{
	gchar **keys;

	if (prefs->fonts_colors_cs != NULL)
		return;

	keys = get_fonts_colors_dialog_keys ();
	prefs->fonts_colors_cs = gconf_client_change_set_from_currentv (prefs->client,
																	(const gchar **) keys, NULL);
	g_strfreev (keys);
}


void
prefs_fonts_colors_dialog_cancelled (void)
{
	if (prefs->fonts_colors_cs == NULL)
		return;

	gconf_client_commit_change_set (prefs->client, prefs->fonts_colors_cs, FALSE, NULL);
	gconf_client_suggest_sync (prefs->client, NULL);
	prefs_fonts_colors_dialog_closed ();
}


void
prefs_fonts_colors_dialog_closed (void)
{
	if (prefs->fonts_colors_cs == NULL)
		return;

	gconf_change_set_unref (prefs->fonts_colors_cs);
	prefs->fonts_colors_cs = NULL;
}


/* Network List Editing Dialog */
void
prefs_netedit_dialog_opened (void)
{
	gchar **keys;

	if (prefs->netedit_cs != NULL)
		return;

	keys = get_netedit_dialog_keys ();
	prefs->netedit_cs = gconf_client_change_set_from_currentv (prefs->client, (const gchar **) keys,
															   NULL);
	g_strfreev (keys);
}


void
prefs_netedit_dialog_cancelled (void)
{
	if (prefs->netedit_cs == NULL)
		return;

	gconf_client_commit_change_set (prefs->client, prefs->netedit_cs, FALSE, NULL);
	gconf_client_suggest_sync (prefs->client, NULL);
	prefs_netedit_dialog_closed ();
}


void
prefs_netedit_dialog_closed (void)
{
	if (prefs->netedit_cs == NULL)
		return;

	gconf_change_set_unref (prefs->netedit_cs);
	prefs->netedit_cs = NULL;
}


/* ******************* *
 *  Utility Functions  *
 * ******************* */

gchar *
prefs_get_monospace_font (void)
{
	gchar *retval;

	retval = gconf_client_get_string (prefs->client, "/desktop/gnome/interface/monospace_font_name",
									  NULL);

	return (retval != NULL && retval[0] != '\0' ? retval : g_strdup ("Monospace 10, mono 10"));
}


gchar *
prefs_get_default_font (void)
{
	gchar *retval;

	retval = gconf_client_get_string (prefs->client, "/desktop/gnome/interface/font_name", NULL);

	return (retval != NULL && retval[0] != '\0' ? retval : g_strdup ("Sans 10, sans-serif 10"));
}


GSList *
prefs_get_uri_prefixes (void)
{
	GSList *retval = NULL,
		*dirs = NULL;

	for (dirs = gconf_client_all_dirs (prefs->client, URL_HANDLER_DIR, NULL);
		 dirs != NULL; dirs = g_slist_remove_link (dirs, dirs))
	{
		if (dirs->data != NULL)
		{
			if (*((gchar *) (dirs->data)) != '\0')
			{
				retval = g_slist_append (retval, g_strconcat (util_gconf_key_key (dirs->data), ":",
															  NULL));
			}

			g_free (dirs->data);
		}
	}

	return retval;
}


gchar *
prefs_get_unique_id (void)
{
	return gconf_unique_key ();
}


gboolean
prefs_get_first_run_completed (void)
{
	return gconf_client_get_bool (prefs->client, FIRST_RUN_KEY, NULL);
}


void
prefs_set_first_run_completed (gboolean completed)
{
	if (gconf_client_key_is_writable (prefs->client, FIRST_RUN_KEY, NULL))
	{
		gconf_client_set_bool (prefs->client, FIRST_RUN_KEY, completed, NULL);
		gconf_client_suggest_sync (prefs->client, NULL);
	}
}


/* **************************** *
 *  Generic Window Preferences  *
 * **************************** */

/* Window Width Functions */
static inline const gchar *
get_window_width_key (GnomechatWindowID id)
{
	switch (id)
	{
	case GNOMECHAT_WINDOW_MAIN:
		return MAIN_WINDOW_WIDTH_KEY;

	case GNOMECHAT_WINDOW_CONNECT:
		return CONNECT_DIALOG_WIDTH_KEY;

	case GNOMECHAT_WINDOW_PREFS:
		return PREFS_DIALOG_WIDTH_KEY;

	default:
		break;
	}

	g_return_val_if_reached (NULL);
}


gboolean
prefs_get_can_set_window_width (GnomechatWindowID id)
{
	g_return_val_if_fail (id >= GNOMECHAT_WINDOW_FIRST && id < GNOMECHAT_WINDOW_LAST, FALSE);

	return gconf_client_key_is_writable (prefs->client, get_window_width_key (id), NULL);
}


gint
prefs_get_window_width (GnomechatWindowID id)
{
	g_return_val_if_fail (id >= GNOMECHAT_WINDOW_FIRST && id < GNOMECHAT_WINDOW_LAST, -1);

	return CLAMP (gconf_client_get_int (prefs->client, get_window_width_key (id), NULL),
				  0, gdk_screen_width ());
}


void
prefs_set_window_width (GnomechatWindowID id, gint width)
{
	const gchar *key;

	g_return_if_fail (id >= GNOMECHAT_WINDOW_FIRST && id < GNOMECHAT_WINDOW_LAST);

	key = get_window_width_key (id);

	if (gconf_client_key_is_writable (prefs->client, key, NULL))
	{
		gconf_client_set_int (prefs->client, key, CLAMP (width, -1, gdk_screen_width ()), NULL);
		gconf_client_suggest_sync (prefs->client, NULL);
	}
}


/* Window Height Functions */
static inline const gchar *
get_window_height_key (GnomechatWindowID id)
{
	switch (id)
	{
	case GNOMECHAT_WINDOW_MAIN:
		return MAIN_WINDOW_HEIGHT_KEY;

	case GNOMECHAT_WINDOW_CONNECT:
		return CONNECT_DIALOG_HEIGHT_KEY;

	case GNOMECHAT_WINDOW_PREFS:
		return PREFS_DIALOG_HEIGHT_KEY;

	default:
		break;
	}

	g_return_val_if_reached (NULL);
}


gboolean
prefs_get_can_set_window_height (GnomechatWindowID id)
{
	g_return_val_if_fail (id >= GNOMECHAT_WINDOW_FIRST && id < GNOMECHAT_WINDOW_LAST, FALSE);

	return gconf_client_key_is_writable (prefs->client, get_window_height_key (id), NULL);
}


gint
prefs_get_window_height (GnomechatWindowID id)
{
	g_return_val_if_fail (id >= GNOMECHAT_WINDOW_FIRST && id < GNOMECHAT_WINDOW_LAST, -1);

	return CLAMP (gconf_client_get_int (prefs->client, get_window_height_key (id), NULL),
				  -1, gdk_screen_height ());
}


void
prefs_set_window_height (GnomechatWindowID id, gint height)
{
	const gchar *key;

	g_return_if_fail (id >= GNOMECHAT_WINDOW_FIRST && id < GNOMECHAT_WINDOW_LAST);

	key = get_window_height_key (id);

	if (gconf_client_key_is_writable (prefs->client, key, NULL))
	{
		gconf_client_set_int (prefs->client, key, CLAMP (height, -1, gdk_screen_height ()), NULL);
		gconf_client_suggest_sync (prefs->client, NULL);
	}
}


/* ************************* *
 *  Main Window Preferences  *
 * ************************* */

/* Force Main Window Size */
gboolean
prefs_get_can_set_main_window_force_size (void)
{

	return gconf_client_key_is_writable (prefs->client, MAIN_WINDOW_FORCE_SIZE_KEY, NULL);
}


gboolean
prefs_get_main_window_force_size (void)
{
	return gconf_client_get_bool (prefs->client, MAIN_WINDOW_FORCE_SIZE_KEY, NULL);
}


void
prefs_set_main_window_force_size (gboolean force)
{
	if (gconf_client_key_is_writable (prefs->client, MAIN_WINDOW_FORCE_SIZE_KEY, NULL))
	{
		gconf_client_set_bool (prefs->client, MAIN_WINDOW_FORCE_SIZE_KEY, force, NULL);
		gconf_client_suggest_sync (prefs->client, NULL);
	}
}


/* Force Main Window Position */
gboolean
prefs_get_can_set_main_window_force_position (void)
{
	return gconf_client_key_is_writable (prefs->client, MAIN_WINDOW_FORCE_POSITION_KEY, NULL);
}


gboolean
prefs_get_main_window_force_position (void)
{
	return gconf_client_get_bool (prefs->client, MAIN_WINDOW_FORCE_POSITION_KEY, NULL);
}


void
prefs_set_main_window_force_position (gboolean force)
{
	if (gconf_client_key_is_writable (prefs->client, MAIN_WINDOW_FORCE_POSITION_KEY, NULL))
	{
		gconf_client_set_bool (prefs->client, MAIN_WINDOW_FORCE_POSITION_KEY, force, NULL);
		gconf_client_suggest_sync (prefs->client, NULL);
	}
}


/* Left Edge */
gboolean
prefs_get_can_set_main_window_left_edge (void)
{
	return gconf_client_key_is_writable (prefs->client, MAIN_WINDOW_LEFT_EDGE_KEY, NULL);
}


gint
prefs_get_main_window_left_edge (void)
{
	return CLAMP (gconf_client_get_int (prefs->client, MAIN_WINDOW_LEFT_EDGE_KEY, NULL), -1,
				  gdk_screen_width () - gconf_client_get_int (prefs->client, MAIN_WINDOW_WIDTH_KEY,
															  NULL));
}


void
prefs_set_main_window_left_edge (gint left_edge)
{
	if (gconf_client_key_is_writable (prefs->client, MAIN_WINDOW_LEFT_EDGE_KEY, NULL))
	{
		gconf_client_set_int (prefs->client, MAIN_WINDOW_LEFT_EDGE_KEY,
							  CLAMP (left_edge, 0,
									 gdk_screen_width () - gconf_client_get_int (prefs->client,
																				 MAIN_WINDOW_WIDTH_KEY,
																				 NULL)), NULL);
		gconf_client_suggest_sync (prefs->client, NULL);
	}
}


/* Top Edge */
gboolean
prefs_get_can_set_main_window_top_edge (void)
{
	return gconf_client_key_is_writable (prefs->client, MAIN_WINDOW_TOP_EDGE_KEY, NULL);
}


gint
prefs_get_main_window_top_edge (void)
{
	return CLAMP (gconf_client_get_int (prefs->client, MAIN_WINDOW_TOP_EDGE_KEY, NULL), 0,
				  gdk_screen_height () - gconf_client_get_int (prefs->client,
															   MAIN_WINDOW_HEIGHT_KEY, NULL));
}


void
prefs_set_main_window_top_edge (gint top_edge)
{
	if (gconf_client_key_is_writable (prefs->client, MAIN_WINDOW_TOP_EDGE_KEY, NULL))
	{
		gconf_client_set_int (prefs->client, MAIN_WINDOW_TOP_EDGE_KEY,
							  CLAMP (top_edge, 0,
									 gdk_screen_height () - gconf_client_get_int (prefs->client,
																				  MAIN_WINDOW_HEIGHT_KEY,
																				  NULL)), NULL);
		gconf_client_suggest_sync (prefs->client, NULL);
	}
}


/* Window Layout */
gboolean
prefs_get_can_set_main_window_layout (void)
{
	return gconf_client_key_is_writable (prefs->client, MAIN_WINDOW_LAYOUT_KEY, NULL);
}


EscoWindowLayoutType
prefs_get_main_window_layout (void)
{
	gint layout;
	gchar *str;
	gboolean found = FALSE;

	str = gconf_client_get_string (prefs->client, MAIN_WINDOW_LAYOUT_KEY, NULL);

	if (str != NULL)
	{
		found = gconf_string_to_enum ((GConfEnumStringPair *) layout_type_table, str, &layout);
		g_free (str);
	}

	if (!found)
	{
		layout = DEFAULT_WIN_LAYOUT;

		if (gconf_client_key_is_writable (prefs->client, MAIN_WINDOW_LAYOUT_KEY, NULL))
		{
			gconf_client_set_string (prefs->client, MAIN_WINDOW_LAYOUT_KEY,
									 gconf_enum_to_string ((GConfEnumStringPair *)
														   layout_type_table, layout), NULL);
			gconf_client_suggest_sync (prefs->client, NULL);
		}
	}

	return layout;
}


void
prefs_set_main_window_layout (EscoWindowLayoutType layout)
{
	const gchar *str;

	g_return_if_fail (layout >= 0 && layout < ESCO_WINDOW_LAYOUT_LAST);

	str = gconf_enum_to_string ((GConfEnumStringPair *) layout_type_table, layout);

	if (gconf_client_key_is_writable (prefs->client, MAIN_WINDOW_LAYOUT_KEY, NULL))
	{
		gconf_client_set_string (prefs->client, MAIN_WINDOW_LAYOUT_KEY, str, NULL);
		gconf_client_suggest_sync (prefs->client, NULL);
	}
}


/* Notebook Tab Side */
gboolean
prefs_get_can_set_main_window_tab_side (void)
{
	return gconf_client_key_is_writable (prefs->client, MAIN_WINDOW_TAB_SIDE_KEY, NULL);
}


GtkPositionType
prefs_get_main_window_tab_side (void)
{
	gint pos;
	gboolean found = FALSE;
	gchar *str;

	str = gconf_client_get_string (prefs->client, MAIN_WINDOW_TAB_SIDE_KEY, NULL);

	if (str != NULL)
	{
		found = gconf_string_to_enum ((GConfEnumStringPair *) tab_side_table, str, &pos);
		g_free (str);
	}

	if (!found)
	{
		pos = DEFAULT_TAB_SIDE;

		if (gconf_client_key_is_writable (prefs->client, MAIN_WINDOW_TAB_SIDE_KEY, NULL))
		{
			gconf_client_set_string (prefs->client, MAIN_WINDOW_TAB_SIDE_KEY,
									 gconf_enum_to_string ((GConfEnumStringPair *) tab_side_table,
														   pos), NULL);
			gconf_client_suggest_sync (prefs->client, NULL);
		}
	}

	return pos;
}


void
prefs_set_main_window_tab_side (GtkPositionType side)
{
	g_return_if_fail (side >= 0 && side <= GTK_POS_BOTTOM);

	if (gconf_client_key_is_writable (prefs->client, MAIN_WINDOW_TAB_SIDE_KEY, NULL))
	{
		const gchar *str;

		str = gconf_enum_to_string ((GConfEnumStringPair *) tab_side_table, side);

		if (str == NULL)
		{
			str = gconf_enum_to_string ((GConfEnumStringPair *) tab_side_table, DEFAULT_TAB_SIDE);
		}

		if (!compare_str_with_value (str, MAIN_WINDOW_TAB_SIDE_KEY))
		{
			gconf_client_set_string (prefs->client, MAIN_WINDOW_TAB_SIDE_KEY, str, NULL);
			gconf_client_suggest_sync (prefs->client, NULL);
		}
	}
}


/* Show Chats Tree */
gboolean
prefs_get_main_window_show_chats_tree (void)
{
	return gconf_client_get_bool (prefs->client, MAIN_WINDOW_SHOW_TREE_KEY, NULL);
}


void
prefs_set_main_window_show_chats_tree (gboolean show_tree)
{
	if (gconf_client_key_is_writable (prefs->client, MAIN_WINDOW_SHOW_TREE_KEY, NULL))
	{
		gconf_client_set_bool (prefs->client, MAIN_WINDOW_SHOW_TREE_KEY, show_tree, NULL);
		gconf_client_suggest_sync (prefs->client, NULL);
	}
}


/* Chats Tree Size */
gint
prefs_get_main_window_chats_tree_size (void)
{
	return gconf_client_get_int (prefs->client, MAIN_WINDOW_TREE_SIZE_KEY, NULL);
}


void
prefs_set_main_window_chats_tree_size (gint size)
{
	if (gconf_client_key_is_writable (prefs->client, MAIN_WINDOW_TREE_SIZE_KEY, NULL)
		&& size != gconf_client_get_int (prefs->client, MAIN_WINDOW_TREE_SIZE_KEY, NULL))
	{
		gconf_client_set_int (prefs->client, MAIN_WINDOW_TREE_SIZE_KEY, size, NULL);
		gconf_client_suggest_sync (prefs->client, NULL);
	}
}


/* Show Users List */
gboolean
prefs_get_main_window_show_user_list (void)
{
	return gconf_client_get_bool (prefs->client, MAIN_WINDOW_SHOW_USERLIST_KEY, NULL);
}


void
prefs_set_main_window_show_user_list (gboolean show_list)
{
	if (gconf_client_key_is_writable (prefs->client, MAIN_WINDOW_SHOW_USERLIST_KEY, NULL))
	{
		gconf_client_set_bool (prefs->client, MAIN_WINDOW_SHOW_USERLIST_KEY, show_list, NULL);
		gconf_client_suggest_sync (prefs->client, NULL);
	}
}


/* User List Size */
gint
prefs_get_main_window_user_list_size (void)
{
	return gconf_client_get_int (prefs->client, MAIN_WINDOW_USERLIST_SIZE_KEY, NULL);
}


void
prefs_set_main_window_user_list_size (gint size)
{
	if (gconf_client_key_is_writable (prefs->client, MAIN_WINDOW_USERLIST_SIZE_KEY, NULL))
	{
		gconf_client_set_int (prefs->client, MAIN_WINDOW_USERLIST_SIZE_KEY, size, NULL);
		gconf_client_suggest_sync (prefs->client, NULL);
	}
}


/* Topic in titlebar */
gboolean
prefs_get_main_window_topic_in_titlebar (void)
{
	return gconf_client_get_bool (prefs->client, MAIN_WINDOW_TOPIC_IN_TITLEBAR_KEY, NULL);
}


/* Use GtkTextView for Main Entry
 * #FIXME: Remove this when EscoWindow uses new Paned. */
gboolean
prefs_get_use_multiline_entry (void)
{
	return gconf_client_get_bool (prefs->client, INTERFACE_USE_MULTILINE_ENTRY_KEY, NULL);
}


void
prefs_set_use_multiline_entry (gboolean use_it)
{
	gconf_client_set_bool (prefs->client, INTERFACE_USE_MULTILINE_ENTRY_KEY, use_it, NULL);
	gconf_client_suggest_sync (prefs->client, NULL);
}


/* **************************** *
 *  Connect Dialog Preferences  *
 * **************************** */

/* Show MOTD on connect */
gboolean
prefs_get_can_set_show_motd_on_connect (void)
{
	return gconf_client_key_is_writable (prefs->client, CONNECT_DIALOG_SHOW_MOTD_KEY, NULL);
}


gboolean
prefs_get_show_motd_on_connect (void)
{
	return gconf_client_get_bool (prefs->client, CONNECT_DIALOG_SHOW_MOTD_KEY, NULL);
}


void
prefs_set_show_motd_on_connect (gboolean show)
{
	if (gconf_client_key_is_writable (prefs->client, CONNECT_DIALOG_SHOW_MOTD_KEY, NULL))
	{
		gconf_client_set_bool (prefs->client, CONNECT_DIALOG_SHOW_MOTD_KEY, show, NULL);
		gconf_client_suggest_sync (prefs->client, NULL);
	}
}


/* Info Pane -- Should this die, or move to the "More Networks..." dialog? */
gboolean
prefs_get_connect_show_info (void)
{
	return gconf_client_get_bool (prefs->client, CONNECT_DIALOG_SHOW_INFO_KEY, NULL);
}


void
prefs_set_connect_show_info (gboolean show_info)
{
	if (gconf_client_key_is_writable (prefs->client, CONNECT_DIALOG_SHOW_INFO_KEY, NULL))
	{
		gconf_client_set_bool (prefs->client, CONNECT_DIALOG_SHOW_INFO_KEY, show_info, NULL);
		gconf_client_suggest_sync (prefs->client, NULL);
	}
}


gint
prefs_get_connect_info_size (void)
{
	return gconf_client_get_int (prefs->client, CONNECT_DIALOG_INFO_SIZE_KEY, NULL);
}


void
prefs_set_connect_info_size (gint size)
{
	if (gconf_client_key_is_writable (prefs->client, CONNECT_DIALOG_INFO_SIZE_KEY, NULL))
	{
		gconf_client_set_int (prefs->client, CONNECT_DIALOG_INFO_SIZE_KEY, size, NULL);
		gconf_client_suggest_sync (prefs->client, NULL);
	}
}


/* Selected Network */
gchar *
prefs_get_connect_selected_network (void)
{
	return gconf_client_get_string (prefs->client, CONNECT_DIALOG_SELECTED_NETWORK_KEY, NULL);
}


void
prefs_set_connect_selected_network (const gchar * uri)
{
	if (gconf_client_key_is_writable (prefs->client, CONNECT_DIALOG_SELECTED_NETWORK_KEY, NULL))
	{
		gconf_client_set_string (prefs->client, CONNECT_DIALOG_SELECTED_NETWORK_KEY, uri, NULL);
		gconf_client_suggest_sync (prefs->client, NULL);
	}
}


/* ******************************** *
 *  Preferences Dialog Preferences  *
 * ******************************** */

/* Prefs Dialog - Windows Page - "More Options" Disclosure */
gboolean
prefs_get_prefs_show_window_options (void)
{
	return gconf_client_get_bool (prefs->client, PREFS_DIALOG_SHOW_WINDOW_OPTIONS_KEY, NULL);
}


void
prefs_set_prefs_show_window_options (gboolean show)
{
	if (gconf_client_key_is_writable (prefs->client, PREFS_DIALOG_SHOW_WINDOW_OPTIONS_KEY, NULL))
	{
		gconf_client_set_bool (prefs->client, PREFS_DIALOG_SHOW_WINDOW_OPTIONS_KEY, show, NULL);
		gconf_client_suggest_sync (prefs->client, NULL);
	}
}


/* ************************ *
 * Find Dialog Preferences  *
 * ************************ */

/* Use Case Insensitive Find */
gboolean
prefs_get_case_insensitive_find (void)
{
	return gconf_client_get_bool (prefs->client, FIND_IS_CASE_INSENSITIVE_KEY, NULL);
}


void
prefs_set_case_insensitive_find (gboolean insensitive)
{
	gconf_client_set_bool (prefs->client, FIND_IS_CASE_INSENSITIVE_KEY, insensitive, NULL);
	gconf_client_suggest_sync (prefs->client, NULL);
}


/* ******************* *
 *  Alert Preferences  *
 * ******************* */

static inline const gchar *
get_alert_key (GnomechatAlertID id)
{
	switch (id)
	{
	case GNOMECHAT_ALERT_CLEAR_CHAT:
		return ASK_TO_CLEAR_CHAT_KEY;

	case GNOMECHAT_ALERT_REMOVE_NETWORK:
		return ASK_TO_REMOVE_NETWORK_KEY;

	case GNOMECHAT_ALERT_REMOVE_SERVER:
		return ASK_TO_REMOVE_SERVER_KEY;

	default:
		break;
	}

	g_return_val_if_reached (NULL);
}


gboolean
prefs_get_can_set_show_alert (GnomechatAlertID id)
{
	g_return_val_if_fail (id >= GNOMECHAT_ALERT_FIRST && id < GNOMECHAT_ALERT_LAST, FALSE);

	return gconf_client_key_is_writable (prefs->client, get_alert_key (id), NULL);
}


gboolean
prefs_get_show_alert (GnomechatAlertID id)
{
	g_return_val_if_fail (id >= GNOMECHAT_ALERT_FIRST && id < GNOMECHAT_ALERT_LAST, FALSE);

	return gconf_client_get_bool (prefs->client, get_alert_key (id), NULL);
}


void
prefs_set_show_alert (GnomechatAlertID id, gboolean show)
{
	const gchar *key;

	g_return_if_fail (id >= GNOMECHAT_ALERT_FIRST && id < GNOMECHAT_ALERT_LAST);

	key = get_alert_key (id);

	if (gconf_client_key_is_writable (prefs->client, key, NULL))
	{
		gconf_client_set_bool (prefs->client, key, show, NULL);
		gconf_client_suggest_sync (prefs->client, NULL);
	}
}


/* ************************** *
 *  Notification Preferences  *
 * ************************** */

/* Notification Colors */
static inline const gchar *
get_notification_color_key (EscoPesterType pester)
{
	switch (pester)
	{
	case ESCO_PESTER_MSG:
		return NOTIFY_COLOR_MSG_KEY;

	default:
		return NOTIFY_COLOR_TEXT_KEY;
	}
}


gboolean
prefs_get_can_set_notification_color (EscoPesterType pester)
{
	const gchar *key;

	g_return_val_if_fail (pester >= ESCO_PESTER_TEXT && pester <= ESCO_PESTER_LAST, FALSE);

	key = get_notification_color_key (pester);

	return gconf_client_key_is_writable (prefs->client, key, NULL);
}


gchar *
prefs_get_notification_color (EscoPesterType pester)
{
	const gchar *key;
	gchar *prefs_color = NULL;
	GdkColor color;

	g_return_val_if_fail (pester >= ESCO_PESTER_TEXT && pester <= ESCO_PESTER_LAST, FALSE);

	key = get_notification_color_key (pester);
	prefs_color = gconf_client_get_string (prefs->client, key, NULL);

	if (prefs_color == NULL || !gdk_color_parse (prefs_color, &color))
	{
		if (pester == ESCO_PESTER_TEXT)
			prefs_color = g_strdup ("#0000FF");
		else
			prefs_color = g_strdup ("#FF0000");

		if (gconf_client_key_is_writable (prefs->client, key, NULL))
		{
			gconf_client_set_string (prefs->client, key, prefs_color, NULL);
			gconf_client_suggest_sync (prefs->client, NULL);
		}
	}

	return prefs_color;
}


void
prefs_set_notification_color (EscoPesterType pester, const gchar * color_str)
{
	const gchar *key;
	GdkColor color;

	g_return_if_fail (pester >= ESCO_PESTER_TEXT && pester <= ESCO_PESTER_LAST);

	if (color_str == NULL || !gdk_color_parse (color_str, &color))
	{
		if (pester == ESCO_PESTER_TEXT)
			color_str = "#0000FF";
		else
			color_str = "#FF0000";
	}

	key = get_notification_color_key (pester);

	if (gconf_client_key_is_writable (prefs->client, key, NULL)
		&& !compare_str_with_value (color_str, key))
	{
		gconf_client_set_string (prefs->client, key, color_str, NULL);
		gconf_client_suggest_sync (prefs->client, NULL);
	}
}


/* Status Tray */
gboolean
prefs_get_can_set_show_tray_icon (void)
{
	return gconf_client_key_is_writable (prefs->client, NOTIFY_USE_TRAY_KEY, NULL);
}


gboolean
prefs_get_show_tray_icon (void)
{
	return gconf_client_get_bool (prefs->client, NOTIFY_USE_TRAY_KEY, NULL);
}


void
prefs_set_show_tray_icon (gboolean show)
{
	gconf_client_set_bool (prefs->client, NOTIFY_USE_TRAY_KEY, show, NULL);
	gconf_client_suggest_sync (prefs->client, NULL);
}


/* ******************** *
 *  Viewer Preferences  *
 * ******************** */

/* Timestamps */
gboolean
prefs_get_can_set_show_timestamps (void)
{
	return gconf_client_key_is_writable (prefs->client, VIEWER_SHOW_TIMESTAMPS_KEY, NULL);
}


gboolean
prefs_get_viewer_show_timestamps (void)
{
	return gconf_client_get_bool (prefs->client, VIEWER_SHOW_TIMESTAMPS_KEY, NULL);
}


void
prefs_set_viewer_show_timestamps (gboolean show_timestamps)
{
	gconf_client_set_bool (prefs->client, VIEWER_SHOW_TIMESTAMPS_KEY, show_timestamps, NULL);
	gconf_client_suggest_sync (prefs->client, NULL);
}


/* Image Smileys */
gboolean
prefs_get_can_set_use_image_smileys (void)
{
	return gconf_client_key_is_writable (prefs->client, VIEWER_USE_IMAGE_SMILEYS_KEY, NULL);
}


gboolean
prefs_get_use_image_smileys (void)
{
	return gconf_client_get_bool (prefs->client, VIEWER_USE_IMAGE_SMILEYS_KEY, NULL);
}


void
prefs_set_use_image_smileys (gboolean use_image_smileys)
{
	gconf_client_set_bool (prefs->client, VIEWER_USE_IMAGE_SMILEYS_KEY, use_image_smileys, NULL);
	gconf_client_suggest_sync (prefs->client, NULL);
}


/* ********************** *
 *  Identity Preferences  *
 * ********************** */

/* Nickname */
gboolean
prefs_get_can_set_nick (void)
{
	return gconf_client_key_is_writable (prefs->client, IDENTITY_NICK_KEY, NULL);
}


gchar *
prefs_get_nick (void)
{
	gchar *retval = gconf_client_get_string (prefs->client, IDENTITY_NICK_KEY, NULL);

	if (retval == NULL)
	{
		retval = g_strdup (g_getenv ("IRCNICK"));

		if (retval == NULL)
		{
			retval = g_strdup (g_get_user_name ());

			if (retval == NULL)
			{
				retval = g_strdup ("GCUser");
			}
		}

		if (gconf_client_key_is_writable (prefs->client, IDENTITY_NICK_KEY, NULL))
		{
			gconf_client_set_string (prefs->client, IDENTITY_NICK_KEY, retval, NULL);
			gconf_client_suggest_sync (prefs->client, NULL);
		}
	}

	return retval;
}


/* Username */
gboolean
prefs_get_can_set_username (void)
{
	return gconf_client_key_is_writable (prefs->client, IDENTITY_USER_NAME_KEY, NULL);
}


gchar *
prefs_get_username (void)
{
	gchar *retval = gconf_client_get_string (prefs->client, IDENTITY_USER_NAME_KEY, NULL);

	if (retval == NULL)
	{
		retval = g_strdup (g_get_user_name ());

		if (retval == NULL)
		{
			retval = g_strdup ("GCUser");
		}

		if (gconf_client_key_is_writable (prefs->client, IDENTITY_USER_NAME_KEY, NULL))
		{
			gconf_client_set_string (prefs->client, IDENTITY_USER_NAME_KEY, retval, NULL);
			gconf_client_suggest_sync (prefs->client, NULL);
		}
	}

	return retval;
}


/* Real name */
gboolean
prefs_get_can_set_realname (void)
{
	return gconf_client_key_is_writable (prefs->client, IDENTITY_REAL_NAME_KEY, NULL);
}


gchar *
prefs_get_realname (void)
{
	gchar *retval = gconf_client_get_string (prefs->client, IDENTITY_REAL_NAME_KEY, NULL);

	if (retval == NULL)
	{
		retval = g_strdup (g_getenv ("IRCNAME"));

		if (retval == NULL)
		{
			retval = g_strdup (g_get_user_name ());

			if (retval == NULL)
			{
				retval = g_strdup ("GnomeChat User");
			}
		}

		if (gconf_client_key_is_writable (prefs->client, IDENTITY_REAL_NAME_KEY, NULL))
		{
			gconf_client_set_string (prefs->client, IDENTITY_REAL_NAME_KEY, retval, NULL);
			gconf_client_suggest_sync (prefs->client, NULL);
		}
	}

	return retval;
}


/* Photo File */
gboolean
prefs_get_can_set_photo_file (void)
{
	return gconf_client_key_is_writable (prefs->client, IDENTITY_PHOTO_KEY, NULL);
}


gchar *
prefs_get_photo_file (void)
{
	return gconf_client_get_string (prefs->client, IDENTITY_PHOTO_KEY, NULL);
}


/* The whole shebang */
void
prefs_set_identity (const gchar * nick,
					const gchar * realname, const gchar * username, const gchar * photo_file)
{
	gchar *default_photo_file;

	g_return_if_fail (util_str_is_not_empty (nick));
	g_return_if_fail (util_str_is_not_empty (realname));
	g_return_if_fail (util_str_is_not_empty (username));

	gconf_client_set_string (prefs->client, IDENTITY_NICK_KEY, nick, NULL);
	gconf_client_set_string (prefs->client, IDENTITY_REAL_NAME_KEY, realname, NULL);
	gconf_client_set_string (prefs->client, IDENTITY_USER_NAME_KEY, username, NULL);

	if (photo_file != NULL)
	{
		gconf_client_set_string (prefs->client, IDENTITY_PHOTO_KEY, photo_file, NULL);
	}
	else
	{
		default_photo_file = g_build_filename (g_get_home_dir (), ".gnome", "photo", NULL);
		gconf_client_set_string (prefs->client, IDENTITY_PHOTO_KEY, default_photo_file, NULL);
	}

	gconf_client_suggest_sync (prefs->client, NULL);
}


/* ***************** *
 *  DCC Preferences  *
 * ***************** */

/* DCC Received Files Naming Style */
gboolean
prefs_get_can_set_dcc_receieved_files_naming (void)
{
	return gconf_client_key_is_writable (prefs->client, DCC_RECV_NAMING_KEY, NULL);
}


GDccRecvFileNamingStyle
prefs_get_dcc_received_files_naming (void)
{
	gchar *str;
	gint style = DEFAULT_NAMING_STYLE;
	gboolean found = FALSE;

	str = gconf_client_get_string (prefs->client, DCC_RECV_NAMING_KEY, NULL);

	if (str != NULL)
	{
		found = gconf_string_to_enum ((GConfEnumStringPair *) recv_naming_table, str, &style);
		g_free (str);
	}


	if (!found)
	{
		style = DEFAULT_NAMING_STYLE;

		if (gconf_client_key_is_writable (prefs->client, DCC_RECV_NAMING_KEY, NULL))
		{
			gconf_client_set_string (prefs->client, DCC_RECV_NAMING_KEY,
									 gconf_enum_to_string ((GConfEnumStringPair *)
														   recv_naming_table, style), NULL);
			gconf_client_suggest_sync (prefs->client, NULL);
		}
	}

	return style;
}


void
prefs_set_dcc_received_files_naming (GDccRecvFileNamingStyle style)
{
	g_return_if_fail (style >= 0 && style < GDCC_RECV_FILE_LAST);

	if (gconf_client_key_is_writable (prefs->client, DCC_RECV_NAMING_KEY, NULL))
	{
		const gchar *str;

		str = gconf_enum_to_string ((GConfEnumStringPair *) recv_naming_table, style);

		if (str == NULL)
		{
			str = gconf_enum_to_string ((GConfEnumStringPair *) recv_naming_table,
										DEFAULT_NAMING_STYLE);
		}

		gconf_client_set_string (prefs->client, DCC_RECV_NAMING_KEY, str, NULL);
		gconf_client_suggest_sync (prefs->client, NULL);
	}
}



/* DCC Received Files Location */
gboolean
prefs_get_can_set_dcc_receieve_folder (void)
{
	return gconf_client_key_is_writable (prefs->client, DCC_RECV_FOLDER_KEY, NULL);
}


gchar *
prefs_get_dcc_receive_folder (void)
{
	return gconf_client_get_string (prefs->client, DCC_RECV_FOLDER_KEY, NULL);
}


void
prefs_set_dcc_receive_folder (const gchar * path)
{
	if (gconf_client_key_is_writable (prefs->client, DCC_RECV_FOLDER_KEY, NULL))
	{
		const gchar *real_path;

		if (!util_str_is_not_empty (path)
			|| !g_file_test (path, (G_FILE_TEST_EXISTS | G_FILE_TEST_IS_DIR)))
		{
			real_path = g_get_home_dir ();
		}
		else
		{
			real_path = path;
		}

		gconf_client_set_string (prefs->client, DCC_RECV_FOLDER_KEY, path, NULL);
		gconf_client_suggest_sync (prefs->client, NULL);
	}
}


/* DCC Send Browse Location */
gchar *
prefs_get_dcc_send_folder (void)
{
	gchar *path;

	path = gconf_client_get_string (prefs->client, DCC_SEND_FOLDER_KEY, NULL);

	if (!util_str_is_not_empty (path)
		|| !g_file_test (path, (G_FILE_TEST_EXISTS | G_FILE_TEST_IS_DIR)))
	{
		g_free (path);

		path = g_strdup (g_get_home_dir ());

		if (gconf_client_key_is_writable (prefs->client, DCC_SEND_FOLDER_KEY, NULL))
		{
			gconf_client_set_string (prefs->client, DCC_SEND_FOLDER_KEY, path, NULL);
			gconf_client_suggest_sync (prefs->client, NULL);
		}
	}

	return path;
}


void
prefs_set_dcc_send_folder (const gchar * path)
{
	const gchar *real_path;

	if (gconf_client_key_is_writable (prefs->client, DCC_SEND_FOLDER_KEY, NULL))
	{
		if (!util_str_is_not_empty (path)
			|| !g_file_test (path, (G_FILE_TEST_EXISTS | G_FILE_TEST_IS_DIR)))
		{
			real_path = g_get_home_dir ();
		}
		else
		{
			real_path = path;
		}

		gconf_client_set_string (prefs->client, DCC_SEND_FOLDER_KEY, real_path, NULL);
		gconf_client_suggest_sync (prefs->client, NULL);
	}
}


/* ************************************************************************** *
 *  Busted from here on out...                                                *
 * ************************************************************************** */

/* Global Viewer Style */
gboolean
prefs_get_can_set_viewer_font (void)
{
	gchar *key = gconf_concat_dir_and_key (VIEWER_GLOBAL_DIR, FONT_KEY_BIT);
	gboolean retval;

	retval = gconf_client_key_is_writable (prefs->client, key, NULL);
	g_free (key);

	return retval;
}


gboolean
prefs_get_can_set_viewer_fg (void)
{
	gchar *key = gconf_concat_dir_and_key (VIEWER_GLOBAL_DIR, FG_KEY_BIT);
	gboolean retval;

	retval = gconf_client_key_is_writable (prefs->client, key, NULL);
	g_free (key);

	return retval;
}


gboolean
prefs_get_can_set_viewer_bg (void)
{
	gchar *key = gconf_concat_dir_and_key (VIEWER_GLOBAL_DIR, BG_KEY_BIT);
	gboolean retval;

	retval = gconf_client_key_is_writable (prefs->client, key, NULL);
	g_free (key);

	return retval;
}


gchar *
prefs_get_viewer_font (void)
{
	return prefs_get_viewer_global (FONT_KEY_BIT);
}


gchar *
prefs_get_viewer_fg (void)
{
	GdkColor color;
	gchar *prefs_color = NULL;

	prefs_color = prefs_get_viewer_global (FG_KEY_BIT);

	if (!prefs_color || !gdk_color_parse (prefs_color, &color))
	{
		prefs_color = g_strdup ("#000000");
	}

	return prefs_color;
}


gchar *
prefs_get_viewer_bg (void)
{
	GdkColor color;
	gchar *prefs_color = NULL;

	prefs_color = prefs_get_viewer_global (BG_KEY_BIT);

	if (!prefs_color || !gdk_color_parse (prefs_color, &color))
	{
		prefs_color = g_strdup ("#FFFFFF");
	}

	return prefs_color;
}


void
prefs_set_viewer_font (const gchar * font)
{
	gchar *key = gconf_concat_dir_and_key (VIEWER_GLOBAL_DIR, FONT_KEY_BIT);

	gconf_client_set_string (prefs->client, key, font, NULL);
	gconf_client_suggest_sync (prefs->client, NULL);

	g_free (key);
}


void
prefs_set_viewer_fg (const gchar * fg_color)
{
	gchar *key = gconf_concat_dir_and_key (VIEWER_GLOBAL_DIR, FG_KEY_BIT);

	gconf_client_set_string (prefs->client, key, fg_color, NULL);
	gconf_client_suggest_sync (prefs->client, NULL);

	g_free (key);
}


void
prefs_set_viewer_bg (const gchar * bg_color)
{
	gchar *key = gconf_concat_dir_and_key (VIEWER_GLOBAL_DIR, BG_KEY_BIT);

	gconf_client_set_string (prefs->client, key, bg_color, NULL);
	gconf_client_suggest_sync (prefs->client, NULL);

	g_free (key);
}


/* Other Viewer Styles */
gboolean
prefs_get_can_set_viewer_style_fg (ViewerStyleType type)
{
	gchar *key = get_style_key (type, FG_KEY_BIT);
	gboolean retval;

	retval = gconf_client_key_is_writable (prefs->client, key, NULL);
	g_free (key);

	return retval;
}


gboolean
prefs_get_can_set_viewer_style_bg (ViewerStyleType type)
{
	gchar *key = get_style_key (type, BG_KEY_BIT);
	gboolean retval;

	retval = gconf_client_key_is_writable (prefs->client, key, NULL);
	g_free (key);

	return retval;
}


gboolean
prefs_get_can_set_viewer_style_bold (ViewerStyleType type)
{
	gchar *key = get_style_key (type, BOLD_KEY_BIT);
	gboolean retval;

	retval = gconf_client_key_is_writable (prefs->client, key, NULL);
	g_free (key);

	return retval;
}


gboolean
prefs_get_can_set_viewer_style_italic (ViewerStyleType type)
{
	gchar *key = get_style_key (type, ITALIC_KEY_BIT);
	gboolean retval;

	retval = gconf_client_key_is_writable (prefs->client, key, NULL);
	g_free (key);

	return retval;
}


gboolean
prefs_get_can_set_viewer_style_uline (ViewerStyleType type)
{
	gchar *key = get_style_key (type, ULINE_KEY_BIT);
	gboolean retval;

	retval = gconf_client_key_is_writable (prefs->client, key, NULL);
	g_free (key);

	return retval;
}


gchar *
prefs_get_viewer_style_fg (ViewerStyleType type)
{
	return g_strdup (prefs->viewer_styles[type]->fg_color);
}


gchar *
prefs_get_viewer_style_bg (ViewerStyleType type)
{
	return g_strdup (prefs->viewer_styles[type]->bg_color);
}


gboolean
prefs_get_viewer_style_bold (ViewerStyleType type)
{
	return prefs->viewer_styles[type]->bold;
}


gboolean
prefs_get_viewer_style_italic (ViewerStyleType type)
{
	return prefs->viewer_styles[type]->italic;
}


gboolean
prefs_get_viewer_style_uline (ViewerStyleType type)
{
	return prefs->viewer_styles[type]->uline;
}


void
prefs_set_viewer_style_fg (ViewerStyleType type, const gchar * fg_color)
{
	gchar *key = get_style_key (type, FG_KEY_BIT);

	gconf_client_set_string (prefs->client, key, fg_color, NULL);
	gconf_client_suggest_sync (prefs->client, NULL);

	g_free (key);
}


void
prefs_set_viewer_style_bg (ViewerStyleType type, const gchar * bg_color)
{
	gchar *key = get_style_key (type, BG_KEY_BIT);

	gconf_client_set_string (prefs->client, key, bg_color, NULL);
	gconf_client_suggest_sync (prefs->client, NULL);

	g_free (key);
}


void
prefs_set_viewer_style_bold (ViewerStyleType type, gboolean bold)
{
	gchar *key = get_style_key (type, BOLD_KEY_BIT);

	gconf_client_set_bool (prefs->client, key, bold, NULL);
	gconf_client_suggest_sync (prefs->client, NULL);

	g_free (key);
}


void
prefs_set_viewer_style_italic (ViewerStyleType type, gboolean italic)
{
	gchar *key = get_style_key (type, ITALIC_KEY_BIT);

	gconf_client_set_bool (prefs->client, key, italic, NULL);
	gconf_client_suggest_sync (prefs->client, NULL);

	g_free (key);
}


void
prefs_set_viewer_style_uline (ViewerStyleType type, gboolean uline)
{
	gchar *key = get_style_key (type, ULINE_KEY_BIT);

	gconf_client_set_bool (prefs->client, key, uline, NULL);
	gconf_client_suggest_sync (prefs->client, NULL);

	g_free (key);
}


ViewerStyle *
prefs_get_viewer_style (ViewerStyleType type)
{
	return prefs->viewer_styles[type];
}


/* Network Tree List Functions */
gboolean
prefs_get_can_set_network_ids (void)
{
	return gconf_client_key_is_writable (prefs->client, NETWORK_LIST_KEY, NULL);
}


GSList *
prefs_get_network_ids (void)
{
	return gconf_client_get_list (prefs->client, NETWORK_LIST_KEY, GCONF_VALUE_STRING, NULL);
}


void
prefs_set_network_ids (GSList * ids)
{
	gconf_client_set_list (prefs->client, NETWORK_LIST_KEY, GCONF_VALUE_STRING, ids, NULL);
	gconf_client_suggest_sync (prefs->client, NULL);
}


void
prefs_delete_network (const gchar * id)
{
	GSList *ids = NULL;
	gchar *dir;

	/* Delete the servers */
	for (ids = prefs_get_network_server_ids (id);
		 ids != NULL; ids = g_slist_remove (ids, ids->data))
	{
		prefs_delete_server (id, (gchar *) ids->data);
		g_free (ids->data);
	}

	dir = g_build_filename (NETWORK_DIR, id, NULL);
	unset_keys_in_dir (dir);
	g_free (dir);
}


gboolean
prefs_get_can_set_network_name (const gchar * id)
{
	gchar *key;
	gboolean value;

	key = g_build_filename (NETWORK_DIR, id, NAME_KEY_BIT, NULL);
	value = gconf_client_key_is_writable (prefs->client, key, NULL);
	g_free (key);

	return value;
}


gchar *
prefs_get_network_name (const gchar * id)
{
	gchar *key,
	 *value;

	key = g_build_filename (NETWORK_DIR, id, NAME_KEY_BIT, NULL);
	value = gconf_client_get_string (prefs->client, key, NULL);
	g_free (key);

	return value;
}


void
prefs_set_network_name (const gchar * id, const gchar * value)
{
	gchar *key;

	key = g_build_filename (NETWORK_DIR, id, NAME_KEY_BIT, NULL);
	gconf_client_set_string (prefs->client, key, value, NULL);
	gconf_client_suggest_sync (prefs->client, NULL);
	g_free (key);
}


gboolean
prefs_get_can_set_network_description (const gchar * id)
{
	gchar *key;
	gboolean value;

	key = g_build_filename (NETWORK_DIR, id, DESCRIPTION_KEY_BIT, NULL);
	value = gconf_client_key_is_writable (prefs->client, key, NULL);
	g_free (key);

	return value;
}


gchar *
prefs_get_network_description (const gchar * id)
{
	gchar *key,
	 *value;

	key = g_build_filename (NETWORK_DIR, id, DESCRIPTION_KEY_BIT, NULL);
	value = gconf_client_get_string (prefs->client, key, NULL);
	g_free (key);

	return value;
}


void
prefs_set_network_description (const gchar * id, const gchar * value)
{
	gchar *key;

	key = g_build_filename (NETWORK_DIR, id, DESCRIPTION_KEY_BIT, NULL);
	gconf_client_set_string (prefs->client, key, value, NULL);
	gconf_client_suggest_sync (prefs->client, NULL);
	g_free (key);
}


gboolean
prefs_get_can_set_network_website (const gchar * id)
{
	gchar *key;
	gboolean value;

	key = g_build_filename (NETWORK_DIR, id, WEBSITE_KEY_BIT, NULL);
	value = gconf_client_key_is_writable (prefs->client, key, NULL);
	g_free (key);

	return value;
}


gchar *
prefs_get_network_website (const gchar * id)
{
	gchar *key,
	 *value;

	key = g_build_filename (NETWORK_DIR, id, WEBSITE_KEY_BIT, NULL);
	value = gconf_client_get_string (prefs->client, key, NULL);
	g_free (key);

	return value;
}


void
prefs_set_network_website (const gchar * id, const gchar * value)
{
	gchar *key;

	key = g_build_filename (NETWORK_DIR, id, WEBSITE_KEY_BIT, NULL);
	gconf_client_set_string (prefs->client, key, value, NULL);
	gconf_client_suggest_sync (prefs->client, NULL);
	g_free (key);
}


gboolean
prefs_get_can_set_network_category (const gchar * id)
{
	gchar *key;
	gboolean retval;

	key = g_build_filename (NETWORK_DIR, id, CATEGORY_KEY_BIT, NULL);
	retval = gconf_client_key_is_writable (prefs->client, key, NULL);
	g_free (key);

	return retval;
}


gchar *
prefs_get_network_category (const gchar * id)
{
	gchar *key,
	 *retval;

	key = g_build_filename (NETWORK_DIR, id, CATEGORY_KEY_BIT, NULL);
	retval = gconf_client_get_string (prefs->client, key, NULL);
	g_free (key);

	return retval;
}


void
prefs_set_network_category (const gchar * id, const gchar * value)
{
	gchar *key;

	key = g_build_filename (NETWORK_DIR, id, CATEGORY_KEY_BIT, NULL);
	gconf_client_set_string (prefs->client, key, value, NULL);
	gconf_client_suggest_sync (prefs->client, NULL);
	g_free (key);
}


gboolean
prefs_get_can_set_network_server_ids (const gchar * id)
{
	gchar *key;
	gboolean value;

	key = g_build_filename (NETWORK_DIR, id, SERVER_IDS_KEY_BIT, NULL);
	value = gconf_client_key_is_writable (prefs->client, key, NULL);
	g_free (key);

	return value;
}


GSList *
prefs_get_network_server_ids (const gchar * id)
{
	gchar *key;
	GSList *value;

	key = g_build_filename (NETWORK_DIR, id, SERVER_IDS_KEY_BIT, NULL);
	value = gconf_client_get_list (prefs->client, key, GCONF_VALUE_STRING, NULL);
	g_free (key);

	return value;
}


void
prefs_set_network_server_ids (const gchar * id, GSList * value)
{
	gchar *key;

	key = g_build_filename (NETWORK_DIR, id, SERVER_IDS_KEY_BIT, NULL);
	gconf_client_set_list (prefs->client, key, GCONF_VALUE_STRING, value, NULL);
	gconf_client_suggest_sync (prefs->client, NULL);
	g_free (key);
}


gboolean
prefs_get_can_set_network_channels (const gchar * id)
{
	gchar *key;
	gboolean value;

	key = g_build_filename (NETWORK_DIR, id, CHANNELS_KEY_BIT, NULL);
	value = gconf_client_key_is_writable (prefs->client, key, NULL);
	g_free (key);

	return value;
}


GSList *
prefs_get_network_channels (const gchar * id)
{
	gchar *key;
	GSList *value;

	key = g_build_filename (NETWORK_DIR, id, CHANNELS_KEY_BIT, NULL);
	value = gconf_client_get_list (prefs->client, key, GCONF_VALUE_STRING, NULL);
	g_free (key);

	return value;
}


void
prefs_set_network_channels (const gchar * id, GSList * value)
{
	gchar *key;

	key = g_build_filename (NETWORK_DIR, id, CHANNELS_KEY_BIT, NULL);
	gconf_client_set_list (prefs->client, key, GCONF_VALUE_STRING, value, NULL);
	gconf_client_suggest_sync (prefs->client, NULL);
	g_free (key);
}


gboolean
prefs_get_can_set_network_autoconnect (const gchar * id)
{
	gchar *key;
	gboolean value;

	key = g_build_filename (NETWORK_DIR, id, AUTOCONNECT_KEY_BIT, NULL);
	value = gconf_client_key_is_writable (prefs->client, key, NULL);
	g_free (key);

	return value;
}


gboolean
prefs_get_network_autoconnect (const gchar * id)
{
	gchar *key;
	gboolean value;

	key = g_build_filename (NETWORK_DIR, id, AUTOCONNECT_KEY_BIT, NULL);
	value = gconf_client_get_bool (prefs->client, key, NULL);
	g_free (key);

	return value;
}


void
prefs_set_network_autoconnect (const gchar * id, gboolean value)
{
	gchar *key;

	key = g_build_filename (NETWORK_DIR, id, AUTOCONNECT_KEY_BIT, NULL);
	gconf_client_set_bool (prefs->client, key, value, NULL);
	gconf_client_suggest_sync (prefs->client, NULL);
	g_free (key);
}


gboolean
prefs_get_can_set_network_charset (const gchar * id)
{
	gchar *key;
	gboolean value;

	key = g_build_filename (NETWORK_DIR, id, CHARSET_KEY_BIT, NULL);
	value = gconf_client_key_is_writable (prefs->client, key, NULL);
	g_free (key);

	return value;
}


gchar *
prefs_get_network_charset (const gchar * id)
{
	gchar *key,
	 *value;

	key = g_build_filename (NETWORK_DIR, id, CHARSET_KEY_BIT, NULL);
	value = gconf_client_get_string (prefs->client, key, NULL);
	g_free (key);

	return value;
}


void
prefs_set_network_charset (const gchar * id, const gchar * value)
{
	gchar *key;

	key = g_build_filename (NETWORK_DIR, id, CHARSET_KEY_BIT, NULL);
	gconf_client_set_string (prefs->client, key, value, NULL);
	gconf_client_suggest_sync (prefs->client, NULL);
	g_free (key);
}


gboolean
prefs_get_can_set_network_icon (const gchar * id)
{
	gchar *key;
	gboolean value;

	key = g_build_filename (NETWORK_DIR, id, ICON_KEY_BIT, NULL);
	value = gconf_client_key_is_writable (prefs->client, key, NULL);
	g_free (key);

	return value;
}


gchar *
prefs_get_network_icon (const gchar * id)
{
	gchar *key,
	 *value;

	key = g_build_filename (NETWORK_DIR, id, ICON_KEY_BIT, NULL);
	value = gconf_client_get_string (prefs->client, key, NULL);
	g_free (key);

	return value;
}


void
prefs_set_network_icon (const gchar * id, const gchar * value)
{
	gchar *key;

	key = g_build_filename (NETWORK_DIR, id, ICON_KEY_BIT, NULL);
	gconf_client_set_string (prefs->client, key, value, NULL);
	gconf_client_suggest_sync (prefs->client, NULL);
	g_free (key);
}


gboolean
prefs_get_can_set_category_name (const gchar * id)
{
	gchar *key;
	gboolean value;

	key = g_build_filename (CATEGORY_DIR, id, NAME_KEY_BIT, NULL);
	value = gconf_client_key_is_writable (prefs->client, key, NULL);
	g_free (key);

	return value;
}


gchar *
prefs_get_category_name (const gchar * id)
{
	gchar *key,
	 *value;

	key = g_build_filename (CATEGORY_DIR, id, NAME_KEY_BIT, NULL);
	value = gconf_client_get_string (prefs->client, key, NULL);
	g_free (key);

	return value;
}


void
prefs_set_category_name (const gchar * id, const gchar * value)
{
	gchar *key;

	key = g_build_filename (CATEGORY_DIR, id, NAME_KEY_BIT, NULL);
	gconf_client_set_string (prefs->client, key, value, NULL);
	gconf_client_suggest_sync (prefs->client, NULL);
	g_free (key);
}


gboolean
prefs_get_can_set_category_description (const gchar * id)
{
	gchar *key;
	gboolean value;

	key = g_build_filename (CATEGORY_DIR, id, DESCRIPTION_KEY_BIT, NULL);
	value = gconf_client_key_is_writable (prefs->client, key, NULL);
	g_free (key);

	return value;
}


gchar *
prefs_get_category_description (const gchar * id)
{
	gchar *key,
	 *value;

	key = g_build_filename (CATEGORY_DIR, id, DESCRIPTION_KEY_BIT, NULL);
	value = gconf_client_get_string (prefs->client, key, NULL);
	g_free (key);

	return value;
}


void
prefs_set_category_description (const gchar * id, const gchar * value)
{
	gchar *key;

	key = g_build_filename (CATEGORY_DIR, id, DESCRIPTION_KEY_BIT, NULL);
	gconf_client_set_string (prefs->client, key, value, NULL);
	gconf_client_suggest_sync (prefs->client, NULL);
	g_free (key);
}


gboolean
prefs_get_can_set_category_icon (const gchar * id)
{
	gchar *key;
	gboolean value;

	key = g_build_filename (CATEGORY_DIR, id, ICON_KEY_BIT, NULL);
	value = gconf_client_key_is_writable (prefs->client, key, NULL);
	g_free (key);

	return value;
}


gchar *
prefs_get_category_icon (const gchar * id)
{
	gchar *key,
	 *value;

	key = g_build_filename (CATEGORY_DIR, id, ICON_KEY_BIT, NULL);
	value = gconf_client_get_string (prefs->client, key, NULL);
	g_free (key);

	return value;
}


void
prefs_set_category_icon (const gchar * id, const gchar * value)
{
	gchar *key;

	key = g_build_filename (CATEGORY_DIR, id, ICON_KEY_BIT, NULL);
	gconf_client_set_string (prefs->client, key, value, NULL);
	gconf_client_suggest_sync (prefs->client, NULL);
	g_free (key);
}


void
prefs_delete_server (const gchar * net_id, const gchar * server_id)
{
	gchar *dir;

	dir = g_build_filename (NETWORK_DIR, net_id, server_id, NULL);
	unset_keys_in_dir (dir);
	g_free (dir);
}


gboolean
prefs_get_can_set_server_enabled (const gchar * net_id, const gchar * id)
{
	gchar *key;
	gboolean value;

	key = g_build_filename (NETWORK_DIR, net_id, id, ENABLED_KEY_BIT, NULL);
	value = gconf_client_key_is_writable (prefs->client, key, NULL);
	g_free (key);

	return value;
}


gboolean
prefs_get_server_enabled (const gchar * net_id, const gchar * id)
{
	gchar *key;
	gboolean value;

	key = g_build_filename (NETWORK_DIR, net_id, id, ENABLED_KEY_BIT, NULL);
	value = gconf_client_get_bool (prefs->client, key, NULL);
	g_free (key);

	return value;
}


void
prefs_set_server_enabled (const gchar * net_id, const gchar * id, gboolean value)
{
	gchar *key;

	key = g_build_filename (NETWORK_DIR, net_id, id, ENABLED_KEY_BIT, NULL);
	gconf_client_set_bool (prefs->client, key, value, NULL);
	gconf_client_suggest_sync (prefs->client, NULL);
	g_free (key);
}


gboolean
prefs_get_can_set_server_address (const gchar * net_id, const gchar * id)
{
	gchar *key;
	gboolean value;

	key = g_build_filename (NETWORK_DIR, net_id, id, NAME_KEY_BIT, NULL);
	value = gconf_client_key_is_writable (prefs->client, key, NULL);
	g_free (key);

	return value;
}


gchar *
prefs_get_server_address (const gchar * net_id, const gchar * id)
{
	gchar *key,
	 *value;

	key = g_build_filename (NETWORK_DIR, net_id, id, ADDRESS_KEY_BIT, NULL);
	value = gconf_client_get_string (prefs->client, key, NULL);
	g_free (key);

	return value;
}


void
prefs_set_server_address (const gchar * net_id, const gchar * id, const gchar * value)
{
	gchar *key;

	key = g_build_filename (NETWORK_DIR, net_id, id, ADDRESS_KEY_BIT, NULL);
	gconf_client_set_string (prefs->client, key, value, NULL);
	gconf_client_suggest_sync (prefs->client, NULL);
	g_free (key);
}


gboolean
prefs_get_can_set_server_port (const gchar * net_id, const gchar * id)
{
	gchar *key;
	gboolean value;

	key = g_build_filename (NETWORK_DIR, net_id, id, PORT_KEY_BIT, NULL);
	value = gconf_client_key_is_writable (prefs->client, key, NULL);
	g_free (key);

	return value;
}


gint
prefs_get_server_port (const gchar * net_id, const gchar * id)
{
	gchar *key;
	gint value;

	key = g_build_filename (NETWORK_DIR, net_id, id, PORT_KEY_BIT, NULL);
	value = get_clamped_int (key, FALSE, 0, 65535, 6667);
	g_free (key);

	return value;
}


void
prefs_set_server_port (const gchar * net_id, const gchar * id, gint value)
{
	gchar *key;

	/* Make sure we don't store bogus values */
	value = CLAMP (value, 0, 65535);

	key = g_build_filename (NETWORK_DIR, net_id, id, PORT_KEY_BIT, NULL);
	gconf_client_set_int (prefs->client, key, value, NULL);
	gconf_client_suggest_sync (prefs->client, NULL);
	g_free (key);
}


gboolean
prefs_get_can_set_server_passwd (const gchar * net_id, const gchar * id)
{
	gchar *key;
	gboolean value;

	key = g_build_filename (NETWORK_DIR, net_id, id, PASSWD_KEY_BIT, NULL);
	value = gconf_client_key_is_writable (prefs->client, key, NULL);
	g_free (key);

	return value;
}


gchar *
prefs_get_server_passwd (const gchar * net_id, const gchar * id)
{
	gchar *key,
	 *value;

	key = g_build_filename (NETWORK_DIR, net_id, id, PASSWD_KEY_BIT, NULL);
	value = gconf_client_get_string (prefs->client, key, NULL);
	g_free (key);

	return value;
}


void
prefs_set_server_passwd (const gchar * net_id, const gchar * id, const gchar * value)
{
	gchar *key;

	key = g_build_filename (NETWORK_DIR, net_id, id, PASSWD_KEY_BIT, NULL);
	gconf_client_set_string (prefs->client, key, value, NULL);
	gconf_client_suggest_sync (prefs->client, NULL);
	g_free (key);
}


gboolean
prefs_get_can_set_server_charset (const gchar * net_id, const gchar * id)
{
	gchar *key;
	gboolean value;

	key = g_build_filename (NETWORK_DIR, net_id, id, CHARSET_KEY_BIT, NULL);
	value = gconf_client_key_is_writable (prefs->client, key, NULL);
	g_free (key);

	return value;
}


gchar *
prefs_get_server_charset (const gchar * net_id, const gchar * id)
{
	gchar *key,
	 *value;

	key = g_build_filename (NETWORK_DIR, net_id, id, CHARSET_KEY_BIT, NULL);
	value = gconf_client_get_string (prefs->client, key, NULL);
	g_free (key);

	return value;
}


void
prefs_set_server_charset (const gchar * net_id, const gchar * id, const gchar * value)
{
	gchar *key;

	key = g_build_filename (NETWORK_DIR, net_id, id, CHARSET_KEY_BIT, NULL);
	gconf_client_set_string (prefs->client, key, value, NULL);
	gconf_client_suggest_sync (prefs->client, NULL);
	g_free (key);
}


gboolean
prefs_get_can_set_server_icon (const gchar * net_id, const gchar * id)
{
	gchar *key;
	gboolean value;

	key = g_build_filename (NETWORK_DIR, net_id, id, ICON_KEY_BIT, NULL);
	value = gconf_client_key_is_writable (prefs->client, key, NULL);
	g_free (key);

	return value;
}


gchar *
prefs_get_server_icon (const gchar * net_id, const gchar * id)
{
	gchar *key,
	 *value;

	key = g_build_filename (NETWORK_DIR, net_id, id, ICON_KEY_BIT, NULL);
	value = gconf_client_get_string (prefs->client, key, NULL);
	g_free (key);

	return value;
}


void
prefs_set_server_icon (const gchar * net_id, const gchar * id, const gchar * value)
{
	gchar *key;

	key = g_build_filename (NETWORK_DIR, net_id, id, ICON_KEY_BIT, NULL);
	gconf_client_set_string (prefs->client, key, value, NULL);
	gconf_client_suggest_sync (prefs->client, NULL);
	g_free (key);
}


gboolean
prefs_get_can_set_server_location (const gchar * net_id, const gchar * id)
{
	gchar *key;
	gboolean value;

	key = g_build_filename (NETWORK_DIR, net_id, id, LOCATION_KEY_BIT, NULL);
	value = gconf_client_key_is_writable (prefs->client, key, NULL);
	g_free (key);

	return value;
}


gchar *
prefs_get_server_location (const gchar * net_id, const gchar * id)
{
	gchar *key,
	 *value;

	key = g_build_filename (NETWORK_DIR, net_id, id, LOCATION_KEY_BIT, NULL);
	value = gconf_client_get_string (prefs->client, key, NULL);
	g_free (key);

	return value;
}


void
prefs_set_server_location (const gchar * net_id, const gchar * id, const gchar * value)
{
	gchar *key;

	key = g_build_filename (NETWORK_DIR, net_id, id, LOCATION_KEY_BIT, NULL);
	gconf_client_set_string (prefs->client, key, value, NULL);
	gconf_client_suggest_sync (prefs->client, NULL);
	g_free (key);
}


gboolean
prefs_get_can_set_server_country_code (const gchar * net_id, const gchar * id)
{
	gchar *key;
	gboolean value;

	key = g_build_filename (NETWORK_DIR, net_id, id, COUNTRY_CODE_KEY_BIT, NULL);
	value = gconf_client_key_is_writable (prefs->client, key, NULL);
	g_free (key);

	return value;
}


gchar *
prefs_get_server_country_code (const gchar * net_id, const gchar * id)
{
	gchar *key,
	 *value;

	key = g_build_filename (NETWORK_DIR, net_id, id, COUNTRY_CODE_KEY_BIT, NULL);
	value = gconf_client_get_string (prefs->client, key, NULL);
	g_free (key);

	return value;
}


void
prefs_set_server_country_code (const gchar * net_id, const gchar * id, const gchar * value)
{
	gchar *key;

	key = g_build_filename (NETWORK_DIR, net_id, id, COUNTRY_CODE_KEY_BIT, NULL);
	gconf_client_set_string (prefs->client, key, value, NULL);
	gconf_client_suggest_sync (prefs->client, NULL);
	g_free (key);
}
