/*
 *  GnomeChat: src/esco-hpaned.h
 *  stolen from nautilus/src/libnautilus-private/nautilus-horizontal-splitter.h
 *
 *  Copyright (c) 2002 James M. Cape.
 *  Copyright (C) 1999, 2000 Eazel, Inc.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef __ESCO_HPANED_H__
#define __ESCO_HPANED_H__


#include <gtk/gtkenums.h>
#include <gtk/gtkhpaned.h>


#define ESCO_TYPE_HPANED			(esco_hpaned_get_type ())
#define ESCO_HPANED(obj)			(GTK_CHECK_CAST ((obj), ESCO_TYPE_HPANED, EscoHPaned))
#define ESCO_HPANED_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), ESCO_TYPE_HPANED, EscoHPanedClass))
#define ESCO_IS_HPANED(obj)			(GTK_CHECK_TYPE ((obj), ESCO_TYPE_HPANED))
#define ESCO_IS_HPANED_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass), ESCO_TYPE_HPANED))
#define ESCO_HPANED_GET_CLASS(obj)	(GTK_CHECK_GET_CLASS ((obj), ESCO_TYPE_HPANED, EscoHPanedClass))


typedef struct _EscoHPaned EscoHPaned;
typedef struct _EscoHPanedClass EscoHPanedClass;

typedef void (*EscoHPanedToggleFunction) (EscoHPaned * paned,
										  gboolean open);

typedef void (*EscoHPanedSizeSaveFunction) (gint size);
typedef gint (*EscoHPanedSizeLoadFunction) (void);


struct _EscoHPaned
{
	GtkPaned parent;

	gboolean open;
	GtkPositionType close_side;

	gint saved_size;
	gdouble press_coord;
	guint32 press_time;

	EscoHPanedSizeSaveFunction size_save_func;
	EscoHPanedSizeLoadFunction size_load_func;
};

struct _EscoHPanedClass
{
	GtkPanedClass parent_class;

	EscoHPanedToggleFunction toggled;
};


GType esco_hpaned_get_type (void);

GtkWidget *esco_hpaned_new (GtkPositionType close_side);
void esco_hpaned_set_size_save_func (EscoHPaned * paned,
									 EscoHPanedSizeSaveFunction prefs_func);
void esco_hpaned_set_size_load_func (EscoHPaned * paned,
									 EscoHPanedSizeLoadFunction prefs_func);

gboolean esco_hpaned_get_open (EscoHPaned * paned);
void esco_hpaned_set_open (EscoHPaned * paned,
						   gboolean open);

GtkPositionType esco_hpaned_get_close_side (EscoHPaned * paned);
void esco_hpaned_set_close_side (EscoHPaned * paned,
								 GtkPositionType close_side);


#endif /* __ESCO_HPANED_H__ */
