/*
 *  GnomeChat: src/esco-window.h
 *
 *  Copyright (c) 2002, 2003 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "gnomechat.h"
#include "esco-window.h"
#include "esco-type-builtins.h"

#include "esco-encoding-menu.h"
#include "esco-chat-box.h"
#include "esco-userlist-view.h"

#include "menus.h"
#include "prefs.h"
#include "pixbufs.h"
#include "stock-items.h"
#include "windows.h"
#include "utils.h"

#include <gtk/gtkcellrendererpixbuf.h>
#include <gtk/gtkcellrenderertext.h>
#include <gtk/gtkcheckmenuitem.h>
#include <gtk/gtkhpaned.h>
#include <gtk/gtklabel.h>
#include <gtk/gtknotebook.h>
#include <gtk/gtkscrolledwindow.h>

#include <string.h>


#define CHATBOX_ROWREF_KEY (get_chatbox_rowref_quark ())


enum
{
	PROP_0,
	PROP_LAYOUT
};

enum
{
	SIGNAL_LAYOUT_CHANGED,
	SIGNAL_LAST
};


static gint esco_window_signals[SIGNAL_LAST] = { 0 };
static gpointer parent_class = NULL;


/* Utility Functions */
static GQuark
get_chatbox_rowref_quark (void)
{
	static GQuark quark = 0;

	if (quark == 0)
	{
		quark = g_quark_from_static_string ("gnomechat-chatbox-rowref");
	}

	return quark;
}


static void
set_title (GtkWindow * win,
		   const gchar * title,
		   const gchar * subtitle)
{
	gchar *text;

	if (subtitle != NULL)
	{
		if (prefs_get_main_window_topic_in_titlebar ()
			&& GIRC_IS_CHANNEL (ESCO_CHAT_BOX (ESCO_WINDOW (win)->current)->target))
		{
			text = g_strdup_printf ("%s (%s) - %s - %s", title,
									GIRC_CHANNEL (ESCO_CHAT_BOX (ESCO_WINDOW (win)->current)->
												  target)->topic, subtitle, GENERIC_NAME);
		}
		else
		{
			text = g_strconcat (title, " - ", subtitle, " - ", GENERIC_NAME, NULL);
		}
	}
	else
	{
		text = g_strconcat (title, " - ", GENERIC_NAME, NULL);
	}

	gtk_window_set_title (win, text);

	g_free (text);
}


static void
render_chat_icon (GtkTreeViewColumn * col,
				  GtkCellRenderer * cell,
				  GtkTreeModel * model,
				  GtkTreeIter * iter,
				  gpointer data)
{
	EscoChatBox *box;

	gtk_tree_model_get (model, iter, ESCO_CHATS_LIST_CHAT_BOX, &box, -1);

	g_object_set (cell, "stock-id", esco_chat_box_get_mode_icon (box), NULL);
}


static void
render_chat_text (GtkTreeViewColumn * col,
				  GtkCellRenderer * cell,
				  GtkTreeModel * model,
				  GtkTreeIter * iter,
				  gpointer data)
{
	EscoChatBox *box;

	gtk_tree_model_get (model, iter, ESCO_CHATS_LIST_CHAT_BOX, &box, -1);

	if (box->pester > ESCO_PESTER_IRC)
	{
		gchar *color = prefs_get_notification_color (box->pester);

		if (color != NULL)
		{
			g_object_set (cell, "text", box->title, "foreground", color, NULL);

			g_free (color);
		}
	}
	else
	{
		g_object_set (cell, "text", box->title, "foreground-set", FALSE, NULL);
	}
}


/* Sub-Widget Signal Callbacks */
static void
paned_button_release_event_cb (GtkPaned * paned,
							   GdkEventButton * event,
							   EscoWindow * win)
{
	prefs_set_main_window_chats_tree_size (gtk_paned_get_position (paned));
}


static void
encoding_menu_changed_cb (EscoEncodingMenu * menu,
						  EscoWindow * win)
{
	esco_chat_box_set_encoding (ESCO_CHAT_BOX (win->current),
								esco_encoding_menu_get_encoding (menu));
}


static void
box_focused_cb (EscoChatBox * box,
				GtkWindow * window)
{
	EscoWindow *ewin = ESCO_WINDOW (window);

	set_title (window, box->title, box->subtitle);
	if (strcmp (ewin->stock_icon, esco_chat_box_get_mode_icon (box)) != 0)
	{
		g_free (ewin->stock_icon);
		ewin->stock_icon = g_strdup (esco_chat_box_get_mode_icon (box));
		util_set_window_icon_from_stock (window, ewin->stock_icon);
	}

	gtk_widget_set_sensitive (ewin->menus->channel_props, (box->mode == ESCO_CHAT_MODE_CHANNEL));

	gtk_widget_set_sensitive (ewin->menus->user_props,
							  ((box->mode == ESCO_CHAT_MODE_USER) ||
							   ESCO_USERLIST_VIEW (box->userlist_view)->any_selected));

	gtk_widget_set_sensitive (ewin->menus->server_props, (box->mode == ESCO_CHAT_MODE_SERVER));
}


static void
box_title_changed_cb (EscoChatBox * box,
					  GtkWindow * window)
{
	EscoWindow *win = ESCO_WINDOW (window);
	GtkTreeRowReference *rowref = g_object_get_qdata (G_OBJECT (box), CHATBOX_ROWREF_KEY);

	if (win->current == GTK_WIDGET (box))
	{
		set_title (window, box->title, box->subtitle);
	}

	if (rowref != NULL)
	{
		GtkTreePath *path;
		GtkTreeIter iter;

		path = gtk_tree_row_reference_get_path (rowref);

		if (gtk_tree_model_get_iter (GTK_TREE_MODEL (win->chats_store), &iter, path))
		{
			gtk_tree_model_row_changed (GTK_TREE_MODEL (win->chats_store), path, &iter);
		}

		gtk_tree_path_free (path);
	}
}


static void
box_mode_changed_cb (EscoChatBox * box,
					 GtkWindow * window)
{
	EscoWindow *ewin = ESCO_WINDOW (window);
	GtkTreeRowReference *rowref = g_object_get_qdata (G_OBJECT (box), CHATBOX_ROWREF_KEY);

	if (ewin->current == GTK_WIDGET (box))
	{
		if (strcmp (ewin->stock_icon, esco_chat_box_get_mode_icon (box)) != 0)
		{
			g_free (ewin->stock_icon);
			ewin->stock_icon = g_strdup (esco_chat_box_get_mode_icon (box));
			util_set_window_icon_from_stock (window, ewin->stock_icon);
		}

		gtk_widget_set_sensitive (ewin->menus->channel_props,
								  (box->mode == ESCO_CHAT_MODE_CHANNEL));

		gtk_widget_set_sensitive (ewin->menus->user_props, (box->mode == ESCO_CHAT_MODE_USER));

		gtk_widget_set_sensitive (ewin->menus->server_props, (box->mode == ESCO_CHAT_MODE_SERVER));
	}

	if (rowref != NULL)
	{
		GtkTreePath *path;
		GtkTreeIter iter;

		path = gtk_tree_row_reference_get_path (rowref);

		if (gtk_tree_model_get_iter (GTK_TREE_MODEL (ewin->chats_store), &iter, path))
		{
			gtk_tree_model_row_changed (GTK_TREE_MODEL (ewin->chats_store), path, &iter);
		}

		gtk_tree_path_free (path);
	}
}


static void
box_pester_cb (EscoChatBox * box,
			   EscoPesterType level,
			   EscoWindow * ewin)
{
	GtkTreeRowReference *rowref = g_object_get_qdata (G_OBJECT (box), CHATBOX_ROWREF_KEY);

	if (rowref != NULL)
	{
		GtkTreePath *path;
		GtkTreeIter iter;

		path = gtk_tree_row_reference_get_path (rowref);

		if (gtk_tree_model_get_iter (GTK_TREE_MODEL (ewin->chats_store), &iter, path))
		{
			gtk_tree_model_row_changed (GTK_TREE_MODEL (ewin->chats_store), path, &iter);
		}

		gtk_tree_path_free (path);
	}
}


static void
box_user_sel_changed_cb (EscoChatBox * box,
						 gboolean any_selected,
						 EscoWindow * win)
{
	gtk_widget_set_sensitive (win->menus->user_props, any_selected);
}


static void
chats_sel_changed_cb (GtkTreeSelection * sel,
					  EscoWindow * win)
{
	GtkTreeIter iter;
	GtkWidget *box;
	gint page_num;

	if (!gtk_tree_selection_get_selected (sel, NULL, &iter))
		return;

	gtk_tree_model_get (GTK_TREE_MODEL (win->chats_store), &iter,
						ESCO_CHATS_LIST_CHAT_BOX, &box, -1);

	if (win->current == box)
		return;

	win->current = box;

	page_num = gtk_notebook_page_num (GTK_NOTEBOOK (win->notebook), box);
	gtk_notebook_set_current_page (GTK_NOTEBOOK (win->notebook), page_num);

	g_signal_handler_disconnect (win->menus->encoding_menu, win->encoding_signal);
	esco_encoding_menu_set_encoding (ESCO_ENCODING_MENU (win->menus->encoding_menu),
									 ESCO_CHAT_BOX (box)->encoding);
	win->encoding_signal = g_signal_connect (win->menus->encoding_menu,
											 "encoding-changed",
											 G_CALLBACK (encoding_menu_changed_cb), win);

	g_signal_emit_by_name (box, "focused", 0);
}


static void
notebook_switch_page_cb (GtkNotebook * notebook,
						 GtkNotebookPage * page,
						 gint page_num,
						 gpointer data)
{
	EscoWindow *win = ESCO_WINDOW (data);
	GtkWidget *box;

	box = gtk_notebook_get_nth_page (notebook, page_num);

	if (win->current == box)
		return;

	win->current = box;

	if (ESCO_IS_CHAT_BOX (box))
	{
		GtkTreeRowReference *rowref = g_object_get_qdata (G_OBJECT (box),
														  CHATBOX_ROWREF_KEY);

		if (rowref != NULL)
		{
			GtkTreePath *path;

			path = gtk_tree_row_reference_get_path (rowref);

			if (path != NULL)
			{
				gtk_tree_selection_select_path (win->chats_sel, path);
				gtk_tree_path_free (path);

				g_signal_handler_disconnect (win->menus->encoding_menu, win->encoding_signal);
				esco_encoding_menu_set_encoding (ESCO_ENCODING_MENU
												 (win->menus->encoding_menu),
												 ESCO_CHAT_BOX (box)->encoding);
				win->encoding_signal =
					g_signal_connect (win->menus->encoding_menu, "encoding-changed",
									  G_CALLBACK (encoding_menu_changed_cb), win);

				g_signal_emit_by_name (box, "focused", 0);
			}
		}
	}
}


/* EscoWindow Callbacks */
static void
esco_window_layout_changed (EscoWindow * win,
							EscoWindowLayoutType layout)
{
	GList *kids;

	win->setting_layout = TRUE;

	if (!layout)
		layout = prefs_get_main_window_layout ();

	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (win->menus->layouts[layout - 1]), TRUE);

	switch (layout)
	{
	case ESCO_WINDOW_LAYOUT_TABS:
		gtk_widget_hide (win->chats_scrwin);
		gtk_notebook_set_show_tabs (GTK_NOTEBOOK (win->notebook), TRUE);
		gtk_container_set_border_width (GTK_CONTAINER (win->paned), 0);

		for (kids = win->children; kids != NULL; kids = kids->next)
			gtk_container_set_border_width (GTK_CONTAINER (kids->data), 2);
		break;

	case ESCO_WINDOW_LAYOUT_TREE:
		gtk_widget_show (win->chats_scrwin);
		gtk_notebook_set_show_tabs (GTK_NOTEBOOK (win->notebook), FALSE);
		gtk_notebook_set_show_border (GTK_NOTEBOOK (win->notebook), FALSE);
		gtk_container_set_border_width (GTK_CONTAINER (win->paned), 2);

		for (kids = win->children; kids != NULL; kids = kids->next)
			gtk_container_set_border_width (GTK_CONTAINER (kids->data), 0);
		break;

	case ESCO_WINDOW_LAYOUT_WINDOW:
		gtk_widget_hide (win->chats_scrwin);
		gtk_notebook_set_show_tabs (GTK_NOTEBOOK (win->notebook), FALSE);
		gtk_notebook_set_show_border (GTK_NOTEBOOK (win->notebook), TRUE);
		gtk_container_set_border_width (GTK_CONTAINER (win->paned), 0);

		for (kids = win->children; kids != NULL; kids = kids->next)
			gtk_container_set_border_width (GTK_CONTAINER (kids->data), 2);
		break;

	default:
		g_assert_not_reached ();
		break;
	}

	win->setting_layout = FALSE;
}


/* GtkWidget Callbacks */
static gboolean
esco_window_configure_event (GtkWidget * widget,
							 GdkEventConfigure * event)
{
	gboolean retval;

	if (GTK_WIDGET_CLASS (parent_class)->configure_event != NULL)
		retval = (*GTK_WIDGET_CLASS (parent_class)->configure_event) (widget, event);
	else
		retval = TRUE;

	/* Save window position */
	if (!prefs_get_main_window_force_position ())
	{
		prefs_set_main_window_left_edge (event->x);
		prefs_set_main_window_top_edge (event->y);
	}

	/* Save window size */
	if (!prefs_get_main_window_force_size ())
	{
		prefs_set_window_width (GNOMECHAT_WINDOW_MAIN, event->width);
		prefs_set_window_height (GNOMECHAT_WINDOW_MAIN, event->height);
	}

	return retval;
}


/* GObject Callbacks */
static void
esco_window_get_property (GObject * object,
						  guint param_id,
						  GValue * value,
						  GParamSpec * pspec)
{
	EscoWindow *win = ESCO_WINDOW (object);

	switch (param_id)
	{
	case PROP_LAYOUT:
		g_value_set_enum (value, win->layout);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}


static void
esco_window_set_property (GObject * object,
						  guint param_id,
						  const GValue * value,
						  GParamSpec * pspec)
{
	EscoWindow *win = ESCO_WINDOW (object);

	switch (param_id)
	{
	case PROP_LAYOUT:
		esco_window_set_layout (win, g_value_get_enum (value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}


static GObject *
esco_window_constructor (GType type,
						 guint n_construct_params,
						 GObjectConstructParam * construct_params)
{
	EscoWindow *win;
	GObject *object;
	GtkCellRenderer *cell;
	GtkTreeViewColumn *col;

	object = (*G_OBJECT_CLASS (parent_class)->constructor)
		(type, n_construct_params, construct_params);

	win = ESCO_WINDOW (object);

	gtk_window_set_role (GTK_WINDOW (win), "Main");

	gtk_window_set_default_size (GTK_WINDOW (win), prefs_get_window_width (GNOMECHAT_WINDOW_MAIN),
								 prefs_get_window_height (GNOMECHAT_WINDOW_MAIN));

	win->paned = gtk_hpaned_new ();
	gtk_paned_set_position (GTK_PANED (win->paned), prefs_get_main_window_chats_tree_size ());
	g_signal_connect (win->paned, "button-release-event",
					  G_CALLBACK (paned_button_release_event_cb), win);
	gnome_app_set_contents (GNOME_APP (win), win->paned);
	gtk_widget_show (win->paned);

	win->chats_scrwin = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (win->chats_scrwin), GTK_SHADOW_IN);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (win->chats_scrwin),
									GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_paned_pack1 (GTK_PANED (win->paned), win->chats_scrwin, FALSE, TRUE);

	win->chats_store = gtk_tree_store_new (ESCO_CHATS_LIST_NUM_COLUMNS, ESCO_TYPE_CHAT_BOX);

	win->chats_view = gtk_tree_view_new_with_model (GTK_TREE_MODEL (win->chats_store));
	gtk_container_add (GTK_CONTAINER (win->chats_scrwin), win->chats_view);
	gtk_widget_show (win->chats_view);

	win->chats_sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (win->chats_view));
	g_signal_connect (win->chats_sel, "changed", G_CALLBACK (chats_sel_changed_cb), win);

	col = gtk_tree_view_column_new ();
	gtk_tree_view_column_set_title (col, _("Chats"));
	gtk_tree_view_append_column (GTK_TREE_VIEW (win->chats_view), col);

	cell = gtk_cell_renderer_pixbuf_new ();
	g_object_set (cell, "stock-size", GTK_ICON_SIZE_MENU, NULL);
	gtk_tree_view_column_pack_start (col, cell, FALSE);
	gtk_tree_view_column_set_cell_data_func (col, cell, render_chat_icon, NULL, NULL);

	cell = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_start (col, cell, TRUE);
	gtk_tree_view_column_set_cell_data_func (col, cell, render_chat_text, NULL, NULL);

	win->notebook = gtk_notebook_new ();
	gtk_notebook_set_tab_pos (GTK_NOTEBOOK (win->notebook), prefs_get_main_window_tab_side ());
	g_signal_connect (win->notebook, "switch-page", G_CALLBACK (notebook_switch_page_cb), win);
	gtk_paned_pack2 (GTK_PANED (win->paned), win->notebook, TRUE, TRUE);
	gtk_widget_show (win->notebook);

	menu_set_main_window_menubar (GTK_WIDGET (win));

	/* This is a new window, so the server/channel/user
	   properties items can't do anything yet, since there isn't any
	   server/channel/user associated with this window yet. */
	gtk_widget_set_sensitive (win->menus->server_props, FALSE);
	gtk_widget_set_sensitive (win->menus->channel_props, FALSE);
	gtk_widget_set_sensitive (win->menus->user_props, FALSE);

	win->encoding_signal = g_signal_connect (win->menus->encoding_menu, "encoding-changed",
											 G_CALLBACK (encoding_menu_changed_cb), win);

	return object;
}


static void
esco_window_finalize (GObject * object)
{
	EscoWindow *ewin = ESCO_WINDOW (object);

	g_free (ewin->stock_icon);
}


/* GType Functions */
static void
esco_window_class_init (EscoWindowClass * class)
{
	GObjectClass *object_class = G_OBJECT_CLASS (class);
	GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	object_class->get_property = esco_window_get_property;
	object_class->set_property = esco_window_set_property;
	object_class->constructor = esco_window_constructor;
	object_class->finalize = esco_window_finalize;

	widget_class->configure_event = esco_window_configure_event;

	class->layout_changed = esco_window_layout_changed;

	g_object_class_install_property (object_class,
									 PROP_LAYOUT,
									 g_param_spec_enum ("layout",
														_("Window Layout"),
														_("The layout style "
														  "of the window"),
														ESCO_TYPE_WINDOW_LAYOUT_TYPE,
														ESCO_WINDOW_LAYOUT_UNSET,
														G_PARAM_READWRITE));
	esco_window_signals[SIGNAL_LAYOUT_CHANGED] =
		g_signal_new ("layout-changed",
					  G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_LAST,
					  G_STRUCT_OFFSET (EscoWindowClass, layout_changed),
					  NULL, NULL,
					  g_cclosure_marshal_VOID__ENUM, G_TYPE_NONE, 1, ESCO_TYPE_WINDOW_LAYOUT_TYPE);
}


static void
esco_window_instance_init (EscoWindow * win)
{
	win->layout = ESCO_WINDOW_LAYOUT_UNSET;
	win->children = NULL;
	win->menus = g_new0 (EscoWindowMenus, 1);
	win->closing = FALSE;

	win->stock_icon = g_strdup (GNOMECHAT_STOCK_ICON);
}


/* PUBLIC API */
GType
esco_window_get_type (void)
{
	static GType type = 0;

	if (!type)
	{
		static const GTypeInfo info = {
			sizeof (EscoWindowClass),
			NULL,				/* base_init */
			NULL,				/* base_finalize */
			(GClassInitFunc) esco_window_class_init,
			NULL,				/* class_finalize */
			NULL,				/* class_data */
			sizeof (EscoWindow),
			0,					/* n_preallocs */
			(GInstanceInitFunc) esco_window_instance_init,
		};

		type = g_type_register_static (GNOME_TYPE_APP, "EscoWindow", &info, 0);
	}

	return type;
}


GtkWidget *
esco_window_new (void)
{
	return GTK_WIDGET (g_object_new (ESCO_TYPE_WINDOW,
									 "app_id", genericname, "title", genericname, NULL));
}


void
esco_window_set_layout (EscoWindow * win,
						EscoWindowLayoutType layout)
{
	if (win->setting_layout || (layout == win->layout && win->layout != ESCO_WINDOW_LAYOUT_UNSET))
		return;

	if (!layout)
	{
		layout = prefs_get_main_window_layout ();
	}

	win->layout = layout;

	g_signal_emit (win, esco_window_signals[SIGNAL_LAYOUT_CHANGED], 0, layout);
}


GtkWidget *
esco_window_add_new_chat (EscoWindow * win)
{
	GtkWidget *box;
	GtkTreeIter iter;
	GtkTreePath *path;
	GtkTreeRowReference *rowref;

	if (win->layout == ESCO_WINDOW_LAYOUT_WINDOW && g_list_length (win->children) > 0)
	{
		return windows_open_new_window ();
	}

	/* Create a new box */
	box = esco_chat_box_new ();

	/* Add to the chats list */
	gtk_tree_store_append (win->chats_store, &iter, NULL);
	gtk_tree_store_set (win->chats_store, &iter, ESCO_CHATS_LIST_CHAT_BOX, box, -1);

	path = gtk_tree_model_get_path (GTK_TREE_MODEL (win->chats_store), &iter);
	rowref = gtk_tree_row_reference_new (GTK_TREE_MODEL (win->chats_store), path);

	g_object_set_qdata_full (G_OBJECT (box), CHATBOX_ROWREF_KEY, rowref,
							 (GDestroyNotify) gtk_tree_row_reference_free);

	/* Connect the signals */
	g_signal_connect (box, "focused", G_CALLBACK (box_focused_cb), win);
	g_signal_connect (box, "title-changed", G_CALLBACK (box_title_changed_cb), win);
	g_signal_connect (box, "mode-changed", G_CALLBACK (box_mode_changed_cb), win);
	g_signal_connect (box, "pester", G_CALLBACK (box_pester_cb), win);
	g_signal_connect (box, "user-selection-changed", G_CALLBACK (box_user_sel_changed_cb), win);

	gtk_widget_show (box);


	win->children = g_list_append (win->children, box);

	if (!(win->layout))
		esco_window_set_layout (win, win->layout);

	switch (win->layout)
	{
	case ESCO_WINDOW_LAYOUT_TABS:
		gtk_container_set_border_width (GTK_CONTAINER (box), 2);
		break;
	case ESCO_WINDOW_LAYOUT_TREE:
	case ESCO_WINDOW_LAYOUT_WINDOW:
		gtk_container_set_border_width (GTK_CONTAINER (box), 0);
		break;
	default:
		g_assert_not_reached ();
		break;
	}

	gtk_notebook_append_page (GTK_NOTEBOOK (win->notebook), box, ESCO_CHAT_BOX (box)->tab_hbox);

	gtk_notebook_set_current_page (GTK_NOTEBOOK (win->notebook), -1);
	gtk_tree_selection_select_path (win->chats_sel, path);
	gtk_tree_path_free (path);

	return box;
}


void
esco_window_remove_current_chat (EscoWindow * win)
{
	EscoChatBox *box;
	GtkTreeRowReference *rowref;
	gint page_num;

	box = ESCO_CHAT_BOX (win->current);

	win->children = g_list_remove (win->children, box);

	if (!(win->children))
	{
		gtk_widget_destroy (GTK_WIDGET (win));
		return;
	}

	rowref = g_object_get_qdata (G_OBJECT (box), CHATBOX_ROWREF_KEY);

	if (rowref != NULL)
	{
		GtkTreePath *path = gtk_tree_row_reference_get_path (rowref);

		if (path != NULL)
		{
			GtkTreeIter iter;

			if (gtk_tree_model_get_iter (GTK_TREE_MODEL (win->chats_store), &iter, path))
			{
				gtk_tree_store_remove (win->chats_store, &iter);
			}

			gtk_tree_path_free (path);
		}
	}

	page_num = gtk_notebook_page_num (GTK_NOTEBOOK (win->notebook), GTK_WIDGET (box));
	gtk_notebook_remove_page (GTK_NOTEBOOK (win->notebook), page_num);

	gtk_widget_destroy (GTK_WIDGET (box));
}


void
esco_window_switch_to_chat (EscoWindow * win,
							GtkWidget * box)
{
	gint page_num;

	page_num = gtk_notebook_page_num (GTK_NOTEBOOK (win->notebook), box);
	gtk_notebook_set_current_page (GTK_NOTEBOOK (win->notebook), page_num);
}
