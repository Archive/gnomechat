/*
 *  GnomeChat: src/utils.c
 *
 *  Copyright (c) 2002 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "gnomechat.h"
#include "utils.h"

#include "stock-items.h"

#include <atk/atkrelationset.h>

#include <gtk/gtktooltips.h>
#include <gtk/gtklabel.h>

#include <libgircclient/girc-channel.h>

#include <string.h>
#include <stdio.h>


#define STYLE_SET_CONNECTED_KEY (get_style_set_connected_quark ())


static GQuark
get_style_set_connected_quark (void)
{
	static GQuark quark = 0;

	if (quark == 0)
	{
		quark = g_quark_from_static_string ("gnomechat-style-set-connected");
	}

	return quark;
}


/* PUBLIC API */
gboolean
util_str_is_not_empty (const gchar * str)
{
	gchar *str_copy = NULL,
	 *stripped_str;
	gboolean retval;

	if (str == NULL || str[0] == '\0')
		return FALSE;

	str_copy = g_strdup (str);
	stripped_str = g_strstrip (str_copy);

	if (stripped_str[0] == '\0')
		retval = FALSE;
	else
		retval = TRUE;

	g_free (str_copy);

	return retval;
}


gint
util_get_font_height (GtkWidget * widget)
{
	PangoContext *context;
	PangoFontMetrics *metrics;
	gint pango_units;

	g_return_val_if_fail (GTK_IS_WIDGET (widget), -1);

	context = gtk_widget_get_pango_context (widget);
	metrics = pango_context_get_metrics (context, widget->style->font_desc, NULL);
	pango_units = pango_font_metrics_get_ascent (metrics);
	pango_units += pango_font_metrics_get_descent (metrics);
	pango_font_metrics_unref (metrics);

	return PANGO_PIXELS (pango_units);
}


gint
util_get_font_width (GtkWidget * widget)
{
	PangoContext *context;
	PangoFontMetrics *metrics;
	gint pango_units;

	g_return_val_if_fail (GTK_IS_WIDGET (widget), -1);

	context = gtk_widget_get_pango_context (widget);
	metrics = pango_context_get_metrics (context, widget->style->font_desc, NULL);

	pango_units = pango_font_metrics_get_approximate_char_width (metrics);
	pango_font_metrics_unref (metrics);

	return PANGO_PIXELS (pango_units);
}


void
util_set_tooltip (GtkWidget * widget, const gchar * tip, const gchar * long_tip,
				  gboolean set_atk_name_from_tip)
{
	static GtkTooltips *tooltips = NULL;
	AtkObject *atk_obj;

	if (tooltips == NULL)
		tooltips = gtk_tooltips_new ();

	gtk_tooltips_set_tip (tooltips, widget, tip, long_tip);

	/* FIXME: Is this correct, or should I be checking for the various types,
	   or what? <mailto:jcape@ignore-your.tv> */
	atk_obj = gtk_widget_get_accessible (widget);

	if (tip != NULL && set_atk_name_from_tip)
		atk_object_set_name (atk_obj, tip);

	if (long_tip != NULL)
		atk_object_set_description (atk_obj, long_tip);
}


void
util_set_label_widget_pair (GtkWidget * label, GtkWidget * widget) 
{
	AtkObject *atk_widget,
	 *atk_label;
	AtkRelationSet *relation_set;
	AtkRelation *relation;
	AtkObject *targets[1];

	if (GTK_IS_LABEL (label))
		gtk_label_set_mnemonic_widget (GTK_LABEL (label), widget);

	atk_widget = gtk_widget_get_accessible (widget);
	atk_label = gtk_widget_get_accessible (label);

	relation_set = atk_object_ref_relation_set (atk_label);
	targets[0] = atk_widget;

	relation = atk_relation_new (targets, 1, ATK_RELATION_LABEL_FOR);
	atk_relation_set_add (relation_set, relation);
	g_object_unref (relation);
}


gchar *
util_gconf_key_directory (const gchar * key)
{
	const gchar *end;
	gchar *retval;
	int len;

	end = strrchr (key, '/');

	if (end == NULL)
	{
		g_warning (_("No '/' in key \"%s\""), key);
		return NULL;
	}

	len = end - key + 1;

	if (len == 1)
	{
		/* Root directory */
		retval = g_strdup ("/");
	}
	else
	{
		retval = g_malloc (len);

		strncpy (retval, key, len);

		retval[len - 1] = '\0';
	}

	return retval;
}


G_CONST_RETURN gchar *
util_gconf_key_key (const gchar * key)
{
	const gchar *end;

	end = strrchr (key, '/');

	++end;

	return end;
}


void
util_scroll_tree_view_to_selection (GtkTreeSelection * sel)
{
	GtkTreeModel *model;
	GtkTreeIter iter;

	if (gtk_tree_selection_get_selected (sel, &model, &iter))
	{
		GtkTreePath *path = gtk_tree_model_get_path (model, &iter);

		gtk_tree_view_scroll_to_cell (sel->tree_view, path, NULL, TRUE, 0.5, 0.5);
		gtk_tree_path_free (path);
	}
}


static void
window_style_set_cb (GtkWindow * window,
					 GtkStyle * old_style,
					 const gchar * stock_id)
{
	GList *list = stock_get_icon_list_from_stock (GTK_WIDGET (window), stock_id);

	gtk_window_set_icon_list (window, list);

	util_g_list_deep_free (list, g_object_unref);
}


void
util_set_window_icon_from_stock (GtkWindow * window,
								 const gchar * stock_id)
{
	GList *list = stock_get_icon_list_from_stock (GTK_WIDGET (window), stock_id);
	gulong sigid = GPOINTER_TO_UINT (g_object_get_qdata (G_OBJECT (window),
														 STYLE_SET_CONNECTED_KEY));

	gtk_window_set_icon_list (window, list);

	util_g_list_deep_free (list, g_object_unref);

	if (sigid != 0)
		g_signal_handler_disconnect (window, sigid);

	sigid = g_signal_connect_data (window, "style-set", G_CALLBACK (window_style_set_cb),
								   g_strdup (stock_id), (GClosureNotify) g_free, 0);

	g_object_set_qdata (G_OBJECT (window), STYLE_SET_CONNECTED_KEY, GUINT_TO_POINTER (sigid));
}


void
util_g_list_deep_free (GList * list,
					   GFreeFunc free_func)
{
	if (list == NULL)
		return;

	if (free_func == NULL)
	{
		g_list_free (list);
		return;
	}

	for (; list != NULL; list = g_list_remove_link (list, list))
	{
		(*free_func) (list->data);
	}
}


void
util_show_again_toggled_cb (GtkToggleButton * button,
							GnomechatWindowID alert)
{
	prefs_set_show_alert (alert, gtk_toggle_button_get_active (button));
}


LocaleMatch
util_locale_match (const gchar * locale1,
				   const gchar * locale2)
{
	LocaleMatch match = LOCALE_MATCH_NONE;
	gboolean locale1_is_posix,
	  locale2_is_posix;

	g_return_val_if_fail (locale1 != NULL && locale2 != NULL, LOCALE_MATCH_NONE);

	locale1_is_posix = (g_ascii_strcasecmp (locale1, "C") == 0
						|| g_ascii_strcasecmp (locale1, "POSIX") == 0);
	locale2_is_posix = (g_ascii_strcasecmp (locale2, "C") == 0
						|| g_ascii_strcasecmp (locale2, "POSIX") == 0);

	if (locale1_is_posix && locale2_is_posix)
	{
		match = LOCALE_MATCH_LANGUAGE_TERRITORY;
	}
	else
	{
		if (g_ascii_strncasecmp (locale1, locale2, 2) == 0)
		{
			match = LOCALE_MATCH_LANGUAGE;

			if (g_ascii_strncasecmp (locale1, locale2, 5) == 0)
			{
				match = LOCALE_MATCH_LANGUAGE_TERRITORY;
			}
		}
	}

	return match;
}


void
util_object_weak_notify (gpointer ** nullify,
						 GObject * where_object_was)
{
	*nullify = NULL;
}


static gboolean
configure_event_cb (GtkWidget * widget,
					GdkEventConfigure * event,
					gpointer data)
{
	prefs_set_window_width (GPOINTER_TO_UINT (data), event->width);
	prefs_set_window_height (GPOINTER_TO_UINT (data), event->height);

	return FALSE;
}


void
util_set_save_window_size (GtkWidget * window,
						   GnomechatWindowID id)
{
	g_signal_connect (window, "configure-event", G_CALLBACK (configure_event_cb),
					  GUINT_TO_POINTER (id));
}


void
util_boldify_label (GtkWidget * label)
{
	PangoFontDescription *font = pango_font_description_copy (label->style->font_desc);

	pango_font_description_set_weight (font, PANGO_WEIGHT_BOLD);
	gtk_widget_modify_font (label, font);
	pango_font_description_free (font);
}


gchar *
util_get_name_from_uri (const gchar * uri)
{
	gchar *host_bit = NULL,
	 *host = NULL;

	sscanf (uri, "%*[^:]://%a[^/]/", &host_bit);

	if (host_bit != NULL)
	{
		host = g_utf8_strchr (host_bit, -1, '@');

		if (host != NULL)
		{
			host = g_strdup (host + 1);
		}
		else
		{
			host = g_strdup (host_bit);
		}

		g_free (host_bit);
	}

	return host;
}
