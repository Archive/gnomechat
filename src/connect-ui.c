/*
 *  GnomeChat: src/connect-ui.h
 *
 *  Copyright (c) 2002 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


/* *INDENT-OFF* */
#define GNOMECHAT_RESPONSE_EDIT -20

#include "gnomechat.h"
#include "connect-ui.h"

#include "connect.h"
#include "esco-encoding-picker.h"
#include "pixbufs.h"
#include "prefs.h"
#include "prefs-ui-utils.h"
#include "stock-items.h"
#include "utils.h"

#include <gtk/gtkalignment.h>
#include <gtk/gtkcellrendererpixbuf.h>
#include <gtk/gtkcellrenderertext.h>
#include <gtk/gtkcellrenderertoggle.h>
#include <gtk/gtkcheckbutton.h>
#include <gtk/gtkentry.h>
#include <gtk/gtkimage.h>
#include <gtk/gtkhbbox.h>
#include <gtk/gtkhbox.h>
#include <gtk/gtkhpaned.h>
#include <gtk/gtkhseparator.h>
#include <gtk/gtklabel.h>
#include <gtk/gtkmessagedialog.h>
#include <gtk/gtknotebook.h>
#include <gtk/gtkprogressbar.h>
#include <gtk/gtkscrolledwindow.h>
#include <gtk/gtksizegroup.h>
#include <gtk/gtkspinbutton.h>
#include <gtk/gtkstock.h>
#include <gtk/gtktable.h>
#include <gtk/gtktextview.h>
#include <gtk/gtktreeview.h>
#include <gtk/gtkvbbox.h>
#include <gtk/gtkvbox.h>

#include <libgnomeui/gnome-href.h>

#include <libgircclient/girc-utils.h>

#include <locale.h>
#include <string.h>
/* *INDENT-ON* */


/* Structs */
typedef struct _ConnectDialog ConnectDialog;
typedef struct _EditNetworksDialog EditNetworksDialog;
typedef struct _ProgressDialog ProgressDialog;
typedef struct _DisconnectDialog DisconnectDialog;


struct _ProgressDialog
{
	GtkWidget *win;

	GtkWidget *image;
	GtkWidget *label;
	GtkWidget *progress;
	GtkWidget *stop_btn;

	gchar *server_id;
	gchar *net_id;

	gint pulse_id;
};

struct _EditNetworksDialog
{
	GtkWidget *win;

	GtkTreeSelection *network_sel;
	GtkTreeRowReference *network_rowref;

	/* Network Tab */
	GtkWidget *name_entry;
	GtkTextBuffer *desc_buffer;
	GtkWidget *website_entry;

	GtkWidget *autoconnect_check;
	GtkWidget *charset_picker;

	/* Server Tab */
	GtkTreeSelection *server_sel;
	GtkTreeRowReference *server_rowref;
	gulong server_insert_handler_id;
	gulong server_delete_handler_id;

	GtkWidget *server_location_entry;
	GtkWidget *server_addr_entry;
	GtkWidget *server_port_spin;
	GtkWidget *server_passwd_entry;
	GtkWidget *server_charset_picker;

	/* Channels Tab */
	GtkTreeSelection *channel_sel;
	gulong channel_insert_handler_id;
	gulong channel_delete_handler_id;

	/* Data for saving */
	gchar *server_id;
	gchar *net_id;
};

struct _ConnectDialog
{
	GtkWidget *win;

	GtkWidget *optmenu;

	GtkTreeSelection *sel;

	GtkWidget *image;
	GtkWidget *name_label;
	GtkWidget *info_label;
	GtkWidget *url_box;
	GtkWidget *url_href;

	EditNetworksDialog *netedit;
};


struct _DisconnectDialog
{
	GtkWidget *win;
	GtkTreeSelection *sel;
};


/* Global Variables */
static GHashTable *progress_dialogs = NULL;


/* Utility Functions */
static GSList *
string_listify_tree_model (GtkTreeModel * model,
						   gint str_column)
{
	GSList *list = NULL;
	gchar *str = NULL;
	GtkTreeIter iter;
	gboolean keep_going;

	for (keep_going = gtk_tree_model_get_iter_first (model, &iter);
		 keep_going == TRUE; keep_going = gtk_tree_model_iter_next (model, &iter))
	{

		gtk_tree_model_get (model, &iter, str_column, &str, -1);

		if (str != NULL)
			list = g_slist_append (list, str);
	}

	return list;
}


static gboolean
find_and_select_network (GtkTreeModel * model,
						 GtkTreePath * path,
						 GtkTreeIter * iter,
						 gpointer data)
{
	gchar *goal,
	 *id = NULL;
	gboolean retval = FALSE;

	goal = prefs_get_connect_selected_network ();
	gtk_tree_model_get (model, iter, NETWORK_LIST_KEY_ITEM, &id, -1);

	if (goal == NULL || (id != NULL && strcmp (goal, id) == 0))
	{
		gtk_tree_selection_select_iter (GTK_TREE_SELECTION (data), iter);
		util_scroll_tree_view_to_selection (GTK_TREE_SELECTION (data));
		retval = TRUE;
	}

	g_free (goal);
	g_free (id);

	return retval;
}


/* Connection Progress Dialog Stuff */
static void
progress_dialog_free (ProgressDialog * dialog)
{
	if (dialog != NULL)
	{
		if (dialog->server_id)
			g_free (dialog->server_id);

		if (dialog->pulse_id != -1)
		{
			g_source_remove (dialog->pulse_id);
			dialog->pulse_id = -1;
		}

		g_free (dialog);
	}
}


static gboolean
progress_bar_pulse (ProgressDialog * dialog)
{
	if (dialog->progress)
		gtk_progress_bar_pulse (GTK_PROGRESS_BAR (dialog->progress));

	if (dialog->image)
		pixbuf_anim_update (dialog->image);

	return TRUE;
}


static void
progress_response_cb (GtkWidget * window,
					  gint response,
					  ProgressDialog * dialog)
{
	g_hash_table_remove (progress_dialogs, dialog->net_id);
	gtk_widget_destroy (window);
}


void
connect_ui_open_progress_dialog (const gchar * net_id,
								 GtkWindow * parent)
{
	ProgressDialog *dialog = NULL;
	GtkWidget *top_box,
	 *hbox;
	gchar *name,
	 *title;

	if (progress_dialogs != NULL)
	{
		dialog = (ProgressDialog *) g_hash_table_lookup (progress_dialogs, net_id);
	}
	else
	{
		progress_dialogs = g_hash_table_new_full (g_str_hash, g_str_equal,
												  g_free, (GDestroyNotify) progress_dialog_free);
	}

	if (dialog != NULL)
	{
		gtk_window_present (GTK_WINDOW (dialog->win));
		return;
	}

	dialog = g_new0 (ProgressDialog, 1);
	g_assert (dialog != NULL);

	dialog->net_id = g_strdup (net_id);
	dialog->server_id = NULL;

	name = prefs_get_network_name (dialog->net_id);
	title = g_strdup_printf (_("Connecting to %s - %s"), name, GENERIC_NAME);

	dialog->win = gtk_dialog_new_with_buttons (title, parent, GTK_DIALOG_NO_SEPARATOR,
											   GTK_STOCK_STOP, GTK_RESPONSE_CANCEL, NULL);
	g_free (title);
	util_set_window_icon_from_stock (GTK_WINDOW (dialog->win), GNOMECHAT_STOCK_CONNECT);
	gtk_window_set_role (GTK_WINDOW (dialog->win), "ConnectProgress");
	gtk_dialog_set_default_response (GTK_DIALOG (dialog->win), GTK_RESPONSE_CANCEL);
	gtk_window_set_resizable (GTK_WINDOW (dialog->win), TRUE);
	gtk_widget_set_size_request (dialog->win, util_get_font_width (dialog->win) * 60, -1);
	g_signal_connect (dialog->win, "response", G_CALLBACK (progress_response_cb), dialog);

	top_box = gtk_vbox_new (FALSE, 6);
	gtk_container_set_border_width (GTK_CONTAINER (top_box), 5);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (dialog->win)->vbox), top_box);
	gtk_widget_show (top_box);

	hbox = gtk_hbox_new (FALSE, 12);
	gtk_container_add (GTK_CONTAINER (top_box), hbox);
	gtk_widget_show (hbox);

	dialog->image = pixbuf_anim_from_file (GNOMECHAT_GLOBE_ANIM_FILE,
										   GNOMECHAT_GLOBE_ANIM_N_FRAMES);
	g_object_add_weak_pointer (G_OBJECT (dialog->image), (gpointer *) & (dialog->image));
	gtk_box_pack_start (GTK_BOX (hbox), dialog->image, FALSE, FALSE, 0);
	gtk_widget_show (dialog->image);

	dialog->label = gtk_label_new (NULL);
	gtk_label_set_use_markup (GTK_LABEL (dialog->label), TRUE);
	gtk_misc_set_alignment (GTK_MISC (dialog->label), 0.0, 0.5);
	gtk_container_add (GTK_CONTAINER (hbox), dialog->label);
	gtk_widget_show (dialog->label);

	dialog->progress = gtk_progress_bar_new ();
	g_object_add_weak_pointer (G_OBJECT (dialog->progress), (gpointer *) & (dialog->progress));
	gtk_progress_bar_set_pulse_step (GTK_PROGRESS_BAR (dialog->progress), 0.02);
	gtk_box_pack_start (GTK_BOX (top_box), dialog->progress, FALSE, FALSE, 0);
	gtk_widget_show (dialog->progress);

	g_hash_table_insert (progress_dialogs, dialog->net_id, dialog);

	dialog->pulse_id = g_timeout_add (50, (GSourceFunc) progress_bar_pulse, dialog);

	gtk_window_present (GTK_WINDOW (dialog->win));

	connect_open_connection (net_id, dialog->label);
}


/* Network Editing Dialog Stuff */
static void
netedit_save_server_store (GtkTreeModel * model,
						   const gchar * net_id)
{
	GSList *list = string_listify_tree_model (model, SERVER_LIST_KEY_ITEM);

	prefs_set_network_server_ids (net_id, list);

	girc_g_slist_deep_free (list, g_free);
}


static void
netedit_save_network_store (GtkTreeModel * model)
{
	GSList *list = string_listify_tree_model (model, NETWORK_LIST_KEY_ITEM);

	prefs_set_network_ids (list);

	girc_g_slist_deep_free (list, g_free);
}


static void
netedit_save_channel_store (GtkTreeModel * model,
							const gchar * net_id)
{
	GSList *list = string_listify_tree_model (model, CHANNEL_LIST_NAME_ITEM);

	prefs_set_network_channels (net_id, list);

	girc_g_slist_deep_free (list, g_free);
}


static void
netedit_server_row_deleted_cb (GtkTreeModel * model,
							   GtkTreePath * path,
							   const gchar * net_id)
{
	g_message ("Server row being deleted.");
	netedit_save_server_store (model, net_id);
}


static void
netedit_server_row_inserted_cb (GtkTreeModel * model,
								GtkTreePath * path,
								GtkTreeIter * iter,
								const gchar * net_id)
{
	g_message ("Server row being inserted.");
	netedit_save_server_store (model, net_id);
}


static void
netedit_channel_row_deleted_cb (GtkTreeModel * model,
								GtkTreePath * path,
								const gchar * net_id)
{
	netedit_save_channel_store (model, net_id);
}


static void
netedit_channel_row_inserted_cb (GtkTreeModel * model,
								 GtkTreePath * path,
								 GtkTreeIter * iter,
								 const gchar * net_id)
{
	netedit_save_channel_store (model, net_id);
}


static gint
sort_server_tree_by_country_code (GtkTreeModel * model,
								  GtkTreeIter * iter1,
								  GtkTreeIter * iter2,
								  gpointer data)
{
	static gchar *current_locale = NULL;
	gchar *value1,
	 *value2;
	gint retval;

	if (current_locale == NULL)
	{
		gchar *tmp = setlocale (LC_CTYPE, NULL);
		gsize len;

		g_assert (tmp != NULL && tmp[0] != '\0');

		len = strlen (tmp);

		/* POSIX locale, assume location == US */
		if (tmp[0] == 'C')
		{
			current_locale = "US";
		}
		else if (len >= 4)
		{
			/* POSIX locale, assume location == US */
			if (g_ascii_strcasecmp (tmp, "POSIX") == 0)
			{
				current_locale = "US";
			}
			else
			{
				current_locale = g_strndup (tmp + 3, 2);
			}
		}
		else
		{
			current_locale = g_strndup (tmp, 2);
		}
	}

	gtk_tree_model_get (model, iter1, SERVER_LIST_COUNTRY_CODE_ITEM, &value1, -1);
	gtk_tree_model_get (model, iter2, SERVER_LIST_COUNTRY_CODE_ITEM, &value2, -1);

	if (value1 != NULL && value2 == NULL)
	{
		retval = -1;
	}
	else if (value1 == NULL && value2 != NULL)
	{
		retval = 1;
	}
	else
	{
		if (value1 == NULL && value2 == NULL)
		{
			retval = 0;
		}
		/* A "**" country code means "random", so put those first */
		else if (strcmp (value1, "**") == 0)
		{
			if (strcmp (value2, "**") == 0)
			{
				retval = 0;
			}
			else
			{
				retval = -1;
			}
		}
		else if (strcmp (value2, "**") == 0)
		{
			retval = 1;
		}
		else
		{
			if (strcmp (value1, current_locale) == 0)
			{
				if (strcmp (value1, value2) == 0)
				{
					retval = 0;
				}
				else
				{
					retval = -1;
				}
			}
			else if (strcmp (value2, current_locale) == 0)
			{
				retval = 1;
			}
			else
			{
				retval = strcmp (value1, value2);
			}
		}
	}

	if (value1 != NULL)
		g_free (value1);

	if (value2 != NULL)
		g_free (value2);

	/* If the country codes are the same, check the location */
	if (retval == 0)
	{
		gtk_tree_model_get (model, iter1, SERVER_LIST_LOCATION_ITEM, &value1, -1);
		gtk_tree_model_get (model, iter2, SERVER_LIST_LOCATION_ITEM, &value2, -1);

		if (value1 == NULL)
		{
			if (value2 == NULL)
				retval = 0;
			else
				retval = 1;
		}
		else if (value2 == NULL)
		{
			retval = -1;
		}
		else
		{
			retval = g_utf8_collate (value1, value2);
		}

		if (value1 != NULL)
			g_free (value1);

		if (value2 != NULL)
			g_free (value2);

		/* If the locations are the same, we do nothing, because they are placed in the
		   schemas in order of capacity :-). */
	}

	return retval;
}


static gint
sort_tree_by_bool (GtkTreeModel * model,
				   GtkTreeIter * iter1,
				   GtkTreeIter * iter2,
				   gpointer sort_col)
{
	gboolean value1,
	  value2;

	gtk_tree_model_get (model, iter1, GPOINTER_TO_INT (sort_col), &value1, -1);
	gtk_tree_model_get (model, iter2, GPOINTER_TO_INT (sort_col), &value2, -1);

	if (value1 > value2)
		return 1;
	else if (value1 < value2)
		return -1;
	else
		return sort_server_tree_by_country_code (model, iter1, iter2, NULL);
}


static void
netedit_network_sel_changed_cb (GtkTreeSelection * sel,
								EditNetworksDialog * netedit)
{
	GtkTreeModel *model;
	GtkTreePath *path;
	GtkTreeIter iter;

	if (netedit->network_rowref != NULL)
	{
		gtk_tree_row_reference_free (netedit->network_rowref);
		netedit->network_rowref = NULL;
	}

	if (gtk_tree_selection_get_selected (sel, &model, &iter))
	{
		gchar *old_id,
		 *encoding,
		 *desc,
		 *name,
		 *website;

		old_id = netedit->net_id;

		gtk_tree_model_get (model, &iter,
							NETWORK_LIST_KEY_ITEM, &(netedit->net_id),
							NETWORK_LIST_DESCRIPTION_ITEM, &desc,
							NETWORK_LIST_NAME_ITEM, &name, NETWORK_LIST_WEBSITE_ITEM, &website, -1);

		/* TreeRowRef */
		path = gtk_tree_model_get_path (model, &iter);

		if (netedit->network_rowref != NULL)
			gtk_tree_row_reference_free (netedit->network_rowref);
		netedit->network_rowref = gtk_tree_row_reference_new (model, path);

		gtk_tree_path_free (path);

		if (netedit->net_id == NULL)
		{
			netedit->net_id = old_id;
			gtk_tree_model_iter_next (model, &iter);
			gtk_tree_selection_select_iter (sel, &iter);
			return;
		}

		/* Server List */
		model = gtk_tree_view_get_model (netedit->server_sel->tree_view);

		if (netedit->server_insert_handler_id != 0 && model != NULL)
			g_signal_handler_block (model, netedit->server_insert_handler_id);
		if (netedit->server_delete_handler_id != 0 && model != NULL)
			g_signal_handler_block (model, netedit->server_delete_handler_id);

		model = prefs_ui_util_get_server_model (netedit->net_id);
		gtk_tree_view_set_model (netedit->server_sel->tree_view, model);
		gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (model),
										 SERVER_LIST_ENABLED_ITEM, sort_tree_by_bool,
										 SERVER_LIST_ENABLED_ITEM, NULL);
		gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (model),
										 SERVER_LIST_COUNTRY_CODE_ITEM,
										 sort_server_tree_by_country_code, NULL, NULL);
		if (gtk_tree_model_get_iter_first (model, &iter))
			gtk_tree_selection_select_iter (netedit->server_sel, &iter);

		if (netedit->server_delete_handler_id != 0)
		{
			g_signal_handler_unblock (model, netedit->server_delete_handler_id);
		}
		else
		{
			netedit->server_delete_handler_id =
				g_signal_connect (model, "row-deleted",
								  G_CALLBACK (netedit_server_row_deleted_cb), netedit->net_id);
		}

		if (netedit->server_insert_handler_id != 0)
		{
			g_signal_handler_unblock (model, netedit->server_insert_handler_id);
		}
		else
		{
			netedit->server_insert_handler_id =
				g_signal_connect (model, "row-inserted",
								  G_CALLBACK (netedit_server_row_inserted_cb), netedit->net_id);
		}

		gtk_widget_set_sensitive (GTK_WIDGET (netedit->server_sel->tree_view),
								  prefs_get_can_set_network_server_ids (netedit->net_id));

		/* Channels List */
		model = gtk_tree_view_get_model (netedit->channel_sel->tree_view);

		if (netedit->channel_insert_handler_id != 0 && model != NULL)
			g_signal_handler_block (model, netedit->channel_insert_handler_id);
		if (netedit->channel_delete_handler_id != 0 && model != NULL)
			g_signal_handler_block (model, netedit->channel_delete_handler_id);

		model = prefs_ui_util_get_channel_model (netedit->net_id);
		gtk_tree_view_set_model (netedit->channel_sel->tree_view, model);
		if (gtk_tree_model_get_iter_first (model, &iter))
			gtk_tree_selection_select_iter (netedit->channel_sel, &iter);

		if (netedit->channel_delete_handler_id != 0)
		{
			g_signal_handler_unblock (model, netedit->channel_delete_handler_id);
		}
		else
		{
			netedit->channel_delete_handler_id =
				g_signal_connect (model, "row-deleted",
								  G_CALLBACK (netedit_channel_row_deleted_cb), netedit->net_id);
		}

		if (netedit->channel_insert_handler_id != 0)
		{
			g_signal_handler_unblock (model, netedit->channel_insert_handler_id);
		}
		else
		{
			netedit->channel_insert_handler_id =
				g_signal_connect (model, "row-inserted",
								  G_CALLBACK (netedit_channel_row_inserted_cb), netedit->net_id);
		}

		gtk_widget_set_sensitive (GTK_WIDGET (netedit->channel_sel->tree_view),
								  prefs_get_can_set_network_channels (netedit->net_id));

		prefs_set_connect_selected_network (netedit->net_id);

		/* Encoding */
		encoding = prefs_get_network_charset (netedit->net_id);
		if (encoding != NULL)
		{
			esco_encoding_picker_set_encoding (ESCO_ENCODING_PICKER
											   (netedit->charset_picker), encoding);
			g_free (encoding);
		}
		else
		{
			esco_encoding_picker_set_encoding (ESCO_ENCODING_PICKER
											   (netedit->charset_picker), "CP1252");
		}


		/* Togglebutton */
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON
									  (netedit->autoconnect_check),
									  prefs_get_network_autoconnect (netedit->net_id));

		/* Name entry */
		if (name != NULL)
		{
			gtk_entry_set_text (GTK_ENTRY (netedit->name_entry), name);
			g_free (name);
		}
		else
		{
			gtk_entry_set_text (GTK_ENTRY (netedit->name_entry), "");
		}

		/* Description */
		if (desc != NULL)
		{
			gtk_text_buffer_set_text (GTK_TEXT_BUFFER (netedit->desc_buffer), desc, -1);
			g_free (desc);
		}
		else
		{
			gtk_text_buffer_set_text (GTK_TEXT_BUFFER (netedit->desc_buffer), "", 0);
		}

		/* Website entry */
		if (website != NULL)
		{
			gtk_entry_set_text (GTK_ENTRY (netedit->website_entry), website);
			g_free (website);
		}
		else
		{
			gtk_entry_set_text (GTK_ENTRY (netedit->website_entry), "");
		}

		if (old_id != NULL)
			g_free (old_id);
	}
}



static void
netedit_add_clicked_cb (GtkButton * btn,
						EditNetworksDialog * netedit)
{
	GtkTreeModel *model;
	GtkTreeIter parent,
	  iter;
	gchar *key,
	 *name = _("New Network");

	model = gtk_tree_view_get_model (netedit->network_sel->tree_view);
	key = prefs_get_unique_id ();
	prefs_set_network_name (key, name);

	gtk_tree_store_append (GTK_TREE_STORE (model), &iter, &parent);
	gtk_tree_store_set (GTK_TREE_STORE (model), &iter,
						NETWORK_LIST_STOCK_PIXBUF_ITEM, GNOMECHAT_STOCK_NETWORK,
						NETWORK_LIST_NAME_ITEM, name, NETWORK_LIST_KEY_ITEM, key, -1);
	netedit_save_network_store (model);

	gtk_tree_selection_select_iter (netedit->network_sel, &iter);
}


static void
netedit_rm_clicked_cb (GtkWidget * btn,
					   EditNetworksDialog * netedit)
{
	GtkWidget *dialog,
	 *hbox,
	 *check;
	GtkTreeModel *model;
	GtkTreeIter iter;
	gchar *id;
	gint response;

	if (gtk_tree_selection_get_selected (netedit->network_sel, &model, &iter))
	{
		/* Get the current item */
		gtk_tree_model_get (model, &iter, NETWORK_LIST_KEY_ITEM, &id, -1);
		netedit->net_id = id;

		if (prefs_get_show_alert (GNOMECHAT_ALERT_REMOVE_NETWORK))
		{
			gchar *name,
			 *str;

			name = prefs_get_network_name (id);

			str = g_strdup_printf ("<b>%s</b>\n%s", _("Remove %s?"),
								   _("Removing a network will prevent future connections to that "
									 "network."));
			g_free (name);
			dialog = gtk_message_dialog_new (GTK_WINDOW (gtk_widget_get_toplevel (btn)),
											 (GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_MODAL),
											 GTK_MESSAGE_WARNING, GTK_BUTTONS_NONE, str, name);
			g_free (str);

			gtk_dialog_add_buttons (GTK_DIALOG (dialog),
									GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
									GTK_STOCK_REMOVE, GTK_RESPONSE_OK, NULL);
			gtk_window_set_role (GTK_WINDOW (dialog), "RemoveNetwork");

			str = g_strdup_printf (_("Remove Network - %s"), GENERIC_NAME);
			gtk_window_set_title (GTK_WINDOW (dialog), str);
			g_free (str);

			util_set_window_icon_from_stock (GTK_WINDOW (dialog), GTK_STOCK_DIALOG_WARNING);
			gtk_label_set_use_markup (GTK_LABEL (GTK_MESSAGE_DIALOG (dialog)->label), TRUE);
			gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_OK);

			hbox = gtk_hbox_new (FALSE, 0);
			gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), hbox, FALSE, FALSE, 6);
			gtk_widget_show (hbox);

			check = gtk_check_button_new_with_mnemonic (_("_Ask before removing networks "
														  "in the future"));
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check), TRUE);
			g_signal_connect (check, "toggled", G_CALLBACK (util_show_again_toggled_cb),
							  GUINT_TO_POINTER (GNOMECHAT_ALERT_REMOVE_NETWORK));
			gtk_box_pack_start (GTK_BOX (hbox), check, FALSE, FALSE, 12);
			gtk_widget_show (check);

			gtk_widget_set_sensitive (netedit->win, FALSE);
			response = gtk_dialog_run (GTK_DIALOG (dialog));
			gtk_widget_set_sensitive (netedit->win, TRUE);
			gtk_widget_destroy (dialog);

			if (response != GTK_RESPONSE_OK)
				return;
		}

		/* Unset all the keys in the dir */
		prefs_delete_network (netedit->net_id);

		/* Remove the selection from the list and select either the next valid iter,
		   or the first one in the list. */
		if (gtk_tree_store_remove (GTK_TREE_STORE (model), &iter)
			|| gtk_tree_model_get_iter_first (model, &iter))
		{
			gtk_tree_selection_select_iter (netedit->network_sel, &iter);
		}

		/* Save the server ids list */
		netedit_save_network_store (model);
	}
}


static void
netedit_name_changed_cb (GtkEntry * entry,
						 EditNetworksDialog * netedit)
{
	GtkTreeModel *model;
	GtkTreePath *path;
	GtkTreeIter iter;
	const gchar *text = gtk_entry_get_text (entry);

	prefs_set_network_name (netedit->net_id, text);

	model = gtk_tree_view_get_model (netedit->network_sel->tree_view);
	path = gtk_tree_row_reference_get_path (netedit->network_rowref);

	if (gtk_tree_model_get_iter (model, &iter, path))
	{
		gtk_tree_store_set (GTK_TREE_STORE (model), &iter, NETWORK_LIST_NAME_ITEM, text, -1);
	}
}


static void
netedit_website_changed_cb (GtkEntry * entry,
							EditNetworksDialog * netedit)
{
	prefs_set_network_website (netedit->net_id, gtk_entry_get_text (entry));
}


static void
netedit_encoding_changed_cb (EscoEncodingPicker * epicker,
							 EditNetworksDialog * netedit)
{
	prefs_set_network_charset (netedit->net_id, esco_encoding_picker_get_encoding (epicker));
}


static void
netedit_autoconnect_toggled_cb (GtkToggleButton * btn,
								EditNetworksDialog * netedit)
{
	prefs_set_network_autoconnect (netedit->net_id, gtk_toggle_button_get_active (btn));
}


static void
netedit_sel_changed_cb (GtkTreeSelection * sel,
						GtkWidget * button)
{
	gtk_widget_set_sensitive (button, gtk_tree_selection_get_selected (sel, NULL, NULL));
}


static void
netedit_server_sel_changed_cb (GtkTreeSelection * sel,
							   EditNetworksDialog * netedit)
{
	GtkTreeModel *model;
	GtkTreeIter iter;

	if (gtk_tree_selection_get_selected (sel, &model, &iter))
	{
		gchar *location,
		 *address,
		 *encoding,
		 *passwd;
		gint port;

		if (netedit->server_id != NULL)
			g_free (netedit->server_id);

		gtk_tree_model_get (model, &iter, SERVER_LIST_KEY_ITEM, &(netedit->server_id),
							SERVER_LIST_LOCATION_ITEM, &location,
							SERVER_LIST_ADDRESS_ITEM, &address, -1);

		if (location != NULL)
		{
			gtk_entry_set_text (GTK_ENTRY (netedit->server_location_entry), location);
			g_free (location);
		}
		else
		{
			gtk_entry_set_text (GTK_ENTRY (netedit->server_location_entry), "");
		}

		if (address != NULL)
		{
			gtk_entry_set_text (GTK_ENTRY (netedit->server_addr_entry), address);
			g_free (address);
		}
		else
		{
			gtk_entry_set_text (GTK_ENTRY (netedit->server_addr_entry), "");
		}

		port = prefs_get_server_port (netedit->net_id, netedit->server_id);
		gtk_spin_button_set_value (GTK_SPIN_BUTTON (netedit->server_port_spin), (gfloat) port);

		passwd = prefs_get_server_passwd (netedit->net_id, netedit->server_id);
		if (passwd != NULL)
		{
			gtk_entry_set_text (GTK_ENTRY (netedit->server_passwd_entry), passwd);
			g_free (passwd);
		}
		else
		{
			gtk_entry_set_text (GTK_ENTRY (netedit->server_passwd_entry), "");
		}

		encoding = prefs_get_server_charset (netedit->net_id, netedit->server_id);
		if (encoding != NULL)
		{
			esco_encoding_picker_set_encoding (ESCO_ENCODING_PICKER
											   (netedit->server_charset_picker), encoding);
			g_free (encoding);
		}
		else
		{
			encoding = prefs_get_network_charset (netedit->net_id);

			if (encoding != NULL)
			{
				esco_encoding_picker_set_encoding (ESCO_ENCODING_PICKER
												   (netedit->server_charset_picker), encoding);
				g_free (encoding);
			}
			else
			{
				esco_encoding_picker_set_encoding (ESCO_ENCODING_PICKER
												   (netedit->server_charset_picker), "UTF-8");
			}
		}
	}
}


static void
netedit_server_enabled_toggled_cb (GtkCellRendererToggle * cell,
								   const gchar * path,
								   EditNetworksDialog * netedit)
{
	GtkTreeModel *model;
	GtkTreeIter iter;
	gchar *key;
	gboolean value;

	model = gtk_tree_view_get_model (netedit->server_sel->tree_view);

	if (gtk_tree_model_get_iter_from_string (model, &iter, path))
	{
		value = gtk_cell_renderer_toggle_get_active (cell);

		gtk_list_store_set (GTK_LIST_STORE (model), &iter, SERVER_LIST_ENABLED_ITEM, !value, -1);

		gtk_tree_model_get (model, &iter, SERVER_LIST_KEY_ITEM, &key, -1);

		if (key != NULL)
		{
			prefs_set_server_enabled (netedit->net_id, key, !value);
			g_free (key);
		}
	}
}


static void
netedit_server_add_button_clicked_cb (GtkButton * btn,
									  EditNetworksDialog * netedit)
{

}


static void
netedit_server_rm_button_clicked_cb (GtkButton * btn,
									 EditNetworksDialog * netedit)
{
	GtkTreeModel *model;
	GtkTreeIter iter;

	if (gtk_tree_selection_get_selected (netedit->server_sel, &model, &iter))
	{
		gchar *id;

		/* Get the current item */
		gtk_tree_model_get (model, &iter, SERVER_LIST_KEY_ITEM, &id, -1);

		if (prefs_get_show_alert (GNOMECHAT_ALERT_REMOVE_SERVER))
		{
			GtkWidget *dialog,
			 *hbox,
			 *check;
			gchar *name = prefs_get_network_name (netedit->net_id),
			 *address = prefs_get_server_address (netedit->net_id, id),
			 *str;
			gint response;

			str = g_strdup_printf ("<b>%s</b>\n%s", _("Remove %s?"),
								   _("Removing a server will prevent connection attempts to that "
									 "server when connecting to %s."));
			dialog = gtk_message_dialog_new (GTK_WINDOW (netedit->win),
											 (GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_MODAL),
											 GTK_MESSAGE_WARNING, GTK_BUTTONS_NONE,
											 str, address, name);
			g_free (name);
			g_free (address);
			g_free (str);

			str = g_strdup_printf (_("Remove Server - %s"), GENERIC_NAME);
			gtk_window_set_title (GTK_WINDOW (dialog), str);
			g_free (str);

			gtk_dialog_add_buttons (GTK_DIALOG (dialog),
									GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
									GTK_STOCK_REMOVE, GTK_RESPONSE_OK, NULL);
			gtk_window_set_role (GTK_WINDOW (dialog), "RemoveServer");

			util_set_window_icon_from_stock (GTK_WINDOW (dialog), GTK_STOCK_DIALOG_WARNING);
			gtk_label_set_use_markup (GTK_LABEL (GTK_MESSAGE_DIALOG (dialog)->label), TRUE);
			gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_OK);

			hbox = gtk_hbox_new (FALSE, 0);
			gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), hbox, FALSE, FALSE, 6);
			gtk_widget_show (hbox);

			check = gtk_check_button_new_with_mnemonic (_("_Ask before removing servers "
														  "in the future"));
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check), TRUE);
			g_signal_connect (check, "toggled",
							  G_CALLBACK (util_show_again_toggled_cb),
							  GUINT_TO_POINTER (GNOMECHAT_ALERT_REMOVE_SERVER));
			gtk_box_pack_start (GTK_BOX (hbox), check, FALSE, FALSE, 12);
			gtk_widget_show (check);

			gtk_widget_set_sensitive (netedit->win, FALSE);
			response = gtk_dialog_run (GTK_DIALOG (dialog));
			gtk_widget_set_sensitive (netedit->win, TRUE);

			gtk_widget_destroy (dialog);

			if (response != GTK_RESPONSE_OK)
				return;
		}

		/* Unset all the keys in the dir */
		prefs_delete_server (netedit->net_id, id);

		/* Remove the selection from the list and select either the next valid iter,
		   or the first one in the list. */
		if (gtk_list_store_remove (GTK_LIST_STORE (model), &iter)
			|| gtk_tree_model_get_iter_first (model, &iter))
		{
			gtk_tree_selection_select_iter (netedit->server_sel, &iter);
		}
	}
}


static void
netedit_server_location_changed_cb (GtkEntry * entry,
									EditNetworksDialog * netedit)
{
	prefs_set_server_location (netedit->net_id, netedit->server_id, gtk_entry_get_text (entry));
}


static void
netedit_server_address_changed_cb (GtkEntry * entry,
								   EditNetworksDialog * netedit)
{
	prefs_set_server_address (netedit->net_id, netedit->server_id, gtk_entry_get_text (entry));
}


static void
netedit_server_port_changed_cb (GtkSpinButton * spinbtn,
								EditNetworksDialog * netedit)
{
	prefs_set_server_port (netedit->net_id, netedit->server_id,
						   gtk_spin_button_get_value_as_int (spinbtn));
}


static void
netedit_server_passwd_changed_cb (GtkEntry * entry,
								  EditNetworksDialog * netedit)
{
	prefs_set_server_passwd (netedit->net_id, netedit->server_id, gtk_entry_get_text (entry));
}


static void
netedit_server_encoding_changed_cb (EscoEncodingPicker * epicker,
									EditNetworksDialog * netedit)
{
	prefs_set_server_charset (netedit->net_id, netedit->server_id,
							  esco_encoding_picker_get_encoding (epicker));
}


static void
netedit_channel_item_edited_cb (GtkCellRendererText * cell,
								const gchar * path,
								const gchar * new_text,
								EditNetworksDialog * netedit)
{
	GtkTreeModel *model;
	GtkTreeIter iter;

	if (gtk_tree_selection_get_selected (netedit->channel_sel, &model, &iter))
	{
		gtk_list_store_set (GTK_LIST_STORE (model), &iter, CHANNEL_LIST_NAME_ITEM, new_text, -1);
		netedit_save_channel_store (model, netedit->net_id);
	}
}


static void
netedit_channel_add_button_clicked_cb (GtkButton * btn,
									   GtkTreeSelection * sel)
{
	GtkTreeModel *model;
	GtkTreePath *path;
	GtkTreeViewColumn *col;
	GtkTreeIter iter;

	model = gtk_tree_view_get_model (sel->tree_view);
	gtk_list_store_append (GTK_LIST_STORE (model), &iter);
	gtk_list_store_set (GTK_LIST_STORE (model), &iter,
						CHANNEL_LIST_NAME_ITEM, _("#ChannelName"), -1);

	path = gtk_tree_model_get_path (model, &iter);
	col = gtk_tree_view_get_column (sel->tree_view, 0);

	gtk_tree_view_set_cursor (sel->tree_view, path, col, TRUE);
	gtk_tree_path_free (path);
}


static void
netedit_channel_rm_button_clicked_cb (GtkButton * btn,
									  EditNetworksDialog * netedit)
{
	GtkTreeModel *model;
	GtkTreeIter iter;

	if (gtk_tree_selection_get_selected (netedit->channel_sel, &model, &iter))
	{
		/* Remove the selection from the list and select either the next valid iter,
		   or the first one in the list. */
		if (gtk_list_store_remove (GTK_LIST_STORE (model), &iter)
			|| gtk_tree_model_get_iter_first (model, &iter))
		{
			gtk_tree_selection_select_iter (netedit->channel_sel, &iter);
		}
	}
}


static void
netedit_response_cb (GtkWidget * win,
					 gint response,
					 ConnectDialog * dialog)
{
	GtkTreeModel *model;

	switch (response)
	{
	case GTK_RESPONSE_HELP:
		g_message ("FIXME: Connect help system to \"Edit Networks\" dialog.");
		return;
		break;

	case GTK_RESPONSE_OK:
		prefs_netedit_dialog_closed ();
		break;

	default:
		prefs_netedit_dialog_cancelled ();
		break;
	}

	/* Reload the ConnectDialog's list */
	model = prefs_ui_util_get_network_model ();

	gtk_tree_view_set_model (dialog->sel->tree_view, model);
	gtk_tree_view_expand_all (dialog->sel->tree_view);
	gtk_tree_model_foreach (model, find_and_select_network, dialog->sel);
	gtk_widget_set_sensitive (dialog->win, TRUE);

	g_object_unref (gtk_tree_view_get_model (dialog->netedit->server_sel->tree_view));
	g_object_unref (gtk_tree_view_get_model (dialog->netedit->channel_sel->tree_view));

	gtk_widget_destroy (win);
	g_free (dialog->netedit);
	dialog->netedit = NULL;
}


static void
open_netedit_dialog (ConnectDialog * dialog)
{
	EditNetworksDialog *netedit;
	GtkWidget *paned,
	 *vbox,
	 *notebook,
	 *page_box,
	 *group_box,
	 *hbox,
	 *table,
	 *bbox,
	 *align,
	 *scrwin,
	 *label,
	 *line,
	 *button,
	 *tree_view,
	 *text_view;
	GtkTreeViewColumn *col;
	GtkCellRenderer *cell;
	GtkSizeGroup *size_group;
	gchar *str;

	g_return_if_fail (dialog != NULL);

	if (dialog->netedit != NULL)
	{
		gtk_window_present (GTK_WINDOW (dialog->netedit->win));
		return;
	}

	prefs_netedit_dialog_opened ();

	dialog->netedit = netedit = g_new0 (EditNetworksDialog, 1);

	str = g_strdup_printf (_("Edit Networks - %s"), GENERIC_NAME);
	netedit->win = gtk_dialog_new_with_buttons (str, GTK_WINDOW (dialog->win),
												(GTK_DIALOG_DESTROY_WITH_PARENT
												 | GTK_DIALOG_NO_SEPARATOR),
												GTK_STOCK_HELP, GTK_RESPONSE_HELP,
												GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
												GTK_STOCK_OK, GTK_RESPONSE_OK, NULL);
	g_free (str);
	util_set_window_icon_from_stock (GTK_WINDOW (netedit->win), GNOMECHAT_STOCK_NETWORK);
	g_signal_connect (netedit->win, "response", G_CALLBACK (netedit_response_cb), dialog);

	size_group = gtk_size_group_new (GTK_SIZE_GROUP_HORIZONTAL);

	paned = gtk_hpaned_new ();
	gtk_container_set_border_width (GTK_CONTAINER (paned), 5);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (netedit->win)->vbox), paned);
	gtk_widget_show (paned);


	/* Networks List Pane */
	vbox = gtk_vbox_new (FALSE, 3);
	gtk_paned_pack1 (GTK_PANED (paned), vbox, FALSE, FALSE);
	gtk_widget_show (vbox);

	scrwin = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW
									(scrwin), GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrwin), GTK_SHADOW_IN);
	gtk_container_add (GTK_CONTAINER (vbox), scrwin);
	gtk_widget_show (scrwin);

	tree_view = gtk_tree_view_new_with_model (prefs_ui_util_get_network_model ());
	gtk_tree_view_expand_all (GTK_TREE_VIEW (tree_view));
	gtk_container_add (GTK_CONTAINER (scrwin), tree_view);
	gtk_widget_show (tree_view);

	netedit->network_sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree_view));
	g_signal_connect (netedit->network_sel, "changed",
					  G_CALLBACK (netedit_network_sel_changed_cb), netedit);

	col = gtk_tree_view_column_new ();
	gtk_tree_view_column_set_title (col, _("Networks"));
	gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view), col);

	cell = gtk_cell_renderer_pixbuf_new ();
	gtk_tree_view_column_pack_start (col, cell, FALSE);
	gtk_tree_view_column_add_attribute (col, cell, "stock-id", NETWORK_LIST_STOCK_PIXBUF_ITEM);

	cell = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_start (col, cell, TRUE);
	gtk_tree_view_column_add_attribute (col, cell, "text", NETWORK_LIST_NAME_ITEM);

	bbox = gtk_hbutton_box_new ();
	gtk_button_box_set_layout (GTK_BUTTON_BOX (bbox), GTK_BUTTONBOX_START);
	gtk_box_pack_start (GTK_BOX (vbox), bbox, FALSE, FALSE, 0);

	if (prefs_get_can_set_network_ids ())
	{
		gtk_tree_view_set_reorderable (GTK_TREE_VIEW (tree_view), TRUE);
		gtk_widget_show (bbox);
	}

	button = gtk_button_new_with_mnemonic (_("Add"));
	g_signal_connect (button, "clicked", G_CALLBACK (netedit_add_clicked_cb), netedit);
	gtk_container_add (GTK_CONTAINER (bbox), button);
	gtk_widget_show (button);

	button = gtk_button_new_from_stock (_("Remove"));
	g_signal_connect (button, "clicked", G_CALLBACK (netedit_rm_clicked_cb), netedit);
	gtk_container_add (GTK_CONTAINER (bbox), button);
	gtk_widget_show (button);


	/* Network Editing Pane */
	notebook = gtk_notebook_new ();
	gtk_paned_pack2 (GTK_PANED (paned), notebook, TRUE, TRUE);
	gtk_widget_show (notebook);

	label = gtk_label_new (_("Network"));
	gtk_widget_show (label);

	/* Network Page */
	page_box = gtk_vbox_new (FALSE, 12);
	gtk_container_set_border_width (GTK_CONTAINER (page_box), 9);
	gtk_notebook_append_page (GTK_NOTEBOOK (notebook), page_box, label);
	gtk_widget_show (page_box);

	/* Information Group */
	group_box = gtk_vbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (page_box), group_box, FALSE, FALSE, 0);
	gtk_widget_show (group_box);

	hbox = gtk_hbox_new (FALSE, 3);
	gtk_box_pack_start (GTK_BOX (group_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new (_("<b>Information</b>"));
	gtk_label_set_use_markup (GTK_LABEL (label), TRUE);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	line = gtk_hseparator_new ();
	gtk_container_add (GTK_CONTAINER (hbox), line);
	gtk_widget_show (line);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (group_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	table = gtk_table_new (3, 2, FALSE);
	gtk_table_set_row_spacings (GTK_TABLE (table), 3);
	gtk_box_pack_start (GTK_BOX (hbox), table, TRUE, TRUE, 24);
	gtk_widget_show (table);

	/* Network Name */
	label = gtk_label_new_with_mnemonic (_("_Name: "));
	gtk_size_group_add_widget (size_group, label);
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 0, 1, GTK_FILL, GTK_FILL, 0, 0);
	gtk_widget_show (label);

	netedit->name_entry = gtk_entry_new ();
	util_set_label_widget_pair (label, netedit->name_entry);
	g_signal_connect (netedit->name_entry, "changed",
					  G_CALLBACK (netedit_name_changed_cb), netedit);
	gtk_table_attach (GTK_TABLE (table), netedit->name_entry,
					  1, 2, 0, 1, (GTK_EXPAND | GTK_FILL), GTK_FILL, 0, 0);
	gtk_widget_show (netedit->name_entry);

	/* Network Description */
	label = gtk_label_new_with_mnemonic (_("_Description: "));
	gtk_size_group_add_widget (size_group, label);
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.0);
	gtk_misc_set_padding (GTK_MISC (label), 0, 4);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 1, 2, GTK_FILL, GTK_FILL, 0, 0);
	gtk_widget_show (label);

	scrwin = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrwin), GTK_POLICY_AUTOMATIC,
									GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrwin), GTK_SHADOW_IN);
	gtk_table_attach (GTK_TABLE (table), scrwin, 1, 2, 1, 2,
					  (GTK_EXPAND | GTK_FILL), GTK_FILL, 0, 0);
	gtk_widget_show (scrwin);

	text_view = gtk_text_view_new ();
	util_set_label_widget_pair (label, text_view);
	gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW (text_view), GTK_WRAP_WORD);
	gtk_container_add (GTK_CONTAINER (scrwin), text_view);
	gtk_widget_show (text_view);

	netedit->desc_buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (text_view));

	/* Network Website URL */
	label = gtk_label_new_with_mnemonic (_("_Website: "));
	gtk_size_group_add_widget (size_group, label);
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.0);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 2, 3, GTK_FILL, GTK_FILL, 0, 0);
	gtk_widget_show (label);

	netedit->website_entry = gtk_entry_new ();
	util_set_label_widget_pair (label, netedit->website_entry);
	g_signal_connect (netedit->website_entry, "changed",
					  G_CALLBACK (netedit_website_changed_cb), netedit);
	gtk_table_attach (GTK_TABLE (table), netedit->website_entry,
					  1, 2, 2, 3, (GTK_EXPAND | GTK_FILL), GTK_FILL, 0, 0);
	gtk_widget_show (netedit->website_entry);

	/* Technical Group */
	group_box = gtk_vbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (page_box), group_box, FALSE, FALSE, 0);
	gtk_widget_show (group_box);

	hbox = gtk_hbox_new (FALSE, 3);
	gtk_box_pack_start (GTK_BOX (group_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new (_("<b>Technical</b>"));
	gtk_label_set_use_markup (GTK_LABEL (label), TRUE);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	line = gtk_hseparator_new ();
	gtk_container_add (GTK_CONTAINER (hbox), line);
	gtk_widget_show (line);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (group_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	table = gtk_table_new (3, 2, FALSE);
	gtk_table_set_row_spacings (GTK_TABLE (table), 3);
	gtk_box_pack_start (GTK_BOX (hbox), table, TRUE, TRUE, 24);
	gtk_widget_show (table);

	/* Autoconnect */
	netedit->autoconnect_check = gtk_check_button_new_with_mnemonic (_("_Automatically "
																	   "connect to this "
																	   "network on " "startup"));
	g_signal_connect (netedit->autoconnect_check, "toggled",
					  G_CALLBACK (netedit_autoconnect_toggled_cb), netedit);
	gtk_table_attach (GTK_TABLE (table), netedit->autoconnect_check, 0, 2, 0, 1,
					  (GTK_EXPAND | GTK_FILL), GTK_FILL, 0, 0);
	gtk_widget_show (netedit->autoconnect_check);

	/* Network Encoding */
	label = gtk_label_new_with_mnemonic (_("Default _encoding: "));
	gtk_size_group_add_widget (size_group, label);
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 1, 2, GTK_FILL, GTK_FILL, 0, 0);
	gtk_widget_show (label);

	align = gtk_alignment_new (0.0, 0.5, 0.0, 0.0);
	gtk_table_attach (GTK_TABLE (table), align, 1, 2, 1, 2,
					  (GTK_EXPAND | GTK_FILL), GTK_FILL, 0, 0);
	gtk_widget_show (align);

	netedit->charset_picker = esco_encoding_picker_new (_("Select An Encoding - IRC Chat"));
	util_set_tooltip (netedit->charset_picker,
					  _("What character encoding this network uses by default"),
					  _("This item allows you to select the character encoding this "
						"network is using. This option is typically only useful when "
						"chats on this network show all \"?\" characters."), FALSE);
	util_set_label_widget_pair (label, netedit->charset_picker);
	g_signal_connect (netedit->charset_picker, "encoding-changed",
					  G_CALLBACK (netedit_encoding_changed_cb), netedit);
	gtk_container_add (GTK_CONTAINER (align), netedit->charset_picker);
	gtk_widget_show (netedit->charset_picker);


	/* Servers Page */
	label = gtk_label_new (_("Servers"));
	gtk_widget_show (label);

	page_box = gtk_vbox_new (FALSE, 12);
	gtk_container_set_border_width (GTK_CONTAINER (page_box), 9);
	gtk_notebook_append_page (GTK_NOTEBOOK (notebook), page_box, label);
	gtk_widget_show (page_box);

	/* Server List */
	group_box = gtk_vbox_new (FALSE, 6);
	gtk_container_add (GTK_CONTAINER (page_box), group_box);
	gtk_widget_show (group_box);

	hbox = gtk_hbox_new (FALSE, 6);
	gtk_container_add (GTK_CONTAINER (group_box), hbox);
	gtk_widget_show (hbox);

	scrwin = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW
									(scrwin), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrwin), GTK_SHADOW_IN);
	gtk_widget_set_size_request (scrwin, util_get_font_width (scrwin) * 45,
								 util_get_font_height (scrwin) * 7);
	gtk_container_add (GTK_CONTAINER (hbox), scrwin);
	gtk_widget_show (scrwin);

	tree_view = gtk_tree_view_new ();
	gtk_tree_view_set_reorderable (GTK_TREE_VIEW (tree_view), TRUE);
	gtk_container_add (GTK_CONTAINER (scrwin), tree_view);
	gtk_widget_show (tree_view);

	netedit->server_sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree_view));
	g_signal_connect (netedit->server_sel, "changed",
					  G_CALLBACK (netedit_server_sel_changed_cb), netedit);

	col = gtk_tree_view_column_new ();
	gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view), col);

	cell = gtk_cell_renderer_toggle_new ();
	g_object_set (cell, "xalign", 0.5, NULL);
	g_signal_connect (cell, "toggled", G_CALLBACK (netedit_server_enabled_toggled_cb), netedit);
	gtk_tree_view_column_pack_start (col, cell, FALSE);
	gtk_tree_view_column_add_attribute (col, cell, "active", SERVER_LIST_ENABLED_ITEM);
	gtk_tree_view_column_set_sort_column_id (col, SERVER_LIST_ENABLED_ITEM);

	col = gtk_tree_view_column_new ();
	gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view), col);
	gtk_tree_view_column_set_title (col, _("Location"));

	cell = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_start (col, cell, FALSE);
	gtk_tree_view_column_add_attribute (col, cell, "text", SERVER_LIST_LOCATION_ITEM);
	gtk_tree_view_column_set_sort_column_id (col, SERVER_LIST_COUNTRY_CODE_ITEM);

	col = gtk_tree_view_column_new ();
	gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view), col);
	gtk_tree_view_column_set_title (col, _("Address"));

	cell = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_start (col, cell, FALSE);
	gtk_tree_view_column_add_attribute (col, cell, "text", SERVER_LIST_ADDRESS_ITEM);
	gtk_tree_view_column_set_sort_column_id (col, SERVER_LIST_ADDRESS_ITEM);

	bbox = gtk_vbutton_box_new ();
	gtk_button_box_set_layout (GTK_BUTTON_BOX (bbox), GTK_BUTTONBOX_START);
	gtk_box_pack_start (GTK_BOX (hbox), bbox, FALSE, FALSE, 0);
	gtk_widget_show (bbox);

	button = gtk_button_new_with_label (_("Add"));
	g_signal_connect (button, "clicked",
					  G_CALLBACK (netedit_server_add_button_clicked_cb), netedit);
	gtk_container_add (GTK_CONTAINER (bbox), button);
	gtk_widget_show (button);

	button = gtk_button_new_with_label (_("Remove"));
	g_signal_connect (button, "clicked", G_CALLBACK (netedit_server_rm_button_clicked_cb), netedit);
	gtk_widget_set_sensitive (button, FALSE);
	gtk_container_add (GTK_CONTAINER (bbox), button);
	gtk_widget_show (button);

	/* We connect the selection signal (again) here so we can pass the rm button */
	g_signal_connect (netedit->server_sel, "changed", G_CALLBACK (netedit_sel_changed_cb), button);

	/* Server Data Group */
	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (group_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	table = gtk_table_new (5, 2, FALSE);
	gtk_table_set_row_spacings (GTK_TABLE (table), 3);
	gtk_box_pack_start (GTK_BOX (hbox), table, TRUE, TRUE, 24);
	gtk_widget_show (table);

	label = gtk_label_new_with_mnemonic (_("_Location: "));
	gtk_size_group_add_widget (size_group, label);
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 0, 1, GTK_FILL, GTK_FILL, 0, 0);
	gtk_widget_show (label);

	netedit->server_location_entry = gtk_entry_new ();
	util_set_label_widget_pair (label, netedit->server_location_entry);
	g_signal_connect (netedit->server_location_entry, "changed",
					  G_CALLBACK (netedit_server_location_changed_cb), netedit);
	gtk_table_attach (GTK_TABLE (table), netedit->server_location_entry, 1, 2, 0, 1,
					  (GTK_EXPAND | GTK_FILL), GTK_FILL, 0, 0);
	gtk_widget_show (netedit->server_location_entry);

	label = gtk_label_new_with_mnemonic (_("_Address: "));
	gtk_size_group_add_widget (size_group, label);
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 1, 2, GTK_FILL, GTK_FILL, 0, 0);
	gtk_widget_show (label);

	netedit->server_addr_entry = gtk_entry_new ();
	util_set_label_widget_pair (label, netedit->server_addr_entry);
	g_signal_connect (netedit->server_addr_entry, "changed",
					  G_CALLBACK (netedit_server_address_changed_cb), netedit);
	gtk_table_attach (GTK_TABLE (table), netedit->server_addr_entry, 1, 2, 1, 2,
					  (GTK_EXPAND | GTK_FILL), GTK_FILL, 0, 0);
	gtk_widget_show (netedit->server_addr_entry);

	label = gtk_label_new_with_mnemonic (_("_Port: "));
	gtk_size_group_add_widget (size_group, label);
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 2, 3, GTK_FILL, GTK_FILL, 0, 0);
	gtk_widget_show (label);

	align = gtk_alignment_new (0.0, 0.5, 0.1, 0.0);
	gtk_table_attach (GTK_TABLE (table), align, 1, 2, 2, 3, GTK_FILL, GTK_FILL, 0, 0);
	gtk_widget_show (align);

	netedit->server_port_spin =
		gtk_spin_button_new (GTK_ADJUSTMENT (gtk_adjustment_new (6667.0, 1.0,
																 65535.0, 1.0,
																 10.0, 10.0)), 1.0, 0);
	util_set_label_widget_pair (label, netedit->server_port_spin);
	g_signal_connect (netedit->server_port_spin, "value-changed",
					  G_CALLBACK (netedit_server_port_changed_cb), netedit);
	gtk_container_add (GTK_CONTAINER (align), netedit->server_port_spin);
	gtk_widget_show (netedit->server_port_spin);

	label = gtk_label_new_with_mnemonic (_("Pass_word: "));
	gtk_size_group_add_widget (size_group, label);
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 3, 4, GTK_FILL, GTK_FILL, 0, 0);
	gtk_widget_show (label);

	netedit->server_passwd_entry = gtk_entry_new ();
	util_set_label_widget_pair (label, netedit->server_passwd_entry);
	g_signal_connect (netedit->server_passwd_entry, "changed",
					  G_CALLBACK (netedit_server_passwd_changed_cb), netedit);
	gtk_table_attach (GTK_TABLE (table), netedit->server_passwd_entry, 1, 2, 3, 4,
					  (GTK_EXPAND | GTK_FILL), GTK_FILL, 0, 0);
	gtk_widget_show (netedit->server_passwd_entry);

	label = gtk_label_new_with_mnemonic (_("_Encoding: "));
	gtk_size_group_add_widget (size_group, label);
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 4, 5, GTK_FILL, GTK_FILL, 0, 0);
	gtk_widget_show (label);

	align = gtk_alignment_new (0.0, 0.5, 0.0, 0.0);
	gtk_table_attach (GTK_TABLE (table), align, 1, 2, 4, 5, (GTK_EXPAND | GTK_FILL),
					  GTK_FILL, 0, 0);
	gtk_widget_show (align);

	netedit->server_charset_picker = esco_encoding_picker_new (_("Select An Encoding - IRC Chat"));
	util_set_tooltip (netedit->server_charset_picker,
					  _("What character encoding this server uses by default"),
					  _("This item allows you to select the character encoding this "
						"network is using. This option is typically only useful when "
						"chats on this network show all \"?\" characters."), FALSE);
	util_set_label_widget_pair (label, netedit->server_charset_picker);
	g_signal_connect (netedit->server_charset_picker, "encoding-changed",
					  G_CALLBACK (netedit_server_encoding_changed_cb), netedit);
	gtk_container_add (GTK_CONTAINER (align), netedit->server_charset_picker);
	gtk_widget_show (netedit->server_charset_picker);


	/* Channels page */
	label = gtk_label_new (_("Channels"));
	gtk_widget_show (label);

	page_box = gtk_vbox_new (FALSE, 12);
	gtk_container_set_border_width (GTK_CONTAINER (page_box), 9);
	gtk_notebook_append_page (GTK_NOTEBOOK (notebook), page_box, label);
	gtk_widget_show (page_box);

	/* Channels Group */
	group_box = gtk_hbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (page_box), group_box, FALSE, FALSE, 0);
	gtk_widget_show (group_box);

	scrwin = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrwin), GTK_POLICY_AUTOMATIC,
									GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrwin), GTK_SHADOW_IN);
	gtk_widget_set_size_request (scrwin, util_get_font_width (scrwin) * 32,
								 util_get_font_height (scrwin) * 7);
	gtk_container_add (GTK_CONTAINER (group_box), scrwin);
	gtk_widget_show (scrwin);

	tree_view = gtk_tree_view_new ();
	gtk_tree_view_set_reorderable (GTK_TREE_VIEW (tree_view), TRUE);
	gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (tree_view), FALSE);
	gtk_container_add (GTK_CONTAINER (scrwin), tree_view);
	gtk_widget_show (tree_view);

	netedit->channel_sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree_view));
	col = gtk_tree_view_column_new ();
	gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view), col);

	cell = gtk_cell_renderer_pixbuf_new ();
	g_object_set (cell, "stock-id", GNOMECHAT_STOCK_CHANNEL, NULL);
	gtk_tree_view_column_pack_start (col, cell, FALSE);

	cell = gtk_cell_renderer_text_new ();
	g_object_set (cell, "editable", TRUE, NULL);
	g_signal_connect (cell, "edited", G_CALLBACK (netedit_channel_item_edited_cb), netedit);
	gtk_tree_view_column_pack_start (col, cell, TRUE);
	gtk_tree_view_column_add_attribute (col, cell, "text", CHANNEL_LIST_NAME_ITEM);

	bbox = gtk_vbutton_box_new ();
	gtk_button_box_set_layout (GTK_BUTTON_BOX (bbox), GTK_BUTTONBOX_START);
	gtk_box_pack_start (GTK_BOX (group_box), bbox, FALSE, FALSE, 0);
	gtk_widget_show (bbox);

	button = gtk_button_new_with_label (_("Add"));
	g_signal_connect (button, "clicked",
					  G_CALLBACK (netedit_channel_add_button_clicked_cb), netedit->channel_sel);
	gtk_container_add (GTK_CONTAINER (bbox), button);
	gtk_widget_show (button);

	button = gtk_button_new_with_label (_("Remove"));
	gtk_widget_set_sensitive (button, FALSE);
	g_signal_connect (button, "clicked",
					  G_CALLBACK (netedit_channel_rm_button_clicked_cb), netedit);
	gtk_container_add (GTK_CONTAINER (bbox), button);
	gtk_widget_show (button);

	/* We connect the selection signal down here so we can pass the rm button */
	g_signal_connect (netedit->channel_sel, "changed", G_CALLBACK (netedit_sel_changed_cb), button);

	g_object_unref (size_group);

	gtk_widget_set_sensitive (dialog->win, FALSE);
	gtk_window_present (GTK_WINDOW (netedit->win));
}


/* "Connect..." Dialog Stuff */
static void
dialog_sel_changed_cb (GtkTreeSelection * sel,
					   ConnectDialog * dialog)
{
	GtkTreeModel *model;
	GtkTreeIter iter;
	gchar *id,
	 *stock_id,
	 *name,
	 *name_text,
	 *info_text,
	 *website;

	if (gtk_tree_selection_get_selected (sel, &model, &iter))
	{
		gtk_dialog_set_response_sensitive (GTK_DIALOG (dialog->win), GTK_RESPONSE_OK, TRUE);
		gtk_tree_model_get (model, &iter,
							NETWORK_LIST_KEY_ITEM, &id,
							NETWORK_LIST_STOCK_PIXBUF_ITEM, &stock_id,
							NETWORK_LIST_NAME_ITEM, &name,
							NETWORK_LIST_DESCRIPTION_ITEM, &info_text,
							NETWORK_LIST_WEBSITE_ITEM, &website, -1);

		if (id != NULL)
		{
			gtk_dialog_set_response_sensitive (GTK_DIALOG (dialog->win), GTK_RESPONSE_OK, TRUE);

			prefs_set_connect_selected_network (id);
			g_free (id);
		}
		else
		{
			gtk_dialog_set_response_sensitive (GTK_DIALOG (dialog->win), GTK_RESPONSE_OK, FALSE);
		}

		gtk_image_set_from_stock (GTK_IMAGE (dialog->image), stock_id, GTK_ICON_SIZE_DIALOG);

		name_text = g_strdup_printf ("<span weight=\"bold\" size=\"xx-large\">%s</span>",
									 (name != NULL ? name : _("(Unknown)")));
		gtk_label_set_markup (GTK_LABEL (dialog->name_label), name_text);
		g_free (name_text);

		gtk_label_set_text (GTK_LABEL (dialog->info_label), info_text);

		if (stock_id != NULL)
			g_free (stock_id);

		if (name != NULL)
			g_free (name);

		if (info_text != NULL)
			g_free (info_text);

		if (website != NULL)
		{
			if (website[0] != '\0')
			{
				gnome_href_set_text (GNOME_HREF (dialog->url_href), website);
				gnome_href_set_url (GNOME_HREF (dialog->url_href), website);

				gtk_widget_show (dialog->url_box);
			}
			else
			{
				gtk_widget_hide (dialog->url_box);
			}

			g_free (website);
		}
		else
		{
			gtk_widget_hide (dialog->url_box);
		}
	}
	else
	{
		gtk_dialog_set_response_sensitive (GTK_DIALOG (dialog->win), GTK_RESPONSE_OK, FALSE);

		/* If nothing is selected, find and select something */
		gtk_tree_model_foreach (model, find_and_select_network, sel);
	}
}


static void
dialog_response_cb (GtkWidget * win,
					gint response,
					ConnectDialog ** dialog_ptr)
{
	ConnectDialog *dialog = *dialog_ptr;

	switch (response)
	{
	case GTK_RESPONSE_HELP:
		g_message ("FIXME: Connect help system to \"Connect\" dialog.");
		break;

	case GNOMECHAT_RESPONSE_EDIT:
		open_netedit_dialog (dialog);
		break;

	case GTK_RESPONSE_OK:
		{
			GtkTreeModel *model;
			GtkTreeIter iter;

			if (gtk_tree_selection_get_selected (dialog->sel, &model, &iter))
			{
				gchar *id;

				gtk_tree_model_get (model, &iter, NETWORK_LIST_KEY_ITEM, &id, -1);
				connect_ui_open_progress_dialog (id, NULL);

				g_free (id);
			}
		}
	default:
		gtk_widget_destroy (win);
		g_free (dialog);
		*dialog_ptr = NULL;
		break;
	}
}


void
connect_ui_open_dialog (GtkWindow * parent)
{
	static ConnectDialog *dialog = NULL;

	GtkWidget *paned,
	 *scrwin,
	 *tree_view,
	 *vbox,
	 *hbox,
	 *align,
	 *label,
	 *line;

	GtkTreeModel *model;
	GtkTreeViewColumn *col;
	GtkCellRenderer *cell;

	if (dialog != NULL)
	{
		gtk_window_present (GTK_WINDOW (dialog->win));

		if (dialog->netedit != NULL)
		{
			gtk_window_present (GTK_WINDOW (dialog->netedit->win));
		}

		return;
	}

	dialog = g_new0 (ConnectDialog, 1);
	dialog->win = gtk_dialog_new_with_buttons (_("Connect - IRC Chat"), parent,
											   GTK_DIALOG_NO_SEPARATOR,
											   GTK_STOCK_HELP, GTK_RESPONSE_HELP,
											   GNOMECHAT_STOCK_EDIT,
											   GNOMECHAT_RESPONSE_EDIT,
											   GTK_STOCK_CLOSE, GTK_RESPONSE_CANCEL,
											   GNOMECHAT_STOCK_CONNECT, GTK_RESPONSE_OK, NULL);
	util_set_window_icon_from_stock (GTK_WINDOW (dialog->win), GNOMECHAT_STOCK_CONNECT);
	gtk_window_set_role (GTK_WINDOW (dialog->win), "Connect");
	gtk_dialog_set_default_response (GTK_DIALOG (dialog->win), GTK_RESPONSE_OK);
	g_signal_connect (dialog->win, "response", G_CALLBACK (dialog_response_cb), &dialog);

	paned = gtk_hpaned_new ();
	gtk_container_set_border_width (GTK_CONTAINER (paned), 5);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (dialog->win)->vbox), paned);
	gtk_widget_show (paned);

	vbox = gtk_vbox_new (FALSE, 3);
	gtk_paned_pack1 (GTK_PANED (paned), vbox, FALSE, FALSE);
	gtk_widget_show (vbox);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new (_("Show: "));
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	align = gtk_alignment_new (0.0, 0.5, 0.0, 0.0);
	gtk_container_add (GTK_CONTAINER (hbox), align);
	gtk_widget_show (align);

	dialog->optmenu = gtk_option_menu_new ();
	gtk_container_add (GTK_CONTAINER (align), dialog->optmenu);
	gtk_widget_show (dialog->optmenu);

	/* List */
	scrwin = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrwin), GTK_POLICY_NEVER,
									GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrwin), GTK_SHADOW_IN);
	gtk_container_add (GTK_CONTAINER (vbox), scrwin);
	gtk_widget_set_size_request (scrwin, -1, util_get_font_height (scrwin) * 9);
	gtk_widget_show (scrwin);

	model = prefs_ui_util_get_network_model ();
	tree_view = gtk_tree_view_new_with_model (model);
	gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (tree_view), FALSE);
	gtk_tree_view_set_reorderable (GTK_TREE_VIEW (tree_view), TRUE);
	gtk_tree_view_expand_all (GTK_TREE_VIEW (tree_view));
	gtk_container_add (GTK_CONTAINER (scrwin), tree_view);
	gtk_widget_show (tree_view);

	dialog->sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree_view));
	g_signal_connect (dialog->sel, "changed", G_CALLBACK (dialog_sel_changed_cb), dialog);

	col = gtk_tree_view_column_new ();
	gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view), col);
	cell = gtk_cell_renderer_pixbuf_new ();

	gtk_tree_view_column_pack_start (col, cell, FALSE);
	gtk_tree_view_column_add_attribute (col, cell, "stock-id", NETWORK_LIST_STOCK_PIXBUF_ITEM);

	cell = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_start (col, cell, TRUE);
	gtk_tree_view_column_add_attribute (col, cell, "text", NETWORK_LIST_NAME_ITEM);

	/* Info Pane */
	hbox = gtk_hbox_new (FALSE, 0);
	gtk_paned_pack2 (GTK_PANED (paned), hbox, TRUE, TRUE);
	gtk_widget_show (hbox);

	vbox = gtk_vbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (hbox), vbox, TRUE, TRUE, 6);
	gtk_widget_show (vbox);

	/* Info Title */
	hbox = gtk_hbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	dialog->image = gtk_image_new ();
	gtk_box_pack_start (GTK_BOX (hbox), dialog->image, FALSE, FALSE, 0);
	gtk_widget_show (dialog->image);

	dialog->name_label = gtk_label_new (NULL);
	gtk_label_set_use_markup (GTK_LABEL (dialog->name_label), TRUE);
	gtk_misc_set_alignment (GTK_MISC (dialog->name_label), 0.0, 0.5);
	gtk_container_add (GTK_CONTAINER (hbox), dialog->name_label);
	gtk_widget_show (dialog->name_label);

	line = gtk_hseparator_new ();
	gtk_box_pack_start (GTK_BOX (vbox), line, FALSE, FALSE, 0);
	gtk_widget_show (line);

	/* Info */
	dialog->info_label = gtk_label_new (NULL);
	gtk_label_set_line_wrap (GTK_LABEL (dialog->info_label), TRUE);
	gtk_misc_set_alignment (GTK_MISC (dialog->info_label), 0.0, 0.0);
	gtk_box_pack_start (GTK_BOX (vbox), dialog->info_label, FALSE, FALSE, 0);
	gtk_widget_show (dialog->info_label);

	dialog->url_box = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (vbox), dialog->url_box, FALSE, FALSE, 0);

	label = gtk_label_new (_("Website: "));
	gtk_box_pack_start (GTK_BOX (dialog->url_box), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	dialog->url_href = gnome_href_new ("", "");
	gtk_box_pack_start (GTK_BOX (dialog->url_box), dialog->url_href, FALSE, FALSE, 0);
	gtk_widget_show (dialog->url_href);

	/* Initial setup */
	gtk_tree_model_foreach (model, find_and_select_network, dialog->sel);


	gtk_window_present (GTK_WINDOW (dialog->win));
}


/* "Disconnect..." Dialog Functions */
static void
fill_disconnect_networks_list (GtkTreeView * tree_view,
							   GtkDialog * win) 
{
	static GtkListStore *model = NULL;
	GSList *ids = NULL;
	gchar *name,
	 *icon,
	 *stock_id;
	GtkTreeIter iter;
	gboolean any_to_select;

	if (model != NULL)
	{
		gtk_list_store_clear (model);
	}
	else
	{
		model = gtk_list_store_new (NETWORK_LIST_NUM_COLS,
									G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING,
									G_TYPE_STRING, G_TYPE_STRING);

		g_object_add_weak_pointer (G_OBJECT (model), (gpointer) & model);
	}

	ids = connect_get_open_connections ();
	any_to_select = (ids != NULL);

	if (ids == NULL)
	{
		gtk_dialog_set_response_sensitive (win, GTK_RESPONSE_OK, FALSE);
		gtk_widget_set_sensitive (GTK_WIDGET (tree_view), FALSE);
	}

	while (ids != NULL)
	{
		name = prefs_get_network_name (ids->data);
		icon = prefs_get_network_icon (ids->data);

		if (icon != NULL)
			stock_id = (gchar *) stock_get_id_from_file (icon);
		else
			stock_id = GNOMECHAT_STOCK_NETWORK;

		gtk_list_store_append (model, &iter);
		gtk_list_store_set (model, &iter,
							NETWORK_LIST_STOCK_PIXBUF_ITEM, stock_id,
							NETWORK_LIST_NAME_ITEM, name, NETWORK_LIST_KEY_ITEM, ids->data, -1);

		ids = g_slist_remove (ids, ids->data);
	}

	gtk_tree_view_set_model (tree_view, GTK_TREE_MODEL (model));

	if (any_to_select)
	{
		gtk_tree_model_get_iter_first (GTK_TREE_MODEL (model), &iter);
		gtk_tree_selection_select_iter (gtk_tree_view_get_selection (tree_view), &iter);
	}
}


static void
disconnect_sel_changed_cb (GtkTreeSelection * sel,
						   DisconnectDialog * dialog) 
{
	if (gtk_tree_selection_count_selected_rows (sel) > 0)
	{
		gtk_dialog_set_response_sensitive (GTK_DIALOG (dialog->win), GTK_RESPONSE_OK, TRUE);
	}
	else
	{
		gtk_dialog_set_response_sensitive (GTK_DIALOG (dialog->win), GTK_RESPONSE_OK, FALSE);
	}
}


static void
disconnect_response_cb (GtkWidget * win,
						gint response,
						DisconnectDialog ** dialog_ptr)
{
	DisconnectDialog *dialog = *dialog_ptr;
	GtkTreeModel *model;
	GList *selection;
	gchar *id;
	GtkTreeIter iter;

	switch (response)
	{
	case GTK_RESPONSE_HELP:
		g_message ("FIXME: Connect help system to \"Disconnect\" dialog.");
		return;
		break;

	case GTK_RESPONSE_OK:
		for (selection = gtk_tree_selection_get_selected_rows (dialog->sel, &model);
			 selection != NULL; selection = g_list_remove (selection, selection->data))
		{
			gtk_tree_model_get_iter (model, &iter, selection->data);
			gtk_tree_model_get (model, &iter, NETWORK_LIST_KEY_ITEM, &id, -1);

			connect_close_connection (id);

			gtk_tree_path_free (selection->data);
		}
	default:
		gtk_widget_destroy (win);
		g_free (*(dialog_ptr));
		*(dialog_ptr) = NULL;
		break;
	}
}


void
connect_ui_open_disconnect_dialog (GtkWindow * parent)
{
	static DisconnectDialog *dialog = NULL;
	GtkWidget *scrwin,
	 *tree_view;
	GtkTreeViewColumn *col;
	GtkCellRenderer *cell;

	if (dialog != NULL)
	{
		gtk_window_present (GTK_WINDOW (dialog->win));
		return;
	}

	dialog = g_new0 (DisconnectDialog, 1);
	g_assert (dialog != NULL);
	dialog->win = gtk_dialog_new_with_buttons (_("Disconnect - IRC Chat"), parent,
											   GTK_DIALOG_NO_SEPARATOR,
											   GTK_STOCK_HELP, GTK_RESPONSE_HELP,
											   GTK_STOCK_CLOSE, GTK_RESPONSE_CANCEL,
											   GNOMECHAT_STOCK_DISCONNECT, GTK_RESPONSE_OK, NULL);
	util_set_window_icon_from_stock (GTK_WINDOW (dialog->win), GNOMECHAT_STOCK_DISCONNECT);
	gtk_window_set_role (GTK_WINDOW (dialog->win), "Disconnect");
	gtk_dialog_set_default_response (GTK_DIALOG (dialog->win), GTK_RESPONSE_OK);
	g_signal_connect (dialog->win, "response", G_CALLBACK (disconnect_response_cb), &dialog);

	scrwin = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrwin), GTK_POLICY_AUTOMATIC,
									GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrwin), GTK_SHADOW_IN);
	gtk_container_set_border_width (GTK_CONTAINER (scrwin), 5);
	gtk_widget_set_size_request (scrwin, util_get_font_width (scrwin) * 50,
								 util_get_font_height (scrwin) * 5);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (dialog->win)->vbox), scrwin);
	gtk_widget_show (scrwin);

	tree_view = gtk_tree_view_new ();
	gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (tree_view), FALSE);
	gtk_container_add (GTK_CONTAINER (scrwin), tree_view);
	gtk_widget_show (tree_view);

	dialog->sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree_view));
	gtk_tree_selection_set_mode (GTK_TREE_SELECTION (dialog->sel), GTK_SELECTION_MULTIPLE);
	g_signal_connect (dialog->sel, "changed", G_CALLBACK (disconnect_sel_changed_cb), dialog);

	col = gtk_tree_view_column_new ();
	gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view), col);
	cell = gtk_cell_renderer_pixbuf_new ();

	gtk_tree_view_column_pack_start (col, cell, FALSE);
	gtk_tree_view_column_add_attribute (col, cell, "stock-id", NETWORK_LIST_STOCK_PIXBUF_ITEM);

	cell = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_start (col, cell, TRUE);
	gtk_tree_view_column_add_attribute (col, cell, "text", NETWORK_LIST_NAME_ITEM);

	fill_disconnect_networks_list (GTK_TREE_VIEW (tree_view), GTK_DIALOG (dialog->win));

	gtk_window_present (GTK_WINDOW (dialog->win));
}
