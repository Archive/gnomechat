/*
 *  GnomeChat: src/network-ui.h
 *
 *  Copyright (c) 2003 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.01234567890
 */


#ifndef __NETWORK_UI_H__
#define __NETWORK_UI_H__

#include "gnomechat-rdf-network.h"

#include <gtk/gtkwindow.h>
#include <gtk/gtktreemodel.h>

void network_ui_open_network_edit_dialog (GtkWindow * parent,
										  GtkTreeModel * rdf_model,
										  GnomechatRdfNetwork * network);

void network_ui_open_connect_dialog (GtkWindow * parent);


#endif /* __NETWORK_UI_H__ */
