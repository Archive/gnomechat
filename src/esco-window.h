/*
 *  GnomeChat: src/esco-window.h
 *
 *  Copyright (c) 2002 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef __ESCO_WINDOW_H__
#define __ESCO_WINDOW_H__

#include "esco-enums.h"

#include <gtk/gtktreestore.h>
#include <gtk/gtktreeselection.h>
#include <libgnomeui/gnome-app.h>


#define ESCO_TYPE_WINDOW			(esco_window_get_type ())
#define ESCO_WINDOW(obj)			(GTK_CHECK_CAST ((obj), ESCO_TYPE_WINDOW, EscoWindow))
#define ESCO_WINDOW_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), ESCO_TYPE_WINDOW, EscoWindowClass))
#define ESCO_IS_WINDOW(obj)			(GTK_CHECK_TYPE ((obj), ESCO_TYPE_WINDOW))
#define ESCO_IS_WINDOW_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass), ESCO_TYPE_WINDOW))
#define ESCO_WINDOW_GET_CLASS(obj)	(GTK_CHECK_GET_CLASS ((obj), ESCO_TYPE_WINDOW, EscoWindowClass))


typedef struct _EscoWindow EscoWindow;
typedef struct _EscoWindowClass EscoWindowClass;
typedef struct _EscoWindowMenus EscoWindowMenus;

typedef void (*EscoWindowLayoutChangedFunction) (EscoWindow * win,
												 EscoWindowLayoutType layout);


struct _EscoWindow
{
	GnomeApp parent;

	/* Internal stuff */
	EscoWindowLayoutType layout;
	GList *children;
	GtkWidget *current;
	gchar *stock_icon;

	/* Widgets & Friends */
	GtkWidget *paned;

	GtkWidget *chats_scrwin;
	GtkTreeStore *chats_store;
	GtkTreeSelection *chats_sel;
	GtkWidget *chats_view;

	GtkWidget *notebook;

	/* Dynamic menu stuff */
	EscoWindowMenus *menus;

	gulong encoding_signal;

	gboolean setting_layout:1;
	gboolean closing:1;
};

struct _EscoWindowClass
{
	GnomeAppClass parent_class;

	/* Signals */
	EscoWindowLayoutChangedFunction layout_changed;
};

struct _EscoWindowMenus
{
	GtkWidget *disconnect;

	GtkWidget *cut;
	GtkWidget *copy;
	GtkWidget *paste;

	GtkWidget *layouts[ESCO_WINDOW_LAYOUT_WINDOW];
	GtkWidget *server_props;
	GtkWidget *channel_props;
	GtkWidget *user_props;

	GtkWidget *encoding_menu;
};


GType esco_window_get_type (void);

GtkWidget *esco_window_new (void);
void esco_window_set_layout (EscoWindow * win,
							 EscoWindowLayoutType layout);
GtkWidget *esco_window_add_new_chat (EscoWindow * win);
void esco_window_remove_current_chat (EscoWindow * win);
void esco_window_switch_to_chat (EscoWindow * win,
								 GtkWidget * box);


#endif /* __ESCO_WINDOW_H__ */
