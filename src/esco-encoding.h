/*
 *  GnomeChat: src/esco-encoding.h
 *
 *  Copyright (c) 2002 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __ESCO_ENCODING_H__
#define __ESCO_ENCODING_H__


#include <glib.h>


/* Types */
typedef enum /* <prefix=ESCO_ENCODING_GROUP> */
{
	ESCO_ENCODING_GROUP_UNICODE,
	ESCO_ENCODING_GROUP_EAST_ASIAN,
	ESCO_ENCODING_GROUP_WEST_ASIAN,
	ESCO_ENCODING_GROUP_EAST_EUROPEAN,
	ESCO_ENCODING_GROUP_WEST_EUROPEAN,
	ESCO_ENCODING_GROUP_MIDDLE_EASTERN,
	ESCO_ENCODING_GROUP_INVALID,
	ESCO_ENCODING_GROUP_LAST = ESCO_ENCODING_GROUP_INVALID
}
EscoEncodingGroupType;


#define ESCO_ENCODING(box)			((EscoEncoding *)(box))
#define ESCO_ENCODING_GROUP(box)	((EscoEncodingGroup *)(box))


typedef struct
{
	const EscoEncodingGroupType group;

	/* Name (encoding) */
	gchar *list_name;
	/* Name: encoding */
	gchar *ui_name;

	/* aliases[0] == GNU canonical name */
	const gchar *const *const aliases;
	const gboolean recommended:1;
}
EscoEncoding;

typedef struct
{
	const EscoEncodingGroupType group;

	gchar *name;

	const EscoEncoding *encodings;
}
EscoEncodingGroup;


/* API */
/* List type = (const EscoEncodingGroup *) */
GSList *esco_encoding_get_all_groups (void);

G_CONST_RETURN EscoEncodingGroup *esco_encoding_get_group (EscoEncodingGroupType group);

G_CONST_RETURN EscoEncodingGroup *esco_encoding_get_group_by_alias (const gchar *
																	encoding);

/* List type = (const EscoEncoding *) */
GSList *esco_encoding_get_recommended_encodings (void);


G_CONST_RETURN EscoEncoding *esco_encoding_get_encoding_by_alias (const gchar * encoding);

/* Checks to see if the encoding is valid */
gboolean esco_encoding_is_valid (const gchar * encoding);

guint esco_encoding_alias_hash (gconstpointer encoding);
gboolean esco_encoding_alias_equal (gconstpointer encoding1,
									gconstpointer encoding2);

#endif /* __ESCO_ENCODING_H__ */
