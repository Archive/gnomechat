/*
 *  GnomeChat: src/gnomechat.h
 *
 *  Copyright (c) 2002 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GNOMECHAT_H__
#define __GNOMECHAT_H__


#ifdef HAVE_CONFIG_H
#	include <config.h>
#endif /* HAVE_CONFIG_H */


#include <glib.h>
#include <libgtcpsocket/gtcp-i18n.h>


#define GENERIC_NAME _(genericname)


extern const gchar *const appname;
extern const gchar *const wmclass;
extern const gchar *const genericname;


gchar *gnomechat_get_pixmap_path (const gchar *filename);
G_CONST_RETURN gchar *gnomechat_get_private_dir (void);
G_CONST_RETURN gchar *gnomechat_get_cache_dir (void);
void gnomechat_quit (void);


#endif /* __GNOMECHAT_H__ */
