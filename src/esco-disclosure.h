/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Author: Gregory Merchan <merchan@phys.lsu.edu>
 *
 *  Copyright 2003 Gregory Merchan
 *
 *  Original Authors: Iain Holmes <iain@ximian.com>
 *
 *  Original Copyright 2002 Iain Holmes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifndef __ESCO_DISCLOSURE_H__
#define __ESCO_DISCLOSURE_H__


#include <gtk/gtkcheckbutton.h>


G_BEGIN_DECLS

#define ESCO_TYPE_DISCLOSURE            (esco_disclosure_get_type ())
#define ESCO_DISCLOSURE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), ESCO_TYPE_DISCLOSURE, EscoDisclosure))
#define ESCO_DISCLOSURE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), ESCO_TYPE_DISCLOSURE, EscoDisclosureClass))
#define ESCO_IS_DISCLOSURE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), ESCO_TYPE_DISCLOSURE))
#define ESCO_IS_DISCLOSURE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), ESCO_TYPE_DISCLOSURE))
#define ESCO_DISCLOSURE_ET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS ((obj), ESCO_TYPE_DISCLOSURE, EscoDisclosureClass))


typedef struct _EscoDisclosure EscoDisclosure;
typedef struct _EscoDisclosureClass EscoDisclosureClass;
typedef struct _EscoDisclosurePrivate EscoDisclosurePrivate;


struct _EscoDisclosure
{
	/*< private > */
	GtkCheckButton parent;

	EscoDisclosurePrivate *_priv;
};

struct _EscoDisclosureClass
{
	GtkCheckButtonClass parent_class;
};


GType esco_disclosure_get_type (void);

GtkWidget *esco_disclosure_new (void);
GtkWidget *esco_disclosure_new_with_label (const gchar * label);
GtkWidget *esco_disclosure_new_with_mnemonic (const gchar * label);

G_END_DECLS

#endif /* __ESCO_DISCLOSURE_H__ */
