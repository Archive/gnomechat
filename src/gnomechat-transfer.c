/*
 *  GnomeChat: src/gnomechat-download.c
 *
 *  Copyright (c) 2003 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.01234567890
 */

#ifdef HAVE_CONFIG_H
#	include <config.h>
#endif /* HAVE_CONFIG_H */

#include "gnomechat-transfer.h"
#include "gnomechat-type-builtins.h"

enum
{
	STATUS_CHANGED,
	PROGRESS,
	CANCELLED,
	LAST_SIGNAL
};


static gint signals[LAST_SIGNAL] = { 0 };


static void
gnomechat_transfer_base_init (gpointer data)
{
	static gboolean initialized = FALSE;

	if (!initialized)
	{
		signals[STATUS_CHANGED] =
			g_signal_new ("status-changed",
						  GNOMECHAT_TYPE_TRANSFER,
						  G_SIGNAL_RUN_LAST,
						  G_STRUCT_OFFSET (GnomechatTransferIface, status_changed),
						  NULL, NULL, g_cclosure_marshal_VOID__ENUM,
						  G_TYPE_NONE, 1, GNOMECHAT_TYPE_TRANSFER_STATUS);
		signals[PROGRESS] =
			g_signal_new ("progress",
						  GNOMECHAT_TYPE_TRANSFER,
						  G_SIGNAL_RUN_LAST,
						  G_STRUCT_OFFSET (GnomechatTransferIface, progress),
						  NULL, NULL, g_cclosure_marshal_VOID__ULONG,
						  G_TYPE_NONE, 1, G_TYPE_ULONG);
		signals[CANCELLED] =
			g_signal_new ("cancelled",
						  GNOMECHAT_TYPE_TRANSFER,
						  G_SIGNAL_RUN_LAST,
						  G_STRUCT_OFFSET (GnomechatTransferIface, cancelled),
						  NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);

		initialized = TRUE;
	}
}


GType
gnomechat_transfer_get_type (void)
{
	static GType type = 0;

	if (type == 0)
	{
		static const GTypeInfo info = {
			sizeof (GnomechatTransferIface),	/* class_size */
			gnomechat_transfer_base_init,	/* base_init */
			NULL,				/* base_finalize */
			NULL,
			NULL,				/* class_finalize */
			NULL,				/* class_data */
			0,
			0,					/* n_preallocs */
			NULL
		};

		type = g_type_register_static (G_TYPE_INTERFACE, "GnomechatTransfer", &info, 0);

		g_type_interface_add_prerequisite (type, G_TYPE_OBJECT);
	}

	return type;
}


GnomechatTransferType
gnomechat_transfer_get_transfer_type (GnomechatTransfer * transfer)
{
	GnomechatTransferIface *iface;

	g_return_val_if_fail (GNOMECHAT_IS_TRANSFER (transfer), GNOMECHAT_TRANSFER_INVALID);

	iface = GNOMECHAT_TRANSFER_GET_IFACE (transfer);

	g_return_val_if_fail (iface->get_transfer_type != NULL, GNOMECHAT_TRANSFER_INVALID);

	return (*iface->get_transfer_type) (transfer);
}


GnomeVFSResult
gnomechat_transfer_get_error (GnomechatTransfer * transfer)
{
	GnomechatTransferIface *iface;

	g_return_val_if_fail (GNOMECHAT_IS_TRANSFER (transfer), GNOME_VFS_ERROR_INTERNAL);

	iface = GNOMECHAT_TRANSFER_GET_IFACE (transfer);

	g_return_val_if_fail (iface->get_error != NULL, GNOME_VFS_ERROR_INTERNAL);

	return (*iface->get_error) (transfer);
}


GnomechatTransferStatus
gnomechat_transfer_get_status (GnomechatTransfer * transfer)
{
	GnomechatTransferIface *iface;

	g_return_val_if_fail (GNOMECHAT_IS_TRANSFER (transfer),
						  GNOMECHAT_TRANSFER_STATUS_INVALID);

	iface = GNOMECHAT_TRANSFER_GET_IFACE (transfer);

	g_return_val_if_fail (iface->get_status != NULL, GNOMECHAT_TRANSFER_STATUS_INVALID);

	return (*iface->get_status) (transfer);
}


GnomeVFSURI *
gnomechat_transfer_get_from_uri (GnomechatTransfer * transfer)
{
	GnomechatTransferIface *iface;

	g_return_val_if_fail (GNOMECHAT_IS_TRANSFER (transfer), NULL);

	iface = GNOMECHAT_TRANSFER_GET_IFACE (transfer);

	g_return_val_if_fail (iface->get_from_uri != NULL, NULL);

	return (*iface->get_from_uri) (transfer);
}


void
gnomechat_transfer_set_from_uri (GnomechatTransfer * transfer,
								 GnomeVFSURI * uri)
{
	GnomechatTransferIface *iface;

	g_return_if_fail (GNOMECHAT_IS_TRANSFER (transfer));

	iface = GNOMECHAT_TRANSFER_GET_IFACE (transfer);

	g_return_if_fail (iface->set_from_uri != NULL);
	g_return_if_fail (uri != NULL);

	(*iface->set_from_uri) (transfer, uri);
}


GnomeVFSURI *
gnomechat_transfer_get_to_uri (GnomechatTransfer * transfer)
{
	GnomechatTransferIface *iface;

	g_return_val_if_fail (GNOMECHAT_IS_TRANSFER (transfer), NULL);

	iface = GNOMECHAT_TRANSFER_GET_IFACE (transfer);

	g_return_val_if_fail (iface->get_to_uri != NULL, NULL);

	return (*iface->get_to_uri) (transfer);
}


void
gnomechat_transfer_set_to_uri (GnomechatTransfer * transfer,
							   GnomeVFSURI * uri)
{
	GnomechatTransferIface *iface;

	g_return_if_fail (GNOMECHAT_IS_TRANSFER (transfer));

	iface = GNOMECHAT_TRANSFER_GET_IFACE (transfer);

	g_return_if_fail (iface->set_to_uri != NULL);
	g_return_if_fail (uri != NULL);

	(*iface->set_to_uri) (transfer, uri);
}


GnomeVFSFileSize
gnomechat_transfer_get_file_size (GnomechatTransfer * transfer)
{
	GnomechatTransferIface *iface;

	g_return_val_if_fail (GNOMECHAT_IS_TRANSFER (transfer), 0);

	iface = GNOMECHAT_TRANSFER_GET_IFACE (transfer);

	g_return_val_if_fail (iface->get_file_size != NULL, 0);

	return (*iface->get_file_size) (transfer);
}


GnomeVFSFileSize
gnomechat_transfer_get_bytes_copied (GnomechatTransfer * transfer)
{
	GnomechatTransferIface *iface;

	g_return_val_if_fail (GNOMECHAT_IS_TRANSFER (transfer), 0);

	iface = GNOMECHAT_TRANSFER_GET_IFACE (transfer);

	g_return_val_if_fail (iface->get_bytes_copied != NULL, 0);

	return (*iface->get_bytes_copied) (transfer);
}


gulong
gnomechat_transfer_get_n_files (GnomechatTransfer * transfer)
{
	GnomechatTransferIface *iface;

	g_return_val_if_fail (GNOMECHAT_IS_TRANSFER (transfer), 0);

	iface = GNOMECHAT_TRANSFER_GET_IFACE (transfer);

	g_return_val_if_fail (iface->get_n_files != NULL, 0);

	return (*iface->get_n_files) (transfer);
}


gulong
gnomechat_transfer_get_file_index (GnomechatTransfer * transfer)
{
	GnomechatTransferIface *iface;

	g_return_val_if_fail (GNOMECHAT_IS_TRANSFER (transfer), 0);

	iface = GNOMECHAT_TRANSFER_GET_IFACE (transfer);

	g_return_val_if_fail (iface->get_file_index != NULL, 0);

	return (*iface->get_file_index) (transfer);
}


void
gnomechat_transfer_start_transfer (GnomechatTransfer * transfer)
{
	GnomechatTransferIface *iface;

	g_return_if_fail (GNOMECHAT_IS_TRANSFER (transfer));

	iface = GNOMECHAT_TRANSFER_GET_IFACE (transfer);

	g_return_if_fail (iface->start_transfer != NULL);

	(*iface->start_transfer) (transfer);
}


void
gnomechat_transfer_cancel_transfer (GnomechatTransfer * transfer)
{
	GnomechatTransferIface *iface;

	g_return_if_fail (GNOMECHAT_IS_TRANSFER (transfer));

	iface = GNOMECHAT_TRANSFER_GET_IFACE (transfer);

	g_return_if_fail (iface->cancel_transfer != NULL);

	(*iface->cancel_transfer) (transfer);
}


void
gnomechat_transfer_status_changed (GnomechatTransfer * transfer,
								   GnomechatTransferStatus status)
{
	static GQuark *quarks = NULL;

	g_return_if_fail (GNOMECHAT_IS_TRANSFER (transfer));
	g_return_if_fail (status >= 0 && status < GNOMECHAT_TRANSFER_STATUS_LAST);

	if (quarks == NULL)
	{
		static const gchar * const strings[GNOMECHAT_TRANSFER_STATUS_LAST] = {
			"ready",
			"checking",
			"transferring",
			"done",
			"error",
			"invalid"
		};
		guint i;
		
		quarks = g_new0 (GQuark, GNOMECHAT_TRANSFER_STATUS_LAST);

		for (i = 0; i < GNOMECHAT_TRANSFER_STATUS_LAST; i++)
			quarks[i] = g_quark_from_static_string (strings[i]);
	}

	g_signal_emit (transfer, signals[STATUS_CHANGED], quarks[status], status);
}


void
gnomechat_transfer_progress (GnomechatTransfer * transfer,
							 GnomeVFSFileSize bytes_completed)
{
	g_return_if_fail (GNOMECHAT_IS_TRANSFER (transfer));

	g_signal_emit (transfer, signals[PROGRESS], 0, bytes_completed);
}


void
gnomechat_transfer_cancelled (GnomechatTransfer * transfer)
{
	g_return_if_fail (GNOMECHAT_IS_TRANSFER (transfer));

	g_signal_emit (transfer, signals[CANCELLED], 0);
}
