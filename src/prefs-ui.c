/*
 *  GnomeChat: src/prefs-ui.c
 *
 *  Copyright (c) 2002 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "gnomechat.h"
#include "prefs-ui.h"

#include "esco-disclosure.h"
#include "esco-viewer.h"
#include "stock-items.h"
#include "utils.h"

#include <gtk/gtkalignment.h>
#include <gtk/gtkcellrendererpixbuf.h>
#include <gtk/gtkcellrenderertext.h>
#include <gtk/gtkdialog.h>
#include <gtk/gtkentry.h>
#include <gtk/gtkhbox.h>
#include <gtk/gtkimage.h>
#include <gtk/gtklabel.h>
#include <gtk/gtkliststore.h>
#include <gtk/gtkmenuitem.h>
#include <gtk/gtknotebook.h>
#include <gtk/gtkoptionmenu.h>
#include <gtk/gtkradiobutton.h>
#include <gtk/gtkscrolledwindow.h>
#include <gtk/gtksizegroup.h>
#include <gtk/gtkspinbutton.h>
#include <gtk/gtkstock.h>
#include <gtk/gtktable.h>
#include <gtk/gtktreeview.h>
#include <gtk/gtkvbox.h>
#include <gtk/gtkviewport.h>

#include <libgnomeui/gnome-color-picker.h>
#include <libgnomeui/gnome-file-entry.h>
#include <libgnomeui/gnome-font-picker.h>
#include <libgnomeui/gnome-pixmap-entry.h>

/* For mkdir() */
#include <sys/stat.h>
#include <sys/types.h>


#define MORE_OPTIONS_OPTMENU_STYLE_KEY	(get_viewer_style_quark ())
#define PREFS_LAYOUT_KEY				(get_layout_style_quark ())
#define PREFS_TAB_SIDE_KEY				(get_tab_side_quark ())


enum
{
	MORE_OPTIONS_TEXT_COL,
	MORE_OPTIONS_STYLE_COL,
	MORE_OPTIONS_NUM_COLUMNS
};


typedef struct _PrefsDialog PrefsDialog;
typedef struct _MoreOptionsDialog MoreOptionsDialog;

struct _MoreOptionsDialog
{
	GtkWidget *win;

	GtkWidget *optmenu;
	GtkWidget *fg_picker;
	GtkWidget *bg_picker;
	GtkWidget *bold_btn;
	GtkWidget *italic_btn;
	GtkWidget *uline_btn;

	GtkWidget *preview;

	ViewerStyleType style;
};

struct _PrefsDialog
{
	GtkWidget *win;

	/* Identity Page */
	/* IRC Identity Frame */
	GtkWidget *nick_entry;
	GtkWidget *username_entry;
	GtkWidget *realname_entry;
	/* Photo Frame */
	GtkWidget *photo_image;
	GtkWidget *photo_entry;

	/* Fonts & Colors Page */
	/* Chatting Frame */
	GtkWidget *fonts_colors_preview;

	/* Windows Page */
	GtkWidget *more_opts_box;
	GtkWidget *width_height_table;
	GtkWidget *width_spin;
	GtkWidget *height_spin;
	GtkWidget *left_top_table;
	GtkWidget *top_spin;
	GtkWidget *left_spin;

	/* DCC */
	GtkWidget *recv_location_entry;
	GSList *recv_naming_group;

	MoreOptionsDialog *more_opts;
};


static GQuark
get_viewer_style_quark (void)
{
	static GQuark quark = 0;

	if (quark == 0)
	{
		quark = g_quark_from_static_string ("gnomechat-viewer-style");
	}

	return quark;
}


static GQuark
get_layout_style_quark (void)
{
	static GQuark quark = 0;

	if (quark == 0)
	{
		quark = g_quark_from_static_string ("gnomechat-layout-style");
	}

	return quark;
}


static GQuark
get_tab_side_quark (void)
{
	static GQuark quark = 0;

	if (quark == 0)
	{
		quark = g_quark_from_static_string ("gnomechat-tab-side");
	}

	return quark;
}


static void
append_chat_preview_to_viewer (EscoViewer * view)
{
	GtkTextBuffer *buffer;
	GtkTextMark *mark;
	GtkTextIter iter;
	gchar *msg = NULL,
	 *nick = NULL;

	gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW (view), GTK_WRAP_NONE);
	nick = prefs_get_nick ();

	esco_viewer_append_line (view, ESCO_VIEWER_MESSAGE_MY_MSG, 0,
							 _("Website: http://ignore-your.tv/"), NULL, NULL, FALSE);
	esco_viewer_append_line (view, ESCO_VIEWER_MESSAGE_MSG, 0,
							 _("SomeUser"), _("This is a regular message."), NULL, FALSE);

	msg = g_strdup_printf (_("This is a message to %s"), nick);
	esco_viewer_append_line (view, ESCO_VIEWER_MESSAGE_MSG, 0, _("SomeUser"), msg, NULL, FALSE);
	g_free (msg);

	esco_viewer_append_line (view, ESCO_VIEWER_MESSAGE_MY_ACTION, 0,
							 _("is doing something."), NULL, NULL, FALSE);

	esco_viewer_append_line (view, ESCO_VIEWER_MESSAGE_ACTION, 0,
							 _("SomeUser"), _("is doing something else."), NULL, FALSE);

	msg = g_strdup_printf (_("is directing an action toward %s"), nick);
	esco_viewer_append_line (view, ESCO_VIEWER_MESSAGE_ACTION, 0, _("SomeUser"), msg, NULL, FALSE);
	g_free (msg);

	esco_viewer_append_line (view, ESCO_VIEWER_MESSAGE_CHANNEL_CHANGED,
							 GIRC_CHANNEL_INFO_TOPIC,
							 _("SomeUser"), _("The new topic"), NULL, FALSE);

	esco_viewer_append_line (view, ESCO_VIEWER_MESSAGE_CHANNEL_USER_CHANGED,
							 GIRC_CHANNEL_USER_CHANGED_KICKED,
							 _("SomeUser"), _("SomeOtherUser"), _("Just Because."), FALSE);

	esco_viewer_append_line (view, ESCO_VIEWER_MESSAGE_SERVER, 0,
							 "irc.someserver.net", _("A sample server message."), NULL, FALSE);

	esco_viewer_append_line (view, ESCO_VIEWER_MESSAGE_ERROR, 0,
							 _("A sample error message."), NULL, NULL, FALSE);

	g_free (nick);

	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (view));
	gtk_text_buffer_get_start_iter (buffer, &iter);
	mark = gtk_text_buffer_create_mark (buffer, NULL, &iter, TRUE);
	gtk_text_view_scroll_to_mark (GTK_TEXT_VIEW (view), mark, 0.0, TRUE, 0.0, 1.0);
	gtk_text_buffer_delete_mark (buffer, mark);
}


static void
fill_styles_list (GtkListStore * model)
{
	GtkTreeIter iter;

	gtk_list_store_append (model, &iter);
	gtk_list_store_set (model, &iter,
						MORE_OPTIONS_TEXT_COL, _("Messages from me"),
						MORE_OPTIONS_STYLE_COL, VIEWER_STYLE_MY_NICK, -1);
	gtk_list_store_append (model, &iter);
	gtk_list_store_set (model, &iter,
						MORE_OPTIONS_TEXT_COL, _("Messages to me"),
						MORE_OPTIONS_STYLE_COL, VIEWER_STYLE_TO_ME_NICK, -1);
	gtk_list_store_append (model, &iter);
	gtk_list_store_set (model, &iter,
						MORE_OPTIONS_TEXT_COL, _("Other messages"),
						MORE_OPTIONS_STYLE_COL, VIEWER_STYLE_NICK, -1);
	gtk_list_store_append (model, &iter);
	gtk_list_store_set (model, &iter,
						MORE_OPTIONS_TEXT_COL, _("Server messages"),
						MORE_OPTIONS_STYLE_COL, VIEWER_STYLE_SERVER, -1);
	gtk_list_store_append (model, &iter);
	gtk_list_store_set (model, &iter,
						MORE_OPTIONS_TEXT_COL, _("Channel messages"),
						MORE_OPTIONS_STYLE_COL, VIEWER_STYLE_CHANNEL, -1);
	gtk_list_store_append (model, &iter);
	gtk_list_store_set (model, &iter,
						MORE_OPTIONS_TEXT_COL, _("Links"),
						MORE_OPTIONS_STYLE_COL, VIEWER_STYLE_LINK, -1);
	gtk_list_store_append (model, &iter);
	gtk_list_store_set (model, &iter,
						MORE_OPTIONS_TEXT_COL, _("Error messages"),
						MORE_OPTIONS_STYLE_COL, VIEWER_STYLE_ERROR, -1);
}


static void
fill_more_options_optmenu (MoreOptionsDialog * more_opts)
{
	GtkWidget *menu,
	 *menuitem;

	switch (more_opts->style)
	{
	case VIEWER_STYLE_MY_ACTIONS:
	case VIEWER_STYLE_MY_NICK:
		menu = gtk_option_menu_get_menu (GTK_OPTION_MENU (more_opts->optmenu));
		if (menu != NULL)
			gtk_widget_destroy (menu);

		menu = gtk_menu_new ();

		menuitem = gtk_menu_item_new_with_mnemonic (_("My _Nickname"));
		g_object_set_qdata (G_OBJECT (menuitem), MORE_OPTIONS_OPTMENU_STYLE_KEY,
							GUINT_TO_POINTER (VIEWER_STYLE_MY_NICK));
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
		gtk_widget_show (menuitem);

		menuitem = gtk_menu_item_new_with_mnemonic (_("My _Actions"));
		g_object_set_qdata (G_OBJECT (menuitem), MORE_OPTIONS_OPTMENU_STYLE_KEY,
							GUINT_TO_POINTER (VIEWER_STYLE_MY_ACTIONS));
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
		gtk_widget_show (menuitem);
		gtk_widget_show (menu);

		gtk_option_menu_set_menu (GTK_OPTION_MENU (more_opts->optmenu), menu);
		break;

	case VIEWER_STYLE_TO_ME:
	case VIEWER_STYLE_TO_ME_ACTIONS:
	case VIEWER_STYLE_TO_ME_NICK:
		menu = gtk_option_menu_get_menu (GTK_OPTION_MENU (more_opts->optmenu));
		if (menu != NULL)
			gtk_widget_destroy (menu);

		menu = gtk_menu_new ();

		menuitem = gtk_menu_item_new_with_mnemonic (_("_Nicknames"));
		g_object_set_qdata (G_OBJECT (menuitem), MORE_OPTIONS_OPTMENU_STYLE_KEY,
							GUINT_TO_POINTER (VIEWER_STYLE_TO_ME_NICK));
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
		gtk_widget_show (menuitem);

		menuitem = gtk_menu_item_new_with_mnemonic (_("_Text"));
		g_object_set_qdata (G_OBJECT (menuitem), MORE_OPTIONS_OPTMENU_STYLE_KEY,
							GUINT_TO_POINTER (VIEWER_STYLE_TO_ME));
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
		gtk_widget_show (menuitem);

		menuitem = gtk_menu_item_new_with_mnemonic (_("_Actions"));
		g_object_set_qdata (G_OBJECT (menuitem), MORE_OPTIONS_OPTMENU_STYLE_KEY,
							GUINT_TO_POINTER (VIEWER_STYLE_MY_ACTIONS));
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
		gtk_widget_show (menuitem);
		gtk_widget_show (menu);

		gtk_option_menu_set_menu (GTK_OPTION_MENU (more_opts->optmenu), menu);
		break;

	case VIEWER_STYLE_ACTIONS:
	case VIEWER_STYLE_NICK:
		menu = gtk_option_menu_get_menu (GTK_OPTION_MENU (more_opts->optmenu));
		if (menu != NULL)
			gtk_widget_destroy (menu);

		menu = gtk_menu_new ();

		menuitem = gtk_menu_item_new_with_mnemonic (_("_Nicknames"));
		g_object_set_qdata (G_OBJECT (menuitem), MORE_OPTIONS_OPTMENU_STYLE_KEY,
							GUINT_TO_POINTER (VIEWER_STYLE_NICK));
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
		gtk_widget_show (menuitem);

		menuitem = gtk_menu_item_new_with_mnemonic (_("_Actions"));
		g_object_set_qdata (G_OBJECT (menuitem), MORE_OPTIONS_OPTMENU_STYLE_KEY,
							GUINT_TO_POINTER (VIEWER_STYLE_ACTIONS));
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
		gtk_widget_show (menuitem);
		gtk_widget_show (menu);

		gtk_option_menu_set_menu (GTK_OPTION_MENU (more_opts->optmenu), menu);
		break;

	case VIEWER_STYLE_SERVER:
	case VIEWER_STYLE_SERVER_NAME:
		menu = gtk_option_menu_get_menu (GTK_OPTION_MENU (more_opts->optmenu));
		if (menu != NULL)
			gtk_widget_destroy (menu);

		menu = gtk_menu_new ();

		menuitem = gtk_menu_item_new_with_mnemonic (_("_Text"));
		g_object_set_qdata (G_OBJECT (menuitem), MORE_OPTIONS_OPTMENU_STYLE_KEY,
							GUINT_TO_POINTER (VIEWER_STYLE_SERVER));
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
		gtk_widget_show (menuitem);

		menuitem = gtk_menu_item_new_with_mnemonic (_("Server _Name"));
		g_object_set_qdata (G_OBJECT (menuitem), MORE_OPTIONS_OPTMENU_STYLE_KEY,
							GUINT_TO_POINTER (VIEWER_STYLE_SERVER_NAME));
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
		gtk_widget_show (menuitem);
		gtk_widget_show (menu);

		gtk_option_menu_set_menu (GTK_OPTION_MENU (more_opts->optmenu), menu);
		break;

	case VIEWER_STYLE_CHANNEL:
	case VIEWER_STYLE_CHANNEL_NICK:
	case VIEWER_STYLE_CHANNEL_NAME:
	case VIEWER_STYLE_CHANNEL_VALUE:
		menu = gtk_option_menu_get_menu (GTK_OPTION_MENU (more_opts->optmenu));
		if (menu != NULL)
			gtk_widget_destroy (menu);

		menu = gtk_menu_new ();

		menuitem = gtk_menu_item_new_with_mnemonic (_("_Text"));
		g_object_set_qdata (G_OBJECT (menuitem), MORE_OPTIONS_OPTMENU_STYLE_KEY,
							GUINT_TO_POINTER (VIEWER_STYLE_CHANNEL));
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
		gtk_widget_show (menuitem);

		menuitem = gtk_menu_item_new_with_mnemonic (_("_Nickname"));
		g_object_set_qdata (G_OBJECT (menuitem), MORE_OPTIONS_OPTMENU_STYLE_KEY,
							GUINT_TO_POINTER (VIEWER_STYLE_CHANNEL_NICK));
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
		gtk_widget_show (menuitem);

		menuitem = gtk_menu_item_new_with_mnemonic (_("_Channel _Name"));
		g_object_set_qdata (G_OBJECT (menuitem), MORE_OPTIONS_OPTMENU_STYLE_KEY,
							GUINT_TO_POINTER (VIEWER_STYLE_CHANNEL_NAME));
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);

		menuitem = gtk_menu_item_new_with_mnemonic (_("New _Setting"));
		g_object_set_qdata (G_OBJECT (menuitem), MORE_OPTIONS_OPTMENU_STYLE_KEY,
							GUINT_TO_POINTER (VIEWER_STYLE_CHANNEL_VALUE));
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
		gtk_widget_show (menuitem);
		gtk_widget_show (menu);

		gtk_option_menu_set_menu (GTK_OPTION_MENU (more_opts->optmenu), menu);
		break;

	case VIEWER_STYLE_LINK:
		menu = gtk_option_menu_get_menu (GTK_OPTION_MENU (more_opts->optmenu));
		if (menu != NULL)
			gtk_widget_destroy (menu);

		menu = gtk_menu_new ();

		menuitem = gtk_menu_item_new_with_mnemonic (_("Links"));
		g_object_set_qdata (G_OBJECT (menuitem), MORE_OPTIONS_OPTMENU_STYLE_KEY,
							GUINT_TO_POINTER (VIEWER_STYLE_LINK));
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
		gtk_widget_show (menuitem);
		gtk_widget_show (menu);

		gtk_option_menu_set_menu (GTK_OPTION_MENU (more_opts->optmenu), menu);
		break;

	case VIEWER_STYLE_ERROR:
		menu = gtk_option_menu_get_menu (GTK_OPTION_MENU (more_opts->optmenu));
		if (menu != NULL)
			gtk_widget_destroy (menu);

		menu = gtk_menu_new ();

		menuitem = gtk_menu_item_new_with_mnemonic (_("Errors"));
		g_object_set_qdata (G_OBJECT (menuitem), MORE_OPTIONS_OPTMENU_STYLE_KEY,
							GUINT_TO_POINTER (VIEWER_STYLE_ERROR));
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
		gtk_widget_show (menuitem);
		gtk_widget_show (menu);

		gtk_option_menu_set_menu (GTK_OPTION_MENU (more_opts->optmenu), menu);
		break;

	default:
		g_assert_not_reached ();
		break;
	}
}


static void
dialog_response_cb (GtkDialog * win,
					gint response,
					gpointer data)
{
	PrefsDialog **ptr = (PrefsDialog **) data;
	PrefsDialog *dialog = *(ptr);

	switch (response)
	{
	case GTK_RESPONSE_HELP:
		break;

	case GTK_RESPONSE_DELETE_EVENT:
	case GTK_RESPONSE_CANCEL:
		prefs_dialog_cancelled ();
		gtk_widget_destroy (dialog->win);
		g_free (dialog);
		*(ptr) = NULL;
		break;

	case GTK_RESPONSE_OK:
		prefs_set_identity (gtk_entry_get_text (GTK_ENTRY (dialog->nick_entry)),
							gtk_entry_get_text (GTK_ENTRY (dialog->realname_entry)),
							gtk_entry_get_text (GTK_ENTRY (dialog->username_entry)),
							gtk_entry_get_text (GTK_ENTRY
												(gnome_file_entry_gtk_entry
												 (GNOME_FILE_ENTRY (dialog->photo_entry)))));
		prefs_set_dcc_receive_folder (gtk_entry_get_text (GTK_ENTRY
														  (gnome_file_entry_gtk_entry
														   (GNOME_FILE_ENTRY
															(dialog->recv_location_entry)))));

		prefs_dialog_closed ();
		gtk_widget_destroy (dialog->win);
		g_free (dialog);
		*(ptr) = NULL;
		break;

	default:
		g_assert_not_reached ();
		break;
	}
}


static void
chatting_font_set_cb (GnomeFontPicker * font_picker,
					  gchar * font,
					  gpointer data)
{
	PangoFontDescription *font_desc;

	prefs_set_viewer_font (font);

	font_desc = pango_font_description_from_string (font);
	gnome_font_picker_fi_set_use_font_in_label (GNOME_FONT_PICKER (font_picker), TRUE,
												(pango_font_description_get_size
												 (font_desc) / PANGO_SCALE));
	pango_font_description_free (font_desc);
}


static void
fg_color_set_cb (GnomeColorPicker * fg_picker,
				 guint red,
				 guint green,
				 guint blue,
				 guint alpha,
				 gpointer data)
{
	gchar *color = g_strdup_printf ("#%02X%02X%02X", red / 256, green / 256, blue / 256);

	prefs_set_viewer_fg (color);

	g_free (color);
}


static void
bg_color_set_cb (GnomeColorPicker * fg_picker,
				 guint red,
				 guint green,
				 guint blue,
				 guint alpha,
				 gpointer data)
{
	gchar *color = g_strdup_printf ("#%02X%02X%02X", red / 256, green / 256, blue / 256);

	prefs_set_viewer_bg (color);

	g_free (color);
}


static void
timestamp_toggled_cb (GtkToggleButton * button,
					  PrefsDialog * dialog)
{
	prefs_set_viewer_show_timestamps (gtk_toggle_button_get_active (button));
}


static void
smileys_toggled_cb (GtkToggleButton * button,
					PrefsDialog * dialog)
{
	prefs_set_use_image_smileys (gtk_toggle_button_get_active (button));
}


static void
notify_msg_color_set_cb (GnomeColorPicker * fg_picker,
						 guint red,
						 guint green,
						 guint blue,
						 guint alpha,
						 gpointer data)
{
	gchar *color = g_strdup_printf ("#%02X%02X%02X", red / 256, green / 256, blue / 256);

	prefs_set_notification_color (ESCO_PESTER_MSG, color);

	g_free (color);
}


static void
notify_text_color_set_cb (GnomeColorPicker * fg_picker,
						  guint red,
						  guint green,
						  guint blue,
						  guint alpha,
						  gpointer data)
{
	gchar *color = g_strdup_printf ("#%02X%02X%02X", red / 256, green / 256, blue / 256);

	prefs_set_notification_color (ESCO_PESTER_TEXT, color);

	g_free (color);
}


static void
win_layout_optmenu_changed_cb (GtkOptionMenu * optmenu,
							   PrefsDialog * dialog)
{
	prefs_set_main_window_layout (gtk_option_menu_get_history (optmenu) + 1);
}


static void
tab_position_optmenu_changed_cb (GtkOptionMenu * optmenu,
								 PrefsDialog * dialog)
{
	prefs_set_main_window_tab_side (gtk_option_menu_get_history (optmenu));
}


static void
more_window_opts_toggled_cb (GtkToggleButton * btn,
							 PrefsDialog * dialog)
{
	if (gtk_toggle_button_get_active (btn))
		gtk_widget_show (dialog->more_opts_box);
	else
		gtk_widget_hide (dialog->more_opts_box);

	prefs_set_prefs_show_window_options (gtk_toggle_button_get_active (btn));
}


static void
force_size_toggled_cb (GtkToggleButton * btn,
					   PrefsDialog * dialog)
{
	gtk_widget_set_sensitive (dialog->width_height_table, gtk_toggle_button_get_active (btn));

	prefs_set_main_window_force_size (gtk_toggle_button_get_active (btn));
}


static void
width_spin_value_changed_cb (GtkSpinButton * btn,
							 PrefsDialog * dialog)
{
	prefs_set_window_width (GNOMECHAT_WINDOW_MAIN, gtk_spin_button_get_value_as_int (btn));
}


static void
height_spin_value_changed_cb (GtkSpinButton * btn,
							  PrefsDialog * dialog)
{
	prefs_set_window_height (GNOMECHAT_WINDOW_MAIN, gtk_spin_button_get_value_as_int (btn));
}


static void
force_position_toggled_cb (GtkToggleButton * btn,
						   PrefsDialog * dialog)
{
	gtk_widget_set_sensitive (dialog->left_top_table, gtk_toggle_button_get_active (btn));

	prefs_set_main_window_force_position (gtk_toggle_button_get_active (btn));
}


static void
left_spin_value_changed_cb (GtkSpinButton * btn,
							PrefsDialog * dialog)
{
	prefs_set_main_window_left_edge (gtk_spin_button_get_value_as_int (btn));
}


static void
top_spin_value_changed_cb (GtkSpinButton * btn,
						   PrefsDialog * dialog)
{
	prefs_set_main_window_top_edge (gtk_spin_button_get_value_as_int (btn));
}


static void
recv_naming_style_toggled_cb (GtkToggleButton * btn,
							  PrefsDialog * dialog)
{
	GSList *list = dialog->recv_naming_group;
	GDccRecvFileNamingStyle style = GDCC_RECV_FILE_SENDER_FILENAME;

	if (!gtk_toggle_button_get_active (btn))
		return;

	while (list != NULL && style >= GDCC_RECV_FILE_FILENAME)
	{
		if (list->data == btn)
		{
			prefs_set_dcc_received_files_naming (style);
			break;
		}

		list = list->next;
		style--;
	}
}


static void
more_options_sel_changed_cb (GtkTreeSelection * sel,
							 MoreOptionsDialog * more_opts)
{
	ViewerStyleType style;
	GtkTreeModel *model;
	GtkTreeIter iter;

	if (gtk_tree_selection_get_selected (sel, &model, &iter) == FALSE)
		return;

	gtk_tree_model_get (model, &iter, MORE_OPTIONS_STYLE_COL, &style, -1);

	more_opts->style = style;

	fill_more_options_optmenu (more_opts);
}


static void
more_options_optmenu_changed_cb (GtkOptionMenu * optmenu,
								 MoreOptionsDialog * more_opts)
{
	GtkWidget *menu;
	GObject *menuitem;
	GdkColor color;
	gchar *color_str;
	gint index;

	menu = gtk_option_menu_get_menu (optmenu);
	index = gtk_option_menu_get_history (optmenu);

	menuitem = G_OBJECT (g_list_nth_data (GTK_MENU_SHELL (menu)->children, index));
	more_opts->style =
		GPOINTER_TO_UINT (g_object_get_qdata (menuitem, MORE_OPTIONS_OPTMENU_STYLE_KEY));


	/* FG Color */
	color_str = prefs_get_viewer_style_fg (more_opts->style);
	if (color_str == NULL)
		color_str = prefs_get_viewer_fg ();

	if (color_str != NULL)
		gdk_color_parse (color_str, &color);
	else
		color.red = color.green = color.blue = 0;

	gnome_color_picker_set_i16 (GNOME_COLOR_PICKER (more_opts->fg_picker),
								color.red, color.green, color.blue, 65535);
	g_free (color_str);

	/* BG Color */
	color_str = prefs_get_viewer_style_bg (more_opts->style);
	if (color_str == NULL)
		color_str = prefs_get_viewer_bg ();

	if (color_str != NULL)
		gdk_color_parse (color_str, &color);
	else
		color.red = color.green = color.blue = 65535;

	gnome_color_picker_set_i16 (GNOME_COLOR_PICKER (more_opts->bg_picker),
								color.red, color.green, color.blue, 65535);
	g_free (color_str);

	/* Font style */
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (more_opts->bold_btn),
								  prefs_get_viewer_style_bold (more_opts->style));
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (more_opts->italic_btn),
								  prefs_get_viewer_style_italic (more_opts->style));
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (more_opts->uline_btn),
								  prefs_get_viewer_style_uline (more_opts->style));

	/* Sensitivity */
	gtk_widget_set_sensitive (more_opts->fg_picker,
							  prefs_get_can_set_viewer_style_fg (more_opts->style));
	gtk_widget_set_sensitive (more_opts->bg_picker,
							  prefs_get_can_set_viewer_style_bg (more_opts->style));

	gtk_widget_set_sensitive (more_opts->bold_btn,
							  prefs_get_can_set_viewer_style_bold (more_opts->style));
	gtk_widget_set_sensitive (more_opts->italic_btn,
							  prefs_get_can_set_viewer_style_italic (more_opts->style));
	gtk_widget_set_sensitive (more_opts->uline_btn,
							  prefs_get_can_set_viewer_style_uline (more_opts->style));
}


static void
more_options_fg_color_set_cb (GnomeColorPicker * fg_picker,
							  guint red,
							  guint green,
							  guint blue,
							  guint alpha,
							  MoreOptionsDialog * more_opts)
{
	gchar *color = g_strdup_printf ("#%02X%02X%02X", red / 256, green / 256, blue / 256);

	prefs_set_viewer_style_fg (more_opts->style, color);

	g_free (color);
}


static void
more_options_bg_color_set_cb (GnomeColorPicker * fg_picker,
							  guint red,
							  guint green,
							  guint blue,
							  guint alpha,
							  MoreOptionsDialog * more_opts)
{
	gchar *color = g_strdup_printf ("#%02X%02X%02X", red / 256, green / 256, blue / 256);

	prefs_set_viewer_style_bg (more_opts->style, color);

	g_free (color);
}


static void
more_options_bold_toggled_cb (GtkToggleButton * button,
							  MoreOptionsDialog * more_opts)
{
	if (prefs_get_viewer_style_bold (more_opts->style) != gtk_toggle_button_get_active (button))
	{
		prefs_set_viewer_style_bold (more_opts->style, gtk_toggle_button_get_active (button));
	}
}


static void
more_options_italic_toggled_cb (GtkToggleButton * button,
								MoreOptionsDialog * more_opts)
{
	if (prefs_get_viewer_style_italic (more_opts->style) != gtk_toggle_button_get_active (button))
	{
		prefs_set_viewer_style_italic (more_opts->style, gtk_toggle_button_get_active (button));
	}
}


static void
more_options_uline_toggled_cb (GtkToggleButton * button,
							   MoreOptionsDialog * more_opts)
{
	if (prefs_get_viewer_style_uline (more_opts->style) != gtk_toggle_button_get_active (button))
	{
		prefs_set_viewer_style_uline (more_opts->style, gtk_toggle_button_get_active (button));
	}
}


static void
more_options_response_cb (GtkDialog * win,
						  gint response,
						  PrefsDialog * dialog)
{
	switch (response)
	{
	case GTK_RESPONSE_HELP:
		g_warning ("FIXME: Connect More Font & Color Options dialog to help system.");
		break;

	case GTK_RESPONSE_DELETE_EVENT:
	case GTK_RESPONSE_CANCEL:
		prefs_fonts_colors_dialog_cancelled ();
		gtk_widget_destroy (dialog->more_opts->win);
		g_free (dialog->more_opts);
		dialog->more_opts = NULL;
		break;

	case GTK_RESPONSE_OK:
		prefs_fonts_colors_dialog_closed ();
		gtk_widget_destroy (dialog->more_opts->win);
		g_free (dialog->more_opts);
		dialog->more_opts = NULL;
		break;

	default:
		g_assert_not_reached ();
		break;
	}
}


static void
render_style_icon (GtkTreeViewColumn * col,
				   GtkCellRenderer * cell,
				   GtkTreeModel * model,
				   GtkTreeIter * iter,
				   gpointer data)
{
	ViewerStyleType viewer_style;
	const gchar *stock_id;

	gtk_tree_model_get (model, iter, MORE_OPTIONS_STYLE_COL, &viewer_style, -1);

	switch (viewer_style)
	{
	case VIEWER_STYLE_MY_NICK:
		stock_id = GNOMECHAT_STOCK_MY_USER;
		break;
	case VIEWER_STYLE_TO_ME_NICK:
		stock_id = GNOMECHAT_STOCK_QUERY;
		break;
	case VIEWER_STYLE_NICK:
		stock_id = GNOMECHAT_STOCK_USER;
		break;
	case VIEWER_STYLE_SERVER:
		stock_id = GNOMECHAT_STOCK_SERVER;
		break;
	case VIEWER_STYLE_CHANNEL:
		stock_id = GNOMECHAT_STOCK_CHANNEL;
		break;
	case VIEWER_STYLE_LINK:
		stock_id = GTK_STOCK_JUMP_TO;
		break;
	case VIEWER_STYLE_ERROR:
		stock_id = GTK_STOCK_DIALOG_ERROR;
		break;

	default:
		stock_id = NULL;
		g_assert_not_reached ();
		break;
	}

	g_object_set (cell, "stock-id", stock_id, NULL);
}


static void
more_options_clicked_cb (GtkButton * btn,
						 PrefsDialog * dialog)
{
	GtkWidget *main_box,
	 *contents_box,
	 *group_box,
	 *hbox,
	 *vbox,
	 *hbox2,
	 *scrwin,
	 *view,
	 *label,
	 *button,
	 *image;
	GtkSizeGroup *label_group, *widget_group;
	GtkListStore *model;
	GtkTreeViewColumn *col;
	GtkCellRenderer *cell;
	GtkTreeSelection *sel;
	GtkTreePath *path;
	gchar *nick,
	 *color_str;
	GdkColor color;

	g_assert (dialog != NULL);

	if (dialog->more_opts != NULL)
	{
		gtk_window_present (GTK_WINDOW (dialog->more_opts->win));
		return;
	}

	prefs_fonts_colors_dialog_opened ();

	label_group = gtk_size_group_new (GTK_SIZE_GROUP_HORIZONTAL);
	widget_group = gtk_size_group_new (GTK_SIZE_GROUP_HORIZONTAL);

	dialog->more_opts = g_new0 (MoreOptionsDialog, 1);

	dialog->more_opts->style = VIEWER_STYLE_MY_NICK;

	dialog->more_opts->win =
		gtk_dialog_new_with_buttons (_("More Font & Color Options - IRC Chat"),
									 GTK_WINDOW (dialog->win),
									 (GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_NO_SEPARATOR),
									 GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
									 GTK_STOCK_OK, GTK_RESPONSE_OK, NULL);
	gtk_window_set_role (GTK_WINDOW (dialog->more_opts->win), "FontColorPreferences");
	gtk_dialog_set_default_response (GTK_DIALOG (dialog->more_opts->win), GTK_RESPONSE_OK);
	util_set_window_icon_from_stock (GTK_WINDOW (dialog->more_opts->win), GTK_STOCK_SELECT_COLOR);
	gtk_container_set_border_width (GTK_CONTAINER (dialog->more_opts->win), 5);
	g_signal_connect (dialog->more_opts->win, "response",
					  G_CALLBACK (more_options_response_cb), dialog);

	main_box = gtk_vbox_new (FALSE, 18);
	gtk_container_set_border_width (GTK_CONTAINER (main_box), 5);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (dialog->more_opts->win)->vbox), main_box);
	gtk_widget_show (main_box);

	/* Prefs Group */
	group_box = gtk_vbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (main_box), group_box, FALSE, FALSE, 0);
	gtk_widget_show (group_box);

	label = gtk_label_new  (_("Fonts & Colors"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	util_boldify_label (label);
	gtk_box_pack_start (GTK_BOX (group_box), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_container_add (GTK_CONTAINER (group_box), hbox);
	gtk_widget_show (hbox);

	label = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	contents_box = gtk_hbox_new (FALSE, 12);
	gtk_container_add (GTK_CONTAINER (hbox), contents_box);
	gtk_widget_show (contents_box);

	/* List */
	scrwin = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrwin), GTK_SHADOW_IN);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrwin), GTK_POLICY_NEVER, 
									GTK_POLICY_AUTOMATIC);
	gtk_box_pack_start (GTK_BOX (contents_box), scrwin, FALSE, FALSE, 0);
	gtk_widget_show (scrwin);

	model = gtk_list_store_new (MORE_OPTIONS_NUM_COLUMNS, G_TYPE_STRING, G_TYPE_INT);

	view = gtk_tree_view_new_with_model (GTK_TREE_MODEL (model));
	gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (view), FALSE);
	gtk_container_add (GTK_CONTAINER (scrwin), view);
	gtk_widget_show (view);

	col = gtk_tree_view_column_new ();
	gtk_tree_view_append_column (GTK_TREE_VIEW (view), col);

	cell = gtk_cell_renderer_pixbuf_new ();
	g_object_set (cell, "stock-size", GTK_ICON_SIZE_MENU, NULL);
	gtk_tree_view_column_pack_start (col, cell, FALSE);
	gtk_tree_view_column_set_cell_data_func (col, cell, render_style_icon, NULL, NULL);

	cell = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_start (col, cell, TRUE);
	gtk_tree_view_column_add_attribute (col, cell, "text", MORE_OPTIONS_TEXT_COL);

	fill_styles_list (model);

	sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (view));
	g_signal_connect (sel, "changed", G_CALLBACK (more_options_sel_changed_cb), dialog->more_opts);

	/* Actual Prefs */
	vbox = gtk_vbox_new (FALSE, 6);
	gtk_container_add (GTK_CONTAINER (contents_box), vbox);
	gtk_widget_show (vbox);

	hbox = gtk_hbox_new (FALSE, 3);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	/* Selection OptionMenu */
	dialog->more_opts->optmenu = gtk_option_menu_new ();
	fill_more_options_optmenu (dialog->more_opts);
	g_signal_connect (dialog->more_opts->optmenu, "changed",
					  G_CALLBACK (more_options_optmenu_changed_cb), dialog->more_opts);
	gtk_box_pack_start (GTK_BOX (hbox), dialog->more_opts->optmenu, FALSE, FALSE, 0);
	gtk_widget_show (dialog->more_opts->optmenu);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_container_add (GTK_CONTAINER (vbox), hbox);
	gtk_widget_show (hbox);

	label = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	vbox = gtk_vbox_new (FALSE, 6);
	gtk_container_add (GTK_CONTAINER (hbox), vbox);
	gtk_widget_show (vbox);

	/* FG Color */
	hbox = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new_with_mnemonic (_("Text _color:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_size_group_add_widget (label_group, label);
	gtk_widget_show (label);

	color_str = prefs_get_viewer_style_fg (dialog->more_opts->style);
	if (color_str != NULL)
	{
		gdk_color_parse (color_str, &color);
		g_free (color_str);
	}
	else
	{
		color.red = color.green = color.blue = 0;
	}

	dialog->more_opts->fg_picker = gnome_color_picker_new ();
	gnome_color_picker_set_title (GNOME_COLOR_PICKER (dialog->more_opts->fg_picker),
								  _("Select a Text Color - IRC Chat"));
	gnome_color_picker_set_i16 (GNOME_COLOR_PICKER (dialog->more_opts->fg_picker),
								color.red, color.green, color.blue, 65535);
	util_set_tooltip (dialog->more_opts->fg_picker, _("Text Color"),
					  _("Change the text color this style uses"), TRUE);
	util_set_label_widget_pair (label, dialog->more_opts->fg_picker);
	g_signal_connect (dialog->more_opts->fg_picker, "color-set",
					  G_CALLBACK (more_options_fg_color_set_cb), dialog->more_opts);
	gtk_container_add (GTK_CONTAINER (hbox), dialog->more_opts->fg_picker);
	gtk_size_group_add_widget (widget_group, dialog->more_opts->fg_picker);
	gtk_widget_show (dialog->more_opts->fg_picker);

	gtk_widget_set_sensitive (dialog->more_opts->fg_picker,
							  prefs_get_can_set_viewer_style_fg (dialog->more_opts->style));

	/* BG Color */
	hbox = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new_with_mnemonic (_("_Background color:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_size_group_add_widget (label_group, label);
	gtk_widget_show (label);

	color_str = prefs_get_viewer_style_bg (dialog->more_opts->style);
	if (color_str != NULL)
	{
		gdk_color_parse (color_str, &color);
		g_free (color_str);
	}
	else
	{
		color.red = color.green = color.blue = 0;
	}

	dialog->more_opts->bg_picker = gnome_color_picker_new ();
	util_set_tooltip (dialog->more_opts->bg_picker, _("Background Color"),
					  _("Change the background color (if any) this style uses"), TRUE);
	util_set_label_widget_pair (label, dialog->more_opts->bg_picker);
	gnome_color_picker_set_title (GNOME_COLOR_PICKER (dialog->more_opts->bg_picker),
								  _("Select a Background Color - IRC Chat"));
	gnome_color_picker_set_i16 (GNOME_COLOR_PICKER (dialog->more_opts->bg_picker),
								color.red, color.green, color.blue, 65535);
	g_signal_connect (dialog->more_opts->bg_picker, "color-set",
					  G_CALLBACK (more_options_bg_color_set_cb), dialog->more_opts);
	gtk_container_add (GTK_CONTAINER (hbox), dialog->more_opts->bg_picker);
	gtk_size_group_add_widget (widget_group, dialog->more_opts->bg_picker);
	gtk_widget_show (dialog->more_opts->bg_picker);

	gtk_widget_set_sensitive (dialog->more_opts->bg_picker,
							  prefs_get_can_set_viewer_style_bg (dialog->more_opts->style));

	/* Font Styles */
	hbox = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new_with_mnemonic (_("Font style:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_size_group_add_widget (label_group, label);
	gtk_widget_show (label);

	hbox2 = gtk_hbox_new (TRUE, 6);
	gtk_container_add (GTK_CONTAINER (hbox), hbox2);
	gtk_size_group_add_widget (widget_group, hbox2);
	gtk_widget_show (hbox2);

	/* Bold */
	button = dialog->more_opts->bold_btn = gtk_toggle_button_new ();
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button),
								  prefs_get_viewer_style_bold (dialog->more_opts->style));
	util_set_tooltip (button, _("Bold"), _("Use a bold font for this style"), TRUE);
	g_signal_connect (button, "toggled",
					  G_CALLBACK (more_options_bold_toggled_cb), dialog->more_opts);
	gtk_container_add (GTK_CONTAINER (hbox2), button);
	gtk_widget_show (button);

	image = gtk_image_new_from_stock (GTK_STOCK_BOLD, GTK_ICON_SIZE_MENU);
	gtk_container_add (GTK_CONTAINER (button), image);
	gtk_widget_show (image);

	gtk_widget_set_sensitive (button,
							  prefs_get_can_set_viewer_style_bold (dialog->more_opts->style));

	/* Italic */
	button = dialog->more_opts->italic_btn = gtk_toggle_button_new ();
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button),
								  prefs_get_viewer_style_italic (dialog->more_opts->style));
	util_set_tooltip (button, _("Italic"), _("Italicize text using this style"), TRUE);
	g_signal_connect (button, "toggled",
					  G_CALLBACK (more_options_italic_toggled_cb), dialog->more_opts);
	gtk_container_add (GTK_CONTAINER (hbox2), button);
	gtk_widget_show (button);

	image = gtk_image_new_from_stock (GTK_STOCK_ITALIC, GTK_ICON_SIZE_MENU);
	gtk_container_add (GTK_CONTAINER (button), image);
	gtk_widget_show (image);

	gtk_widget_set_sensitive (button,
							  prefs_get_can_set_viewer_style_italic (dialog->more_opts->style));

	/* Underline */
	button = dialog->more_opts->uline_btn = gtk_toggle_button_new ();
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button),
								  prefs_get_viewer_style_uline (dialog->more_opts->style));
	util_set_tooltip (button, _("Underline"), _("Underline text using this style"), TRUE);
	g_signal_connect (button, "toggled",
					  G_CALLBACK (more_options_uline_toggled_cb), dialog->more_opts);
	gtk_container_add (GTK_CONTAINER (hbox2), button);
	gtk_widget_show (button);

	image = gtk_image_new_from_stock (GTK_STOCK_UNDERLINE, GTK_ICON_SIZE_MENU);
	gtk_container_add (GTK_CONTAINER (button), image);
	gtk_widget_show (image);

	gtk_widget_set_sensitive (button,
							  prefs_get_can_set_viewer_style_uline (dialog->more_opts->style));

	/* Preview Group */
	group_box = gtk_vbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (main_box), group_box, FALSE, FALSE, 0);
	gtk_widget_show (group_box);

	label = gtk_label_new  (_("Preview"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	util_boldify_label (label);
	gtk_box_pack_start (GTK_BOX (group_box), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_container_add (GTK_CONTAINER (group_box), hbox);
	gtk_widget_show (hbox);

	label = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	/* Preview */
	scrwin = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrwin), GTK_SHADOW_IN);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrwin), GTK_POLICY_AUTOMATIC,
									GTK_POLICY_AUTOMATIC);
	gtk_widget_set_size_request (scrwin, util_get_font_width (scrwin) * 55,
								 util_get_font_height (scrwin) * 7);
	gtk_container_add (GTK_CONTAINER (hbox), scrwin);
	gtk_widget_show (scrwin);

	nick = prefs_get_nick ();
	dialog->more_opts->preview = esco_viewer_new (ESCO_CHAT_MODE_CHANNEL, nick);
	gtk_container_add (GTK_CONTAINER (scrwin), dialog->more_opts->preview);
	gtk_widget_show (dialog->more_opts->preview);
	append_chat_preview_to_viewer (ESCO_VIEWER (dialog->more_opts->preview));

	path = gtk_tree_path_new_first ();
	gtk_tree_selection_select_path (sel, path);
	gtk_tree_path_free (path);

	g_object_unref (label_group);
	g_object_unref (widget_group);

	gtk_window_present (GTK_WINDOW (dialog->more_opts->win));
}


static void
show_motd_toggled_cb (GtkToggleButton * btn,
					  gpointer data)
{
	prefs_set_show_motd_on_connect (gtk_toggle_button_get_active (btn));
}


static void
show_tray_icon_toggled_cb (GtkToggleButton * btn,
						   gpointer data)
{
	prefs_set_show_tray_icon (gtk_toggle_button_get_active (btn));
}


void
prefs_ui_open_prefs_dialog (GtkWindow * parent)
{
	static PrefsDialog *dialog = NULL;

	/* Returned from prefs_get_* functions */
	GDccRecvFileNamingStyle name_style;
	gchar *font = NULL,
	 *color_str = NULL,
	 *dcc_dir = NULL,
	 *str;
	gboolean can_set;

	/* Results of parsing prefs_get_* returns */
	GdkColor color;
	PangoFontDescription *font_desc = NULL;

	/* Warning, these pointers are used for multiple different objects */
	GtkObject *adjust;
	GtkWidget *notebook,
	 *button,
	 *label,
	 *image,
	 *frame,
	 *scrwin,
	 *page_box,
	 *group_box,
	 *contents_box,
	 *hbox,
	 *table,
	 *align,
	 *optmenu,
	 *check,
	 *picker,
	 *menu,
	 *menuitem,
	 *vbox,
	 *radio;
	GtkSizeGroup *label_group,
	 *widget_group;

	if (dialog != NULL)
	{
		gtk_window_present (GTK_WINDOW (dialog->win));
		return;
	}

	prefs_dialog_opened ();

	label_group = gtk_size_group_new (GTK_SIZE_GROUP_HORIZONTAL);
	widget_group = gtk_size_group_new (GTK_SIZE_GROUP_HORIZONTAL);

	dialog = g_new0 (PrefsDialog, 1);
	dialog->recv_naming_group = NULL;

	dialog->win = gtk_dialog_new_with_buttons (_("Preferences - IRC Chat"), parent,
											   GTK_DIALOG_NO_SEPARATOR,
											   GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
											   GTK_STOCK_OK, GTK_RESPONSE_OK, NULL);
	gtk_dialog_set_default_response (GTK_DIALOG (dialog->win), GTK_RESPONSE_OK);
	gtk_window_set_role (GTK_WINDOW (dialog->win), "Preferences");
	gtk_window_set_default_size (GTK_WINDOW (dialog->win),
								 prefs_get_window_width (GNOMECHAT_WINDOW_PREFS),
								 prefs_get_window_height (GNOMECHAT_WINDOW_PREFS));
	util_set_window_icon_from_stock (GTK_WINDOW (dialog->win), GTK_STOCK_PREFERENCES);
	util_set_save_window_size (dialog->win, GNOMECHAT_WINDOW_PREFS);
	g_signal_connect (dialog->win, "response", G_CALLBACK (dialog_response_cb), &dialog);
	gtk_container_set_border_width (GTK_CONTAINER (dialog->win), 5);

	/* Notebook */
	notebook = gtk_notebook_new ();
	gtk_container_set_border_width (GTK_CONTAINER (notebook), 5);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (dialog->win)->vbox), notebook);
	gtk_widget_show (notebook);


	/* Fonts & Colors Label */
	label = gtk_label_new (_("Fonts & Colors"));
	gtk_widget_show (label);

	/* Fonts & Colors Page */
	page_box = gtk_vbox_new (FALSE, 18);
	gtk_container_set_border_width (GTK_CONTAINER (page_box), 12);
	gtk_notebook_append_page (GTK_NOTEBOOK (notebook), page_box, label);
	gtk_widget_show (page_box);

	group_box = gtk_vbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (page_box), group_box, FALSE, FALSE, 0);
	gtk_widget_show (group_box);

	label = gtk_label_new (_("Chatting"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	util_boldify_label (label);
	gtk_box_pack_start (GTK_BOX (group_box), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (group_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	contents_box = gtk_vbox_new (FALSE, 6);
	gtk_container_add (GTK_CONTAINER (hbox), contents_box);
	gtk_widget_show (contents_box);

	/* Font */
	hbox = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (contents_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new_with_mnemonic (_("_Font:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_size_group_add_widget (label_group, label);
	gtk_widget_show (label);

	font = prefs_get_viewer_font ();
	if (font == NULL)
		font = prefs_get_default_font ();

	font_desc = pango_font_description_from_string (font);

	picker = gnome_font_picker_new ();
	util_set_label_widget_pair (label, picker);
	gnome_font_picker_set_mode (GNOME_FONT_PICKER (picker), GNOME_FONT_PICKER_MODE_FONT_INFO);
	gnome_font_picker_set_font_name (GNOME_FONT_PICKER (picker), font);
	g_free (font);
	gnome_font_picker_set_preview_text (GNOME_FONT_PICKER (picker), _("<Nickname> Message"));
	gnome_font_picker_set_title (GNOME_FONT_PICKER (picker), _("Select a Font - IRC Chat"));
	util_set_tooltip (picker, _("Font"), _("Change the font"), FALSE);
	gnome_font_picker_fi_set_use_font_in_label (GNOME_FONT_PICKER (picker), TRUE,
												(pango_font_description_get_size
												 (font_desc) / PANGO_SCALE));
	pango_font_description_free (font_desc);
	g_signal_connect (picker, "font-set", G_CALLBACK (chatting_font_set_cb), NULL);
	gtk_container_add (GTK_CONTAINER (hbox), picker);
	gtk_size_group_add_widget (widget_group, picker);
	gtk_widget_show (picker);

	gtk_widget_set_sensitive (picker, prefs_get_can_set_viewer_font ());

	/* Global FG Color */
	hbox = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (contents_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new_with_mnemonic (_("Text _color:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_size_group_add_widget (label_group, label);
	gtk_widget_show (label);

	color_str = prefs_get_viewer_fg ();
	if (color_str != NULL)
		gdk_color_parse (color_str, &color);
	else
		color.red = color.green = color.blue = 0;
	g_free (color_str);

	picker = gnome_color_picker_new ();
	util_set_label_widget_pair (label, picker);
	gnome_color_picker_set_title (GNOME_COLOR_PICKER (picker), _("Select a Text Color - IRC Chat"));
	gnome_color_picker_set_i16 (GNOME_COLOR_PICKER (picker),
								color.red, color.green, color.blue, 65535);
	util_set_tooltip (picker, _("Default Text Color"), _("Change the default text color"), FALSE);
	g_signal_connect (picker, "color-set", G_CALLBACK (fg_color_set_cb), NULL);
	gtk_container_add (GTK_CONTAINER (hbox), picker);
	gtk_size_group_add_widget (widget_group, picker);
	gtk_widget_show (picker);

	gtk_widget_set_sensitive (picker, prefs_get_can_set_viewer_fg ());

	/* Global BG Color */
	hbox = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (contents_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new_with_mnemonic (_("_Background color:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_size_group_add_widget (label_group, label);
	gtk_widget_show (label);

	color_str = prefs_get_viewer_bg ();
	if (color_str != NULL)
		gdk_color_parse (color_str, &color);
	else
		color.red = color.green = color.blue = 65535;
	g_free (color_str);

	picker = gnome_color_picker_new ();
	util_set_label_widget_pair (label, picker);
	gnome_color_picker_set_title (GNOME_COLOR_PICKER (picker),
								  _("Select a Background Color - IRC Chat"));
	util_set_tooltip (picker, _("Default Background Color"),
					  _("Change the default background color"), FALSE);
	gnome_color_picker_set_i16 (GNOME_COLOR_PICKER (picker),
								color.red, color.green, color.blue, 65535);
	g_signal_connect (picker, "color-set", G_CALLBACK (bg_color_set_cb), NULL);
	gtk_container_add (GTK_CONTAINER (hbox), picker);
	gtk_size_group_add_widget (widget_group, picker);
	gtk_widget_show (picker);

	gtk_widget_set_sensitive (picker, prefs_get_can_set_viewer_bg ());

	/* Show Timestamps checkbutton */
	check = gtk_check_button_new_with_mnemonic (_("_Show the time a message was sent"));
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check), prefs_get_viewer_show_timestamps ());
	g_signal_connect (check, "toggled", G_CALLBACK (timestamp_toggled_cb), dialog);
	gtk_box_pack_start (GTK_BOX (contents_box), check, FALSE, FALSE, 0);
	gtk_widget_show (check);

	gtk_widget_set_sensitive (check, prefs_get_can_set_show_timestamps ());

	/* Image Smileys checkbutton */
	check = gtk_check_button_new_with_mnemonic (_("_Show smileys as pictures"));
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check), prefs_get_use_image_smileys ());
	g_signal_connect (check, "toggled", G_CALLBACK (smileys_toggled_cb), dialog);
	gtk_box_pack_start (GTK_BOX (contents_box), check, FALSE, FALSE, 0);
	gtk_widget_show (check);

	gtk_widget_set_sensitive (check, prefs_get_can_set_use_image_smileys ());

	/* "More Options..." button */
	align = gtk_alignment_new (1.0, 0.5, 0.0, 0.0);
	gtk_box_pack_start (GTK_BOX (contents_box), align, FALSE, FALSE, 0);
	gtk_widget_show (align);

	button = gtk_button_new ();
	util_set_tooltip (button, _("Advanced font and style settings"),
					  _("Change the font styles used individually for the various types of "
						"messages recieved on IRC."), FALSE);
	gtk_container_add (GTK_CONTAINER (align), button);
	g_signal_connect (button, "clicked", G_CALLBACK (more_options_clicked_cb), dialog);
	gtk_widget_show (button);

	align = gtk_alignment_new (0.5, 0.5, 0.0, 0.0);
	gtk_container_add (GTK_CONTAINER (button), align);
	gtk_widget_show (align);

	hbox = gtk_hbox_new (FALSE, 3);
	gtk_container_add (GTK_CONTAINER (align), hbox);
	gtk_widget_show (hbox);

	/* And no, I won't change this to GTK_ICON_SIZE_BUTTON */
	image = gtk_image_new_from_stock (GTK_STOCK_SELECT_COLOR, GTK_ICON_SIZE_MENU);
	gtk_box_pack_start (GTK_BOX (hbox), image, FALSE, FALSE, 0);
	gtk_widget_show (image);

	label = gtk_label_new_with_mnemonic (_("_More Options"));
	util_set_label_widget_pair (label, button);
	gtk_container_add (GTK_CONTAINER (hbox), label);
	gtk_widget_show (label);

	/* Notification group */
	group_box = gtk_vbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (page_box), group_box, TRUE, TRUE, 0);
	gtk_widget_show (group_box);

	label = gtk_label_new (_("Notification"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	util_boldify_label (label);
	gtk_box_pack_start (GTK_BOX (group_box), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (group_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	contents_box = gtk_vbox_new (FALSE, 6);
	gtk_container_add (GTK_CONTAINER (hbox), contents_box);
	gtk_widget_show (contents_box);

	check = gtk_check_button_new_with_label (_("Show a notification icon in the status tray"));
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check), prefs_get_show_tray_icon ());
	g_signal_connect (check, "toggled", G_CALLBACK (show_tray_icon_toggled_cb), NULL);
	gtk_box_pack_start (GTK_BOX (contents_box), check, FALSE, FALSE, 0);
	gtk_widget_show (check);

	gtk_widget_set_sensitive (check, prefs_get_can_set_show_tray_icon ());

	/* Messages to me */
	hbox = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (contents_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new_with_mnemonic (_("M_essages to me:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_size_group_add_widget (label_group, label);
	gtk_widget_show (label);

	color_str = prefs_get_notification_color (ESCO_PESTER_MSG);
	if (color_str != NULL)
	{
		gdk_color_parse (color_str, &color);
		g_free (color_str);
	}
	else
	{
		color.red = 65535;
		color.green = color.blue = 0;
	}
	picker = gnome_color_picker_new ();
	gnome_color_picker_set_title (GNOME_COLOR_PICKER (picker),
								  _("Select Message Notification Color - IRC Chat"));
	gnome_color_picker_set_i16 (GNOME_COLOR_PICKER (picker),
								color.red, color.green, color.blue, 65535);
	util_set_tooltip (picker, _("Color to use for chats with new messages to you in them"),
					  _("The message notification color is used to highlight the names of chats in "
						"the \"Chats\" menu and on notebook tabs when a new message with your "
						"nickname in it is recieved."), FALSE);
	util_set_label_widget_pair (label, picker);
	g_signal_connect (picker, "color-set", G_CALLBACK (notify_msg_color_set_cb), NULL);
	gtk_container_add (GTK_CONTAINER (hbox), picker);
	gtk_size_group_add_widget (widget_group, picker);
	gtk_widget_show (picker);

	gtk_widget_set_sensitive (picker, prefs_get_can_set_notification_color (ESCO_PESTER_MSG));

	/* Messages in general */
	hbox = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (contents_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new_with_mnemonic (_("_Discussion:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_size_group_add_widget (label_group, label);
	gtk_widget_show (label);

	color_str = prefs_get_notification_color (ESCO_PESTER_TEXT);
	if (color_str != NULL)
	{
		gdk_color_parse (color_str, &color);
		g_free (color_str);
	}
	else
	{
		color.red = color.green = 0;
		color.blue = 65535;
	}
	picker = gnome_color_picker_new ();
	gnome_color_picker_set_title (GNOME_COLOR_PICKER (picker),
								  _("Select Text Notification Color - IRC Chat"));
	gnome_color_picker_set_i16 (GNOME_COLOR_PICKER (picker),
								color.red, color.green, color.blue, 65535);
	util_set_tooltip (picker, _("Color to use for chats with new discussions in them"),
					  _("The message notification color is used to highlight the names of chats in "
						"the \"Chats\" menu and on notebook tabs when a new message is recieved."),
					  FALSE);
	util_set_label_widget_pair (label, picker);
	g_signal_connect (picker, "color-set", G_CALLBACK (notify_text_color_set_cb), NULL);
	gtk_container_add (GTK_CONTAINER (hbox), picker);
	gtk_size_group_add_widget (widget_group, picker);
	gtk_widget_show (picker);

	gtk_widget_set_sensitive (picker, prefs_get_can_set_notification_color (ESCO_PESTER_TEXT));


	/* Windows Label */
	label = gtk_label_new (_("Windows"));
	gtk_widget_show (label);

	/* Windows Page */
	page_box = gtk_vbox_new (TRUE, 18);
	gtk_container_set_border_width (GTK_CONTAINER (page_box), 12);
	gtk_notebook_append_page (GTK_NOTEBOOK (notebook), page_box, label);
	gtk_widget_show (page_box);

	/* New Windows Group */
	group_box = gtk_vbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (page_box), group_box, TRUE, TRUE, 0);
	gtk_widget_show (group_box);

	label = gtk_label_new (_("New Windows"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	util_boldify_label (label);
	gtk_box_pack_start (GTK_BOX (group_box), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (group_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	contents_box = gtk_vbox_new (FALSE, 6);
	gtk_container_add (GTK_CONTAINER (hbox), contents_box);
	gtk_widget_show (contents_box);

	/* Layout */
	hbox = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (contents_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new_with_mnemonic (_("_Layout:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_size_group_add_widget (label_group, label);
	gtk_widget_show (label);

	menu = gtk_menu_new ();
	menuitem = gtk_menu_item_new_with_mnemonic (_("Tabs"));
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	g_object_set_qdata (G_OBJECT (menuitem), PREFS_LAYOUT_KEY,
						GUINT_TO_POINTER (ESCO_WINDOW_LAYOUT_TABS));
	gtk_widget_show (menuitem);

	menuitem = gtk_menu_item_new_with_mnemonic (_("List"));
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	g_object_set_qdata (G_OBJECT (menuitem), PREFS_LAYOUT_KEY,
						GUINT_TO_POINTER (ESCO_WINDOW_LAYOUT_TREE));
	gtk_widget_show (menuitem);

	menuitem = gtk_menu_item_new_with_mnemonic (_("Separate Windows"));
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	g_object_set_qdata (G_OBJECT (menuitem), PREFS_LAYOUT_KEY,
						GUINT_TO_POINTER (ESCO_WINDOW_LAYOUT_WINDOW));
	gtk_widget_show (menuitem);
	gtk_widget_show (menu);

	optmenu = gtk_option_menu_new ();
	gtk_option_menu_set_menu (GTK_OPTION_MENU (optmenu), menu);
	gtk_option_menu_set_history (GTK_OPTION_MENU (optmenu), prefs_get_main_window_layout () - 1);
	g_signal_connect (optmenu, "changed", G_CALLBACK (win_layout_optmenu_changed_cb), dialog);
	gtk_container_add (GTK_CONTAINER (hbox), optmenu);
	gtk_size_group_add_widget (widget_group, optmenu);
	gtk_widget_show (optmenu);

	gtk_widget_set_sensitive (optmenu, prefs_get_can_set_main_window_layout ());

	/* Tab side */
	hbox = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (contents_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new_with_mnemonic (_("_Tab side:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_size_group_add_widget (label_group, label);
	gtk_widget_show (label);

	menu = gtk_menu_new ();
	menuitem = gtk_menu_item_new_with_mnemonic (_("Left"));
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	g_object_set_qdata (G_OBJECT (menuitem), PREFS_TAB_SIDE_KEY, GUINT_TO_POINTER (GTK_POS_LEFT));
	gtk_widget_show (menuitem);

	menuitem = gtk_menu_item_new_with_mnemonic (_("Right"));
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	g_object_set_qdata (G_OBJECT (menuitem), PREFS_TAB_SIDE_KEY, GUINT_TO_POINTER (GTK_POS_RIGHT));
	gtk_widget_show (menuitem);

	menuitem = gtk_menu_item_new_with_mnemonic (_("Top"));
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	g_object_set_qdata (G_OBJECT (menuitem), PREFS_TAB_SIDE_KEY, GUINT_TO_POINTER (GTK_POS_TOP));
	gtk_widget_show (menuitem);

	menuitem = gtk_menu_item_new_with_label (_("Bottom"));
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	g_object_set_qdata (G_OBJECT (menuitem), PREFS_TAB_SIDE_KEY, GUINT_TO_POINTER (GTK_POS_BOTTOM));
	gtk_widget_show (menuitem);
	gtk_widget_show (menu);

	optmenu = gtk_option_menu_new ();
	gtk_option_menu_set_menu (GTK_OPTION_MENU (optmenu), menu);
	gtk_option_menu_set_history (GTK_OPTION_MENU (optmenu), prefs_get_main_window_tab_side ());
	g_signal_connect (optmenu, "changed", G_CALLBACK (tab_position_optmenu_changed_cb), dialog);
	gtk_container_add (GTK_CONTAINER (hbox), optmenu);
	gtk_size_group_add_widget (widget_group, optmenu);
	gtk_widget_show (optmenu);

	gtk_widget_set_sensitive (optmenu, prefs_get_can_set_main_window_tab_side ());

	/* More options */
	check = esco_disclosure_new_with_mnemonic (_("_More options"));
	g_signal_connect (check, "toggled", G_CALLBACK (more_window_opts_toggled_cb), dialog);
	gtk_box_pack_start (GTK_BOX (contents_box), check, FALSE, FALSE, 0);
	gtk_widget_show (check);

	dialog->more_opts_box = gtk_vbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (contents_box), dialog->more_opts_box, FALSE, FALSE, 0);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check),
								  prefs_get_prefs_show_window_options ());

	/* Width/Height Group */
	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (dialog->more_opts_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	check = gtk_check_button_new_with_mnemonic (_("Force si_ze of new windows:"));
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check), prefs_get_main_window_force_size ());
	g_signal_connect (check, "toggled", G_CALLBACK (force_size_toggled_cb), dialog);
	gtk_box_pack_start (GTK_BOX (hbox), check, FALSE, FALSE, 0);
	gtk_widget_show (check);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (dialog->more_opts_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new ("        ");
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	table = dialog->width_height_table = gtk_table_new (2, 2, FALSE);
	gtk_table_set_col_spacings (GTK_TABLE (table), 12);
	gtk_table_set_row_spacings (GTK_TABLE (table), 6);
	gtk_widget_set_sensitive (table, prefs_get_main_window_force_size ());
	gtk_container_add (GTK_CONTAINER (hbox), table);
	gtk_widget_show (table);

	/* Width */
	label = gtk_label_new_with_mnemonic (_("_Width:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 0, 1,
					  (GTK_EXPAND | GTK_FILL), GTK_FILL, 0, 0);
	gtk_widget_show (label);

	align = gtk_alignment_new (0.0, 0.5, 0.0, 0.0);
	gtk_table_attach (GTK_TABLE (table), align, 1, 2, 0, 1, GTK_FILL, GTK_FILL, 0, 0);
	gtk_size_group_add_widget (widget_group, align);
	gtk_widget_show (align);

	adjust = gtk_adjustment_new ((gfloat) prefs_get_window_width (GNOMECHAT_WINDOW_MAIN), 0.0,
								 (gfloat) gdk_screen_width (), 1.0, 1.0, 10.0);
	dialog->width_spin = gtk_spin_button_new (GTK_ADJUSTMENT (adjust), 1.0, 0);
	g_signal_connect (dialog->width_spin, "value-changed",
					  G_CALLBACK (width_spin_value_changed_cb), dialog);
	gtk_container_add (GTK_CONTAINER (align), dialog->width_spin);
	gtk_widget_show (dialog->width_spin);


	/* Height */
	label = gtk_label_new_with_mnemonic (_("_Height:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 1, 2,
					  (GTK_EXPAND | GTK_FILL), GTK_FILL, 0, 0);
	gtk_widget_show (label);

	align = gtk_alignment_new (0.0, 0.5, 0.0, 0.0);
	gtk_table_attach (GTK_TABLE (table), align, 1, 2, 1, 2, GTK_FILL, GTK_FILL, 0, 0);
	gtk_size_group_add_widget (widget_group, align);
	gtk_widget_show (align);

	adjust = gtk_adjustment_new ((gfloat) prefs_get_window_height (GNOMECHAT_WINDOW_MAIN), 0.0,
								 (gfloat) gdk_screen_height (), 1.0, 1.0, 10.0);
	dialog->height_spin = gtk_spin_button_new (GTK_ADJUSTMENT (adjust), 1.0, 0);
	g_signal_connect (dialog->height_spin, "value-changed",
					  G_CALLBACK (height_spin_value_changed_cb), dialog);
	gtk_container_add (GTK_CONTAINER (align), dialog->height_spin);
	gtk_widget_show (dialog->height_spin);

	can_set = prefs_get_can_set_main_window_force_size ();
	gtk_widget_set_sensitive (check, can_set);
	gtk_widget_set_sensitive (dialog->width_spin, can_set);
	gtk_widget_set_sensitive (dialog->height_spin, can_set);

	/* Left/Top Group */
	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (dialog->more_opts_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	check = gtk_check_button_new_with_mnemonic (_("Force _placement of new windows:"));
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check),
								  prefs_get_main_window_force_position ());
	g_signal_connect (check, "toggled", G_CALLBACK (force_position_toggled_cb), dialog);
	gtk_box_pack_start (GTK_BOX (hbox), check, FALSE, FALSE, 0);
	gtk_widget_show (check);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (dialog->more_opts_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new ("        ");
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	table = dialog->left_top_table = gtk_table_new (2, 2, FALSE);
	gtk_table_set_col_spacings (GTK_TABLE (table), 3);
	gtk_table_set_row_spacings (GTK_TABLE (table), 3);
	gtk_widget_set_sensitive (table, prefs_get_main_window_force_position ());
	gtk_container_add (GTK_CONTAINER (hbox), table);
	gtk_widget_show (table);

	/* Left */
	label = gtk_label_new_with_mnemonic (_("Left _edge:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 0, 1,
					  (GTK_EXPAND | GTK_FILL), GTK_FILL, 0, 0);
	gtk_widget_show (label);

	align = gtk_alignment_new (0.0, 0.5, 0.0, 0.0);
	gtk_table_attach (GTK_TABLE (table), align, 1, 2, 0, 1, GTK_FILL, GTK_FILL, 0, 0);
	gtk_size_group_add_widget (widget_group, align);
	gtk_widget_show (align);

	adjust = gtk_adjustment_new ((gfloat) prefs_get_main_window_left_edge (), 0.0,
								 (gfloat) gdk_screen_width (), 1.0, 1.0, 10.0);
	dialog->left_spin = gtk_spin_button_new (GTK_ADJUSTMENT (adjust), 1.0, 0);
	g_signal_connect (dialog->left_spin, "value-changed",
					  G_CALLBACK (left_spin_value_changed_cb), dialog);
	gtk_container_add (GTK_CONTAINER (align), dialog->left_spin);
	gtk_widget_show (dialog->left_spin);

	/* Top */
	label = gtk_label_new_with_mnemonic (_("T_op edge:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 1, 2,
					  (GTK_EXPAND | GTK_FILL), GTK_FILL, 0, 0);
	gtk_widget_show (label);

	align = gtk_alignment_new (0.0, 0.5, 0.0, 0.0);
	gtk_table_attach (GTK_TABLE (table), align, 1, 2, 1, 2, GTK_FILL, GTK_FILL, 0, 0);
	gtk_size_group_add_widget (widget_group, align);
	gtk_widget_show (align);

	adjust = gtk_adjustment_new ((gfloat) prefs_get_main_window_top_edge (), 0.0,
								 (gfloat) gdk_screen_height (), 1.0, 1.0, 10.0);
	dialog->top_spin = gtk_spin_button_new (GTK_ADJUSTMENT (adjust), 1.0, 0);
	g_signal_connect (dialog->top_spin, "value-changed",
					  G_CALLBACK (top_spin_value_changed_cb), dialog);
	gtk_container_add (GTK_CONTAINER (align), dialog->top_spin);
	gtk_widget_show (dialog->top_spin);

	can_set = prefs_get_can_set_main_window_force_position ();
	gtk_widget_set_sensitive (check, can_set);
	gtk_widget_set_sensitive (dialog->left_spin, can_set);
	gtk_widget_set_sensitive (dialog->top_spin, can_set);


	/* Identity Label */
	label = gtk_label_new (_("Identity"));
	gtk_widget_show (label);

	/* Identity Page */
	page_box = gtk_vbox_new (FALSE, 18);
	gtk_container_set_border_width (GTK_CONTAINER (page_box), 12);
	gtk_notebook_append_page (GTK_NOTEBOOK (notebook), page_box, label);
	gtk_widget_show (page_box);

	/* Identity Group */
	group_box = gtk_vbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (page_box), group_box, FALSE, FALSE, 0);
	gtk_widget_show (group_box);

	label = gtk_label_new (_("Information"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	util_boldify_label (label);
	gtk_box_pack_start (GTK_BOX (group_box), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (group_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	contents_box = gtk_vbox_new (FALSE, 6);
	gtk_container_add (GTK_CONTAINER (hbox), contents_box);
	gtk_widget_show (contents_box);

	/* Nickname */
	hbox = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (contents_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new_with_mnemonic (_("_Nickname:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_size_group_add_widget (label_group, label);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	dialog->nick_entry = gtk_entry_new ();
	str = prefs_get_nick ();
	if (str != NULL)
	{
		gtk_entry_set_text (GTK_ENTRY (dialog->nick_entry), str);
		g_free (str);
	}
	util_set_label_widget_pair (label, dialog->nick_entry);
	gtk_container_add (GTK_CONTAINER (hbox), dialog->nick_entry);
	gtk_size_group_add_widget (widget_group, dialog->nick_entry);
	gtk_widget_show (dialog->nick_entry);

	gtk_widget_set_sensitive (dialog->nick_entry, prefs_get_can_set_nick ());

	/* Real name */
	hbox = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (contents_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);
	
	label = gtk_label_new_with_mnemonic (_("_Real name:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_size_group_add_widget (label_group, label);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	dialog->realname_entry = gtk_entry_new ();
	str = prefs_get_realname ();
	if (str != NULL)
	{
		gtk_entry_set_text (GTK_ENTRY (dialog->realname_entry), str);
		g_free (str);
	}
	util_set_label_widget_pair (label, dialog->realname_entry);
	gtk_container_add (GTK_CONTAINER (hbox), dialog->realname_entry);
	gtk_size_group_add_widget (widget_group, dialog->realname_entry);
	gtk_widget_show (dialog->realname_entry);

	gtk_widget_set_sensitive (dialog->realname_entry, prefs_get_can_set_realname ());

	/* User name */
	hbox = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (contents_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);
	
	label = gtk_label_new_with_mnemonic (_("_User name:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_size_group_add_widget (label_group, label);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	dialog->username_entry = gtk_entry_new ();
	str = prefs_get_username ();
	if (str != NULL)
	{
		gtk_entry_set_text (GTK_ENTRY (dialog->username_entry), str);
		g_free (str);
	}
	util_set_label_widget_pair (label, dialog->username_entry);
	gtk_container_add (GTK_CONTAINER (hbox), dialog->username_entry);
	gtk_size_group_add_widget (widget_group, dialog->username_entry);
	gtk_widget_show (dialog->username_entry);

	gtk_widget_set_sensitive (dialog->username_entry, prefs_get_can_set_username ());

	/* Photo file */
	group_box = gtk_vbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (page_box), group_box, TRUE, TRUE, 0);
	gtk_widget_show (group_box);

	label = gtk_label_new (_("Photo"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	util_boldify_label (label);
	gtk_box_pack_start (GTK_BOX (group_box), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (group_box), hbox, TRUE, TRUE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	dialog->photo_entry = gnome_pixmap_entry_new ("photo-history",
												  _("Select a Photo - IRC Chat"), TRUE);
	str = prefs_get_photo_file ();
	gnome_file_entry_set_filename (GNOME_FILE_ENTRY (dialog->photo_entry), str);
	g_free (str);
	gnome_pixmap_entry_set_preview_size (GNOME_PIXMAP_ENTRY (dialog->photo_entry), 100, 100);
	gtk_box_pack_start (GTK_BOX (hbox), dialog->photo_entry, TRUE, TRUE, 0);
	gtk_widget_show (dialog->photo_entry);

	gtk_widget_set_sensitive (dialog->photo_entry, prefs_get_can_set_photo_file ());


	/* DCC Label */
	label = gtk_label_new (_("File Transfer"));
	gtk_widget_show (label);

	/* DCC Page */
	page_box = gtk_vbox_new (FALSE, 18);
	gtk_container_set_border_width (GTK_CONTAINER (page_box), 12);
	gtk_notebook_append_page (GTK_NOTEBOOK (notebook), page_box, label);
	gtk_widget_show (page_box);

	/* Recieved Files Group */
	group_box = gtk_vbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (page_box), group_box, TRUE, TRUE, 0);
	gtk_widget_show (group_box);

	label = gtk_label_new (_("Recieved Files"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	util_boldify_label (label);
	gtk_box_pack_start (GTK_BOX (group_box), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (group_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	contents_box = gtk_vbox_new (FALSE, 6);
	gtk_container_add (GTK_CONTAINER (hbox), contents_box);
	gtk_widget_show (contents_box);

	/* DCC Recv Location */
	hbox = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (contents_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new_with_mnemonic (_("_Place received files in:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_size_group_add_widget (label_group, label);
	gtk_widget_show (label);

	dialog->recv_location_entry = gnome_file_entry_new ("dcc-recv-location",
														_("Select A Folder - IRC Chat"));
	gnome_file_entry_set_directory_entry (GNOME_FILE_ENTRY (dialog->recv_location_entry), TRUE);
	dcc_dir = prefs_get_dcc_receive_folder ();
	if (dcc_dir == NULL)
	{
		dcc_dir = g_strdup (g_get_home_dir ());
	}
	else if (!g_file_test (dcc_dir, G_FILE_TEST_EXISTS))
	{
		mkdir (dcc_dir, 0777);
	}
	else if (!g_file_test (dcc_dir, G_FILE_TEST_IS_DIR))
	{
		g_free (dcc_dir);
		dcc_dir = g_strdup (g_get_home_dir ());
	}
	gnome_file_entry_set_filename (GNOME_FILE_ENTRY (dialog->recv_location_entry), dcc_dir);
	g_free (dcc_dir);
	gtk_container_add (GTK_CONTAINER (hbox), dialog->recv_location_entry);
	gtk_size_group_add_widget (widget_group, dialog->recv_location_entry);
	gtk_widget_show (dialog->recv_location_entry);

	gtk_widget_set_sensitive (dialog->recv_location_entry,
							  prefs_get_can_set_dcc_receieve_folder ());

	/* DCC Recv File Naming */
	hbox = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (contents_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new_with_mnemonic (_("_Received files are:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.04);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_size_group_add_widget (label_group, label);
	gtk_widget_show (label);

	vbox = gtk_vbox_new (FALSE, 3);
	gtk_container_add (GTK_CONTAINER (hbox), vbox);
	gtk_widget_show (vbox);

	radio = gtk_radio_button_new_with_label (dialog->recv_naming_group,
											 _("Placed in the above folder as-is"));
	dialog->recv_naming_group = gtk_radio_button_get_group (GTK_RADIO_BUTTON (radio));
	g_signal_connect (radio, "toggled", G_CALLBACK (recv_naming_style_toggled_cb), dialog);
	gtk_box_pack_start (GTK_BOX (vbox), radio, FALSE, FALSE, 0);
	gtk_size_group_add_widget (widget_group, radio);
	gtk_widget_show (radio);

	radio = gtk_radio_button_new_with_label (dialog->recv_naming_group,
											 _("Placed in a sub-folder called <Sender>"));
	dialog->recv_naming_group = gtk_radio_button_get_group (GTK_RADIO_BUTTON (radio));
	g_signal_connect (radio, "toggled", G_CALLBACK (recv_naming_style_toggled_cb), dialog);
	gtk_box_pack_start (GTK_BOX (vbox), radio, FALSE, FALSE, 0);
	gtk_size_group_add_widget (widget_group, radio);
	gtk_widget_show (radio);

	radio = gtk_radio_button_new_with_label (dialog->recv_naming_group,
											 _("Re-named to <Sender>.<Filename>"));
	dialog->recv_naming_group = gtk_radio_button_get_group (GTK_RADIO_BUTTON (radio));
	g_signal_connect (radio, "toggled", G_CALLBACK (recv_naming_style_toggled_cb), dialog);
	gtk_box_pack_start (GTK_BOX (vbox), radio, FALSE, FALSE, 0);
	gtk_size_group_add_widget (widget_group, radio);
	gtk_widget_show (radio);

	gtk_widget_set_sensitive (vbox, prefs_get_can_set_dcc_receieved_files_naming ());

	/*
	 * Reverse the name style so it works with the screwy list ordering in 
	 * GtkRadioButton.
	 */
	name_style = prefs_get_dcc_received_files_naming ();
	if (name_style == GDCC_RECV_FILE_FILENAME)
		name_style = GDCC_RECV_FILE_SENDER_FILENAME;
	else if (name_style == GDCC_RECV_FILE_SENDER_FILENAME)
		name_style = GDCC_RECV_FILE_FILENAME;

	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (g_slist_nth_data (dialog->recv_naming_group,
																	   name_style)), TRUE);


	/* Miscellaneous Label */
	label = gtk_label_new (_("Miscellaneous"));
	gtk_widget_show (label);

	/* Miscellaneous Page */
	page_box = gtk_vbox_new (FALSE, 18);
	gtk_container_set_border_width (GTK_CONTAINER (page_box), 12);
	gtk_notebook_append_page (GTK_NOTEBOOK (notebook), page_box, label);
	gtk_widget_show (page_box);

	/* IRC Options */
	group_box = gtk_vbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (page_box), group_box, FALSE, FALSE, 0);
	gtk_widget_show (group_box);

	label = gtk_label_new (_("IRC Options"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	util_boldify_label (label);
	gtk_box_pack_start (GTK_BOX (group_box), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (group_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	contents_box = gtk_vbox_new (FALSE, 6);
	gtk_container_add (GTK_CONTAINER (hbox), contents_box);
	gtk_widget_show (contents_box);

	check = gtk_check_button_new_with_mnemonic (_("Show message of the day after " "connecting"));
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check), prefs_get_show_motd_on_connect ());
	g_signal_connect (check, "toggled", G_CALLBACK (show_motd_toggled_cb), NULL);
	gtk_box_pack_start (GTK_BOX (contents_box), check, FALSE, FALSE, 0);
	gtk_widget_show (check);

	/* Alert Group */
	group_box = gtk_vbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (page_box), group_box, TRUE, TRUE, 0);
	gtk_widget_show (group_box);

	label = gtk_label_new (_("Alerts"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	util_boldify_label (label);
	gtk_box_pack_start (GTK_BOX (group_box), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (group_box), hbox, TRUE, TRUE, 0);
	gtk_widget_show (hbox);

	label = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	scrwin = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrwin), GTK_POLICY_AUTOMATIC,
									GTK_POLICY_AUTOMATIC);
	gtk_box_pack_start (GTK_BOX (hbox), scrwin, TRUE, TRUE, 0);
	gtk_widget_show (scrwin);

	frame = gtk_viewport_new (NULL, NULL);
	gtk_viewport_set_shadow_type (GTK_VIEWPORT (frame), GTK_SHADOW_NONE);
	gtk_container_add (GTK_CONTAINER (scrwin), frame);
	gtk_widget_show (frame);

	contents_box = gtk_vbox_new (FALSE, 6);
	gtk_container_add (GTK_CONTAINER (frame), contents_box);
	gtk_widget_show (contents_box);

	check = gtk_check_button_new_with_mnemonic (_("Ask before removing _networks"));
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check),
								  prefs_get_show_alert (GNOMECHAT_ALERT_REMOVE_NETWORK));
	g_signal_connect (check, "toggled", G_CALLBACK (util_show_again_toggled_cb),
					  GUINT_TO_POINTER (GNOMECHAT_ALERT_REMOVE_NETWORK));
	gtk_box_pack_start (GTK_BOX (contents_box), check, FALSE, FALSE, 0);
	gtk_widget_show (check);

	check = gtk_check_button_new_with_mnemonic (_("Ask before removing _servers"));
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check),
								  prefs_get_show_alert (GNOMECHAT_ALERT_REMOVE_SERVER));
	g_signal_connect (check, "toggled", G_CALLBACK (util_show_again_toggled_cb),
					  GUINT_TO_POINTER (GNOMECHAT_ALERT_REMOVE_SERVER));
	gtk_box_pack_start (GTK_BOX (contents_box), check, FALSE, FALSE, 0);
	gtk_widget_show (check);

	check = gtk_check_button_new_with_mnemonic (_("Ask before _clearing chats"));
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check),
								  prefs_get_show_alert (GNOMECHAT_ALERT_CLEAR_CHAT));
	g_signal_connect (check, "toggled", G_CALLBACK (util_show_again_toggled_cb),
					  GUINT_TO_POINTER (GNOMECHAT_ALERT_CLEAR_CHAT));
	gtk_box_pack_start (GTK_BOX (contents_box), check, FALSE, FALSE, 0);
	gtk_widget_show (check);

	g_object_unref (label_group);
	g_object_unref (widget_group);

	/* Last line */
	gtk_window_present (GTK_WINDOW (dialog->win));
}
