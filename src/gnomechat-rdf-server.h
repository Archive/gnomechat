/*
 *  GnomeChat: src/gnomechat-rdf-server.h
 *
 *  Copyright (c) 2003 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. 01234567890
 */

#ifndef __GNOMECHAT_RDF_SERVER_H__
#define __GNOMECHAT_RDF_SERVER_H__

#include <glib-object.h>


#define GNOMECHAT_TYPE_RDF_SERVER			(gnomechat_rdf_server_get_type ())
#define GNOMECHAT_RDF_SERVER(obj)			(G_TYPE_CHECK_INSTANCE_CAST ((obj), GNOMECHAT_TYPE_RDF_SERVER, GnomechatRdfServer))
#define GNOMECHAT_IS_RDF_SERVER(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GNOMECHAT_TYPE_RDF_SERVER))
#define GNOMECHAT_RDF_SERVER_CLASS(obj)		(G_TYPE_CHECK_CLASS_CAST ((obj), GNOMECHAT_TYPE_RDF_SERVER, GnomechatRdfServerClass))
#define GNOMECHAT_IS_RDF_SERVER_CLASS(obj)	(G_TYPE_CHECK_CLASS_TYPE ((obj), GNOMECHAT_TYPE_RDF_SERVER))
#define GNOMECHAT_RDF_SERVER_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS ((obj), GNOMECHAT_TYPE_RDF_SERVER, GnomechatRdfServerClass))


typedef struct _GnomechatRdfServer GnomechatRdfServer;
typedef struct _GnomechatRdfServerPrivate GnomechatRdfServerPrivate;
typedef struct _GnomechatRdfServerClass GnomechatRdfServerClass;

struct _GnomechatRdfServer
{
	/*< private > */
	GObject parent;

	GnomechatRdfServerPrivate *_priv;
};

struct _GnomechatRdfServerClass
{
	GObjectClass parent_class;
};


GType gnomechat_rdf_server_get_type (void);

GObject *gnomechat_rdf_server_new (const gchar * uri);

G_CONST_RETURN gchar *gnomechat_rdf_server_get_uri (GnomechatRdfServer * server);
void gnomechat_rdf_server_set_uri (GnomechatRdfServer * server, const gchar * uri);

G_CONST_RETURN gchar *gnomechat_rdf_server_get_data_uri (GnomechatRdfServer * server);
void gnomechat_rdf_server_set_data_uri (GnomechatRdfServer * server, const gchar * data_uri);
G_CONST_RETURN gchar *gnomechat_rdf_server_get_data_path (GnomechatRdfServer * server);

G_CONST_RETURN gchar *gnomechat_rdf_server_get_name (GnomechatRdfServer * server);
void gnomechat_rdf_server_try_name (GnomechatRdfServer * server, const gchar * name,
									const gchar * lang);

G_CONST_RETURN gchar *gnomechat_rdf_server_get_encoding (GnomechatRdfServer * server);
void gnomechat_rdf_server_set_encoding (GnomechatRdfServer * server, const gchar * encoding);

GSList *gnomechat_rdf_server_get_ports (GnomechatRdfServer * server);
void gnomechat_rdf_server_append_port (GnomechatRdfServer * server, guint port);
void gnomechat_rdf_server_remove_port (GnomechatRdfServer * server, guint port);

#endif /* __GNOMECHAT_RDF_SERVER_H__ */
