/*
 *  GnomeChat: src/gnomechat-rdf-model.c
 *
 *  Copyright (c) 2003 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.01234567890
 */

/* Parser/TreeModel for IRC-RDF files */

#include "gnomechat.h"
#include "gnomechat-rdf-model.h"
#include "gnomechat-rdf-network.h"
#include "gnomechat-rdf-server.h"
#include "gnomechat-rdf-types.h"

#include "pixbufs.h"
#include "utils.h"
#include "stock-items.h"

#include <stdio.h>
#include <string.h>
#include <raptor.h>
#include <locale.h>

#include <libgircclient/girc-utils.h>

#include <gtk/gtkstock.h>
#include <libgnomevfs/gnome-vfs-uri.h>
#include <libgnomeui/gnome-thumbnail.h>


#define DEFAULT_GLOBAL_PATH		DATADIR "/gnomechat/networks.xml"
#define USER_GLOBAL_FILE		"networks.xml"
#define DEFAULT_BOOKMARKS_FILE	DATADIR "/gnomechat/user.xml"
#define USER_BOOKMARKS_FILE		"user.xml"


/* *************** *
 *  Private Types  *
 * *************** */

enum
{
	DATA_COLUMN,
	DATA_PROGRESS_COLUMN,
	ICON_PROGRESS_COLUMN,
	N_COLUMNS
};

enum
{
	PROP_0,
	FILENAME
};

enum
{
	PREDICATE_INVALID,
	PREDICATE_RDF_TYPE,
	PREDICATE_DC_TITLE,
	PREDICATE_DC_DESCRIPTION,
	PREDICATE_IRC_SERVER,
	PREDICATE_IRC_PORT,
	PREDICATE_IRC_NETWORK,
	PREDICATE_IRC_ENCODING,
	PREDICATE_IRC_LOCATION,
	PREDICATE_IRC_WEBSITE,
	PREDICATE_IRC_ICON,
	PREDICATE_LAST
};


struct _GnomechatRdfModelPrivate
{
	gchar *filename;
	raptor_parser *parser;

	/* Key = ID (gchar *),
	   Value = (GSList *) of (GnomechatI18nString *) data items */
	GHashTable *rdf_groups;

	/* Key = (gchar *) URI, value = (GnomechatIrcRdfData *) */
	GHashTable *data_items;
	GSList *data_to_load;
	GSList *messages;
};


/* ********************* *
 *  Classwide Variables  *
 * ********************* */

static gpointer parent_class = NULL;
static GHashTable *predicates = NULL;
static GtkTreeModel *bookmarks = NULL;


/* ********************* *
 *  Debugging Functions  *
 * ********************* */

static G_CONST_RETURN gchar *
DEBUG_get_raptor_identifier_type_name (raptor_identifier_type ident)
{
	const gchar *retval = "(Invalid Type)";

	switch (ident)
	{
	case RAPTOR_IDENTIFIER_TYPE_ANONYMOUS:
		retval = "Anonymous";
		break;
	case RAPTOR_IDENTIFIER_TYPE_LITERAL:
		retval = "Literal";
		break;
	case RAPTOR_IDENTIFIER_TYPE_ORDINAL:
		retval = "Ordinal";
		break;
	case RAPTOR_IDENTIFIER_TYPE_PREDICATE:
		retval = "Predicate";
		break;
	case RAPTOR_IDENTIFIER_TYPE_RESOURCE:
		retval = "Resource";
		break;
	case RAPTOR_IDENTIFIER_TYPE_XML_LITERAL:
		retval = "XML Literal";
		break;
	case RAPTOR_IDENTIFIER_TYPE_UNKNOWN:
		retval = "Unknown";
		break;
	}

	return retval;
}


/* ******************* *
 *  Utility Functions  *
 * ******************* */


static void
i18n_list_free (gpointer slist)
{
	girc_g_slist_deep_free (slist, (GFreeFunc) gnomechat_i18n_string_free);
}


static gboolean
search_for_data (GtkTreeModel * model, GtkTreePath * path, GtkTreeIter * iter, gpointer data[3])
{
	GObject *object = NULL;

	gtk_tree_model_get (model, iter, DATA_COLUMN, &object, -1);

	if (object == data[0])
	{
		GtkTreeIter *target = data[1];

		*target = *iter;

		data[2] = NULL + 1;
	}

	if (object != NULL)
		g_object_unref (object);

	return (data[2] != NULL);
}


static gboolean
get_iter_from_data (GnomechatRdfModel * rdf, GtkTreeIter * iter, GObject * data)
{
	gpointer search_data[3];

	search_data[0] = data;
	search_data[1] = iter;
	search_data[3] = NULL;

	gtk_tree_model_foreach (GTK_TREE_MODEL (rdf), (GtkTreeModelForeachFunc) search_for_data,
							search_data);

	return (search_data[3] != NULL);
}


static void
object_notify_cb (GObject * object, GParamSpec * pspec, GtkTreeStore * store)
{
	GtkTreeIter iter;

	if (get_iter_from_data (GNOMECHAT_RDF_MODEL (store), &iter, object))
	{
		gtk_tree_store_set (store, &iter, DATA_COLUMN, object, -1);
	}
}


static void
object_notify_data_path_cb (GObject * object, GParamSpec * pspec, GnomechatRdfModel * rdf)
{
	if (rdf->_priv->parser == NULL)
	{
		G_CONST_RETURN gchar *data_path = NULL;

		if (GNOMECHAT_IS_RDF_NETWORK (object))
			data_path = gnomechat_rdf_network_get_data_path ((GnomechatRdfNetwork *) object);
		else
			data_path = gnomechat_rdf_server_get_data_path ((GnomechatRdfServer *) object);

		gnomechat_rdf_model_append_file (rdf, data_path);
	}
	else if (g_slist_find (rdf->_priv->data_to_load, object) == NULL)
	{
		rdf->_priv->data_to_load = g_slist_append (rdf->_priv->data_to_load, g_object_ref (object));
	}
}


static GObject *
get_data_from_uri (GnomechatRdfModel * rdf, const gchar * uri)
{
	GObject *object = g_hash_table_lookup (rdf->_priv->data_items, uri);

	if (object == NULL)
	{
		gchar *name = NULL,
			*target = NULL;
		gboolean is_server = FALSE;

		sscanf (uri, "irc://%a[^/]/%as", &name, &target);

		if (name != NULL)
		{
			if (strchr (name, '.') != NULL)
			{
				is_server = TRUE;
			}
			else if (target != NULL)
			{
				is_server = (strstr (target, ",isserver") != NULL);
			}
		}

		if (is_server)
		{
			object = gnomechat_rdf_server_new (uri);
			g_hash_table_insert (rdf->_priv->data_items, g_strdup (uri), g_object_ref (object));
		}
		else
		{
			object = gnomechat_rdf_network_new (uri);
			gnomechat_rdf_model_append_network (rdf, (GnomechatRdfNetwork *) object);
		}

		g_free (name);
		g_free (target);
	}
	else
	{
		g_object_ref (object);
	}

	return object;
}


/* ********************* *
 *  LibRaptor Functions  *
 * ********************* */

static void
rdf_error_handler (gpointer data, raptor_locator * locator, const gchar * message)
{
//  GnomechatIrcRdf *rdf = (GnomechatRdfModel *) data;

	g_warning ("RDF error at line %d, column %d of %s: %s", locator->line,
			   locator->column, locator->file, message);
}


static void
rdf_warning_handler (gpointer data, raptor_locator * locator, const gchar * message)
{
//  GnomechatIrcRdf *rdf = (GnomechatRdfModel *) data;

	g_warning ("RDF warning at line %d, column %d of %s: %s", locator->line,
			   locator->column, locator->file, message);
}


/* Parse Tree */

/* Subject = Generated ID for the data,
   Predicate = URI for type of RDF list,
   Object = Exact list type */
static inline void
ntriple_anonymous_predicate_resource (GnomechatRdfModel * rdf, const raptor_statement * statement)
{
}


static inline void
ntriple_anonymous_predicate (GnomechatRdfModel * rdf, const raptor_statement * statement)
{
	switch (statement->object_type)
	{
	case RAPTOR_IDENTIFIER_TYPE_RESOURCE:
		ntriple_anonymous_predicate_resource (rdf, statement);
		break;

	default:
		g_warning (G_STRLOC ": Unhandled object type: %s",
				   DEBUG_get_raptor_identifier_type_name (statement->object_type));
		break;
	}
}


/*
 * Here, we always store the raw object string + locale, no matter what statement->object_type is.
 * We do this because libraptor doesn't tell us what a list item is for (irc:server, irc:icon,
 * irc:port, dc:Description, dc:Title, whatever) until after all the list items have been parsed.
 */
static inline void
ntriple_anonymous_ordinal (GnomechatRdfModel * rdf, const raptor_statement * statement)
{
	gpointer key;
	GSList *list = NULL;
	GnomechatI18nString *str = gnomechat_i18n_string_new (statement->object,
														  statement->object_literal_language);

	if (g_hash_table_lookup_extended (rdf->_priv->rdf_groups, statement->subject,
									  &key, (gpointer *) & list))
	{
		g_hash_table_steal (rdf->_priv->rdf_groups, key);
	}
	else
	{
		key = g_strdup (statement->subject);
	}

	list = g_slist_prepend (list, str);

	g_hash_table_insert (rdf->_priv->rdf_groups, key, list);
}


static inline void
ntriple_anonymous (GnomechatRdfModel * rdf, const raptor_statement * statement)
{
	switch (statement->predicate_type)
	{
	case RAPTOR_IDENTIFIER_TYPE_PREDICATE:
		ntriple_anonymous_predicate (rdf, statement);
		break;

	case RAPTOR_IDENTIFIER_TYPE_ORDINAL:
		ntriple_anonymous_ordinal (rdf, statement);
		break;

	default:
		g_warning (G_STRLOC ": Unhandled predicate type: %s",
				   DEBUG_get_raptor_identifier_type_name (statement->predicate_type));
		break;
	}
}


/*
 * Subject = URI being described,
 * Predicate = URI for type of data,
 * Object = Actual (gchar *) data
 *
 * Takes raw single-item string data (e.g. not an RDF list) and sets the appropriate struct member.
 */
static inline void
ntriple_resource_predicate_literal (GnomechatRdfModel * rdf, const raptor_statement * statement)
{
	GObject *item = get_data_from_uri (rdf, statement->subject);
	guint predicate_id = GPOINTER_TO_UINT (g_hash_table_lookup (predicates, statement->predicate));

	switch (predicate_id)
	{
	case PREDICATE_DC_TITLE:
		if (GNOMECHAT_IS_RDF_SERVER (item))
		{
			gnomechat_rdf_server_try_name ((GnomechatRdfServer *) item, statement->object,
										   statement->object_literal_language);
		}
		else
		{
			g_warning ("Got dc:Title for non-server item.");
		}
		break;

	case PREDICATE_DC_DESCRIPTION:
		if (GNOMECHAT_IS_RDF_NETWORK (item))
		{
			gnomechat_rdf_network_try_description ((GnomechatRdfNetwork *) item, statement->object,
												   statement->object_literal_language);
		}
		else
		{
			g_warning ("Got dc:description for non-network item.");
		}
		break;

	case PREDICATE_IRC_ENCODING:
		g_object_set (item, "encoding", statement->object, NULL);
		break;

	case PREDICATE_IRC_WEBSITE:
		if (GNOMECHAT_IS_RDF_NETWORK (item))
		{
			gnomechat_rdf_network_try_website ((GnomechatRdfNetwork *) item, statement->object,
											   statement->object_literal_language);
		}
		else
		{
			g_warning ("Got irc:website for non-network item.");
		}
		break;

	default:
		g_warning (G_STRLOC ": Unhandled predicate ID (URI): %d (%s)", predicate_id,
				   (const gchar *) statement->predicate);
		break;
	}

	g_object_unref (item);
}


/*
 * Subject = URI being described,
 * Predicate = URI for type of data,
 * Object = ID for list of data
 * 
 * Assigns an existing RDF List (rdf:Bag, rdf:Alt, etc.) stored in rdf->_priv->rdf_groups to an
 * item in the appropriate GnomechatIrcRdfData structure
 */
static inline void
ntriple_resource_predicate_anonymous (GnomechatRdfModel * rdf, const raptor_statement * statement)
{
	GObject *item = get_data_from_uri (rdf, (const gchar *) statement->subject);
	guint predicate_id = GPOINTER_TO_UINT (g_hash_table_lookup (predicates, statement->predicate));
	GSList *list = NULL;

	switch (predicate_id)
	{
	case PREDICATE_DC_DESCRIPTION:
		if (GNOMECHAT_IS_RDF_NETWORK (item))
		{
			for (list = g_hash_table_lookup (rdf->_priv->rdf_groups, statement->object);
				 list != NULL; list = list->next)
			{
				GnomechatI18nString *str = list->data;

				gnomechat_rdf_network_try_description ((GnomechatRdfNetwork *) item, str->str,
													   str->lang);
			}

			g_hash_table_remove (rdf->_priv->rdf_groups, statement->object);
		}
		else
		{
			g_warning ("Got list of dc:Description data for non-network item.");
		}
		break;

	case PREDICATE_DC_TITLE:
		if (GNOMECHAT_IS_RDF_SERVER (item))
		{
			for (list = g_hash_table_lookup (rdf->_priv->rdf_groups, statement->object);
				 list != NULL; list = list->next)
			{
				GnomechatI18nString *str = list->data;

				gnomechat_rdf_server_try_name ((GnomechatRdfServer *) item, str->str, str->lang);
			}

			g_hash_table_remove (rdf->_priv->rdf_groups, statement->object);
		}
		break;

	case PREDICATE_IRC_PORT:
		if (GNOMECHAT_IS_RDF_SERVER (item))
		{
			for (list = g_hash_table_lookup (rdf->_priv->rdf_groups, statement->object);
				 list != NULL; list = list->next)
			{
				GnomechatI18nString *str = list->data;
				guint port = 0;

				if (sscanf (str->str, "%u", &port) == 1)
				{
					gnomechat_rdf_server_append_port ((GnomechatRdfServer *) item, port);
				}
			}

			g_hash_table_remove (rdf->_priv->rdf_groups, statement->object);
		}
		else
		{
			g_warning ("Got list of irc:port data for a non-server item.");
		}
		break;

	case PREDICATE_IRC_SERVER:
		if (GNOMECHAT_IS_RDF_NETWORK (item))
		{
			GtkTreeIter parent;

			if (get_iter_from_data (rdf, &parent, item))
			{
				for (list = g_hash_table_lookup (rdf->_priv->rdf_groups, statement->object);
					 list != NULL; list = list->next)
				{
					GnomechatI18nString *str = list->data;
					GObject *server = get_data_from_uri (rdf, str->str);

					gnomechat_rdf_model_append_server (rdf, (GnomechatRdfServer *) server,
													   (GnomechatRdfNetwork *) item);

					g_object_unref (server);
				}
			}

			g_hash_table_remove (rdf->_priv->rdf_groups, statement->object);
		}
		else
		{
			g_warning ("Got list of irc:server data for a non-network item.");
		}
		break;

	case PREDICATE_IRC_WEBSITE:
		if (GNOMECHAT_IS_RDF_NETWORK (item))
		{
			for (list = g_hash_table_lookup (rdf->_priv->rdf_groups, statement->object);
				 list != NULL; list = list->next)
			{
				GnomechatI18nString *str = list->data;

				gnomechat_rdf_network_try_website ((GnomechatRdfNetwork *) item, str->str,
												   str->lang);
			}

			g_hash_table_remove (rdf->_priv->rdf_groups, statement->object);
		}
		else
		{
			g_warning ("Recieved list of irc:website for non-network item.");
		}
		break;

	default:
		g_warning (G_STRLOC ": Unhandled predicate ID (URI): %d (%s)",
				   predicate_id, (const gchar *) statement->predicate);
		break;
	}

	g_object_unref (item);
}


/* Subject = URI being described,
 * Predicate = URI for type of data,
 * Object = URI location for data
 * 
 * Handles URIs for external data/icon files.
 */
static inline void
ntriple_resource_predicate_resource (GnomechatRdfModel * rdf, const raptor_statement * statement)
{
	GObject *item = get_data_from_uri (rdf, (const gchar *) statement->subject);
	guint predicate_id = GPOINTER_TO_UINT (g_hash_table_lookup (predicates, statement->predicate));

	switch (predicate_id)
	{
	case PREDICATE_IRC_ICON:
		if (GNOMECHAT_IS_RDF_NETWORK (item))
		{
			gnomechat_rdf_network_set_icon_uri ((GnomechatRdfNetwork *) item, statement->object);
		}
		else
		{
			g_warning ("Recieved irc:icon for non-network item.");
		}
		break;

	case PREDICATE_IRC_NETWORK:
	case PREDICATE_IRC_SERVER:
		g_object_set (item, "data-uri", statement->object, NULL);
		break;

	default:
		g_warning (G_STRLOC ": Unhandled predicate ID (URI): %d (%s)",
				   predicate_id, (const gchar *) statement->predicate);
		break;
	}

	g_object_unref (item);
}


static inline void
ntriple_resource_predicate (GnomechatRdfModel * rdf, const raptor_statement * statement)
{
	switch (statement->object_type)
	{
	case RAPTOR_IDENTIFIER_TYPE_LITERAL:
		ntriple_resource_predicate_literal (rdf, statement);
		break;

	case RAPTOR_IDENTIFIER_TYPE_ANONYMOUS:
		ntriple_resource_predicate_anonymous (rdf, statement);
		break;

	case RAPTOR_IDENTIFIER_TYPE_RESOURCE:
		ntriple_resource_predicate_resource (rdf, statement);
		break;

	case RAPTOR_IDENTIFIER_TYPE_XML_LITERAL:
		/* Subject = URI being described,
		   Predicate = URI for type of data,
		   Object = Actual (gchar *) data, as an XML literal */

	default:
		g_warning (G_STRLOC ": Unhandled object type: %s",
				   DEBUG_get_raptor_identifier_type_name (statement->object_type));
		break;
	}
}


static inline void
ntriple_resource_ordinal (GnomechatRdfModel * rdf, const raptor_statement * statement)
{
	switch (statement->object_type)
	{
	default:
		g_warning (G_STRLOC ": Unhandled object type: %s",
				   DEBUG_get_raptor_identifier_type_name (statement->predicate_type));
		break;
	}
}


static inline void
ntriple_resource (GnomechatRdfModel * rdf, const raptor_statement * statement)
{
	switch (statement->predicate_type)
	{
	case RAPTOR_IDENTIFIER_TYPE_PREDICATE:
		ntriple_resource_predicate (rdf, statement);
		break;

	case RAPTOR_IDENTIFIER_TYPE_ORDINAL:
		ntriple_resource_ordinal (rdf, statement);
		break;

	default:
		g_warning (G_STRLOC ": Unhandled predicate type: %s",
				   DEBUG_get_raptor_identifier_type_name (statement->predicate_type));
		break;
	}
}


static void
rdf_statement_handler (gpointer data, const raptor_statement * statement)
{
	GnomechatRdfModel *rdf = (GnomechatRdfModel *) data;

	switch (statement->subject_type)
	{
	case RAPTOR_IDENTIFIER_TYPE_RESOURCE:
		/* Subject is a resource URI (rdf:about="resource") */
		ntriple_resource (rdf, statement);
		break;

	case RAPTOR_IDENTIFIER_TYPE_ANONYMOUS:
		/* Subject is "_:something" or a generated ID for an RDF list (rdf:Bag, rdf:Alt, etc.). */
		ntriple_anonymous (rdf, statement);
		break;

	default:
		g_warning (G_STRLOC ": Unhandled subject type: %s",
				   DEBUG_get_raptor_identifier_type_name (statement->subject_type));
		break;
	}
}


static inline void
init_parser (GnomechatRdfModel * rdf)
{
	if (rdf->_priv->parser == NULL)
	{
		rdf->_priv->parser = raptor_new_parser ("rdfxml");

		raptor_set_error_handler (rdf->_priv->parser, rdf, rdf_error_handler);
		raptor_set_warning_handler (rdf->_priv->parser, rdf, rdf_warning_handler);
		raptor_set_statement_handler (rdf->_priv->parser, rdf, rdf_statement_handler);
	}
}


static void
parse_file (GnomechatRdfModel * rdf, const gchar * filename)
{
	init_parser (rdf);

	if (g_file_test (filename, (G_FILE_TEST_EXISTS | G_FILE_TEST_IS_REGULAR)))
	{
		gchar *text_uri;
		raptor_uri *r_uri;

		text_uri = g_strdup_printf ("file://%s", filename);
		r_uri = raptor_new_uri (text_uri);
		g_free (text_uri);

		raptor_parse_uri (rdf->_priv->parser, r_uri, NULL);
		raptor_free_uri (r_uri);
	}

	for (; rdf->_priv->data_to_load != NULL;
		 rdf->_priv->data_to_load = g_slist_remove_link (rdf->_priv->data_to_load,
														 rdf->_priv->data_to_load))
	{
		if (GNOMECHAT_IS_RDF_NETWORK (rdf->_priv->data_to_load->data))
		{
			parse_file (rdf, gnomechat_rdf_network_get_data_path
						((GnomechatRdfNetwork *) (rdf->_priv->data_to_load->data)));
		}
		else
		{
			parse_file (rdf, gnomechat_rdf_server_get_data_path
						((GnomechatRdfServer *) (rdf->_priv->data_to_load->data)));
		}

		g_object_unref (rdf->_priv->data_to_load->data);
	}

	raptor_free_parser (rdf->_priv->parser);
	rdf->_priv->parser = NULL;
}


/* ******************* *
 *  GObject Functions  *
 * ******************* */

/* Object VTable */
static void
gnomechat_rdf_model_set_property (GObject * object, guint param_id, const GValue * value,
								  GParamSpec * pspec)
{
	GnomechatRdfModel *rdf = GNOMECHAT_RDF_MODEL (object);

	switch (param_id)
	{
	case FILENAME:
		g_free (rdf->_priv->filename);
		rdf->_priv->filename = g_value_dup_string (value);

		parse_file (rdf, rdf->_priv->filename);

		g_object_notify (object, "filename");
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}


static void
gnomechat_rdf_model_get_property (GObject * object, guint param_id, GValue * value,
								  GParamSpec * pspec)
{
	GnomechatRdfModel *rdf = GNOMECHAT_RDF_MODEL (object);

	switch (param_id)
	{
	case FILENAME:
		g_value_set_string (value, rdf->_priv->filename);
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}


static void
gnomechat_rdf_model_dispose (GObject * object)
{
	g_hash_table_destroy (GNOMECHAT_RDF_MODEL (object)->_priv->data_items);

	if (G_OBJECT_CLASS (parent_class)->dispose != NULL)
		(*G_OBJECT_CLASS (parent_class)->dispose) (object);
}


static void
gnomechat_rdf_model_finalize (GObject * object)
{
	GnomechatRdfModel *rdf = GNOMECHAT_RDF_MODEL (object);

	if (rdf->_priv->parser != NULL)
		raptor_free_parser (rdf->_priv->parser);

	g_hash_table_destroy (rdf->_priv->rdf_groups);
	g_free (rdf->_priv->filename);

	g_free (rdf->_priv);

	if (G_OBJECT_CLASS (parent_class)->finalize != NULL)
		(*G_OBJECT_CLASS (parent_class)->finalize) (object);
}


/* ***************** *
 *  GType Functions  *
 * ***************** */

static void
gnomechat_rdf_model_class_init (GnomechatRdfModelClass * class)
{
	GObjectClass *object_class = G_OBJECT_CLASS (class);
	gchar *default_filename = g_build_filename (g_get_home_dir (), gnomechat_get_private_dir (),
												USER_BOOKMARKS_FILE, NULL);

	parent_class = g_type_class_peek_parent (class);

	object_class->set_property = gnomechat_rdf_model_set_property;
	object_class->get_property = gnomechat_rdf_model_get_property;
	object_class->dispose = gnomechat_rdf_model_dispose;
	object_class->finalize = gnomechat_rdf_model_finalize;

	g_object_class_install_property (object_class, FILENAME,
									 g_param_spec_string ("filename",
														  "Target Filename",
														  "The filename which this object saves to.",
														  default_filename,
														  (G_PARAM_READWRITE | G_PARAM_CONSTRUCT)));

	g_free (default_filename);
}


static void
gnomechat_rdf_model_instance_init (GnomechatRdfModel * rdf)
{
	GType types[] = {
		G_TYPE_OBJECT,
		G_TYPE_FLOAT,
		G_TYPE_FLOAT
	};

	rdf->_priv = g_new0 (GnomechatRdfModelPrivate, 1);

	gtk_tree_store_set_column_types (GTK_TREE_STORE (rdf), N_COLUMNS, types);

	rdf->_priv->rdf_groups = g_hash_table_new_full (g_str_hash, g_str_equal, g_free,
													i18n_list_free);

	rdf->_priv->data_items = g_hash_table_new_full (g_str_hash, g_str_equal, g_free,
													g_object_unref);
}


static void
gnomechat_rdf_model_base_init (gpointer data)
{
	static const gchar *const predicate_uris[] = {
		/* RDF Namespace                       (rdf:____) */
		"http://www.w3.org/1999/02/22-rdf-syntax-ns#type",

		/* Dublin Core Namespace         (dc:___________) */
		"http://www.purl.org/dc/elements/1.1#Title",
		"http://www.purl.org/dc/elements/1.1#Description",

		/* IRC-RDF Namespace                   (irc:________) */
		"http://ignore-your.tv/irc-rdf/elements/1.0#server",
		"http://ignore-your.tv/irc-rdf/elements/1.0#port",
		"http://ignore-your.tv/irc-rdf/elements/1.0#network",
		"http://ignore-your.tv/irc-rdf/elements/1.0#encoding",
		"http://ignore-your.tv/irc-rdf/elements/1.0#location",
		"http://ignore-your.tv/irc-rdf/elements/1.0#website",
		"http://ignore-your.tv/irc-rdf/elements/1.0#icon"
	};
	guint i;

	raptor_init ();

	predicates = g_hash_table_new (g_str_hash, g_str_equal);

	for (i = 1; i <= G_N_ELEMENTS (predicate_uris); i++)
	{
		g_hash_table_insert (predicates, (gpointer) predicate_uris[i - 1], GUINT_TO_POINTER (i));
	}
}


/* base_finalize is only called when a module is unloaded */
//static void
//gnomechat_rdf_model_base_finalize (gpointer data)
//{
//  raptor_finish ();
//
//  g_hash_table_destroy (predicates);
//}


/* ************ *
 *  Public API  *
 * ************ */

GType
gnomechat_rdf_model_get_type (void)
{
	static GType type = G_TYPE_INVALID;

	if (type == G_TYPE_INVALID)
	{
		static const GTypeInfo info = {
			sizeof (GnomechatRdfModelClass),
			gnomechat_rdf_model_base_init,
			NULL,				/* base_finalize */
			(GClassInitFunc) gnomechat_rdf_model_class_init,
			NULL,				/* class_finalize */
			NULL,				/* class_data */
			sizeof (GnomechatRdfModel),
			0,					/* n_preallocs */
			(GInstanceInitFunc) gnomechat_rdf_model_instance_init,
		};

		type = g_type_register_static (GTK_TYPE_TREE_STORE, "GnomechatRdfModel", &info, 0);
	}

	return type;
}


GtkTreeModel *
gnomechat_rdf_model_get_bookmarks (void)
{
	if (bookmarks == NULL)
	{
		gchar *filename = NULL;

		bookmarks = g_object_new (GNOMECHAT_TYPE_RDF_MODEL, NULL);
		g_object_add_weak_pointer (G_OBJECT (bookmarks), (gpointer *) & bookmarks);

		filename = ((GnomechatRdfModel *) bookmarks)->_priv->filename;

		if (!g_file_test (filename, (G_FILE_TEST_EXISTS | G_FILE_TEST_IS_REGULAR)))
		{
			gnomechat_rdf_model_append_file (GNOMECHAT_RDF_MODEL (bookmarks), DEFAULT_GLOBAL_PATH);
		}
	}
	else
	{
		g_object_ref (bookmarks);
	}

	return bookmarks;
}


void
gnomechat_rdf_model_get_global (GFunc callback)
{
	g_warning (G_STRLOC ": FIXME: Implement getting global model (requires checking for updates).");
}


void
gnomechat_rdf_model_save (GnomechatRdfModel * rdf)
{
	g_return_if_fail (GNOMECHAT_IS_RDF_MODEL (rdf));

	g_warning (G_STRLOC ": FIXME: Implement saving RDF data.");
}


void
gnomechat_rdf_model_append_file (GnomechatRdfModel * rdf, const gchar * filename)
{
	g_return_if_fail (GNOMECHAT_IS_RDF_MODEL (rdf));
	g_return_if_fail (filename != NULL && filename[0] != '\0');
	g_return_if_fail (g_file_test (filename, G_FILE_TEST_EXISTS));

	parse_file (rdf, filename);
}


GObject *
gnomechat_rdf_model_get_data (GnomechatRdfModel * rdf, GtkTreeIter * iter)
{
	GObject *data;

	g_return_val_if_fail (GNOMECHAT_IS_RDF_MODEL (rdf), NULL);

	gtk_tree_model_get ((GtkTreeModel *) rdf, iter, DATA_COLUMN, &data, -1);

	return data;
}


GObject *
gnomechat_rdf_model_get_data_from_uri (GnomechatRdfModel * rdf, const gchar * uri)
{
	GObject *object;

	g_return_val_if_fail (GNOMECHAT_IS_RDF_MODEL (rdf), NULL);
	g_return_val_if_fail (uri != NULL && uri[0] != '\0', NULL);

	object = g_hash_table_lookup (rdf->_priv->data_items, uri);

	return g_object_ref (object);
}


gboolean
gnomechat_rdf_model_get_iter_from_uri (GnomechatRdfModel * rdf, GtkTreeIter * iter,
									   const gchar * uri)
{
	GObject *data = NULL;

	g_return_val_if_fail (GNOMECHAT_IS_RDF_MODEL (rdf), FALSE);
	g_return_val_if_fail (iter != NULL, FALSE);
	g_return_val_if_fail (uri != NULL && uri[0] != '\0', FALSE);

	data = g_hash_table_lookup (rdf->_priv->data_items, uri);

	if (data == NULL)
		return FALSE;

	return get_iter_from_data (rdf, iter, data);
}


gboolean
gnomechat_rdf_model_get_iter_from_data (GnomechatRdfModel * rdf, GtkTreeIter * iter, GObject * data)
{
	g_return_val_if_fail (GNOMECHAT_IS_RDF_MODEL (rdf), FALSE);
	g_return_val_if_fail (iter != NULL, FALSE);
	g_return_val_if_fail (GNOMECHAT_IS_RDF_NETWORK (data) || GNOMECHAT_IS_RDF_SERVER (data), FALSE);

	return get_iter_from_data (rdf, iter, data);
}


void
gnomechat_rdf_model_remove (GnomechatRdfModel * rdf, GtkTreeIter * iter)
{
	GObject *object = NULL;

	g_return_if_fail (GNOMECHAT_IS_RDF_MODEL (rdf));
	g_return_if_fail (iter != NULL);

	gtk_tree_model_get (GTK_TREE_MODEL (rdf), iter, DATA_COLUMN, &object, -1);

	g_return_if_fail (object != NULL);

	g_signal_handlers_disconnect_by_func (object, object_notify_cb, rdf);
	g_signal_handlers_disconnect_by_func (object, object_notify_data_path_cb, rdf);

	if (GNOMECHAT_IS_RDF_NETWORK (object))
	{
		g_hash_table_remove (rdf->_priv->data_items,
							 gnomechat_rdf_network_get_uri ((GnomechatRdfNetwork *) object));
	}
	else
	{
		g_hash_table_remove (rdf->_priv->data_items,
							 gnomechat_rdf_server_get_uri ((GnomechatRdfServer *) object));
	}

	gtk_tree_store_remove (GTK_TREE_STORE (rdf), iter);

	g_object_unref (object);
}


void
gnomechat_rdf_model_remove_network (GnomechatRdfModel * rdf, GnomechatRdfNetwork * network)
{
	GtkTreeIter iter;

	g_return_if_fail (GNOMECHAT_IS_RDF_MODEL (rdf));
	g_return_if_fail (GNOMECHAT_IS_RDF_NETWORK (network));

	g_hash_table_remove (rdf->_priv->data_items, gnomechat_rdf_network_get_uri (network));

	g_signal_handlers_disconnect_by_func (network, object_notify_cb, rdf);
	g_signal_handlers_disconnect_by_func (network, object_notify_data_path_cb, rdf);

	/* Make sure the network is actually in the GtkTreeModel */
	g_return_if_fail (get_iter_from_data (rdf, &iter, (GObject *) network));

	gtk_tree_store_remove (GTK_TREE_STORE (rdf), &iter);
}


void
gnomechat_rdf_model_append_network (GnomechatRdfModel * rdf, GnomechatRdfNetwork * network)
{
	GtkTreeIter iter;

	g_return_if_fail (GNOMECHAT_IS_RDF_MODEL (rdf));
	g_return_if_fail (GNOMECHAT_IS_RDF_NETWORK (network));

	/* Make sure the network is not already in the GtkTreeModel */
	if (get_iter_from_data (rdf, &iter, (GObject *) network))
		return;

	gtk_tree_store_append (GTK_TREE_STORE (rdf), &iter, NULL);
	gtk_tree_store_set (GTK_TREE_STORE (rdf), &iter, DATA_COLUMN, network, -1);
	g_hash_table_insert (rdf->_priv->data_items, g_strdup (gnomechat_rdf_network_get_uri (network)),
						 g_object_ref (network));

	g_signal_connect (network, "notify::icon", G_CALLBACK (object_notify_cb), rdf);
	g_signal_connect (network, "notify::uri", G_CALLBACK (object_notify_cb), rdf);
	g_signal_connect (network, "notify::data-path", G_CALLBACK (object_notify_data_path_cb), rdf);
}


void
gnomechat_rdf_model_append_server (GnomechatRdfModel * rdf, GnomechatRdfServer * server,
								   GnomechatRdfNetwork * network)
{
	G_CONST_RETURN gchar *uri;
	GtkTreeIter parent,
	  iter;

	g_return_if_fail (GNOMECHAT_IS_RDF_MODEL (rdf));
	g_return_if_fail (GNOMECHAT_IS_RDF_SERVER (server));
	g_return_if_fail (GNOMECHAT_IS_RDF_NETWORK (network));
	/* Make sure the server isn't already in the GtkTreeModel */
	g_return_if_fail (!get_iter_from_data (rdf, &iter, (GObject *) server));
	/* ...and make sure the network is */
	g_return_if_fail (get_iter_from_data (rdf, &parent, (GObject *) network));

	gnomechat_rdf_network_append_server (network, server);

	uri = gnomechat_rdf_server_get_uri (server);

	if (g_hash_table_lookup (rdf->_priv->data_items, uri) == NULL)
		g_hash_table_insert (rdf->_priv->data_items, g_strdup (uri), g_object_ref (server));

	gtk_tree_store_append (GTK_TREE_STORE (rdf), &iter, &parent);
	gtk_tree_store_set (GTK_TREE_STORE (rdf), &iter, DATA_COLUMN, server, -1);

	g_signal_connect (server, "notify::uri", G_CALLBACK (object_notify_cb), rdf);
	g_signal_connect (server, "notify::data-path", G_CALLBACK (object_notify_data_path_cb), rdf);
}


void
gnomechat_rdf_model_render_icon (GtkTreeViewColumn * col, GtkCellRenderer * cell,
								 GtkTreeModel * tree_model, GtkTreeIter * iter, gpointer user_data)
{
	GObject *data = NULL;
	GdkPixbuf *pixbuf = NULL;
	GtkIconSize size;
	const gchar *stock_id = NULL;

	g_return_if_fail (GTK_IS_TREE_MODEL (tree_model));

	gtk_tree_model_get (tree_model, iter, DATA_COLUMN, &data, -1);
	g_object_get (cell, "stock-size", &size, NULL);

	if (data != NULL)
	{
		if (GNOMECHAT_IS_RDF_NETWORK (data))
		{
			pixbuf = gnomechat_rdf_network_get_icon ((GnomechatRdfNetwork *) data);

			if (pixbuf != NULL)
				pixbuf = pixbuf_scale_to_stock_size (pixbuf, size);
			else
				stock_id = GNOMECHAT_STOCK_NETWORK;
		}
		else
		{
			stock_id = GNOMECHAT_STOCK_SERVER;
		}
	}
	else
	{
		stock_id = GTK_STOCK_DIALOG_ERROR;
	}

	g_object_unref (data);

	g_object_set (cell, "stock-id", stock_id, "pixbuf", pixbuf, NULL);

	if (pixbuf != NULL)
		g_object_unref (pixbuf);
}


void
gnomechat_rdf_model_render_name (GtkTreeViewColumn * col, GtkCellRenderer * cell,
								 GtkTreeModel * tree_model, GtkTreeIter * iter, gpointer user_data)
{
	GObject *data = NULL;

	g_return_if_fail (GTK_IS_TREE_MODEL (tree_model));

	gtk_tree_model_get (tree_model, iter, DATA_COLUMN, &data, -1);

	if (data != NULL)
	{
		gchar *name = NULL;

		if (GNOMECHAT_IS_RDF_NETWORK (data))
		{
			GnomechatRdfNetwork *network = (GnomechatRdfNetwork *) data;

			name = util_get_name_from_uri (gnomechat_rdf_network_get_uri (network));
		}
		else
		{
			GnomechatRdfServer *server = (GnomechatRdfServer *) data;

			name = g_strdup (gnomechat_rdf_server_get_name (server));

			if (name == NULL)
			{
				name = util_get_name_from_uri (gnomechat_rdf_server_get_uri (server));
			}
		}

		g_object_set (cell, "text", name, NULL);

		g_free (name);
		g_object_unref (data);
	}
	else
	{
		g_object_set (cell, "text", _("(Unknown)"), NULL);
	}
}
