/*
 *  GnomeChat: src/tray-icon.c
 *
 *  Copyright (c) 2003 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "tray-icon.h"
#include "eggtrayicon.h"

#include "gnomechat.h"
#include "stock-items.h"
#include "esco-chats-menu.h"
#include "utils.h"

#include <gtk/gtkeventbox.h>
#include <gtk/gtkimage.h>


typedef struct
{
	GtkWidget *icon;
	GtkWidget *menu;
	GtkWidget *img;

	EscoPesterType level;
}
TrayIcon;

static TrayIcon *icon = NULL;


static void do_tray_init (void);


static gboolean
icon_destroy_event_cb (GtkWidget * widget,
					   GdkEvent * event,
					   gpointer data)
{
	if (icon != NULL)
		do_tray_init ();

	return FALSE;
}


static void
position_chats_menu (GtkMenu * menu,
					 gint * x,
					 gint * y,
					 gboolean * push_in,
					 GdkRectangle * rect)
{
	*x = rect->x;
	*y = rect->y;
	*push_in = TRUE;
}


static gboolean
evbox_button_press_event_cb (GtkWidget * widget,
							 GdkEventButton * event,
							 gpointer data)
{
	gtk_widget_set_state (GTK_WIDGET (icon->icon), GTK_STATE_ACTIVE);

	switch (event->button)
	{
	case 1:
	case 3:
		{
			GdkRectangle rect = { 0 };

			rect.x = (gint) event->x_root - event->x;
			rect.y = (gint) event->y_root - event->y + widget->allocation.height;

			gtk_menu_popup (GTK_MENU (icon->menu), NULL, NULL,
							(GtkMenuPositionFunc) position_chats_menu,
							&rect, event->button, event->time);
		}
		break;

	case 2:
		break;

	default:
		break;
	}

	return FALSE;
}


static gboolean
evbox_button_release_event_cb (GtkWidget * widget,
							   GdkEventButton * event,
							   gpointer data)
{
	switch (event->button)
	{
	case 1:
	case 3:
		gtk_menu_popdown (GTK_MENU (icon->menu));
		break;

	case 2:
		break;

	default:
		break;
	}

	gtk_widget_set_state (icon->icon, GTK_STATE_NORMAL);

	return FALSE;
}


static void
do_tray_init (void)
{
	GtkWidget *evbox;

	icon->icon = GTK_WIDGET (egg_tray_icon_new (_("IRC Chats")));
	util_set_tooltip (icon->icon, _("IRC Chats"), NULL, TRUE);
	g_signal_connect (icon->icon, "destroy-event", G_CALLBACK (icon_destroy_event_cb),
					  NULL);
	gtk_widget_show (icon->icon);

	evbox = gtk_event_box_new ();
	g_signal_connect (evbox, "button-press-event",
					  G_CALLBACK (evbox_button_press_event_cb), NULL);
	g_signal_connect (evbox, "button-release-event",
					  G_CALLBACK (evbox_button_release_event_cb), NULL);
	gtk_container_add (GTK_CONTAINER (icon->icon), evbox);
	gtk_widget_show (evbox);

	icon->img = gtk_image_new_from_stock (GNOMECHAT_STOCK_ICON, GTK_ICON_SIZE_MENU);
	gtk_container_add (GTK_CONTAINER (evbox), icon->img);
	gtk_widget_show (icon->img);
}


static void
menu_pester_changed_cb (EscoChatsMenu * menu,
						EscoPesterType level,
						gpointer data)
{
	if (icon == NULL)
		return;

	switch (level)
	{
	case ESCO_PESTER_IRC:
	case ESCO_PESTER_NONE:
		gtk_image_set_from_stock (GTK_IMAGE (icon->img), GNOMECHAT_STOCK_ICON,
								  GTK_ICON_SIZE_MENU);
		break;

	case ESCO_PESTER_TEXT:
		gtk_image_set_from_stock (GTK_IMAGE (icon->img), GNOMECHAT_STOCK_CHANNEL,
								  GTK_ICON_SIZE_MENU);
		break;

	case ESCO_PESTER_MSG:
		gtk_image_set_from_stock (GTK_IMAGE (icon->img), GNOMECHAT_STOCK_QUERY,
								  GTK_ICON_SIZE_MENU);
		break;

	default:
		g_return_if_reached ();
		break;
	}
}


static void
menu_selection_done_cb (GtkWidget * menu,
						gpointer data)
{
	gtk_widget_set_state (icon->icon, GTK_STATE_NORMAL);
}


void
tray_icon_init (void)
{
	if (icon != NULL)
		return;

	icon = g_new0 (TrayIcon, 1);

	do_tray_init ();

	icon->menu = esco_chats_menu_new ();
	g_signal_connect (icon->menu, "pester", G_CALLBACK (menu_pester_changed_cb), NULL);
	g_signal_connect (icon->menu, "selection-done", G_CALLBACK (menu_selection_done_cb),
					  NULL);
	gtk_widget_show (icon->menu);
}


void
tray_icon_shutdown (void)
{
	TrayIcon *tmp_icon = icon;

	if (tmp_icon == NULL)
		return;

	icon = NULL;

	gtk_widget_destroy (tmp_icon->menu);
	gtk_widget_destroy (GTK_WIDGET (tmp_icon->icon));
	g_free (tmp_icon);
}
