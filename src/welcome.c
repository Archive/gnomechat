/*
 *  GnomeChat: src/welcome.c
 *
 *  Copyright (c) 2002 James M. Cape.
 *  All Rights Reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "gnomechat.h"

#include "connect-ui.h"
#include "pixbufs.h"
#include "prefs.h"
#include "prefs-ui-utils.h"
#include "stock-items.h"
#include "utils.h"
#include "windows.h"

#include <gtk/gtkentry.h>
#include <gtk/gtkhbox.h>
#include <gtk/gtklabel.h>
#include <gtk/gtkmessagedialog.h>
#include <gtk/gtkoptionmenu.h>
#include <gtk/gtkradiobutton.h>
#include <gtk/gtkstock.h>

#include <libgnomeui/gnome-druid.h>
#include <libgnomeui/gnome-druid-page-standard.h>

/* For mkdir() & creat() */
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>


#define MAX_NAME_LEN 64
#define MAX_NICK_LEN 9

#define IRCNAME_ENV_VAR "IRCNAME"
#define IRCNICK_ENV_VAR "IRCNICK"


typedef struct
{
	GtkWidget *window;
	GtkWidget *druid;

	GtkWidget *start_page;
	GtkWidget *id_page;
	GtkWidget *server_page;
	GtkWidget *last_page;

	GtkWidget *name_entry;
	GtkWidget *nick_entry;
/*
	GtkWidget *vcard_button;
	GtkWidget *vcard_label;
*/

	GtkWidget *none_radio;
	GtkWidget *existing_radio;
	GtkWidget *custom_radio;
	GtkWidget *optmenu;

	GtkWidget *custom_server_entry;

	gchar *vcard;
	gchar *net_id;
}
WelcomeWin;

/*
typedef struct
{
	GtkWidget *label;
	GtkWidget *icon;
	GtkWidget *file_entry;

	gchar *vcard;

	WelcomeWin *win;
}
WelcomeVCardWin;
*/


/* Utility Functions */
static gboolean
ok_to_cancel_druid (WelcomeWin * win)
{
	static GtkWidget *dialog = NULL;
	gint response;
	gchar *str;

	if (dialog)
	{
		gtk_window_present (GTK_WINDOW (dialog));
		return FALSE;
	}

	str = g_strdup_printf ("<b>%s</b>%s\n", _("Quit Setup?"),
						   _("If you quit before completing the initial setup, you will have to "
							 "complete it later."));
	dialog = gtk_message_dialog_new (GTK_WINDOW (win->window),
									 (GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT),
									 GTK_MESSAGE_QUESTION, GTK_BUTTONS_NONE, str);
	g_free (str);

	str = g_strconcat (_("Quit Setup?"), " - ", genericname, NULL);
	gtk_window_set_title (GTK_WINDOW (dialog), str);
	g_free (str);
	gtk_window_set_role (GTK_WINDOW (dialog), "QuitSetup");
	util_set_window_icon_from_stock (GTK_WINDOW (dialog), GTK_STOCK_DIALOG_QUESTION);
	gtk_label_set_use_markup (GTK_LABEL (GTK_MESSAGE_DIALOG (dialog)->label), TRUE);
	gtk_dialog_add_buttons (GTK_DIALOG (dialog), GTK_STOCK_CANCEL, FALSE, GTK_STOCK_QUIT, TRUE,
							NULL);

	response = gtk_dialog_run (GTK_DIALOG (dialog));

	if (response)
	{
		gtk_widget_destroy (win->window);
		g_free (win);
		gnomechat_quit ();
		return TRUE;
	}
	else
	{
		gtk_widget_destroy (dialog);
		dialog = NULL;
		return FALSE;
	}
}


/* Callbacks */
static void
name_entry_changed (GtkEntry * entry,
					gpointer data)
{
	WelcomeWin *win = data;

	if (util_str_is_not_empty (gtk_entry_get_text (entry)))
	{
		gtk_widget_set_sensitive (GNOME_DRUID (win->druid)->next, TRUE);
		gtk_widget_set_sensitive (GNOME_DRUID (win->druid)->back, TRUE);
	}
	else
	{
		gtk_widget_set_sensitive (GNOME_DRUID (win->druid)->next, FALSE);
		gtk_widget_set_sensitive (GNOME_DRUID (win->druid)->back, FALSE);
	}
}


static void
nick_entry_changed (GtkEntry * entry,
					gpointer data)
{
	WelcomeWin *win = data;

	if (util_str_is_not_empty (gtk_entry_get_text (entry)))
	{
		gtk_widget_set_sensitive (GNOME_DRUID (win->druid)->next, TRUE);
		gtk_widget_set_sensitive (GNOME_DRUID (win->druid)->back, TRUE);
	}
	else
	{
		gtk_widget_set_sensitive (GNOME_DRUID (win->druid)->next, FALSE);
		gtk_widget_set_sensitive (GNOME_DRUID (win->druid)->back, FALSE);
	}
}


static void
none_radio_toggled (GtkToggleButton * button,
					gpointer data)
{
	WelcomeWin *win = data;

	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (win->none_radio)))
	{
		gtk_widget_set_sensitive (win->optmenu, FALSE);
		gtk_widget_set_sensitive (win->custom_server_entry, FALSE);
		gtk_widget_set_sensitive (GNOME_DRUID (win->druid)->next, TRUE);
		gtk_widget_set_sensitive (GNOME_DRUID (win->druid)->back, TRUE);
	}
	else if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (win->existing_radio)))
	{
		gtk_widget_set_sensitive (win->optmenu, TRUE);
		gtk_widget_set_sensitive (win->custom_server_entry, FALSE);
		gtk_widget_set_sensitive (GNOME_DRUID (win->druid)->next, TRUE);
		gtk_widget_set_sensitive (GNOME_DRUID (win->druid)->back, TRUE);
	}
	else
	{
		gtk_widget_set_sensitive (win->optmenu, FALSE);
		gtk_widget_set_sensitive (win->custom_server_entry, TRUE);
		if (util_str_is_not_empty (gtk_entry_get_text (GTK_ENTRY (win->custom_server_entry))))
		{
			gtk_widget_set_sensitive (GNOME_DRUID (win->druid)->next, TRUE);
			gtk_widget_set_sensitive (GNOME_DRUID (win->druid)->back, TRUE);
		}
		else
		{
			gtk_widget_set_sensitive (GNOME_DRUID (win->druid)->next, FALSE);
			gtk_widget_set_sensitive (GNOME_DRUID (win->druid)->back, FALSE);
		}
		gtk_window_set_focus (GTK_WINDOW (win->window), win->custom_server_entry);
	}
}


static void
custom_server_entry_changed (GtkEntry * entry,
							 gpointer data)
{
	WelcomeWin *win = data;

	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (win->custom_radio)))
	{
		if (util_str_is_not_empty (gtk_entry_get_text (entry)))
		{
			gtk_widget_set_sensitive (GNOME_DRUID (win->druid)->next, TRUE);
			gtk_widget_set_sensitive (GNOME_DRUID (win->druid)->back, TRUE);
		}
		else
		{
			gtk_widget_set_sensitive (GNOME_DRUID (win->druid)->next, FALSE);
			gtk_widget_set_sensitive (GNOME_DRUID (win->druid)->back, FALSE);
		}
	}
}


static void
net_optmenu_changed_cb (GtkOptionMenu * optmenu,
						gchar ** net_id)
{
	gchar *tmp;

	tmp = (gchar *) g_object_get_qdata (G_OBJECT (optmenu->menu_item), PREFS_NETWORK_ID_KEY);

	if (*net_id != NULL)
		g_free (*net_id);

	*net_id = g_strdup (tmp);
}


static gboolean
window_delete_event (GtkWidget * window,
					 GdkEvent * event,
					 gpointer data)
{
	return !(ok_to_cancel_druid (data));
}


static void
prepare_start_page (GnomeDruidPage * page,
					GtkWidget * druid,
					gpointer data)
{
	WelcomeWin *win = data;

	gnome_druid_set_buttons_sensitive (GNOME_DRUID (druid), FALSE, TRUE, TRUE, TRUE);
	gnome_druid_set_show_finish (GNOME_DRUID (druid), FALSE);

	gtk_window_set_default (GTK_WINDOW (win->window), GNOME_DRUID (druid)->next);
	gtk_window_set_focus (GTK_WINDOW (win->window), GNOME_DRUID (druid)->next);
}


static void
prepare_id_page (GnomeDruidPage * page,
				 GtkWidget * druid,
				 gpointer data)
{
	WelcomeWin *win = data;

	gnome_druid_set_buttons_sensitive (GNOME_DRUID (druid), TRUE, TRUE, TRUE, TRUE);
	gnome_druid_set_show_finish (GNOME_DRUID (druid), FALSE);
	name_entry_changed (GTK_ENTRY (win->name_entry), win);
	nick_entry_changed (GTK_ENTRY (win->nick_entry), win);

	gtk_window_set_default (GTK_WINDOW (win->window), GNOME_DRUID (druid)->next);
	gtk_window_set_focus (GTK_WINDOW (win->window), win->nick_entry);
}


static void
prepare_server_page (GnomeDruidPage * page,
					 GtkWidget * druid,
					 gpointer data)
{
	WelcomeWin *win = data;

	gnome_druid_set_buttons_sensitive (GNOME_DRUID (druid), TRUE, TRUE, TRUE, TRUE);
	gnome_druid_set_show_finish (GNOME_DRUID (druid), FALSE);
	custom_server_entry_changed (GTK_ENTRY (win->custom_server_entry), win);

	gtk_window_set_default (GTK_WINDOW (win->window), GNOME_DRUID (druid)->next);
	gtk_window_set_focus (GTK_WINDOW (win->window), win->existing_radio);
}


static void
prepare_last_page (GnomeDruidPage * page,
				   GtkWidget * druid,
				   gpointer data)
{
	WelcomeWin *win = data;

	gnome_druid_set_buttons_sensitive (GNOME_DRUID (druid), TRUE, FALSE, TRUE, TRUE);
	gnome_druid_set_show_finish (GNOME_DRUID (druid), TRUE);

	gtk_window_set_default (GTK_WINDOW (win->window), GNOME_DRUID (druid)->finish);
	gtk_window_set_focus (GTK_WINDOW (win->window), GNOME_DRUID (druid)->finish);
}


static void
druid_finished (GtkWidget * finish_button,
				gpointer data)
{
	WelcomeWin *win = data;

	prefs_set_identity (gtk_entry_get_text (GTK_ENTRY (win->nick_entry)),
						gtk_entry_get_text (GTK_ENTRY (win->name_entry)), g_get_user_name (), NULL);
	prefs_set_first_run_completed (TRUE);

	windows_open_new_window ();

	if (win->net_id != NULL
		&& gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (win->existing_radio)))
	{
		connect_ui_open_progress_dialog (win->net_id, NULL);
		g_free (win->net_id);
	}

	gtk_widget_destroy (win->window);
	g_free (win);
}


static void
druid_canceled (GnomeDruid * druid,
				gpointer data)
{
	WelcomeWin *win = data;

	ok_to_cancel_druid (win);
}


void
welcome_open_window (void)
{
	WelcomeWin *win = NULL;

	GtkWidget *label,
	 *vbox1,
	 *hbox;
	const gchar *ircname,
	 *ircnick;
	gchar *title,
	 *str;
	GdkPixbuf *icon_pixbuf,
	 *pixbuf;

	win = g_new0 (WelcomeWin, 1);
	win->vcard = NULL;

	/* Window */
	win->window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	title = g_strconcat (_("Welcome to "), genericname, NULL);
	gtk_window_set_title (GTK_WINDOW (win->window), title);
	gtk_window_set_role (GTK_WINDOW (win->window), "Welcome");
	g_signal_connect (win->window, "delete-event", G_CALLBACK (window_delete_event), win);

	/* Druid */
	win->druid = gnome_druid_new ();
	g_signal_connect (win->druid, "cancel", G_CALLBACK (druid_canceled), win);
	g_signal_connect_after (GNOME_DRUID (win->druid)->finish,
							"clicked", G_CALLBACK (druid_finished), win);
	gtk_container_add (GTK_CONTAINER (win->window), win->druid);
	gtk_button_set_label (GTK_BUTTON (GNOME_DRUID (win->druid)->finish), GTK_STOCK_OK);

	/* FIXME: Make GDK_Escape activate "Cancel" button */

	pixbuf = pixbuf_load_file (GNOMECHAT_WATERMARK_FILE);

	/* Start Page */
	win->start_page = gnome_druid_page_standard_new_with_vals (title, NULL, pixbuf);
	icon_pixbuf = gtk_widget_render_icon (win->start_page, GNOMECHAT_STOCK_ICON,
										  GTK_ICON_SIZE_DIALOG, NULL);
	gnome_druid_page_standard_set_logo (GNOME_DRUID_PAGE_STANDARD (win->start_page), icon_pixbuf);
	g_object_unref (icon_pixbuf);
	gnome_druid_append_page (GNOME_DRUID (win->druid), GNOME_DRUID_PAGE (win->start_page));
	g_signal_connect_after (win->start_page, "prepare", G_CALLBACK (prepare_start_page), win);

	str = g_strdup_printf ("<b>%s</b>\n%s", _("Welcome To IRC Chat"),
						   _("IRC Chat is a program for chatting on IRC networks.\n\n"
							 "This assistant will help you configure IRC Chat and connect you to "
							 "an IRC server to chat.\n\n"
							 "To get started, just click \"Forward\"."));
	label = gtk_label_new (str);
	g_free (str);

	gtk_label_set_use_markup (GTK_LABEL (label), TRUE);
	gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
	gtk_container_add (GTK_CONTAINER (GNOME_DRUID_PAGE_STANDARD (win->start_page)->vbox), label);

	/* Identity Page */
	win->id_page = gnome_druid_page_standard_new_with_vals (_("Your Identity"), NULL, pixbuf);
	icon_pixbuf = gtk_widget_render_icon (win->id_page, GNOMECHAT_STOCK_MY_USER,
										  GTK_ICON_SIZE_DIALOG, NULL);
	gnome_druid_page_standard_set_logo (GNOME_DRUID_PAGE_STANDARD (win->id_page), icon_pixbuf);
	g_object_unref (icon_pixbuf);
	gnome_druid_append_page (GNOME_DRUID (win->druid), GNOME_DRUID_PAGE (win->id_page));
	g_signal_connect_after (win->id_page, "prepare", G_CALLBACK (prepare_id_page), win);

	win->nick_entry = gtk_entry_new ();
	ircnick = g_getenv (IRCNICK_ENV_VAR);
	if (ircnick == NULL)
		ircnick = g_get_user_name ();
	gtk_entry_set_text (GTK_ENTRY (win->nick_entry), ircnick);
	gtk_entry_set_max_length (GTK_ENTRY (win->nick_entry), MAX_NICK_LEN);
	g_signal_connect (win->nick_entry, "changed", G_CALLBACK (nick_entry_changed), win);
	gnome_druid_page_standard_append_item (GNOME_DRUID_PAGE_STANDARD (win->id_page),
										   _("What nickname do you want to _use?"), win->nick_entry,
										   _("Your nickname is what people on IRC will know you "
											 "by. It will appear before things\n" "you say."));

	win->name_entry = gtk_entry_new ();
	gtk_entry_set_max_length (GTK_ENTRY (win->name_entry), MAX_NAME_LEN);
	ircname = g_getenv (IRCNAME_ENV_VAR);
	if (ircname == NULL)
		ircname = g_get_real_name ();
	gtk_entry_set_text (GTK_ENTRY (win->name_entry), ircname);
	g_signal_connect (win->name_entry, "changed", G_CALLBACK (name_entry_changed), win);
	gnome_druid_page_standard_append_item (GNOME_DRUID_PAGE_STANDARD (win->id_page),
										   _("What is your _name?"), win->name_entry,
										   _("You do not have to use your real name if "
											 "you do not want to."));
/*
	hbox = gtk_hbox_new (FALSE, 4);
	gtk_box_pack_start (GTK_BOX
						(GNOME_DRUID_PAGE_STANDARD (win->id_page)->vbox), hbox,
						FALSE, FALSE, 0);

	win->vcard_label = gtk_label_new (NULL);
	gtk_misc_set_alignment (GTK_MISC (win->vcard_label), 1.0, 0.5);
	gtk_container_add (GTK_CONTAINER (hbox), win->vcard_label);


	win->vcard_button = gtk_button_new_with_mnemonic (_("_Business Card..."));
	g_signal_connect (win->vcard_button, "clicked",
					  G_CALLBACK (vcard_button_clicked_cb), win);
	gtk_box_pack_start (GTK_BOX (hbox), win->vcard_button, FALSE, FALSE, 0);
*/

	/* Server Page */
	win->server_page = gnome_druid_page_standard_new_with_vals (_("Connect To A Server"),
																NULL, pixbuf);
	icon_pixbuf = gtk_widget_render_icon (win->id_page, GNOMECHAT_STOCK_SERVER,
										  GTK_ICON_SIZE_DIALOG, NULL);
	gnome_druid_page_standard_set_logo (GNOME_DRUID_PAGE_STANDARD (win->server_page), icon_pixbuf);
	g_object_unref (icon_pixbuf);
	gnome_druid_append_page (GNOME_DRUID (win->druid), GNOME_DRUID_PAGE (win->server_page));
	g_signal_connect_after (win->server_page, "prepare", G_CALLBACK (prepare_server_page), win);

	vbox1 = gtk_vbox_new (FALSE, 4);
	gnome_druid_page_standard_append_item (GNOME_DRUID_PAGE_STANDARD (win->server_page),
										   _("What server do you want to _connect to?"), vbox1,
										   _("You can also connect from the \"Connect...\" item in "
											 "the \"File\" menu."));

	win->none_radio = gtk_radio_button_new_with_mnemonic (NULL, _("Do _not connect now"));
	g_signal_connect (win->none_radio, "toggled", G_CALLBACK (none_radio_toggled), win);
	gtk_box_pack_start (GTK_BOX (vbox1), win->none_radio, FALSE, FALSE, 0);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (vbox1), hbox, FALSE, FALSE, 0);

	win->existing_radio =
		gtk_radio_button_new_with_mnemonic_from_widget (GTK_RADIO_BUTTON (win->none_radio),
														_("_Connect to:"));
	g_signal_connect (win->existing_radio, "toggled", G_CALLBACK (none_radio_toggled), win);
	gtk_box_pack_start (GTK_BOX (hbox), win->existing_radio, FALSE, FALSE, 0);

	win->optmenu = gtk_option_menu_new ();
	gtk_option_menu_set_menu (GTK_OPTION_MENU (win->optmenu), prefs_ui_util_get_network_menu ());
	g_signal_connect (win->optmenu, "changed", G_CALLBACK (net_optmenu_changed_cb), &(win->net_id));
	gtk_box_pack_start (GTK_BOX (hbox), win->optmenu, FALSE, FALSE, 0);

	win->custom_radio =
		gtk_radio_button_new_with_mnemonic_from_widget (GTK_RADIO_BUTTON (win->existing_radio),
														_("Connect to a _different server"));
	g_signal_connect (win->custom_radio, "toggled", G_CALLBACK (none_radio_toggled), win);
	gtk_box_pack_start (GTK_BOX (vbox1), win->custom_radio, FALSE, FALSE, 0);

	win->custom_server_entry = gtk_entry_new ();
	g_signal_connect (win->custom_server_entry, "changed",
					  G_CALLBACK (custom_server_entry_changed), win);
	gtk_box_pack_start (GTK_BOX (vbox1), win->custom_server_entry, FALSE, FALSE, 0);

	/* Last Page */
	win->last_page = gnome_druid_page_standard_new_with_vals (_("Setup Finished"), NULL, pixbuf);
	icon_pixbuf = gtk_widget_render_icon (win->id_page, GNOMECHAT_STOCK_ICON,
										  GTK_ICON_SIZE_DIALOG, NULL);
	gnome_druid_page_standard_set_logo (GNOME_DRUID_PAGE_STANDARD (win->last_page), icon_pixbuf);
	g_object_unref (icon_pixbuf);
	gnome_druid_append_page (GNOME_DRUID (win->druid), GNOME_DRUID_PAGE (win->last_page));
	g_signal_connect_after (win->last_page, "prepare", G_CALLBACK (prepare_last_page), win);

	str = g_strdup_printf ("<b>%s</b>\n%s", _("Setup Finished"),
						   _("IRC Chat is now configured properly for your system.\n\n"
							 "To connect to a server the next time you start IRC Chat, use the "
							 "\"Connect...\" item in the \"IRC Chat\" menu.\n\n"
							 "To change other settings, use the \"Preferences...\" item  in the "
							 "\"Edit\" menu."));
	label = gtk_label_new (str);
	g_free (str);
	gtk_label_set_use_markup (GTK_LABEL (label), TRUE);
	gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
	gtk_container_add (GTK_CONTAINER (GNOME_DRUID_PAGE_STANDARD (win->last_page)->vbox), label);

	/* Initial Setup */
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (win->existing_radio), TRUE);
	gtk_option_menu_set_history (GTK_OPTION_MENU (win->optmenu), 2);
	gtk_widget_set_sensitive (win->custom_server_entry, FALSE);

	g_object_unref (pixbuf);

	/* Show all */
	gtk_widget_show_all (win->window);
	g_signal_emit_by_name (win->start_page, "prepare", win->druid);
}


/* PUT OFF UNTIL I CARE MORE :-)
enum
{
	TARGET_TEXT_XVCARD,
	TARGET_TEXT_DIRECTORY
};

static const GtkTargetEntry targets[] = {
	{"text/x-vcard", 0, TARGET_TEXT_XVCARD},
	{"text/directory", 0, TARGET_TEXT_DIRECTORY}
};

static void
vcard_button_clicked_cb (GtkButton * button,
						 gpointer data)
{
	WelcomeWin *win = data;
	static GtkWidget *dialog = NULL;

	WelcomeVCardWin *vcwin = NULL;
	GtkWidget *hbox,
	 *vbox;
	gchar *icon_filename;
	gint response;

	GnomeVFSHandle *handle;
	GnomeVFSFileInfo info;
	GnomeVFSFileSize bytes_done;
	gchar *path,
	 *vcard_filename = NULL,
	 *mimetype,
	 *vcard_text_uri;

	if (dialog)
	{
		gtk_window_present (GTK_WINDOW (dialog));
		return;
	}

	vcwin = g_new0 (WelcomeVCardWin, 1);
	vcwin->vcard = NULL;

	dialog = gtk_dialog_new_with_buttons (_("Business Card - GnomeChat"),
										  GTK_WINDOW (win->window),
										  (GTK_DIALOG_MODAL |
										   GTK_DIALOG_DESTROY_WITH_PARENT),
										  GTK_STOCK_CANCEL, GTK_RESPONSE_REJECT,
										  GTK_STOCK_OK, GTK_RESPONSE_ACCEPT,
										  NULL);
	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_ACCEPT);
	gtk_window_set_icon (GTK_WINDOW (dialog), VCARD_PIXBUF);
	gtk_widget_realize (dialog);
	gtk_drag_dest_set (dialog, GTK_DEST_DEFAULT_ALL, targets,
					   (guint) (sizeof (targets) / sizeof (GtkTargetEntry)),
					   GDK_ACTION_MOVE | GDK_ACTION_LINK | GDK_ACTION_COPY);
	g_signal_connect (dialog, "drag-motion",
					  G_CALLBACK (dialog_drag_motion_cb), NULL);
	g_signal_connect (dialog, "drag-leave",
					  G_CALLBACK (dialog_drag_leave_cb), NULL);
	g_signal_connect (dialog, "drag-data-received",
					  G_CALLBACK (dialog_drag_data_received_cb), vcwin);

	hbox = gtk_hbox_new (FALSE, 8);
	gtk_container_set_border_width (GTK_CONTAINER (hbox), 4);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (dialog)->vbox), hbox);

	icon_filename = gnome_program_locate_file (appinfo->program,
											   GNOME_FILE_DOMAIN_APP_PIXMAP,
											   "gnome-question.png", FALSE,
											   NULL);
	vcwin->icon = gtk_image_new_from_file (icon_filename);
	gtk_box_pack_start (GTK_BOX (hbox), vcwin->icon, FALSE, FALSE, 0);

	vbox = gtk_vbox_new (FALSE, 4);
	gtk_container_add (GTK_CONTAINER (hbox), vbox);

	vcwin->label = gtk_label_new (_("Drag a contact here to use it as your "
									"business card, or select a vCard file "
									"manually."));
	gtk_label_set_line_wrap (GTK_LABEL (vcwin->label), TRUE);
	gtk_label_set_use_markup (GTK_LABEL (vcwin->label), TRUE);
	gtk_misc_set_alignment (GTK_MISC (vcwin->label), 0.0, 0.5);
	gtk_container_add (GTK_CONTAINER (vbox), vcwin->label);

	vcwin->file_entry = gnome_file_entry_new ("vcard_files", _("Select A vCard "
															   "File "
															   "- GnomeChat"));
	gtk_widget_set_sensitive (gnome_file_entry_gnome_entry
					 (GNOME_FILE_ENTRY (vcwin->file_entry)), FALSE);
	g_signal_connect (gnome_file_entry_gtk_entry
					 (GNOME_FILE_ENTRY (vcwin->file_entry)), "changed", G_CALLBACK (file_entry_changed_cb), &vcard_filename);

	gtk_box_pack_start (GTK_BOX (vbox), vcwin->file_entry, FALSE, FALSE, 0);

	gtk_widget_show_all (hbox);

	gtk_widget_set_sensitive (win->window, FALSE);
	response = gtk_dialog_run (GTK_DIALOG (dialog));
	gtk_drag_dest_unset (dialog);
	gtk_widget_set_sensitive (win->window, TRUE);

	gtk_widget_destroy (dialog);
	dialog = NULL;

	if (response == GTK_RESPONSE_ACCEPT)
	{
		if (vcard_filename
			&& strcmp (_("(Imported)"), vcard_filename) != 0
			&& vcard_filename[0] == '/')
		{
			if (g_file_test (vcard_filename, G_FILE_TEST_EXISTS))
			{
				vcard_text_uri =
					gnome_vfs_get_uri_from_local_path (vcard_filename);
				mimetype = gnome_vfs_get_mime_type (vcard_text_uri);

				if (strcmp (mimetype, "text/x-vcard") == 0 &&
					strcmp (mimetype, "text/directory") == 0)
				{
					gnome_vfs_open (&handle, vcard_text_uri,
									GNOME_VFS_OPEN_READ);
					gnome_vfs_get_file_info_from_handle (handle, &info,
														 GNOME_VFS_FILE_INFO_DEFAULT);

					vcwin->vcard = g_new0 (gchar, info.size + 1);

					gnome_vfs_read (handle, vcwin->vcard, info.size,
									&bytes_done);
					gnome_vfs_close (handle);
				}
			}
		}

		if (vcwin->vcard)
		{
			path = g_strconcat (g_get_home_dir (), "/.gnomechat", NULL);
			mkdir (path, 0755);

			vcard_text_uri = g_strconcat ("file://", path, "/vcard.vcf", NULL);
			gnome_vfs_create (&handle, vcard_text_uri, GNOME_VFS_OPEN_WRITE,
							  FALSE, 0644);
			gnome_vfs_write (handle, vcwin->vcard, strlen (vcwin->vcard),
							 &bytes_done);
			gnome_vfs_close (handle);

			gtk_label_set_text (GTK_LABEL (win->vcard_label),
								util_get_vcard_name (vcwin->vcard));
		}
	}

	if (vcwin->vcard)
	{
		g_free (vcwin->vcard);
		vcwin->vcard = NULL;
		vcwin->win = NULL;
	}

	g_free (vcwin);
}


static void
file_entry_changed_cb (GtkEntry *entry, gchar **vcard_filename)
{
	if (*vcard_filename)
		g_free (*vcard_filename);

	*vcard_filename = g_strdup (gtk_entry_get_text (entry));
}


static gboolean
dialog_drag_motion_cb (GtkWidget * dialog,
					   GdkDragContext * context,
					   gint x,
					   gint y,
					   guint time,
					   gpointer data)
{
	return FALSE;
}


static void
dialog_drag_leave_cb (GtkWidget * dialog,
					  GdkDragContext * context,
					  guint time,
					  gpointer data)
{
	gtk_widget_queue_draw (dialog);
}


static void
dialog_drag_data_received_cb (GtkWidget * dialog,
							  GdkDragContext * context,
							  gint x,
							  gint y,
							  GtkSelectionData * selection_data,
							  guint info,
							  guint t,
							  gpointer data)
{
	WelcomeVCardWin *vcwin = data;

	if ((info == TARGET_TEXT_XVCARD || info == TARGET_TEXT_DIRECTORY)
		&& !g_ascii_strncasecmp ("BEGIN:VCARD", selection_data->data, 11))
	{
		if (vcwin->vcard)
			g_free (vcwin->vcard);

		gtk_entry_set_text (GTK_ENTRY
							(gnome_file_entry_gtk_entry
							 (GNOME_FILE_ENTRY (vcwin->file_entry))),
							_("(Imported)"));

		vcwin->vcard = g_strndup (selection_data->data, selection_data->length);

		gtk_image_set_from_pixbuf (GTK_IMAGE (vcwin->icon), VCARD_PIXBUF);

		gtk_label_set_text (GTK_LABEL (vcwin->label),
							_("You have selected your business card."));
	}
} */
